//
//  SearchAddressViewController.swift
//  ZIG
//
//  Created by Isaac on 05/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit

class SearchAddressViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var SearchTable: UITableView!
    
   
    @IBOutlet weak var AddressSearchtxt: UITextField!
    var addressStringFor:String?
    var addressString:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let passwordimage = UIImage(named: "source_Marker")
        
        addLeftImageTo(txtField:AddressSearchtxt,andImage:passwordimage!)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchAddress",
                                                     for: indexPath) as? SearchAddress
            else { return UITableViewCell() }
        
        
        return cell
    }

}
class SearchAddress:UITableViewCell{
    @IBOutlet weak var FirstAddress: UILabel!
    
    @IBOutlet weak var SecondAddress: UILabel!
    @IBOutlet weak var ResultImage: UIImageView!
}
