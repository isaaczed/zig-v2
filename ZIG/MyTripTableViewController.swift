//
//  MyTripTableViewController.swift
//  ZIG
//
//  Created by Isaac on 18/01/18.
//  Copyright © 2018 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import NYAlertViewController
import NVActivityIndicatorView
import CoreLocation
import XLActionController
import SafariServices
import SCLAlertView

class MyTripTableViewController: UITableViewController {
    var AssistiveTouchBool:Bool = true
    
    var assistiveTouch = AssistiveTouch()
    let assistiveTouchSOS = AssistiveTouch()
    var boltListApi: [Bolt_list]?

    var firstServiceCallComplete = false
    var secondServiceCallComplete = false
    var loaderView = UIView()
    var BirdBikeID = ""
    var BirdBikeLat = 0.0
    var BirdBikeLon = 0.0
    var BirdIsReserved = 0
    var BirdIsDisabled = 0
    var BirdSourceDistance = 0.0
    var BirdtempDistance = 0.0
    var templat = 0.0
    var templong = 0.0
    var uberBool = Bool()
    var isLyft = Bool()
    var BikedistanceInMeters = 0.0
    
    //
    var LimeBikeID = ""
    var LimeBikeLat = 0.0
    var LimeBikeLon = 0.0
    var LimeIsReserved = 0
    var LimeIsDisabled = 0
    var LimeSourceDistance = 0.0
    var LimetempDistance = 0.0
    var LimedistanceInMeters = 0.0
    var carSelected = Bool()
          var bikeSelected = Bool()
          var carFirstSelected = Bool()
          var bikeFirstSelected = Bool()
    //
    var BoltBikeID = ""
    var BoltBikeLat = 0.0
    var BoltBikeLon = 0.0
    var BoltIsReserved = 0
    var BoltIsDisabled = 0
    var BoltSourceDistance = 0.0
    var BolttempDistance = 0.0
    var BoltdistanceInMeters = 0.0
    var birdListApi: [data_list]?
    var LimeListApi: [Lime_list]?
    
    var savedTripArray : DataResponse<SavedRootClass>?
    @IBOutlet var myTripTableView: UITableView!
    var BirdDatalistapicount:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "perferenceSelectedSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("perferenceSelectedSaved"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
        
        analytics.GetPageHitCount(PageName: "Saved Trips")
        
        self.title = "Saved Trips"
        self.showLoader()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.title = "MY TICKETS"
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated:true);

        let userDefaults1 = UserDefaults.standard
        let accessToken = userDefaults1.value(forKey: "accessToken")
        let url = "\(APIUrl.ZIGTARCAPI)SaveTrips/Get"
        let parameters: Parameters = [
            "Token": accessToken!
        ]
      //  print("https://tripplan.ridetarc.org/ZIGSTarc/api/SaveTrips/Get?Token")
        
    
       
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<SavedRootClass>) in
                switch response.result {
                case .success:
                    self.hideLoader()
                    if (response.result.value?.trips?.count)! > 0{
                        print(response)
                        self.savedTripArray = response
                        self.myTripTableView.reloadData()
                    }else{
                        self.showAlertBox(text: "You don't have any saved trips")
                        self.OnBackClicked()
                    }
                        case .failure(let error):
                        print(error)
                        self.hideLoader()
                        self.showErrorDialogBox(viewController: self)
        }
        }
   
       
    }
    override func viewWillAppear(_ animated: Bool) {
        
            assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

            assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
            assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
            let window = UIApplication.shared.keyWindow!
            window.addSubview(assistiveTouch)
       
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
               tapGesture.numberOfTapsRequired = 1
               assistiveTouchSOS.addGestureRecognizer(tapGesture)
               let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
               longPress.minimumPressDuration = 1.0
               self.assistiveTouchSOS.addGestureRecognizer(longPress)
               assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
               assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
                //let window = UIApplication.shared.keyWindow!
                         // window.addSubview(assistiveTouchSOS)

         
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
          if gesture.state == UIGestureRecognizerState.began {
              UIDevice.vibrate()
              print("Long Press")
              currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                         if Success{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                          print("\(Address),\(LatsosLat),\(Longsoslong)")
                          MapsScheduleVC.CurrentlocationAddressString = Address
                          MapsScheduleVC.latSOS = LatsosLat
                          MapsScheduleVC.longSOS = Longsoslong
                        self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                 //self.CurrentlocationAddress.text = Address
                             }
                         }
              
          }
      }
    
      @objc func tappedSOS(sender: UIButton) {
       
          let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                  // action here
          }
                  
          SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
         
      }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        assistiveTouch.removeFromSuperview()
         assistiveTouchSOS.removeFromSuperview()
         NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }
    @objc func tapped(sender: UIButton) {
        print("\(sender) has been touched")
        
        
        let actionController = TwitterActionController()
        
        actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        if comVar.DateImplementation {
            
            actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
//                self.navigationController?.pushViewController(nextViewController, animated: true)
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                           
                           
            }))
            
        }
        
        
        actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        actionController.addAction(Action(ActionData(title: "Preferences", subtitle: "", image: UIImage.init(named: "Image-1")!), style: .default, handler: { action in
            let userDefaults1 = UserDefaults.standard
            self.uberBool = userDefaults1.bool(forKey: "isUber")
            self.isLyft = userDefaults1.bool(forKey: "isLyft")
 
            comVar.status = 7
       
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
            vc.uberSelected = self.uberBool
            vc.lyftSelected = self.isLyft
            vc.isclickFromTripPlanner = true
            vc.saveTripBool = true
            self.present(vc, animated: true, completion: nil)
            
        }))
        actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
            
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        actionController.headerData = "Quick View"
        present(actionController, animated: true, completion: nil)
        
        
        
        
    }
    @objc func OnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if ((savedTripArray?.result.value?.trips) != nil) {
            return (savedTripArray!.result.value?.trips?.count)!
        }
        return 0;
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! MyTicketTableViewCell
        
        //let valueDic = savedTripArray[indexPath.row] as! NSMutableDictionary
        let valueData = savedTripArray?.result.value?.trips?[indexPath.row]
        cell.tripLable.frame = CGRect.init(x: 30, y: 10, width: cell.frame.size.width, height: 30)
        cell.tripLable.text = "Trip \(indexPath.row+1)"
        cell.tripLable.font = UIFont.setTarcBold(size: 20.0)
        cell.tripLable.textColor = UIColor.black
        cell.tripLable.textAlignment = NSTextAlignment.left
        cell.contentView.addSubview(cell.tripLable)
        
        cell.fromLable.frame = CGRect.init(x: 30, y: 50, width: 50, height: 30)
        cell.fromLable.text = "FROM :  "
        cell.fromLable.font = UIFont.setTarcBold(size: 13.0)
        cell.fromLable.textColor = UIColor.black
        cell.fromLable.textAlignment = NSTextAlignment.left
        cell.contentView.addSubview(cell.fromLable)
        
        cell.fromTextLable.frame = CGRect.init(x: 80, y: 50, width: cell.frame.size.width-80, height: 30)
        cell.fromTextLable.text = valueData?.sourceAddress
        cell.fromTextLable.font = UIFont.setTarcRegular(size: 13.0)
        cell.fromTextLable.textColor = UIColor.black
        cell.fromTextLable.textAlignment = NSTextAlignment.left
        //
        cell.fromTextLable.lineBreakMode = .byWordWrapping
        cell.fromTextLable.numberOfLines = 2
        cell.contentView.addSubview(cell.fromTextLable)
        
        cell.toLable.frame = CGRect.init(x: 30, y: 80, width: 50, height: 30)
        cell.toLable.text = "TO       :  "
        cell.toLable.font = UIFont.setTarcBold(size: 13.0)
        cell.toLable.textColor = UIColor.black
        cell.toLable.textAlignment = NSTextAlignment.left
        cell.contentView.addSubview(cell.toLable)
        
        cell.toTextLable.frame = CGRect.init(x: 80, y: 80, width: cell.frame.size.width-80, height: 30)
        cell.toTextLable.text = valueData?.destinationAddress
        cell.toTextLable.font = UIFont.setTarcRegular(size: 13.0)
        cell.toTextLable.textColor = UIColor.black
        cell.toTextLable.textAlignment = NSTextAlignment.left
        cell.toTextLable.lineBreakMode = .byWordWrapping
        cell.toTextLable.numberOfLines = 2
        cell.contentView.addSubview(cell.toTextLable)
        
        
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    showLoader()
         let cell = tableView.cellForRow(at: indexPath) as! MyTicketTableViewCell
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripSelectionViewController") as! TripSelectionViewController
        //let valueDic = savedTripArray[indexPath.row] as! NSMutableDictionary
        nextViewController.sourceaddressString = cell.fromTextLable.text
        nextViewController.DestinationAddressString = cell.toTextLable.text
        nextViewController.ameniticsTitle = ""
        nextViewController.AmenititesTypeTrip = ""
        let userDefaults1 = UserDefaults.standard
        let birdOn = userDefaults1.bool(forKey: "isBirdOn")
        let LimeOn = userDefaults1.bool(forKey: "LimeOn")
        let BoltOn = userDefaults1.bool(forKey: "BoltOn")
        
        print(cell.toTextLable.text!)
        print(cell.fromTextLable.text!)
        let valueData = savedTripArray?.result.value?.trips?[indexPath.row]
       
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json"
        let parameters: Parameters = [
            "latlng": "\(Double((valueData?.sourceLat)!)),\(Double((valueData?.sourceLong)!))",
            "key": Key.GoogleAPIKey
        ]
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<MyLocationAddress>) in
                switch response.result {
                case .success:
                    print(response)
                    //to get status code
                    if (response.result.value?.results?.count)! > 0
                    {
                    
                    let weatherResponse = response.result.value?.results![0]
                    print(weatherResponse ?? "nil")
                    
                       

                    nextViewController.sourceString = (weatherResponse?.placeId)!
                    nextViewController.sourceLat = Double((valueData?.sourceLat)!)
                    nextViewController.sourceLng = Double((valueData?.sourceLong)!)
                       
                    
                    
                    let url1 = "https://maps.googleapis.com/maps/api/geocode/json"
                    let parameters1: Parameters = [
                        "latlng": "\(Double((valueData?.destinationLat)!)),\(Double((valueData?.destinationLong)!))",
                        "key": Key.GoogleAPIKey
                    ]
                    
                    var CheckBikeCount = 0
                    Alamofire.request(url1, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                        .responseObject{ (response: DataResponse<MyLocationAddress>) in
                            switch response.result {
                            case .success:
                                print(response)
                                //to get status code
                                
                                if (response.result.value?.results?.count)! > 0
                                {
                                let weatherResponse1 = response.result.value?.results![0]
                                print(weatherResponse1 ?? "nil")
                                nextViewController.destinationString = (weatherResponse1?.placeId)!
                                nextViewController.destinationLat = Double((valueData?.destinationLat)!)
                                nextViewController.destinationLng = Double((valueData?.destinationLong)!)
                               // self.navigationController?.pushViewController(nextViewController, animated: true)
                               
                                   
                                    comFunc.GetLYFT(SourceLat: (valueData?.sourceLat)!, SourceLong: (valueData?.sourceLong)!, DestinationLat: (valueData?.destinationLat)!, DestinationLong: (valueData?.destinationLong)!)
                                    
                                    comFunc.uberDataApi(sourceLat: (valueData?.sourceLat)!, sourceLng: (valueData?.sourceLong)!, destinationLng: (valueData?.destinationLong)!, destinationLat: (valueData?.destinationLat)!)
                                    if birdOn
                                    {
                                    Alamofire.request("https://mds.bird.co/gbfs/louisville/free_bikes").responseObject { (response: DataResponse<getbirdstation>) in
                                        switch response.result {
                                        case .success:
                                            self.secondServiceCallComplete = true
                                           
                                            if response.result.value?.code != 400 && (response.result.value?.total)! > 0 && response.result.value!.datalistfromapi!.count > 0{
                                                
                                                let Sourcelatlatlong = CLLocation(latitude: Double((weatherResponse?.geometry?.location?.lat)!), longitude: Double((weatherResponse?.geometry?.location?.lng)!))
                                                
                                                let responseResutl = response.result.value
                                                self.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                                                nextViewController.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                                                self.birdListApi = responseResutl?.datalistfromapi
                                                nextViewController.birdListApi = self.birdListApi
                                                
                                                for BirdBike in (responseResutl?.datalistfromapi)! {
                                                    
                                                    
                                                    let ss = CLLocation(latitude: BirdBike.bike_lat!, longitude: BirdBike.bike_lon!)
                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                    print(dd)
                                                    print((dd*100).rounded()/100)
                                                    
                                                    
                                                    
                                                    
                                                    if CheckBikeCount == 0 {
                                                        
                                                        self.BirdBikeID = BirdBike.bike_id!
                                                        self.BirdBikeLat = BirdBike.bike_lat!
                                                        self.BirdBikeLon = BirdBike.bike_lon!
                                                        self.BirdIsReserved = BirdBike.bike_is_reserved!
                                                        self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                                        let destinationlatlong = CLLocation(latitude: self.BirdBikeLat, longitude: self.BirdBikeLon)
                                                        self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                        self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                        CheckBikeCount = CheckBikeCount + 1
                                                        
                                                        nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                        nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                        nextViewController.BikedistanceInMeters = self.BirdtempDistance
                                                        
                                                        
                                                    }
                                                    else{
                                                        self.templat = BirdBike.bike_lat!
                                                        self.templong = BirdBike.bike_lon!
                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                        self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                        self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                        if self.BirdtempDistance < self.BirdSourceDistance {
                                                            
                                                            self.BirdBikeID = BirdBike.bike_id!
                                                            self.BirdBikeLat = BirdBike.bike_lat!
                                                            self.BirdBikeLon = BirdBike.bike_lon!
                                                            self.BirdIsReserved = BirdBike.bike_is_reserved!
                                                            self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                                            self.BirdSourceDistance = self.BirdtempDistance
                                                            nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                            nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                            nextViewController.BikedistanceInMeters = self.BirdtempDistance
                                                            CheckBikeCount = CheckBikeCount + 1
                                                            
                                                        }
                                                        else{
                                                            CheckBikeCount = CheckBikeCount + 1
                                                        }
                                                    }
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }
                                                print("\(self.BirdBikeLat) Bird")
                                                print(self.BirdBikeLon)
                                                print(self.BirdSourceDistance)
                                                // Lime Integration
                                                var LimeCheckBikeCount = 0
                                                
                                                if LimeOn
                                              { Alamofire.request("https://data.lime.bike/api/partners/v1/gbfs/louisville/free_bike_status").responseObject { (response: DataResponse<getLimestation>) in
                                                    switch response.result {
                                                    case .success:
                                                        self.secondServiceCallComplete = true
                                                        if response.result.value?.code != 400 {
                                                            if (response.result.value?.total)! >= 0 {
                                                                let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                
                                                                let responseResutl = response.result.value
                                                                print("\(responseResutl?.datalistfromapi?.count)")
                                                                self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                nextViewController.LimeDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                
                                                                self.LimeListApi = responseResutl?.datalistfromapi
                                                                nextViewController.LimeListApi = self.LimeListApi
                                   
                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                    let LimeLatitude = Double("\(BirdBikeLat)")
                                                                    let LimeLontitude = Double("\(BirdBikeLng)")
                                                                    
                                                                    let ss = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                    //  print(dd)
                                                                    // print((dd*100).rounded()/100)
                                                                    
                                                                    
                                                                    
                                                                    
                                                    if LimeCheckBikeCount == 0 {
                                                                        
                                        if LimeBike.bike_id != nil {  self.LimeBikeID = LimeBike.bike_id!
                                                            }
                                                                        else
                                                           {
                                                            self.LimeBikeID = "0"
                                                                        }
                                                                        self.LimeBikeLat = LimeLatitude!
                                                                        self.LimeBikeLon = LimeLontitude!
                                                                        self.LimeIsReserved = LimeBike.bike_is_reserved!
                                                                        self.LimeIsDisabled = LimeBike.bike_is_disabled!
                                                                        let destinationlatlong = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                        self.LimedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                        LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                        
                                                        nextViewController.LimeBikeLat = LimeLatitude!
                                                        nextViewController.LimeBikeLon = LimeLontitude!
                                                                        
                                                                        
                                                                        //print(self.BirdSourceDistance)
                                                                        nextViewController.LimedistanceInMeters = self.BirdSourceDistance
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.templat = LimeLatitude!
                                                                        self.templong = LimeLontitude!
                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                        self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                        if self.BirdtempDistance < self.BirdSourceDistance {
                                                                            
                                                if LimeBike.bike_id != nil
                                                            {
                                                    self.BirdBikeID = LimeBike.bike_id!
                                                            }
                                                            else
                                                            {
                                                    self.BirdBikeID = "0"
                                                        }
                                                self.BirdBikeLat = LimeLatitude!
                                                                            self.BirdBikeLon = LimeLontitude!
                                                                            self.BirdIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BirdIsDisabled = LimeBike.bike_is_disabled!
                                                                            self.BirdSourceDistance = self.BirdtempDistance
                                                                            nextViewController.LimeBikeLat = LimeLatitude!
                                                                            nextViewController.LimeBikeLon = LimeLontitude!
                                                                            // print(self.BirdtempDistance)
                                                                            nextViewController.LimedistanceInMeters = self.BirdtempDistance
                                                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                            
                                                                        }
                                                                        else{
                                                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                // Bolt Integration
                                                                var BoltCheckBikeCount = 0
                                                                
                                                               if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                    switch response.result {
                                                                    case .success:
                                                                        self.secondServiceCallComplete = true
                                                                        if response.result.value?.code != 400 && response.result.value?.code != nil {
                                                                            if (response.result.value?.total)! >= 0 {
                                                                                
                                                                                
                                                                                
                                                                                let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                
                                                                                let responseResutl = response.result.value
                                                                                print("\(responseResutl?.datalistfromapi?.count)")
                                                                                SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                                                print("*****Bolt")
                                                                                self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                self.boltListApi = responseResutl?.datalistfromapi
                                                                                nextViewController.boltListApi = self.boltListApi
                                                                                
                                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                    let BoltLatitude = Double("\(LimeBike.bike_lat!)")
                                                                                    let BoltLontitude = Double("\(LimeBike.bike_lon!)")
                                                                                    print("AARUN ==> LAT\(BoltLatitude) and Lan \(BoltLontitude)")
                                                                                    let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                                    //  print(dd)
                                                                                    // print((dd*100).rounded()/100)
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    if BoltCheckBikeCount == 0 {
                                                                                        
                                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                                        self.BoltBikeLat = BoltLatitude!
                                                                                        self.BoltBikeLon = BoltLontitude!
                                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                        let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                        self.BoltdistanceInMeters = 0.0
                                                                                        let distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong) as Double
                                                                                        self.BoltdistanceInMeters = distanceInMeters
                                                                                        self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                        print("AARUN ==> Source \(self.BoltSourceDistance)")
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        
                                                                                        print("Lime Bike \(self.LimeBikeLat) and \(self.LimeBikeLon)")
                                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                                        
                                                                                        
                                                                                        //print(self.BirdSourceDistance)
                                                                                        nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                        
                                                                                        
                                                                                        
                                                                                    }
                                                                                    else{
                                                                                        self.templat = BoltLatitude!
                                                                                        self.templong = BoltLontitude!
                                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                        self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                        print("AARUN ==> Distance \(self.BolttempDistance)")
                                                                                        //BolttempDistance
                                                                                        if self.BolttempDistance < self.BoltSourceDistance {
                                                                                            
                                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                                            self.BoltBikeLat = BoltLatitude!
                                                                                            self.BoltBikeLon = BoltLontitude!
                                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                            self.BoltSourceDistance = self.BolttempDistance
                                                                                            print("AARUN ==> Distance \(self.BoltSourceDistance)")
                                                                                            nextViewController.BoltBikeLat = BoltLatitude!
                                                                                            nextViewController.BoltBikeLon = BoltLontitude!
                                                                                            // print(self.BirdtempDistance)
                                                                                            nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                            
                                                                                        }
                                                                                        else{
                                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        }
                                                                                    }
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                
                                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                
                                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                
                                                                                self.hideLoader()
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                        }
                                                                        else{
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            self.hideLoader()
                                                                            
                                                                        }
                                                                        
                                                                    case .failure(let error):
                                                                        print(error)
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                        
                                                                    }
                                                                }
                                                            }
                                                            else
                                                               {
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                                }
                                                                
                                                                
                                                                
                                                            }
                                                        }
                                                        else{
                                                            // Bolt Integration
                                                            var BoltCheckBikeCount = 0
                                                            
                                                            if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                switch response.result {
                                                                case .success:
                                                                    self.secondServiceCallComplete = true
                                                                    if response.result.value?.code != 400 &&  response.result.value?.code != nil {
                                                                        if (response.result.value?.total)! >= 0 {
                                                                            let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                            let responseResutl = response.result.value
                                                                            self.boltListApi = responseResutl?.datalistfromapi
                                                                            nextViewController.boltListApi = self.boltListApi
                                                                            self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                            nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                let BoltLatitude = Double("\(LimeBike.bike_lat!)")
                                                                                let BoltLontitude = Double("\(LimeBike.bike_lon!)")
                                                                                
                                                                                let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                                //  print(dd)
                                                                                // print((dd*100).rounded()/100)
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                if BoltCheckBikeCount == 0 {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                                    
                                                                                    
                                                                                    //print(self.BirdSourceDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                else{
                                                                                    self.templat = BoltLatitude!
                                                                                    self.templong = BoltLontitude!
                                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    if self.BolttempDistance < self.BoltSourceDistance {
                                                                                        
                                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                                        self.BoltBikeLat = BoltLatitude!
                                                                                        self.BoltBikeLon = BoltLontitude!
                                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                        self.BoltSourceDistance = self.BolttempDistance
                                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                                        // print(self.BirdtempDistance)
                                                                                        nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        
                                                                                    }
                                                                                    else{
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                            
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            
                                                                            self.hideLoader()
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                                    else{
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                        
                                                                    }
                                                                    
                                                                case .failure(let error):
                                                                    print(error)
                                                                    
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                    
                                                                }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                            }
                                                        }
                                                        
                                                    case .failure(let error):
                                                        print(error)
                                                        
                                                        
                                                                    var BoltCheckBikeCount = 0
                                                                    
                                                                   if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                        switch response.result {
                                                                        case .success:
                                                                            self.secondServiceCallComplete = true
                                                                            if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                if (response.result.value?.total)! >= 0 {
                                                                                     let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                    let responseResutl = response.result.value
                                                            self.boltListApi = responseResutl?.datalistfromapi
                                                        nextViewController.boltListApi = self.boltListApi
                                                                                    self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                    nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                    for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                        let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                        let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                        let BoltLatitude = Double("\(LimeBike.bike_lat!)")
                                                                                        let BoltLontitude = Double("\(LimeBike.bike_lon!)")
                                                                                        
                                                                                        let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                        let dd = Sourcelatlatlong.distance(from: ss)
                                                                                        //  print(dd)
                                                                                        // print((dd*100).rounded()/100)
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        if BoltCheckBikeCount == 0 {
                                                                                            
                                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                                            self.BoltBikeLat = BoltLatitude!
                                                                                            self.BoltBikeLon = BoltLontitude!
                                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                            let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                            self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                            
                                                                                            nextViewController.BoltBikeLat = BoltLatitude!
                                                                                            nextViewController.BoltBikeLon = BoltLontitude!
                                                                                            
                                                                                            
                                                                                            //print(self.BirdSourceDistance)
                                                                                            nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                            
                                                                                            
                                                                                            
                                                                                        }
                                                                                        else{
                                                                                            self.templat = BoltLatitude!
                                                                                            self.templong = BoltLontitude!
                                                                                            let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                            self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                            if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                
                                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                self.BoltSourceDistance = self.BolttempDistance
                                                                                                nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                // print(self.BirdtempDistance)
                                                                                                nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                
                                                                                            }
                                                                                            else{
                                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                    }
                                                                                    
                                                                                    nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                    
                                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                    
                                                                                    self.hideLoader()
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                            }
                                                                            else{
                                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                self.hideLoader()
                                                                                
                                                                            }
                                                                            
                                                                        case .failure(let error):
                                                                            print(error)
                                                                            
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            self.hideLoader()
                                                                            
                                                                        }
                                                                    }
                                                    }
                                                    else
                                                                   {
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                        }
                                                        
                                                    }
                                                }
                                                
                                            }
                                         else
                                              {
                                               var BoltCheckBikeCount = 0
                                                                                                                         if BoltOn {
                                                                                                                         Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                                                             switch response.result {
                                                                                                                             case .success:
                                                                                                                                 self.secondServiceCallComplete = true
                                                                                                                                 if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                                                                     if (response.result.value?.total)! >= 0 {
                                                                                                                                            let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                                                         
                                                                                                                                         let responseResutl = response.result.value
                                                                                                                 self.boltListApi = responseResutl?.datalistfromapi
                                                                                                                                         SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                                                                             nextViewController.boltListApi = self.boltListApi
                                                                                          print("\(responseResutl?.datalistfromapi?.count)")
                                                                                                                                         self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                                                         nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                                                         for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                                                             let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                                                             let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                                                             let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                                                                             let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                                                                             
                                                                                                                                             let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                                             let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                                                             //  print(dd)
                                                                                                                                             // print((dd*100).rounded()/100)
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             if BoltCheckBikeCount == 0 {
                                                                                                                                                 
                                                                                                                                                 self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                                 self.BoltBikeLat = BoltLatitude!
                                                                                                                                                 self.BoltBikeLon = BoltLontitude!
                                                                                                                                                 self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                                 self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                                 let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                                                 self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                                 self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                                 BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                                 
                                                                                                                                                 nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                                 nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                                 
                                                                                                                                                 
                                                                                                                                                 //print(self.BirdSourceDistance)
                                                                                                                                                 nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                                                                 
                                                                                                                                                 
                                                                                                                                                 
                                                                                                                                             }
                                                                                                                                             else{
                                                                                                                                                 self.templat = BoltLatitude!
                                                                                                                                                 self.templong = BoltLontitude!
                                                                                                                                                 let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                                                                 self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                                 self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                                 if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                                                                     
                                                                                                                                                     self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                                     self.BoltBikeLat = BoltLatitude!
                                                                                                                                                     self.BoltBikeLon = BoltLontitude!
                                                                                                                                                     self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                                     self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                                     self.BoltSourceDistance = self.BolttempDistance
                                                                                                                                                     nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                                     nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                                     // print(self.BirdtempDistance)
                                                                                                                                                     nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                                                                     BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                                     
                                                                                                                                                 }
                                                                                                                                                 else{
                                                                                                                                                     BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                                 }
                                                                                                                                             }
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                         }
                                                                                                                                         
                                                                                                                                         nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                                                         
                                                                                                                                         self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                         self.hideLoader()
                                                                                                                                         //self.hideLoader()
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                     }
                                                                                                                                 }
                                                                                                                                 else{
                                                                                                                                     self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                     self.hideLoader()
                                                                                                                                     
                                                                                                                                 }
                                                                                                                                 
                                                                                                                             case .failure(let error):
                                                                                                                                 print(error)
                                                                                                                                 self.hideLoader()
                                                                                                                                 self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                 
                                                                                                                             }
                                                                                                                         }
                                                                                          
                                                                                      }
                                                else
                                                                                                                         {
                                                                                                                            self.hideLoader()
                                                                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                }
                                             //bolt
                                            }
                                                
                                                
                                                
                                                
                                                
                                            }
                                            
                                            else{
                                                // Lime Integration
                                                var LimeCheckBikeCount = 0
                                                if  LimeOn
                                              { Alamofire.request("https://data.lime.bike/api/partners/v1/gbfs/louisville/free_bike_status").responseObject { (response: DataResponse<getLimestation>) in
                                                    switch response.result {
                                                    case .success:
                                                        self.secondServiceCallComplete = true
                                                        if response.result.value?.code != 400 {
                                                            if (response.result.value?.total)! >= 0 {
                                                                let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                
                                                                let responseResutl = response.result.value
                                                                print("\(responseResutl?.datalistfromapi?.count)")
                                                                self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                nextViewController.LimeDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                
                                                                self.LimeListApi = responseResutl?.datalistfromapi
                                                                nextViewController.LimeListApi = self.LimeListApi
                                                                
                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                    let LimeLatitude = Double("\(BirdBikeLat)")
                                                                    let LimeLontitude = Double("\(BirdBikeLng)")
                                                                    
                                                                    let ss = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                    //  print(dd)
                                                                    // print((dd*100).rounded()/100)
                                                                    
                                                                    
                                                                    
                                                                    
                                    if LimeCheckBikeCount == 0 {
                                                                        
                                //self.LimeBikeID = LimeBike.bike_id!
                                                                        self.LimeBikeLat = LimeLatitude!
                                                                        self.LimeBikeLon = LimeLontitude!
                                                                        self.LimeIsReserved = LimeBike.bike_is_reserved!
                                                                        self.LimeIsDisabled = LimeBike.bike_is_disabled!
                                                                        let destinationlatlong = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                        self.LimedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                        LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                        
                                                                        nextViewController.LimeBikeLat = LimeLatitude!
                                                                        nextViewController.LimeBikeLon = LimeLontitude!
                                                                        
                                                                        
                                                                        //print(self.BirdSourceDistance)
                                                                        nextViewController.LimedistanceInMeters = self.BirdSourceDistance
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.templat = LimeLatitude!
                                                                        self.templong = LimeLontitude!
                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                        self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                        if self.BirdtempDistance < self.BirdSourceDistance {
                                                                            
                                                                            //self.BirdBikeID = LimeBike.bike_id!
                                                                            self.BirdBikeLat = LimeLatitude!
                                                                            self.BirdBikeLon = LimeLontitude!
                                                                            self.BirdIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BirdIsDisabled = LimeBike.bike_is_disabled!
                                                                            self.BirdSourceDistance = self.BirdtempDistance
                                                                            nextViewController.LimeBikeLat = LimeLatitude!
                                                                            nextViewController.LimeBikeLon = LimeLontitude!
                                                                            // print(self.BirdtempDistance)
                                                                            nextViewController.LimedistanceInMeters = self.BirdtempDistance
                                                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                            
                                                                        }
                                                                        else{
                                                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                // Bolt Integration
                                                                var BoltCheckBikeCount = 0
                                                                if BoltOn {
                                                                Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                    switch response.result {
                                                                    case .success:
                                                                        self.secondServiceCallComplete = true
                                                                        if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                            if (response.result.value?.total)! >= 0 {
                                                                                  let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                
                                                                                let responseResutl = response.result.value
                                                self.boltListApi = responseResutl?.datalistfromapi
                                                                                SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                                                print("*********************Bolt")
                                        nextViewController.boltListApi = self.boltListApi

                                                                                self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                    let BoltLatitude = Double("\(LimeBike.bike_lat!)")
                                                                                    let BoltLontitude = Double("\(LimeBike.bike_lon!)")
                                                                                    
                                                                                    let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                                    //  print(dd)
                                                                                    // print((dd*100).rounded()/100)
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    if BoltCheckBikeCount == 0 {
                                                                                        
                                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                                        self.BoltBikeLat = BoltLatitude!
                                                                                        self.BoltBikeLon = BoltLontitude!
                                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                        let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                        self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        
                                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                                        
                                                                                        
                                                                                        //print(self.BirdSourceDistance)
                                                                                        nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                        
                                                                                        
                                                                                        
                                                                                    }
                                                                                    else{
                                                                                        self.templat = BoltLatitude!
                                                                                        self.templong = BoltLontitude!
                                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                        self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                        if self.BolttempDistance < self.BoltSourceDistance {
                                                                                            
                                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                                            self.BoltBikeLat = BoltLatitude!
                                                                                            self.BoltBikeLon = BoltLontitude!
                                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                            self.BoltSourceDistance = self.BolttempDistance
                                                                                            nextViewController.BoltBikeLat = BoltLatitude!
                                                                                            nextViewController.BoltBikeLon = BoltLontitude!
                                                                                            // print(self.BirdtempDistance)
                                                                                            nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                            
                                                                                        }
                                                                                        else{
                                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        }
                                                                                    }
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                
                                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                
                                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                
                                                                                self.hideLoader()
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                        }
                                                                        else{
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            self.hideLoader()
                                                                            
                                                                        }
                                                                        
                                                                    case .failure(let error):
                                                                        print(error)
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                                   self.hideLoader()
                                                                                                                                                   
                                                                        
                                                                    }
                                                                }
                                                            }
                                                             else
                                                                {
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                }
                                                                
                                                                
                                                                
                                                            }
                                                        }
                                                        else{
                                                            // Bolt Integration
                                                            var BoltCheckBikeCount = 0
                                                            
                                                            if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                switch response.result {
                                                                case .success:
                                                                    self.secondServiceCallComplete = true
                                                                    if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                        if (response.result.value?.total)! >= 0 {
                                                                               let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                            
                                                                            let responseResutl = response.result.value
                                                    self.boltListApi = responseResutl?.datalistfromapi
                                                                            SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                nextViewController.boltListApi = self.boltListApi
                             print("\(responseResutl?.datalistfromapi?.count)")
                                                                            self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                            nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                
                                                                                let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                                //  print(dd)
                                                                                // print((dd*100).rounded()/100)
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                if BoltCheckBikeCount == 0 {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                                    
                                                                                    
                                                                                    //print(self.BirdSourceDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                else{
                                                                                    self.templat = BoltLatitude!
                                                                                    self.templong = BoltLontitude!
                                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    if self.BolttempDistance < self.BoltSourceDistance {
                                                                                        
                                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                                        self.BoltBikeLat = BoltLatitude!
                                                                                        self.BoltBikeLon = BoltLontitude!
                                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                        self.BoltSourceDistance = self.BolttempDistance
                                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                                        // print(self.BirdtempDistance)
                                                                                        nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        
                                                                                    }
                                                                                    else{
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                            
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            self.hideLoader()
                                                                            //self.hideLoader()
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                                    else{
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                        
                                                                    }
                                                                    
                                                                case .failure(let error):
                                                                    print(error)
                                                                    self.hideLoader()
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    
                                                                }
                                                            }
                                                        }
                                                       else
                                                            {
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                            }
                                                            
                                                        }
                                                        
                                                    case .failure(let error):
                                                        print(error)
                                                        
                                                                                                                   var BoltCheckBikeCount = 0
                                                                                       if BoltOn {
                                                                                       Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                           switch response.result {
                                                                                           case .success:
                                                                                               self.secondServiceCallComplete = true
                                                                                               if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                                   if (response.result.value?.total)! >= 0 {
                                                                                                          let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                       
                                                                                                       let responseResutl = response.result.value
                                                                               self.boltListApi = responseResutl?.datalistfromapi
                                                                                                       SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                                           nextViewController.boltListApi = self.boltListApi
                                                        print("\(responseResutl?.datalistfromapi?.count)")
                                                                                                       self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                       nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                       for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                           let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                           let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                           let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                                           let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                                           
                                                                                                           let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                           let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                           //  print(dd)
                                                                                                           // print((dd*100).rounded()/100)
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                           if BoltCheckBikeCount == 0 {
                                                                                                               
                                                                                                               self.BoltBikeID = LimeBike.bike_id!
                                                                                                               self.BoltBikeLat = BoltLatitude!
                                                                                                               self.BoltBikeLon = BoltLontitude!
                                                                                                               self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                               self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                               let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                               self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                               self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                               BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                               
                                                                                                               nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                               nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                               
                                                                                                               
                                                                                                               //print(self.BirdSourceDistance)
                                                                                                               nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                               
                                                                                                               
                                                                                                               
                                                                                                           }
                                                                                                           else{
                                                                                                               self.templat = BoltLatitude!
                                                                                                               self.templong = BoltLontitude!
                                                                                                               let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                               self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                               self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                               if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                                   
                                                                                                                   self.BoltBikeID = LimeBike.bike_id!
                                                                                                                   self.BoltBikeLat = BoltLatitude!
                                                                                                                   self.BoltBikeLon = BoltLontitude!
                                                                                                                   self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                   self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                   self.BoltSourceDistance = self.BolttempDistance
                                                                                                                   nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                   nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                   // print(self.BirdtempDistance)
                                                                                                                   nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                                   BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                   
                                                                                                               }
                                                                                                               else{
                                                                                                                   BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                               }
                                                                                                           }
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                       }
                                                                                                       
                                                                                                       nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                       
                                                                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                       self.hideLoader()
                                                                                                       //self.hideLoader()
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                   }
                                                                                               }
                                                                                               else{
                                                                                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                   self.hideLoader()
                                                                                                   
                                                                                               }
                                                                                               
                                                                                           case .failure(let error):
                                                                                               print(error)
                                                                                               self.hideLoader()
                                                                                               self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                               
                                                                                           }
                                                                                       }
                                                        
                                                    }
                                                     else
                                                                                       {
                                                                                        
                                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                        self.hideLoader()
                                                                                        
                                                        }
                                                    }
                                                }
                                                
                                            }
                                             else
                                              {
                                                var BoltCheckBikeCount = 0
                                                                                                                         if BoltOn {
                                                                                                                         Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                                                             switch response.result {
                                                                                                                             case .success:
                                                                                                                                 self.secondServiceCallComplete = true
                                                                                                                                 if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                                                                     if (response.result.value?.total)! >= 0 {
                                                                                                                                            let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                                                         
                                                                                                                                         let responseResutl = response.result.value
                                                                                                                 self.boltListApi = responseResutl?.datalistfromapi
                                                                                                                                         SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                                                                             nextViewController.boltListApi = self.boltListApi
                                                                                          print("\(responseResutl?.datalistfromapi?.count)")
                                                                                                                                         self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                                                         nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                                                         for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                                                             let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                                                             let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                                                             let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                                                                             let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                                                                             
                                                                                                                                             let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                                             let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                                                             //  print(dd)
                                                                                                                                             // print((dd*100).rounded()/100)
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             if BoltCheckBikeCount == 0 {
                                                                                                                                                 
                                                                                                                                                 self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                                 self.BoltBikeLat = BoltLatitude!
                                                                                                                                                 self.BoltBikeLon = BoltLontitude!
                                                                                                                                                 self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                                 self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                                 let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                                                 self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                                 self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                                 BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                                 
                                                                                                                                                 nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                                 nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                                 
                                                                                                                                                 
                                                                                                                                                 //print(self.BirdSourceDistance)
                                                                                                                                                 nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                                                                 
                                                                                                                                                 
                                                                                                                                                 
                                                                                                                                             }
                                                                                                                                             else{
                                                                                                                                                 self.templat = BoltLatitude!
                                                                                                                                                 self.templong = BoltLontitude!
                                                                                                                                                 let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                                                                 self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                                 self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                                 if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                                                                     
                                                                                                                                                     self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                                     self.BoltBikeLat = BoltLatitude!
                                                                                                                                                     self.BoltBikeLon = BoltLontitude!
                                                                                                                                                     self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                                     self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                                     self.BoltSourceDistance = self.BolttempDistance
                                                                                                                                                     nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                                     nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                                     // print(self.BirdtempDistance)
                                                                                                                                                     nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                                                                     BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                                     
                                                                                                                                                 }
                                                                                                                                                 else{
                                                                                                                                                     BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                                 }
                                                                                                                                             }
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                         }
                                                                                                                                         
                                                                                                                                         nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                                                         
                                                                                                                                         self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                         self.hideLoader()
                                                                                                                                         //self.hideLoader()
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                     }
                                                                                                                                 }
                                                                                                                                 else{
                                                                                                                                     self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                     self.hideLoader()
                                                                                                                                     
                                                                                                                                 }
                                                                                                                                 
                                                                                                                             case .failure(let error):
                                                                                                                                 print(error)
                                                                                                                                 self.hideLoader()
                                                                                                                                 self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                 
                                                                                                                             }
                                                                                                                         }
                                                                                          
                                                                                      }
                                                else
                                                                                                                         {
                                                                                                                            self.hideLoader()
                                                                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                }
                                                
                                                
                                            }
                                                
                                            }

                                            
                                        case .failure(let error):
                                            print(error)
                                            nextViewController.BirdDatalistapicount = 0
                                            nextViewController.BikedistanceInMeters = 1000
                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                            self.hideLoader()
                                            
                                        }
                                    }
                                
                                }
                                else
                                    {
                                           var LimeCheckBikeCount = 0
                                        // Lime and bold
                                        if LimeOn
                                                                                     { Alamofire.request("https://data.lime.bike/api/partners/v1/gbfs/louisville/free_bike_status").responseObject { (response: DataResponse<getLimestation>) in
                                                                                           switch response.result {
                                                                                           case .success:
                                                                                               self.secondServiceCallComplete = true
                                                                                               if response.result.value?.code != 400 {
                                                                                                   if (response.result.value?.total)! >= 0 {
                                                                                                       let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                       
                                                                                                       let responseResutl = response.result.value
                                                                                                       print("\(responseResutl?.datalistfromapi?.count)")
                                                                                                       self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                       nextViewController.LimeDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                       
                                                                                                       self.LimeListApi = responseResutl?.datalistfromapi
                                                                                                       nextViewController.LimeListApi = self.LimeListApi
                                                                          
                                                                                                       for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                           let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                           let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                           let LimeLatitude = Double("\(BirdBikeLat)")
                                                                                                           let LimeLontitude = Double("\(BirdBikeLng)")
                                                                                                           
                                                                                                           let ss = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                                                           let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                           //  print(dd)
                                                                                                           // print((dd*100).rounded()/100)
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                           if LimeCheckBikeCount == 0 {
                                                                                                               
                                                                               if LimeBike.bike_id != nil {  self.LimeBikeID = LimeBike.bike_id!
                                                                                                   }
                                                                                                               else
                                                                                                  {
                                                                                                   self.LimeBikeID = "0"
                                                                                                               }
                                                                                                               self.LimeBikeLat = LimeLatitude!
                                                                                                               self.LimeBikeLon = LimeLontitude!
                                                                                                               self.LimeIsReserved = LimeBike.bike_is_reserved!
                                                                                                               self.LimeIsDisabled = LimeBike.bike_is_disabled!
                                                                                                               let destinationlatlong = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                                                               self.LimedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                               self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                                                               LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                                                               
                                                                                               nextViewController.LimeBikeLat = LimeLatitude!
                                                                                               nextViewController.LimeBikeLon = LimeLontitude!
                                                                                                               
                                                                                                               
                                                                                                               //print(self.BirdSourceDistance)
                                                                                                               nextViewController.LimedistanceInMeters = self.BirdSourceDistance
                                                                                                               
                                                                                                               
                                                                                                               
                                                                                                           }
                                                                                                           else{
                                                                                                               self.templat = LimeLatitude!
                                                                                                               self.templong = LimeLontitude!
                                                                                                               let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                               self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                               self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                                                               if self.BirdtempDistance < self.BirdSourceDistance {
                                                                                                                   
                                                                                       if LimeBike.bike_id != nil
                                                                                                   {
                                                                                           self.BirdBikeID = LimeBike.bike_id!
                                                                                                   }
                                                                                                   else
                                                                                                   {
                                                                                           self.BirdBikeID = "0"
                                                                                               }
                                                                                       self.BirdBikeLat = LimeLatitude!
                                                                                                                   self.BirdBikeLon = LimeLontitude!
                                                                                                                   self.BirdIsReserved = LimeBike.bike_is_reserved!
                                                                                                                   self.BirdIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                   self.BirdSourceDistance = self.BirdtempDistance
                                                                                                                   nextViewController.LimeBikeLat = LimeLatitude!
                                                                                                                   nextViewController.LimeBikeLon = LimeLontitude!
                                                                                                                   // print(self.BirdtempDistance)
                                                                                                                   nextViewController.LimedistanceInMeters = self.BirdtempDistance
                                                                                                                   LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                                                                   
                                                                                                               }
                                                                                                               else{
                                                                                                                   LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                                                               }
                                                                                                           }
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                           
                                                                                                       }
                                                                                                       
                                                                                                       nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                       
                                                                                                       // Bolt Integration
                                                                                                       var BoltCheckBikeCount = 0
                                                                                                       
                                                                                                      if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                                           switch response.result {
                                                                                                           case .success:
                                                                                                               self.secondServiceCallComplete = true
                                                                                                               if response.result.value?.code != 400 && response.result.value?.code != nil {
                                                                                                                   if (response.result.value?.total)! >= 0 {
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                       let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                                       
                                                                                                                       let responseResutl = response.result.value
                                                                                                                       print("\(responseResutl?.datalistfromapi?.count)")
                                                                                                                       SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                                                                                       print("*****Bolt")
                                                                                                                       self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                                       nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                                       self.boltListApi = responseResutl?.datalistfromapi
                                                                                                                       nextViewController.boltListApi = self.boltListApi
                                                                                                                       
                                                                                                                       for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                                           let BoltLatitude = Double("\(LimeBike.bike_lat!)")
                                                                                                                           let BoltLontitude = Double("\(LimeBike.bike_lon!)")
                                                                                                                           print("AARUN ==> LAT\(BoltLatitude) and Lan \(BoltLontitude)")
                                                                                                                           let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                           let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                                           //  print(dd)
                                                                                                                           // print((dd*100).rounded()/100)
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                           if BoltCheckBikeCount == 0 {
                                                                                                                               
                                                                                                                               self.BoltBikeID = LimeBike.bike_id!
                                                                                                                               self.BoltBikeLat = BoltLatitude!
                                                                                                                               self.BoltBikeLon = BoltLontitude!
                                                                                                                               self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                               self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                               let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                               self.BoltdistanceInMeters = 0.0
                                                                                                                               let distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong) as Double
                                                                                                                               self.BoltdistanceInMeters = distanceInMeters
                                                                                                                               self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                               print("AARUN ==> Source \(self.BoltSourceDistance)")
                                                                                                                               BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                               
                                                                                                                               print("Lime Bike \(self.LimeBikeLat) and \(self.LimeBikeLon)")
                                                                                                                               nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                               nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                               
                                                                                                                               
                                                                                                                               //print(self.BirdSourceDistance)
                                                                                                                               nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                                               
                                                                                                                               
                                                                                                                               
                                                                                                                           }
                                                                                                                           else{
                                                                                                                               self.templat = BoltLatitude!
                                                                                                                               self.templong = BoltLontitude!
                                                                                                                               let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                                               self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                               self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                               print("AARUN ==> Distance \(self.BolttempDistance)")
                                                                                                                               //BolttempDistance
                                                                                                                               if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                                                   
                                                                                                                                   self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                   self.BoltBikeLat = BoltLatitude!
                                                                                                                                   self.BoltBikeLon = BoltLontitude!
                                                                                                                                   self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                   self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                   self.BoltSourceDistance = self.BolttempDistance
                                                                                                                                   print("AARUN ==> Distance \(self.BoltSourceDistance)")
                                                                                                                                   nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                   nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                   // print(self.BirdtempDistance)
                                                                                                                                   nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                                                   BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                   
                                                                                                                               }
                                                                                                                               else{
                                                                                                                                   BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                               }
                                                                                                                           }
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                       }
                                                                                                                       
                                                                                                                       nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                                       
                                                                                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                       
                                                                                                                       self.hideLoader()
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                   }
                                                                                                               }
                                                                                                               else{
                                                                                                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                   self.hideLoader()
                                                                                                                   
                                                                                                               }
                                                                                                               
                                                                                                           case .failure(let error):
                                                                                                               print(error)
                                                                                                               self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                               self.hideLoader()
                                                                                                               
                                                                                                           }
                                                                                                       }
                                                                                                   }
                                                                                                   else
                                                                                                      {
                                                                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                       self.hideLoader()
                                                                                                       }
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                   }
                                                                                               }
                                                                                               else{
                                                                                                   // Bolt Integration
                                                                                                   var BoltCheckBikeCount = 0
                                                                                                   
                                                                                                   if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                                       switch response.result {
                                                                                                       case .success:
                                                                                                           self.secondServiceCallComplete = true
                                                                                                           if response.result.value?.code != 400 &&  response.result.value?.code != nil {
                                                                                                               if (response.result.value?.total)! >= 0 {
                                                                                                                   let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                                   let responseResutl = response.result.value
                                                                                                                   self.boltListApi = responseResutl?.datalistfromapi
                                                                                                                   nextViewController.boltListApi = self.boltListApi
                                                                                                                   self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                                   nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                                   for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                                       let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                                       let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                                       let BoltLatitude = Double("\(LimeBike.bike_lat!)")
                                                                                                                       let BoltLontitude = Double("\(LimeBike.bike_lon!)")
                                                                                                                       
                                                                                                                       let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                       let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                                       //  print(dd)
                                                                                                                       // print((dd*100).rounded()/100)
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                       if BoltCheckBikeCount == 0 {
                                                                                                                           
                                                                                                                           self.BoltBikeID = LimeBike.bike_id!
                                                                                                                           self.BoltBikeLat = BoltLatitude!
                                                                                                                           self.BoltBikeLon = BoltLontitude!
                                                                                                                           self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                           self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                           let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                           self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                           self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                           BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                           
                                                                                                                           nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                           nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                           
                                                                                                                           
                                                                                                                           //print(self.BirdSourceDistance)
                                                                                                                           nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                       }
                                                                                                                       else{
                                                                                                                           self.templat = BoltLatitude!
                                                                                                                           self.templong = BoltLontitude!
                                                                                                                           let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                                           self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                           self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                           if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                                               
                                                                                                                               self.BoltBikeID = LimeBike.bike_id!
                                                                                                                               self.BoltBikeLat = BoltLatitude!
                                                                                                                               self.BoltBikeLon = BoltLontitude!
                                                                                                                               self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                               self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                               self.BoltSourceDistance = self.BolttempDistance
                                                                                                                               nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                               nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                               // print(self.BirdtempDistance)
                                                                                                                               nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                                               BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                               
                                                                                                                           }
                                                                                                                           else{
                                                                                                                               BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                           }
                                                                                                                       }
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                       
                                                                                                                   }
                                                                                                                   
                                                                                                                   nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                                   
                                                                                                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                   
                                                                                                                   self.hideLoader()
                                                                                                                   
                                                                                                                   
                                                                                                                   
                                                                                                                   
                                                                                                                   
                                                                                                               }
                                                                                                           }
                                                                                                           else{
                                                                                                               self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                               self.hideLoader()
                                                                                                               
                                                                                                           }
                                                                                                           
                                                                                                       case .failure(let error):
                                                                                                           print(error)
                                                                                                           
                                                                                                           self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                           self.hideLoader()
                                                                                                           
                                                                                                       }
                                                                                                       }
                                                                                                   }
                                                                                                   else
                                                                                                   {
                                                                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                       self.hideLoader()
                                                                                                   }
                                                                                               }
                                                                                               
                                                                                           case .failure(let error):
                                                                                               print(error)
                                                                                               
                                                                                               
                                                                                                           var BoltCheckBikeCount = 0
                                                                                                           
                                                                                                          if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                                               switch response.result {
                                                                                                               case .success:
                                                                                                                   self.secondServiceCallComplete = true
                                                                                                                   if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                                                       if (response.result.value?.total)! >= 0 {
                                                                                                                            let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                                           let responseResutl = response.result.value
                                                                                                   self.boltListApi = responseResutl?.datalistfromapi
                                                                                               nextViewController.boltListApi = self.boltListApi
                                                                                                                           self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                                           nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                                           for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                                               let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                                               let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                                               let BoltLatitude = Double("\(LimeBike.bike_lat!)")
                                                                                                                               let BoltLontitude = Double("\(LimeBike.bike_lon!)")
                                                                                                                               
                                                                                                                               let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                               let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                                               //  print(dd)
                                                                                                                               // print((dd*100).rounded()/100)
                                                                                                                               
                                                                                                                               
                                                                                                                               
                                                                                                                               
                                                                                                                               if BoltCheckBikeCount == 0 {
                                                                                                                                   
                                                                                                                                   self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                   self.BoltBikeLat = BoltLatitude!
                                                                                                                                   self.BoltBikeLon = BoltLontitude!
                                                                                                                                   self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                   self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                   let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                                   self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                   self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                   BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                   
                                                                                                                                   nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                   nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                   
                                                                                                                                   
                                                                                                                                   //print(self.BirdSourceDistance)
                                                                                                                                   nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                                                   
                                                                                                                                   
                                                                                                                                   
                                                                                                                               }
                                                                                                                               else{
                                                                                                                                   self.templat = BoltLatitude!
                                                                                                                                   self.templong = BoltLontitude!
                                                                                                                                   let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                                                   self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                   self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                   if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                                                       
                                                                                                                                       self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                       self.BoltBikeLat = BoltLatitude!
                                                                                                                                       self.BoltBikeLon = BoltLontitude!
                                                                                                                                       self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                       self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                       self.BoltSourceDistance = self.BolttempDistance
                                                                                                                                       nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                       nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                       // print(self.BirdtempDistance)
                                                                                                                                       nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                                                       BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                       
                                                                                                                                   }
                                                                                                                                   else{
                                                                                                                                       BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                   }
                                                                                                                               }
                                                                                                                               
                                                                                                                               
                                                                                                                               
                                                                                                                               
                                                                                                                               
                                                                                                                           }
                                                                                                                           
                                                                                                                           nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                                           
                                                                                                                           self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                           
                                                                                                                           self.hideLoader()
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                           
                                                                                                                       }
                                                                                                                   }
                                                                                                                   else{
                                                                                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                       self.hideLoader()
                                                                                                                       
                                                                                                                   }
                                                                                                                   
                                                                                                               case .failure(let error):
                                                                                                                   print(error)
                                                                                                                   
                                                                                                                   self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                   self.hideLoader()
                                                                                                                   
                                                                                                               }
                                                                                                           }
                                                                                           }
                                                                                           else
                                                                                                          {
                                                                                                           self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                           self.hideLoader()
                                                                                               }
                                                                                               
                                                                                           }
                                                                                       }
                                                                                       
                                                                                   }
                                        else
                                          {
                                           var BoltCheckBikeCount = 0
                                                                                                                     if BoltOn {
                                                                                                                     Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                                                         switch response.result {
                                                                                                                         case .success:
                                                                                                                             self.secondServiceCallComplete = true
                                                                                                                             if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                                                                 if (response.result.value?.total)! >= 0 {
                                                                                                                                        let Sourcelatlatlong = CLLocation(latitude: Double(((weatherResponse?.geometry?.location!.lat)!)), longitude: Double(((weatherResponse?.geometry?.location!.lng)!)))
                                                                                                                                     
                                                                                                                                     let responseResutl = response.result.value
                                                                                                             self.boltListApi = responseResutl?.datalistfromapi
                                                                                                                                     SourceDestination.boltListApi = responseResutl?.datalistfromapi
                                                                                                         nextViewController.boltListApi = self.boltListApi
                                                                                      print("\(responseResutl?.datalistfromapi?.count)")
                                                                                                                                     self.BirdDatalistapicount = responseResutl?.datalistfromapi!.count
                                                                                                                                     nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                                                                     for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                                                         let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                                                         let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                                                         let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                                                                         let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                                                                         
                                                                                                                                         let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                                         let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                                                         //  print(dd)
                                                                                                                                         // print((dd*100).rounded()/100)
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         if BoltCheckBikeCount == 0 {
                                                                                                                                             
                                                                                                                                             self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                             self.BoltBikeLat = BoltLatitude!
                                                                                                                                             self.BoltBikeLon = BoltLontitude!
                                                                                                                                             self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                             self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                             let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                                                             self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                             self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                             BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                             
                                                                                                                                             nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                             nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             //print(self.BirdSourceDistance)
                                                                                                                                             nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                                                             
                                                                                                                                             
                                                                                                                                             
                                                                                                                                         }
                                                                                                                                         else{
                                                                                                                                             self.templat = BoltLatitude!
                                                                                                                                             self.templong = BoltLontitude!
                                                                                                                                             let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                                                             self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                                                             self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                                                             if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                                                                 
                                                                                                                                                 self.BoltBikeID = LimeBike.bike_id!
                                                                                                                                                 self.BoltBikeLat = BoltLatitude!
                                                                                                                                                 self.BoltBikeLon = BoltLontitude!
                                                                                                                                                 self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                                                                 self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                                                                 self.BoltSourceDistance = self.BolttempDistance
                                                                                                                                                 nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                                                                 nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                                                                 // print(self.BirdtempDistance)
                                                                                                                                                 nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                                                                 BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                                 
                                                                                                                                             }
                                                                                                                                             else{
                                                                                                                                                 BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                                                             }
                                                                                                                                         }
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                         
                                                                                                                                     }
                                                                                                                                     
                                                                                                                                     nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                                                                     
                                                                                                                                     self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                     self.hideLoader()
                                                                                                                                     //self.hideLoader()
                                                                                                                                     
                                                                                                                                     
                                                                                                                                     
                                                                                                                                     
                                                                                                                                     
                                                                                                                                 }
                                                                                                                             }
                                                                                                                             else{
                                                                                                                                 self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                 self.hideLoader()
                                                                                                                                 
                                                                                                                             }
                                                                                                                             
                                                                                                                         case .failure(let error):
                                                                                                                             print(error)
                                                                                                                             self.hideLoader()
                                                                                                                             self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                             
                                                                                                                         }
                                                                                                                     }
                                                                                      
                                                                                  }
                                            else
                                                                                                                     {
                                                                                                                        self.hideLoader()
                                                                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                            }
                                         //bolt
                                        }
                                        
                                        
                                        
                                    }
                                
                                
                                
                                }
                                else
                                {
                                    self.hideLoader()
                                    self.showErrorDialogBox(viewController: self)
                                }
                            case .failure(let error):
                                self.hideLoader()
                                self.showErrorDialogBox(viewController: self)
                                print(error)
                               
                            }
                    }
                    
                    }
                    else
                    {
                        self.hideLoader()
                        self.showErrorDialogBox(viewController: self)
                    }
                   
                case .failure(let error):
                    self.showErrorDialogBox(viewController: self)
                    print(error)
                   self.hideLoader()
                }
        }
        
    
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */


    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let ticketDatavalue = savedTripArray?.result.value?.trips?[indexPath.row]
            print("TRIP ID:\(ticketDatavalue?.savedTripID ?? 0)")
            
            let alertViewController = NYAlertViewController()
            // Set a title and message
            alertViewController.title = "Tickets"
            alertViewController.message = "Are you sure to delete this trip from your favourite?"
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            let activateAction = NYAlertAction(
                title: "Yes",
                style: .default,
                handler: { (action: NYAlertAction!) -> Void in
                    let userDefaults1 = UserDefaults.standard
                    let accessToken = userDefaults1.value(forKey: "accessToken")
                    let url = "\(APIUrl.ZIGTARCAPI)SaveTrips/Delete"
                    let parameters: Parameters = [
                        "Token" : accessToken!,
                        "TripId" : ticketDatavalue?.savedTripID ?? 0
                    ]
                    
                    
                    
                    Alamofire.request(url, method: .delete, parameters: parameters, encoding: URLEncoding.default)
                        .responseObject{ (response: DataResponse<Postpayment>) in
                            switch response.result {
                            case .success:
                            print(response)
                            if response.result.value?.message == "OK"
                            {
                                self.viewDidLoad()
                            }
                            case .failure(let error):
                                print(error)
                                self.hideLoader()
                                self.showErrorDialogBox(viewController: self)
                            }
                    }
                    self.dismiss(animated: true, completion: nil)
            }
            )
            let cancelAction = NYAlertAction(
                title: "No",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    self.dismiss(animated: true, completion: nil)
                }
            )
            alertViewController.addAction(activateAction)
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
            
            
            
            // handle delete (by removing the data from your array and updating the tableview)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
 

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func showErrorDialogBox(viewController : UIViewController){
        let alertViewController = NYAlertViewController()
        alertViewController.title = "Error"
        alertViewController.message = "Server Error! Please try again later!"
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = viewController.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        // Add alert actions
        
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                
                viewController.dismiss(animated: true, completion: nil)
        }
        )
        
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        viewController.present(alertViewController, animated: true, completion: nil)
        
    }

    func showAlertBox(text:String){
       
        let alertViewController = NYAlertViewController()
        // Set a title and message
        alertViewController.title = "Details Missing"
        alertViewController.message = "\(text)"
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = self.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                // self.sourceTextField.becomeFirstResponder()
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
                self.dismiss(animated: true, completion: nil)
        }
        )
        //        alertViewController.addAction(activateAction)
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!
            
             let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
        
    }
    @objc func closePreferenceAction(sender: UIButton!)
    {
         self.navigationController?.setNavigationBarHidden(false, animated: true)
               UIView.animate(withDuration: 1,
                              delay: 0.1,
                              options: UIViewAnimationOptions.curveEaseInOut,
                              animations: { () -> Void in
                               
                               let userDefaults1 = UserDefaults.standard
                               let accessToken = userDefaults1.value(forKey: "accessToken")
                               //self.showLoader()
                               userDefaults1.set(true, forKey: "isBus")
                               var cabBool = NSString()
                               cabBool = "true"
                               if self.uberBool == false && self.isLyft == false
                               {
                                   cabBool = "false"
                               }
                               let url = "\(APIUrl.ZIGTARCAPI)Preferences/send"
                               let parameters: Parameters = [
                                   "TokenId": accessToken!,
                                   "bus": "true",
                                   "bike": "true",
                                   "train": "true",
                                   "cab": "true",
                                   "uber": "true",
                                   "lyft": "true",
                                   "lime": "false",
                                   "bolt": "false",
                                   "bikeshare": "\(self.bikeFirstSelected)",
                                   "Cabshare": "\(self.carFirstSelected)",
                                   "Message":""
                               ]
                               print("\(parameters)")
                               print("Aruncheck ==> \(self.uberBool) \(self.isLyft)")
                               Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                                   .responseObject{ (response: DataResponse<TokenSend>) in
                                       switch response.result {
                                       case .success:
                                           self.hideLoader()
                                           // print(response.result.value?.message ?? "nil")
                                           userDefaults1.set("true", forKey: "isBus")
                                           //userDefaults1.set(self.bikebuscheckbox.isOn, forKey: "isBike")
                                           userDefaults1.set("true", forKey: "isBike")
                                           userDefaults1.set("true", forKey: "isTrain")
                                           userDefaults1.set(cabBool, forKey: "isCab")
                                           userDefaults1.set(self.uberBool, forKey: "isUber")
                                           userDefaults1.set(self.isLyft, forKey: "isLyft")
                                           userDefaults1.set(self.carFirstSelected, forKey: "Cabshare")
                                                                         userDefaults1.set(self.bikeFirstSelected, forKey: "Bikeshare")
                                           userDefaults1.set(self.carSelected, forKey: "Cab")
                                           userDefaults1.set(self.bikeSelected, forKey: "Bike")
                                           
                                           userDefaults1.synchronize()
                                       case .failure(let error):
                                           self.hideLoader()

                                           print(error.localizedDescription)
                                       }
                                       
                               }
                               
                               
                               
               }, completion: { (finished) -> Void in
                   
               })
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        let uberSelect = notification.object as! NSMutableArray
        let uberDict = uberSelect.object(at: 0) as! NSDictionary
        self.uberBool = uberDict.value(forKey: "uber") as! Bool
        self.isLyft = uberDict.value(forKey: "lyft") as! Bool
        self.carSelected = uberDict.value(forKey: "Cab") as! Bool
        self.bikeSelected = uberDict.value(forKey: "Bike") as! Bool
        self.carFirstSelected = uberDict.value(forKey: "Cabshare") as! Bool
        self.bikeFirstSelected = uberDict.value(forKey: "Bikeshare") as! Bool
        closePreferenceAction(sender: nil)
        
    }

    
}
extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}

