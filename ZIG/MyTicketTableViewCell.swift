//
//  MyTicketTableViewCell.swift
//  ZIG
//
//  Created by Isaac on 18/01/18.
//  Copyright © 2018 Isaac. All rights reserved.
//

import UIKit

class MyTicketTableViewCell: UITableViewCell {

    var tripLable = UILabel()
    let fromLable = UILabel()
    let toLable = UILabel()
    let fromTextLable = UILabel()
    let toTextLable = UILabel()
    let Address = UILabel()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        tripLable.text = nil
        fromLable.text = nil
        toLable.text = nil
        fromTextLable.text = nil
        toTextLable.text = nil
        Address.text = ""
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
