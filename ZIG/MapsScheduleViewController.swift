//
//  MapsScheduleViewController.swift
//  ZIG
//
//  Created by Isaac on 31/01/18.
//  Copyright © 2018 Isaac. All rights reserved.
//


import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import GoogleMaps
import CoreLocation
import ExpandableCell
import SafariServices
import MapKit
import NVActivityIndicatorView
import NYAlertViewController
import SCLAlertView
import XLActionController
struct RealTimeSchedule {
    static var SelectedSchedule = ""
    static var SelectedScheduleRoute = ""
    static var SelectedScheduleRouteColor = ""
    
}

class MapsScheduleViewController: UIViewController,GMSMapViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
            let assistiveTouchSOS = AssistiveTouch()
     //let googlereponseDirectionList : NSMutableDictionary = [:]
    var SchudleArrayList = [[String:Any]]()
    var SchudleArrayValue : NSMutableDictionary = [:]
   
    @IBOutlet weak var mapBackgroundView: GMSMapView!
    var scheduleArray : NSMutableArray = []
    var dummyScheduleArray : NSMutableArray = []
    var realTimeMapTimer: Timer!
    let selectProfileBackView = UIView()

    let locationManager = CLLocationManager()
    var menuBackView = UIView()
    var paymentBackView = UIView()
    let selectedProfileArray : NSMutableArray = []
    var routeIDString = ""
    var rasterSize: CGFloat = 11.0
    var isSorta = true
    var startStop = String()
    var stopStop = String()
       var selectProfileTableView = UITableView()
     var totalFareLabel = UILabel()
    var profileDataList:DataResponse<GetProfile>!
    var viewConstraints: [NSLayoutConstraint]?
  
    var vehicleMarkerArray : NSMutableArray = []
    var menuTabelView :ExpandableTableView = ExpandableTableView()
    var busBtn = UIButton()
    var brtBtn = UIButton()
    var railBtn = UIButton()
    var trolleyBtn = UIButton()
    var btnFlag = 0
    var backgroundBlurView = UIView()
    var backgroundBlurView1 = UIView()
    var loaderView = UIView()
    var animationPolyline = GMSPolyline()
    var animationPath = GMSMutablePath()
    var stepArray : NSMutableArray = []
    var submitBtn =  UIButton()
    var i: UInt = 0
    var animateTimer: Timer!
    var animatePath = GMSPath()
    let addedProfileID : NSMutableArray = []
    var selectProfileLbl = UILabel()
 
    var selectProfileDropDown = UILabel()
    //var selectProfileTableView = UITableView()
    var buyNowbtn = UIButton()
      var selectProfileArray : NSMutableArray = []
    var cellEx: UITableViewCell {
        return (menuTabelView.dequeueReusableCell(withIdentifier: ExpandedCell.ID)!)
    }
    let cellReuseIdentifier = "cell"
    var routeDetails : NSMutableDictionary = [:]
    var routeIDArray : NSMutableArray = []
    var isMenuActive = false
    var busNameStrin = String()
    
    override func viewWillAppear(_ animated: Bool){
        
       
        
    }


    
   
    override func viewDidDisappear(_ animated: Bool) {
        
      
         self.OnMenuClicked()
//        mapBackgroundView = nil
//        
//        if realTimeMapTimer != nil
//        {
//            realTimeMapTimer.invalidate()
//        }
//        if animateTimer != nil
//        {
//            animateTimer.invalidate()
//        }
    }
    
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!
            
             let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
        
    }
 
    override func viewWillDisappear(_ animated: Bool) {
        self.hideLoader()
        NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }
    

    override func viewDidLoad() {
      
        
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
       
        analytics.GetPageHitCount(PageName: "Maps & Schedules")
       
        let camera = GMSCameraPosition.camera(withLatitude:38.2526647 , longitude:-85.75845570000001 , zoom: 11.0)
        mapBackgroundView.isMyLocationEnabled = true
        mapBackgroundView?.animate(to: camera)
        busBtn.isHidden = true
        railBtn.isHidden = true
        brtBtn.isHidden = true
        trolleyBtn.isHidden = true
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.title = " "
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
       backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.setHidesBackButton(true, animated:true);
        let menuButton = UIBarButtonItem(image: UIImage(named: "mapsSchdMenu"),
                                         style: UIBarButtonItemStyle.plain ,
                                         target: self, action: #selector(OnMenuClicked))
        self.navigationItem.rightBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
         self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.title = "Real-Time Schedules"
    
        
        
        
        //locationManager.delegate = self
      //  locationManager.requestWhenInUseAuthorization()
      //  locationManager.startUpdatingLocation()
        // GOOGLE MAPS SDK: COMPASS
        mapBackgroundView.settings.compassButton = true
        mapBackgroundView.delegate = self
        // GOOGLE MAPS SDK: USER'S LOCATION
        
        do {
            if let styleURL = Bundle.main.url(forResource: "custom", withExtension: "json") {
                mapBackgroundView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        menuBackView.frame = CGRect.init(x:2000, y: 0, width: (self.view.frame.size.width/2)+90, height: self.view.frame.size.height)
        menuBackView.backgroundColor = UIColor.white
        menuBackView.layer.borderWidth = 2
        menuBackView.layer.borderColor = UIColor.init(red:225/255.0, green:225/255.0, blue:225/255.0, alpha: 1.0).cgColor
        self.view.addSubview(menuBackView)
      
        
        paymentBackView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.width-80, height: self.view.frame.height-120)
        paymentBackView.backgroundColor = UIColor.white
        self.view.addSubview(paymentBackView)
        paymentBackView.layer.borderWidth = 2
        paymentBackView.layer.borderColor = UIColor.init(red:225/255.0, green:225/255.0, blue:225/255.0, alpha: 1.0).cgColor
        
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
         self.OnMenuClicked()
        }
        // Do any additional setup after loading the view.
        
        let assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

        assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
        assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
        view.addSubview(assistiveTouch)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
        tapGesture.numberOfTapsRequired = 1
        assistiveTouchSOS.addGestureRecognizer(tapGesture)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
        longPress.minimumPressDuration = 1.0
        self.assistiveTouchSOS.addGestureRecognizer(longPress)
        assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
        assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
      //  view.addSubview(assistiveTouchSOS)


        
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
           if gesture.state == UIGestureRecognizerState.began {
               UIDevice.vibrate()
               print("Long Press")
               currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                          if Success{
                         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                         let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                           print("\(Address),\(LatsosLat),\(Longsoslong)")
                           MapsScheduleVC.CurrentlocationAddressString = Address
                           MapsScheduleVC.latSOS = LatsosLat
                           MapsScheduleVC.longSOS = Longsoslong
                         self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                  //self.CurrentlocationAddress.text = Address
                              }
                          }
               
           }
       }
     
       @objc func tappedSOS(sender: UIButton) {
        
           let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                   // action here
           }
                   
           SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
          
       }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    @objc func tapped(sender: UIButton) {
        print("\(sender) has been touched")
        
        
        let actionController = TwitterActionController()
        
        actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        if comVar.DateImplementation {
            
            actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
//                self.navigationController?.pushViewController(nextViewController, animated: true)
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                           
                           
            }))
            
        }
        
        
        
        actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
            
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MapsScheduleViewController") as! MapsScheduleViewController
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        actionController.headerData = "Quick View"
        present(actionController, animated: true, completion: nil)
        
        
        
        
    }
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//
//
//        let camera = GMSCameraPosition.camera(withLatitude:38.2526647 , longitude:-85.75845570000001 , zoom: 11.0)
//        mapBackgroundView.isMyLocationEnabled = true
//        mapBackgroundView?.animate(to: camera)
//
//        //Finally stop updating location otherwise it will come again and again in this delegate
//        self.locationManager.stopUpdatingLocation()
//    }
    @objc func OnMenuClicked(){
        
        
        
        menuTabelView.expansionStyle = .single
        if !isMenuActive {
            self.showLoader()
            DispatchQueue.main.async {
                self.menuTabelView.removeFromSuperview()
            }
         isMenuActive = true
        UIView.animate(withDuration: 1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        self.menuBackView.frame = CGRect.init(x: (self.view.frame.size.width/2) - 90, y: 0, width: (self.view.frame.size.width/2) + 90, height: self.view.frame.size.height)
        }, completion: { (finished) -> Void in
            var paymentURL1 : URL
            if self.btnFlag == 0
            {
                paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=rail")!
            }
           
            else
            {
                paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=bus")!
            }
            var request1 = URLRequest(url: paymentURL1)
            
            request1.httpMethod = "GET"
            
            URLSession.shared.dataTask(with: request1) { (data, response, error) -> Void in
                
                do
                {
                    if data != nil
                    {
                        self.dummyScheduleArray.removeAllObjects()
                       self.scheduleArray.removeAllObjects()

                        self.scheduleArray = try (JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray)!
                        
                        
                        
                        self.dummyScheduleArray.addObjects(from: self.scheduleArray as! [Any])
                        DispatchQueue.main.async {
                        for scheduleValue in self.scheduleArray
                        {
                            let scheduleData : NSMutableDictionary = scheduleValue as! NSMutableDictionary
                            
                            
                            
  //                          let mapObjects = MapsandScheduleRealmModel()
//                            mapObjects.LineName = scheduleData.value(forKey:"LineName")! as! String
//                            mapObjects.RouteID = scheduleData.value(forKey:"RouteID")! as! String
//                            mapObjects.RouteName = scheduleData.value(forKey:"RouteName")! as! String
//                            try! self.uiRealm.write {
//                                self.uiRealm.add(mapObjects, update: true)
//                            }
                        }
                        }
                        DispatchQueue.main.async {
                            
                            
                           
                            //self.aroundMeSearchBar.isHidden = false
                            self.railBtn.frame = CGRect.init(x: 20, y: 70, width: (self.menuBackView.frame.width/4)-15, height: 40)
                            
                           // self.railBtn.setImage(UIImage(named:"cota_clicked"), for: UIControlState.normal)
                            self.railBtn.setTitle("RAIL", for: .normal)
                            self.railBtn.setTitleColor(UIColor.white, for: .normal)
                            if self.btnFlag == 0
                            {
                                self.railBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
                            }
                            else
                            {
                                self.railBtn.setBackgroundColor(UIColor.init(red: 221.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0), for: .normal)
                            }
                            self.railBtn.tag = 0
                            self.railBtn.titleLabel?.font = UIFont.setTarcRegular(size: 13.0)
                            self.railBtn.addTarget(self, action:#selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
                            self.menuBackView.addSubview(self.railBtn)
                            
                            
                            self.busBtn.frame = CGRect.init(x: self.railBtn.frame.size.width+self.railBtn.frame.origin.x+10, y: 70, width: (self.menuBackView.frame.width/4)-15, height: 40)
                            self.busBtn.titleLabel?.font = UIFont.setTarcRegular(size: 13.0)
                            self.busBtn.setTitle("BUS", for: .normal)
                            self.busBtn.setTitleColor(UIColor.white, for: .normal)
                            if self.btnFlag == 1
                            {
                                self.busBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
                            }
                            else
                            {
                                self.busBtn.setBackgroundColor(UIColor.init(red: 221.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0), for: .normal)
                            }
                            self.busBtn.tag = 1
                            self.busBtn.addTarget(self, action:#selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
                            
                            self.menuBackView.addSubview(self.busBtn)
                            
                            self.brtBtn.frame = CGRect.init(x: self.busBtn.frame.size.width+self.busBtn.frame.origin.x+10, y: 70, width: (self.menuBackView.frame.width/4)-15, height: 40)
                           self.brtBtn.titleLabel?.font = UIFont.setTarcRegular(size: 13.0)
                            
                            self.brtBtn.setTitle("BRT", for: .normal)
                            self.brtBtn.setTitleColor(UIColor.white, for: .normal)
                            if self.btnFlag == 2
                            {
                                self.brtBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
                            }
                            else
                            {
                                self.brtBtn.setBackgroundColor(UIColor.init(red: 221.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0), for: .normal)
                            }
                            self.brtBtn.tag = 2
                           
                            self.brtBtn.addTarget(self, action:#selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
                            
                            self.menuBackView.addSubview(self.brtBtn)
                            
                           self.trolleyBtn.frame = CGRect.init(x: self.brtBtn.frame.size.width+self.brtBtn.frame.origin.x+10, y: 70, width: (self.menuBackView.frame.width/4)-15, height: 40)
                            self.trolleyBtn.titleLabel?.font = UIFont.setTarcRegular(size: 12.0)
                    
                            self.trolleyBtn.setTitle("TROLLEY", for: .normal)
                            self.trolleyBtn.setTitleColor(UIColor.white, for: .normal)
                            if self.btnFlag == 3
                            {
                                self.trolleyBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
                            }
                            else
                            {
                            self.trolleyBtn.setBackgroundColor(UIColor.init(red: 221.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0), for: .normal)
                            }
                            self.trolleyBtn.tag = 3
                           
                            self.trolleyBtn.addTarget(self, action:#selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
                            
                            self.menuBackView.addSubview(self.trolleyBtn)
                            
                            
                            self.menuTabelView = ExpandableTableView()
                            self.menuTabelView.expandableDelegate = self
                           self.menuTabelView.expansionStyle = .single
                            self.menuTabelView.animation = .automatic
                            self.menuTabelView.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: NormalCell.ID)
                            self.menuTabelView.register(UINib(nibName: "ExpandedCell", bundle: nil), forCellReuseIdentifier: ExpandedCell.ID)
                            self.menuTabelView.register(UINib(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: ExpandableCell2.ID)

                            self.menuTabelView.frame = CGRect.init(x: 0, y: 0, width: self.menuBackView.frame.width, height: self.menuBackView.frame.height)
                            self.menuTabelView.allowsMultipleSelection = true
                            self.menuTabelView.backgroundColor = UIColor.white
                            self.menuBackView.addSubview(self.menuTabelView)
                            
                            self.hideLoader()
                        }
                    }
                }catch{
                    
                }
                
                }.resume();
            
           
            
            
        })
        }
        else
        {
            isMenuActive = false
            UIView.animate(withDuration: 2,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            DispatchQueue.main.async {
                               // self.aroundMeSearchBar.resignFirstResponder()
                               // self.aroundMeSearchBar.isHidden = true
                            }
                            
                           // self.aroundMeSearchBar.removeFromSuperview()
                            self.menuBackView.frame = CGRect.init(x: 2000, y: 0, width: (self.view.frame.size.width/2)+90, height: self.view.frame.size.height)
            }, completion: { (finished) -> Void in
                
                
            })
        }
    }
    @objc func sortaBtnAction(sender: UIButton!){
        isSorta = true
        var paymentURL1 : URL
        print(sender.tag)
        self.btnFlag = sender.tag
        if self.btnFlag == 0
        {
            self.railBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0/255.0, blue: 198.0/255.0, alpha: 1.0), for: .normal)
             self.busBtn.setBackgroundColor(UIColor.init(red: 221.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0), for: .normal)
             self.brtBtn.setBackgroundColor(UIColor.init(red: 221.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0), for: .normal)
             self.trolleyBtn.setBackgroundColor(UIColor.init(red: 221.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0), for: .normal)
            paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=rail")!
        }
        
        else
        {
            paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=bus")!
        }
        
        var request1 = URLRequest(url: paymentURL1)
//        self.sortaBtn.setImage(UIImage(named:"cota_clicked"), for: UIControlState.normal)
//        self.tankBtn.setImage(UIImage(named:"delaware_unclicked"), for: UIControlState.normal)
        let camera = GMSCameraPosition.camera(withLatitude:38.2526647 , longitude:-85.75845570000001 , zoom: 11.0)
        mapBackgroundView.isMyLocationEnabled = true
        mapBackgroundView?.animate(to: camera)
        request1.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request1) { (data, response, error) -> Void in
            
            do
            {
                if data != nil
                {
                    self.dummyScheduleArray.removeAllObjects()
                    self.scheduleArray.removeAllObjects()
                    
                    self.scheduleArray = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray)!
                    
                    self.dummyScheduleArray.addObjects(from: self.scheduleArray as! [Any])
                    DispatchQueue.main.async {
                        self.menuTabelView.reloadData()
                    }
                    
                }
            }catch{
                
            }
            
            }.resume();
        
    }
    
    
    
    func plotRouteDetails() {
       print(routeDetails)
       print(routeIDArray.count)
        mapBackgroundView.clear()
        if routeIDArray.count == 0 {
            routeDetails.removeAllObjects()
        }
        else{
            for routeID in routeIDArray
            {
                let arrayOfKeys : NSArray = routeDetails.allKeys as NSArray
                if (arrayOfKeys.contains(routeID))
                {
                    
                
                    
            }
                
                
        }
        }
    }
   
    
    func imageViewWithIcon(_ icon: UIImage, rasterSize: CGFloat) -> UIImageView {
        let imgView = UIImageView(image: icon)
        imgView.frame = CGRect(x: 0, y: 0, width: icon.size.width + rasterSize * 2.0, height: icon.size.height)
        imgView.contentMode = .center
        imgView.tintColor = UIColor(red: 0.75, green: 0, blue: 0, alpha: 1)
        return imgView
    }
   

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60;
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) ->Int{
        
        
        print(stepArray.count)
        return stepArray.count+2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if tableView == selectProfileTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
            tableView.separatorColor = UIColor.clear;
            cell.separatorInset = .zero
            cell.textLabel?.text = selectProfileArray.object(at: indexPath.row) as? String
            
            
            return cell
//        }
//        return
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if(tableView == selectProfileTableView){
            selectedProfileArray.remove(selectProfileArray.object(at: indexPath.row))
        }
        
        print(selectedProfileArray)
    }
    
    
    @objc func OnBackClicked() {
        self.navigationController?.popViewController(animated: true)
        self.mapBackgroundView = nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//

    func moveCameraToLocation(latitude:Double,longitude:Double){
        GMSCameraPosition.camera(withLatitude:latitude , longitude:longitude , zoom: 11.0)
    }
    @objc func openSchedules(sender: UIButton!) {
        
        
        let valueDic : NSMutableDictionary = self.scheduleArray[sender.tag%10000]  as! NSMutableDictionary
        
        RealTimeSchedule.SelectedSchedule = "\(valueDic.value(forKey: "RouteID") as! String)"
        
        RealTimeSchedule.SelectedScheduleRoute = "\(valueDic.value(forKey: "RouteID") as! String) - \(valueDic.value(forKey: "LineName") as! String)"
        
        
        let homeView = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        present(homeView, animated: true, completion: nil)
        
        
    }
    @objc func openPassportAppLink(sender: UIButton!) {
        
        let valueDic : NSMutableDictionary = self.scheduleArray[sender.tag%1000] as! NSMutableDictionary
        
        routeIDArray.add((valueDic.value(forKey: "RouteID") as! String))
        
        var routeIDString = valueDic.value(forKey: "RouteID") as! String
        print(routeIDString)
        routeIDString = routeIDString.replacingOccurrences(of: " ", with: "%20")
        
        
        
        //        guard let url = URL(string: "") else { return }
        //        UIApplication.shared.open(url)
        
        if let url = URL(string: "\(APIUrl.ZIGTARCBASEAPI)img/pdf/\(routeIDString).pdf") {
            let vc: SFSafariViewController
            
            if #available(iOS 11.0, *) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = false
                vc = SFSafariViewController(url: url, configuration: config)
            } else {
                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
            }
            vc.delegate = self as? SFSafariViewControllerDelegate
            present(vc, animated: true)
        }
        
        
    }
    
    func GetRealTimeSchedule(RouteID:String){
        
        
        
        
    }
}

extension MapsScheduleViewController: ExpandableDelegate {
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
       
        let cell1 = menuTabelView.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell
       
        
        let addToArray = UIButton()
        let lineLable = UILabel()
        let buyTicketBtn = UIButton()
        addToArray.frame = CGRect.init(x: 0, y: 0, width: (self.menuTabelView.frame.size.width/2)-1, height: 44)
        addToArray.tag = 1000+indexPath.row
        addToArray.setTitle("Map It", for: .normal)
        addToArray.setTitleColor(UIColor.white, for: .normal)
        addToArray.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        addToArray.addTarget(self, action: #selector(self.addTripToArrayAction), for: .touchUpInside)
        cell1.contentView.addSubview(addToArray)
        
        lineLable.frame = CGRect.init(x:(self.menuTabelView.frame.size.width/2)-1, y: 0, width: 1, height: 44)
        lineLable.backgroundColor = UIColor.white
        cell1.contentView.addSubview(lineLable)
         buyTicketBtn.tag = 1000+indexPath.row
        buyTicketBtn.frame = CGRect.init(x: (self.menuTabelView.frame.size.width/2), y: 0, width: (self.menuTabelView.frame.size.width/2)-1, height: 44)
        buyTicketBtn.setTitle("Schedules", for: .normal)
        buyTicketBtn.addTarget(self, action: #selector(self.openSchedules), for: .touchUpInside)
        buyTicketBtn.tag = 10000+indexPath.row
        buyTicketBtn.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        buyTicketBtn.setTitleColor(UIColor.white, for: .normal)
        cell1.contentView.addSubview(buyTicketBtn)
        return [cell1]
    }
   
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == selectProfileTableView{
            print("Select profile table view")
            if !selectedProfileArray.contains(selectProfileArray.object(at: indexPath.row))
            {
                selectedProfileArray.add(selectProfileArray.object(at: indexPath.row))
            }
            else
            {
                
            }
            print(selectedProfileArray)
            
            
        }
    }
    
   

    
    

    
    
    
    
    
    
    
//    @objc func dissmissbuttonAction(sender: UIButton!) {
//        UIView.animate(withDuration: 1,
//                       delay: 0.1,
//                       options: UIViewAnimationOptions.curveEaseInOut,
//                       animations: { () -> Void in
//                        self.backgroundBlurView.removeFromSuperview()
//                        self.paymentBackView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.size.width-80, height: self.view.frame.size.height-120)
//                        self.paymentBackView = UIView();
//                        self.paymentBackView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.size.width-80, height: self.view.frame.size.height-120)
//                        self.paymentBackView.backgroundColor = UIColor.white
//                        self.view.addSubview(self.paymentBackView)
//                        self.paymentBackView.layer.borderWidth = 2
//                        self.paymentBackView.layer.borderColor = UIColor.init(red:225/255.0, green:225/255.0, blue:225/255.0, alpha: 1.0).cgColor
//        }, completion: { (finished) -> Void in
//        })
//    }
    @objc func addTripToArrayAction(sender: UIButton!)
    {
        print(sender.tag)
        self.showLoader()
        routeDetails.removeAllObjects()
        routeIDArray.removeAllObjects()
        //aroundMeSearchBar.resignFirstResponder()
        //self.aroundMeSearchBar.isHidden = true
       // aroundMeSearchBar.removeFromSuperview()
        let valueDic : NSMutableDictionary = self.scheduleArray[sender.tag%1000] as! NSMutableDictionary
        if mapBackgroundView != nil
        {
        mapBackgroundView.clear()
        }
       
        routeIDArray.add((valueDic.value(forKey: "RouteID") as! String))
        var paymentURL1 :URL
        var routeIDString = valueDic.value(forKey: "RouteID") as! String
        print(routeIDString)
        //GetRealTimeSchedule(RouteID: routeIDString)
        routeIDString = routeIDString.replacingOccurrences(of: " ", with: "%20")
        
            paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetTripListforBuslines?RouteID=\(routeIDString)")!
        
        var request1 = URLRequest(url: paymentURL1)
        
        request1.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request1) { (data, response, error) -> Void in
        
            do
            {
                if data != nil
                {

                    self.routeDetails.setObject((try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray) ?? "nil", forKey: valueDic.value(forKey: "RouteID") as! String as NSCopying)
                    //SchudleArrayList
                   // SchudleArrayValue
                    var valueSha : NSMutableDictionary = [:]
                     let pathArray : NSMutableArray = []
                   
                    
                    self.routeIDString = valueDic.value(forKey: "RouteID") as! String
                    for valueDetails in self.routeDetails.object(forKey: valueDic.value(forKey: "RouteID") as! String) as! NSArray
                        {
                        let valuePlot : NSMutableDictionary = valueDetails as! NSMutableDictionary
                        print(valuePlot.value(forKey: "TripHeadsign")!)
                            let TitleForSchudle = valuePlot.value(forKey: "TripHeadsign")
                            
                        valueSha = valuePlot.value(forKey: "TripDetails") as! NSMutableDictionary
                            let tripValueID : Int = valueSha.value(forKey: "TripId") as! Int
                            let RouteColor : String = valueSha.value(forKey: "RouteColor") as? String ?? "3D3D3D"
                            
                            self.SchudleArrayValue.setValue(routeIDString, forKey: "RouteID")
                            self.SchudleArrayValue.setValue(TitleForSchudle, forKey: "TripHeadsign")
                            self.SchudleArrayValue.setValue(RouteColor, forKey: "RouteColor")
                            self.SchudleArrayList.append(self.SchudleArrayValue as! [String : Any])
                            
                            let url2 : String
                           
                                url2 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetRealtimetripDetails"
                            
                            let parameters2: Parameters = [
                                "TripID" : tripValueID
                                ]
                            
                            let path = GMSMutablePath()
                            self.animatePath = GMSPath()
                            Alamofire.request(url2, method: .get, parameters: parameters2, encoding: URLEncoding.default)
                                .responseObject{ (response: DataResponse<MapsTripRootClass>) in
                                    switch response.result {
                                    case .success:
                                    print(response)
                                    if response.result.value?.shapes != nil
                                    {
                                    for shapesValue in (response.result.value?.shapes)!
                                    {
                                       
                                    
                                        let latit = Double(shapesValue.lat!)
                                        let longit = Double(shapesValue.longField!)
                                        path.addLatitude(latit!, longitude: longit!)
                                        
                                    }
                                    
                                    DispatchQueue.main.async {
                                        
                                        
                                        self.animatePath = path
                                        let polyline = GMSPolyline(path: path)
                                        polyline.map = self.mapBackgroundView
                                        polyline.strokeWidth = 4.0
                                        if response.result.value?.routeColor  != nil
                                        {
                                            let string = response.result.value?.routeColor
                                            let characters = Array(string!)
                                            if characters.count == 6 {
                                           // let color2 = UIColor(hexString: (response.result.value?.routeColor!)!)
                                                let color2 = color.hexStringToUIColor(hex: (response.result.value?.routeColor!)!)
                                                polyline.strokeColor = color2
                                            }else{
                                                polyline.strokeColor = UIColor.black
                                            }
                                        }
                                        else
                                        {
                                            polyline.strokeColor = UIColor.black
                                        }
                                        
//                                        self.animateTimer = Timer.scheduledTimer(timeInterval: 0.0003, target: self, selector: #selector(self.animatePolylinePath), userInfo: nil, repeats: true)
                                        
                                        
                                    }
                                    }
                                    case .failure(let error):
                                        print(error)
                                        self.showErrorDialogBox(viewController: self)
                            }
                        
                        
                       
                       
                            }
                           
                       
                        
                    }
                    
                    self.hideLoader()
                    
                        
                        
                    
                   
                }
                else
                {
                    self.hideLoader()
                }
            }catch{
                
                
            }
        
        }.resume();
        if isSorta
        {
            let routeIDString  = (valueDic.value(forKey: "RouteID") as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let paymentURL2 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeBusesforRouteID?RouteID=\(routeIDString!)")!
        var request2 = URLRequest(url: paymentURL2)
        
        request2.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request2) { (data, response, error) -> Void in
            
            do
            {
                if data != nil
                {
                    let jsonValue = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray
                    for jsonDetails in jsonValue!
                    {
                        let jsonDic : NSMutableDictionary = jsonDetails as! NSMutableDictionary
                      //  print(jsonDic)
                        if jsonDic.value(forKey: "lat") as! Double != 0 && jsonDic.value(forKey: "lng") as! Double != 0
                        {
                            DispatchQueue.main.async {
                                let vehMaker = GMSMarker()
                                vehMaker.position = CLLocationCoordinate2D(latitude:(jsonDic.value(forKey: "lat") as! Double), longitude: (jsonDic.value(forKey: "lng") as! Double))
                                let titleString = "\(jsonDic.value(forKey: "tripID")!)"
                                 vehMaker.title = "TripID: \(titleString)"
                                
                             
                                var nextstopmsg = "\n"
                                if let Nextstops : [NSDictionary] = jsonDic["Nextstops"] as? [NSDictionary]
                                {
                                    
                                    
                                    
                                        
                                        for (subject, NextstopsListvalue) in Nextstops[0]
                                        {
                                          //  print ("\(subject): \(NextstopsListvalue)")
                                            
                                            
                                            if "\(subject)" == "StopName" {
                                                
                                                //print(NextstopsListvalue)
                                                
                                                nextstopmsg = NextstopsListvalue as! String
                                            
                                        }
                                        
                                    }
                                }
                                vehMaker.snippet = "Next Stop: \((nextstopmsg))"
                                
                                
                                
                    
                                        vehMaker.icon = UIImage(named: "bus_Transit")
//                                var alertmssage = nextarray
//
//
                            
                                
                               
                                vehMaker.map = self.mapBackgroundView
                                self.vehicleMarkerArray.add(vehMaker)
                                
                            }
                        
                        
                        }
                    }
                   
                    
                }
            }catch{
                
            }
            
            }.resume();
        DispatchQueue.main.async {
            if self.realTimeMapTimer == nil
            {
                self.realTimeMapTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
            }
        }
        }
        else
        {
            let routeIDString  = (valueDic.value(forKey: "RouteID") as! String).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let paymentURL2 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeBusesforRouteID?RouteID=\(routeIDString ?? "")")!
            var request2 = URLRequest(url: paymentURL2)
            
            request2.httpMethod = "GET"
            
            URLSession.shared.dataTask(with: request2) { (data, response, error) -> Void in
                
                do
                {
                    if data != nil
                    {
                        let jsonValue = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray
                        for jsonDetails in jsonValue!
                        {
                            let jsonDic : NSMutableDictionary = jsonDetails as! NSMutableDictionary
                           // print(jsonDic)
                            if jsonDic.value(forKey: "lat") as! Double != 0 && jsonDic.value(forKey: "lng") as! Double != 0
                            {
                                DispatchQueue.main.async {
                                    let vehMaker = GMSMarker()
                                    vehMaker.position = CLLocationCoordinate2D(latitude:(jsonDic.value(forKey: "lat") as! Double), longitude: (jsonDic.value(forKey: "lng") as! Double))
                                    let titleString = "\(jsonDic.value(forKey: "tripID")!)"
                                    vehMaker.title = "TripID: \(titleString)"
                                    var nextstopmsg = "\n"
                                    if let Nextstops : [NSDictionary] = jsonDic["Nextstops"] as? [NSDictionary]
                                    {
                                        
                                        
                                        
                                        
                                        for (subject, NextstopsListvalue) in Nextstops[0]
                                        {
                                            //  print ("\(subject): \(NextstopsListvalue)")
                                            
                                            
                                            if "\(subject)" == "StopName" {
                                                
                                              //  print(NextstopsListvalue)
                                                
                                                nextstopmsg = NextstopsListvalue as! String
                                                
                                            }
                                            
                                        }
                                    }
                                    vehMaker.snippet = "Next Stop: \((nextstopmsg))"
                                    
                                        vehMaker.icon = UIImage(named: "bus_Transit")
                                    
                                    vehMaker.map = self.mapBackgroundView
                                    self.vehicleMarkerArray.add(vehMaker)
                                    
                                }
                                
                                
                            }
                        }
                        
                        
                    }
                }catch{
                    
                }
                
                }.resume();
            DispatchQueue.main.async {
                if self.realTimeMapTimer == nil
                {
                    self.realTimeMapTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                }
            }
        }
        self.isMenuActive = false
        UIView.animate(withDuration: 1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        self.menuBackView.frame = CGRect.init(x: 2000, y: 0, width: (self.view.frame.size.width/2)+90, height: self.view.frame.size.height)
        }, completion: { (finished) -> Void in
            
            
        })
        print(self.routeIDArray.count)
        
    }
    
    @objc func runTimedCode()
    {
        
        let paymentURL2 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeBusesforRouteID?RouteID=\(self.routeIDString)")!
        var request2 = URLRequest(url: paymentURL2)
        print(paymentURL2)
        request2.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request2) { (data, response, error) -> Void in
            
            do
            {
                if self.vehicleMarkerArray.count != 0 {
                   
                   // dump(self.vehicleMarkerArray)
                for markerDet in self.vehicleMarkerArray
                {
                    // print(self.vehicleMarkerArray)
                    DispatchQueue.main.async {
                        let removeMarker : GMSMarker = markerDet as! GMSMarker
                        removeMarker.map = nil;
                       // self.vehicleMarkerArray.remove(markerDet)
                    }
                    }
                    self.vehicleMarkerArray.removeAllObjects()
                }
                
                if data != nil
                {
                    let jsonValue = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray
                    if jsonValue != nil
                    {
                        for jsonDetails in jsonValue!
                        {
                            let jsonDic : NSMutableDictionary = jsonDetails as! NSMutableDictionary
                           // print(jsonDic)
                            if jsonDic.value(forKey: "lat") as! Double != 0 && jsonDic.value(forKey: "lng") as! Double != 0
                            {
                                DispatchQueue.main.async {
                                    let vehMaker = GMSMarker()
                                    vehMaker.position = CLLocationCoordinate2D(latitude:(jsonDic.value(forKey: "lat") as! Double), longitude: (jsonDic.value(forKey: "lng") as! Double))
                                    let titleString = "\(jsonDic.value(forKey: "tripID")!)"
                                      vehMaker.title = "TripID: \(titleString)"
                                   
                                    var nextstopmsg = "\n"
                                    if let Nextstops : [NSDictionary] = jsonDic["Nextstops"] as? [NSDictionary]
                                    {
                                        
                                        
                                        
                                        
                                        for (subject, NextstopsListvalue) in Nextstops[0]
                                        {
                                            //  print ("\(subject): \(NextstopsListvalue)")
                                            
                                            
                                            if "\(subject)" == "StopName" {
                                                
                                               // print(NextstopsListvalue)
                                                
                                                nextstopmsg = NextstopsListvalue as! String
                                                
                                            }
                                            
                                        }
                                    }
                                    vehMaker.snippet = "Next Stop: \((nextstopmsg))"
                                        vehMaker.icon = UIImage(named: "bus_Transit")
                                    
                                    vehMaker.map = self.mapBackgroundView
                                    self.vehicleMarkerArray.add(vehMaker)
                                }
                                if self.realTimeMapTimer == nil
                                {
                                    self.realTimeMapTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                                }
                            }
                        }
                    }
                    
                }
            }catch{
                
            }
            }.resume();
        
        
        
    }
    func showErrorDialogBox(viewController : UIViewController){
        let alertViewController = NYAlertViewController()
        alertViewController.title = "Error"
        alertViewController.message = "Server Error! Please try again later!"
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = viewController.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        // Add alert actions
        
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                
                viewController.dismiss(animated: true, completion: nil)
        }
        )
        
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        viewController.present(alertViewController, animated: true, completion: nil)
        
    }
        
    @objc func animatePolylinePath() {
        if (self.i < self.animatePath.count()) {
            self.animationPath.add(self.animatePath.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.black
            self.animationPolyline.strokeWidth = 4
            self.animationPolyline.map = self.mapBackgroundView
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
        
        
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {
        
        return [44.0]
        
    }
    
    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return 1
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return self.scheduleArray.count
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
               print("didSelectRow:\(indexPath)")
      
        
        
    }
    
   
    
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {
        
        //        print("didSelectExpandedRowAt:\(indexPath)")
    }
   
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
        if let cell = expandedCell as? ExpandedCell {
           
            cell.accessoryType = cell.isSelected ? .checkmark : .none
        }
    }
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
                guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: ExpandableCell2.ID) else { return cellEx }
                let valueDic : NSMutableDictionary = self.scheduleArray[indexPath.row] as! NSMutableDictionary
    
            cell.separatorInset = .zero
                cell.textLabel?.text = "\(valueDic.value(forKey: "RouteID") as! String). \(valueDic.value(forKey: "LineName") as! String)"
       
                cell.textLabel?.font = UIFont.setTarcRegular(size: 13.0)
                return cell
            }
    
    
    
    
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 44
    }
    
    func expandableTableView(_ expandableTableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
       
    }
    
    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func closebar(){
        
    }
}
//extension MapsScheduleViewController: Constrainable {
//    func setupLayoutConstraints() {
//        <#code#>
//    }
//
//
//
//
//
//
//
//}

