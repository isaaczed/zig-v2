//
//  EmergencyAlertViewcontrollerViewController.swift
//  ZIG
//
//  Created by Mac HD on 4/23/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import SafariServices
struct EmergencyAlert {
    static var DontShowAgain:Bool = true
}
class EmergencyAlertViewcontrollerViewController: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var ALertTitle: UILabel!
    @IBOutlet weak var backViewAlert: UIView!

    @IBOutlet weak var btn_box: UIButton!
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var AlertContent: UILabel!
    @IBOutlet weak var dontShowLabel: UILabel!
    @IBOutlet weak var seeNewsEvent: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    var Notificationalert = Bool()
    @IBOutlet weak var AlertImage: UIImageView!
    var  CovidNotificationStatus = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        ALertTitle.text = "COVID - 19"
        AlertImage.image = UIImage(named: "Covid-19")
        let Content = "Attention! Effective April 26, Passengers will be Required to Wear a Face Covering."
        // Do any additional setup after loading the view.
       // AlertContent.attributedText = Content.htmlToAttributedString
        SaveButton.backgroundColor = UIColor.init(red: 248/255.0, green: 0/255.0, blue: 27/255.0, alpha: 1.0)
        

       let userDefaults1 = UserDefaults.standard
         CovidNotificationStatus = userDefaults1.bool(forKey: "CovidNotificationStatus")
        if CovidNotificationStatus == true
        {
            tempNotification()
        }
        else
        {
     
        
        
        
        AlertContent.text = Content
         EmergencyAlert.DontShowAgain = true
        
        ALertTitle.isHidden = true
      //  btn_box.isHidden = true
        //SaveButton.isHidden = true
        AlertContent.isHidden = true
        AlertImage.isHidden = true
        //dontShowLabel.isHidden = true
        seeNewsEvent.isHidden = true
        Notificationalert = false

        let heightView = 315
        let scrollView  = UIScrollView()
        scrollView.frame = CGRect.init(x: 0, y: 55, width: 343, height: heightView)
        scrollView.backgroundColor = UIColor.white
        backViewAlert .addSubview(scrollView)
         scrollView.contentSize = CGSize.init(width: 343, height: heightView)
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = true
        scrollView.delegate = self
        scrollView.bringSubview(toFront: closeButton)
       let userDictonary = userDefaults1.value(forKey: "covidDictionary") as! NSDictionary

        
        var currentCount = 0
      let names = ["Attention! Effective April 26, Passengers will be Required to Wear a Face Covering.Attention! Effective April 26, Passengers will be Required to Wear a Face Covering.Attention! Effective April 26, Passengers will be Required to Wear a Face Covering.Attention! Effective April 26, Passengers will be Required to Wear a Face Covering."]

        let covidname = userDictonary.value(forKey: "CovidTitle") as! String
        let imageURL = userDictonary.value(forKey: "CovidimageURL") as! String
        let content = userDictonary.value(forKey: "CovidContent") as! String

        
        for _ in names {
         //let closeButton
            let backView = UIView()
            backView.frame = CGRect.init(x: currentCount, y: 0, width: 343, height: heightView)
            backView.backgroundColor = UIColor.white
            scrollView.addSubview(backView)
           
            let backscrollView  = UIScrollView()
            backscrollView.frame = CGRect.init(x: 0, y: 0, width: 343, height: heightView)
            backscrollView.backgroundColor = UIColor.white
            backView .addSubview(backscrollView)
            
                    backscrollView.contentSize = CGSize.init(width: 343 , height: heightView)
                   backscrollView.showsVerticalScrollIndicator = true
            
            
            let titleLabel = UILabel()
            titleLabel.frame = CGRect.init(x: 0, y: 0, width: backView.frame.size.width, height: 35)
            titleLabel.text = covidname
            titleLabel.textColor = UIColor.init(red: 54/255, green: 131/255, blue: 196/255, alpha: 1.0)
            titleLabel.textAlignment = .center
            titleLabel.font = UIFont.init(name: "Linotte-Heavy", size: 35)
            backscrollView.addSubview(titleLabel)

            
            let url = URL(string:imageURL as String)
                                   let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            let centerImage = UIImageView()
            centerImage.frame = CGRect.init(x: 116, y: titleLabel.frame.maxY+10, width: 111, height: 111)
            centerImage.image = UIImage(data: data!)
            backscrollView.addSubview(centerImage)
            
            
            let descriptionLabel = UITextView()
            descriptionLabel.frame = CGRect.init(x: 26, y: centerImage.frame.maxY+10, width: 291, height: 51)
            descriptionLabel.font = UIFont.init(name: "Linotte-Regular", size: 17)
            
   
            
            descriptionLabel.attributedText = content.htmlToAttributedString
           
            descriptionLabel.sizeToFit()
           // descriptionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            let height = heightForView(text: content as String, font: UIFont.init(name: "Linotte-Regular", size: 17)!, width:291 )
            print("height:\(height)")
            descriptionLabel.textAlignment = .center
            descriptionLabel.isUserInteractionEnabled = true
           // descriptionLabel.numberOfLines = 0
            descriptionLabel.isEditable = false
            descriptionLabel.dataDetectorTypes = .link
            backscrollView.addSubview(descriptionLabel)
           
            descriptionLabel.frame = CGRect.init(x: 26, y: centerImage.frame.maxY+10, width: 291, height: height)
            descriptionLabel.backgroundColor = UIColor.clear
            
            let seeNewsEvents = UIButton()
            seeNewsEvents.frame = CGRect.init(x: 0, y: descriptionLabel.frame.maxY+10, width: 343, height: 0)
            seeNewsEvents.setTitle("(See News & Events)", for: .normal)
            seeNewsEvents.titleLabel?.font = UIFont.init(name: "Linotte-Heavy", size: 15)
            seeNewsEvents.setTitleColor(UIColor.black, for: .normal)
            seeNewsEvents.addTarget(self, action: #selector(OpenPage(_:)), for: .touchUpInside)
            //backscrollView.addSubview(seeNewsEvents)
           
            backView.frame = CGRect.init(x: currentCount, y: 0, width: 343, height: Int(176+height))

            backscrollView.contentSize = CGSize.init(width: 343 , height: 176+height)

            currentCount = currentCount + Int(scrollView.frame.size.width)

            
//            let checkBox = UIButton()
//            checkBox.frame = CGRect.init(x: 27, y: 374, width: 35, height: 24)
//            checkBox.setImage(UIImage.init(named: "checkbox_in"), for: .normal)
//            backView.addSubview(checkBox)
//
//            let checkLabel = UILabel()
//            checkLabel.frame = CGRect.init(x: 62, y: 376, width: 233, height: 20.5)
//            checkLabel.text = "Don't show again"
//            checkLabel.textColor = UIColor.darkGray
//            checkLabel.font = UIFont.init(name: "Avenir Next Regular", size: 15.0)
//            backView.addSubview(checkLabel)
//
//
//            let okayButton = UIButton()
//            okayButton.frame = CGRect.init(x: 26, y: 405, width: 291, height: 50)
//            okayButton.setTitle("OK", for: .normal)
//            okayButton.backgroundColor = UIColor.init(red: 54/255, green: 134/255, blue: 291/255, alpha: 1.0)
//            okayButton.titleLabel?.font = UIFont.init(name: "Linotte-Heavy", size: 22.0)
//            backView.addSubview(okayButton)




            
            
            
        }
        
        }

        
    }
   
    func tempNotification()
    {
        Notificationalert = true
        dontShowLabel.isHidden = true
        btn_box.isHidden = true
        let userDefaults1 = UserDefaults.standard
        let userDictonary = userDefaults1.value(forKey: "NotificationDictionary") as! NSDictionary

         let NotificationTitle = userDictonary.value(forKey: "NotificationTitle") as! String
        let NotificationContent = userDictonary.value(forKey: "NotificationContent") as! String

        
        let heightView = 315
              let scrollView  = UIScrollView()
              
              scrollView.frame = CGRect.init(x: 0, y: 55, width: 343, height: heightView)
              scrollView.backgroundColor = UIColor.white
              backViewAlert .addSubview(scrollView)
               scrollView.contentSize = CGSize.init(width: 343, height: heightView)
              scrollView.isPagingEnabled = true
              scrollView.showsHorizontalScrollIndicator = true
              scrollView.delegate = self
              scrollView.bringSubview(toFront: closeButton)

              
              var currentCount = 0
            let names = ["Attention! Effective April 26, Passengers will be Required to Wear a Face Covering."]

           

              
              for name in names {
               //let closeButton
                  let backView = UIView()
                  backView.frame = CGRect.init(x: currentCount, y: 0, width: 343, height: heightView)
                  backView.backgroundColor = UIColor.white
                  scrollView.addSubview(backView)
                 
                  let backscrollView  = UIScrollView()
                  backscrollView.frame = CGRect.init(x: 0, y: 0, width: 343, height: heightView)
                  backscrollView.backgroundColor = UIColor.white
                  backView .addSubview(backscrollView)
                  
                          backscrollView.contentSize = CGSize.init(width: 343 , height: heightView)
                         backscrollView.showsVerticalScrollIndicator = true
                  
                  
                  let titleLabel = UILabel()
                  titleLabel.frame = CGRect.init(x: 20, y: 0, width: backView.frame.size.width-40, height: 35)
                  titleLabel.text = NotificationTitle
                  titleLabel.textAlignment = .center
                titleLabel.sizeToFit()
                    titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                  titleLabel.font = UIFont.init(name: "Linotte-Heavy", size: 35)
                titleLabel.numberOfLines = 0;
                let titleheight = heightForView(text: NotificationContent as String, font: UIFont.init(name: "Linotte-Heavy", size: 35)!, width:backView.frame.size.width )
                titleLabel.frame = CGRect.init(x: 0, y: 0, width: backView.frame.size.width, height: titleheight)
                titleLabel.textColor = UIColor.init(red: 54/255, green: 131/255, blue: 196/255, alpha: 1.0)

                  backscrollView.addSubview(titleLabel)

                  
                  
                  
                  let descriptionLabel = UILabel()
                  descriptionLabel.frame = CGRect.init(x: 26, y: titleLabel.frame.maxY+30, width: 291, height: 51)
                  descriptionLabel.font = UIFont.init(name: "Linotte-Regular", size: 17)
                  let yourAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.init(name: "Linotte-Regular", size: 17)]

                let attributedString = NSMutableAttributedString(string: NotificationContent,attributes: yourAttributes as [NSAttributedStringKey : Any])

                // *** Create instance of `NSMutableParagraphStyle`
                let paragraphStyle = NSMutableParagraphStyle()

                // *** set LineSpacing property in points ***
                paragraphStyle.lineSpacing = 2 // Whatever line spacing you want in points

                // *** Apply attribute to string ***
                attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
                
               


                  descriptionLabel.attributedText = attributedString
                 
                  descriptionLabel.sizeToFit()
                  descriptionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                var height = heightForView(text: NotificationContent as String, font: UIFont.init(name: "Linotte-Regular", size: 17)!, width:291 )
                  print("height:\(height)")
                  descriptionLabel.textAlignment = .center
                  descriptionLabel.numberOfLines = 0
                  backscrollView.addSubview(descriptionLabel)
                 height = height + 10
                  descriptionLabel.frame = CGRect.init(x: 26, y: titleLabel.frame.maxY+10, width: 291, height: height)
                  descriptionLabel.backgroundColor = UIColor.clear
                  
                  let seeNewsEvents = UIButton()
                  seeNewsEvents.frame = CGRect.init(x: 0, y: descriptionLabel.frame.maxY+10, width: 343, height: 0)
                  seeNewsEvents.setTitle("(See News & Events)", for: .normal)
                  seeNewsEvents.titleLabel?.font = UIFont.init(name: "Linotte-Heavy", size: 15)
                  seeNewsEvents.setTitleColor(UIColor.black, for: .normal)
                  seeNewsEvents.addTarget(self, action: #selector(OpenPage(_:)), for: .touchUpInside)
                  //backscrollView.addSubview(seeNewsEvents)
                 
                  backView.frame = CGRect.init(x: currentCount, y: 0, width: 343, height: Int(141+height+titleheight))

                  backscrollView.contentSize = CGSize.init(width: 343 , height: 141+height+titleheight)

                  currentCount = currentCount + Int(scrollView.frame.size.width)
    }
    }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect.init(x:0,y: 0,width: width, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    @IBAction func btn_box(sender: UIButton) {
        if (btn_box.isSelected == true)
        {
            print("notselect")
             btn_box.setImage(UIImage.init(named: "checkbox_out"), for: .normal)
            EmergencyAlert.DontShowAgain = false
            btn_box.isSelected = false
        }
        else
        {
            print("select")
             btn_box.setImage(UIImage.init(named: "checkbox_in"), for: .normal)
            EmergencyAlert.DontShowAgain = true
            btn_box.isSelected = true
        }
    }
    
    @IBAction func save(_ sender: Any) {
        if CovidNotificationStatus == true
               {
                   self.dismiss(animated: true, completion: nil)

               }
        else
        {
        let userDefaults1 = UserDefaults.standard
        userDefaults1.removeObject(forKey: "ShowEmergencyAlert")
        if (EmergencyAlert.DontShowAgain){
            // userDefaults1.set(false, forKey: "ShowEmergencyAlert")
            print("Not show")
            
             userDefaults1.set("Yes", forKey: "ShowEmergencyAlert")
                       userDefaults1.synchronize()
        }
        else{
           userDefaults1.removeObject(forKey: "ShowEmergencyAlert")
             print("Show ... ")
        }


      self.dismiss(animated: true, completion: nil)


        }

    }
    @IBAction func close(_ sender: Any) {
      
     
              self.dismiss(animated: true, completion: nil)

        
    }
    @IBAction func OpenPage(_ sender: Any) {
                if let url = URL(string: "https://bit.ly/3cAYQEP") {
                    let vc: SFSafariViewController
        
                    if #available(iOS 11.0, *) {
                        let config = SFSafariViewController.Configuration()
                        config.entersReaderIfAvailable = false
                        vc = SFSafariViewController(url: url, configuration: config)
                    } else {
                        vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
                    }
                    vc.delegate = self as? SFSafariViewControllerDelegate
                    present(vc, animated: true)
                }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension String {
    var htmlToAttributedString: NSAttributedString? {

        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
       
        return htmlToAttributedString?.string ?? ""
    }
}
extension NSMutableAttributedString {

    public func setAsLink(textToFind:String, linkURL:String) -> Bool {

        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(.link, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}
