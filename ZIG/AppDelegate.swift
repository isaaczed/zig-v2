//
//  AppDelegate.swift
//  ZIG
//
//  Created by Isaac on 24/11/17.
//  Copyright © 2017 Isaac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import UberCore
import LyftSDK
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import CocoaLumberjack

import RealmSwift
import DLLocalNotifications
import IQKeyboardManagerSwift
import Firebase
import Siren
import CoreBluetooth
import UserNotifications
import AVFoundation
import SwiftyJSON
import CoreLocation
import SCLAlertView
import Toast_Swift

//Comment-line

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CBCentralManagerDelegate, CBPeripheralDelegate, CLLocationManagerDelegate, UNUserNotificationCenterDelegate {
    //let pianoSound = URL(fileURLWithPath: Bundle.main.path(forResource: "TicketActivated", ofType: "mp3")!)
    var audioPlayer = AVAudioPlayer()
    var checkFirstDataPush: Bool = false
    var window: UIWindow?
    let realm = try! Realm()
    var ObeaconFirstTimeBool: Bool = true
    var firsttimeload: Bool = false
    var Active_Ticket_Count: Int = 0
    var reactivationTicketcount:Int = 0
    var Validated_ticket_count: Int = 0
    var devicenameInitial = "BIBO 1.1 A"
    var currentlat: Double?
    var currentLong: Double?
    var BeaconMacID = ""
    var BeaconName = ""
    var BeaconMessage = ""
    var NewVarReactivationcount:Int = 0
     var NewVarReactivationcountFirst:Int = 0
    var TicketStatusFor: String?
    var illegalEntryFlag = true
    var RouteID_macID: String?
    var badgecount = [String]()
    var fileLogger: DDFileLogger?
    var mydeviceToken: Data!
    var pageCount = 0
    var totalPageCount = 0
    var tankTotalPage = 0
    var tankPageIndex = 0
    var isActiveTicket: Bool = false
    var isValideActiveTicket: Bool = false
    var ticketCheckId: Int = 0
    var ticketRotueID = ""
    var realTimeMapTimer: Timer!
    var locationRealm = CLLocation()
    var PushData: Timer!
    var isValidated = false

    var RSSIs = [NSNumber]()
    var data = NSMutableData()
    var writeData: String = ""
    var alertView = SCLAlertView()
    var timer: Timer!
    var timerEmail : Timer?
    var TicketDownload:Timer?
    var txCharacteristic: CBCharacteristic?
    var rxCharacteristic: CBCharacteristic?
    var characteristicASCIIValue = String()
    var centralManager: CBCentralManager!
    var BIBOPeripheral: CBPeripheral!
    let BLE_Characteristic_uuid_Tx = CBUUID(string: beaconData.kBLE_Characteristic_uuid_Tx)//(Property = Write without response)
    let BLE_Characteristic_uuid_Rx = CBUUID(string: beaconData.kBLE_Characteristic_uuid_Rx)// (Property = Read/Notify)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //self.callSystemMapApi(pageNumber: 0)

        comparedate()

         
        
        if isReachable() {
            GetallBeaconData()
            let defaults = UserDefaults.standard

            if defaults.object(forKey: "isAnalyticsSession") == nil {
                defaults.set("No", forKey: "isAnalyticsSession")
                defaults.synchronize()
                analytics.getSession()
                let SessionID = defaults.value(forKey: "AnalyticsSessionID")
                // ////print("Session==>\((SessionID)!)")
            }
            else
            {
                analytics.getSession()
                let SessionID = defaults.value(forKey: "AnalyticsSessionID")
                // ////print("Session==>\((SessionID)!)")

            }
            GetstartBackground()

            
        }
                   if UserDefaults.standard.object(forKey: "SaveLocalMail") != nil{
                      print("Here you will get saved value")
                        timerEmail = Timer.scheduledTimer(timeInterval:10, target: self, selector: #selector(offlineCard), userInfo: nil, repeats: true)
                   } else {
                      print("No value in Userdefault,Either you can save value here or perform other operation")
                   }

        badgecount.removeAll()



        FirebaseApp.configure()
        let userDefaults1 = UserDefaults.standard
        let accessToken = userDefaults1.value(forKey: "accessToken")
        ////print(accessToken ?? nil ?? "no token")

        if accessToken != nil
            {
            if let rootViewController = self.window!.rootViewController as? UINavigationController {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let viewcontroller = storyboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as? NewDashboardViewController {
                    rootViewController.pushViewController(viewcontroller, animated: true)
                }
            }
            userDefaults1.set("false", forKey: "showPreferencesOnLogin")

        } //Fabric.sharedSDK().debug = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(WithOutLoginMainViewController.self)
        // Kontakt.setAPIKey("IfPtgnGWtHRoDNRyJwBBVTnsaxaNpztB")
        LyftConfiguration.developer = (token: "y+GCru+bjcV95J3RTpQLAydFZumXQN3H/ZPKj9J8n6Xvo5QyiJ8/exkwQRz2T6JzLmsO39BnyIWYfZcH+Za8YtkzwevC8lD7g3hhgkfMAV8FOqQMaRcBjNc=", clientId: "YwqXKXdioxmu")
        GMSServices.provideAPIKey(Key.GoogleAPIKey)
        GMSPlacesClient.provideAPIKey(Key.GoogleAPIKey)



        if application.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:))) {
            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        }


        #if false
            UIApplication.shared.registerForRemoteNotifications()
        #endif

        if self.fileLogger == nil {
            DDLog.add(DDTTYLogger.sharedInstance)
            self.fileLogger = DDFileLogger()
            self.fileLogger!.logFileManager.maximumNumberOfLogFiles = 1
            self.fileLogger!.maximumFileSize = UInt64(0.5 * 1024 * 1024)
            self.fileLogger?.logFormatter = DDLogFileFormatterDefault()
            DDLog.add(self.fileLogger!)
        }

        do {
            try Network.reachability = Reachability(hostname: "www.google.com")
        }
        catch {
            switch error as? Network.Error {
            case let .failedToCreateWith(hostname)?:
                print("Network error:\nFailed to create reachability object With host named:", hostname)
            case let .failedToInitializeWith(address)?:
                print("Network error:\nFailed to initialize reachability object With address:", address)
            case .failedToSetCallout?:
                print("Network error:\nFailed to set callout")
            case .failedToSetDispatchQueue?:
                print("Network error:\nFailed to set DispatchQueue")
            case .none:
                print(error)
            }
        }

                                      if  userDefaults1.value(forKey: "userIdString") == nil
                                              {
                                                Crashlytics.sharedInstance().setUserIdentifier("WithoutLogin")

                                      }
                          else
                                      {
                                       var  userIDString:Int?
                                       
                                       if userDefaults1.integer(forKey: "userIdString") != nil {
                                           userIDString = userDefaults1.integer(forKey: "userIdString")
                                        Crashlytics.sharedInstance().setUserIdentifier("\(userIDString ?? 0)")
                                       }
        }

        return true
    }

    func stopTimerTest() {
         if self.timerEmail != nil {
                       self.timerEmail!.invalidate()
                       self.timerEmail = nil
                   }
    }
    @objc func offlineCard()
    {
        let userdefaults = UserDefaults.standard

        let returnValue: [String : Any] = (UserDefaults.standard.object(forKey: "SaveLocalMail") as? [String : Any])!
        let returnFailedValue: [String : Any] = (UserDefaults.standard.object(forKey: "SaveLocalFailedMail") as? [String : Any])!

if isReachable()
{
        Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: returnValue, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                case .success:
                    
                  //  self.hideLoader()
                    if (response.result.value != nil) {
                        
                        let responseVar = response.result.value
                        let json = JSON(responseVar!)
                        print(json)
                        if json["Message"] == "Ok" {
                            UserDefaults.standard.removeObject(forKey: "SaveLocalMail")
                            UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")

                            self.stopTimerTest()
                                                                              print("OFFLINE==>Valid")

                            
                        }else{
                          //  self.hideLoader()
                            print("OFFLINE==>Invalid")
                            self.sendEmailFailed(sendMailedparameter: returnFailedValue)
                            UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")
                            self.stopTimerTest()

                        }
                        
                    }else{
                       // self.hideLoader()
                        print("OFFLINE==>Invalid")
                        self.sendEmailFailed(sendMailedparameter: returnFailedValue)
                        UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")
                        self.stopTimerTest()

                    }
                case .failure(let error):
                    print(error)
                    //self.hideLoader()
                    print("OFFLINE==>Invalid")
                    self.sendEmailFailed(sendMailedparameter: returnFailedValue)
                    UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")
                    self.stopTimerTest()

                }
        }
    }

        else
{
    print("OFFLINE==>APPdelegateoffline")

        }
    }

    func sendEmailFailed(sendMailedparameter: [String : Any])
        {
          
            if isReachable()
            {
            Alamofire.request(APIUrl.sendEmailFailureMail, method: .post, parameters: sendMailedparameter, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        
                      
                        if (response.result.value != nil) {
                            
                            let responseVar = response.result.value
                          
  
                            
                        }else{
                           
                            print("OFFLINE==>failure")

                        }
                    case .failure(let error):
                        print(error)
                        print("OFFLINE==>OFFLINE")

                    }
            }
        }
        }

    func comparedate() {

        Alamofire.request(APIUrl.ExpiryDate, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in

                switch response.result {
                case .success:


                    if response.result.value != nil {
                        let expiryarray = response.result.value
                        let DateExpiry = JSON(expiryarray!)

                        var StartDate = "\(DateExpiry["Startdate"])"
                        var EndDate = "\(DateExpiry["Enddate"])"
                        let dateFormatterq = DateFormatter()
                        dateFormatterq.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        dateFormatterq.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX

                        let StartExpirydate = dateFormatterq.date(from:StartDate)!
                        let EndtExpiryDate = dateFormatterq.date(from:EndDate)!

                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy-MM-dd"

                        let myString = formatter.string(from: Date()) // string purpose I add here
                        // convert your string to date
                      //  let yourDate = formatter.date(from: myString)
                        //then again set the date format whhich type of output you need
                        formatter.dateFormat = "yyyy-MM-dd"
                        // again convert your date to string
                        StartDate = formatter.string(from: StartExpirydate)
                        EndDate = formatter.string(from: EndtExpiryDate)

                        //print("\(StartDate), \(EndDate)")


        let date11 = StartDate
        let date22 = EndDate
                        //let date22 = "2020-09-01"
                        
//                        let date11 = "2019-07-01"
//                               let date22 = "2020-09-01"

        let current = Date()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currentDate = dateFormatter.string(from: current)

        let date1 = dateFormatter.date(from: date11)!
        let date2 = dateFormatter.date(from: date22)!
        var currentDateFinal = dateFormatter.date(from: currentDate)

        let date1String = dateFormatter.string(from: date1)
        let date2String = dateFormatter.string(from: date2)

        if date1String <= currentDate {
            if currentDate <= date2String {

                comVar.DateImplementation = true

                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Expiry"), object: nil)

            }
        }
        else {
            print("Looser")

            comVar.DateImplementation = false
            //comVar.DateImplementation = true
        }
                    }
                case .failure(let error):
                    //print(error)
                     comVar.DateImplementation = true
                }
        }

    }
    func GetallBeaconData() {

//print(APIUrl.MacIDURL)
        if isReachable()
            {
            Alamofire.request(APIUrl.MacIDURL, method: .get, encoding: JSONEncoding.default)
                .responseJSON { response in

                    switch response.result {
                    case .success:
                        //print("Beacon Data Featch Success")
                        if response.result.value != nil {
                            if let json = response.result.value as? [String: Any] {
                                if json["BeaconIds"] != nil {
                                    let Macids = json["BeaconIds"]  as! NSArray
                                    for Macidlist in Macids {


                                        let BeacanMacID = BeaconRealmModel()
                                        BeacanMacID.MacID = Macidlist as! String
                                        try! self.realm.write {

                                            self.realm.add(BeacanMacID, update: .all)

                                        }

                                    }
                                    do {
                                        let beacon_db = try! Realm().objects(BeaconRealmModel.self)
                                        //print("beacon_db count\(beacon_db.count)")

                                        //print("saveditem beacon_db")
                                    }
                                    catch {
                                        //print(error)
                                    }
                                }
                            }


                        }
                    case .failure(let error):
                        print(error)

                    }
            }
        }


    }
            func  GetstartBackground() {
                if(isReachable())
                {
                    let userDefaults1 = UserDefaults.standard
                    let accessToken = userDefaults1.value(forKey: "accessToken")
                   
                    //print(accessToken ?? nil ?? "no token")
                    if accessToken != nil {
                        userDefaults1.set(true, forKey: "IsUserLogin")
                        TicketSave.TicketDataUpload(CountForRow: 0, Expiry: true) { (SuccessMsg, cOunt) in
                            if SuccessMsg {



                                print("First Time Ticket Saving ......  ")
                            }
                        }

                        let url = "\(APIUrl.ZIGTARCAPI)Preferences/GetAppPreferences?"
                        let parameters: Parameters = [
                            "Pin": "ZIG19",

                        ]


                        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                            .responseObject { (response: DataResponse<preferenceGet>) in
                                switch response.result {
                                case .success:
                                    //print(response.result.value ?? "nil")
                                    if response.result.value?.Message != nil
                                        {
                                        
                                        userDefaults1.set("true", forKey: "isBusOn")
                                        userDefaults1.set(response.result.value?.Bird, forKey: "isBirdOn")
                                        userDefaults1.set(response.result.value?.Louvelo, forKey: "LouveloOn")
                                        userDefaults1.set(response.result.value?.Uber, forKey: "UberOn")
                                        userDefaults1.set(response.result.value?.Lyft, forKey: "LyftOn")
                                        userDefaults1.set(false, forKey: "BoltOn")
                                        userDefaults1.set(response.result.value?.Lime, forKey: "LimeOn")
                                        userDefaults1.set(response.result.value?.Livepayment, forKey: "Livepayment")
                                            userDefaults1.set(response.result.value?.COVID, forKey: "EmergencyAlert")

                                            let covidDictionary = NSMutableDictionary()
                                            covidDictionary.setValue(response.value?.CovidTitle, forKey: "CovidTitle")
                                            covidDictionary.setValue(response.value?.CovidimageURL, forKey: "CovidimageURL")
                                            covidDictionary.setValue(response.value?.CovidContent, forKey: "CovidContent")
                                            print(covidDictionary)
                                            userDefaults1.set(covidDictionary, forKey: "covidDictionary")
                                         

                                            
                                            
                                        userDefaults1.synchronize()
                                            //let ShowEmergencyAlert = userDefaults1.object(forKey: "ShowEmergencyAlert") as! String
                                          //  userDefaults1.value(forKey: "ShowEmergencyAlert")
                                        //    print(response.result.value!.COVID!)
                                        //    print(ShowEmergencyAlert)
                                            //ArunLocal
                                            if ((response.result.value!.COVID!) && (userDefaults1.object(forKey: "ShowEmergencyAlert") == nil)) {
                                                userDefaults1.set(false, forKey: "CovidNotificationStatus")

                                                if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EmergencyAlertViewcontrollerViewController") as? EmergencyAlertViewcontrollerViewController {
                                                    if let window = self.window, let rootViewController = window.rootViewController {
                                                        var currentController = rootViewController
                                                        while let presentedController = currentController.presentedViewController {
                                                            currentController = presentedController
                                                        }
                                                        currentController.present(controller, animated: true, completion: nil)
                                                    }
                                                }
                                                
                                                
                                                
                                                
                                            }
                                            
                                             
                                            
                                            
                                        self.perferenceMethod(accessToken: accessToken as! NSString)
                                            
                                            
                                    }
                                    else
                                    {
                                        self.perferenceMethod(accessToken: accessToken as! NSString)
                                    }
                                case .failure(let error):
                                    print(error)
                                    self.perferenceMethod(accessToken: accessToken as! NSString)

                                }
                        }

                        
                        
                        
                        

                        let tokenValidationUrl = "\(APIUrl.ZIGTARCAPI)validation/UserTokenValidation"
                        let tokenParameters: Parameters = [
                            "token": accessToken!,
                        ]
                        Alamofire.request(tokenValidationUrl, method: .get, parameters: tokenParameters, encoding: URLEncoding.default)
                            .responseObject { (response: DataResponse<ValidateToken>) in
                                switch response.result {
                                case .success:
                                    //print(response.result.value ?? "nil")
                                    if response.result.value?.message == "Ok" {

                                    } else {
                                        
                                        let appearance = SCLAlertView.SCLAppearance(
                                                           showCloseButton: false

                                                       )
                                        self.alertView.hideView()

                                        self.alertView = SCLAlertView(appearance: appearance)


                                        self.alertView.addButton("login") {

                                              Crashlytics.sharedInstance().setUserIdentifier("Without Login")
                                                                                        let userDefaults = UserDefaults.standard
                                                                  userDefaults.removeObject(forKey: "userIdString")
                                                                                                      userDefaults.removeObject(forKey: "AnalyticsSessionID")
                                                                                                      userDefaults.removeObject(forKey: "userName")
                                                                                        userDefaults.removeObject(forKey: "accessToken")
                                                                                         userDefaults.removeObject(forKey: "userName")
                                                                                         userDefaults.removeObject(forKey: "userPhoto")
                                                                                         userDefaults.removeObject(forKey: "userEmail")
                                                                                         userDefaults.removeObject(forKey: "feedback")
                                                                                         userDefaults.removeObject(forKey: "isLyft")
                                                                                         userDefaults.removeObject(forKey: "isUber")
                                                                                         userDefaults.removeObject(forKey: "isCab")
                                                                                         userDefaults.removeObject(forKey: "ProfileID")
                                                                                         userDefaults.removeObject(forKey: "IsUserLogin")
                                                                                         userDefaults.removeObject(forKey: "SavedDictionary")
                                                                                         userDefaults.removeObject(forKey: "phoneNumber")

                                                                                          let Ticketlist_db = try! Realm().objects(TicketRealmModelNewNew.self)
                                                                                         print(Ticketlist_db.count)
                                                                                         print("Updated Date - \(Date())")
                                                                                         userDefaults.synchronize()
                                                                                        let realma = try! Realm()
                                                                                                         try! realma.write {
                                                                                                             let deletedNotifications = realma.objects(TicketRealmModelNewNew.self)
                                                                                          realma.delete(deletedNotifications)
                                                                                        }
                                                                                         
                                                                                         let realm = try! Realm()
                                                                                                                    try! realm.write {
                                                                                                                        realm.deleteAll()
                                                                                                                    }
                                                      

                                                                                
                                                                                        if let rootViewController = self.window!.rootViewController as? UINavigationController {
                                                                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                            if let viewcontroller = storyboard.instantiateViewController(withIdentifier: "initalscreenViewController") as? initalscreenViewController {
                                                                                                viewcontroller.fromAppDelegateBool = true
                                                                                                rootViewController.pushViewController(viewcontroller, animated: true)
                                                                                            }
                                                                                        }
                                                       }

                                        self.alertView.showWarning("Session Expired", subTitle: "Please re-login to renew your session.")
                                        
                       


                                    }

                                case .failure(let error):
                                    print(error)

                                }
                        }



                    }
                    else
                    {

                    }





                    let notificatonurl = "\(APIUrl.ZIGTARCAPI)Preferences/GetNotification"

                    Alamofire.request(notificatonurl, method: .get, parameters: nil, encoding: URLEncoding.default)
                        .responseObject { (response: DataResponse<notificationpreferenceGet>) in
                            switch response.result {
                            case .success:
                                //print(response.result.value ?? "nil")
                                if response.result.value?.Message != nil
                                    {
                                    

                                     
                                        let notificationDictionary = NSMutableDictionary()
                                        notificationDictionary.setValue(response.value?.NotificationStatus, forKey: "NotificationStatus")
                                        notificationDictionary.setValue(response.value?.NotificationTitle, forKey: "NotificationTitle")
                                        notificationDictionary.setValue(response.value?.NotificationContent, forKey: "NotificationContent")
                                        print(notificationDictionary)
                                        userDefaults1.set(notificationDictionary, forKey: "NotificationDictionary")
                                        
                                        
                                    userDefaults1.synchronize()

                                        
                                          if (response.result.value!.NotificationStatus!)
                                        {
                                            
                                            userDefaults1.set(true, forKey: "CovidNotificationStatus")

                                            if let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EmergencyAlertViewcontrollerViewController") as? EmergencyAlertViewcontrollerViewController {
                                                if let window = self.window, let rootViewController = window.rootViewController {
                                                    var currentController = rootViewController
                                                    while let presentedController = currentController.presentedViewController {
                                                        currentController = presentedController
                                                    }
                                                    currentController.present(controller, animated: true, completion: nil)
                                                }
                                            }
                                            
                                            
                                            
                                            
                                        }
                                         
                                        
                                        
                                        
                                        
                                }
                                else
                                {
                                }
                            case .failure(let error):
                                print(error)

                            }
                    }
                    
                    


                }
            }
    func perferenceMethod(accessToken: NSString)
    {
        //
        let userDefaults1 = UserDefaults.standard

        let urls = "\(APIUrl.ZIGTARCAPI)Preferences/get"
        let parameterss: Parameters = [
            "TokenID": accessToken,

        ]


        Alamofire.request(urls, method: .get, parameters: parameterss, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<TokenGet>) in
                switch response.result {
                case .success:
                    //print(response.result.value ?? "nil")
                    if response.result.value?.message == nil
                        {

                        //Arun 23 june


                        userDefaults1.set(response.result.value?.bus, forKey: "isBus")
                        if !userDefaults1.bool(forKey: "isBirdOn")
                            {
                            userDefaults1.set("false", forKey: "isBike")
                        }
                        else
                        {
                            userDefaults1.set("true", forKey: "isBike")
                        }

                        if !userDefaults1.bool(forKey: "LouveloOn")
                            {
                            userDefaults1.set("false", forKey: "isTrain")

                        }
                        else
                        {
                            userDefaults1.set(response.result.value?.train, forKey: "isTrain")

                        }
                        if response.result.value?.bikeshare == nil
                            {
                            userDefaults1.set("false", forKey: "bikeshare")
                        }
                        else
                        {
                            userDefaults1.set(response.result.value?.bikeshare, forKey: "bikeshare")
                        }
                        if response.result.value?.uber == nil && response.result.value?.lyft == nil
                            {
                            if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                                {
                                userDefaults1.set(false, forKey: "isCab")
                                userDefaults1.set(false, forKey: "isUber")
                                userDefaults1.set(false, forKey: "isLyft")
                            }
                            else
                            {
                                userDefaults1.set(true, forKey: "isCab")
                                userDefaults1.set(true, forKey: "isUber")
                                userDefaults1.set(false, forKey: "isLyft")
                            }

                        }
                        else
                        {
                            if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                                {
                                userDefaults1.set(false, forKey: "isCab")
                                userDefaults1.set(false, forKey: "isUber")
                                userDefaults1.set(false, forKey: "isLyft")
                            }
                            else
                            {
                                userDefaults1.set(response.result.value?.cab, forKey: "isCab")
                                userDefaults1.set(response.result.value?.uber, forKey: "isUber")
                                userDefaults1.set(response.result.value?.lyft, forKey: "isLyft")

                                if !userDefaults1.bool(forKey: "UberOn")
                                    {
                                    if response.result.value?.uber == true
                                        {
                                        userDefaults1.set("false", forKey: "isUber")
                                    }

                                }
                                else if !userDefaults1.bool(forKey: "LyftOn")
                                    {
                                    if response.result.value?.lyft == true
                                        {
                                        userDefaults1.set("false", forKey: "isLyft")
                                    }
                                }
                            }
                        }


                        userDefaults1.synchronize()
                    }
                    else
                    {
                        userDefaults1.set("true", forKey: "isBus")

                        if !userDefaults1.bool(forKey: "isBirdOn")
                            {
                            userDefaults1.set("false", forKey: "isBike")
                        }
                        else
                        {
                            userDefaults1.set("true", forKey: "isBike")
                        }

                        if !userDefaults1.bool(forKey: "LouveloOn")
                            {
                            userDefaults1.set("false", forKey: "isTrain")

                        }
                        else
                        {
                            userDefaults1.set(response.result.value?.train, forKey: "isTrain")

                        }

                        if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                            {
                            userDefaults1.set(false, forKey: "isCab")
                            userDefaults1.set(false, forKey: "isUber")
                            userDefaults1.set(false, forKey: "isLyft")
                        }
                        else
                        {
                            userDefaults1.set(true, forKey: "isCab")
                            userDefaults1.set(true, forKey: "isUber")
                            userDefaults1.set(false, forKey: "isLyft")
                            if !userDefaults1.bool(forKey: "UberOn")
                                {
                                userDefaults1.set(false, forKey: "isCab")
                                userDefaults1.set(false, forKey: "isUber")
                                userDefaults1.set(false, forKey: "isLyft")
                            }


                        }
                        if response.result.value?.bikeshare == nil
                            {
                            userDefaults1.set("false", forKey: "bikeshare")
                        }
                        else
                        {
                            userDefaults1.set(response.result.value?.bikeshare, forKey: "bikeshare")
                        }

                        userDefaults1.synchronize()
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        if application.applicationState != UIApplicationState.active {
            let data = ["localnotif": notification]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "SALocalNotification"), object: nil, userInfo: data)
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        UIApplication.shared.isIdleTimerDisabled = false

    }


    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool {
        let handledUberURL = UberAppDelegate.shared.application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation] as Any)

        return handledUberURL
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.isIdleTimerDisabled = true

    }


    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.isIdleTimerDisabled = true
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        // Here you can check whether you have allowed the permission or not.

//        if comVar.DateImplementation
//        {
        centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: false])
        // }
      //  siren.majorUpdateAlertType = .force
         Siren.shared.majorUpdateAlertType = .force
        Siren.shared.checkVersion(checkType: .immediately)
       
        //application.applicationIconBadgeNumber = 0


    }

    func applicationWillTerminate(_ application: UIApplication) {
        UIApplication.shared.isIdleTimerDisabled = false
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }







    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject: AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        //UserInfo dictionary will contain data sent from the server
    }
    func getlatlongvalue() {
        let locManager = CLLocationManager()
        var currentLocation: CLLocation!

        if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .notDetermined) {

            currentLocation = locManager.location

            if currentLocation != nil {
                currentLong = currentLocation.coordinate.longitude
                currentlat = currentLocation.coordinate.latitude
                //print("locations = \(currentlat!),\(currentLong!)")
            }
            // else{




            // }
        }
        else {
            locManager.requestWhenInUseAuthorization()

            if(CLLocationManager.authorizationStatus() == .notDetermined)
            {

            }
            else
            {

                currentLong = nil
                currentlat = nil

                if self.timer != nil {
                    self.timer.invalidate()
                    self.timer = nil
                }

                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                alertView.hideView()
                alertView = SCLAlertView(appearance: appearance)

                alertView.addButton("Location setting") {
                    //ComValue.internetalertstatus = true
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }

                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            // Checking for setting is opened or not
                            //print("Setting is opened: \(success)")

                            // self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.StartBluetoothScan), userInfo: nil, repeats: false)
                        })
                    }
                }

                alertView.addButton("Done") {

                    //self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.StartBluetoothScan), userInfo: nil, repeats: false)
                }
                //print("LocationARUN==>A")

                alertView.showWarning("Location Denied", subTitle: "Please enable location access for TARC in your iOS settings")
            }
        }

    }
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("central.state is .unknown")
        case .resetting:
            print("central.state is .resetting")
        case .unsupported:
            print("central.state is .unsupported")
        case .unauthorized:
            print("central.state is .unauthorized")
        case .poweredOff:
            print("central.state is .poweredOff")
        case .poweredOn:
            print("central.state is .poweredOn")
            print(comVar.DateImplementation)

            if comVar.DateImplementation {
            getlatlongvalue()
            StartBluetoothScan()
            }


        }
    }
    @objc func StartBluetoothScan() {
        //print("Start Scan \(Date())")
        //if comVar.DateImplementation {
        if UserDefaults.standard.object(forKey: "accessToken") != nil && UserDefaults.standard.bool(forKey: "IsUserLogin") == true {

            if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .notDetermined) {
                //print("!firsttimeload - \(!firsttimeload)")
                if !firsttimeload {

                    firsttimeload = true

                    if isReachable() {
                        //print("!isReachable - \(!firsttimeload)")
                        TicketSave.TicketDataUpload(CountForRow: 0, Expiry: true, completion: { (SuccessMessage, Count) in
                            if SuccessMessage {
                                //print(Count)
                                //print(SuccessMessage)
                                if Count > 0 {

                                    let TicketRealm = self.realm.objects(TicketRealmModelNewNew.self)

                                    self.centralManager.scanForPeripherals(withServices: nil)
                                    let activateTicket = self.realm.objects(TicketRealmModelNewNew.self).filter("Status == 2")
                                    //print(activateTicket.count)
                                    //print("Testing For Before")
                                    self.Active_Ticket_Count = activateTicket.count

                                    let ValidateTicket = self.realm.objects(TicketRealmModelNewNew.self).filter("Status == 3")
                                    //print(ValidateTicket.count)
                                    //print("Testing For Before")
                                    self.Validated_ticket_count = ValidateTicket.count
                                }
                                else {
                                    self.Validated_ticket_count = 0
                                    self.Active_Ticket_Count = 0
                                }

                                self.centralManager.scanForPeripherals(withServices: nil)

                            }
                            else {
                                self.centralManager.scanForPeripherals(withServices: nil)
                            }
                        })
                    }
                    else {
                        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)
                        //print("No Internet connection")
                    }
                }
                else {



                    let TicketRealm = self.realm.objects(TicketRealmModelNewNew.self)
                    
                    let reactivationTicket = realm.objects(TicketRealmModelNewNew.self).filter("Reactivatecount > 0")
                    self.reactivationTicketcount = reactivationTicket.count
                            
                    //self.centralManager.scanForPeripherals(withServices: nil)
                    let activateTicket = self.realm.objects(TicketRealmModelNewNew.self).filter("Status == 2")
                    //print(activateTicket.count)
                    //print("Testing For Before")
                    self.Active_Ticket_Count = activateTicket.count

                    let ValidateTicket = self.realm.objects(TicketRealmModelNewNew.self).filter("Status == 3")
                    //print(ValidateTicket.count)
                    //print("Testing For Before")
                    self.Validated_ticket_count = ValidateTicket.count
                    self.centralManager.scanForPeripherals(withServices: nil)
                }

            }
            else {

                //print("Location")
                firsttimeload = false
                // macAddressGet = false
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false

                )
                alertView.hideView()

                alertView = SCLAlertView(appearance: appearance)

                alertView.addButton("Location setting") {
                    //ComValue.internetalertstatus = true
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }

                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            // Checking for setting is opened or not
                            //print("Setting is opened: \(success)")

                            self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.StartBluetoothScan), userInfo: nil, repeats: false)
                        })
                    }
                }

                alertView.addButton("Close") {

                    self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.StartBluetoothScan), userInfo: nil, repeats: false)
                }
                //print("LocationARUN==>B")

                alertView.showWarning("Location Denied", subTitle: "Please enable location access for TARC in your iOS settings")

            }
        }
        else {
            //print("Waiting For Login no login ")
            firsttimeload = false
            // macAddressGet = false
            self.timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)

        }
//
//        else{
//            //print("ExpiryDate check Field")
//            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)
//
//        }


    }


    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
        advertisementData: [String: Any], rssi RSSI: NSNumber) {

        if UserDefaults.standard.object(forKey: "accessToken") != nil && UserDefaults.standard.bool(forKey: "IsUserLogin") == true {

            let device = (advertisementData as NSDictionary)
                .object(forKey: CBAdvertisementDataLocalNameKey)
            as? NSString

            // let MACID =


            // //print(macid)
print(devicenameInitial)
            if device?.contains(devicenameInitial) == true
                {

                //print(peripheral.identifier)
                //  let macid = GetmacID(manufacturerData: advertisementData["kCBAdvDataManufacturerData"] as! Data)

                let DeviceString = "\((device)!)"
                if DeviceString == peripheral.name {

                    //print("Device Name Initial - \(devicenameInitial)")
                    // dump(peripheral)
                    //print(DeviceString)
                    //print(peripheral.name!)

                    //print("Device Name Initial - \(devicenameInitial)")
                    //print("Device - \((peripheral.name)!)")
                    BIBOPeripheral = peripheral
                    BIBOPeripheral.delegate = self

                    //print("Device name check 1 \((BIBOPeripheral.name)!)")
                    let reactivationTicket = realm.objects(TicketRealmModelNewNew.self).filter("Reactivatecount > 0")
                    self.reactivationTicketcount = reactivationTicket.count
                    
                    let activateTicket = realm.objects(TicketRealmModelNewNew.self).filter("Status == 2")
            
                    self.Active_Ticket_Count = activateTicket.count

                    let ValidateTicket = realm.objects(TicketRealmModelNewNew.self).filter("Status == 3")
                    //print(ValidateTicket.count)
                    //print("Offline validate saveditem beacon_db")
                    self.Validated_ticket_count = ValidateTicket.count

                    //print("Ticket Count Active - \((self.Active_Ticket_Count)), Validate - \((self.Validated_ticket_count))")

                    
                    if !SOSandSTOP.SOSEnable && !SOSandSTOP.StopRequest {
                    
                    
                    if self.Active_Ticket_Count == 0 && self.Validated_ticket_count == 0 {
                        //print("no ticket")


                        if BIBOPeripheral.name! != beaconData.BIBO_IN {
                            self.devicenameInitial = beaconData.BIBO_IN
                            centralManager.stopScan()
                            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)

                        }
                        else {
                            centralManager.stopScan()
                            TicketStatusFor = "Illegal"
                            centralManager.connect(BIBOPeripheral)
                        }

                    }
                    else if self.Active_Ticket_Count == 0 && self.Validated_ticket_count > 0 {

                        if BIBOPeripheral.name! != beaconData.BIBO_OUT {
                            self.devicenameInitial = beaconData.BIBO_OUT
                            centralManager.stopScan()
                            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)
                        }
                        else {
                            centralManager.stopScan()
                            if ObeaconFirstTimeBool {
                                ObeaconFirstTimeBool = false
                                self.TicketStatusFor = "Validated"
                                self.centralManager.connect(self.BIBOPeripheral)
                            }
                            else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 15)
                                {

                                    self.TicketStatusFor = "Validated"
                                    self.centralManager.connect(self.BIBOPeripheral)
                                }
                            }


                        }
                    }
                    else if self.Active_Ticket_Count > 0 && self.Validated_ticket_count == 0 {
                        
                        if reactivationTicketcount == 0{
                        

                        if BIBOPeripheral.name! != beaconData.BIBO_IN {
                            self.devicenameInitial = beaconData.BIBO_IN
                            centralManager.stopScan()
                            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)

                        }
                        else {
                            ObeaconFirstTimeBool = true
                            centralManager.stopScan()
                            TicketStatusFor = "Active"
                            centralManager.connect(BIBOPeripheral)
                        }
                        }
                        else{
                            print(NewVarReactivationcount)
                            if NewVarReactivationcount > 5
                            {
                                if BIBOPeripheral.name! != beaconData.BIBO_OUT {
                                                           self.devicenameInitial = beaconData.BIBO_OUT
                                                           centralManager.stopScan()
                                                           self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)
                                                       }
                                                       else {
                                                           centralManager.stopScan()
                                                           if ObeaconFirstTimeBool {
                                                            NewVarReactivationcountFirst = 0
                                                            NewVarReactivationcount = 0
                                                               ObeaconFirstTimeBool = false
                                                               self.TicketStatusFor = "Validated"
                                                               self.centralManager.connect(self.BIBOPeripheral)
                                                           }
                                                           else {
                                                               DispatchQueue.main.asyncAfter(deadline: .now() + 15)
                                                               {
                                                                self.NewVarReactivationcountFirst = 0
                                                                self.NewVarReactivationcount = 0
                                                                   self.TicketStatusFor = "Validated"
                                                                   self.centralManager.connect(self.BIBOPeripheral)
                                                               }
                                                           }


                                                       }
                                
                                 
                            }
                            else{
                                if BIBOPeripheral.name! != beaconData.BIBO_IN {
                                    self.devicenameInitial = beaconData.BIBO_IN
                                    centralManager.stopScan()
                                    self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)
                                    
                                    NewVarReactivationcount = NewVarReactivationcount + 1


                                }
                                else {
                                    ObeaconFirstTimeBool = true
                                    centralManager.stopScan()
                                    TicketStatusFor = "Active"
                                    centralManager.connect(BIBOPeripheral)
                                    NewVarReactivationcount = 6

                                }
                                
                            }
                            
                           // reactivationTicketcount
                            
                            
                            
                            
                        }

                    }
                    else if self.Active_Ticket_Count > 0 && self.Validated_ticket_count > 0 {
                        
                        if BIBOPeripheral.name! != beaconData.BIBO_IN {
                            self.devicenameInitial = beaconData.BIBO_IN
                            centralManager.stopScan()
                            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)

                        }
                        else {
                            ObeaconFirstTimeBool = true
                            centralManager.stopScan()
                            TicketStatusFor = "Active"
                            centralManager.connect(BIBOPeripheral)
                        }
                    }
                    
                    }
                    else{
                        
                        
                        if BIBOPeripheral.name! != beaconData.BIBO_OUT {
                            self.devicenameInitial = beaconData.BIBO_OUT
                            centralManager.stopScan()
                            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)

                        }
                        else {
                            ObeaconFirstTimeBool = true
                            centralManager.stopScan()
                            TicketStatusFor = "SOS&Stop"
                            centralManager.connect(BIBOPeripheral)
                        }

                        
                        
                    }
                    
                }
                else {
                    //print("Error code: 2123. Please contact TARC")

                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController

                        }
                        topController.view.makeToast("Error code: 2123. Please contact TARC")
                        //topController

                        // topController should now be your topmost view controller
                    }


                    centralManager.stopScan()
                    self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)

                }
            }
            else {

                //print("reloadd********************")
print("\(SOSandSTOP.SOSEnable) \(SOSandSTOP.StopRequest)" )
 if !SOSandSTOP.SOSEnable && !SOSandSTOP.StopRequest {
                if self.Active_Ticket_Count == 0 && self.Validated_ticket_count == 0 {
                    //print("no ticket")


                    self.devicenameInitial = beaconData.BIBO_IN

                }
                else if self.Active_Ticket_Count == 0 && self.Validated_ticket_count > 0 {

                    self.devicenameInitial = beaconData.BIBO_OUT
                    if TicketDownload == nil {
                        
                    TicketDownload = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(TicketDownloadData), userInfo: nil, repeats: true)
                    }

                }
                else if self.Active_Ticket_Count > 0 && self.Validated_ticket_count == 0 {
                    
                    if reactivationTicketcount == 0{
                        self.devicenameInitial = beaconData.BIBO_IN
                    }
                    else{
                        
                        if NewVarReactivationcountFirst > 5
                                                   {

                                                    self.devicenameInitial = beaconData.BIBO_OUT
                                                    print(NewVarReactivationcountFirst)
                                                   // NewVarReactivationcount = 0
                                                                                  

                                                       
                                                        
                                                   }
                                                   else{
                            self.devicenameInitial = beaconData.BIBO_IN
                                                       
                                                           
                                                           NewVarReactivationcountFirst = NewVarReactivationcountFirst + 1


                                                       }

                                                       
                                                   
                        
                    }
                    
                   

                }
                else if self.Active_Ticket_Count > 0 && self.Validated_ticket_count > 0 {
                    self.devicenameInitial = beaconData.BIBO_IN
                    if TicketDownload == nil {
                                       
                                   TicketDownload = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(TicketDownloadData), userInfo: nil, repeats: true)
                                   }
                }



                }
 else {
    
      self.devicenameInitial = beaconData.BIBO_OUT
    
    
    
    }



                // Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)
                // devicenameInitial = "BIBO 1.1"





                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController

                    }




                    let currentPage = UserDefaults.standard.bool(forKey: "TickViewController")
                    if currentPage == true
                        {
                       // topController.view.makeToast("Scanning \(devicenameInitial)")

                    }
                    //topController

                    // topController should now be your topmost view controller
                }

                centralManager.stopScan()
                StartBluetoothScan()
            }


        }
        else {
            //print("Waiting for Login connection")
            centralManager.stopScan()
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)


        }
    }
    @objc func TicketDownloadData(){
        TicketSave.TicketDataUpload(CountForRow: 0, Expiry: true, completion: { (SuccessMessage, Count) in
            if SuccessMessage {

                print("-ChangeAPI No CALL Success push")

                print("saving ...... again every 30 min *******************************************************************")
                
                
                if var topController = UIApplication.shared.keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController

                    }
                  //  topController.view.makeToast("Data Send", position: .top)
                    //topController

                    // topController should now be your topmost view controller
                }
                
            }
        })

        
        
    }
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        // toast.sailAwayLabelAction()

        //print("connect!")
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController

            }
            topController.view.makeToast("\(BIBOPeripheral.name!) Connected")
            //topController

            // topController should now be your topmost view controller
        }

        getlatlongvalue()

        BIBOPeripheral.discoverServices(nil)




    }
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        //print("disconnect Co \(devicenameInitial)")
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController

            }
            //topController.view.makeToast("\(peripheral.name!) Disconnected")
            //topController

            // topController should now be your topmost view controller
        }

        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(StartBluetoothScan), userInfo: nil, repeats: false)
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }

        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
        }
        ////print(services)
    }
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService,
        error: Error?) {
        if ((error) != nil) {
            //print("Error discovering services: \(error!.localizedDescription)")
            return
        }

        guard let characteristics = service.characteristics else {
            return
        }

        //  //print("Found \(characteristics.count) characteristics!")

        for characteristic in characteristics {
            //looks for the right characteristic

            if characteristic.uuid.isEqual(BLE_Characteristic_uuid_Rx) {
                rxCharacteristic = characteristic

                //Once found, subscribe to the this particular characteristic...
                peripheral.setNotifyValue(true, for: rxCharacteristic!)
                // We can return after calling CBPeripheral.setNotifyValue because CBPeripheralDelegate's
                // didUpdateNotificationStateForCharacteristic method will be called automatically
                peripheral.readValue(for: characteristic)
                // //print("Rx Characteristic: \(characteristic.uuid)")
            }
            if characteristic.uuid.isEqual(BLE_Characteristic_uuid_Tx) {
                txCharacteristic = characteristic
                //  //print("Tx Characteristic: \(characteristic.uuid)")
            }
            if characteristic.value != nil {
              //  print(characteristic.value!)
            peripheral.discoverDescriptors(for: characteristic)
            }
        }


    }

    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        //print("Testing \(illegalEntryFlag)")
        if BIBOPeripheral != nil {

            if characteristic == rxCharacteristic {
                dump(characteristic)
                //  print(characteristic.value)
                if characteristic.value != nil
                {
                    // if let ASCIIstring = String(data: characteristic.value!, encoding: .utf8) {
                    //String.Encoding.utf8
                    
                    if let ASCIIstring = String(data: characteristic.value!, encoding: String.Encoding.utf8) {
                        characteristicASCIIValue = ASCIIstring
                        print("Value Recieved: \((characteristicASCIIValue as String))")
                        let valueRecieved = (characteristicASCIIValue as String).split(separator: "#")
                        //print(valueRecieved)
                        
                        if valueRecieved.count > 1
                        {
                            BeaconMacID = "\(valueRecieved[1])"
                            BeaconName = "\(valueRecieved[0])"
                            if BeaconName == "O" {
                            beaconData.CurrentBeaconID = BeaconMacID
                            print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                            print(beaconData.CurrentBeaconID)
                               print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                            print(SOSandSTOP.SOSEnable)
                                   print(SOSandSTOP.StopRequest)
                            }
                          //  if !SOSandSTOP.SOSEnable && !SOSandSTOP.StopRequest {
                            
                            
                            if BeaconName == "A" && TicketStatusFor == "Illegal" && illegalEntryFlag {
                                stopTimer()
                                illegalEntryFlag = false
                                checkFirstDataPush = true
                                
                                OfflineTicketVerifyApi()
                                
                            }
                            else if BeaconName == "A" && TicketStatusFor == "Active" {
                                
                                stopTimer()
                                OfflineTicketVerifyApi()
                                
                                checkFirstDataPush = true
                                
                                
                            }
                            else if BeaconName == "O" && TicketStatusFor == "Validated" {
                                //print("Validate Ticket Pass data to online ")
                                
                                
                                OfflineTicketVerifyApiOBeacon()
                                
                                
                                
                                
                            }
                            
                            else if BeaconName == "O" && TicketStatusFor == "SOS&Stop" {
                                    if SOSandSTOP.SOSEnable {
                                        let userDefaults1 = UserDefaults.standard
                                        let useridSOS:String = String(userDefaults1.integer(forKey: "userIdString"))
                                        print(useridSOS)
                                        //userIdString
                                                           let dataString = "sos\(useridSOS)"
                                        print(dataString)
                                                           let valueString = (dataString as NSString).data(using: String.Encoding.utf8.rawValue)
                                                           self.BIBOPeripheral.writeValue(valueString!, for: self.txCharacteristic!, type: CBCharacteristicWriteType.withResponse)
                                        SOSandSTOP.SOSEnable = false
                                        
                                         DispatchQueue.main.asyncAfter(deadline: .now() + 1)
                                                        {
                                                         self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                                                        }
                                        
                                        
                                    }
                                    else if SOSandSTOP.StopRequest {
                                       
                                                           let dataString = "stop"
                                        print(dataString)
                                                           let valueString = (dataString as NSString).data(using: String.Encoding.utf8.rawValue)
                                                           self.BIBOPeripheral.writeValue(valueString!, for: self.txCharacteristic!, type: CBCharacteristicWriteType.withResponse)
                                        SOSandSTOP.StopRequest = false
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1)
                                                       {
                                                        self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                                                       }
                                        
                                    }
                                    else{
                                        stopTimer()
                                        self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                                    }
                                
                                    
                            }
                            else {
                                //print("No data from here")
                                //print(BeaconName)
                                //print(TicketStatusFor!)
                                //print(illegalEntryFlag)
                                stopTimer()
                                self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                            }
                            
                       // }
                        
                        }
                    }
                    
                }
                else
                {
                    // Its should not happend
                    if var topController = UIApplication.shared.keyWindow?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController
                            
                        }
                     //   topController.view.makeToast("Error code: 2025. Please contact TARC")
                        //topController
                        
                        // topController should now be your topmost view controller
                    }
                    // self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                BIBOPeripheral.discoverServices(nil)
                    
                }
            }
        }
    }




    func stopTimer() {

        // let  PushDatad = Timer.scheduledTimer(TicketDownload: 60, target: self, selector: #selector(OffLineDataPush), userInfo: nil, repeats: true)
        if PushData != nil {
            PushData?.invalidate()
            PushData = nil
        }
    }
    func stopTimerDownload() {

           // let  PushDatad = Timer.scheduledTimer(TicketDownload: 60, target: self, selector: #selector(OffLineDataPush), userInfo: nil, repeats: true)
           if TicketDownload != nil {
               TicketDownload?.invalidate()
               TicketDownload = nil
           }
       }
    @objc func OffLineDataPush() {
        //print("Start Data Push to internet")
        let beaconadd: NSMutableArray = []
        if isReachable() {
            let userDefaults1 = UserDefaults.standard
            let userTOken = userDefaults1.value(forKey: "accessToken")
            let realm = try! Realm()
            // if ComValue.internetchecker == true {
            let isUpdated = false
            do
            {

                let objs = realm.objects(ChangeStatusDataNew.self).filter("isUpdated == %@", isUpdated)
                print(realm.objects(ChangeStatusDataNew.self).count)
                print(objs.count)
                print(realm.objects(ChangeStatusDataNew.self))
                print(objs)
                if objs.count > 0 {
                    beaconadd.removeAllObjects()
                    for Onlinedata in objs {

                        let dataID = random(digits: 8)


                        let BeaconAddList: NSMutableDictionary = [:]
                        //  BeaconAddList.setValue(dataID, forKey: "id")
                        BeaconAddList.setValue(Onlinedata.Latitude, forKey: "latitude")
                        BeaconAddList.setValue(Onlinedata.Longitude, forKey: "longitude")
                        BeaconAddList.setValue(Onlinedata.TicketId, forKey: "ticketid")
                        BeaconAddList.setValue("\(Onlinedata.RouteID)", forKey: "routeid")
                        BeaconAddList.setValue("\((userTOken!))", forKey: "accesstoken")
                        BeaconAddList.setValue("\(Onlinedata.Message)", forKey: "message")
                        BeaconAddList.setValue("\(Onlinedata.DateValue)", forKey: "date")
                        BeaconAddList.setValue("\(Onlinedata.BeaconMacID)", forKey: "beaconid")
                        BeaconAddList.setValue("0", forKey: "tripid")
                        BeaconAddList.setValue(false, forKey: "IsDriver")
                        BeaconAddList.setValue(true, forKey: "IsBibo")


                        beaconadd.add(BeaconAddList)
                    }


                    let listDic: NSMutableDictionary = [:]
                    listDic.setValue(beaconadd, forKey: "List")
                    print(listDic)
                    //print(APIUrl.TicketstateChangeURL)
                    let dataJson = try? JSONSerialization.data(withJSONObject: listDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                    //  print(dataJson)
                    let StatusChangeUrl = URL(string: APIUrl.TicketstateChangeURL)!
                    var request = URLRequest(url: StatusChangeUrl)
                    request.httpMethod = HTTPMethod.post.rawValue
                    request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                    request.httpBody = dataJson

                    //print("200 - times - \(Date()) -\n \(APIUrl.TicketstateChangeURL)")
                    Alamofire.request(request).responseObject { (response: DataResponse<statuschangeapiVerify>) in
                        switch response.result {


                        case .success:
                            let Statusmessage = response.result.value
                            if Statusmessage?.Message == "Ok"
                                {
                                //print(JSON(Statusmessage!))

                                TicketSave.TicketDataUpload(CountForRow: 0, Expiry: true, completion: { (SuccessMessage, Count) in
                                    if SuccessMessage {

                                        print("-ChangeAPI CALL Success push")

                                        print("saving ...... again every 30 min *******************************************************************")
                                        
                                        
                                        if var topController = UIApplication.shared.keyWindow?.rootViewController {
                                            while let presentedViewController = topController.presentedViewController {
                                                topController = presentedViewController

                                            }
                                          //  topController.view.makeToast("Data Send", position: .top)
                                            //topController

                                            // topController should now be your topmost view controller
                                        }
                                        
                                    }
                                })

                                //   //print(dataJson as Any)


                                let statusChangeApifilterArray = realm.objects(ChangeStatusDataNew.self).filter("isUpdated == %@", isUpdated)
                                try! realm.write {
                                    for statusChangeApifilter in statusChangeApifilterArray {
                                        statusChangeApifilter.isUpdated = true
                                        //print(statusChangeApifilter.TicketId)
                                        //print("********************updated Push ")
                                        //print(statusChangeApifilter.isUpdated)


                                    }
                                }

                                do {


                                    let objects = realm.objects(ChangeStatusDataNew.self)
                                    print("Deleted before Count \(objects.count)")
                                    try! realm.write {
                                        realm.delete(objects)
                                        print("Deleted Count \(objects.count)")
                                    }
                                } catch let error as NSError {
                                    // handle error
                                    print("error - \(error.localizedDescription)")
                                }
                                print("Deleted Change status")
                                print(realm.objects(ChangeStatusDataNew.self))

                            }
                            else
                            {
                                print("ERROR Status- \(Statusmessage!.Message!)")
                            }
                        case .failure(let error):
                            print(error)
                        }
                    }





                    //print("Push Done")

                }

            }
        }
        else {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController

                }
                topController.view.makeToast("No Internet connection.  Please turn on WiFi or enable data.", position: .top)
                //topController

                // topController should now be your topmost view controller
            }

        }
    }

    func OfflineTicketVerifyApiOBeacon() {





        let userDefaults1 = UserDefaults.standard
        let userTOken = userDefaults1.value(forKey: "accessToken")
        var isUpdated = false
        //print("Offile")
        getlatlongvalue()
        let Timestamp = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        let utctimeStamp = dateFormatter.string(from: Timestamp)

        let users = realm.objects(BeaconRealmModel.self)
        //  //print(users)
        //print("offline Data Validate \(BeaconMacID)")

        //print(realm.objects(BeaconRealmModel.self).filter("MacID == %@",BeaconMacID).count)

        if realm.objects(BeaconRealmModel.self).filter("MacID == %@", BeaconMacID).count != 0 {
            let MacRealmfilter = realm.objects(BeaconRealmModel.self).filter("MacID == %@", BeaconMacID).first! as BeaconRealmModel
            // //print(ChoseMacID.count)
            // //print(MacRealmfilter)
            RouteID_macID = BeaconMacID

            //print("Offline ChooseMacID \(RouteID_macID!)")

            do
            {

                let objs = realm.objects(TicketRealmModelNewNew.self).filter("(Status == 2 || Status == 3)")

                //print(objs)

                if objs.count == 0
                    {
                    //print("You do not have any active tickets for this bus")


                    DispatchQueue.main.asyncAfter(deadline: .now() + 1)
                    {
                        self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                    }

                }
                else {
                    for realmValue in objs
                    {


                        if (realmValue.Status == 2) || (realmValue.Status == 3)
                            {
                                stopTimerDownload()

                            print(checkFirstDataPush)
                            if checkFirstDataPush {

                                checkFirstDataPush = false
                                if isReachable() {
                                    OffLineDataPush()

                                } else {
                                    checkFirstDataPush = true
                                }



                            }
                            else {
                                if PushData == nil {



                                    PushData = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(OffLineDataPush), userInfo: nil, repeats: true)
                                }
                            }

                            if currentlat != nil && currentLong != nil {
                                let dataID = random(digits: 8)
                                let ChangeStatus = ChangeStatusDataNew()
                                ChangeStatus.id = dataID
                                ChangeStatus.Latitude = currentlat!
                                ChangeStatus.Longitude = currentLong!
                                ChangeStatus.DateValue = utctimeStamp
                                ChangeStatus.TicketId = realmValue.TicketID
                                ChangeStatus.RouteID = realmValue.RouteID
                                ChangeStatus.Message = "OUT"
                                ChangeStatus.BeaconMacID = BeaconMacID
                                ChangeStatus.isUpdated = false
                                try! self.realm.write {

                                    self.realm.add(ChangeStatus, update: .all)
                                    //print("write Completed O Beaconn \(Date())")
                                }


                            }




                        }



                    }



                    DispatchQueue.main.asyncAfter(deadline: .now() + 1)
                    {
                        self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                    }

                }

            }
            catch {
                //print(error)
                self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
            }

        }

        else {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController

                }
                
                
                
                
                topController.view.makeToast("Error code: 2122. Please contact TARC")
                //topController

                // topController should now be your topmost view controller
            }

            self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
            //GetallBeaconData()
        }


    }
    func OfflineTicketVerifyApi() {





        let userDefaults1 = UserDefaults.standard
        let userTOken = userDefaults1.value(forKey: "accessToken")
        var isUpdated = false
        //print("Offile")
        getlatlongvalue()
        let Timestamp = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        let utctimeStamp = dateFormatter.string(from: Timestamp)

        //print(utctimeStamp)

        let users = realm.objects(BeaconRealmModel.self)
        //  //print(users)
        //print("offline Data Validate \(BeaconMacID)")

        //print(realm.objects(BeaconRealmModel.self).filter("MacID == %@",BeaconMacID).count)

        if realm.objects(BeaconRealmModel.self).filter("MacID == %@", BeaconMacID).count != 0 {
            let MacRealmfilter = realm.objects(BeaconRealmModel.self).filter("MacID == %@", BeaconMacID).first! as BeaconRealmModel
            // //print(ChoseMacID.count)
            //print(MacRealmfilter)
            RouteID_macID = "4"

            //print("Offline ChooseMacID \(RouteID_macID!)")

            do
            {

                let objs = realm.objects(TicketRealmModelNewNew.self).filter("(Status == 2 || Status == 3 || Status == 1)")

                //print(objs)

                if objs.count == 0
                    {
                    //print("You do not have any active tickets for this bus")

                   // let NameFor = userDefaults1.value(forKey: "ProFileNameUser")!
                    let string = "You do not have any active tickets, Please buy a new ticket."
                    let utterance = AVSpeechUtterance(string: string)
                    utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
                    utterance.rate = 0.5

                    let synth = AVSpeechSynthesizer()

                    synth.speak(utterance)
                    let dataString = "1000"
                    let valueString = (dataString as NSString).data(using: String.Encoding.utf8.rawValue)
                    self.BIBOPeripheral.writeValue(valueString!, for: self.txCharacteristic!, type: CBCharacteristicWriteType.withResponse)
                    self.shownotify(Title: "Ticket Status", SubTitle: "No Ticket", Body: "You do not have any active tickets for this bus")
                    if currentlat != nil && currentLong != nil {
                        let DateId = random(digits: 8)
                        let ChangeStatus = ChangeStatusDataNew()
                        ChangeStatus.id = DateId
                        ChangeStatus.Latitude = currentlat!
                        ChangeStatus.Longitude = currentLong!
                        ChangeStatus.DateValue = utctimeStamp
                        ChangeStatus.TicketId = 0
                        ChangeStatus.RouteID = "0"
                        ChangeStatus.Message = "Illegal"
                        ChangeStatus.BeaconMacID = BeaconMacID
                        ChangeStatus.isUpdated = false
                        try! self.realm.write {

                            self.realm.add(ChangeStatus, update: .all)
                            //print("write Completed")
                        }
                    }
                    OffLineDataPush()

                    comVar.status = 0
                    comVar.timer = 0
                    comVar.ticketId = 0
                    comVar.ExpiryDate = ""



                    if let wd = UIApplication.shared.delegate?.window {
                        var vc = wd!.rootViewController
                        if(vc is UINavigationController) {
                            vc = (vc as! UINavigationController).visibleViewController

                        }

                        if(vc is TicketViewController) {
                            //your code
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let destinationViewController = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)

                            //  self.window?.rootViewController?.present(destinationViewController, animated: true, completion: nil)

                        }

                        else {

                            //                                    let ChangeStatusDataNew = self.realm.objects(ChangeStatusDataNew.self)
                            //                                    //print(ChangeStatusDataNew)
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)

                            let destinationViewController = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController

                            self.window?.rootViewController?.present(destinationViewController, animated: true, completion: nil)
                        }
                    }



                    DispatchQueue.main.asyncAfter(deadline: .now() + 1)
                    {
                        self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                    }

                }
                else {
                    var countforPip = ""
                    countforPip.removeAll()
//                    let string = "Your ticket Validated! Thank you!"
//                                         let utterance = AVSpeechUtterance(string: string)
//                                         utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
//                                         utterance.rate = 0.5
//
//                                         let synth = AVSpeechSynthesizer()
//
//                                         synth.speak(utterance)
                    for realmValue in objs
                    {

                        if realmValue.Status == 2
                            {
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
//                                self.PianoC()
//                            }
                         //   let NameFor = userDefaults1.value(forKey: "ProFileNameUser")!
                            countforPip.append("Activated")
                     

                            let OFF_ticketid = realmValue.TicketID
                            //print(OFF_ticketid)
                            //print("Validated V")
                            let dataString = "0011"
                            let valueString = (dataString as NSString).data(using: String.Encoding.utf8.rawValue)
                            self.BIBOPeripheral.writeValue(valueString!, for: self.txCharacteristic!, type: CBCharacteristicWriteType.withResponse)
                            shownotify(Title: "Ticket Status", SubTitle: "Ticket Validate", Body: "Your Ticket #\(realmValue.TicketID) Validated Successfully ")

                            let ticketstatus = realm.objects(TicketRealmModelNewNew.self).filter("TicketID == %@", OFF_ticketid).first
                            try! realm.write {
                                ticketstatus!.Status = 3

                                //print(ticketstatus!.Status)
                            }

                            if currentlat != nil && currentLong != nil {

                                let DateID = random(digits: 8)
                                let ChangeStatus = ChangeStatusDataNew()
                                ChangeStatus.id = DateID
                                ChangeStatus.Latitude = currentlat!
                                ChangeStatus.Longitude = currentLong!
                                ChangeStatus.DateValue = utctimeStamp
                                ChangeStatus.TicketId = realmValue.TicketID
                                ChangeStatus.RouteID = realmValue.RouteID
                                ChangeStatus.Message = "IN"
                                ChangeStatus.BeaconMacID = BeaconMacID
                                ChangeStatus.isUpdated = false
                                try! self.realm.write {

                                    self.realm.add(ChangeStatus, update: .all)
                                    //print("write Completed")
                                }

                            }
                            comVar.status = 3
                            comVar.timer = realmValue.RemainingTime
                            comVar.ticketId = realmValue.TicketID
                            comVar.ExpiryDate = realmValue.ExpriyDate


                            /// let topviewControl = UIWindow.getVisibleViewControllerFrom(UIViewController?)


                            if let wd = UIApplication.shared.delegate?.window {
                                var vc = wd!.rootViewController
                                if(vc is UINavigationController) {
                                    vc = (vc as! UINavigationController).visibleViewController

                                }

                                if(vc is TicketViewController) {
                                    //your code
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
                                   
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)

                                    //  self.window?.rootViewController?.present(destinationViewController, animated: true, completion: nil)

                                }

                                else {

//                                    let ChangeStatusDataNew = self.realm.objects(ChangeStatusDataNew.self)
//                                    //print(ChangeStatusDataNew)
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)

                                    let destinationViewController = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController


                                    self.window?.rootViewController?.present(destinationViewController, animated: true, completion: nil)
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                                }
                            }






                        }
                        else if realmValue.Status == 3
                            {
                            if currentlat != nil && currentLong != nil {

                                let DataID = random(digits: 8)
                                let ChangeStatus = ChangeStatusDataNew()
                                ChangeStatus.id = DataID
                                ChangeStatus.Latitude = currentlat!
                                ChangeStatus.Longitude = currentLong!
                                ChangeStatus.DateValue = utctimeStamp
                                ChangeStatus.TicketId = realmValue.TicketID
                                ChangeStatus.RouteID = realmValue.RouteID
                                ChangeStatus.Message = "OUT"
                                ChangeStatus.BeaconMacID = BeaconMacID
                                ChangeStatus.isUpdated = false
                                try! self.realm.write {

                                    self.realm.add(ChangeStatus, update: .all)
                                    //print("write Completed")
                                }

                            }





                        }
                        else if realmValue.Status == 1 {

                            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                                while let presentedViewController = topController.presentedViewController {
                                    topController = presentedViewController

                                }
                                //  topController.view.makeToast("Please Activate your Ticket", position: .top)
                                //topController

                                // topController should now be your topmost view controller
                            }


                        }


                    }
                    
                    //print(countforPip.count)
                    let counttr: Double = Double(countforPip.count + 3)
                    //print(countforPip)

                    DispatchQueue.main.asyncAfter(deadline: .now() + counttr)
                    {
                        self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
                    }




                }

            }
            catch {
                //print(error)
                self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
            }

        }

        else {

            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController

                }
                topController.view.makeToast("Error code: 2122. Please contact TARC")
                //topController

                // topController should now be your topmost view controller
            }
            self.centralManager.cancelPeripheralConnection(self.BIBOPeripheral)
            //GetallBeaconData()
        }


    }

//    func changeStatusApi(){
//
//
//    }
//
//    @IBAction func PianoC() {
//        do {
//            audioPlayer = try AVAudioPlayer(contentsOf: pianoSound)
//            audioPlayer.play()
//        } catch {
//            // couldn't load file :(
//        }
//    }

    func shownotify(Title: String, SubTitle: String, Body: String) {
        badgecount.append("added")
        let content = UNMutableNotificationContent()

        //adding title, subtitle, body and badge
        content.title = Title
        content.subtitle = SubTitle
        content.body = Body
        content.badge = badgecount.count as NSNumber
        content.sound = UNNotificationSound.default()
        //getting the notification trigger
        //it will be called after 5 seconds
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)

        //getting the notification request
        let request = UNNotificationRequest(identifier: "TicketViewController", content: content, trigger: trigger)

        UNUserNotificationCenter.current().delegate = self

        //adding the notification to notification center
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }



    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            //print("Permission granted: \(granted)")

            guard granted else { return }
            // self.getNotificationSettings()
            // self.shownotify()
        }
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        //displaying the ios local notification when app is in foreground
        completionHandler([.alert, .badge, .sound])
    }
    func random(digits: Int) -> Int {

        let min = Int(pow(Double(10), Double(digits - 1))) - 1
        let max = Int(pow(Double(10), Double(digits))) - 1
        return Int(Range(uncheckedBounds: (min, max)))
    }


       
        
}


extension Int {
    init(_ range: Range<Int>) {
        let delta = range.lowerBound < 0 ? abs(range.lowerBound) : 0
        let min = UInt32(range.lowerBound + delta)
        let max = UInt32(range.upperBound + delta)
        self.init(Int(min + arc4random_uniform(max - min)) - delta)
    }
}
