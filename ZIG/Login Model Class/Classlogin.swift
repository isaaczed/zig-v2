//
//	Classlogin.swift


import Foundation 
import ObjectMapper


class Classlogin : NSObject, NSCoding, Mappable{

	var message : String?
	var token : String?
	var username : String?
	var UserType : Double?
    var UserId : Int?


	class func newInstance(map: Map) -> Mappable?{
		return Classlogin()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		message <- map["Message"]
		token <- map["Token"]
		username <- map["Username"]
		UserType <- map["UserType"]
        UserId <- map["UserId"]

	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "Message") as? String
         token = aDecoder.decodeObject(forKey: "Token") as? String
         username = aDecoder.decodeObject(forKey: "Username") as? String
         UserType = aDecoder.decodeObject(forKey: "isAdmin") as? Double
        UserId = aDecoder.decodeObject(forKey: "UserId") as? Int
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if token != nil{
			aCoder.encode(token, forKey: "Token")
		}
		if username != nil{
			aCoder.encode(username, forKey: "Username")
		}
		if UserType != nil{
			aCoder.encode(UserType, forKey: "UserType")
		}
        if UserId != nil{
            aCoder.encode(UserId, forKey: "UserId")
        }
	}

}
