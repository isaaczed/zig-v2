//
//  ResetPasswordModel.swift
//  ZIG
//
//  Created by Isaac on 20/04/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class ResetPasswordModel: Mappable {
    var Message:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        Message <- map["Message"]
    }
    
    
}
