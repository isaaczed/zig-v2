//
//  DataBaseAutoComplete.swift
//  ZIG
//
//  Created by Isaac on 06/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import ObjectMapper


class DataBaseAutoComplete:Mappable {
    var Message:String?
    var address:[AddressList]?
    var Count:Int?
    
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        Message <- map["Message"]
        Count <- map["MaxCount"]
        address <- map["address"]
       
        
    }
    
    
}
class AddressList: Mappable {
     var p_id:Int?
    var FullAddress:String?
    var category:String?
    var Lat:String?
    var Long:String?
    var placeID:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        p_id <- map["P_ID"]
        FullAddress <- map["FullAddress"]
        category <- map["category"]
        Lat <- map["LAT"]
        Long <- map["LON"]
         placeID <- map["PlaceID"]
    }
    
   

}

class GoogleAutoComplete:Mappable {
    var predictions:[AddressListGoogle]?
    var Status:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        Status <- map["status"]
        predictions <- map["predictions"]
    }
    
    
}
class AddressListGoogle: Mappable {
    var description:String?
    var place_id:String?
    var types:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        description <- map["description"]
        place_id <- map["place_id"]
        types <- map["types"]
        
    }
    
    
}
