//
//  GetGoLouisville.swift
//  ZIG
//
//  Created by Isaac on 21/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import ObjectMapper

class GetGoLouisville: Mappable {
    var EventTitle:String?
    var list:[ListEvent]?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        list <- map["list"]
        EventTitle <- map["EventName"]
    }
    
    
}

class ListEvent: Mappable {
    var latitude:String?
    var longitude:String?
    
    required init?(map: Map) {
       
    }
    
     func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
    
    
}
