//
//  walkingResponse.swift
//  ZIG
//
//  Created by VC on 22/01/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import ObjectMapper

class SuggestWalkingDirectionalClass:Mappable {
    var route : [SuggestWalking]?
    var status : String?
    var geocoded_waypoints : [geocoded_waypointsWalking]?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        route <- map["routes"]
        status <- map["status"]
        geocoded_waypoints <- map["geocoded_waypoints"]
    }
    

}
class geocoded_waypointsWalking:Mappable{
    var geocoder_status:String?
    var place_id:String?
    
    required init?(map: Map) {
        }
    
     func mapping(map: Map) {
        geocoder_status <- map["geocoder_status"]
        place_id <- map["place_id"]
        
    }
    
    
}
class SuggestWalking: Mappable {
    var bounds : SuggestwalkingBound?
    var copyrights : String?
    var legs : [SuggestwalkingLeg]?
    var overviewPolyline : SuggestwalkingPolyline?
    var summary : String?
    var warnings : String?
    var waypointOrder : [AnyObject]?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        bounds <- map["bounds"]
         copyrights <- map["copyrights"]
         legs <- map["legs"]
         overviewPolyline <- map["overview_polyline"]
         summary <- map["summary"]
         warnings <- map["warnings"]
         waypointOrder <- map["waypointOrder"]
    }
    
    
}

class SuggestwalkingBound: Mappable {
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
    }
    
    
}
class SuggestwalkingLeg: Mappable {
    var start_address : String?
    var end_address : String?
    var distance : SuggestWalkingDistance?
    var duration : SuggestwalkingDuration?
    var steps : [SuggestwalkingStep]?
    var start_location : SuggestWalkingNortheast?
    var end_location:SuggestWalkingNortheast?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        distance <- map["distance"]
        duration <- map["duration"]
        steps <- map["steps"]
        start_address <- map["start_address"]
        end_address <- map["end_address"]
        start_location <- map["start_location"]
        end_location <- map["end_location"]
        
        
    }
    
    
}
class SuggestWalkingDistance: Mappable {
    var text : String?
    var value : Int?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
    
    
}
class SuggestwalkingStep: Mappable {
    var distance : SuggestWalkingDistance?
    var duration : SuggestwalkingDuration?
    var endLocation : SuggestWalkingNortheast?
    var htmlInstructions : String?
    var polyline : SuggestwalkingPolyline?
    var startLocation : SuggestWalkingNortheast?
    var travelMode : String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        distance <- map["distance"]
        duration <- map["duration"]
        endLocation <- map["end_location"]
        htmlInstructions <- map["html_instructions"]
        startLocation <- map["start_location"]
        travelMode <- map["travel_mode"]
        
    }
    
    
}
class SuggestwalkingPolyline: Mappable {
    var Points:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        Points <- map["points"]
    }
    
    
}
class SuggestWalkingNortheast: Mappable {
    
    
    var lat : Double?
    var lng : Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
    
}
class SuggestwalkingDuration: Mappable {
    var text : String?
    var value : Int?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
    
    
}
