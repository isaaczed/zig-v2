//
//	RealTimeBusClass.swift
//
//	Create by Isaac on 27/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class RealTimeBusClass : NSObject, NSCoding, Mappable{

	var arrivalTime : AnyObject?
	var blockID : AnyObject?
	var busStops : AnyObject?
	var departureTime : AnyObject?
	var eTAminutes : AnyObject?
	var linelist : [AnyObject]?
	var nextstops : [AnyObject]?
	var realtime : Bool?
	var resetTripFlag : Bool?
	var routeID : AnyObject?
	var routeShortName : AnyObject?
	var staticArrivalTime : AnyObject?
	var staticDepartureTime : AnyObject?
	var stopID : AnyObject?
	var stopSequence : Int?
	var timeTravelled : AnyObject?
	var tripStatus : AnyObject?
	var alert : AnyObject?
	var currStatus : AnyObject?
	var lat : Double?
	var lng : Double?
	var routeColor : AnyObject?
	var status : Bool?
	var tripID : Int?
	var tripTitle : AnyObject?
	var vehicle : Bool?


	class func newInstance(map: Map) -> Mappable?{
		return RealTimeBusClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		arrivalTime <- map["ArrivalTime"]
		blockID <- map["BlockID"]
		busStops <- map["BusStops"]
		departureTime <- map["DepartureTime"]
		eTAminutes <- map["ETAminutes"]
		linelist <- map["Linelist"]
		nextstops <- map["Nextstops"]
		realtime <- map["Realtime"]
		resetTripFlag <- map["ResetTripFlag"]
		routeID <- map["RouteID"]
		routeShortName <- map["RouteShortName"]
		staticArrivalTime <- map["StaticArrivalTime"]
		staticDepartureTime <- map["StaticDepartureTime"]
		stopID <- map["StopID"]
		stopSequence <- map["StopSequence"]
		timeTravelled <- map["TimeTravelled"]
		tripStatus <- map["TripStatus"]
		alert <- map["alert"]
		currStatus <- map["currStatus"]
		lat <- map["lat"]
		lng <- map["lng"]
		routeColor <- map["routeColor"]
		status <- map["status"]
		tripID <- map["tripID"]
		tripTitle <- map["tripTitle"]
		vehicle <- map["vehicle"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         arrivalTime = aDecoder.decodeObject(forKey: "ArrivalTime") as? AnyObject
         blockID = aDecoder.decodeObject(forKey: "BlockID") as? AnyObject
         busStops = aDecoder.decodeObject(forKey: "BusStops") as? AnyObject
         departureTime = aDecoder.decodeObject(forKey: "DepartureTime") as? AnyObject
         eTAminutes = aDecoder.decodeObject(forKey: "ETAminutes") as? AnyObject
         linelist = aDecoder.decodeObject(forKey: "Linelist") as? [AnyObject]
         nextstops = aDecoder.decodeObject(forKey: "Nextstops") as? [AnyObject]
         realtime = aDecoder.decodeObject(forKey: "Realtime") as? Bool
         resetTripFlag = aDecoder.decodeObject(forKey: "ResetTripFlag") as? Bool
         routeID = aDecoder.decodeObject(forKey: "RouteID") as? AnyObject
         routeShortName = aDecoder.decodeObject(forKey: "RouteShortName") as? AnyObject
         staticArrivalTime = aDecoder.decodeObject(forKey: "StaticArrivalTime") as? AnyObject
         staticDepartureTime = aDecoder.decodeObject(forKey: "StaticDepartureTime") as? AnyObject
         stopID = aDecoder.decodeObject(forKey: "StopID") as? AnyObject
         stopSequence = aDecoder.decodeObject(forKey: "StopSequence") as? Int
         timeTravelled = aDecoder.decodeObject(forKey: "TimeTravelled") as? AnyObject
         tripStatus = aDecoder.decodeObject(forKey: "TripStatus") as? AnyObject
         alert = aDecoder.decodeObject(forKey: "alert") as? AnyObject
         currStatus = aDecoder.decodeObject(forKey: "currStatus") as? AnyObject
         lat = aDecoder.decodeObject(forKey: "lat") as? Double
         lng = aDecoder.decodeObject(forKey: "lng") as? Double
         routeColor = aDecoder.decodeObject(forKey: "routeColor") as? AnyObject
         status = aDecoder.decodeObject(forKey: "status") as? Bool
         tripID = aDecoder.decodeObject(forKey: "tripID") as? Int
         tripTitle = aDecoder.decodeObject(forKey: "tripTitle") as? AnyObject
         vehicle = aDecoder.decodeObject(forKey: "vehicle") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if arrivalTime != nil{
			aCoder.encode(arrivalTime, forKey: "ArrivalTime")
		}
		if blockID != nil{
			aCoder.encode(blockID, forKey: "BlockID")
		}
		if busStops != nil{
			aCoder.encode(busStops, forKey: "BusStops")
		}
		if departureTime != nil{
			aCoder.encode(departureTime, forKey: "DepartureTime")
		}
		if eTAminutes != nil{
			aCoder.encode(eTAminutes, forKey: "ETAminutes")
		}
		if linelist != nil{
			aCoder.encode(linelist, forKey: "Linelist")
		}
		if nextstops != nil{
			aCoder.encode(nextstops, forKey: "Nextstops")
		}
		if realtime != nil{
			aCoder.encode(realtime, forKey: "Realtime")
		}
		if resetTripFlag != nil{
			aCoder.encode(resetTripFlag, forKey: "ResetTripFlag")
		}
		if routeID != nil{
			aCoder.encode(routeID, forKey: "RouteID")
		}
		if routeShortName != nil{
			aCoder.encode(routeShortName, forKey: "RouteShortName")
		}
		if staticArrivalTime != nil{
			aCoder.encode(staticArrivalTime, forKey: "StaticArrivalTime")
		}
		if staticDepartureTime != nil{
			aCoder.encode(staticDepartureTime, forKey: "StaticDepartureTime")
		}
		if stopID != nil{
			aCoder.encode(stopID, forKey: "StopID")
		}
		if stopSequence != nil{
			aCoder.encode(stopSequence, forKey: "StopSequence")
		}
		if timeTravelled != nil{
			aCoder.encode(timeTravelled, forKey: "TimeTravelled")
		}
		if tripStatus != nil{
			aCoder.encode(tripStatus, forKey: "TripStatus")
		}
		if alert != nil{
			aCoder.encode(alert, forKey: "alert")
		}
		if currStatus != nil{
			aCoder.encode(currStatus, forKey: "currStatus")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lng != nil{
			aCoder.encode(lng, forKey: "lng")
		}
		if routeColor != nil{
			aCoder.encode(routeColor, forKey: "routeColor")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if tripID != nil{
			aCoder.encode(tripID, forKey: "tripID")
		}
		if tripTitle != nil{
			aCoder.encode(tripTitle, forKey: "tripTitle")
		}
		if vehicle != nil{
			aCoder.encode(vehicle, forKey: "vehicle")
		}

	}

}
