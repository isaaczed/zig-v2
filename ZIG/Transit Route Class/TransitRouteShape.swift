//
//	TransitRouteShape.swift


import Foundation 
import ObjectMapper


class TransitRouteShape : NSObject, NSCoding, Mappable{

	var distance : Int?
	var lat : String?
	var longField : String?
	var seq : AnyObject?
	var shapeId : String?


	class func newInstance(map: Map) -> Mappable?{
		return TransitRouteShape()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		distance <- map["Distance"]
		lat <- map["Lat"]
		longField <- map["Long"]
		seq <- map["Seq"]
		shapeId <- map["ShapeId"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         distance = aDecoder.decodeObject(forKey: "Distance") as? Int
         lat = aDecoder.decodeObject(forKey: "Lat") as? String
         longField = aDecoder.decodeObject(forKey: "Long") as? String
         seq = aDecoder.decodeObject(forKey: "Seq") as? AnyObject
         shapeId = aDecoder.decodeObject(forKey: "ShapeId") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if distance != nil{
			aCoder.encode(distance, forKey: "Distance")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "Lat")
		}
		if longField != nil{
			aCoder.encode(longField, forKey: "Long")
		}
		if seq != nil{
			aCoder.encode(seq, forKey: "Seq")
		}
		if shapeId != nil{
			aCoder.encode(shapeId, forKey: "ShapeId")
		}

	}

}