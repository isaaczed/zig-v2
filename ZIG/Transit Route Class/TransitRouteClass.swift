//
//	TransitRouteClass.swift


import Foundation 
import ObjectMapper


class TransitRouteClass : NSObject, NSCoding, Mappable{

	var triplist : [TransitRouteTriplist]?
    var message : String?


	class func newInstance(map: Map) -> Mappable?{
		return TransitRouteClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		triplist <- map["triplist"]
        message <- map["Message"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         triplist = aDecoder.decodeObject(forKey: "triplist") as? [TransitRouteTriplist]
        message = aDecoder.decodeObject(forKey: "Message") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if triplist != nil{
			aCoder.encode(triplist, forKey: "triplist")
		}
        if message != nil{
            aCoder.encode(message, forKey: "Message")
        }

	}

}
