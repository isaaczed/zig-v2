//
//	TransitRouteTriplist.swift


import Foundation 
import ObjectMapper


class TransitRouteTriplist : NSObject, NSCoding, Mappable{

	var arrivalStop : String?
	var arrivalTime : String?
	var blockID : String?
	var departureStop : String?
	var departureTime : String?
	var directionID : Int?
	var routeColor : String?
	var shapeID : String?
	var shapes : [TransitRouteShape]?
	var travel : String?
	var tripID : String?
    

	class func newInstance(map: Map) -> Mappable?{
		return TransitRouteTriplist()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		arrivalStop <- map["ArrivalStop"]
		arrivalTime <- map["ArrivalTime"]
		blockID <- map["BlockID"]
		departureStop <- map["DepartureStop"]
		departureTime <- map["DepartureTime"]
		directionID <- map["DirectionID"]
		routeColor <- map["RouteColor"]
		shapeID <- map["ShapeID"]
		shapes <- map["Shapes"]
		travel <- map["Travel"]
		tripID <- map["TripID"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         arrivalStop = aDecoder.decodeObject(forKey: "ArrivalStop") as? String
         arrivalTime = aDecoder.decodeObject(forKey: "ArrivalTime") as? String
         blockID = aDecoder.decodeObject(forKey: "BlockID") as? String
         departureStop = aDecoder.decodeObject(forKey: "DepartureStop") as? String
         departureTime = aDecoder.decodeObject(forKey: "DepartureTime") as? String
         directionID = aDecoder.decodeObject(forKey: "DirectionID") as? Int
         routeColor = aDecoder.decodeObject(forKey: "RouteColor") as? String
         shapeID = aDecoder.decodeObject(forKey: "ShapeID") as? String
         shapes = aDecoder.decodeObject(forKey: "Shapes") as? [TransitRouteShape]
         travel = aDecoder.decodeObject(forKey: "Travel") as? String
         tripID = aDecoder.decodeObject(forKey: "TripID") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if arrivalStop != nil{
			aCoder.encode(arrivalStop, forKey: "ArrivalStop")
		}
		if arrivalTime != nil{
			aCoder.encode(arrivalTime, forKey: "ArrivalTime")
		}
		if blockID != nil{
			aCoder.encode(blockID, forKey: "BlockID")
		}
		if departureStop != nil{
			aCoder.encode(departureStop, forKey: "DepartureStop")
		}
		if departureTime != nil{
			aCoder.encode(departureTime, forKey: "DepartureTime")
		}
		if directionID != nil{
			aCoder.encode(directionID, forKey: "DirectionID")
		}
		if routeColor != nil{
			aCoder.encode(routeColor, forKey: "RouteColor")
		}
		if shapeID != nil{
			aCoder.encode(shapeID, forKey: "ShapeID")
		}
		if shapes != nil{
			aCoder.encode(shapes, forKey: "Shapes")
		}
		if travel != nil{
			aCoder.encode(travel, forKey: "Travel")
		}
		if tripID != nil{
			aCoder.encode(tripID, forKey: "TripID")
		}

	}

}
