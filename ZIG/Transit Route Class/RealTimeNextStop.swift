//
//    RealTimeNextStop.swift


import Foundation
import ObjectMapper


class RealTimeNextStop : NSObject, NSCoding, Mappable{
    
    var arrivalTime : String?
    var eTA : String?
    var stopDistance : String?
    var stopId : AnyObject?
    var stopName : String?
    var status : Bool?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return RealTimeNextStop()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        arrivalTime <- map["ArrivalTime"]
        eTA <- map["ETA"]
        stopDistance <- map["StopDistance"]
        stopId <- map["StopId"]
        stopName <- map["StopName"]
        status <- map["status"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        arrivalTime = aDecoder.decodeObject(forKey: "ArrivalTime") as? String
        eTA = aDecoder.decodeObject(forKey: "ETA") as? String
        stopDistance = aDecoder.decodeObject(forKey: "StopDistance") as? String
        stopId = aDecoder.decodeObject(forKey: "StopId") as? AnyObject
        stopName = aDecoder.decodeObject(forKey: "StopName") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Bool
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if arrivalTime != nil{
            aCoder.encode(arrivalTime, forKey: "ArrivalTime")
        }
        if eTA != nil{
            aCoder.encode(eTA, forKey: "ETA")
        }
        if stopDistance != nil{
            aCoder.encode(stopDistance, forKey: "StopDistance")
        }
        if stopId != nil{
            aCoder.encode(stopId, forKey: "StopId")
        }
        if stopName != nil{
            aCoder.encode(stopName, forKey: "StopName")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}
