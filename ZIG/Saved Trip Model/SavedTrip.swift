//
//	SavedTrip.swift


import Foundation 
import ObjectMapper


class SavedTrip : NSObject, NSCoding, Mappable{

	var destinationAddress : String?
	var destinationLat : Double?
	var destinationLong : Double?
	var savedTripID : Double?
	var sourceAddress : String?
	var sourceLat : Double?
	var sourceLong : Double?
	var userId : Double?


	class func newInstance(map: Map) -> Mappable?{
		return SavedTrip()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		destinationAddress <- map["DestinationAddress"]
		destinationLat <- map["DestinationLat"]
		destinationLong <- map["DestinationLong"]
		savedTripID <- map["SavedTripID"]
		sourceAddress <- map["SourceAddress"]
		sourceLat <- map["SourceLat"]
		sourceLong <- map["SourceLong"]
		userId <- map["UserId"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         destinationAddress = aDecoder.decodeObject(forKey: "DestinationAddress") as? String
         destinationLat = aDecoder.decodeObject(forKey: "DestinationLat") as? Double
         destinationLong = aDecoder.decodeObject(forKey: "DestinationLong") as? Double
         savedTripID = aDecoder.decodeObject(forKey: "SavedTripID") as? Double
         sourceAddress = aDecoder.decodeObject(forKey: "SourceAddress") as? String
         sourceLat = aDecoder.decodeObject(forKey: "SourceLat") as? Double
         sourceLong = aDecoder.decodeObject(forKey: "SourceLong") as? Double
         userId = aDecoder.decodeObject(forKey: "UserId") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if destinationAddress != nil{
			aCoder.encode(destinationAddress, forKey: "DestinationAddress")
		}
		if destinationLat != nil{
			aCoder.encode(destinationLat, forKey: "DestinationLat")
		}
		if destinationLong != nil{
			aCoder.encode(destinationLong, forKey: "DestinationLong")
		}
		if savedTripID != nil{
			aCoder.encode(savedTripID, forKey: "SavedTripID")
		}
		if sourceAddress != nil{
			aCoder.encode(sourceAddress, forKey: "SourceAddress")
		}
		if sourceLat != nil{
			aCoder.encode(sourceLat, forKey: "SourceLat")
		}
		if sourceLong != nil{
			aCoder.encode(sourceLong, forKey: "SourceLong")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "UserId")
		}

	}

}
