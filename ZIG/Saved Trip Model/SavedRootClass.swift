//
//	SavedRootClass.swift


import Foundation 
import ObjectMapper


class SavedRootClass : NSObject, NSCoding, Mappable{

	var message : String?
	var trips : [SavedTrip]?


	class func newInstance(map: Map) -> Mappable?{
		return SavedRootClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		message <- map["Message"]
		trips <- map["Trips"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "Message") as? String
         trips = aDecoder.decodeObject(forKey: "Trips") as? [SavedTrip]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if trips != nil{
			aCoder.encode(trips, forKey: "Trips")
		}

	}

}