////
////  HelpViewController.swift
////  ZIG
////
////  Created by Isaac on 08/05/19.
////  Copyright © 2019 Isaac. All rights reserved.
////
////
//
//import UIKit
//
//class HelpViewController: UIViewController,  ATCWalkthroughViewControllerDelegate{
//    let walkthroughs = [
//        ATCWalkthroughModel(title: "Trip Planner", subtitle: "Plan trips, view route options, find places to go with the Trip Planner.", icon: "helpTrip-Planner"),
//        ATCWalkthroughModel(title: "Real-Time Schedules", subtitle: "Locate your bus on the map and see it's next stops.", icon: "helpRealTimeSchedules"),
//        ATCWalkthroughModel(title: "System Map ", subtitle: "See all of TARC's busses on the System Map.", icon: "helpSystemMap"),
//        ATCWalkthroughModel(title: "Favorites", subtitle: "Your favorite routes are stored and accessible here.", icon: "helpFavorites"),
//        ATCWalkthroughModel(title: "Trip Planner \nDepart", subtitle: "You can edit your departure time here.", icon: "HelpDepartNow"),
//        ATCWalkthroughModel(title: "Places", subtitle: "Find Restaurants, Banks, City Services, etc", icon: "helpPlaces"),
//        ATCWalkthroughModel(title: "Preferences", subtitle: "Edit preferences for your route options. ", icon: "helpPreferences"),
//           ATCWalkthroughModel(title: "Add to Favorites", subtitle: "Select this to add the current Route to your Favorites.", icon: "AddRouteToFavorites"),
//              ATCWalkthroughModel(title: "Overview", subtitle: "Use this app to navigate the greater Louisville area through all of the transit options the city has to offer. ", icon: "TARCoverview-29"),
//    ]
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let walkthroughVC = self.walkthroughVC()
//        walkthroughVC.delegate = self
//        self.addChildViewControllerWithView(walkthroughVC)
//        
//        
//        
//       
//        
//    }
//    
//    func walkthroughViewControllerDidFinishFlow(_ vc: ATCWalkthroughViewController) {
//        
//        let userDefaults1 = UserDefaults.standard
//        let accessToken = userDefaults1.value(forKey: "accessToken")
//        print(accessToken ?? nil ?? "no token")
//        if accessToken != nil
//        {
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//             self.navigationController?.pushViewController(nextViewController, animated: true)
//            
//            userDefaults1.set("false", forKey: "showPreferencesOnLogin")
//            
//        }
//        else{
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WithOutLoginMainViewController") as! WithOutLoginMainViewController
//            self.navigationController?.pushViewController(nextViewController, animated: true)
//            
//            userDefaults1.set("false", forKey: "showPreferencesOnLogin")
//        }
//        
//        
//       
//    }
//    
//    fileprivate func walkthroughVC() -> ATCWalkthroughViewController {
//        let viewControllers = walkthroughs.map { ATCClassicWalkthroughViewController(model: $0, nibName: "ATCClassicWalkthroughViewController", bundle: nil) }
//        return ATCWalkthroughViewController(nibName: "ATCWalkthroughViewController",
//                                            bundle: nil,
//                                            viewControllers: viewControllers)
//        
//       
//        
//    }
//}
//
