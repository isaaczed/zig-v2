//
//  FeedBackViewController.swift
//  ZIG
//
//  Created by Isaac on 16/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import NVActivityIndicatorView
import NYAlertViewController
import MessageUI
import Foundation

class SupportVCViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var loaderView = UIView()
    @IBOutlet weak var FullPanel: UIView!
    
    @IBOutlet weak var SendBtn: UIButton!
    @IBOutlet weak var screenshot: UIImageView!
    var imagePicker = UIImagePickerController()
    var profileDataList : DataResponse<GetProfile>!
   var newImage: UIImage?
     var token = ""
    var DocumentURL_API = ""
    @IBOutlet weak var imageUploadBtn: UIButton!
    @IBOutlet weak var feedbackTxt: UITextView!
    var profilename:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        analytics.GetPageHitCount(PageName: "Support")
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.title = "SUPPORT"
        
        
        feedbackTxt.delegate = self
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        
        feedbackTxt.text = "Description"
        feedbackTxt.textColor = UIColor.lightGray
        
        // Do any additional setup after loading the view.
        FullPanel.layer.cornerRadius = 10
        
        FullPanel.center = self.view.center
        //FullPanel.backgroundColor = UIColor.yellow
        FullPanel.layer.shadowColor = UIColor.black.cgColor
        FullPanel.layer.shadowOpacity = 0.6
        FullPanel.layer.shadowOffset = CGSize.zero
        FullPanel.layer.shadowRadius = 5
        imageUploadBtn.titleLabel!.numberOfLines = 0; // Dynamic number of lines
        imageUploadBtn.titleLabel!.lineBreakMode = NSLineBreakMode.byWordWrapping
        SendBtn.layer.cornerRadius = 4
        
        
        let userDefaults1 = UserDefaults.standard
        let accessToken = userDefaults1.value(forKey: "accessToken")
        token = accessToken as! String
        let url = "\(APIUrl.ZIGTARCAPI)User/Profilelist"
        let parameters: Parameters = [
            "token": accessToken!,
            
            ]
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<GetProfile>) in
                switch response.result {
                case .success:
                    if response.result.value?.message == "Ok"
                    {
                        print(response.result.value!)
                        self.profileDataList = response
                        
                    }
                    else
                    {
                       self.view.makeToast(response.result.value?.message)
                    }
                case .failure(let error):
                    print(error)
                  
                }
                
        }
        
//        let contactLabel = UITextView()
//        contactLabel.frame = CGRect.init(x: 0, y: FullPanel.frame.maxY - 80, width: FullPanel.frame.size.width, height: 40)
//        contactLabel.text = "Please contact Customer Support (6145000069) or (helpdesk@zed.digital.net)"
//        contactLabel.textAlignment = .center
//        contactLabel.isEditable = false
//        contactLabel.dataDetectorTypes = UIDataDetectorTypes.all;
     //   FullPanel .addSubview(contactLabel)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
         NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    @IBAction func ImageBtnTap(_ sender: UIButton) {
        
        print("Tapped on Image")
        
        
        print("work")
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
//            // self.openCamera()
//        }))
        
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = (sender as UIView)
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    @objc func OnBackClicked() {
        
                self.navigationController?.popViewController(animated: true)
     
        // self.navigationController?.popViewController(animated: true)
        
        
    }
    
    func openGallary(){
       // self.screenshot.isHidden = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
       // imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if feedbackTxt.textColor == UIColor.lightGray {
            feedbackTxt.text = nil
            feedbackTxt.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if feedbackTxt.text.isEmpty {
           feedbackTxt.text = "Description"
            feedbackTxt.textColor = UIColor.lightGray
        }
    }
    //Process to happen when the user selects the image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMddyyyyhhmm"
         let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        profilename = "\(dateInFormat).jpeg"
        
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
           self.screenshot.image = editedImage
            self.newImage = editedImage
            
             self.imageUploadBtn.setTitle("   \((profilename)!)", for: .normal)
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
     func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func imageError(image: UIImage,didFinishSavingWithError error:NSErrorPointer, contextInfo:UnsafeRawPointer){
        if error != nil{
            let alert = UIAlertController(title: "Save Failed", message: "Failed to save image", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            self.present(alert,animated: true,completion: nil)
        }
    }
    
    @IBAction func SendMail(_ sender: Any) {
        if isReachable()
        {
        
            let userDefaults1 = UserDefaults.standard
            
           if userDefaults1.object(forKey: "userName") == nil || userDefaults1.object(forKey: "userEmail") == nil
           {
            userDefaults1.set("", forKey: "userEmail")
            
            userDefaults1.set("", forKey: "userName")
            }
            else
           {
            
            }
            
        let userprofilename = userDefaults1.object(forKey: "userName") as! NSString
        let emailAddress =  userDefaults1.object(forKey: "userEmail") as! NSString
         self.showLoader()
        var textmessage = feedbackTxt.text
        if textmessage != nil && textmessage != "" && textmessage != "Description"
        {
        
       
        if screenshot?.image != nil {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMddyyyyhhmm"
            let dateInFormat = dateFormatter.string(from: NSDate() as Date)
            
        guard let imageCompress = self.newImage,
            let imageData = UIImageJPEGRepresentation(imageCompress, 0.5) else {
                return
        }
           
            
            print("\(APIUrl.ZIGTARCAPI)User/DocumentUpload?Username=\((userprofilename))&Profilename=\((userprofilename))")
            
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "photo_path", fileName: "\(dateInFormat).jpeg", mimeType: "image/jpeg")
            
        }, to:"\(APIUrl.ZIGTARCAPI)User/DocumentUpload?Username=\((userprofilename))&Profilename=\((userprofilename))")
            
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    dump(response)
                    if let ResultValue = response.result.value as? [String:Any] {
                        let responseMessage = ResultValue["Message"] as? String
                        if (responseMessage == "Ok") {
                            self.DocumentURL_API = (ResultValue["DocPath"]! as? String)!
                            print(self.DocumentURL_API)
                            self.view.makeToast("Your document has been successfully uploaded.")
                            
                            
                                print("******* Connect *****")
                                
                                
                                  let accessToken = UserDefaults.standard.value(forKey: "accessToken")
                                //userEmail
                          //  let emailid = UserDefaults.standard.string(forKey: "userEmail")
                                let dataDic : NSMutableDictionary = [
                                    "token": accessToken!,
                                    "issuetype":"Issue/Support iOS",
                                    "link": self.DocumentURL_API ,
                                    "description": textmessage!,
                                    "Callback":"false",
                                    "emailid":emailAddress,
                                    "agencyid":1
                                    ]
                                
                                let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
                                let url2 = URL(string: "\(APIUrl.ZIGTARCAPI)Feedback/ReportIssue/Add")!
                                var request = URLRequest(url: url2)
                                request.httpMethod = HTTPMethod.post.rawValue
                                request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                                request.httpBody = dataJson
                                
                                
                                Alamofire.request(request)
                                    .responseObject{ (response: DataResponse<Postpayment>) in
                                        switch response.result {
                                        case .success:
                                            dump(response.result.value)
                                            if response.result.value?.message == "Issue added Successfully"  || response.result.value?.message == "Your feedback has been successfully received." {
                                                self.emailsending(Username: userprofilename as String, Email: emailAddress as String, message: textmessage!, Url: self.DocumentURL_API)
                                                self.hideLoader()
                                                textmessage = ""
                                                self.feedbackTxt.text = ""
                                                self.screenshot.image = nil
                                                self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                                               // self.view.makeToast(response.result.value?.message)
                                                
                                                self.hideLoader()
                                                let alertViewController = NYAlertViewController()
                                                alertViewController.title = "Support"
                                                alertViewController.message = "Your feedback has been successfully received."
                                                
                                                // Customize appearance as desired
                                                alertViewController.buttonCornerRadius = 20.0
                                                alertViewController.view.tintColor = self.view.tintColor
                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.swipeDismissalGestureEnabled = true
                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                alertViewController.buttonColor = UIColor.red
                                                // Add alert actions
                                                
                                                
                                                let cancelAction = NYAlertAction(
                                                    title: "OK",
                                                    style: .cancel,
                                                    handler: { (action: NYAlertAction!) -> Void in
                                                       
                                                        
                                                        
                                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                                        
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        
                                                        
                                                        self.dismiss(animated: true, completion: nil)
                                                }
                                                )
                                                
                                                alertViewController.addAction(cancelAction)
                                                
                                                // Present the alert view controller
                                                self.present(alertViewController, animated: true, completion: nil)
                                                
                                                
                                                
                                            }
                                            else{
                                                //print(response.result.value?.message!)
                                                self.hideLoader()
                                                textmessage = ""
                                                self.feedbackTxt.text = ""
                                                self.screenshot.image = nil
                                                self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                                                let alertViewController = NYAlertViewController()
                                                alertViewController.title = "Support"
                                                alertViewController.message = response.result.value?.message
                                                
                                                // Customize appearance as desired
                                                alertViewController.buttonCornerRadius = 20.0
                                                alertViewController.view.tintColor = self.view.tintColor
                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.swipeDismissalGestureEnabled = true
                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                alertViewController.buttonColor = UIColor.red
                                                // Add alert actions
                                                
                                                
                                                let cancelAction = NYAlertAction(
                                                    title: "OK",
                                                    style: .cancel,
                                                    handler: { (action: NYAlertAction!) -> Void in
                                                        
                                                        self.dismiss(animated: true, completion: nil)
                                                }
                                                )
                                                
                                                alertViewController.addAction(cancelAction)
                                                
                                                // Present the alert view controller
                                                self.present(alertViewController, animated: true, completion: nil)
                                            }
                                            
                                             self.hideLoader()
                                            
                                        case .failure(let error):
                                            
                                            print(error)
                                           self.view.makeToast(error.localizedDescription)
                                            self.hideLoader()
                                        }
                                        
                                }
                                
                            
                            
                            
                            
                            
                        }
                        else{
                            self.DocumentURL_API = ""
                            self.view.makeToast("Error: \(responseMessage!)")
                            self.hideLoader()
                            
                            
                        }
                        
                        
                        
                        
                        
//                        let Timestamp = Date()
//                        let dateFormatter = DateFormatter()
//                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//                        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
//                        let utctimeStamp = dateFormatter.string(from: Timestamp)
                        
                        
                    }
                }
                
                
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
                self.DocumentURL_API = ""
                self.view.makeToast("Error: \(encodingError)")
        
            }
        }
    }
        else{
            
            
            let accessToken = UserDefaults.standard.value(forKey: "accessToken")
            
            let dataDic : NSMutableDictionary = [
                "Token": accessToken!,
                "emailid":emailAddress,
                "issuetype":"Issue/Support iOS",
                "Link": self.DocumentURL_API,
                "Description": textmessage!,
                "AgencyId":1
                
                
            ]
            
            let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
            let url2 = URL(string: "\(APIUrl.ZIGTARCAPI)Feedback/ReportIssue/Add")!
            var request = URLRequest(url: url2)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = dataJson
            
            
            Alamofire.request(request)
                .responseObject{ (response: DataResponse<Postpayment>) in
                    switch response.result {
                    case .success:
                        dump(response.result.value)
                        if response.result.value?.message == "Issue added Successfully" || response.result.value?.message == "Your feedback has been successfully received." {
                            
                            self.emailsending(Username: userprofilename as String, Email: emailAddress as String, message: textmessage!, Url: self.DocumentURL_API)
                            self.hideLoader()
                            textmessage = ""
                            self.feedbackTxt.text = ""
                            self.screenshot.image = nil
                            self.imageUploadBtn.setTitle("Upload Screenshot", for: .normal)
                           // self.view.makeToast(response.result.value?.message)
                            
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Support"
                            alertViewController.message = "Your feedback has been successfully received."
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            textmessage = ""
                            self.feedbackTxt.text = ""
                            self.screenshot.image = nil
                            self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    
                                    
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                    
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                    
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                            
                            
                            
                        }
                        else{
                            textmessage = ""
                            self.feedbackTxt.text = ""
                            self.screenshot.image = nil
                            self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                            //print(response.result.value?.message!)
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Support"
                            alertViewController.message = response.result.value?.message
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                        
                        self.hideLoader()
                        
                    case .failure(let error):
                        
                        print(error)
                        self.view.makeToast(error.localizedDescription)
                        self.hideLoader()
                    }
                    //self.sendEmail()
                    
            }
            
            
        }
        }
        else{
            
            self.hideLoader()
            let alertViewController = NYAlertViewController()
            alertViewController.title = "Support"
            alertViewController.message = "Please complete all the fields required."
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            
            
            let cancelAction = NYAlertAction(
                title: "OK",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    
                    self.dismiss(animated: true, completion: nil)
            }
            )
            
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
            
            
            
        }
    }
    else
    {
    self.view.makeToast("No Internet Conenction")
    }
    }
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }


    func emailsending(Username:String,Email:String,message:String,Url:String){
        
        

       // let testmsg = message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        print("\(message)")
        let testmsg = message.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[]{} ").inverted)
let appVersion = getAppVersion() as NSString
        
        let note = "Mobile: \(UIDevice.modelName) \n IP: \((UIDevice.current.ipAddress())!) \n iOS Version:\(UIDevice.current.systemVersion) \n App Version:\(appVersion)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let Url_Reset = "https://www.ridetarc.org/DMI/mail.php?&Message=\((testmsg)!)&url=\(Url)&emailaddress=\(Email)&name=\(Username)&note=\((note)!)"
        print(Url_Reset)
        Alamofire.request(Url_Reset, method: .get,encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<ResetPasswordModel>) in
                switch response.result {
                case .success:
                    print(response)
                    //to get status code
                    
                        self.view.makeToast("Your email has been successfully sent.")
                    print("messageeeeeeeeeeeee")
                        
                    
                   
                case .failure(let error):
                    print(error)
                    
                }
                
        }
        
    }
    
   func getAppVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let versionValue = dictionary["CFBundleShortVersionString"] ?? "0"
        let buildValue = dictionary["CFBundleVersion"] ?? "0"
        return "\(versionValue) (build \(buildValue))"
    }
}

