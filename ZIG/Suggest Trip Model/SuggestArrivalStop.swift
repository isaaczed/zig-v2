//
//	SuggestArrivalStop.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestArrivalStop : NSObject, NSCoding, Mappable{

	var location : SuggestNortheast?
	var name : String?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestArrivalStop()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		location <- map["location"]
		name <- map["name"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         location = aDecoder.decodeObject(forKey: "location") as? SuggestNortheast
         name = aDecoder.decodeObject(forKey: "name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}

	}

}
