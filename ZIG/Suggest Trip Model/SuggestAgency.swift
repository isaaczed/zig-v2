//
//	SuggestAgency.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestAgency : NSObject, NSCoding, Mappable{

	var name : String?
	var phone : String?
	var url : String?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestAgency()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		name <- map["name"]
		phone <- map["phone"]
		url <- map["url"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         name = aDecoder.decodeObject(forKey: "name") as? String
         phone = aDecoder.decodeObject(forKey: "phone") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}

	}

}
