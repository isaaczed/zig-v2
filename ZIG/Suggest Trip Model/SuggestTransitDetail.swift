//
//	SuggestTransitDetail.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestTransitDetail : NSObject, NSCoding, Mappable{

	var arrivalStop : SuggestArrivalStop?
	var arrivalTime : SuggestArrivalTime?
	var departureStop : SuggestArrivalStop?
	var departureTime : SuggestArrivalTime?
	var headsign : String?
	var headway : Int?
	var line : SuggestLine?
	var numStops : Int?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestTransitDetail()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		arrivalStop <- map["arrival_stop"]
		arrivalTime <- map["arrival_time"]
		departureStop <- map["departure_stop"]
		departureTime <- map["departure_time"]
		headsign <- map["headsign"]
		headway <- map["headway"]
		line <- map["line"]
		numStops <- map["num_stops"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         arrivalStop = aDecoder.decodeObject(forKey: "arrival_stop") as? SuggestArrivalStop
         arrivalTime = aDecoder.decodeObject(forKey: "arrival_time") as? SuggestArrivalTime
         departureStop = aDecoder.decodeObject(forKey: "departure_stop") as? SuggestArrivalStop
         departureTime = aDecoder.decodeObject(forKey: "departure_time") as? SuggestArrivalTime
         headsign = aDecoder.decodeObject(forKey: "headsign") as? String
         headway = aDecoder.decodeObject(forKey: "headway") as? Int
         line = aDecoder.decodeObject(forKey: "line") as? SuggestLine
         numStops = aDecoder.decodeObject(forKey: "num_stops") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if arrivalStop != nil{
			aCoder.encode(arrivalStop, forKey: "arrival_stop")
		}
		if arrivalTime != nil{
			aCoder.encode(arrivalTime, forKey: "arrival_time")
		}
		if departureStop != nil{
			aCoder.encode(departureStop, forKey: "departure_stop")
		}
		if departureTime != nil{
			aCoder.encode(departureTime, forKey: "departure_time")
		}
		if headsign != nil{
			aCoder.encode(headsign, forKey: "headsign")
		}
		if headway != nil{
			aCoder.encode(headway, forKey: "headway")
		}
		if line != nil{
			aCoder.encode(line, forKey: "line")
		}
		if numStops != nil{
			aCoder.encode(numStops, forKey: "num_stops")
		}

	}

}
