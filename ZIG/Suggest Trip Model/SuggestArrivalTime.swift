//
//	SuggestArrivalTime.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestArrivalTime : NSObject, NSCoding, Mappable{

	var text : String?
	var timeZone : String?
	var value : Double?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestArrivalTime()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		text <- map["text"]
		timeZone <- map["time_zone"]
		value <- map["value"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         text = aDecoder.decodeObject(forKey: "text") as? String
         timeZone = aDecoder.decodeObject(forKey: "time_zone") as? String
         value = aDecoder.decodeObject(forKey: "value") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if text != nil{
			aCoder.encode(text, forKey: "text")
		}
		if timeZone != nil{
			aCoder.encode(timeZone, forKey: "time_zone")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}
