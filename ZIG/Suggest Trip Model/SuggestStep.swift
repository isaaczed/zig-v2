//
//	SuggestStep.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestStep : NSObject, NSCoding, Mappable{

	var distance : SuggestDistance?
	var duration : SuggestDuration?
	var endLocation : SuggestNortheast?
	var htmlInstructions : String?
	var polyline : SuggestPolyline?
	var startLocation : SuggestNortheast?
	var travelMode : String?
	var steps : [SuggestStep]?
	var transitDetails : SuggestTransitDetail?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestStep()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		distance <- map["distance"]
		duration <- map["duration"]
		endLocation <- map["end_location"]
		htmlInstructions <- map["html_instructions"]
		polyline <- map["polyline"]
		startLocation <- map["start_location"]
		travelMode <- map["travel_mode"]
		steps <- map["steps"]
		transitDetails <- map["transit_details"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         distance = aDecoder.decodeObject(forKey: "distance") as? SuggestDistance
         duration = aDecoder.decodeObject(forKey: "duration") as? SuggestDuration
         endLocation = aDecoder.decodeObject(forKey: "end_location") as? SuggestNortheast
         htmlInstructions = aDecoder.decodeObject(forKey: "html_instructions") as? String
         polyline = aDecoder.decodeObject(forKey: "polyline") as? SuggestPolyline
         startLocation = aDecoder.decodeObject(forKey: "start_location") as? SuggestNortheast
         travelMode = aDecoder.decodeObject(forKey: "travel_mode") as? String
         steps = aDecoder.decodeObject(forKey: "steps") as? [SuggestStep]
         transitDetails = aDecoder.decodeObject(forKey: "transit_details") as? SuggestTransitDetail

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if distance != nil{
			aCoder.encode(distance, forKey: "distance")
		}
		if duration != nil{
			aCoder.encode(duration, forKey: "duration")
		}
		if endLocation != nil{
			aCoder.encode(endLocation, forKey: "end_location")
		}
		if htmlInstructions != nil{
			aCoder.encode(htmlInstructions, forKey: "html_instructions")
		}
		if polyline != nil{
			aCoder.encode(polyline, forKey: "polyline")
		}
		if startLocation != nil{
			aCoder.encode(startLocation, forKey: "start_location")
		}
		if travelMode != nil{
			aCoder.encode(travelMode, forKey: "travel_mode")
		}
		if steps != nil{
			aCoder.encode(steps, forKey: "steps")
		}
		if transitDetails != nil{
			aCoder.encode(transitDetails, forKey: "transit_details")
		}

	}

}
