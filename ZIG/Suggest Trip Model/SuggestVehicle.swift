//
//	SuggestVehicle.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestVehicle : NSObject, NSCoding, Mappable{

	var icon : String?
	var localIcon : AnyObject?
	var name : String?
	var type : String?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestVehicle()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		icon <- map["icon"]
		localIcon <- map["local_icon"]
		name <- map["name"]
		type <- map["type"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         icon = aDecoder.decodeObject(forKey: "icon") as? String
         localIcon = aDecoder.decodeObject(forKey: "local_icon") as? AnyObject
         name = aDecoder.decodeObject(forKey: "name") as? String
         type = aDecoder.decodeObject(forKey: "type") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if icon != nil{
			aCoder.encode(icon, forKey: "icon")
		}
		if localIcon != nil{
			aCoder.encode(localIcon, forKey: "local_icon")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}
