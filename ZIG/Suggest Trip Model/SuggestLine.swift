//
//	SuggestLine.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestLine : NSObject, NSCoding, Mappable{

	var agencies : [SuggestAgency]?
	var color : String?
	var icon : AnyObject?
	var name : String?
	var shortName : String?
	var textColor : String?
	var url : String?
	var vehicle : SuggestVehicle?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestLine()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		agencies <- map["agencies"]
		color <- map["color"]
		icon <- map["icon"]
		name <- map["name"]
		shortName <- map["short_name"]
		textColor <- map["text_color"]
		url <- map["url"]
		vehicle <- map["vehicle"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agencies = aDecoder.decodeObject(forKey: "agencies") as? [SuggestAgency]
         color = aDecoder.decodeObject(forKey: "color") as? String
         icon = aDecoder.decodeObject(forKey: "icon") as? AnyObject
         name = aDecoder.decodeObject(forKey: "name") as? String
         shortName = aDecoder.decodeObject(forKey: "short_name") as? String
         textColor = aDecoder.decodeObject(forKey: "text_color") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String
         vehicle = aDecoder.decodeObject(forKey: "vehicle") as? SuggestVehicle

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if agencies != nil{
			aCoder.encode(agencies, forKey: "agencies")
		}
		if color != nil{
			aCoder.encode(color, forKey: "color")
		}
		if icon != nil{
			aCoder.encode(icon, forKey: "icon")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if shortName != nil{
			aCoder.encode(shortName, forKey: "short_name")
		}
		if textColor != nil{
			aCoder.encode(textColor, forKey: "text_color")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}
		if vehicle != nil{
			aCoder.encode(vehicle, forKey: "vehicle")
		}

	}

}
