//
//	SuggestDirectionaClass.swift
//
//	Create by Isaac on 14/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestDirectionaClass : NSObject, NSCoding, Mappable{

	var errorMessage : AnyObject?
	var routes : [SuggestRoute]?
	var status : String?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestDirectionaClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		errorMessage <- map["error_message"]
		routes <- map["routes"]
		status <- map["status"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         errorMessage = aDecoder.decodeObject(forKey: "error_message") as? AnyObject
         routes = aDecoder.decodeObject(forKey: "routes") as? [SuggestRoute]
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "error_message")
		}
		if routes != nil{
			aCoder.encode(routes, forKey: "routes")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
