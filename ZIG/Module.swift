//
//  Module.swift
//  ZIG
//
//  Created by apple on 26/11/18.
//  Copyright © 2018 Isaac. All rights reserved.
//


import Foundation
import ObjectMapper

class Module: Mappable {
    var arrivalStop : String?
    var arrivalTime : String?
    var blockID : String?
    var departureStop : String?
    var departureTime : String?
    var directionID : Int?
    var routeColor : String?
    var shapeID : String?
    var shapes : [Shapes]?
    var travel : String?
    var tripID : Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        tripID <- map["TripID"]
        blockID <- map["BlockID"]
        departureStop <- map["DepartureStop"]
        departureTime <- map["DepartureTime"]
        arrivalStop <- map["ArrivalStop"]
        directionID <- map["DirectionID"]
        arrivalTime <- map["ArrivalTime"]
        shapeID <- map["ShapeID"]
        routeColor <- map["RouteColor"]
        shapes <- map["Shapes"]
        
        
    }
    
    
}

class Shapes:Mappable{
    var distance : Int?
    var lat : String?
    var longField : String?
    var seq : AnyObject?
    var shapeId : String?

    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        longField <- map["Long"]
        lat <- map["Lat"]
        distance <- map["Distance"]
        shapeId <- map["ShapeId"]
        seq <- map["Seq"]
    }
    
    
}
