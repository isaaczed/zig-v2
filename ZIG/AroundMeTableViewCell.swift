//
//  AroundMeTableViewCell.swift
//  ZIG
//
//  Created by Isaac on 29/01/18.
//  Copyright © 2018 Isaac. All rights reserved.
//

import UIKit

class AroundMeTableViewCell: UITableViewCell {

    var aroundScrollView = UIScrollView()
    var aroundBackView = UIView()
    var aroundImageView = UIImageView()
    var titleLable = UILabel()
    var dateLable = UILabel()
    var detailsBtn = UIButton()
    var tripPlanBtn = UIButton()
    var likeBtn = UIButton()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aroundImageView.image = nil
        titleLable.text = nil
        dateLable.text = nil
       
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
