//
//  Font.swift
//  Vrobal
//
//  Created by Apple on 10/8/18.
//  Copyright © 2018 OCS. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    static func setTarcBold(size:CGFloat) -> UIFont {
        
        return UIFont.init(name: "Linotte-Bold", size: size)!
        
    }
    static func setTarcLight(size:CGFloat) -> UIFont {
        
        return UIFont.init(name: "Linotte-Light", size: size)!
        
    }
    
    static func setTarcHeavy(size:CGFloat) -> UIFont {
        
        return UIFont.init(name: "Linotte-Heavy", size: size)!
        
    }
    static func setTarcRegular(size:CGFloat) -> UIFont {
        
        return UIFont.init(name: "Linotte-Regular", size: size)!
        
    }
    static func setTarcSemiBold(size:CGFloat) -> UIFont {
        
        return UIFont.init(name: "Linotte-SemiBold", size: size)!
        
    }
}
