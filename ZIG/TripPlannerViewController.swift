
//
//  TripPlannerViewController.swift
//  ZIG
//
//  Created by Isaac on 24/11/17.


//  Copyright © 2017 Isaac. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import XLActionController
import UberCore
import LyftSDK
import Toast_Swift
import UberRides
import SCLAlertView
import EventKit
import NYAlertViewController
import DateTimePicker
import UIDropDown
import NVActivityIndicatorView
import Toast_Swift
import SwiftyJSON
import SafariServices
import RealmSwift
import SwiftyJSON
import Firebase


class TripPlannerViewController: UIViewController, LoginButtonDelegate, CLLocationManagerDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchDisplayDelegate,UITableViewDelegate,UITableViewDataSource,GMSMapViewDelegate,UITextFieldDelegate {
    let assistiveTouch = AssistiveTouch()
    let assistiveTouchSOS = AssistiveTouch()
    
    var Modetype = "DepartNow"
    var photolinkUser = ""
    
    @IBOutlet weak var WhatsNearMe: UIButton!
    @IBOutlet weak var FavButton: UIButton!
    var firstServiceCallComplete = false
    var secondServiceCallComplete = false
    var scopes: [UberScope] = []
    var isshecduleLater = true
    var indexPos = 0
    let placesClient = GMSPlacesClient.shared()
    var isMenuClicked = true
    var isBackAmenities = false
    var CorrectRegin:Bool = true
    var isPlaceSelected = true
    var typeString = "bus_station"
    var sourceStringTrip : String = ""
    var popupopen = true
    var ameniticsTitle = ""
    var menuButton = UIBarButtonItem()
    var redBikeStatus:DataResponse<RedBikeStatusClass>!
    var redBikeInformation: DataResponse<RedBikeClass>!
    var preferenceBtn = UIButton()
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var sourceTextField: UITextField!
    var location :CLLocation!
    var nearbyAddress = String()

    var boltListApi: [Bolt_list]?
    
    var datePicker = DateTimePicker()
    var responseVarAddress = [AddressList]()
    var profileDataList : DataResponse<GetProfile>!
    var autoFillArray :NSMutableArray = []
    var autofillTableView: UITableView = UITableView()
    var autofillTableView1: UITableView = UITableView()
    var menuTabelView :UITableView = UITableView()
    var markerDict : NSMutableArray = []
    var resultBackView = UIView()
    var polylinePoint = GMSPolyline()
    var listViewBtn = UIButton()
    var listBackView = UIView()
    var isListSelected = false
    var resultBackView1 = UIView()
    var menuBackView = UIView()
    var preferencesBackView = UIView()
    var fetcher: GMSAutocompleteFetcher?
    var fetcher1: GMSAutocompleteFetcher?
    var collectionview: UICollectionView!
    var cellId = "Cell"
    let cellReuseIdentifier = "cell"
    let locationManager = CLLocationManager()
    let didFindMyLocation = false
    var blurEffectView = UIVisualEffectView()
    var amenityView = UIView()
    var autoFillbounds = GMSCoordinateBounds()
    var AddressSearch = [AddressListGoogle]()
    var sourcePlace :GMSPlace?
    var destinationPlace : GMSPlace?
    var paybackLabel : UILabel = UILabel()
    var paybackSlider : UISlider = UISlider()
    var sliderValue = 1
    let preferenceView = UIView()
    var destinationStringTrip : String = ""
    let buscheckbox = UISwitch()
    let listViewBtnBackView = UIView()
    let traincheckbox = UISwitch()
    let bikebuscheckbox = UISwitch()
    let rideShareCheckBox = UISwitch()
    let uberCheckBox = UISwitch()
    let lyftCheckBox = UISwitch()
    let source_marker = GMSMarker()
    let destination_marker = GMSMarker()
    var sourceLatTrip : Double = 0.0
    var sourceLngTrip : Double = 0.0
    var destinationLatTrip : Double = 0.0
    var destinationLngTrip : Double = 0.0
    var isSourceMarker = false
    var scheduleLaterTime = Date()
    var isCurrentLoc : Bool = true
    var amenityTextButtonX = 0 ,amenityTextButtonY = 0, amenityTextButtonWidth = 0, amenityTextButtonHeight = 0
    var amenityBtnX = 0, amenityBtnY = 0, amenityBtnWidth = 0, amenityBtnHeight = 0
    var preferenceBtnX = 0,preferenceBtnY = 0,preferenceBtnWidth = 0,preferenceBtnHeight = 0
    var preferenceTextBtnX = 0,preferenceTextBtnY = 0,preferenceTextBtnWidth = 0,preferenceTextBtnHeight = 0
    var scheduleLaterBtnX = 0,scheduleLaterBtnY = 0,scheduleLaterBtnWidth = 0,scheduleLaterBtnHeight = 0
    var scheduleLaterTextBtnX = 0,scheduleLaterTextBtnY = 0,scheduleLaterTextBtnWidth = 0,scheduleLaterTextBtnHeight = 0
    var preferencesBackViewX = 0 ,preferencesBackViewY = 0 ,preferencesBackViewWidth = 0 ,preferencesBackViewHeight = 0
    var amenityImageViewX = 0,amenityImageViewY = 0,amenityImageViewWidth = 0,amenityImageViewHeight = 0
    var amenityCellLabelX = 0,amenityCellLabelY = 0,amenityCellLabelWidth = 0,amenityCellLabelHeight = 0
    var amenityViewX = 0,amenityViewY = 0,amenityViewWidth = 0,amenityViewHeight = 0, clearMarkerX = 0, clearMarkerY = 0, clearMarkerWidth = 80, clearMarkerHeight = 40
    var listViewBtnSize = 0
    var preferenceFont = 0.0
    var amenityTypeString = "Near my source"
    var versionLabel = UILabel()
    var showPreferencesOnLogin = "false"
    let uberImage = UIImageView()
    let uberLabel = UILabel()
    let lyftImage = UIImageView()
    let lyftLabel = UILabel()
    var BirdBikeID = ""
    var BirdBikeLat = 0.0
    var BirdBikeLon = 0.0
    var BirdIsReserved = 0
    var BirdIsDisabled = 0
    var BirdSourceDistance = 0.0
    var BirdtempDistance = 0.0
    var templat = 0.0
    var templong = 0.0
    var BikedistanceInMeters = 0.0
    var carSelected = Bool()
        var bikeSelected = Bool()
        var carFirstSelected = Bool()
        var bikeFirstSelected = Bool()
    
    //
    var LimeBikeID = ""
    var LimeBikeLat = 0.0
    var LimeBikeLon = 0.0
    var LimeIsReserved = 0
    var LimeIsDisabled = 0
    var LimeSourceDistance = 0.0
    var LimetempDistance = 0.0
    var LimedistanceInMeters = 0.0
    
    
    var BoltBikeID = ""
    var BoltBikeLat = 0.0
    var BoltBikeLon = 0.0
    var BoltIsReserved = 0
    var BoltIsDisabled = 0
    var BoltSourceDistance = 0.0
    var BolttempDistance = 0.0
    var BoltdistanceInMeters = 0.0
    //
    
    var BirdDatalistapicount:Int?
    var sourceaddressString:String?
    var DestinationAddressString:String?
    var eventArrayList = [String]()
    struct Place {
        let id: Int
        let name: String
        let lat: CLLocationDegrees
        let lng: CLLocationDegrees
        let icon: String
    }
    var destinationcount = 0
    struct State {
        let name: String
        let long: CLLocationDegrees
        let lat: CLLocationDegrees
        let placeId : String
        let distanceVale : String
        let formattedAddress : String
        let type : String
        let imageUrl : String
    }
    
    let clearBtn = UIButton()
    var markersArray: [State] = []
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    let sourceSearchView = UIView()
    let destinationSearchView = UIView()
    let eventsScheduleArray : NSMutableArray = []
    var calendarEvent = false
    var loaderView = UIView()
    let markerClearButton = UIButton()
    var firsttimeDestination = true
    let contentView = UIView ()
    var bubble: BubbleControl!
    let amenityTextBtn = UIButton()
    let scheduleLaterTextBtn = UIButton()
    let preferenceTextBtn = UIButton()
    @IBOutlet weak var tripNotationImage: UIImageView!
    @IBOutlet weak var mapBackgorundView: GMSMapView!
    var amenityBtn = UIButton()
    var scheduleLaterBtn = UIButton()
    @IBOutlet weak var planTripBtn: UIButton!
    var birdListApi: [data_list]?
    var LimeListApi: [Lime_list]?
    var nearbyLinesBool = Bool()
    var SavedAddressArray = [[String: String]]()
var savedCount = Int()
    //    var alertView: AlertOnboarding!
    //
    //    var arrayOfImage = ["Dash-Tripplaner","Dash-Map & sch","Dash-fav","TARCoverview-29"]
    //    var arrayOfTitle = ["Multimodal Trip Planner", "Real-Time Schedules", "Saved Trips", "TARC"]
    //    var arrayOfDescription = ["Using transit, rideshare, bike, scooters in one!.","Locate your bus on the map and see it's next stops.","Save your favorite routes here.","Use this app to navigate the greater Louisville area through all of the transit options the city has to offer."]
    //
    //
    
    override func viewWillAppear(_ animated: Bool) {
        
        isMenuClicked = false
        self.OnMenuClicked()
        Modetype = "DepartNow"
        scheduleLaterTextBtn.setTitle("Depart Now", for: .normal)
        
        scopes = [.profile, .places, .history]
        FavButton.layer.zPosition = 10000000
        CheckFavTrip()
        scheduleLaterTime = NSDate() as Date
        self.navigationItem.setHidesBackButton(true, animated:true);
        calendarEvent = false
        let userDefaults1 = UserDefaults.standard
        let accessToken = userDefaults1.value(forKey: "accessToken")
        // let tokenString = accessToken as! String
        print("Access Token: \((accessToken)!)")
        let url = "\(APIUrl.ZIGTARCAPI)User/Profilelist"
        let parameters: Parameters = [
            "token": accessToken!,
            
            ]
        
        if userDefaults1.value(forKey: "userName") == nil || userDefaults1.value(forKey: "ProfileID") == nil ||  userDefaults1.value(forKey: "phoneNumber") == nil
                   {
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<GetProfile>) in
                switch response.result {
                    
                case .success:
                    let userDefaults1 = UserDefaults.standard
                    if response.result.value?.message == "Ok"
                    {
                        
                        
                        self.profileDataList = response
                        
                        userDefaults1.set(self.profileDataList.result.value?.result![0].username!, forKey: "userName")
                        userDefaults1.synchronize()
                        for profileModel in (self.profileDataList.result.value?.result![0].profileModels)!
                        {
                            
                            
                            if profileModel.Photolink?.range(of: "https://") != nil
                            {
                                self.photolinkUser = profileModel.Photolink!
                                userDefaults1.set(profileModel.Photolink!, forKey: "userPhoto")
                                userDefaults1.synchronize()
                                //  profileImage.downloadedFrom(link: profileModel.Photolink!)
                            }
                            else
                            {
                                userDefaults1.set("", forKey: "userPhoto")
                                userDefaults1.synchronize()
                                //  profileImage.image = UIImage(named:"male-user-profile-picture")
                            }
                        }
                        //  self.createUI()
                    }
                    else
                    {
                        userDefaults1.set("", forKey: "userName")
                        userDefaults1.set("", forKey: "userPhoto")
                        userDefaults1.synchronize()
                        print(response.result.value?.message)
                        //  self.showErrorDialogBox(viewController: self)
                        self.showAlertBox(text: response.result.value!.message!)
                        
                    }
                case .failure(let error):
                    print(error)
                    let userDefaults1 = UserDefaults.standard
                    userDefaults1.set("", forKey: "userName")
                    userDefaults1.set("", forKey: "userPhoto")
                    userDefaults1.synchronize()
                    self.showErrorDialogBox(viewController: self)
                    print(error.localizedDescription)
                }
                
                
        }
    }

        if userDefaults1.integer(forKey: "userIdString") == nil {
         print("new")
         let accessToken = userDefaults1.value(forKey: "accessToken")
        
         let getUserId = "\(APIUrl.ZIGTARCAPI)GetUserId?Token=\(accessToken!)"

         Alamofire.request(getUserId, method: .post, parameters: nil, encoding: JSONEncoding.default)
                                   .responseJSON { response in
                                       switch response.result {
                                       case .success:
                                           
                                           
                                           if (response.result.value != nil) {
                                               
                                             let responseVar = response.result.value
                                             let json = JSON(responseVar!)
                                             print(json.rawString()!)
                                             let useridString = json.rawString()! as! String
                                             let userid = Int(useridString)
                                             let userDefaults = UserDefaults.standard
                                              userDefaults.set(userid, forKey: "userIdString")

                                               
                                           }
                                       case .failure(let error):
                                           print(error)
                                          
                                          //
                                                                                          
                                      
                                         
                                            self.hideLoader()
                                       }
                               }
             }
        
        //        let urlString = "https://tripplan.ridetarc.org/ZIGSTarc/api/Fare/GetAllFare?Token=\(accessToken!)"
        //
        //        let url1 = URL(string: urlString)
        //        URLSession.shared.dataTask(with: url1!, completionHandler: {
        //            (data, response, error) in
        //            if(error != nil){
        //
        //            }else{
        //                do{
        //                    let json : NSArray = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSArray
        //
        //                    //self.mapView.clear()
        //
        //                    OperationQueue.main.addOperation({
        //                        for route in json
        //                        {
        //                            let userDefaults1 = UserDefaults.standard
        //                            let routeValues : NSDictionary = route as! NSDictionary
        //
        //                            let agencyNmae = routeValues.object(forKey: "Agencyname") as! String
        //                            let categoryName = routeValues.object(forKey: "CategoryId") as! Double
        //                            if agencyNmae == "SEAT" && categoryName == 1.0
        //                            {
        ////                                userDefaults1.set(routeValues.object(forKey: "Fareamount") as! Double, forKey: "generalFare")
        //                                 userDefaults1.set(1.75, forKey: "generalFare")
        //                                userDefaults1.synchronize()
        //                            }
        //
        //                        }
        //                    })
        //                }catch let error as NSError{
        //                    print("error:\(error)")
        //                    userDefaults1.set(1.75, forKey: "generalFare")
        //                    userDefaults1.synchronize()
        //                }
        //            }
        //        }).resume()
        
        
        SavedAddressArray = NSArray() as! [[String : String]]
          if UserDefaults.standard.array(forKey: "FavoriteSaved") != nil {
               // userDefault has a value
           SavedAddressArray = UserDefaults.standard.array(forKey: "FavoriteSaved") as! [[String : String]]
          }
        
        userDefaults1.set(1.75, forKey: "generalFare")
        
        if nearbyLinesBool == true
          {
              print("\(sourceStringTrip) and  \(sourceLatTrip) \(sourceLngTrip)")
                                   self.sourceTextField.text = nearbyAddress

              self.source_marker.position = CLLocationCoordinate2D(latitude: self.sourceLatTrip, longitude: self.sourceLngTrip)
                               self.source_marker.title = self.sourcePlace?.name
                               self.source_marker.icon = UIImage(named: "source_Marker")
                               self.source_marker.map = self.mapBackgorundView
                               
                               self.typeString = "bus_station"
                               //self.getPlaces(type: self.typeString)
              self.markerDict.removeAllObjects()
                                               self.markersArray.removeAll()
                               self.getbusStops(type: self.typeString)
            mapBackgorundView.bringSubview(toFront: amenityBtn)
                  mapBackgorundView.bringSubview(toFront: listViewBtn)
                  mapBackgorundView.bringSubview(toFront: listBackView)
                  
                  mapBackgorundView.bringSubview(toFront: scheduleLaterBtn)
                  mapBackgorundView.bringSubview(toFront: preferenceBtn)
                  mapBackgorundView.bringSubview(toFront: self.FavButton)
                  mapBackgorundView.bringSubview(toFront: planTripBtn)
            
          }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
            self.view.makeToast("Location access was limited. Kindly enable full permissions.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            self.view.makeToast("Location access was disabled. Please enable Location permissions for TARC.")
        //mapBackgorundView.isHidden = false
        case .notDetermined:
            self.view.makeToast("Location access was disabled. Please enable Location permissions for TARC.")
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
            
        case .authorizedWhenInUse:
            print("Location status is OK.")
            //viewMap.isHidden = true
            
        }
    }
    
    func showLoader()
    {
        
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.hideLoader()
    //    let userDefaults1 = UserDefaults.standard
     //   let accessToken = userDefaults1.value(forKey: "accessToken")
        // let tokenString = accessToken as! String
       // print("Access Token: \((accessToken)!)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
          if gesture.state == UIGestureRecognizerState.began {
              UIDevice.vibrate()
              print("Long Press")
              currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                         if Success{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                          print("\(Address),\(LatsosLat),\(Longsoslong)")
                          MapsScheduleVC.CurrentlocationAddressString = Address
                          MapsScheduleVC.latSOS = LatsosLat
                          MapsScheduleVC.longSOS = Longsoslong
                        self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                 //self.CurrentlocationAddress.text = Address
                             }
                         }
              
          }
      }
    
      @objc func tappedSOS(sender: UIButton) {
       
          let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                  // action here
          }
                  
          SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
         
      }
    override func viewDidLoad() {
        super.viewDidLoad()
        WhatsNearMe.isHidden = true
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "perferenceSelected"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("perferenceSelected"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
        
        analytics.GetPageHitCount(PageName: "Trip Planner")
        
        assistiveTouch.frame = CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56)
        assistiveTouch.accessibilityLabel = "Quick Menu"
        assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
        assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
        view.addSubview(assistiveTouch)
        self.FavButton.accessibilityLabel = "Save Trip Button"
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
//        tapGesture.numberOfTapsRequired = 1
//        assistiveTouchSOS.addGestureRecognizer(tapGesture)
//        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
//        longPress.minimumPressDuration = 1.0
//        self.assistiveTouchSOS.addGestureRecognizer(longPress)
//        assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
//        assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
//        view.addSubview(assistiveTouchSOS)
        
        
        
        
        OnBoardMsg.Buttoncolor = "#D0302E"
        getplotTARCTourismCategory()
        
        //        alertView = AlertOnboarding(arrayOfImage: arrayOfImage, arrayOfTitle: arrayOfTitle, arrayOfDescription: arrayOfDescription)
        //        alertView.delegate = self
        
        scheduleLaterTextBtn.semanticContentAttribute = .forceRightToLeft
        //scheduleLaterTextBtn.setImage(UIImage(named: "down_arrow"), for: .normal)
        let origImage = UIImage(named: "down_arrow")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        scheduleLaterTextBtn.setImage(tintedImage, for: .normal)
        scheduleLaterTextBtn.tintColor = .white
        
        let camera = GMSCameraPosition.camera(withLatitude:38.2526647 , longitude:-85.75845570000001 , zoom: 11.0)
        mapBackgorundView.isMyLocationEnabled = true
        mapBackgorundView?.animate(to: camera)
        //setupBubble()
        let userDefaults1 = UserDefaults.standard
        userDefaults1.set("true", forKey: "isBus")
        userDefaults1.set("true", forKey: "isBike")
        userDefaults1.set("true", forKey: "isTrain")
        //Arun 23 June
        //  userDefaults1.set("true", forKey: "isCab")
        
        userDefaults1.synchronize()
        
        
        
       
        
        
        self.markerClearButton.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        
        //self.navigationController?.title = "TRIP PLANNER"

        
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        menuButton = UIBarButtonItem(image: UIImage(named: "Menu_Icon"),
                                     style: UIBarButtonItemStyle.done ,
                                     target: self, action: #selector(OnMenuClicked))
        menuButton.accessibilityLabel = "Menu"
        self.navigationItem.rightBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.view.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"
        self.navigationItem.leftBarButtonItem = backButton
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        
   
        if UIScreen.main.sizeType == .iPhone4 {
            // Make specific layout for small devices.
            print("Iphone 4")
            
            preferencesBackViewX = 40
            preferencesBackViewY = 55
            preferencesBackViewWidth = 80
            preferencesBackViewHeight = 180
            amenityTextButtonX = 26
            amenityTextButtonY = -4
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 10
            amenityBtnY = 5
            amenityBtnWidth = 20
            amenityBtnHeight = 20
            preferenceBtnX = -3
            preferenceBtnY = 6
            preferenceBtnWidth = 20
            preferenceBtnHeight = 20
            preferenceTextBtnX = 3
            preferenceTextBtnY = -5
            preferenceTextBtnWidth = 20
            preferenceTextBtnHeight = 40
            scheduleLaterBtnX = 0
            scheduleLaterBtnY = 6
            scheduleLaterBtnWidth = 15
            scheduleLaterBtnHeight = 15
            scheduleLaterTextBtnX = 10
            scheduleLaterTextBtnY = -5
            scheduleLaterTextBtnWidth = -10
            scheduleLaterTextBtnHeight = 40
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 20
            amenityImageViewHeight = 20
            amenityCellLabelX = 30
            amenityCellLabelY = -3
            amenityCellLabelWidth = 20
            amenityCellLabelHeight = 30
            amenityViewX = 30
            amenityViewY = 80
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 14.0
            listViewBtnSize = 40
            
        }else if UIScreen.main.sizeType == .iPhone5{
            print("iPhone 5 or 5s or SE")
            preferencesBackViewX = 40
            preferencesBackViewY = 55
            preferencesBackViewWidth = 80
            preferencesBackViewHeight = 180
            amenityTextButtonX = 26
            amenityTextButtonY = -4
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 10
            amenityBtnY = 5
            amenityBtnWidth = 20
            amenityBtnHeight = 20
            preferenceBtnX = -3
            preferenceBtnY = 6
            preferenceBtnWidth = 20
            preferenceBtnHeight = 20
            preferenceTextBtnX = 3
            preferenceTextBtnY = -5
            preferenceTextBtnWidth = 20
            preferenceTextBtnHeight = 40
            scheduleLaterBtnX = 0
            scheduleLaterBtnY = 6
            scheduleLaterBtnWidth = 15
            scheduleLaterBtnHeight = 15
            scheduleLaterTextBtnX = 10
            scheduleLaterTextBtnY = -5
            scheduleLaterTextBtnWidth = -10
            scheduleLaterTextBtnHeight = 40
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 20
            amenityImageViewHeight = 20
            amenityCellLabelX = 30
            amenityCellLabelY = -3
            amenityCellLabelWidth = 20
            amenityCellLabelHeight = 30
            amenityViewX = 30
            amenityViewY = 80
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 14.0
            listViewBtnSize = 40
            
            
        }else if UIScreen.main.sizeType == .iPhone6{
            print("iPhone 6 or 7 or 8")
            preferencesBackViewX = 60
            preferencesBackViewY = 70
            preferencesBackViewWidth = 120
            preferencesBackViewHeight = 210
            amenityTextButtonX = 30
            amenityTextButtonY = 0
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 15
            amenityBtnY = 2
            amenityBtnWidth = 30
            amenityBtnHeight = 30
            preferenceBtnX = 10
            preferenceBtnY = 6
            preferenceBtnWidth = 30
            preferenceBtnHeight = 30
            preferenceTextBtnX = 0
            preferenceTextBtnY = 0
            preferenceTextBtnWidth = 40
            preferenceTextBtnHeight = 40
            scheduleLaterBtnX = -10
            scheduleLaterBtnY = 6
            scheduleLaterBtnWidth = 25
            scheduleLaterBtnHeight = 25
            scheduleLaterTextBtnX = 6
            scheduleLaterTextBtnY = 0
            scheduleLaterTextBtnWidth = 20
            scheduleLaterTextBtnHeight = 40
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 30
            amenityImageViewHeight = 30
            amenityCellLabelX = 35
            amenityCellLabelY = -6
            amenityCellLabelWidth = 30
            amenityCellLabelHeight = 40
            amenityViewX = 60
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 18.0
            listViewBtnSize = 45
            
        }else if UIScreen.main.sizeType == .iPhone6Plus{
            print("iPhone 6plus or 7plus or 8plus")
            preferencesBackViewX = 50
            preferencesBackViewY = 70
            preferencesBackViewWidth = 100
            preferencesBackViewHeight = 250
            amenityTextButtonX = 50
            amenityTextButtonY = 0
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 20
            amenityBtnY = -10
            amenityBtnWidth = 50
            amenityBtnHeight = 50
            preferenceBtnX = -15
            preferenceBtnY = 0
            preferenceBtnWidth = 36
            preferenceBtnHeight = 36
            preferenceTextBtnX = 30
            preferenceTextBtnY = 20
            preferenceTextBtnWidth = 0
            preferenceTextBtnHeight = 0
            scheduleLaterBtnX = -20
            scheduleLaterBtnY = -5
            scheduleLaterBtnWidth = 50
            scheduleLaterBtnHeight = 50
            scheduleLaterTextBtnX = 24
            scheduleLaterTextBtnY = 20
            scheduleLaterTextBtnWidth = -10
            scheduleLaterTextBtnHeight = 0
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 40
            amenityImageViewHeight = 40
            amenityCellLabelX = 45
            amenityCellLabelY = 0
            amenityCellLabelWidth = 30
            amenityCellLabelHeight = 40
            amenityViewX = 60
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 25
            listViewBtnSize = 60
            clearMarkerY = 20
            
        }
        else if UIScreen.main.nativeBounds.height == 1920 {
            print("iPhone 6plus or 7plus or 8plus")
            preferencesBackViewX = 50
            preferencesBackViewY = 70
            preferencesBackViewWidth = 100
            preferencesBackViewHeight = 250
            amenityTextButtonX = 50
            amenityTextButtonY = 0
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 20
            amenityBtnY = -10
            amenityBtnWidth = 50
            amenityBtnHeight = 50
            preferenceBtnX = -15
            preferenceBtnY = 0
            preferenceBtnWidth = 36
            preferenceBtnHeight = 36
            preferenceTextBtnX = 30
            preferenceTextBtnY = 20
            preferenceTextBtnWidth = 0
            preferenceTextBtnHeight = 0
            scheduleLaterBtnX = -20
            scheduleLaterBtnY = -5
            scheduleLaterBtnWidth = 50
            scheduleLaterBtnHeight = 50
            scheduleLaterTextBtnX = 24
            scheduleLaterTextBtnY = 20
            scheduleLaterTextBtnWidth = -10
            scheduleLaterTextBtnHeight = 0
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 40
            amenityImageViewHeight = 40
            amenityCellLabelX = 45
            amenityCellLabelY = 0
            amenityCellLabelWidth = 30
            amenityCellLabelHeight = 40
            amenityViewX = 60
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 25
            listViewBtnSize = 60
            clearMarkerY = 20
        }
        else if UIScreen.main.nativeBounds.height == 2688 {
            print("iPhone X max")
            preferencesBackViewX = 50
            preferencesBackViewY = 70
            preferencesBackViewWidth = 100
            preferencesBackViewHeight = 220
            amenityTextButtonX = 46
            amenityTextButtonY = 0
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 20
            amenityBtnY = -8
            amenityBtnWidth = 50
            amenityBtnHeight = 50
            preferenceBtnX = -25
            preferenceBtnY = 0
            preferenceBtnWidth = 36
            preferenceBtnHeight = 36
            preferenceTextBtnX = 30
            preferenceTextBtnY = 20
            preferenceTextBtnWidth = 0
            preferenceTextBtnHeight = 0
            scheduleLaterBtnX = -15
            scheduleLaterBtnY = -5
            scheduleLaterBtnWidth = 50
            scheduleLaterBtnHeight = 50
            scheduleLaterTextBtnX = 24
            scheduleLaterTextBtnY = 20
            scheduleLaterTextBtnWidth = -20
            scheduleLaterTextBtnHeight = 0
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 40
            amenityImageViewHeight = 40
            amenityCellLabelX = 45
            amenityCellLabelY = 0
            amenityCellLabelWidth = 30
            amenityCellLabelHeight = 40
            amenityViewX = 50
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 22
            listViewBtnSize = 50
            //mapBackgorundView.frame = CGRect(x: 30, y: 30, width: 0, height: 0)
            clearMarkerY = 20
        }
        else if UIScreen.main.nativeBounds.height == 1792 {
            
            preferencesBackViewX = 50
            preferencesBackViewY = 70
            preferencesBackViewWidth = 100
            preferencesBackViewHeight = 250
            amenityTextButtonX = 40
            amenityTextButtonY = 0
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 20
            amenityBtnY = -10
            amenityBtnWidth = 50
            amenityBtnHeight = 50
            preferenceBtnX = -15
            preferenceBtnY = 0
            preferenceBtnWidth = 36
            preferenceBtnHeight = 36
            preferenceTextBtnX = 30
            preferenceTextBtnY = 20
            preferenceTextBtnWidth = 0
            preferenceTextBtnHeight = 0
            scheduleLaterBtnX = 0
            scheduleLaterBtnY = -5
            scheduleLaterBtnWidth = 50
            scheduleLaterBtnHeight = 50
            scheduleLaterTextBtnX = 30
            scheduleLaterTextBtnY = 20
            scheduleLaterTextBtnWidth = 0
            scheduleLaterTextBtnHeight = 0
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 40
            amenityImageViewHeight = 40
            amenityCellLabelX = 45
            amenityCellLabelY = 0
            amenityCellLabelWidth = 30
            amenityCellLabelHeight = 40
            amenityViewX = 60
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 25
            listViewBtnSize = 60
            clearMarkerY = 20
            
        }
            
        else if UIScreen.main.sizeType == .iPhoneX{
            print("iPhone X")
            preferencesBackViewX = 50
            preferencesBackViewY = 70
            preferencesBackViewWidth = 100
            preferencesBackViewHeight = 220
            amenityTextButtonX = 36
            amenityTextButtonY = 0
            amenityTextButtonWidth = 40
            amenityTextButtonHeight = 40
            amenityBtnX = 10
            amenityBtnY = -8
            amenityBtnWidth = 50
            amenityBtnHeight = 50
            preferenceBtnX = -15
            preferenceBtnY = 0
            preferenceBtnWidth = 36
            preferenceBtnHeight = 36
            preferenceTextBtnX = 20
            preferenceTextBtnY = 20
            preferenceTextBtnWidth = 0
            preferenceTextBtnHeight = 0
            scheduleLaterBtnX = -20
            scheduleLaterBtnY = -5
            scheduleLaterBtnWidth = 50
            scheduleLaterBtnHeight = 50
            scheduleLaterTextBtnX = 24
            scheduleLaterTextBtnY = 20
            scheduleLaterTextBtnWidth = -10
            scheduleLaterTextBtnHeight = 0
            amenityImageViewX = 0
            amenityImageViewY = 0
            amenityImageViewWidth = 40
            amenityImageViewHeight = 40
            amenityCellLabelX = 45
            amenityCellLabelY = 0
            amenityCellLabelWidth = 30
            amenityCellLabelHeight = 40
            amenityViewX = 50
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 22
            listViewBtnSize = 50
            //mapBackgorundView.frame = CGRect(x: 30, y: 30, width: 0, height: 0)
            clearMarkerY = 20
            
        }else if UIScreen.main.sizeType == .iPadPro129{
            print("iPad 12.9")
            preferencesBackViewX = 200
            preferencesBackViewY = 250
            preferencesBackViewWidth = 400
            preferencesBackViewHeight = 550
            amenityTextButtonX = 165
            amenityTextButtonY = -2
            amenityTextButtonWidth = 70
            amenityTextButtonHeight = 40
            amenityBtnX = 210
            amenityBtnY = -10
            amenityBtnWidth = 50
            amenityBtnHeight = 50
            preferenceBtnX = 20
            preferenceBtnY = 0
            preferenceBtnWidth = 35
            preferenceBtnHeight = 35
            preferenceTextBtnX = 0
            preferenceTextBtnY = 0
            preferenceTextBtnWidth = 90
            preferenceTextBtnHeight = 40
            scheduleLaterBtnX = 20
            scheduleLaterBtnY = -5
            scheduleLaterBtnWidth = 50
            scheduleLaterBtnHeight = 50
            scheduleLaterTextBtnX = 6
            scheduleLaterTextBtnY = 0
            scheduleLaterTextBtnWidth = 100
            scheduleLaterTextBtnHeight = 40
            amenityImageViewX = 70
            amenityImageViewY = 0
            amenityImageViewWidth = 60
            amenityImageViewHeight = 60
            amenityCellLabelX = 150
            amenityCellLabelY = -5
            amenityCellLabelWidth = 60
            amenityCellLabelHeight = 80
            amenityViewX = 60
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 180
            preferenceFont = 20
            listViewBtnSize = 60
            clearMarkerX = 280
            clearMarkerY = -20
            clearMarkerWidth = 100
            clearMarkerHeight = 60
            
        }else if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
            print("iPad Air 2 or iPad Mini 4")
            preferencesBackViewX = 200
            preferencesBackViewY = 250
            preferencesBackViewWidth = 400
            preferencesBackViewHeight = 600
            amenityTextButtonX = 148
            amenityTextButtonY = 0
            amenityTextButtonWidth = 70
            amenityTextButtonHeight = 40
            amenityBtnX = 180
            amenityBtnY = -4
            amenityBtnWidth = 40
            amenityBtnHeight = 40
            preferenceBtnX = 20
            preferenceBtnY = 2
            preferenceBtnWidth = 35
            preferenceBtnHeight = 35
            preferenceTextBtnX = 0
            preferenceTextBtnY = 0
            preferenceTextBtnWidth = 90
            preferenceTextBtnHeight = 40
            scheduleLaterBtnX = 20
            scheduleLaterBtnY = 0
            scheduleLaterBtnWidth = 40
            scheduleLaterBtnHeight = 40
            scheduleLaterTextBtnX = 8
            scheduleLaterTextBtnY = 0
            scheduleLaterTextBtnWidth = 100
            scheduleLaterTextBtnHeight = 40
            amenityImageViewX = 70
            amenityImageViewY = 0
            amenityImageViewWidth = 60
            amenityImageViewHeight = 60
            amenityCellLabelX = 150
            amenityCellLabelY = -5
            amenityCellLabelWidth = 60
            amenityCellLabelHeight = 80
            amenityViewX = 60
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 18.0
            listViewBtnSize = 60
        }
        else if UIScreen.main.sizeType == .iPadPro105{
            preferencesBackViewX = 200
            preferencesBackViewY = 250
            preferencesBackViewWidth = 400
            preferencesBackViewHeight = 600
            amenityTextButtonX = 148
            amenityTextButtonY = 0
            amenityTextButtonWidth = 70
            amenityTextButtonHeight = 40
            amenityBtnX = 180
            amenityBtnY = -4
            amenityBtnWidth = 40
            amenityBtnHeight = 40
            preferenceBtnX = 20
            preferenceBtnY = 2
            preferenceBtnWidth = 35
            preferenceBtnHeight = 35
            preferenceTextBtnX = 0
            preferenceTextBtnY = 0
            preferenceTextBtnWidth = 90
            preferenceTextBtnHeight = 40
            scheduleLaterBtnX = 20
            scheduleLaterBtnY = 0
            scheduleLaterBtnWidth = 40
            scheduleLaterBtnHeight = 40
            scheduleLaterTextBtnX = 8
            scheduleLaterTextBtnY = 0
            scheduleLaterTextBtnWidth = 100
            scheduleLaterTextBtnHeight = 40
            amenityImageViewX = 70
            amenityImageViewY = 0
            amenityImageViewWidth = 60
            amenityImageViewHeight = 60
            amenityCellLabelX = 150
            amenityCellLabelY = -5
            amenityCellLabelWidth = 60
            amenityCellLabelHeight = 80
            amenityViewX = 60
            amenityViewY = 100
            amenityViewWidth = 60
            amenityViewHeight = 250
            preferenceFont = 18.0
            listViewBtnSize = 60
        }
        
        
        edgesForExtendedLayout = []
        sourceTextField?.text = "Current Location"
        sourceTextField?.autoresizingMask = .flexibleWidth
        sourceTextField?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                                   for: .editingChanged)
        sourceTextField?.addTarget(self, action: #selector(textFieldDidBeginEditing(textField:)),
                                   for: .editingDidBegin)
        sourceTextField.tag = 1001
        sourceTextField.delegate = self
        destinationTextField.returnKeyType = .go
        sourceTextField.returnKeyType = .next
        
        destinationTextField?.autoresizingMask = .flexibleWidth
        destinationTextField?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                                        for: .editingChanged)
        destinationTextField?.addTarget(self, action: #selector(textFieldDidBeginEditing(textField:)),
                                        for: .editingDidBegin)
        destinationTextField.tag = 1002
        destinationTextField.delegate = self
        
        
        
        
        
        
        // self.view.addSubview(preferencesBackView)
        
        
        
        
        let extraBtnView = UIView()
        extraBtnView.frame = CGRect.init(x: 0, y: destinationTextField.frame.origin.y+destinationTextField.frame.size.height, width: self.view.frame.size.width, height: 50)
        extraBtnView.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        self.view.addSubview(extraBtnView)
        
        
        
        
        
        
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            //            preferencesBackView.frame = CGRect.init(x: Int((self.view.frame.size.width/2))-preferencesBackViewX, y: preferencesBackViewY, width: preferencesBackViewWidth, height: preferencesBackViewHeight)
            amenityBtn.frame = CGRect.init(x: Int((self.view.frame.size.width/2))-amenityBtnX, y: amenityBtnY, width: amenityBtnWidth, height: amenityBtnHeight)
            amenityTextBtn.frame = CGRect.init(x: Int((self.view.frame.size.width/2))-amenityTextButtonX, y: amenityTextButtonY, width: amenityTextButtonWidth, height: amenityTextButtonHeight)
            scheduleLaterBtn.frame = CGRect.init(x: Int(amenityTextBtn.frame.origin.x)+Int(amenityTextBtn.frame.size.width)+scheduleLaterBtnX, y: scheduleLaterBtnY, width: scheduleLaterBtnWidth, height: scheduleLaterBtnHeight)
            scheduleLaterTextBtn.frame = CGRect.init(x: Int(scheduleLaterBtn.frame.origin.x)+Int(scheduleLaterBtn.frame.size.width)-scheduleLaterTextBtnX, y: scheduleLaterTextBtnY, width: scheduleLaterTextBtnWidth, height: scheduleLaterTextBtnHeight)
            
            preferenceBtn.frame = CGRect.init(x: Int(scheduleLaterTextBtn.frame.origin.x)+Int(scheduleLaterTextBtn.frame.size.width)+preferenceBtnX, y: preferenceBtnY, width: preferenceBtnWidth, height: preferenceBtnHeight)
            
            preferenceTextBtn.frame = CGRect.init(x: Int(preferenceBtn.frame.origin.x)+Int(preferenceBtn.frame.size.width), y: preferenceTextBtnY, width: preferenceTextBtnWidth, height: preferenceTextBtnHeight)
            
            markerClearButton.frame = CGRect.init(x: Int(self.view.frame.width) - clearMarkerWidth - 20, y: Int(preferenceTextBtn.frame.origin.y + preferenceTextBtn.frame.height ) + clearMarkerY, width: clearMarkerWidth, height: clearMarkerHeight)
            
            
            
        }
        else
        {
            //        preferencesBackView.frame = CGRect.init(x: preferencesBackViewX, y: preferencesBackViewY, width: Int(self.view.frame.size.width) - preferencesBackViewWidth, height: Int(self.view.frame.size.height)  - preferencesBackViewHeight)
            amenityBtn.frame = CGRect.init(x: amenityBtnX, y: amenityBtnY, width: amenityBtnWidth, height: amenityBtnHeight)
            
            amenityTextBtn.frame = CGRect.init(x: amenityTextButtonX, y: amenityTextButtonY, width: Int((self.view.frame.size.width/3)) - amenityTextButtonWidth, height: amenityTextButtonHeight)
            scheduleLaterBtn.frame = CGRect.init(x: Int(amenityTextBtn.frame.origin.x)+Int(amenityTextBtn.frame.size.width)+scheduleLaterBtnX , y: scheduleLaterBtnY, width: scheduleLaterBtnWidth, height: scheduleLaterBtnHeight)
            
            scheduleLaterTextBtn.frame = CGRect.init(x: Int(scheduleLaterBtn.frame.origin.x)+Int(scheduleLaterBtn.frame.size.width)-scheduleLaterTextBtnX, y: amenityTextButtonY, width: Int((self.view.frame.size.width/3))-scheduleLaterTextBtnWidth, height: amenityTextButtonHeight)
            
            preferenceBtn.frame = CGRect.init(x: Int(scheduleLaterTextBtn.frame.origin.x)+Int(scheduleLaterTextBtn.frame.size.width)+preferenceBtnX, y: preferenceBtnY, width: preferenceBtnWidth, height: preferenceBtnHeight)
            
            preferenceTextBtn.frame = CGRect.init(x: Int(preferenceBtn.frame.origin.x)+Int(preferenceBtn.frame.size.width) - preferenceTextBtnX, y: amenityTextButtonY, width: Int((self.view.frame.size.width/3))-preferenceTextBtnWidth, height: amenityTextButtonHeight)
            
            
            markerClearButton.frame = CGRect.init(x: Int(preferenceTextBtn.frame.origin.x + 10), y: Int(preferenceTextBtn.frame.origin.y + preferenceTextBtn.frame.height - 15) + clearMarkerY , width: 80, height: 40)
            
        }
        
        preferencesBackView.backgroundColor = UIColor.white;
        
        
        markerClearButton.setTitle("Clear", for: .normal)
        markerClearButton.setTitleColor(UIColor.white, for: .normal)
        markerClearButton.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        markerClearButton.titleLabel?.font = UIFont.setTarcRegular(size: 16.0)
        mapBackgorundView.addSubview(markerClearButton)
        mapBackgorundView.bringSubview(toFront: markerClearButton)
        mapBackgorundView.bringSubview(toFront: WhatsNearMe)
        
        
        markerClearButton.addTarget(self, action: #selector(clearMarkerOption), for: UIControlEvents.touchUpInside)
        
        
        amenityBtn.setBackgroundImage(UIImage(named: "ZIG icons-40.png"), for: UIControlState.normal)
        //amenityBtn.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        amenityBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        amenityBtn.addTarget(self, action:#selector(amentiyAction), for: UIControlEvents.touchUpInside)
        extraBtnView.addSubview(amenityBtn)
        
        
        
        amenityTextBtn.titleLabel?.font = UIFont.setTarcBold(size: 13.0)
        amenityTextBtn.setTitle("Places", for: UIControlState.normal)
        amenityTextBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        amenityTextBtn.titleLabel?.textAlignment = NSTextAlignment.center
        amenityTextBtn.addTarget(self, action:#selector(amentiyAction), for: UIControlEvents.touchUpInside)
        extraBtnView.addSubview(amenityTextBtn)
        
        scheduleLaterBtn.setBackgroundImage(UIImage(named: "ZIG icons-38.png"), for: UIControlState.normal)
        scheduleLaterBtn.addTarget(self, action:#selector(ChooseMode), for: UIControlEvents.touchUpInside)
        //scheduleLaterBtn.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        extraBtnView.addSubview(scheduleLaterBtn)
        
        scheduleLaterTextBtn.setTitle("Depart Now", for: UIControlState.normal)
        scheduleLaterTextBtn.addTarget(self, action:#selector(ChooseMode), for: UIControlEvents.allTouchEvents)
        scheduleLaterTextBtn.titleLabel?.font = UIFont.setTarcBold(size: 13.0)
        scheduleLaterTextBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        scheduleLaterTextBtn.titleLabel?.textAlignment = NSTextAlignment.center
        extraBtnView.addSubview(scheduleLaterTextBtn)
        
        preferenceBtn.setBackgroundImage(UIImage(named: "preferences_Icon"), for: UIControlState.normal)
        preferenceBtn.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        preferenceBtn.addTarget(self, action:#selector(preferencesBtnAction), for: UIControlEvents.touchUpInside)
        extraBtnView.addSubview(preferenceBtn)
        
        preferenceTextBtn.setTitle("Preferences", for: UIControlState.normal)
        preferenceTextBtn.titleLabel?.font = UIFont.setTarcBold(size: 13.0)
        preferenceTextBtn.addTarget(self, action:#selector(preferencesBtnAction), for: UIControlEvents.touchUpInside)
        preferenceTextBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        preferenceTextBtn.titleLabel?.textAlignment = NSTextAlignment.center
        extraBtnView.addSubview(preferenceTextBtn)
        
        
        listViewBtnBackView.frame = CGRect.init(x: 10, y: Int(extraBtnView.frame.origin.y+extraBtnView.frame.size.height+10), width: listViewBtnSize, height: listViewBtnSize)
        listViewBtnBackView.backgroundColor = UIColor.red
        listViewBtnBackView.layer.masksToBounds = false
        listViewBtnBackView.layer.shadowColor = UIColor.black.cgColor
        listViewBtnBackView.layer.shadowOpacity = 0.6
        listViewBtnBackView.layer.shadowOffset = CGSize(width: -1, height: 1)
        listViewBtnBackView.layer.shadowRadius = 3
        
        listViewBtnBackView.layer.shadowPath = UIBezierPath(rect: listViewBtnBackView.bounds).cgPath
        listViewBtnBackView.layer.shouldRasterize = true
        listViewBtnBackView.layer.rasterizationScale = 1
        self.view.addSubview(listViewBtnBackView)
        
        listViewBtn.frame = CGRect.init(x: 10, y: Int(extraBtnView.frame.origin.y+extraBtnView.frame.size.height+10), width: listViewBtnSize, height: listViewBtnSize)
        listViewBtn.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        listViewBtn.setBackgroundImage(UIImage(named: "listMenu_Icon"), for: .normal)
        listViewBtn.tag = 1
        listViewBtn.accessibilityLabel = "List Menu"

        listViewBtn.addTarget(self, action:#selector(listViewBtnAction), for: UIControlEvents.touchUpInside)
        
        self.view.addSubview(listViewBtn)
        
        
        listBackView.frame = CGRect.init(x: -2000, y: listViewBtn.frame.origin.y + listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
        listBackView.backgroundColor = UIColor.clear
        self.view.addSubview(listBackView)
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            menuBackView.frame = CGRect.init(x: 2000, y: 0, width: 290, height: self.view.frame.height)
        }
        else
        {
            menuBackView.frame = CGRect.init(x: 2000, y: 0, width: self.view.frame.width-90, height: self.view.frame.height)
        }
        menuBackView.backgroundColor = UIColor.white
        menuBackView.layer.borderWidth = 0
        menuBackView.layer.borderColor = UIColor.init(red:225/255.0, green:225/255.0, blue:225/255.0, alpha: 1.0).cgColor
        self.view.addSubview(menuBackView)
        
        
        
        
        //Location Manager code to fetch current location
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    if nearbyLinesBool == true
    {
    
    }
        else
    {
        locationManager.startUpdatingLocation()

        }
        
        // GOOGLE MAPS SDK: COMPASS
        mapBackgorundView.settings.compassButton = true
        mapBackgorundView.delegate = self
        // GOOGLE MAPS SDK: USER'S LOCATION
        mapBackgorundView.settings.myLocationButton = true
        
        do {
            if let styleURL = Bundle.main.url(forResource: "custom", withExtension: "json") {
                mapBackgorundView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    @objc func tapped(sender: UIButton) {
        print("\(sender) has been touched")
        
        
        let actionController = TwitterActionController()
        
        actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
            
            //                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            //                self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        if comVar.DateImplementation {
            
            
            
            actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                           
                           
            }))
            
        }
       
       
        actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        actionController.headerData = "Quick View"
        present(actionController, animated: true, completion: nil)
        
        
        
        
    }
    func getLatLngandPlantrip(address : String, valueDate : String) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            guard
                let placemarks = placemarks,
                let location = placemarks.first?.location
                
                else {
                    // handle no location found
                    return
            }
            
            self.destinationTextField.text = address
            self.destinationLatTrip = location.coordinate.latitude
            self.destinationLngTrip = location.coordinate.longitude
            self.destinationStringTrip = address
            self.calendarEvent = true
            
        }
        
        if address != "" {
            
            let alertViewController = NYAlertViewController()
            // Set a title and message
            alertViewController.title = "Plan Trip"
            alertViewController.message = "Use your current location to plan this trip?"
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            
            let activateAction = NYAlertAction(
                title: "Yes",
                style: .default,
                handler: { (action: NYAlertAction!) -> Void in
                    
                    self.planTripAction()
                    
                    self.dismiss(animated: true, completion: nil)
            }
            )
            let cancelAction = NYAlertAction(
                title: "No",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    self.sourceTextField.becomeFirstResponder()
                    self.dismiss(animated: true, completion: nil)
            }
            )
            alertViewController.addAction(activateAction)
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
            
        }
        else {
            let alertViewController = NYAlertViewController()
            alertViewController.title = "Calendar Events"
            alertViewController.message = "Please add a location for your event"
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            
            
            let cancelAction = NYAlertAction(
                title: "OK",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    
                    self.dismiss(animated: true, completion: nil)
            }
            )
            
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    @objc private func textFieldDidBeginEditing(textField: UITextField)
    {
        textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
        if textField == sourceTextField
        {
            self.isSourceMarker = true
            let camera = GMSCameraPosition.camera(withLatitude: (source_marker.position.latitude), longitude: (source_marker.position.longitude), zoom: 17.0)
            mapBackgorundView.isMyLocationEnabled = true
            mapBackgorundView?.animate(to: camera)
        }
        else
        {
            let camera = GMSCameraPosition.camera(withLatitude: (destination_marker.position.latitude), longitude: (destination_marker.position.longitude), zoom: 17.0)
            mapBackgorundView.isMyLocationEnabled = true
            mapBackgorundView?.animate(to: camera)
            self.isSourceMarker = false
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        responseVarAddress.removeAll()
        AddressSearch.removeAll()
        
        self.DataBaseAutocompleteFunc(SearchString: textField.text!)
        DispatchQueue.main.async {
            if textField.tag == 1001
            {
                
                //self.isSourceMarker = true
                self.isBackAmenities = false
                self.closebuttonAction(sender: nil)
                
                self.resultBackView.frame = CGRect.init(x: self.sourceTextField.frame.origin.x, y: self.sourceTextField.frame.origin.y+self.sourceTextField.frame.size.height+10, width: self.sourceTextField.frame.size.width, height: self.view.frame.size.height-(self.sourceTextField.frame.origin.y+70))
                self.resultBackView.isHidden = false
                self.resultBackView.backgroundColor = UIColor.clear
                
                
                self.autofillTableView.register(MyTicketTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
                
                // This view controller itself will provide the delegate methods and row data for the table view.
                self.autofillTableView.delegate = self
                self.autofillTableView.dataSource = self
                self.autofillTableView.backgroundColor = UIColor.white
                self.view.addSubview(self.resultBackView)
                self.resultBackView.addSubview(self.autofillTableView)
                
                // self.fetcher?.sourceTextHasChanged(textField.text!)
                self.CheckFavTrip()
            }
            else
            {
                // self.isSourceMarker = false
                self.resultBackView1.frame = CGRect.init(x:  self.destinationTextField.frame.origin.x, y: self.destinationTextField.frame.origin.y+self.destinationTextField.frame.size.height+10, width: self.destinationTextField.frame.size.width, height: self.view.frame.size.height-(self.destinationTextField.frame.origin.y+70))
                self.resultBackView1.isHidden = false
                self.resultBackView1.backgroundColor = UIColor.clear
                self.autofillTableView1.register(MyTicketTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
                
                // This view controller itself will provide the delegate methods and row data for the table view.
                self.autofillTableView1.delegate = self
                self.autofillTableView1.dataSource = self
                self.autofillTableView1.backgroundColor = UIColor.white
                self.view.addSubview(self.resultBackView1)
                self.resultBackView1.addSubview(self.autofillTableView1)
                self.fetcher?.sourceTextHasChanged(textField.text!)
                self.CheckFavTrip()
            }
        }
        
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        print("Value of notification : ", notification.object ?? "")
        let uberSelect = notification.object as! NSMutableArray
        let uberDict = uberSelect.object(at: 0) as! NSDictionary
        self.uberCheckBox.isOn = uberDict.value(forKey: "uber") as! Bool
        self.lyftCheckBox.isOn = uberDict.value(forKey: "lyft") as! Bool
        
        self.carSelected = uberDict.value(forKey: "Cab") as! Bool
                     self.bikeSelected = uberDict.value(forKey: "Bike") as! Bool
                     self.carFirstSelected = uberDict.value(forKey: "Cabshare") as! Bool
                     self.bikeFirstSelected = uberDict.value(forKey: "Bikeshare") as! Bool
        
        print(self.uberCheckBox.isOn)
        print(self.lyftCheckBox.isOn)
        
        closePreferenceAction(sender: nil)
        
    }
    @objc func OnMenuClicked() {
        
        if isMenuClicked
        {
            // self.closePreferenceAction(sender: nil)
            isMenuClicked = false
            DispatchQueue.main.async {
                self.sourceTextField.resignFirstResponder()
                self.destinationTextField.resignFirstResponder()
                self.autofillTableView.removeFromSuperview()
                self.autofillTableView1.removeFromSuperview()
            }
            DispatchQueue.main.async {
                self.listViewBtn.setBackgroundImage(UIImage(named: "listMenu_Icon"), for: .normal)
            }
            isListSelected = false
            UIView.animate(withDuration: 0,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                            
            }, completion: { (finished) -> Void in
                self.listBackView = UIView()
                self.indexPos = 0
                self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                self.listBackView.backgroundColor = UIColor.clear
                self.view.addSubview(self.listBackView)
            })
            UIView.animate(withDuration: 0,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            if UIDevice.current.userInterfaceIdiom == .pad
                            {
                                self.menuBackView.frame = CGRect.init(x: self.view.frame.size.width-290, y: 0, width: 290, height: self.view.frame.height)
                            }
                            else
                            {
                                self.menuBackView.frame = CGRect.init(x: 90, y: 0, width: self.view.frame.width-90, height: self.view.frame.height)
                            }
            }, completion: { (finished) -> Void in
                
                let profileBackView = UIView()
                profileBackView.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
                self.menuBackView.addSubview(profileBackView)
                
                let versionLabel = UILabel()
                versionLabel.frame = CGRect.init(x: 0, y: 5, width: self.menuBackView.frame.size.width-10, height: 20)
                versionLabel.textAlignment = .right
                versionLabel.font = UIFont.setTarcRegular(size: 12.0)
                self.menuBackView .addSubview(versionLabel)
                //First get the nsObject by defining as an optional anyObject
                let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?

                //Then just cast the object as a String, but be careful, you may want to double check for nil
                let version = nsObject as! String
                let versionString = "Version \(version)"
                versionLabel.text = versionString
                versionLabel.textColor = color.hexStringToUIColor(hex: "#d3d3d3")
                let profileImage = UIImageView()
                profileImage.frame = CGRect.init(x: (self.menuBackView.frame.size.width/2)-60, y: 20, width: 120, height: 120)
                let userDefaults1 = UserDefaults.standard
                if (self.photolinkUser != "") {
                    
                    profileImage.image = UIImage(named:"tarclogo")
                    
                }
                let photoLinkString =  self.photolinkUser
                if photoLinkString.range(of: "https://") != nil
                {
                    profileImage.downloadedFrom(link: photoLinkString )
                    profileImage.layer.borderColor = UIColor.black.cgColor
                    profileImage.layer.borderWidth=1.0

                }
                else
                {
                    profileImage.image = UIImage(named:"tarclogo")
                }
                
                
                profileImage.layer.masksToBounds = false
               // profileImage.backgroundColor = .gray
                profileImage.layer.cornerRadius = profileImage.frame.size.height/2
                profileImage.clipsToBounds = true
                self.menuBackView.addSubview(profileImage)
                
                let userNameLabel = UILabel()
                userNameLabel.frame = CGRect.init(x: 0, y: profileImage.frame.origin.y+profileImage.frame.size.height+20, width: self.menuBackView.frame.width, height: 40)
                let usernameString : String = userDefaults1.value(forKey: "userName") as? String ?? "Welcome"
                if usernameString != ""
                {
                    userNameLabel.text = usernameString.uppercased()
                }
                else
                {
                    userNameLabel.text = "Welcome"
                }
                
                
                userNameLabel.textColor = UIColor.white
                userNameLabel.font = UIFont.setTarcRegular(size: 18.0)
                userNameLabel.textAlignment = NSTextAlignment.center
                self.menuBackView.addSubview(userNameLabel)
                profileBackView.frame = CGRect.init(x: 0, y: 0, width: self.menuBackView.frame.size.width, height: userNameLabel.frame.origin.y+userNameLabel.frame.size.height+5)
                self.menuTabelView.register(UITableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
                
                self.menuTabelView.frame = CGRect.init(x: 0, y: userNameLabel.frame.size.height+userNameLabel.frame.origin.y+10, width: self.menuBackView.frame.width, height: self.view.frame.size.height-(userNameLabel.frame.size.height+userNameLabel.frame.origin.y+10))
                
                self.menuTabelView.delegate = self
                self.menuTabelView.dataSource = self
                self.menuTabelView.backgroundColor = UIColor.white
                self.menuBackView.addSubview(self.menuTabelView)
                
                self.versionLabel.frame = CGRect.init(x: Int(self.menuTabelView.frame.width/2) - 50, y: Int(self.menuTabelView.frame.height) + 190, width: Int(self.menuTabelView.frame.width), height: 20)
                self.versionLabel.text = "Version 1.0"
                self.versionLabel.font = UIFont.setTarcRegular(size: 12.0)
                // self.menuBackView.addSubview(self.versionLabel)
                
                
                
            })
        }
        else
        {
            isMenuClicked = true
            isBackAmenities = false
            self.closebuttonAction(sender: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getCalenderActivities() {
        let eventStore = EKEventStore()
        
       // eventStore.requestAccess(to: .event, completion:{_,_ in })
        eventStore.requestAccess(to: .event) { (granted, error) in
        if let error = error {
           print(error)
               
           return
        }
               if granted {
        let calendars = eventStore.calendars(for: .event)
        
                self.eventsScheduleArray.removeAllObjects()
        for calendar in calendars {
            
            
            let oneMonthAgo = NSDate(timeIntervalSinceNow:0)
            let oneMonthAfter = NSDate(timeIntervalSinceNow: +24*3600)
            print(oneMonthAgo)
            print(oneMonthAfter)
            
            let predicate = eventStore.predicateForEvents(withStart: oneMonthAgo as Date, end: oneMonthAfter as Date, calendars: [calendar])
            
            let events = eventStore.events(matching: predicate)
            print(events)
            for event in events {
                if event.location != nil {
                if event.location! != ""
                {
                    let dummyDic : NSMutableDictionary = [:]
                    dummyDic.setValue(event.location!, forKey: "Location")
                    dummyDic.setValue(event.startDate!, forKey: "Date")
                    dummyDic.setValue(event.title!, forKey: "Title")
                    self.eventsScheduleArray.add(dummyDic)
                }
                }
                
            }
            
        }
                print(self.eventsScheduleArray)
                if self.eventsScheduleArray.count > 0
        {
            let actionController = YoutubeActionController()
            actionController.title = "Events from your Calendar"
            for eventsData in self.eventsScheduleArray{
                let eventsValue : NSMutableDictionary = eventsData as! NSMutableDictionary
                actionController.addAction(Action(ActionData(title: eventsValue.value(forKey: "Title") as! String), style: .default, handler: { action in
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = NSTimeZone.local
                    dateFormatter.dateFormat = "dd/MM/YYYY HH:mm"
                    
                    
                    let timeStamp = dateFormatter.string(from: eventsValue.value(forKey: "Date") as! Date)
                    
                    self.scheduleLaterTime = eventsValue.value(forKey: "Date") as! Date
                    self.getLatLngandPlantrip(address: eventsValue.value(forKey: "Location") as! String, valueDate: timeStamp)
                }))
                
            }
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
               DispatchQueue.main.async {
            self.present(actionController, animated: true, completion: nil)
            }
        }
        else{
                DispatchQueue.main.async {
            let alertViewController = NYAlertViewController()
            alertViewController.title = "Calendar Events"
            alertViewController.message = "There are no events currently scheduled in your calendar. Please add an event."
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            
            
            let cancelAction = NYAlertAction(
                title: "OK",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    
                    self.dismiss(animated: true, completion: nil)
            }
            )
            
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
        }
                }
    }
               else{
                  DispatchQueue.main.async {
                let alertViewController = NYAlertViewController()
                    alertViewController.title = "Calendar Events"
                    alertViewController.message = "The app is not permitted to access Google Calander event, make sure to grant permission in the settings and try again"
                    
                    // Customize appearance as desired
                    alertViewController.buttonCornerRadius = 20.0
                    alertViewController.view.tintColor = self.view.tintColor
                    alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                    alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.swipeDismissalGestureEnabled = true
                    alertViewController.backgroundTapDismissalGestureEnabled = true
                    alertViewController.buttonColor = UIColor.red
                    // Add alert actions
                    
                    
                    let cancelAction = NYAlertAction(
                        title: "OK",
                        style: .cancel,
                        handler: { (action: NYAlertAction!) -> Void in
                            
                            self.dismiss(animated: true, completion: nil)
                    }
                    )
                    
                    alertViewController.addAction(cancelAction)
                    
                    // Present the alert view controller
                    self.present(alertViewController, animated: true, completion: nil)
                
                }
            }
        }
        }
    
    @objc func clearMarkerOption(){
        
        markerClearButton.isHidden = true
        listViewBtn.isHidden = true
        listBackView.isHidden = true
        listViewBtnBackView.isHidden = true
        
        
        self.hideLoader()
        DispatchQueue.main.async {
            self.listViewBtn.setBackgroundImage(UIImage(named: "listMenu_Icon"), for: .normal)
        }
        isListSelected = false
        UIView.animate(withDuration: 0,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                        
        }, completion: { (finished) -> Void in
            self.listBackView = UIView()
            self.indexPos = 0
            self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
            self.listBackView.backgroundColor = UIColor.clear
            self.view.addSubview(self.listBackView)
        })
        
        
        for markerDet in self.markerDict
        {
            let removeMarker : GMSMarker = markerDet as! GMSMarker
            removeMarker.map = nil;
        }
        self.markerDict.removeAllObjects()
        self.markersArray.removeAll()
        
        
    }
    
    
    @objc func preferencesBtnAction() {
        comVar.status = 7
        print(self.uberCheckBox.isOn)
        print(self.lyftCheckBox.isOn)
        let userDefaults1 = UserDefaults.standard
        
        var uberBool = Bool()
        var isLyft  =  Bool()
        uberBool = userDefaults1.bool(forKey: "isUber")
        isLyft = userDefaults1.bool(forKey: "isLyft")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
        vc.uberSelected = uberBool
        vc.lyftSelected = isLyft
        vc.isclickFromTripPlanner = true
        vc.saveTripBool = false
        self.present(vc, animated: true, completion: nil)
        
       
    }
    func CheckFavTrip() {
        //print(self.sourceTextField.text != "" && self.destinationTextField.text != "")
        if self.sourceTextField.text != "" && self.destinationTextField.text != "" {
            FavButton.isEnabled = true
            let userDefaults1 = UserDefaults.standard
            let accessToken = userDefaults1.value(forKey: "accessToken")
            let url = "\(APIUrl.ZIGTARCAPI)SaveTrips/Get"
            let parameters: Parameters = [
                "Token": accessToken!
            ]
            self.FavButton.isUserInteractionEnabled = false
            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<SavedRootClass>) in
                    switch response.result {
                    case .success:
                        //  print(response)
                        if response.result.value?.trips != nil
                        {
                            for responseData in (response.result.value?.trips)!
                            {
                                // print("\(responseData.sourceAddress), \(self.sourceTextField.text)")
                                // print("\(responseData.destinationAddress), \(self.destinationTextField.text)")
                                // print(!(responseData.sourceAddress == self.sourceTextField.text && responseData.destinationAddress == self.destinationTextField.text))
                                if !(responseData.sourceAddress == self.sourceTextField.text && responseData.destinationAddress == self.destinationTextField.text)
                                {
                                    //let FavButtonAct  = UIButton(type: .custom)
                                    self.FavButton.setImage(UIImage(named: "FavDeactive"), for: .normal)
                                    
                                    
                                }
                                else
                                {
                                    //let FavButtonAct  = UIButton(type: .custom)
                                    self.FavButton.setImage(UIImage(named: "FavActive"), for: .normal)
                                    /// self.FavButton.isEnabled = false
                                    
                                    break
                                    
                                }
                                
                            }
                            self.FavButton.isUserInteractionEnabled = true

                        }
                        else
                        {
                            //let FavButtonAct  = UIButton(type: .custom)
                            self.FavButton.setImage(UIImage(named: "FavDeactive"), for: .normal)
                            self.FavButton.isUserInteractionEnabled = true

                        }
                        
                    case .failure(let error):
                        // self.showErrorDialogBox(viewController: self)
                        print(error.localizedDescription)
                        self.FavButton.isUserInteractionEnabled = true

                    }
            }
            
        }
        else{
            FavButton.isEnabled = false
        }
    }
    @IBAction func AddFavTrip(_ sender: UIButton) {
        if self.FavButton.currentImage == UIImage(named: "FavActive") {
            let alertViewController = NYAlertViewController()
            // Set a title and message
            alertViewController.title = "Saved Trip"
            alertViewController.message = "Already saved to Favorites."
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            
            
            let cancelAction = NYAlertAction(
                title: "OK",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    
                    self.dismiss(animated: true, completion: nil)
            }
            )
            
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
        }
        else {
            //switch to image-one
            
            self.showLoader()
            let userDefaults1 = UserDefaults.standard
            let accessToken = userDefaults1.value(forKey: "accessToken")
            let url1 = "\(APIUrl.ZIGTARCAPI)SaveTrips/post"
            let parameters1: Parameters = [
                "Token": accessToken!,
                "SourceAddress" : self.sourceTextField.text!,
                "sourceLat" : self.sourceLatTrip,
                "sourceLong" : self.sourceLngTrip,
                "DestinationAddress" : self.destinationTextField.text!,
                "DestinationLat" : self.destinationLatTrip,
                "DestinationLong" : self.destinationLngTrip
                
            ]
            print("\(APIUrl.ZIGTARCAPI)SaveTrips/post?Token=\(accessToken!)&SourceAddress=\(self.sourceTextField.text!)&sourceLat=\(self.sourceLatTrip)&sourceLong=\(self.sourceLngTrip)&DestinationAddress=\(self.destinationTextField.text!)&DestinationLat=\(self.destinationLatTrip)&DestinationLong=\(self.destinationLngTrip)")
           Alamofire.request(url1, method: .post, parameters: parameters1, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<Postpayment>) in
                    switch response.result {
                    case .success:
                        //  print(response.result.value?.message ?? "nil")
                        let image = UIImage(named: "FavActive")
                        sender.setImage(image, for: .normal)
                        self.hideLoader()
                        self.CheckFavTrip()
                        
                    case .failure(let error):
                        self.showErrorDialogBox(viewController: self)
                        print(error.localizedDescription)
                        let image = UIImage(named: "FavDeactive")
                        sender.setImage(image, for: .normal)
                        self.hideLoader()
                    }
                    
            }
            
            
            
            //        let image = UIImage(named: "FavActive")
            //sender.setImage(image, for: .normal)
        }
    }
    @objc func enableRideShareOptions(){
        
        if self.rideShareCheckBox.isOn{
            
            self.uberImage.isHidden = false
            self.uberLabel.isHidden = false
            self.lyftImage.isHidden = false
            self.lyftLabel.isHidden = false
            self.uberCheckBox.isHidden = false
            self.lyftCheckBox.isHidden = false
            self.uberCheckBox.isEnabled = true
            self.lyftCheckBox.isEnabled = true
            self.uberCheckBox.setOn(true, animated: true)
            self.lyftCheckBox.setOn(false, animated: true)
            
            
        }else{
            
            self.uberImage.isHidden = true
            self.uberLabel.isHidden = true
            self.lyftImage.isHidden = true
            self.lyftLabel.isHidden = true
            self.uberCheckBox.isHidden = true
            self.lyftCheckBox.isHidden = true
            self.lyftCheckBox.setOn(false, animated: true)
            self.uberCheckBox.setOn(false, animated: true)
            self.uberCheckBox.isEnabled = false
            self.lyftCheckBox.isEnabled = false
        }
    }
    
    @objc func onUberPreferenceClicked(){
        let userDefaults1 = UserDefaults.standard
        if self.uberCheckBox.isOn{
            self.lyftCheckBox.setOn(false, animated: true)
            userDefaults1.set("false", forKey: "isLyft")
            userDefaults1.set("true", forKey: "isUber")
            
            
        }else{
            self.lyftCheckBox.setOn(true, animated: true)
            // userDefaults1.set("false", forKey: "isCab")
            userDefaults1.set("true", forKey: "isLyft")
            userDefaults1.set("false", forKey: "isUber")
        }
        userDefaults1.synchronize()
    }
    
    @objc func onLyftPreferenceClicked(){
        let userDefaults1 = UserDefaults.standard
        if self.lyftCheckBox.isOn{
            self.uberCheckBox.setOn(false, animated: true)
            userDefaults1.set("true", forKey: "isLyft")
            userDefaults1.set("false", forKey: "isUber")
        }else{
            self.uberCheckBox.setOn(true, animated: true)
            userDefaults1.set("false", forKey: "isLyft")
            userDefaults1.set("true", forKey: "isUber")
        }
        userDefaults1.synchronize()
    }
    
    func loginButton(_ button: LoginButton, didLogoutWithSuccess success: Bool) {
        // success is true if logout succeeded, false otherwise
        if success{
            isMenuClicked = false
            self.OnMenuClicked()
        }
        else{
            
        }
    }
    
    func loginButton(_ button: LoginButton, didCompleteLoginWithToken accessToken: AccessToken?, error: NSError?) {
        
        if let _ = accessToken {
            // AccessToken Saved
            
            
            let ridesClient = RidesClient()
            let pickupLocation = CLLocation(latitude: 37.787654, longitude: -122.402760)
            let dropoffLocation = CLLocation(latitude: 37.775200, longitude: -122.417587)
            let builder = RideParametersBuilder()
            builder.pickupLocation = pickupLocation
            builder.dropoffLocation = dropoffLocation
            
            ridesClient.fetchProducts(pickupLocation: pickupLocation) { products, response in
                guard let uberX = products.filter({$0.productGroup == .uberX}).first else {
                    // Handle error, UberX does not exist at this location
                    return
                }
                builder.productID = uberX.productID
                ridesClient.fetchRideRequestEstimate(parameters: builder.build(), completion: { rideEstimate, response in
                    guard let rideEstimate = rideEstimate else {
                        // Handle error, unable to get an ride request estimate
                        return
                    }
                    //   print(rideEstimate.distanceEstimate)
                    builder.upfrontFare = rideEstimate.fare
                    print(rideEstimate.fare?.display as Any)
                    
                })
            }
            isMenuClicked = false
            self.OnMenuClicked()
            
            
            print("uber login sucess")
        } else if error != nil {
            // An error occured
            self.showErrorDialogBox(viewController: self)
            //print(error?.localizedDescription)
            print(error?.localizedDescription as Any)
        }
    }
    @objc func listViewBtnAction(){
        if self.listViewBtn.currentBackgroundImage!.isEqual(UIImage(named: "listMenu_Icon")) {
            //do something here
            
            print(self.markerDict.count)
            print("YES Yes")
            
            DispatchQueue.main.async {
                self.sourceTextField.resignFirstResponder()
                self.destinationTextField.resignFirstResponder()
                self.autofillTableView.removeFromSuperview()
                self.autofillTableView1.removeFromSuperview()
                self.isBackAmenities = false
                self.closebuttonAction(sender: nil)
            }
            
            if !isListSelected
            {
                self.showLoader()
                DispatchQueue.main.async {
                    self.listViewBtn.setBackgroundImage(UIImage(named: "white_Close"), for: .normal)
                }
                isListSelected = true
                UIView.animate(withDuration: 0,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseInOut,
                               animations: { () -> Void in
                                self.listBackView.frame = CGRect.init(x:0, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                }, completion: { (finished) -> Void in
                    let aroundScrollView = UIScrollView()
                    aroundScrollView.delegate = self
                    aroundScrollView.backgroundColor = UIColor.clear
                    aroundScrollView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 200)
                    aroundScrollView.showsHorizontalScrollIndicator = false
                    self.listBackView.addSubview(aroundScrollView)
                    var isaa = 0
                    var isaac = 0
                    //                for stateMarker in self.markerDict
                    //                {
                    isaa = isaa+1
                    
                    // let markerDetail : GMSMarker = stateMarker as! GMSMarker
                    // print(self.markersArray.count)
                    for markerValue in self.markersArray
                    {
                        
                        //                        if markerDetail.title == markerValue.name && (markerDetail.snippet == markerValue.formattedAddress || markerValue.type == "LOUVELO" || markerValue.type == "Parking")
                        //                        {
                        if markerValue.type == "Places"
                        {
                            let aroundBackView = UIView()
                            let aroundImageView = UIImageView()
                            let titleLable = UILabel()
                            let titleDescTextView = UITextView()
                            let distanceLable = UILabel()
                            aroundBackView.frame = CGRect.init(x: (20*(self.indexPos+1))+(self.indexPos * 160), y: 0, width: 160, height: 200)
                            aroundBackView.backgroundColor = UIColor.white
                            aroundBackView.layer.masksToBounds = false
                            aroundBackView.layer.shadowColor = UIColor.black.cgColor
                            aroundBackView.layer.shadowOpacity = 0.6
                            aroundBackView.layer.shadowOffset = CGSize(width: -1, height: 1)
                            aroundBackView.layer.shadowRadius = 3
                            
                            aroundBackView.layer.shadowPath = UIBezierPath(rect: aroundBackView.bounds).cgPath
                            aroundScrollView.addSubview(aroundBackView)
                            
                            aroundImageView.frame = CGRect.init(x: 0, y: 10, width: 160, height: 80)
                            aroundImageView.contentMode = .scaleAspectFit
                            aroundImageView.image = UIImage(named: markerValue.imageUrl)
                            aroundBackView.addSubview(aroundImageView)
                            
                            //  let decodedString = CFXMLCreateStringByUnescapingEntities(nil, markerValue.name, nil) as String
                            
                            
                            titleLable.frame = CGRect.init(x: 10, y: 95, width: 150, height: 15)
                            titleLable.text = markerValue.name.decodeEnt()
                            titleLable.textColor = UIColor.black
                            titleLable.font = UIFont.setTarcBold(size: 11.0)
                            aroundBackView.addSubview(titleLable)
                            titleDescTextView.frame = CGRect.init(x: 0, y: 115, width: 160, height: 55)
                            titleDescTextView.isEditable = false
                            titleDescTextView.isSelectable = false
                            titleDescTextView.font = UIFont.setTarcRegular(size: 10.0)
                            titleDescTextView.text = markerValue.formattedAddress
                            titleDescTextView.textColor = UIColor.black
                            aroundBackView.addSubview(titleDescTextView)
                            
                            distanceLable.frame = CGRect.init(x: 10, y: 175, width: 150, height: 15)
                            distanceLable.textColor = UIColor.black
                            distanceLable.font = UIFont.setTarcRegular(size: 12.0)
                            let disFloat = String(format: "%.2f", Float(markerValue.distanceVale) ?? 0)
                            distanceLable.text = "Distance : \(disFloat) miles"
                            distanceLable.textAlignment = .center
                            aroundBackView.addSubview(distanceLable)
                            
                            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
                            tapGesture.numberOfTapsRequired = 1
                            print(2000+self.indexPos)
                            aroundBackView.tag = 2000+self.indexPos
                            aroundBackView.addGestureRecognizer(tapGesture)
                            
                            aroundScrollView.contentSize = CGSize(width: aroundBackView.frame.origin.x+aroundBackView.frame.size.width+20, height: 80)
                            self.indexPos = self.indexPos+1
                            
                        }
                        else if markerValue.type == "LOUVELO"
                        {
                            let aroundBackView = UIView()
                            let aroundImageView = UIImageView()
                            let titleLable = UILabel()
                            let titleDescTextView = UITextView()
                            aroundBackView.frame = CGRect.init(x: (20*(self.indexPos+1))+(self.indexPos * 160), y: 0, width: 160, height: 180)
                            aroundBackView.backgroundColor = UIColor.white
                            aroundScrollView.addSubview(aroundBackView)
                            aroundImageView.frame = CGRect.init(x: 0, y: 10, width: 160, height: 80)
                            aroundImageView.contentMode = .scaleAspectFit
                            aroundImageView.image = UIImage(named: markerValue.imageUrl)
                            aroundBackView.addSubview(aroundImageView)
                            
                            
                            titleLable.frame = CGRect.init(x: 10, y: 95, width: 150, height: 15)
                            titleLable.text = markerValue.name
                            titleLable.textColor = UIColor.black
                            titleLable.font = UIFont.setTarcBold(size: 11.0)
                            aroundBackView.addSubview(titleLable)
                            titleDescTextView.frame = CGRect.init(x: 0, y: 115, width: 160, height: 55)
                            titleDescTextView.isEditable = false
                            titleDescTextView.isSelectable = false
                            titleDescTextView.font = UIFont.setTarcRegular(size: 10.0)
                            titleDescTextView.text = markerValue.formattedAddress
                            titleDescTextView.textColor = UIColor.black
                            aroundBackView.addSubview(titleDescTextView)
                            
                            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
                            tapGesture.numberOfTapsRequired = 1
                            aroundBackView.tag = 2000+self.indexPos
                            aroundBackView.addGestureRecognizer(tapGesture)
                            
                            aroundScrollView.contentSize = CGSize(width: aroundBackView.frame.origin.x+aroundBackView.frame.size.width+20, height: 80)
                            self.indexPos = self.indexPos+1
                            
                        }
                        else if markerValue.type == "Parking"
                        {
                            let aroundBackView = UIView()
                            let aroundImageView = UIImageView()
                            let titleLable = UILabel()
                            let titleDescTextView = UITextView()
                            
                            aroundBackView.frame = CGRect.init(x: (20*(self.indexPos+1))+(self.indexPos * 160), y: 0, width: 160, height: 180)
                            aroundBackView.backgroundColor = UIColor.white
                            aroundScrollView.addSubview(aroundBackView)
                            aroundImageView.frame = CGRect.init(x: 0, y: 10, width: 160, height: 80)
                            aroundImageView.contentMode = .scaleAspectFit
                            aroundImageView.image = UIImage(named: markerValue.imageUrl)
                            aroundBackView.addSubview(aroundImageView)
                            
                            
                            titleLable.frame = CGRect.init(x: 10, y: 95, width: 150, height: 15)
                            titleLable.text = markerValue.name
                            titleLable.textColor = UIColor.black
                            titleLable.font = UIFont.setTarcBold(size: 11.0)
                            aroundBackView.addSubview(titleLable)
                            titleDescTextView.frame = CGRect.init(x: 0, y: 115, width: 160, height: 55)
                            titleDescTextView.isEditable = false
                            titleDescTextView.isSelectable = false
                            titleDescTextView.font = UIFont.setTarcRegular(size: 10.0)
                            titleDescTextView.text = markerValue.formattedAddress
                            titleDescTextView.textColor = UIColor.black
                            aroundBackView.addSubview(titleDescTextView)
                            
                            
                            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
                            tapGesture.numberOfTapsRequired = 1
                            aroundBackView.tag = 2000+self.indexPos
                            aroundBackView.addGestureRecognizer(tapGesture)
                            
                            aroundScrollView.contentSize = CGSize(width: aroundBackView.frame.origin.x+aroundBackView.frame.size.width+20, height: 80)
                            self.indexPos = self.indexPos+1
                            
                        }
                        else
                        {
                            isaac = isaac+1
                            print("\(isaa),\(isaac)")
                            
                            let aroundBackView = UIView()
                            let aroundImageView = UIImageView()
                            let titleLable = UILabel()
                            let titleDescTextView = UITextView()
                            
                            aroundBackView.frame = CGRect.init(x: (20*(self.indexPos+1))+(self.indexPos * 160), y: 0, width: 160, height: 180)
                            aroundBackView.backgroundColor = UIColor.white
                            aroundBackView.layer.masksToBounds = false
                            aroundBackView.layer.shadowColor = UIColor.black.cgColor
                            aroundBackView.layer.shadowOpacity = 0.6
                            aroundBackView.layer.shadowOffset = CGSize(width: -1, height: 1)
                            aroundBackView.layer.shadowRadius = 3
                            
                            aroundBackView.layer.shadowPath = UIBezierPath(rect: aroundBackView.bounds).cgPath
                            aroundScrollView.addSubview(aroundBackView)
                            
                            
                            
                            
                            aroundImageView.frame = CGRect.init(x: 0, y: 10, width: 160, height: 80)
                            aroundImageView.contentMode = .scaleAspectFit
                            if markerValue.type == "plotTARCTourism"{
                                print(markerValue.imageUrl)
                                
                                let url = URL(string: markerValue.imageUrl)
                                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                aroundImageView.image = UIImage(data: data!)
                                
                            }
                            else{
                                aroundImageView.image = UIImage(named: markerValue.imageUrl)
                            }
                            
                            
                            aroundBackView.addSubview(aroundImageView)
                            
                            
                            titleLable.frame = CGRect.init(x: 10, y: 95, width: 150, height: 15)
                            titleLable.text = markerValue.name
                            print(markerValue.name)
                            print(2000+self.indexPos)
                            print("----===========>")
                            titleLable.textColor = UIColor.black
                            titleLable.font = UIFont.setTarcBold(size: 11.0)
                            aroundBackView.addSubview(titleLable)
                            titleDescTextView.frame = CGRect.init(x: 0, y: 115, width: 160, height: 55)
                            titleDescTextView.isEditable = false
                            titleDescTextView.isSelectable = false
                            titleDescTextView.font = UIFont.setTarcRegular(size: 10.0)
                            titleDescTextView.text = markerValue.formattedAddress
                            titleDescTextView.textColor = UIColor.black
                            aroundBackView.addSubview(titleDescTextView)
                            
                            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
                            tapGesture.numberOfTapsRequired = 1
                            aroundBackView.tag = 2000+self.indexPos
                            aroundBackView.addGestureRecognizer(tapGesture)
                            
                            aroundScrollView.contentSize = CGSize(width: aroundBackView.frame.origin.x+aroundBackView.frame.size.width+20, height: 80)
                            self.indexPos = self.indexPos+1
                            
                            //                            }
                            //                        }
                        }
                    }
                    self.hideLoader()
                    
                })
            }
            else
            {
                self.hideLoader()
                DispatchQueue.main.async {
                    self.listViewBtn.setBackgroundImage(UIImage(named: "listMenu_Icon"), for: .normal)
                }
                isListSelected = false
                UIView.animate(withDuration: 0,
                               delay: 0.1,
                               options: UIViewAnimationOptions.curveEaseInOut,
                               animations: { () -> Void in
                                self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                                
                }, completion: { (finished) -> Void in
                    self.listBackView = UIView()
                    self.indexPos = 0
                    self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                    self.listBackView.backgroundColor = UIColor.clear
                    self.view.addSubview(self.listBackView)
                })
            }
        } else {
            self.hideLoader()
            DispatchQueue.main.async {
                self.listViewBtn.setBackgroundImage(UIImage(named: "listMenu_Icon"), for: .normal)
            }
            isListSelected = false
            UIView.animate(withDuration: 0,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                            
            }, completion: { (finished) -> Void in
                self.listBackView = UIView()
                self.indexPos = 0
                self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                self.listBackView.backgroundColor = UIColor.clear
                self.view.addSubview(self.listBackView)
            })
        }
    }
    func closelistview(){
        if self.listViewBtn.currentBackgroundImage!.isEqual(UIImage(named: "white_Close")) {
            self.hideLoader()
            DispatchQueue.main.async {
                self.listViewBtn.setBackgroundImage(UIImage(named: "listMenu_Icon"), for: .normal)
            }
            isListSelected = false
            UIView.animate(withDuration: 0,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                            
            }, completion: { (finished) -> Void in
                //            self.listBackView = UIView()
                //            self.indexPos = 0
                //            self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                //            self.listBackView.backgroundColor = UIColor.clear
                //            self.view.addSubview(self.listBackView)
            })
        }
        
    }
    @objc func tapBlurButton(_ sender: UITapGestureRecognizer) {
        if self.markerDict.count > 0 {
        // print("Please Help! %d", sender.view?.tag ?? 0 )
        print(sender.view?.tag)
        let marker : GMSMarker = self.markerDict[(sender.view?.tag)!%2000] as! GMSMarker
        let url = "https://maps.googleapis.com/maps/api/geocode/json"
        let parameters: Parameters = [
            "latlng": "\(marker.position.latitude),\(marker.position.longitude)",
            "key": Key.GoogleAPIKey
        ]
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<MyLocationAddress>) in
                switch response.result {
                case .success:
                    //   print(response)
                    //to get status code
                    if response.result.value?.results != nil
                    {
                        
                        let weatherResponse = response.result.value?.results![0]
                        //  print(weatherResponse ?? "nil")
                        
                        self.destinationStringTrip = (weatherResponse?.placeId)!
                        self.destinationLatTrip = Double((weatherResponse?.geometry?.location?.lat)!)
                        self.destinationLngTrip = Double((weatherResponse?.geometry?.location?.lng)!)
                        self.destinationTextField.text = marker.title?.decodeEnt()
                    }
                case .failure(let error):
                    self.showErrorDialogBox(viewController: self)
                    print(error.localizedDescription)
                    print(error)
                }
        }
        
        if menuBackView.isHidden == false{
            UIView.animate(withDuration: 0,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseOut,
                           animations: { () -> Void in
                            self.amenityView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.size.width-80, height: self.view.frame.size.height-140)
                            self.menuBackView.frame = CGRect.init(x: 2000, y: 90, width: self.view.frame.size.width-80, height: self.view.frame.size.height-120)
                            
                            
            }, completion: { (finished) -> Void in
                self.blurEffectView.isHidden = true;
            })
        }
        
    }
    }
    @objc func amentiyAction() {
        
        if popupopen {
            self.menuButton.isEnabled = false
            popupopen = false
        }
        
        //
        self.hideLoader()
        DispatchQueue.main.async {
            self.listViewBtn.setBackgroundImage(UIImage(named: "listMenu_Icon"), for: .normal)
        }
        isListSelected = false
        UIView.animate(withDuration: 0,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
                        
        }, completion: { (finished) -> Void in
            self.listBackView = UIView()
            self.indexPos = 0
            self.listBackView.frame = CGRect.init(x:-2000, y: self.listViewBtn.frame.origin.y + self.listViewBtn.frame.size.width+10, width: self.view.frame.size.width, height: 200)
            self.listBackView.backgroundColor = UIColor.clear
            self.view.addSubview(self.listBackView)
        })
        
        
        //
        sourceTextField.resignFirstResponder()
        destinationTextField.resignFirstResponder()
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.prominent)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        amenityView = UIView()
        amenityView.frame = CGRect.init(x: 30, y: -2000, width: self.view.frame.size.width-60, height: self.view.frame.size.height-100)
        
        amenityView.backgroundColor = UIColor.clear
        blurEffectView.contentView.addSubview(amenityView)
        
        let closeButton = UIButton(frame: CGRect(x: 14, y: 16, width: 50, height: 50))
        closeButton.setImage(UIImage(named: "closeBtn"), for: .normal)
        self.isBackAmenities = true
        closeButton.addTarget(self, action: #selector(closebuttonAction), for: .touchUpInside)
        blurEffectView.contentView.addSubview(closeButton)
        
        UIView.animate(withDuration: 1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        if UIDevice.current.userInterfaceIdiom == .pad
                        {
                            self.amenityView.frame = CGRect.init(x: self.amenityViewX, y: self.amenityViewY, width: Int(self.view.frame.size.width) - self.amenityViewWidth, height: Int(self.view.frame.size.height) - self.amenityViewHeight)
                        }
                        else
                        {
                            self.amenityView.frame = CGRect.init(x: self.amenityViewX, y: self.amenityViewY, width: Int(self.view.frame.size.width) - self.amenityViewWidth, height: Int(self.view.frame.size.height) - self.amenityViewHeight)
                        }
        }, completion: { (finished) -> Void in
            
        })
        
        
        // Create an instance of UICollectionViewFlowLayout since you cant
        // Initialize UICollectionView without a layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: ((self.view.frame.size.width-80)/2)-1, height: ((self.view.frame.size.width-80)/2)-1)
        
        collectionview = UICollectionView(frame:CGRect.init(x: 4, y: 4, width: amenityView.frame.size.width-8, height: amenityView.frame.size.height-8), collectionViewLayout: layout)
        //print("frame : %@",collectionview.frame)
        collectionview.dataSource = self
        collectionview.delegate = self
        collectionview.bounces = false
        collectionview.register(AmenitiesCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionview.showsVerticalScrollIndicator = false
        collectionview.backgroundColor = UIColor.clear
        amenityView.addSubview(collectionview)
        
        paybackSlider.frame = CGRect.init(x: 80, y: self.view.frame.height-70, width: self.view.frame.width-160, height: 50)
        paybackSlider.minimumValue = 1
        paybackSlider.maximumValue = 20
        paybackSlider.isContinuous = true
        paybackSlider.tintColor = UIColor.red
        let userDefaults = UserDefaults.standard
        let distanceValue = userDefaults.integer(forKey: "distanceValue")
        if distanceValue == 0
        {
            paybackSlider.value = 1
            
        }
        else
        {
            paybackSlider.value = Float(distanceValue)
        }
        paybackSlider.isUserInteractionEnabled = true
        paybackSlider.addTarget(self, action: #selector(sliderValueDidChange), for: .valueChanged)
        paybackLabel.text = "\(paybackSlider.value)"
        blurEffectView.contentView.addSubview(paybackSlider)
        
        paybackLabel.frame = CGRect.init(x: paybackSlider.frame.size.width+paybackSlider.frame.origin.x+4, y: self.view.frame.height-70, width: 70, height: 50)
        sliderValue = Int(paybackSlider.value)
        if sliderValue == 1 {
            paybackLabel.text = "\(sliderValue) mile"
        }
        else
        {
            paybackLabel.text = "\(sliderValue) miles"
        }
        
        paybackLabel.textAlignment = NSTextAlignment.center
        paybackLabel.textColor = UIColor.black
        
        paybackLabel.font = UIFont.setTarcRegular(size: 17.0)
        blurEffectView.contentView.addSubview(paybackLabel)
        
        let radiusLabel = UILabel()
        radiusLabel.frame = CGRect.init(x: 10, y: self.view.frame.height-70, width: 70, height: 50)
        radiusLabel.text = "Radius"
        radiusLabel.textAlignment = NSTextAlignment.center
        radiusLabel.textColor = UIColor.black
        
        radiusLabel.font = UIFont.setTarcRegular(size: 17.0)
        blurEffectView.contentView.addSubview(radiusLabel)
        
        var drop: UIDropDown!
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            drop = UIDropDown(frame: CGRect(x: (blurEffectView.frame.size.width/2)-100, y:blurEffectView.frame.height-200 , width: 200, height: 30))
        }
        else
        {
            drop = UIDropDown(frame: CGRect(x: (blurEffectView.frame.size.width/2)-100, y:blurEffectView.frame.height-140 , width: 200, height: 30))
        }
        drop.placeholder = amenityTypeString
        drop.animationType = .Bouncing
        drop.hideOptionsWhenSelect = true
        drop.options = ["Near my source", "Near my destination"]
        drop.didSelect { (option, index) in
            // print("You just select: \(option) at index: \(index)")
            self.amenityTypeString = option
        }
        blurEffectView.contentView.addSubview(drop)
        
        blurEffectView.bringSubview(toFront: drop)
        
        
        
        
        
        
    }
    @objc func sliderValueDidChange(sender: UISlider!)
    {
        // print("payback value: \(sender.value)")
        sliderValue = Int(sender.value)
        let userDefaults = UserDefaults.standard
        userDefaults.set(sliderValue, forKey: "distanceValue")
        userDefaults.synchronize()
        if sliderValue == 1 {
            paybackLabel.text = "\(sliderValue) mile"
        }
        else
        {
            paybackLabel.text = "\(sliderValue) miles"
        }
    }
    
    
    func showAlertBox(text:String){
        self.hideLoader()
        let alertViewController = NYAlertViewController()
        // Set a title and message
        alertViewController.title = "Trip Planner"
        alertViewController.message = "\(text)"
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = self.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        // Add alert actions
        
        //        let activateAction = NYAlertAction(
        //            title: "Yes",
        //            style: .default,
        //            handler: { (action: NYAlertAction!) -> Void in
        //
        //                self.planTripAction()
        //
        //                self.dismiss(animated: true, completion: nil)
        //        }
        //        )
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                // self.sourceTextField.becomeFirstResponder()
                self.dismiss(animated: true, completion: nil)
        }
        )
        //        alertViewController.addAction(activateAction)
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
    }
    func showErrorDialogBox(viewController : UIViewController){
        let alertViewController = NYAlertViewController()
        alertViewController.title = "Error"
        alertViewController.message = "Server Error! Please try again later!"
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = viewController.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        // Add alert actions
        
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                
                viewController.dismiss(animated: true, completion: nil)
        }
        )
        
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        viewController.present(alertViewController, animated: true, completion: nil)
        
    }
    
    @objc func ChooseMode() {
        let actionController = YoutubeActionController()
        actionController.addAction(Action(ActionData(title: "Depart Now", image: UIImage(named: "clock")!), style: .default, handler: { action in
            self.Modetype = "DepartNow"
            self.scheduleLaterTextBtn.setTitle("Depart Now", for: UIControlState.normal)
            //self.getCinciPlaces(type: "Wineries_And_Vineyards")
        }))
        actionController.addAction(Action(ActionData(title: "Depart At", image: UIImage(named: "calander")!), style: .default, handler: { action in
            self.scheduleLaterTextBtn.setTitle("Depart At", for: UIControlState.normal)
            self.Modetype = "DepartAt"
            // self.getCinciPlaces(type: "Wineries_And_Vineyards")
            self.scheduleLaterAction()
        }))
        actionController.addAction(Action(ActionData(title: "Arrival At", image: UIImage(named: "calander")!), style: .default, handler: { action in
            self.Modetype = "ArivalAt"
            self.scheduleLaterTextBtn.setTitle("Arrival At", for: UIControlState.normal)
            self.scheduleLaterAction()
            // self.getCinciPlaces(type: "Wineries_And_Vineyards")
        }))
        actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
        
        present(actionController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func scheduleLaterAction() {
        //print("schedulealter")
        
        // if isshecduleLater
        // {
        isshecduleLater = false
        let currentdate = Date()
        let min = currentdate
        let max = Date().addingTimeInterval(60 * 60 * 24 * 7)
        datePicker = DateTimePicker.show(selected: currentdate, minimumDate: min, maximumDate: max)
        
        datePicker.isHidden = false
        //datePicker.minimumDate = Date()
        datePicker.highlightColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        datePicker.timeZone = NSTimeZone.local
        datePicker.todayButtonTitle = "Time Now"
        datePicker.is12HourFormat = true
        datePicker.isTimePickerOnly = false // to hide time and show only date picker
        datePicker.completionHandler = { date in
            // do something after tapping done
            //  print(date)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
            
            
            let timeStamp = dateFormatter.string(from: date)
            // print(Formatter.iso8601.date(from: timeStamp)!)
            self.scheduleLaterTime = date
        }
        //}
        //        else
        //        {
        //            isshecduleLater = true
        //            datePicker.isHidden = true
        //        }
    }
    
    func BirdAPI(){
        
    }
            @IBAction func planTripAction() {
                self.showLoader()
                BirdAPI()
                let souText = sourceTextField.text
                let desText = destinationTextField.text
                let userDefaults1 = UserDefaults.standard
                                          // let birdOn = userDefaults1.bool(forKey: "isBirdOn")
                                        //   let LimeOn = userDefaults1.bool(forKey: "LimeOn")
                                           let BoltOn = userDefaults1.bool(forKey: "BoltOn")
                if souText != "" && desText != ""
                {
                    if souText == desText
                    {
                        showAlertBox(text: "Source and Destination address are same. Cannot process your search.")
                        
                    }
                    else
                    {
                        comFunc.uberDataApi(sourceLat: sourceLatTrip, sourceLng: sourceLngTrip, destinationLng: destinationLngTrip, destinationLat: destinationLatTrip)
                        comFunc.GetLYFT(SourceLat: sourceLatTrip, SourceLong: sourceLngTrip, DestinationLat: destinationLatTrip, DestinationLong: destinationLngTrip)
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripSelectionViewController") as! TripSelectionViewController
                        
                        nextViewController.AmenititesTypeTrip = self.typeString
                        nextViewController.ameniticsTitle = self.ameniticsTitle
                        
                        if souText == "Current Location"
                        {
                            
                            
                    let locationString = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
                            
                            let url = "https://maps.googleapis.com/maps/api/geocode/json"
                            let parameters: Parameters = [
                                "latlng": locationString,
                                "key": Key.GoogleAPIKey
                            ]
                            
                            //print(url)
                            // print(parameters)
                            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                                .responseObject{ (response: DataResponse<MyLocationAddress>) in
                                    switch response.result {
                                    case .success:
                                        //  print(response)
                                        //to get status code
                                        
                                        self.firstServiceCallComplete = true
                                        let weatherResponse = response.result.value?.results![0]
                                        //  print(weatherResponse ?? "nil")
                                        print(self.destinationLatTrip)
                                        print(self.destinationLngTrip)
                                        
                                        nextViewController.sourceString = (weatherResponse?.placeId)!
                                        nextViewController.modetype = self.Modetype
                                        nextViewController.sourceLat = Double((weatherResponse?.geometry?.location?.lat)!)
                                        nextViewController.sourceLng = Double((weatherResponse?.geometry?.location?.lng)!)
                                        // print((self.destinationPlace?.formattedAddress)!)
                                        nextViewController.destinationString = self.destinationStringTrip
                                        nextViewController.destinationLat = self.destinationLatTrip
                                        nextViewController.destinationLng = self.destinationLngTrip
                                        nextViewController.tripSchduleTime = self.scheduleLaterTime
                                        nextViewController.isCalenderEvent = self.calendarEvent
                                        nextViewController.sourceaddressString = souText
                                        nextViewController.DestinationAddressString = desText
                                        
                                        var CheckBikeCount = 0
                                        
                                        Alamofire.request("https://mds.bird.co/gbfs/louisville/free_bikes").responseObject { (response: DataResponse<getbirdstation>) in
                                            switch response.result {
                                            case .success:
                                                self.secondServiceCallComplete = true
                                                if response.result.value?.code != 400 {
                                                    if  response.result.value!.datalistfromapi!.count > 0{
                                                        let Sourcelatlatlong = CLLocation(latitude: Double((weatherResponse?.geometry?.location?.lat)!), longitude: Double((weatherResponse?.geometry?.location?.lng)!))
                                                        
                                let responseResutl = response.result.value
            self.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
        nextViewController.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                                                        self.birdListApi = responseResutl?.datalistfromapi
                                                        nextViewController.birdListApi = self.birdListApi
                                                        
                                                        
                                                        for BirdBike in (responseResutl?.datalistfromapi)! {
                                                            
                                                            
                    let ss = CLLocation(latitude: BirdBike.bike_lat!, longitude: BirdBike.bike_lon!)
                        let dd = Sourcelatlatlong.distance(from: ss)
                                                            //  print(dd)
                                                            // print((dd*100).rounded()/100)
                                                            
                                                            
                                                            
                                                            
                                                            if CheckBikeCount == 0 {
                                                                
                                                                self.BirdBikeID = BirdBike.bike_id!
                                                                self.BirdBikeLat = BirdBike.bike_lat!
                                                                self.BirdBikeLon = BirdBike.bike_lon!
                                                                self.BirdIsReserved = BirdBike.bike_is_reserved!
                                                                self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                                                let destinationlatlong = CLLocation(latitude: self.BirdBikeLat, longitude: self.BirdBikeLon)
                                                                self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                              
                                                                CheckBikeCount = CheckBikeCount + 1
                                                                
                                                                
                                                                nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                                nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                                
                                                                
                                                                //print(self.BirdSourceDistance)
                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                
                                                                
                                                            }
                                                            else{
                                                                self.templat = BirdBike.bike_lat!
                                                                self.templong = BirdBike.bike_lon!
                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                    if self.BirdtempDistance < self.BirdSourceDistance {
                                                                    
                            self.BirdBikeID = BirdBike.bike_id!
                    self.BirdBikeLat = BirdBike.bike_lat!
                    self.BirdBikeLon = BirdBike.bike_lon!
            self.BirdIsReserved = BirdBike.bike_is_reserved!
            self.BirdIsDisabled = BirdBike.bike_is_disabled!
            self.BirdSourceDistance = self.BirdtempDistance
                                                                    nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                                    nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                                    // print(self.BirdtempDistance)
                                                                    nextViewController.BikedistanceInMeters = self.BirdtempDistance
                                                                    CheckBikeCount = CheckBikeCount + 1
                                                                    
                                                                }
                                                                else{
                                                                    CheckBikeCount = CheckBikeCount + 1
                                                                }
                                                            }
                                                            
                                                            
                                                            
                                        //401 w cardinal blvd, l
                                                            
                                                        }
                                                        print("BirdLat\(self.BirdBikeLat) and long \(self.BirdBikeLon)")
                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                        
                                                        //Lime Integtation
                                                        Alamofire.request("https://mds.bird.co/gbfs/louisville/free_bikes").responseObject { (response: DataResponse<getbirdstation>) in
                                                            switch response.result {
                                                            case .success:
                                                                self.secondServiceCallComplete = true
                                                                if response.result.value?.code != 400 {
                                                                    if response.result.value!.datalistfromapi!.count > 0{
                                                                        let Sourcelatlatlong = CLLocation(latitude: Double((weatherResponse?.geometry?.location?.lat)!), longitude: Double((weatherResponse?.geometry?.location?.lng)!))
                                                                        
                                                                        let responseResutl = response.result.value
                                                                        self.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                        nextViewController.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                        
                                                                        self.birdListApi = responseResutl?.datalistfromapi
                                                                        nextViewController.birdListApi = self.birdListApi
                                                                        for BirdBike in (responseResutl?.datalistfromapi)! {
                                                                            
                                                                            
                                                                            let ss = CLLocation(latitude: BirdBike.bike_lat!, longitude: BirdBike.bike_lon!)
                                                                            let dd = Sourcelatlatlong.distance(from: ss)
                                                                            print(dd)
                                                                            // print((dd*100).rounded()/100)
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            if CheckBikeCount == 0 {
                                                                                
                                                                                self.BirdBikeID = BirdBike.bike_id!
                                                                                self.BirdBikeLat = BirdBike.bike_lat!
                                                                                self.BirdBikeLon = BirdBike.bike_lon!
                                                                                self.BirdIsReserved = BirdBike.bike_is_reserved!
                                                                                self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                                                                let destinationlatlong = CLLocation(latitude: self.BirdBikeLat, longitude: self.BirdBikeLon)
                                                                                self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                                CheckBikeCount = CheckBikeCount + 1
                                                                                
                                                                                nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                                                nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                                                
                                                                                
                                                                                //print(self.BirdSourceDistance)
                                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            else{
                                                                                self.templat = BirdBike.bike_lat!
                                                                                self.templong = BirdBike.bike_lon!
                                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                                if self.BirdtempDistance < self.BirdSourceDistance {
                                                                                    
                                                                                    self.BirdBikeID = BirdBike.bike_id!
                                                                                    self.BirdBikeLat = BirdBike.bike_lat!
                                                                                    self.BirdBikeLon = BirdBike.bike_lon!
                                                                                    self.BirdIsReserved = BirdBike.bike_is_reserved!
                                                                                    self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                                                                    self.BirdSourceDistance = self.BirdtempDistance
                                                                                    nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                                                    nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                                                    // print(self.BirdtempDistance)
                                                                                    nextViewController.BikedistanceInMeters = self.BirdtempDistance
                                                                                    CheckBikeCount = CheckBikeCount + 1
                                                                                    
                                                                                }
                                                                                else{
                                                                                    CheckBikeCount = CheckBikeCount + 1
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                        
                                                                        // Bolt Integration
                                                                        var BoltCheckBikeCount = 0
                                                                        
                                                                       if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                            switch response.result {
                                                                            case .success:
                                                                                self.secondServiceCallComplete = true
                                                                                if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                    if (response.result.value?.total)! >= 0  && response.result.value!.datalistfromapi!.count > 0{
                                                                                        let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                                        
                                                                                        let responseResutl = response.result.value
                                                                                        print("\((responseResutl?.datalistfromapi?.count)!)")
                                                                           
                                                                                        nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                        self.boltListApi = responseResutl?.datalistfromapi
                                                                                        
                                                                                        for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                            let BirdBikeLat = LimeBike.bike_lat! as NSString
                                                                                            let BirdBikeLng = LimeBike.bike_lon! as NSString
                                                    let BoltLatitude = Double("\(BirdBikeLat)")
                                                    let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                            
                                                    let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                                            //  print(dd)
                                                                                            // print((dd*100).rounded()/100)
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                            if BoltCheckBikeCount == 0 {
                                                                                                
                                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                
                                                                                                nextViewController.BoltBikeLat = BoltLatitude
                                                                                                nextViewController.BoltBikeLon = BoltLontitude
                                                                                                
                                                                                                
                                                                                                //print(self.BirdSourceDistance)
                                                                                                nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                
                                                                                                
                                                                                                
                                                                                            }
                                                                                            else{
                                                                                                self.templat = BoltLatitude!
                                                                                                self.templong = BoltLontitude!
                                                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                    
                                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                    self.BoltSourceDistance = self.BolttempDistance
                                                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                    // print(self.BirdtempDistance)
                                                                                                    nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                    
                                                                                                }
                                                                                                else{
                                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                }
                                                                                            }
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                            
                                                                                        }
                                                                                        
                                                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                        
                                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                        
                                                                                        self.hideLoader()
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                    }
                                                                                }
                                                                                else{
                                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                    self.hideLoader()
                                                                                    
                                                                                }
                                                                                
                                                                            case .failure(let error):
                                                                                print(error)
                                                                                
                                                                            }
                                                                        }
                                                                    }
                                                                      else
                                                                       {
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                }
                                                                else{
                                                                    // Bolt Integration
                                                                    var BoltCheckBikeCount = 0
                                                                    
                                                                   if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                        switch response.result {
                                                                        case .success:
                                                                            self.secondServiceCallComplete = true
                                                                            if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                if response.result.value!.datalistfromapi!.count > 0{
                                                                                    let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                                    
                                                                                    let responseResutl = response.result.value
                                                                                    print("\(responseResutl?.datalistfromapi?.count)")
                                                                                    self.boltListApi = responseResutl?.datalistfromapi
                                                                                    nextViewController.boltListApi = self.boltListApi
                                                                                    
                                                                            
                                                                                    nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                    for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                        let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                        let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                        let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                        let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                        
                                                                                        let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                print(dd)
                                                                                        // print((dd*100).rounded()/100)
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        if BoltCheckBikeCount == 0 {
                                                                                            
                                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                                            self.BoltBikeLat = BoltLatitude!
                                                                                            self.BoltBikeLon = BoltLontitude!
                                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                            let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                            self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                            
                                                                                            nextViewController.BoltBikeLat = BoltLatitude
                                                                                            nextViewController.BoltBikeLon = BoltLontitude
                                                                                            
                                                                                            
                                                                                            //print(self.BirdSourceDistance)
                                                                                            nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                            
                                                                                            
                                                                                            
                                                                                        }
                                                                                        else{
                                                                                            self.templat = BoltLatitude!
                                                                                            self.templong = BoltLontitude!
                                                                                            let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                            self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                            if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                
                                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                self.BoltSourceDistance = self.BolttempDistance
                                                                                                nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                // print(self.BirdtempDistance)
                                                                                                nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                
                                                                                            }
                                                                                            else{
                                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                            }
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                        
                                                                                    }
                                                                                    
                                                                                    nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                    
                                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                    
                                                                                    self.hideLoader()
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                            }
                                                                            else{
                                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                self.hideLoader()
                                                                                
                                                                            }
                                                                            
                                                                        case .failure(let error):
                                                                            print(error)
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                                }
                                                          else
                                                                   {
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                    }
                                                                }
                                                                
                                                            case .failure(let error):
                                                                print(error)
                                                                
                                                            }
                                                        }
                                                        
                                                        
                                                        ///*
                                                        
                                                        
                                                        
                                                        ///*
                                                    }
                                                    else{
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                   self.hideLoader()
                                                    }
                                                }
                                                else{
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    self.hideLoader()
                                                    
                                                }
                                                
                                            case .failure(let error):
                                               self.hideLoader()

                                                print(error)
                                                
                                            }
                                        }
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                    case .failure(let error):
                                        self.showErrorDialogBox(viewController: self)
                                        
                                        print(error.localizedDescription)
                                    }
                                    
                            }
                            
                        }
                        else
                        {
                            nextViewController.modetype = self.Modetype
                            nextViewController.sourceString = self.sourceStringTrip
                            nextViewController.tripSchduleTime = self.scheduleLaterTime
                            nextViewController.sourceLat = self.sourceLatTrip
                            nextViewController.sourceLng = self.sourceLngTrip
                            nextViewController.destinationString = self.destinationStringTrip
                            nextViewController.destinationLat = self.destinationLatTrip
                            nextViewController.destinationLng = self.destinationLngTrip
                            nextViewController.isCalenderEvent = calendarEvent
                            nextViewController.sourceaddressString = souText
                            nextViewController.DestinationAddressString = desText
                            var CheckBikeCount = 0
                            let userDefaults1 = UserDefaults.standard
                            let birdOn = userDefaults1.bool(forKey: "isBirdOn")
                            let LimeOn = userDefaults1.bool(forKey: "LimeOn")
                            let BoltOn = userDefaults1.bool(forKey: "BoltOn")
                            
                    
                            
                            
                            if birdOn {
                            Alamofire.request("https://mds.bird.co/gbfs/louisville/free_bikes").responseObject { (response: DataResponse<getbirdstation>) in
                                switch response.result {
                                case .success:
                                    self.secondServiceCallComplete = true
                                    if response.result.value?.code != 400 && response.result.value!.datalistfromapi! != nil{
                                        if  response.result.value!.datalistfromapi!.count > 0 {
                                            let Sourcelatlatlong = CLLocation(latitude: self.sourceLatTrip, longitude: self.sourceLngTrip)
                                            
                                            let responseResutl = response.result.value
                                            self.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                                            
                                            nextViewController.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                                            self.birdListApi = responseResutl?.datalistfromapi
                                            nextViewController.birdListApi = self.birdListApi
                                            
                                            for BirdBike in (responseResutl?.datalistfromapi)! {
                                                //
                                                //
                                                //                                    let BirdLatLong = CLLocation(latitude: BirdBike.bike_lat!, longitude: BirdBike.bike_lon!)
                                                //                                    let dd = Sourcelatlatlong.distance(from: BirdLatLong)
                                                //                                    print(dd)
                                                //                                    print((dd*100).rounded()/100)
                                                
                                                
                                                
                                                
                                                if CheckBikeCount == 0 {
                                                    
                                                    self.BirdBikeID = BirdBike.bike_id!
                                                    self.BirdBikeLat = BirdBike.bike_lat!
                                                    self.BirdBikeLon = BirdBike.bike_lon!
                                                    self.BirdIsReserved = BirdBike.bike_is_reserved!
                                                    self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                                    let destinationlatlong = CLLocation(latitude: self.BirdBikeLat, longitude: self.BirdBikeLon)
                                                    self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                    self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                    CheckBikeCount = CheckBikeCount + 1
                                                    nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                    nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                    // print(self.BirdSourceDistance)
                                                    nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                }
                                                else{
                                                    self.templat = BirdBike.bike_lat!
                                                    self.templong = BirdBike.bike_lon!
                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                    self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                    self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                    if self.BirdtempDistance < self.BirdSourceDistance {
                                                        
                                                        self.BirdBikeID = BirdBike.bike_id!
                                                        self.BirdBikeLat = BirdBike.bike_lat!
                                                        self.BirdBikeLon = BirdBike.bike_lon!
                                                        self.BirdIsReserved = BirdBike.bike_is_reserved!
                                                        self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                                        self.BirdSourceDistance = self.BirdtempDistance
                                                        CheckBikeCount = CheckBikeCount + 1
                                                        nextViewController.BirdBikeLat = BirdBike.bike_lat!
                                                        nextViewController.BirdBikeLon = BirdBike.bike_lon!
                                                        // print(self.BirdtempDistance)
                                                        // print("Karthick")
                                                        nextViewController.BikedistanceInMeters = self.BirdtempDistance
                                                        
                                                    }
                                                    else{
                                                        CheckBikeCount = CheckBikeCount + 1
                                                    }
                                                }
                                                
                                                
                                                
                                                
                                                
                                            }
                                            
                                            // Lime Integration
                                            var LimeCheckBikeCount = 0
                                            if LimeOn
                                            {
                                            Alamofire.request("https://data.lime.bike/api/partners/v1/gbfs/louisville/free_bike_status").responseObject { (response: DataResponse<getLimestation>) in
                                                switch response.result {
                                                case .success:
                                                    self.secondServiceCallComplete = true
                                                    if response.result.value?.code != 400 {
                                                        if response.result.value!.datalistfromapi!.count > 0{
                                                            let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                            
                                                            let responseResutl = response.result.value
                                                            print("\(responseResutl?.datalistfromapi?.count)")
                                                           
                                                            nextViewController.LimeDatalistapicount = responseResutl?.datalistfromapi?.count
                                                            
                                                            self.LimeListApi = responseResutl?.datalistfromapi
                                                            nextViewController.LimeListApi = self.LimeListApi
                                                            
                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                let LimeLatitude = Double("\(BirdBikeLat)")
                                                                let LimeLontitude = Double("\(BirdBikeLng)")
                                                                
                                                                let ss = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                //  print(dd)
                                                                // print((dd*100).rounded()/100)
                                                                
                                                                
                                                                
                                                                
                                                                if LimeCheckBikeCount == 0 {
                                                                    if LimeBike.bike_id != nil
                                                                    {
                                                                        self.LimeBikeID = LimeBike.bike_id!

                                                                    }
                                                                    else
                                                                    {
                                                                        self.LimeBikeID = ""

                                                                    }
                                                                    self.LimeBikeLat = LimeLatitude!
                                                                    self.LimeBikeLon = LimeLontitude!
                                                                    self.LimeIsReserved = LimeBike.bike_is_reserved!
                                                                    self.LimeIsDisabled = LimeBike.bike_is_disabled!
                                                                    let destinationlatlong = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                    self.LimedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                    LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                    
                                                                    nextViewController.LimeBikeLat = LimeLatitude
                                                                    nextViewController.LimeBikeLon = LimeLontitude
                                                                    
                                                                    
                                                                    //print(self.BirdSourceDistance)
                                                                    nextViewController.LimedistanceInMeters = self.BirdSourceDistance
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    self.templat = LimeLatitude!
                                                                    self.templong = LimeLontitude!
                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                    self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                    if self.BirdtempDistance < self.BirdSourceDistance {
                                                                        if LimeBike.bike_id != nil
                                                                        {
                                                                            self.BirdBikeID = LimeBike.bike_id!

                                                                        }
                                                                        else
                                                                        {
                                                                            self.BirdBikeID = ""

                                                                        }
                                                                        self.BirdBikeLat = LimeLatitude!
                                                                        self.BirdBikeLon = LimeLontitude!
                                                                        self.BirdIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BirdIsDisabled = LimeBike.bike_is_disabled!
                                                                        self.BirdSourceDistance = self.BirdtempDistance
                                                                        nextViewController.LimeBikeLat = LimeLatitude!
                                                                        nextViewController.LimeBikeLon = LimeLontitude!
                                                                        // print(self.BirdtempDistance)
                                                                        nextViewController.LimedistanceInMeters = self.BirdtempDistance
                                                                        LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                        
                                                                    }
                                                                    else{
                                                                        LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                    }
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                            
                                                            
                                                            // Bolt Integration
                                                            var BoltCheckBikeCount = 0
                                                            
                                                            if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                switch response.result {
                                                                case .success:
                                                                    self.secondServiceCallComplete = true
                                                                    if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                        if response.result.value!.datalistfromapi!.count > 0{
                                                                            let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                            
                                                                            let responseResutl = response.result.value
                                                                            print("\(responseResutl?.datalistfromapi?.count)")
                                                                       
                                                                            nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                            self.boltListApi = responseResutl?.datalistfromapi
                                                                            nextViewController.boltListApi = self.boltListApi
                                                                            
                                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                
                                                                                let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                                //  print(dd)
                                                                                // print((dd*100).rounded()/100)
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                if BoltCheckBikeCount == 0 {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                    nextViewController.BoltBikeLat = BoltLatitude
                                                                                    nextViewController.BoltBikeLon = BoltLontitude
                                                                                    
                                                                                    
                                                                                    //print(self.BirdSourceDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                else{
                                                                                    self.templat = BoltLatitude!
                                                                                    self.templong = BoltLontitude!
                                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    if self.BolttempDistance < self.BoltSourceDistance {
                                                                                        
                                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                                        self.BoltBikeLat = BoltLatitude!
                                                                                        self.BoltBikeLon = BoltLontitude!
                                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                        self.BoltSourceDistance = self.BolttempDistance
                                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                                        // print(self.BirdtempDistance)
                                                                                        nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        
                                                                                    }
                                                                                    else{
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                            
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            
                                                                            self.hideLoader()
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        else{
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            self.hideLoader()
                                                                        }
                                                                    }
                                                                    else{
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                        
                                                                    }
                                                                    
                                                                case .failure(let error):
                                                                    print(error)
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                   self.hideLoader()
                                                                    
                                                                }
                                                            }
                                                        }
                                                         else
                                                            {
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                            }
                                                            
                                                            
                                                            
                                                        }
                                                    }
                                                    else{
                                                        
                                                        // Bolt Integration
                                                        var BoltCheckBikeCount = 0
                                                        if BoltOn {
                                                        Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                            switch response.result {
                                                            case .success:
                                                                self.secondServiceCallComplete = true
                                                                if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                    if response.result.value!.datalistfromapi!.count > 0{
                                                                        let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                        
                                                                        let responseResutl = response.result.value
                                                                        print("\(responseResutl?.datalistfromapi?.count)")
                                                                     
                                                                        nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                        self.boltListApi = responseResutl?.datalistfromapi
                                                                        nextViewController.boltListApi = self.boltListApi
                                                                        
                                                                        for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                            let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                            let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                            let BoltLatitude = Double("\(BirdBikeLat)")
                                                                            let BoltLontitude = Double("\(BirdBikeLng)")
                                                                            
                                                                            let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                            let dd = Sourcelatlatlong.distance(from: ss)
                                                                            //  print(dd)
                                                                            // print((dd*100).rounded()/100)
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            if BoltCheckBikeCount == 0 {
                                                                                
                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                
                                                                                nextViewController.BoltBikeLat = BoltLatitude
                                                                                nextViewController.BoltBikeLon = BoltLontitude
                                                                                
                                                                                
                                                                                //print(self.BirdSourceDistance)
                                                                                nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            else{
                                                                                self.templat = BoltLatitude!
                                                                                self.templong = BoltLontitude!
                                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                if self.BolttempDistance < self.BoltSourceDistance {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    self.BoltSourceDistance = self.BolttempDistance
                                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                                    // print(self.BirdtempDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                }
                                                                                else{
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                        
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        
                                                                        self.hideLoader()
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                }
                                                                else{
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                    
                                                                }
                                                                
                                                            case .failure(let error):
                                                                print(error)
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                           //self.hideLoader()
                                                                self.hideLoader()
                                                            }
                                                        }
                                                    }
                                                        else
                                                        {
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                                                  //self.hideLoader()
                                                                                                                       self.hideLoader()
                                                        }
                                                    }
                                                    
                                                case .failure(let error):
                                                    print(error)
                                                    // Bolt Integration
                                                    var BoltCheckBikeCount = 0
                                                    
                                                   if BoltOn
                                                   { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                        switch response.result {
                                                        case .success:
                                                            self.secondServiceCallComplete = true
                                                            if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                if response.result.value!.datalistfromapi!.count > 0{
                                                                    let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                    
                                                                    let responseResutl = response.result.value
                                                                    print("\(responseResutl?.datalistfromapi?.count)")
                                                                 
                                                                    nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                    self.boltListApi = responseResutl?.datalistfromapi
                                                                    nextViewController.boltListApi = self.boltListApi
                                                                    
                                                                    for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                        let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                        let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                        let BoltLatitude = Double("\(BirdBikeLat)")
                                                                        let BoltLontitude = Double("\(BirdBikeLng)")
                                                                        
                                                                        let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                        let dd = Sourcelatlatlong.distance(from: ss)
                                                                        //  print(dd)
                                                                        // print((dd*100).rounded()/100)
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        if BoltCheckBikeCount == 0 {
                                                                            
                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                            self.BoltBikeLat = BoltLatitude!
                                                                            self.BoltBikeLon = BoltLontitude!
                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                            let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                            self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            
                                                                            nextViewController.BoltBikeLat = BoltLatitude
                                                                            nextViewController.BoltBikeLon = BoltLontitude
                                                                            
                                                                            
                                                                            //print(self.BirdSourceDistance)
                                                                            nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        else{
                                                                            self.templat = BoltLatitude!
                                                                            self.templong = BoltLontitude!
                                                                            let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                            self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                            if self.BolttempDistance < self.BoltSourceDistance {
                                                                                
                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                self.BoltSourceDistance = self.BolttempDistance
                                                                                nextViewController.BoltBikeLat = BoltLatitude!
                                                                                nextViewController.BoltBikeLon = BoltLontitude!
                                                                                // print(self.BirdtempDistance)
                                                                                nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                
                                                                            }
                                                                            else{
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                    
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    
                                                                    self.hideLoader()
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else
                                                                {
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                          self.hideLoader()
                                                                }
                                                            }
                                                            else{
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                                
                                                            }
                                                            
                                                        case .failure(let error):
                                                            print(error)
                                                          
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                       self.hideLoader()
                                                            
                                                        }
                                                    }
                                                }
                                                    else
                                                   {
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    self.hideLoader()
                                                    }
                                                }
                                            }
                                            }
                                            else
                                            {

                                                // Bolt Integration
                                                var BoltCheckBikeCount = 0
                                                if BoltOn
                                                {
                                                Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                    switch response.result {
                                                    case .success:
                                                        self.secondServiceCallComplete = true
                                                        if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                            if response.result.value!.datalistfromapi!.count > 0{
                                                                let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                
                                                                let responseResutl = response.result.value
                                                                print("\(responseResutl?.datalistfromapi?.count)")
                                                           
                                                                nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                self.boltListApi = responseResutl?.datalistfromapi
                                                                nextViewController.boltListApi = self.boltListApi
                                                                
                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                    let BoltLatitude = Double("\(BirdBikeLat)")
                                                                    let BoltLontitude = Double("\(BirdBikeLng)")
                                                                    
                                                                    let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                    //  print(dd)
                                                                    // print((dd*100).rounded()/100)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    if BoltCheckBikeCount == 0 {
                                                                        
                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                        self.BoltBikeLat = BoltLatitude!
                                                                        self.BoltBikeLon = BoltLontitude!
                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                        let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        
                                                                        nextViewController.BoltBikeLat = BoltLatitude
                                                                        nextViewController.BoltBikeLon = BoltLontitude
                                                                        
                                                                        
                                                                        //print(self.BirdSourceDistance)
                                                                        nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.templat = BoltLatitude!
                                                                        self.templong = BoltLontitude!
                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        if self.BolttempDistance < self.BoltSourceDistance {
                                                                            
                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                            self.BoltBikeLat = BoltLatitude!
                                                                            self.BoltBikeLon = BoltLontitude!
                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                            self.BoltSourceDistance = self.BolttempDistance
                                                                            nextViewController.BoltBikeLat = BoltLatitude!
                                                                            nextViewController.BoltBikeLon = BoltLontitude!
                                                                            // print(self.BirdtempDistance)
                                                                            nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            
                                                                        }
                                                                        else{
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                
                                                                self.hideLoader()
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            else{
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                            }
                                                        }
                                                        else{
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                            self.hideLoader()
                                                            
                                                        }
                                                        
                                                    case .failure(let error):
                                                        print(error)
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                       self.hideLoader()
                                                        
                                                    }
                                                }
                                            }
                                                else
                                                {
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    self.hideLoader()
                                                }
                                            }
                                            
                                        }
                                        else{
                                            
                                            var LimeCheckBikeCount = 0
                                            if LimeOn
                                            {
                                            Alamofire.request("https://data.lime.bike/api/partners/v1/gbfs/louisville/free_bike_status").responseObject { (response: DataResponse<getLimestation>) in
                                                switch response.result {
                                                case .success:
                                                    self.secondServiceCallComplete = true
                                                    if response.result.value?.code != 400 {
                                                        if response.result.value!.datalistfromapi!.count > 0{
                                                            let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                            
                                                            let responseResutl = response.result.value
                                                            print("\(responseResutl?.datalistfromapi?.count)")
                                                           
                                                            nextViewController.LimeDatalistapicount = responseResutl?.datalistfromapi?.count
                                                            
                                                            self.LimeListApi = responseResutl?.datalistfromapi
                                                            nextViewController.LimeListApi = self.LimeListApi
                                                            
                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                let LimeLatitude = Double("\(BirdBikeLat)")
                                                                let LimeLontitude = Double("\(BirdBikeLng)")
                                                                
                                                                let ss = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                //  print(dd)
                                                                // print((dd*100).rounded()/100)
                                                                
                                                                
                                                                
                                                                
                                                                if LimeCheckBikeCount == 0 {
                                                                    if LimeBike.bike_id != nil
                                                                    {
                                                                        self.LimeBikeID = LimeBike.bike_id!

                                                                    }
                                                                    else
                                                                    {
                                                                        self.LimeBikeID = ""

                                                                    }
                                                                    self.LimeBikeLat = LimeLatitude!
                                                                    self.LimeBikeLon = LimeLontitude!
                                                                    self.LimeIsReserved = LimeBike.bike_is_reserved!
                                                                    self.LimeIsDisabled = LimeBike.bike_is_disabled!
                                                                    let destinationlatlong = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                    self.LimedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                    LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                    
                                                                    nextViewController.LimeBikeLat = LimeLatitude
                                                                    nextViewController.LimeBikeLon = LimeLontitude
                                                                    
                                                                    
                                                                    //print(self.BirdSourceDistance)
                                                                    nextViewController.LimedistanceInMeters = self.BirdSourceDistance
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    self.templat = LimeLatitude!
                                                                    self.templong = LimeLontitude!
                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                    self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                    if self.BirdtempDistance < self.BirdSourceDistance {
                                                                        if LimeBike.bike_id != nil
                                                                        {
                                                                            self.BirdBikeID = LimeBike.bike_id!

                                                                        }
                                                                        else
                                                                        {
                                                                            self.BirdBikeID = ""

                                                                        }
                                                                        self.BirdBikeLat = LimeLatitude!
                                                                        self.BirdBikeLon = LimeLontitude!
                                                                        self.BirdIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BirdIsDisabled = LimeBike.bike_is_disabled!
                                                                        self.BirdSourceDistance = self.BirdtempDistance
                                                                        nextViewController.LimeBikeLat = LimeLatitude!
                                                                        nextViewController.LimeBikeLon = LimeLontitude!
                                                                        // print(self.BirdtempDistance)
                                                                        nextViewController.LimedistanceInMeters = self.BirdtempDistance
                                                                        LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                        
                                                                    }
                                                                    else{
                                                                        LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                    }
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                            
                                                            
                                                            // Bolt Integration
                                                            var BoltCheckBikeCount = 0
                                                            if BoltOn
                                                            {
                                                            Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                switch response.result {
                                                                case .success:
                                                                    self.secondServiceCallComplete = true
                                                                    if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                        if (response.result.value?.total)! >= 0 {
                                                                            let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                            
                                                                            let responseResutl = response.result.value
                                                                            print("\(responseResutl?.datalistfromapi?.count)")
                                                                       
                                                                            nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                            self.boltListApi = responseResutl?.datalistfromapi
                                                                            nextViewController.boltListApi = self.boltListApi
                                                                            
                                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                
                                                                                let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                                //  print(dd)
                                                                                // print((dd*100).rounded()/100)
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                if BoltCheckBikeCount == 0 {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                    nextViewController.BoltBikeLat = BoltLatitude
                                                                                    nextViewController.BoltBikeLon = BoltLontitude
                                                                                    
                                                                                    
                                                                                    //print(self.BirdSourceDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                else{
                                                                                    self.templat = BoltLatitude!
                                                                                    self.templong = BoltLontitude!
                                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    if self.BolttempDistance < self.BoltSourceDistance {
                                                                                        
                                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                                        self.BoltBikeLat = BoltLatitude!
                                                                                        self.BoltBikeLon = BoltLontitude!
                                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                        self.BoltSourceDistance = self.BolttempDistance
                                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                                        // print(self.BirdtempDistance)
                                                                                        nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        
                                                                                    }
                                                                                    else{
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                            
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            
                                                                            self.hideLoader()
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                    }
                                                                    else{
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                        
                                                                    }
                                                                    
                                                                case .failure(let error):
                                                                    print(error)
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                   self.hideLoader()
                                                                    
                                                                }
                                                            }
                                                        }
                                                          else
                                                            {
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                                                                              self.hideLoader()
                                                            }
                                                            
                                                            
                                                            
                                                        }
                                                        else{
                                                            
                                                            // Bolt Integration
                                                            var BoltCheckBikeCount = 0
                                                            if BoltOn
                                                            {
                                                            Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                switch response.result {
                                                                case .success:
                                                                    self.secondServiceCallComplete = true
                                                                    if response.result.value?.code != 400 {
                                                                        if response.result.value!.datalistfromapi!.count > 0{
                                                                            let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                            
                                                                            let responseResutl = response.result.value
                                                                            print("\(responseResutl?.datalistfromapi?.count)")
                                                                         
                                                                            nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                            self.boltListApi = responseResutl?.datalistfromapi
                                                                            nextViewController.boltListApi = self.boltListApi
                                                                            
                                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                
                                                                                let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                                //  print(dd)
                                                                                // print((dd*100).rounded()/100)
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                if BoltCheckBikeCount == 0 {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                    nextViewController.BoltBikeLat = BoltLatitude
                                                                                    nextViewController.BoltBikeLon = BoltLontitude
                                                                                    
                                                                                    
                                                                                    //print(self.BirdSourceDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                    
                                                                                    
                                                                                    
                                                                                }
                                                                                else{
                                                                                    self.templat = BoltLatitude!
                                                                                    self.templong = BoltLontitude!
                                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                    self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                    if self.BolttempDistance < self.BoltSourceDistance {
                                                                                        
                                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                                        self.BoltBikeLat = BoltLatitude!
                                                                                        self.BoltBikeLon = BoltLontitude!
                                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                        self.BoltSourceDistance = self.BolttempDistance
                                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                                        // print(self.BirdtempDistance)
                                                                                        nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                        
                                                                                    }
                                                                                    else{
                                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    }
                                                                                }
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            
                                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                            
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            
                                                                            self.hideLoader()
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        else{
                                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                           self.hideLoader()
                                                                        }
                                                                    }
                                                                    else{
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                        
                                                                    }
                                                                    
                                                                case .failure(let error):
                                                                    print(error)
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                            self.hideLoader()
                                                                    
                                                                }
                                                            }
                                                        }
                                                            else
                                                            {
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                       self.hideLoader()
                                                            }
                                                        }
                                                    }
                                                    else{
                                                        
                                                        // Bolt Integration
                                                        var BoltCheckBikeCount = 0
                                                        
                                                       if BoltOn
                                                       { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                            switch response.result {
                                                            case .success:
                                                                self.secondServiceCallComplete = true
                                                                if response.result.value?.code != 400 && response.result.value?.code != nil{
                                                                    if response.result.value!.datalistfromapi!.count > 0{
                                                                        let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                        
                                                                        let responseResutl = response.result.value
                                                                        print("\(responseResutl?.datalistfromapi?.count)")
                                                                     
                                                                        nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                        self.boltListApi = responseResutl?.datalistfromapi
                                                                        nextViewController.boltListApi = self.boltListApi
                                                                        
                                                                        for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                            let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                            let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                            let BoltLatitude = Double("\(BirdBikeLat)")
                                                                            let BoltLontitude = Double("\(BirdBikeLng)")
                                                                            
                                                                            let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                            let dd = Sourcelatlatlong.distance(from: ss)
                                                                            //  print(dd)
                                                                            // print((dd*100).rounded()/100)
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            if BoltCheckBikeCount == 0 {
                                                                                
                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                
                                                                                nextViewController.BoltBikeLat = BoltLatitude
                                                                                nextViewController.BoltBikeLon = BoltLontitude
                                                                                
                                                                                
                                                                                //print(self.BirdSourceDistance)
                                                                                nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            else{
                                                                                self.templat = BoltLatitude!
                                                                                self.templong = BoltLontitude!
                                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                if self.BolttempDistance < self.BoltSourceDistance {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    self.BoltSourceDistance = self.BolttempDistance
                                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                                    // print(self.BirdtempDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                }
                                                                                else{
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                        
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        
                                                                        self.hideLoader()
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                    }
                                                                }
                                                                else{
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                    
                                                                }
                                                                
                                                            case .failure(let error):
                                                                print(error)
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                
                                                            }
                                                        }
                                                    }
                                                        else
                                                       {
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                           self.hideLoader()
                                                        }
                                                    }
                                                    
                                                case .failure(let error):
                                                    print(error)
                                                    var BoltCheckBikeCount = 0
                                                    
                                                   if BoltOn
                                                   { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                        switch response.result {
                                                        case .success:
                                                            self.secondServiceCallComplete = true
                                                            if response.result.value?.code != 400 && response.result.value?.code != nil{
                                                                if response.result.value!.datalistfromapi!.count > 0{
                                                                    let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                    
                                                                    let responseResutl = response.result.value
                                                                    print("\(responseResutl?.datalistfromapi?.count)")
                                                                 
                                                                    nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                    self.boltListApi = responseResutl?.datalistfromapi
                                                                    nextViewController.boltListApi = self.boltListApi
                                                                    
                                                                    for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                        let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                        let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                        let BoltLatitude = Double("\(BirdBikeLat)")
                                                                        let BoltLontitude = Double("\(BirdBikeLng)")
                                                                        
                                                                        let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                        let dd = Sourcelatlatlong.distance(from: ss)
                                                                        //  print(dd)
                                                                        // print((dd*100).rounded()/100)
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        if BoltCheckBikeCount == 0 {
                                                                            
                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                            self.BoltBikeLat = BoltLatitude!
                                                                            self.BoltBikeLon = BoltLontitude!
                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                            let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                            self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            
                                                                            nextViewController.BoltBikeLat = BoltLatitude
                                                                            nextViewController.BoltBikeLon = BoltLontitude
                                                                            
                                                                            
                                                                            //print(self.BirdSourceDistance)
                                                                            nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        else{
                                                                            self.templat = BoltLatitude!
                                                                            self.templong = BoltLontitude!
                                                                            let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                            self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                            if self.BolttempDistance < self.BoltSourceDistance {
                                                                                
                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                self.BoltSourceDistance = self.BolttempDistance
                                                                                nextViewController.BoltBikeLat = BoltLatitude!
                                                                                nextViewController.BoltBikeLon = BoltLontitude!
                                                                                // print(self.BirdtempDistance)
                                                                                nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                
                                                                            }
                                                                            else{
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                    
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    
                                                                    self.hideLoader()
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                }
                                                            }
                                                            else{
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                                
                                                            }
                                                            
                                                        case .failure(let error):
                                                            print(error)
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                            
                                                        }
                                                    }
                                                }
                                                  else
                                                   {
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                   self.hideLoader()
                                                    }
                                                }
                                            }
                                        }
                                         else
                                            {

                                                // Bolt Integration
                                                var BoltCheckBikeCount = 0
                                                if BoltOn
                                                {
                                                Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                    switch response.result {
                                                    case .success:
                                                        self.secondServiceCallComplete = true
                                                        if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                            if (response.result.value?.total)! >= 0 {
                                                                let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                
                                                                let responseResutl = response.result.value
                                                                print("\(responseResutl?.datalistfromapi?.count)")
                                                           
                                                                nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                self.boltListApi = responseResutl?.datalistfromapi
                                                                nextViewController.boltListApi = self.boltListApi
                                                                
                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                    let BoltLatitude = Double("\(BirdBikeLat)")
                                                                    let BoltLontitude = Double("\(BirdBikeLng)")
                                                                    
                                                                    let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                    //  print(dd)
                                                                    // print((dd*100).rounded()/100)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    if BoltCheckBikeCount == 0 {
                                                                        
                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                        self.BoltBikeLat = BoltLatitude!
                                                                        self.BoltBikeLon = BoltLontitude!
                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                        let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        
                                                                        nextViewController.BoltBikeLat = BoltLatitude
                                                                        nextViewController.BoltBikeLon = BoltLontitude
                                                                        
                                                                        
                                                                        //print(self.BirdSourceDistance)
                                                                        nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.templat = BoltLatitude!
                                                                        self.templong = BoltLontitude!
                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        if self.BolttempDistance < self.BoltSourceDistance {
                                                                            
                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                            self.BoltBikeLat = BoltLatitude!
                                                                            self.BoltBikeLon = BoltLontitude!
                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                            self.BoltSourceDistance = self.BolttempDistance
                                                                            nextViewController.BoltBikeLat = BoltLatitude!
                                                                            nextViewController.BoltBikeLon = BoltLontitude!
                                                                            // print(self.BirdtempDistance)
                                                                            nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            
                                                                        }
                                                                        else{
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                
                                                                self.hideLoader()
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                        }
                                                        else{
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                            self.hideLoader()
                                                            
                                                        }
                                                        
                                                    case .failure(let error):
                                                        print(error)
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                       self.hideLoader()
                                                        
                                                    }
                                                    
                                                }
                                                    
                                                }
                                                else
                                                {
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    self.hideLoader()
                                                }
                                            }
                                            
                                            
                                          
                                        }
                                    }
                                    else{
                                        
                                        var LimeCheckBikeCount = 0
                                        if LimeOn
                                        {
                                        Alamofire.request("https://data.lime.bike/api/partners/v1/gbfs/louisville/free_bike_status").responseObject { (response: DataResponse<getLimestation>) in
                                            switch response.result {
                                            case .success:
                                                self.secondServiceCallComplete = true
                                                if response.result.value?.code != 400 {
                                                    if response.result.value!.datalistfromapi!.count > 0{
                                                        let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                        
                                                        let responseResutl = response.result.value
                                                        print("\(responseResutl?.datalistfromapi?.count)")
                                                       
                                                        nextViewController.LimeDatalistapicount = responseResutl?.datalistfromapi?.count
                                                        
                                                        self.LimeListApi = responseResutl?.datalistfromapi
                                                        nextViewController.LimeListApi = self.LimeListApi
                                                        
                                                        for LimeBike in (responseResutl?.datalistfromapi)! {
                                                            let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                            let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                            let LimeLatitude = Double("\(BirdBikeLat)")
                                                            let LimeLontitude = Double("\(BirdBikeLng)")
                                                            
                                                            let ss = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                            let dd = Sourcelatlatlong.distance(from: ss)
                                                            //  print(dd)
                                                            // print((dd*100).rounded()/100)
                                                            
                                                            
                                                            
                                                            
                                                            if LimeCheckBikeCount == 0 {
                                                                if LimeBike.bike_id != nil
                                                                {
                                                                    self.LimeBikeID = LimeBike.bike_id!

                                                                }
                                                                else
                                                                {
                                                                    self.LimeBikeID = ""

                                                                }
                                                                self.LimeBikeLat = LimeLatitude!
                                                                self.LimeBikeLon = LimeLontitude!
                                                                self.LimeIsReserved = LimeBike.bike_is_reserved!
                                                                self.LimeIsDisabled = LimeBike.bike_is_disabled!
                                                                let destinationlatlong = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                                self.LimedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                
                                                                nextViewController.LimeBikeLat = LimeLatitude
                                                                nextViewController.LimeBikeLon = LimeLontitude
                                                                
                                                                
                                                                //print(self.BirdSourceDistance)
                                                                nextViewController.LimedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                
                                                                
                                                            }
                                                            else{
                                                                self.templat = LimeLatitude!
                                                                self.templong = LimeLontitude!
                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                                if self.BirdtempDistance < self.BirdSourceDistance {
                                                                    if LimeBike.bike_id != nil
                                                                    {
                                                                        self.BirdBikeID = LimeBike.bike_id!

                                                                    }
                                                                    else
                                                                    {
                                                                        self.BirdBikeID = ""

                                                                    }
                                                                    self.BirdBikeLat = LimeLatitude!
                                                                    self.BirdBikeLon = LimeLontitude!
                                                                    self.BirdIsReserved = LimeBike.bike_is_reserved!
                                                                    self.BirdIsDisabled = LimeBike.bike_is_disabled!
                                                                    self.BirdSourceDistance = self.BirdtempDistance
                                                                    nextViewController.LimeBikeLat = LimeLatitude!
                                                                    nextViewController.LimeBikeLon = LimeLontitude!
                                                                    // print(self.BirdtempDistance)
                                                                    nextViewController.LimedistanceInMeters = self.BirdtempDistance
                                                                    LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                    
                                                                }
                                                                else{
                                                                    LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                                }
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                        
                                                        
                                                        // Bolt Integration
                                                        var BoltCheckBikeCount = 0
                                                        
                                                       if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                            switch response.result {
                                                            case .success:
                                                                self.secondServiceCallComplete = true
                                                                if response.result.value?.code != 400 &&  response.result.value?.code != nil {
                                                                    if (response.result.value?.total)! >= 0 {
                                                                        let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                        
                                                                        let responseResutl = response.result.value
                                                                       // print("\(responseResutl?.datalistfromapi?.count)")
                                                                   
                                                                        nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                        self.boltListApi = responseResutl?.datalistfromapi
                                                                        nextViewController.boltListApi = self.boltListApi
                                                                        
                                                                        for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                            let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                            let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                            let BoltLatitude = Double("\(BirdBikeLat)")
                                                                            let BoltLontitude = Double("\(BirdBikeLng)")
                                                                            
                                                                            let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                            let dd = Sourcelatlatlong.distance(from: ss)
                                                                            //  print(dd)
                                                                            // print((dd*100).rounded()/100)
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            if BoltCheckBikeCount == 0 {
                                                                                
                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                
                                                                                nextViewController.BoltBikeLat = BoltLatitude
                                                                                nextViewController.BoltBikeLon = BoltLontitude
                                                                                
                                                                                
                                                                                //print(self.BirdSourceDistance)
                                                                                nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            else{
                                                                                self.templat = BoltLatitude!
                                                                                self.templong = BoltLontitude!
                                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                if self.BolttempDistance < self.BoltSourceDistance {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    self.BoltSourceDistance = self.BolttempDistance
                                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                                    // print(self.BirdtempDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                }
                                                                                else{
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                        
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        
                                                                        self.hideLoader()
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                }
                                                                else{
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                    
                                                                }
                                                                
                                                            case .failure(let error):
                                                                print(error)
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                               self.hideLoader()
                                                                
                                                            }
                                                        }
                                                        
                                                    }
                                                        else
                                                                                                                   {
                                                                                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                       self.hideLoader()
                                                                                                                   }
                                                        
                                                        
                                                    }
                                                    else{
                                                        
                                                        // Bolt Integration
                                                        var BoltCheckBikeCount = 0
                                                        
                                                        if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                            switch response.result {
                                                            case .success:
                                                                self.secondServiceCallComplete = true
                                                                if response.result.value?.code != 400 {
                                                                    if response.result.value!.datalistfromapi!.count > 0{
                                                                        let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                        
                                                                        let responseResutl = response.result.value
                                                                        print("\(responseResutl?.datalistfromapi?.count)")
                                                                     
                                                                        nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                        self.boltListApi = responseResutl?.datalistfromapi
                                                                        nextViewController.boltListApi = self.boltListApi
                                                                        
                                                                        for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                            let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                            let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                            let BoltLatitude = Double("\(BirdBikeLat)")
                                                                            let BoltLontitude = Double("\(BirdBikeLng)")
                                                                            
                                                                            let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                            let dd = Sourcelatlatlong.distance(from: ss)
                                                                            //  print(dd)
                                                                            // print((dd*100).rounded()/100)
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            if BoltCheckBikeCount == 0 {
                                                                                
                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                
                                                                                nextViewController.BoltBikeLat = BoltLatitude
                                                                                nextViewController.BoltBikeLon = BoltLontitude
                                                                                
                                                                                
                                                                                //print(self.BirdSourceDistance)
                                                                                nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                
                                                                                
                                                                                
                                                                            }
                                                                            else{
                                                                                self.templat = BoltLatitude!
                                                                                self.templong = BoltLontitude!
                                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                if self.BolttempDistance < self.BoltSourceDistance {
                                                                                    
                                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                                    self.BoltBikeLat = BoltLatitude!
                                                                                    self.BoltBikeLon = BoltLontitude!
                                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                    self.BoltSourceDistance = self.BolttempDistance
                                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                                    // print(self.BirdtempDistance)
                                                                                    nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                    
                                                                                }
                                                                                else{
                                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                }
                                                                            }
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        
                                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                        
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        
                                                                        self.hideLoader()
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                        self.hideLoader()
                                                                    }
                                                                }
                                                                else{
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    self.hideLoader()
                                                                    
                                                                }
                                                                
                                                            case .failure(let error):
                                                                print(error)
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                            }
                                                        }
                                                    }
                                                        else
                                                        {
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        }
                                                    }
                                                }
                                                else{
                                                    
                                                    // Bolt Integration
                                                    var BoltCheckBikeCount = 0
                                                    
                                                    if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                        switch response.result {
                                                        case .success:
                                                            self.secondServiceCallComplete = true
                                                            if response.result.value?.code != 400 && response.result.value?.code != nil{
                                                                if response.result.value!.datalistfromapi!.count > 0{
                                                                    let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                    
                                                                    let responseResutl = response.result.value
                                                                    print("\(responseResutl?.datalistfromapi?.count)")
                                                                 
                                                                    nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                    self.boltListApi = responseResutl?.datalistfromapi
                                                                    nextViewController.boltListApi = self.boltListApi
                                                                    
                                                                    for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                        let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                        let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                        let BoltLatitude = Double("\(BirdBikeLat)")
                                                                        let BoltLontitude = Double("\(BirdBikeLng)")
                                                                        
                                                                        let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                        let dd = Sourcelatlatlong.distance(from: ss)
                                                                        //  print(dd)
                                                                        // print((dd*100).rounded()/100)
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        if BoltCheckBikeCount == 0 {
                                                                            
                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                            self.BoltBikeLat = BoltLatitude!
                                                                            self.BoltBikeLon = BoltLontitude!
                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                            let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                            self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            
                                                                            nextViewController.BoltBikeLat = BoltLatitude
                                                                            nextViewController.BoltBikeLon = BoltLontitude
                                                                            
                                                                            
                                                                            //print(self.BirdSourceDistance)
                                                                            nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                            
                                                                            
                                                                            
                                                                        }
                                                                        else{
                                                                            self.templat = BoltLatitude!
                                                                            self.templong = BoltLontitude!
                                                                            let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                            self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                            self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                            if self.BolttempDistance < self.BoltSourceDistance {
                                                                                
                                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                                self.BoltBikeLat = BoltLatitude!
                                                                                self.BoltBikeLon = BoltLontitude!
                                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                self.BoltSourceDistance = self.BolttempDistance
                                                                                nextViewController.BoltBikeLat = BoltLatitude!
                                                                                nextViewController.BoltBikeLon = BoltLontitude!
                                                                                // print(self.BirdtempDistance)
                                                                                nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                
                                                                            }
                                                                            else{
                                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                    nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                    
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                    
                                                                    self.hideLoader()
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                           self.hideLoader()
                                                                }
                                                            }
                                                            else{
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                                
                                                            }
                                                            
                                                        case .failure(let error):
                                                            print(error)
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                            self.hideLoader()
                                                        }
                                                    }
                                                }
                                                    else
                                                    {
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        self.hideLoader()
                                                    }
                                                }
                                                
                                            case .failure(let error):
                                                print(error)
                                                var BoltCheckBikeCount = 0
                                                
                                               if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                    switch response.result {
                                                    case .success:
                                                        self.secondServiceCallComplete = true
                                                        if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                            if response.result.value!.datalistfromapi!.count > 0{
                                                                let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                
                                                                let responseResutl = response.result.value
                                                                print("\(responseResutl?.datalistfromapi?.count)")
                                                             
                                                                nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                self.boltListApi = responseResutl?.datalistfromapi
                                                                nextViewController.boltListApi = self.boltListApi
                                                                
                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                    let BoltLatitude = Double("\(BirdBikeLat)")
                                                                    let BoltLontitude = Double("\(BirdBikeLng)")
                                                                    
                                                                    let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                    //  print(dd)
                                                                    // print((dd*100).rounded()/100)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    if BoltCheckBikeCount == 0 {
                                                                        
                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                        self.BoltBikeLat = BoltLatitude!
                                                                        self.BoltBikeLon = BoltLontitude!
                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                        let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        
                                                                        nextViewController.BoltBikeLat = BoltLatitude
                                                                        nextViewController.BoltBikeLon = BoltLontitude
                                                                        
                                                                        
                                                                        //print(self.BirdSourceDistance)
                                                                        nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.templat = BoltLatitude!
                                                                        self.templong = BoltLontitude!
                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        if self.BolttempDistance < self.BoltSourceDistance {
                                                                            
                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                            self.BoltBikeLat = BoltLatitude!
                                                                            self.BoltBikeLon = BoltLontitude!
                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                            self.BoltSourceDistance = self.BolttempDistance
                                                                            nextViewController.BoltBikeLat = BoltLatitude!
                                                                            nextViewController.BoltBikeLon = BoltLontitude!
                                                                            // print(self.BirdtempDistance)
                                                                            nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            
                                                                        }
                                                                        else{
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                
                                                                self.hideLoader()
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            else{
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                   self.hideLoader()
                                                            }
                                                        }
                                                        else{
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                            self.hideLoader()
                                                            
                                                        }
                                                        
                                                    case .failure(let error):
                                                        print(error)
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        self.hideLoader()
                                                    }
                                                }
                                            }
                                           else
                                               {
                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                self.hideLoader()
                                                }
                                                
                                            }
                                        }
                                    }
                                    else
                                        {

                                            // Bolt Integration
                                            
                                            var BoltCheckBikeCount = 0
                                            
                                           if BoltOn
                                            { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                switch response.result {
                                                case .success:
                                                    self.secondServiceCallComplete = true
                                                    if response.result.value?.code != 400 &&  response.result.value?.code != nil {
                                                        if (response.result.value?.total)! >= 0 {
                                                            let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                            
                                                            let responseResutl = response.result.value
                                                           // print("\(responseResutl?.datalistfromapi?.count)")
                                                       
                                                            nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                            self.boltListApi = responseResutl?.datalistfromapi
                                                            nextViewController.boltListApi = self.boltListApi
                                                            
                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                let BoltLatitude = Double("\(BirdBikeLat)")
                                                                let BoltLontitude = Double("\(BirdBikeLng)")
                                                                
                                                                let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                //  print(dd)
                                                                // print((dd*100).rounded()/100)
                                                                
                                                                
                                                                
                                                                
                                                                if BoltCheckBikeCount == 0 {
                                                                    
                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                    self.BoltBikeLat = BoltLatitude!
                                                                    self.BoltBikeLon = BoltLontitude!
                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                    
                                                                    nextViewController.BoltBikeLat = BoltLatitude
                                                                    nextViewController.BoltBikeLon = BoltLontitude
                                                                    
                                                                    
                                                                    //print(self.BirdSourceDistance)
                                                                    nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    self.templat = BoltLatitude!
                                                                    self.templong = BoltLontitude!
                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                    if self.BolttempDistance < self.BoltSourceDistance {
                                                                        
                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                        self.BoltBikeLat = BoltLatitude!
                                                                        self.BoltBikeLon = BoltLontitude!
                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                        self.BoltSourceDistance = self.BolttempDistance
                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                        // print(self.BirdtempDistance)
                                                                        nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        
                                                                    }
                                                                    else{
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                    }
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                            
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                            
                                                            self.hideLoader()
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        }
                                                    }
                                                    else{
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        self.hideLoader()
                                                        
                                                    }
                                                    
                                                case .failure(let error):
                                                    print(error)
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                   self.hideLoader()
                                                    
                                                }
                                            }
                                                
                                            }
                                            else
                                           {
                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                            self.hideLoader()
                                            }
                                        }
                                        
                                        
                                      
                                    }
                                    
                                case .failure(let error):
                                    print(error)
                                    nextViewController.BirdDatalistapicount = 0
                                    nextViewController.BikedistanceInMeters = 1000
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                    self.hideLoader()
                                    
                                }
                                
                            
                            
                            }
                            
                            }
                            else
                            {
                               var LimeCheckBikeCount = 0
                                if LimeOn
                                {
                                Alamofire.request("https://data.lime.bike/api/partners/v1/gbfs/louisville/free_bike_status").responseObject { (response: DataResponse<getLimestation>) in
                                    switch response.result {
                                    case .success:
                                        self.secondServiceCallComplete = true
                                        if response.result.value?.code != 400 {
                                            if response.result.value!.datalistfromapi!.count > 0{
                                                let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                
                                                let responseResutl = response.result.value
                                                print("\(responseResutl?.datalistfromapi?.count)")
                                               
                                                nextViewController.LimeDatalistapicount = responseResutl?.datalistfromapi?.count
                                                
                                                self.LimeListApi = responseResutl?.datalistfromapi
                                                nextViewController.LimeListApi = self.LimeListApi
                                                
                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                    let LimeLatitude = Double("\(BirdBikeLat)")
                                                    let LimeLontitude = Double("\(BirdBikeLng)")
                                                    
                                                    let ss = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                    //  print(dd)
                                                    // print((dd*100).rounded()/100)
                                                    
                                                    
                                                    
                                                    
                                                    if LimeCheckBikeCount == 0 {
                                                        if LimeBike.bike_id != nil
                                                        {
                                                            self.LimeBikeID = LimeBike.bike_id!

                                                        }
                                                        else
                                                        {
                                                            self.LimeBikeID = ""

                                                        }
                                                        self.LimeBikeLat = LimeLatitude!
                                                        self.LimeBikeLon = LimeLontitude!
                                                        self.LimeIsReserved = LimeBike.bike_is_reserved!
                                                        self.LimeIsDisabled = LimeBike.bike_is_disabled!
                                                        let destinationlatlong = CLLocation(latitude: LimeLatitude!, longitude: LimeLontitude!)
                                                        self.LimedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                        self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                        LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                        
                                                        nextViewController.LimeBikeLat = LimeLatitude
                                                        nextViewController.LimeBikeLon = LimeLontitude
                                                        
                                                        
                                                        //print(self.BirdSourceDistance)
                                                        nextViewController.LimedistanceInMeters = self.BirdSourceDistance
                                                        
                                                        
                                                        
                                                    }
                                                    else{
                                                        self.templat = LimeLatitude!
                                                        self.templong = LimeLontitude!
                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                        self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                        self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                                                        if self.BirdtempDistance < self.BirdSourceDistance {
                                                            if LimeBike.bike_id != nil
                                                            {
                                                                self.BirdBikeID = LimeBike.bike_id!

                                                            }
                                                            else
                                                            {
                                                                self.BirdBikeID = ""

                                                            }
                                                            self.BirdBikeLat = LimeLatitude!
                                                            self.BirdBikeLon = LimeLontitude!
                                                            self.BirdIsReserved = LimeBike.bike_is_reserved!
                                                            self.BirdIsDisabled = LimeBike.bike_is_disabled!
                                                            self.BirdSourceDistance = self.BirdtempDistance
                                                            nextViewController.LimeBikeLat = LimeLatitude!
                                                            nextViewController.LimeBikeLon = LimeLontitude!
                                                            // print(self.BirdtempDistance)
                                                            nextViewController.LimedistanceInMeters = self.BirdtempDistance
                                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                            
                                                        }
                                                        else{
                                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                                        }
                                                    }
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }
                                                
                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                
                                                
                                                // Bolt Integration
                                                var BoltCheckBikeCount = 0
                                                
                                                if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                    switch response.result {
                                                    case .success:
                                                        self.secondServiceCallComplete = true
                                                        if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                            if response.result.value!.datalistfromapi!.count > 0{
                                                                let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                
                                                                let responseResutl = response.result.value
                                                                print("\(responseResutl?.datalistfromapi?.count)")
                                                           
                                                                nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                self.boltListApi = responseResutl?.datalistfromapi
                                                                nextViewController.boltListApi = self.boltListApi
                                                                
                                                                for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                    let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                    let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                    let BoltLatitude = Double("\(BirdBikeLat)")
                                                                    let BoltLontitude = Double("\(BirdBikeLng)")
                                                                    
                                                                    let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                    let dd = Sourcelatlatlong.distance(from: ss)
                                                                    //  print(dd)
                                                                    // print((dd*100).rounded()/100)
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    if BoltCheckBikeCount == 0 {
                                                                        
                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                        self.BoltBikeLat = BoltLatitude!
                                                                        self.BoltBikeLon = BoltLontitude!
                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                        let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        
                                                                        nextViewController.BoltBikeLat = BoltLatitude
                                                                        nextViewController.BoltBikeLon = BoltLontitude
                                                                        
                                                                        
                                                                        //print(self.BirdSourceDistance)
                                                                        nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                        
                                                                        
                                                                        
                                                                    }
                                                                    else{
                                                                        self.templat = BoltLatitude!
                                                                        self.templong = BoltLontitude!
                                                                        let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                        self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                        self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                        if self.BolttempDistance < self.BoltSourceDistance {
                                                                            
                                                                            self.BoltBikeID = LimeBike.bike_id!
                                                                            self.BoltBikeLat = BoltLatitude!
                                                                            self.BoltBikeLon = BoltLontitude!
                                                                            self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                            self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                            self.BoltSourceDistance = self.BolttempDistance
                                                                            nextViewController.BoltBikeLat = BoltLatitude!
                                                                            nextViewController.BoltBikeLon = BoltLontitude!
                                                                            // print(self.BirdtempDistance)
                                                                            nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                            
                                                                        }
                                                                        else{
                                                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                                nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                
                                                                self.hideLoader()
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            else{
                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                self.hideLoader()
                                                            }
                                                        }
                                                        else{
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                            self.hideLoader()
                                                            
                                                        }
                                                        
                                                    case .failure(let error):
                                                        print(error)
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                       self.hideLoader()
                                                        
                                                    }
                                                }
                                            }
                                             else
                                                {
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    self.hideLoader()
                                                }
                                                
                                                
                                                
                                            }
                                        }
                                        else{
                                            
                                            // Bolt Integration
                                            var BoltCheckBikeCount = 0
                                            if BoltOn {
                                            Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                switch response.result {
                                                case .success:
                                                    self.secondServiceCallComplete = true
                                                    if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                        if response.result.value!.datalistfromapi!.count > 0{
                                                            let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                            
                                                            let responseResutl = response.result.value
                                                            print("\(responseResutl?.datalistfromapi?.count)")
                                                         
                                                            nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                            self.boltListApi = responseResutl?.datalistfromapi
                                                            nextViewController.boltListApi = self.boltListApi
                                                            
                                                            for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                let BoltLatitude = Double("\(BirdBikeLat)")
                                                                let BoltLontitude = Double("\(BirdBikeLng)")
                                                                
                                                                let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                let dd = Sourcelatlatlong.distance(from: ss)
                                                                //  print(dd)
                                                                // print((dd*100).rounded()/100)
                                                                
                                                                
                                                                
                                                                
                                                                if BoltCheckBikeCount == 0 {
                                                                    
                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                    self.BoltBikeLat = BoltLatitude!
                                                                    self.BoltBikeLon = BoltLontitude!
                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                    
                                                                    nextViewController.BoltBikeLat = BoltLatitude
                                                                    nextViewController.BoltBikeLon = BoltLontitude
                                                                    
                                                                    
                                                                    //print(self.BirdSourceDistance)
                                                                    nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                    
                                                                    
                                                                    
                                                                }
                                                                else{
                                                                    self.templat = BoltLatitude!
                                                                    self.templong = BoltLontitude!
                                                                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                    self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                    self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                    if self.BolttempDistance < self.BoltSourceDistance {
                                                                        
                                                                        self.BoltBikeID = LimeBike.bike_id!
                                                                        self.BoltBikeLat = BoltLatitude!
                                                                        self.BoltBikeLon = BoltLontitude!
                                                                        self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                        self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                        self.BoltSourceDistance = self.BolttempDistance
                                                                        nextViewController.BoltBikeLat = BoltLatitude!
                                                                        nextViewController.BoltBikeLon = BoltLontitude!
                                                                        // print(self.BirdtempDistance)
                                                                        nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                        
                                                                    }
                                                                    else{
                                                                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                    }
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            
                                                            nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                            
                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                            
                                                            self.hideLoader()
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        }
                                                    }
                                                    else{
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        self.hideLoader()
                                                        
                                                    }
                                                    
                                                case .failure(let error):
                                                    print(error)
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                               //self.hideLoader()
                                                    self.hideLoader()
                                                }
                                            }
                                        }
                                            else
                                            {
                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                                      //self.hideLoader()
                                                                                                           self.hideLoader()
                                            }
                                        }
                                        
                                    case .failure(let error):
                                        print(error)
                                        // Bolt Integration
                                        var BoltCheckBikeCount = 0
                                        
                                       if BoltOn
                                       { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                            switch response.result {
                                            case .success:
                                                self.secondServiceCallComplete = true
                                                if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                    if response.result.value!.datalistfromapi!.count > 0{
                                                        let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                        
                                                        let responseResutl = response.result.value
                                                        print("\(responseResutl?.datalistfromapi?.count)")
                                                     
                                                        nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                        self.boltListApi = responseResutl?.datalistfromapi
                                                        nextViewController.boltListApi = self.boltListApi
                                                        
                                                        for LimeBike in (responseResutl?.datalistfromapi)! {
                                                            let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                            let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                            let BoltLatitude = Double("\(BirdBikeLat)")
                                                            let BoltLontitude = Double("\(BirdBikeLng)")
                                                            
                                                            let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                            let dd = Sourcelatlatlong.distance(from: ss)
                                                            //  print(dd)
                                                            // print((dd*100).rounded()/100)
                                                            
                                                            
                                                            
                                                            
                                                            if BoltCheckBikeCount == 0 {
                                                                
                                                                self.BoltBikeID = LimeBike.bike_id!
                                                                self.BoltBikeLat = BoltLatitude!
                                                                self.BoltBikeLon = BoltLontitude!
                                                                self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                
                                                                nextViewController.BoltBikeLat = BoltLatitude
                                                                nextViewController.BoltBikeLon = BoltLontitude
                                                                
                                                                
                                                                //print(self.BirdSourceDistance)
                                                                nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                
                                                                
                                                                
                                                            }
                                                            else{
                                                                self.templat = BoltLatitude!
                                                                self.templong = BoltLontitude!
                                                                let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                if self.BolttempDistance < self.BoltSourceDistance {
                                                                    
                                                                    self.BoltBikeID = LimeBike.bike_id!
                                                                    self.BoltBikeLat = BoltLatitude!
                                                                    self.BoltBikeLon = BoltLontitude!
                                                                    self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                    self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                    self.BoltSourceDistance = self.BolttempDistance
                                                                    nextViewController.BoltBikeLat = BoltLatitude!
                                                                    nextViewController.BoltBikeLon = BoltLontitude!
                                                                    // print(self.BirdtempDistance)
                                                                    nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                    
                                                                }
                                                                else{
                                                                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                }
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        }
                                                        
                                                        nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                        
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        
                                                        self.hideLoader()
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    }
                                                    else
                                                    {
                                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                              self.hideLoader()
                                                    }
                                                }
                                                else{
                                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                                    self.hideLoader()
                                                    
                                                }
                                                
                                            case .failure(let error):
                                                print(error)
                                              
                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                           self.hideLoader()
                                                
                                            }
                                        }
                                    }
                                        else
                                       {
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                              self.hideLoader()
                                        }
                                    }
                                }
                                }
                                else
                                {
                                    
                                                                               // Bolt Integration
                                                                               var BoltCheckBikeCount = 0
                                                                               
                                                                               if BoltOn { Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
                                                                                   switch response.result {
                                                                                   case .success:
                                                                                       self.secondServiceCallComplete = true
                                                                                       if response.result.value?.code != 400 &&  response.result.value?.code != nil{
                                                                                           if response.result.value!.datalistfromapi!.count > 0{
                                                                                               let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                                                                                               
                                                                                               let responseResutl = response.result.value
                                                                                               print("\(responseResutl?.datalistfromapi?.count)")
                                                                                          
                                                                                               nextViewController.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                                                                                               self.boltListApi = responseResutl?.datalistfromapi
                                                                                               nextViewController.boltListApi = self.boltListApi
                                                                                               
                                                                                               for LimeBike in (responseResutl?.datalistfromapi)! {
                                                                                                   let BirdBikeLat = LimeBike.bike_lat! as! NSString
                                                                                                   let BirdBikeLng = LimeBike.bike_lon! as! NSString
                                                                                                   let BoltLatitude = Double("\(BirdBikeLat)")
                                                                                                   let BoltLontitude = Double("\(BirdBikeLng)")
                                                                                                   
                                                                                                   let ss = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                   let dd = Sourcelatlatlong.distance(from: ss)
                                                                                                   //  print(dd)
                                                                                                   // print((dd*100).rounded()/100)
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                                   if BoltCheckBikeCount == 0 {
                                                                                                       
                                                                                                       self.BoltBikeID = LimeBike.bike_id!
                                                                                                       self.BoltBikeLat = BoltLatitude!
                                                                                                       self.BoltBikeLon = BoltLontitude!
                                                                                                       self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                       self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                       let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                                                                                                       self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                       self.BoltSourceDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                       BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                       
                                                                                                       nextViewController.BoltBikeLat = BoltLatitude
                                                                                                       nextViewController.BoltBikeLon = BoltLontitude
                                                                                                       
                                                                                                       
                                                                                                       //print(self.BirdSourceDistance)
                                                                                                       nextViewController.BoltdistanceInMeters = self.BoltSourceDistance
                                                                                                       
                                                                                                       
                                                                                                       
                                                                                                   }
                                                                                                   else{
                                                                                                       self.templat = BoltLatitude!
                                                                                                       self.templong = BoltLontitude!
                                                                                                       let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                                                                                       self.BoltdistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                                                                                                       self.BolttempDistance = (self.BoltdistanceInMeters*100).rounded()/100
                                                                                                       if self.BolttempDistance < self.BoltSourceDistance {
                                                                                                           
                                                                                                           self.BoltBikeID = LimeBike.bike_id!
                                                                                                           self.BoltBikeLat = BoltLatitude!
                                                                                                           self.BoltBikeLon = BoltLontitude!
                                                                                                           self.BoltIsReserved = LimeBike.bike_is_reserved!
                                                                                                           self.BoltIsDisabled = LimeBike.bike_is_disabled!
                                                                                                           self.BoltSourceDistance = self.BolttempDistance
                                                                                                           nextViewController.BoltBikeLat = BoltLatitude!
                                                                                                           nextViewController.BoltBikeLon = BoltLontitude!
                                                                                                           // print(self.BirdtempDistance)
                                                                                                           nextViewController.BoltdistanceInMeters = self.BolttempDistance
                                                                                                           BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                           
                                                                                                       }
                                                                                                       else{
                                                                                                           BoltCheckBikeCount = BoltCheckBikeCount + 1
                                                                                                       }
                                                                                                   }
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                                   
                                                                                               }
                                                                                               
                                                                                               nextViewController.BikedistanceInMeters = self.BirdSourceDistance
                                                                                               
                                                                                               self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                               
                                                                                               self.hideLoader()
                                                                                               
                                                                                               
                                                                                               
                                                                                               
                                                                                               
                                                                                           }
                                                                                           else{
                                                                                               self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                               self.hideLoader()
                                                                                           }
                                                                                       }
                                                                                       else{
                                                                                           self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                           self.hideLoader()
                                                                                           
                                                                                       }
                                                                                       
                                                                                   case .failure(let error):
                                                                                       print(error)
                                                                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                                                                                      self.hideLoader()
                                                                                       
                                                                                   }
                                                                               }
                                                                           }
                                    else
                                                                               {
                                                                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                                                                self.hideLoader()
                                    }
                                }
                            }
                            
                            
                            
                        }

                        
                    }
                }
                else{
                    if souText == "" && desText != ""
                    {
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Trip Planner"
                        alertViewController.message = "Enter source address"
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                    }
                    else if desText == "" && souText != ""
                    {
                        
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Trip Planner"
                        alertViewController.message = "Enter destination address"
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                        
                    }
                    else {
                        
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Trip Planner"
                        alertViewController.message = "Enter source & destination"
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                    }
                    
                }
            
                print("login btn presses");
                
            }
    @objc func CompletBothAPI(){
        
        if self.firstServiceCallComplete && self.secondServiceCallComplete {
            // Handle the fact that you're finished
        }
        
    }
    @objc func closePreferenceAction(sender: UIButton!)
    {
       self.navigationController?.setNavigationBarHidden(false, animated: true)
        UIView.animate(withDuration: 1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        
                        let userDefaults1 = UserDefaults.standard
                        let accessToken = userDefaults1.value(forKey: "accessToken")
                        print("\(self.buscheckbox.isOn)")
                        print(self.bikebuscheckbox.isOn)
                        print(self.traincheckbox.isOn)
                        print(self.rideShareCheckBox.isOn)
                        print(self.uberCheckBox.isOn)
                        print(self.lyftCheckBox.isOn)
                        var cabBool = NSString()
                        cabBool = "true"
                        if self.uberCheckBox.isOn == false && self.lyftCheckBox.isOn == false
                        {
                            cabBool = "false"
                        }
                        userDefaults1.set(self.buscheckbox.isOn, forKey: "isBus")
                        
                        let url = "\(APIUrl.ZIGTARCAPI)Preferences/send"
                        let parameters: Parameters = [
                                                   "TokenId": accessToken!,
                                                   "bus": "true",
                                                   "bike": "true",
                                                   "train": "true",
                                                   "cab": "true",
                                                   "lime": "false",
                                                   "bolt": "false",
                                                   "bikeshare": "\(self.carFirstSelected)",
                                                   "Cabshare": "\(self.bikeFirstSelected)",
                                                   "Message":""
                                               ]
                                               print("\(parameters)")
                                               Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                                                   .responseObject{ (response: DataResponse<TokenSend>) in
                                                       switch response.result {
                                                       case .success:
                                                           // print(response.result.value?.message ?? "nil")
                                                           userDefaults1.set("true", forKey: "isBus")
                                                           //userDefaults1.set(self.bikebuscheckbox.isOn, forKey: "isBike")
                                                           userDefaults1.set("true", forKey: "isBike")
                                                           userDefaults1.set("true", forKey: "isTrain")
                                                           userDefaults1.set(cabBool, forKey: "isCab")
                                                        userDefaults1.set(self.carFirstSelected, forKey: "Cabshare")
                                                                                      userDefaults1.set(self.bikeFirstSelected, forKey: "Bikeshare")
                                                           userDefaults1.set(self.carSelected, forKey: "Cab")
                                                           userDefaults1.set(self.bikeSelected, forKey: "Bike")
                                                           userDefaults1.synchronize()
                                                       case .failure(let error):
                                                           print(error.localizedDescription)
                                                       }
                                                       
                                               }
                        
                        
                        
        }, completion: { (finished) -> Void in
            
        })
    }
    
    @objc func closebuttonAction(sender: UIButton!) {
        print("Button tapped")
        menuButton.isEnabled = true
        popupopen = true
        UIView.animate(withDuration: 0,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        self.amenityView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.size.width-80, height: self.view.frame.size.height-140)
                        self.menuBackView.frame = CGRect.init(x: 2000, y: 90, width: self.view.frame.size.width-80, height: self.view.frame.size.height-120)
                        
                        
        }, completion: { (finished) -> Void in
            if self.isBackAmenities
            {
                self.blurEffectView.isHidden = true;
                //                if self.isPlaceSelected
                //                {
                //                    self.getPlaces(type: self.typeString)
                //                }
                //                else
                //                {
                //                    self.plotTARCTourism(category: self.typeString)
                //                    //self.getCinciPlaces(type: self.typeString)
                //                }
            }
        })
    }
    func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
        
        //  self.source_marker = GMSMarker()
        
        if isSourceMarker
        {
            self.source_marker.position = coordinates
            // self.destination_marker.title = self.destinationPlace?.name
            self.source_marker.icon = UIImage(named: "source_Marker")
            self.source_marker.map = self.mapBackgorundView
            self.CheckFavTrip()
            
        }
        else
        {
            print(self.destinationPlace?.name)
            self.destination_marker.position = coordinates
            self.destination_marker.title = self.destinationPlace?.name
            self.destination_marker.icon = UIImage(named: "destination_Marker")
            self.destination_marker.map = self.mapBackgorundView
            self.CheckFavTrip()
        }
    }
    
    
    
    // Camera change Position this methods will call every time
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        var destinationLocation = CLLocation()
        
        destinationLocation = CLLocation(latitude: position.target.latitude,  longitude: position.target.longitude)
        let destinationCoordinate = destinationLocation.coordinate
        
        
        
        
        
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json"
        let parameters: Parameters = [
            "latlng": "\(position.target.latitude),\(position.target.longitude)",
            "key": Key.GoogleAPIKey
        ]
        
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<MyLocationAddress>) in
                
                
                switch response.result {
                case .success:
                    // print(response)
                    //to get status code
                    
                    if response.result.value?.results != nil
                    {
                        
                        if(response.result.value?.status == "OK"){
                            
                            let weatherResponse = response.result.value?.results![0]
                            // print(weatherResponse ?? "nil")
                            
                            if self.isSourceMarker
                            {
                                self.sourceLatTrip =  Double((weatherResponse?.geometry?.location?.lat)!)
                                self.sourceLngTrip =  Double((weatherResponse?.geometry?.location?.lng)!)
                                self.sourceStringTrip = (weatherResponse?.placeId)!
                                self.sourceTextField.text = (weatherResponse?.formattedAddress)!
                                
                                self.source_marker.title = (weatherResponse?.formattedAddress)!
                                //Arun
                                //                                          self.markersArray.append(State(name: (weatherResponse?.formattedAddress)!, long: CLLocationDegrees( self.sourceLatTrip), lat: CLLocationDegrees(self.sourceLngTrip ), placeId:" ", distanceVale: "", formattedAddress:  (weatherResponse?.formattedAddress)!, type: "", imageUrl: ""))
                                
                                
                            }
                            else
                            {
                                if !self.firsttimeDestination && self.destinationcount == 2{
                                    
                                    self.destinationStringTrip = (weatherResponse?.placeId)!
                                    self.destinationLatTrip = Double((weatherResponse?.geometry?.location?.lat)!)
                                    self.destinationLngTrip = Double((weatherResponse?.geometry?.location?.lng)!)
                                    self.destinationTextField.text = (weatherResponse?.formattedAddress)!
                                    
                                    
                                    print(weatherResponse?.formattedAddress)
                                    self.destination_marker.title = (weatherResponse?.formattedAddress)!
                                    //Arun
                                    //                                    self.markersArray.append(State(name: (weatherResponse?.formattedAddress)!, long: CLLocationDegrees( self.destinationLatTrip), lat: CLLocationDegrees(self.destinationLngTrip ), placeId:" ", distanceVale: "", formattedAddress:  (weatherResponse?.formattedAddress)!, type: "", imageUrl: ""))
                                }
                                else{
                                    self.firsttimeDestination = false
                                    self.destinationcount = self.destinationcount + 1
                                }
                            }
                            
                            //  self.getPlaces(type: self.typeString)
                        }
                        
                        
                        
                    }
                    
                case .failure(let error):
                    self.showErrorDialogBox(viewController: self)
                    print(error.localizedDescription)
                }
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        var destinationLocation = CLLocation()
        
        destinationLocation = CLLocation(latitude: position.target.latitude,  longitude: position.target.longitude)
        let destinationCoordinate = destinationLocation.coordinate
        updateLocationoordinates(coordinates: destinationCoordinate)
        
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
        
        for state in self.markersArray {
            if state.name == marker.title
                
            {
                self.ameniticsTitle = state.name
                
                if state.type != "LOUVELO"
                {
                    let url = "https://maps.googleapis.com/maps/api/geocode/json"
                    let parameters: Parameters = [
                        "latlng": "\(marker.position.latitude),\(marker.position.longitude)",
                        "key": Key.GoogleAPIKey
                    ]
                    
                    
                    Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                        .responseObject{ (response: DataResponse<MyLocationAddress>) in
                            
                            
                            switch response.result {
                            case .success:
                                //print(response)
                                //to get status code
                                
                                
                                let weatherResponse = response.result.value?.results![0]
                                print(weatherResponse ?? "nil")
                                
                                self.destinationStringTrip = (weatherResponse?.placeId)!
                                self.destinationLatTrip = Double((weatherResponse?.geometry?.location?.lat)!)
                                self.destinationLngTrip = Double((weatherResponse?.geometry?.location?.lng)!)
                                self.destinationTextField.text = (weatherResponse?.formattedAddress)!
                            case .failure(let error):
                                self.showErrorDialogBox(viewController: self)
                                print(error.localizedDescription)
                            }
                    }
                    
                }
                else
                {
                    self.typeString = "LouVelo"
                    let addressString = state.formattedAddress.split(separator: "\n")
                    print(state.formattedAddress)
                    self.destinationStringTrip = state.formattedAddress
                    self.destinationLatTrip = marker.position.latitude
                    self.destinationLngTrip = marker.position.longitude
                    self.destinationTextField.text = "\(addressString[0])"
                    
                }
                
            }
        }
        
        
    }
    // Display the start dates and event summaries in the UITextView
    
    
    
    // Helper for showing an alert
    func showAlert(title : String, message: String) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        let ok = UIAlertAction(
            title: "OK",
            style: UIAlertActionStyle.default,
            handler: nil
        )
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        // Get a reference for the custom overlay
        let customInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)![0] as! CustomInfoWindow
        customInfoWindow.MarkerTitleLable.text = marker.title
        customInfoWindow.markerImageView.image = marker.icon
        customInfoWindow.markerImageView.contentMode = .scaleAspectFit
        for state in self.markersArray {
            if state.name == marker.title
            {
                // print(state.formattedAddress)
                customInfoWindow.markerDescTextView.text = state.formattedAddress
            }
        }
        return customInfoWindow
    }
    
    
    func getPlaces(type: String?) {
        // if popupopen {
        self.menuButton.isEnabled = true
        //   popupopen = false
        // }
        // listViewBtnAction()
        self.closelistview()
        var locationString = ""
        markerClearButton.isHidden = false
        listViewBtn.isHidden = false
        
        listViewBtnBackView.isHidden = false
        
        if sourceLatTrip != 0.0 {
            if amenityTypeString == "Near my source"
            {
                if sourceTextField.text == "Current Location" || isCurrentLoc
                {
                    
                    locationString = "\((sourceLatTrip)),\((sourceLngTrip))"
                }
                else
                {
                    locationString = "\((sourceLatTrip)),\((sourceLngTrip))"
                }
            }
            else if amenityTypeString == "Near my destination"
            {
                locationString = "\(destinationLatTrip),\(destinationLngTrip)"
            }
            
            
            let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
            let parameters: Parameters = [
                "location":locationString,
                "radius": "\(sliderValue*1609)",
                "type":type ?? "park",
                "key": Key.GoogleAPIKey
            ]
            
            print("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(locationString)&radius=\(sliderValue*1609)&type=\((type)!)&key=\(Key.GoogleAPIKey)")
            if locationString != "0.0,0.0"{
                
                
                Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                    .responseObject{ (response: DataResponse<RootClass>) in
                        switch response.result {
                        case .success:
                            if response.result.value?.status == "OK"
                            {
                                // print(response)
                                //to get status code
                                
                                
                                let weatherResponse = response.result.value
                                //   print(weatherResponse!.status ?? "user nil" )
                                
                                DispatchQueue.main.async {
                                    for markerDet in self.markerDict
                                    {
                                        let removeMarker : GMSMarker = markerDet as! GMSMarker
                                        removeMarker.map = nil;
                                    }
                                    self.markerDict.removeAllObjects()
                                    self.markersArray.removeAll()
                                    
                                    
                                    if let threeDayForecast = weatherResponse?.results {
                                        for forecast in threeDayForecast {
                                            // print(forecast.name ?? "user nil")
                                            //print(forecast.vicinity ?? "user nil")
                                            if let geometryLoc = forecast.geometry
                                            {
                                                if let latlong = geometryLoc.location
                                                {
                                                    
                                                    
                                                    self.markersArray.append(State(name: forecast.name!, long: latlong.lat!, lat: latlong.lng!, placeId: forecast.placeId!, distanceVale: " ", formattedAddress: forecast.vicinity!, type: "Amenities", imageUrl: type!))
                                                }
                                            }
                                            
                                            //print(forecast.placeId ?? "place id nil")
                                        }
                                        
                                        
                                        for state in self.markersArray {
                                            let state_marker = GMSMarker()
                                            let latMaker = state.lat
                                            let lngMaker = state.long
                                            //  print(latMaker,lngMaker)
                                            state_marker.position = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
                                            state_marker.title = state.name
                                            state_marker.icon = UIImage(named: type!)
                                            state_marker.snippet = state.formattedAddress
                                            state_marker.map = self.mapBackgorundView
                                            self.markerDict.add(state_marker)
                                        }
                                        self.blurEffectView.isHidden = true;
                                        if self.collectionview != nil
                                        {
                                            self.collectionview.removeFromSuperview()
                                        }
                                    }
                                }
                            }
                            else
                            {
                                
                                
                                for markerDet in self.markerDict
                                {
                                    let removeMarker : GMSMarker = markerDet as! GMSMarker
                                    removeMarker.map = nil;
                                }
                                self.markerDict.removeAllObjects()
                                self.markersArray.removeAll()
                                let alertViewController = NYAlertViewController()
                                // Set a title and message
                                alertViewController.title = "Places"
                                if type == "bus_station" {
                                    alertViewController.message = "No Bus stops available for the location."
                                }
                                else{
                                    alertViewController.message = "No results found for given radius. Please increase the radius."
                                }
                                // Customize appearance as desired
                                alertViewController.buttonCornerRadius = 20.0
                                alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.swipeDismissalGestureEnabled = true
                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                alertViewController.buttonColor = UIColor.red
                                // Add alert actions
                                
                                
                                let cancelAction = NYAlertAction(
                                    title: "OK",
                                    style: .cancel,
                                    handler: { (action: NYAlertAction!) -> Void in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                }
                                )
                                
                                alertViewController.addAction(cancelAction)
                                
                                // Present the alert view controller
                                self.present(alertViewController, animated: true, completion: nil)
                                
                            }
                        case .failure(let error):
                            let alertViewController = NYAlertViewController()
                            // Set a title and message
                            alertViewController.title = "Places"
                            alertViewController.message = "Server Error! Please try again later!"
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                            print(error)
                        }
                }
            }
        }
        else{
            self.hideLoader()
            self.view.makeToast("Location access was disabled. Please enable Location permissions for TARC.")
        }
    }
    
    
    func getbusStops(type: String?) {
        // if popupopen {
        self.menuButton.isEnabled = true
        //   popupopen = false
        // }
        // listViewBtnAction()
        self.closelistview()
        var locationString = ""
        markerClearButton.isHidden = false
        listViewBtn.isHidden = false
        
        listViewBtnBackView.isHidden = false
        
        if sourceLatTrip != 0.0 {
            if amenityTypeString == "Near my source"
            {
                if sourceTextField.text == "Current Location" || isCurrentLoc
                {
                    
                    locationString = "\((sourceLatTrip)),\((sourceLngTrip))"
                }
                else
                {
                    locationString = "\((sourceLatTrip)),\((sourceLngTrip))"
                }
            }
            else if amenityTypeString == "Near my destination"
            {
                locationString = "\(destinationLatTrip),\(destinationLngTrip)"
            }
            
            
            let url = "\(APIUrl.ZIGTARCBASEAPI)TarcWebAPI/api/Stop/GetNearestBusStops"
            let parameters: Parameters = [
                "LatLong":locationString,
                "radius": "\(300)",
                "type":type ?? "park"
                
            ]
            
            print("https://tripplan.ridetarc.org/TarcWebAPI/api/Stop/GetNearestBusStops?LatLong=\(locationString)&radius=\(300)&type=\((type)!)")
            if locationString != "0.0,0.0"{
                
                
                //ArunCrashes
                
                var paymentURL1: URL
                
                paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)TarcWebAPI/api/Stop/GetNearestBusStops?LatLong=\(locationString)&radius=\(300)&type=\((type)!)")!
                
                var request1 = URLRequest(url: paymentURL1)
                request1.httpMethod = "GET"
                URLSession.shared.dataTask(with: request1) { (data, response, error) -> Void in
                    do
                    {
                        if data != nil
                        {
                            if  self.markerDict != nil
                            {
                            //   DispatchQueue.main.async {
                            for markerDet in self.markerDict
                            {
                                let removeMarker : GMSMarker = markerDet as! GMSMarker
                                removeMarker.map = nil;
                            }
                            self.markerDict.removeAllObjects()
                            self.markersArray.removeAll()
                            let scheduleArray = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray)!
                            
                            print("\(scheduleArray)")
                            if scheduleArray.count>0
                            {
                                for busStop in scheduleArray {
                                    
                                    let busStopDict = busStop as! NSDictionary
                                    let nameString = busStopDict.value(forKey: "Stop_name") as! NSString
                                    let latString = busStopDict.value(forKey: "Stop_lat") as! NSString
                                    let longString = busStopDict.value(forKey: "Stop_lon") as! NSString
                                    let placeidString = busStopDict.value(forKey: "Stop_id") as! NSString
                                    let latitudeDegrees : CLLocationDegrees = (latString as NSString).doubleValue
                                    let lontitudeDegrees : CLLocationDegrees = (longString as NSString).doubleValue
                                    
                                    self.markersArray.append(State(name: nameString as String, long: latitudeDegrees, lat: lontitudeDegrees, placeId: placeidString as String, distanceVale: " ", formattedAddress: nameString as String, type: "Amenities", imageUrl: type!))
                                    
                                }
                                
                                for state in self.markersArray {
                                    DispatchQueue.main.async {
                                        let state_marker = GMSMarker()
                                        let latMaker = state.lat
                                        let lngMaker = state.long
                                        //  print(latMaker,lngMaker)
                                        state_marker.position = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
                                        state_marker.title = state.name
                                        state_marker.icon = UIImage(named: type!)
                                        state_marker.snippet = state.formattedAddress
                                        state_marker.map = self.mapBackgorundView
                                        self.markerDict.add(state_marker)
                                    }
                                }
                                DispatchQueue.main.async {
                                    
                                    self.blurEffectView.isHidden = true;
                                }
                                if self.collectionview != nil
                                {
                                    DispatchQueue.main.async {
                                        self.collectionview.removeFromSuperview()
                                    }
                                }
                            }
                            }
                        }
                    } catch {
                        
                    }
                    
                    }.resume();
                
                
                
                
            
            }
        }
        else{
            self.hideLoader()
            self.view.makeToast("Location access was disabled. Please enable Location permissions for TARC.")
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath as IndexPath) as! AmenitiesCollectionViewCell
        
        let imageView = UIImageView()
        let cellName : String?
        let imageName: String?
        if indexPath.row == 0 {
            
            imageName = "restaurant-ame"
            cellName = "FOOD & DRINK"
        }
        else if indexPath.row == 1{
            
            imageName = "store"
            cellName = "SHOPPING"
        }
        else if indexPath.row == 2{
            imageView.backgroundColor = UIColor(red: 64.0/255.0, green: 224.0/255.0, blue: 208.0/255.0, alpha: 1.0)
            imageName = "transport"
            cellName = "TRAVEL"
        }
        else if indexPath.row == 3{
            
            imageName = "public_utility"
            cellName = "SERVICES"
        }
        else if indexPath.row == 4{
            
            imageName = "fun_activities"
            cellName = "ACTIVITIES"
        }
        else if indexPath.row == 5{
            
            imageName = "Hospital_Places"
            cellName = "HEALTH"
        }
        else if indexPath.row == 6{
            
            imageName = "school_Icon"
            cellName = "EDUCATION"
        }
        else if indexPath.row == 7{
            
            imageName = "public_Places"
            cellName = "GOVERNMENT"
        }
            //        else if indexPath.row == 8{
            //            imageName = "parking.png"
            //            cellName = "PARKING"
            //        }
        else if indexPath.row == 8
        {
            imageName = "louveloIconBox"
            cellName = "LOUVELO"
        }
        else if indexPath.row == 9
        {
            imageName = "tourism_Icon"
            cellName = "EVENTS"
        }
            //        else if indexPath.row == 11
            //        {
            //            imageName = "places_Icon"
            //            cellName = "PLACES"
            //        }
        else
        {
            imageName = "social_service"
            cellName = "SOCIAL SERVICE"
        }
        
        
        let image = UIImage(named: imageName!)
        
        cell.amenitiesImage.frame = CGRect(x: amenityImageViewX, y: amenityImageViewY, width: amenityImageViewWidth, height: amenityImageViewHeight)
        cell.amenitiesImage.image = image;
        cell.contentView.addSubview(cell.amenitiesImage)
        
        
        cell.amenitiesText.frame = CGRect.init(x: amenityCellLabelX, y: amenityCellLabelY, width: Int(cell.frame.size.width) - amenityCellLabelWidth, height: amenityCellLabelHeight)
        cell.amenitiesText.text = cellName
        cell.amenitiesText.textColor = UIColor.black
        cell.amenitiesText.textAlignment = NSTextAlignment.left
        cell.amenitiesText.font = UIFont.setTarcRegular(size: 12.0)
        cell.contentView.addSubview(cell.amenitiesText)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        print("cell size : %f",((self.amenityView.frame.width-16)/2))
        return CGSize(width: ((self.amenityView.frame.width-18)/2), height: 60)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "Bakery", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "bakery"
                self.getPlaces(type: "bakery")
            }))
            actionController.addAction(Action(ActionData(title: "Bar", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "bar"
                self.getPlaces(type: "bar")
                
            }))
            actionController.addAction(Action(ActionData(title: "Grocery", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "grocery_or_supermarket"
                self.getPlaces(type: "grocery_or_supermarket")
            }))
            actionController.addAction(Action(ActionData(title: "Restuarant", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "restaurant"
                self.getPlaces(type: "restaurant")
            }))
            actionController.addAction(Action(ActionData(title: "Liquor Store", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "liquor_store"
                self.getPlaces(type: "liquor_store")
            }))
            actionController.addAction(Action(ActionData(title: "Cafe", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "cafe"
                self.getPlaces(type: "cafe")
            }))
            actionController.addAction(Action(ActionData(title: "Free Breakfast", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = false
                self.typeString = "Free_Breakfast"
                self.getPlaces(type: "Free_Breakfast")
            }))
            actionController.addAction(Action(ActionData(title: "Chili", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = false
                self.typeString = "Chili"
                self.getPlaces(type: "Chili")
            }))
            actionController.addAction(Action(ActionData(title: "Breweries", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = false
                self.typeString = "Breweries"
                self.getPlaces(type: "Breweries")
            }))
            actionController.addAction(Action(ActionData(title: "Wineries & Vineyards", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = false
                self.typeString = "Wineries_And_Vineyards"
                self.getPlaces(type: "Wineries_And_Vineyards")
            }))
            
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            present(actionController, animated: true, completion: nil)
        }
        else if indexPath.row == 1 {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "Book Store", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "book_store"
                self.getPlaces(type: "book_store")
            }))
            actionController.addAction(Action(ActionData(title: "Convenience store", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "convenience_store"
                self.getPlaces(type: "convenience_store")
            }))
            actionController.addAction(Action(ActionData(title: "Car Dealer", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "car_dealer"
                self.getPlaces(type: "car_dealer")
            }))
            actionController.addAction(Action(ActionData(title: "Clothing", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "clothing_store"
                self.getPlaces(type: "clothing_store")
            }))
            actionController.addAction(Action(ActionData(title: "Hardware", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "hardware_store"
                self.getPlaces(type: "hardware_store")
            }))
            actionController.addAction(Action(ActionData(title: "Home Goods", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "home_goods_store"
                self.getPlaces(type: "home_goods_store")
            }))
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            
            present(actionController, animated: true, completion: nil)
        }
        else if indexPath.row == 2 {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "Bus Station", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "bus_station"
                self.getPlaces(type: "bus_station")
            }))
            actionController.addAction(Action(ActionData(title: "Lodging", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "lodging"
                self.getPlaces(type: "lodging")
            }))
            actionController.addAction(Action(ActionData(title: "Car Rental", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "car_rental"
                self.getPlaces(type: "car_rental")
            }))
            actionController.addAction(Action(ActionData(title: "Airport", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "airport"
                self.getPlaces(type: "airport")
            }))
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            present(actionController, animated: true, completion: nil)
        }
        else if indexPath.row == 3 {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "ATM", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "atm"
                self.getPlaces(type: "atm")
            }))
            actionController.addAction(Action(ActionData(title: "Bank", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "bank"
                self.getPlaces(type: "bank")
            }))
            actionController.addAction(Action(ActionData(title: "Florist", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "florist"
                self.getPlaces(type: "florist")
            }))
            actionController.addAction(Action(ActionData(title: "Laundry", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "laundry"
                self.getPlaces(type: "laundry")
            }))
            actionController.addAction(Action(ActionData(title: "Lawyer", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "lawyer"
                self.getPlaces(type: "lawyer")
            }))
            actionController.addAction(Action(ActionData(title: "Post office", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "post_office"
                self.getPlaces(type: "post_office")
            }))
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            present(actionController, animated: true, completion: nil)
        }
        else if indexPath.row == 4 {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "Bowling alley", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "bowling_alley"
                self.getPlaces(type: "bowling_alley")
            }))
            actionController.addAction(Action(ActionData(title: "Casino", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "casino"
                self.getPlaces(type: "casino")
            }))
            actionController.addAction(Action(ActionData(title: "Movie Theatre", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "movie_theater"
                self.getPlaces(type: "movie_theater")
            }))
            actionController.addAction(Action(ActionData(title: "Museum", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "museum"
                self.getPlaces(type: "museum")
            }))
            actionController.addAction(Action(ActionData(title: "Night Club", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "night_club"
                self.getPlaces(type: "night_club")
            }))
            actionController.addAction(Action(ActionData(title: "Park", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "park"
                self.getPlaces(type: "park")
            }))
            actionController.addAction(Action(ActionData(title: "Gaming", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = false
                self.typeString = "Gaming"
                self.getPlaces(type: "Gaming")
            }))
            actionController.addAction(Action(ActionData(title: "Sports", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = false
                self.typeString = "Sports"
                self.getPlaces(type: "Sports")
            }))
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            present(actionController, animated: true, completion: nil)
        }
        else if indexPath.row == 5 {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "Hospital", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "hospital"
                self.getPlaces(type: "hospital")
            }))
            actionController.addAction(Action(ActionData(title: "Pharmacy", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "pharmacy"
                self.getPlaces(type: "pharmacy")
            }))
            
            actionController.addAction(Action(ActionData(title: "Veterinary Care", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "veterinary_care"
                self.getPlaces(type: "veterinary_care")
            }))
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            present(actionController, animated: true, completion: nil)
        }
        else if indexPath.row == 6 {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "School", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "school"
                self.getPlaces(type: "school")
            }))
            actionController.addAction(Action(ActionData(title: "University", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "university"
                self.getPlaces(type: "university")
            }))
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            present(actionController, animated: true, completion: nil)
        }
        else if indexPath.row == 7 {
            let actionController = YoutubeActionController()
            actionController.addAction(Action(ActionData(title: "City Hall", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "city_hall"
                self.getPlaces(type: "city_hall")
            }))
            actionController.addAction(Action(ActionData(title: "Court House", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "courthouse"
                self.getPlaces(type: "courthouse")
            }))
            actionController.addAction(Action(ActionData(title: "Fire Station", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "fire_station"
                self.getPlaces(type: "fire_station")
            }))
            
            actionController.addAction(Action(ActionData(title: "Police", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                self.isPlaceSelected = true
                self.typeString = "police"
                self.getPlaces(type: "police")
            }))
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            present(actionController, animated: true, completion: nil)
        }
            //                    else if(indexPath.row == 8)
            //                    {
            //                        loadKml("Parking")
            //                    }
        else if(indexPath.row == 8)
        {
            self.menuButton.isEnabled = true
            
            let url = "https://lou.publicbikesystem.net/ube/gbfs/v1/en/station_status"
            let parameters: Parameters = [:]
            
            
            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<RedBikeStatusClass>) in
                    switch response.result {
                    case .success:
                        print(response.result.value ?? "nil")
                        self.redBikeStatus = response
                        let url1 = "https://lou.publicbikesystem.net/ube/gbfs/v1/en/station_information"
                        let parameters1: Parameters = [:]
                        
                        
                        Alamofire.request(url1, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                            .responseObject{ (response: DataResponse<RedBikeClass>) in
                                switch response.result {
                                case .success:
                                    print(response.result.value ?? "nil")
                                    self.redBikeInformation = response
                                    DispatchQueue.main.async {
                                        for markerDet in self.markerDict
                                        {
                                            let removeMarker : GMSMarker = markerDet as! GMSMarker
                                            removeMarker.map = nil;
                                        }
                                        self.markerDict.removeAllObjects()
                                        self.markersArray.removeAll()
                                        
                                        
                                        var redCount = 0;
                                        for plotRedBike in (self.redBikeInformation.result.value?.data?.stations)!
                                        {
                                            
                                            
                                            let latMaker = Double(plotRedBike.lat!)
                                            let lngMaker = Double(plotRedBike.lon!)
                                            var coordinate0 = CLLocation()
                                            print(latMaker)
                                            print(self.amenityTypeString)
                                            //
                                            
                                            let coordinate1 = CLLocation(latitude: latMaker, longitude: lngMaker)
                                            print(coordinate1)
                                            let distanceInMeters = coordinate0.distance(from: coordinate1) // result is in meters
                                            print("Distance \(distanceInMeters)")
                                            let userDefaults = UserDefaults.standard
                                            let distanceValue = userDefaults.integer(forKey: "distanceValue")
                                            
                                            redCount += 1
                                            // under 1 mile
                                            let state_marker = GMSMarker()
                                            
                                            print(latMaker,lngMaker)
                                            
                                            for plotStatus in (self.redBikeStatus.result.value?.data?.stations)!
                                            {
                                                if plotRedBike.stationId == plotStatus.stationId
                                                {
                                                    state_marker.snippet = "\(plotStatus.numBikesAvailable ?? 0) Bikes & \(plotStatus.numDocksAvailable ?? 0) Docks Available"
                                                    
                                                    print(plotRedBike.name!)
                                                    print("\(plotRedBike.lon!), \(plotRedBike.lat!)")
                                                    print(String(plotRedBike.stationId!))
                                                    print(plotRedBike.name!)
                                                    print(plotStatus.numBikesAvailable!)
                                                    
                                                    self.markersArray.append(State(name: plotRedBike.name!, long: CLLocationDegrees(plotRedBike.lon!), lat: CLLocationDegrees(plotRedBike.lat!), placeId: " ", distanceVale: String(plotRedBike.stationId!), formattedAddress: "\(plotRedBike.name!)\n\(plotStatus.numBikesAvailable ?? 0) Bikes & \(plotStatus.numDocksAvailable ?? 0) Docks Available", type: "LOUVELO", imageUrl: "LouveloMapIcon"))
                                                    
                                                    print(plotStatus.numBikesAvailable!)
                                                    
                                                    
                                                    
                                                    
                                                }
                                            }
                                            
                                            
                                            
                                            state_marker.position = CLLocationCoordinate2D(latitude: latMaker, longitude: lngMaker)
                                            state_marker.title = plotRedBike.name!
                                            for plotStatus in (self.redBikeStatus.result.value?.data?.stations)!
                                            {
                                                print(plotRedBike.stationId == plotStatus.stationId)
                                                if plotRedBike.stationId == plotStatus.stationId
                                                {
                                                    state_marker.snippet = "\(plotStatus.numBikesAvailable ?? 0) Bikes & \(plotStatus.numDocksAvailable ?? 0) Docks Available"
                                                }
                                            }
                                            
                                            state_marker.icon = UIImage(named: "LouveloMapIcon")
                                            state_marker.map = self.mapBackgorundView
                                            self.markerDict.add(state_marker)
                                            
                                            
                                        }
                                        
                                        self.blurEffectView.isHidden = true;
                                    }
                                case .failure(let error):
                                    self.showErrorDialogBox(viewController: self)
                                    print(error.localizedDescription)
                                }
                        }
                        
                    case .failure(let error):
                        self.showErrorDialogBox(viewController: self)
                        print(error.localizedDescription)
                    }
            }
        }
            //        else if indexPath.row == 10{
            //            let actionController = YoutubeActionController()
            //            actionController.addAction(Action(ActionData(title: "Amusement Park", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Amusement_Park"
            //                self.getCinciPlaces(type: "Amusement_Park")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Animal Attractions", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Animal_Attraction"
            //                self.getCinciPlaces(type: "Animal_Attraction")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Outdoors and Recreation", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Outdoors_And_Recreation"
            //                self.getCinciPlaces(type: "Outdoors_And_Recreation")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Performing Arts", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Performing_Arts"
            //                self.getCinciPlaces(type: "Performing_Arts")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Tours", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Tours"
            //                self.getCinciPlaces(type: "Tours")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Visual Arts and Culture", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Visual_Arts_And_Culture"
            //                self.getCinciPlaces(type: "Visual_Arts_And_Culture")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            //
            //            present(actionController, animated: true, completion: nil)
            //        }
            //        else if indexPath.row == 11{
            //            let actionController = YoutubeActionController()
            //            actionController.addAction(Action(ActionData(title: "Hotels", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Hotels"
            //                self.getCinciPlaces(type: "Hotels")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "B&Bs/Rentals", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "B_And_Bs_Rentals"
            //                self.getCinciPlaces(type: "B_And_Bs_Rentals")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Indoor Pool", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Indoor_Pool"
            //                self.getCinciPlaces(type: "Indoor_Pool")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Downtown", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Downtown"
            //                self.getCinciPlaces(type: "Downtown")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Riverfront", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Riverfront"
            //                self.getCinciPlaces(type: "Riverfront")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            //
            //            present(actionController, animated: true, completion: nil)
            //        }
            //        else if indexPath.row == 8
            //        {
            //            //            self.isPlaceSelected = false
            //            //            self.typeString = "Events"
            //            //            self.getCinciPlaces(type: "Events")
            //            let actionController = YoutubeActionController()
            //            actionController.addAction(Action(ActionData(title: "Aging", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Aging"
            //                self.plotSocialServices(type: "Aging")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Aging & Disability", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Aging&Disability"
            //                self.plotSocialServices(type: "Aging&Disability")
            //
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Crisis", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Crisis"
            //                self.plotSocialServices(type: "Crisis")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Education", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Education"
            //                self.plotSocialServices(type: "Education")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Employment", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Employment"
            //                self.plotSocialServices(type: "Employment")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Financial", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Financial"
            //                self.plotSocialServices(type: "Financial")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Health", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Health"
            //                self.plotSocialServices(type: "Health")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Housing", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Housing"
            //                self.plotSocialServices(type: "Housing")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Legal", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Legal"
            //                self.plotSocialServices(type: "Legal")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Other", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Other"
            //                self.plotSocialServices(type: "Other")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Transportation", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Transportation"
            //                self.plotSocialServices(type: "Transportation")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Veteran", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Veteran"
            //                self.plotSocialServices(type: "Veteran")
            //            }))
            //
            //            present(actionController, animated: true, completion: nil)
            //
            //        }
        else if indexPath.row == 9{
            let actionController = YoutubeActionController()
            if self.eventArrayList.count > 0 {
                for Eventname in self.eventArrayList {
                    
                    actionController.addAction(Action(ActionData(title: Eventname, image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = false
                        self.typeString = Eventname
                        self.plotTARCTourism(category:Eventname)
                    }))
                }
            }
            actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
            
            //
            //            actionController.addAction(Action(ActionData(title: "Restaurants", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Restaurants"
            //                self.plotTARCTourism(category:"Restaurants")
            //
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Hotels", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Hotels"
            //                self.plotTARCTourism(category: "Hotels")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Events", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Events"
            //                self.plotTARCTourism(category: "Events")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Attractions", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Attractions"
            //                self.plotTARCTourism(category:"Attractions")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Night life", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Night life"
            //                self.plotTARCTourism(category:"Night life")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Music", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Music"
            //                self.plotTARCTourism(category:"Music")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Bars", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Bars"
            //                self.plotTARCTourism(category: "Bars")
            //            }))
            //            actionController.addAction(Action(ActionData(title: "Motels", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
            //                self.isPlaceSelected = false
            //                self.typeString = "Motels"
            //                self.plotTARCTourism(category: "Motels")
            //            }))
            present(actionController, animated: true, completion: nil)
        }
        else{
            return
        }
        
    }
    
    
    
    
    func getplotTARCTourismCategory(){
        
        
        
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/events/getlist").responseArray { (response: DataResponse<[GetGoLouisville]>) in
            switch response.result {
            case .success:
                
                print(response.result.value!.count)
                let eventarray = response.result.value
                if eventarray != nil {
                    for Eventlist in eventarray! {
                        
                        let eventname = Eventlist.EventTitle
                        self.eventArrayList.append(eventname!)
                        
                    }
                    
                }
                else{
                    
                    self.eventArrayList.removeAll()
                    
                }
                
            case .failure(let error):
                // self.showErrorDialogBox(viewController: self)
                self.hideLoader()
                self.eventArrayList.removeAll()
                print(error.localizedDescription)
            }
        }
        
    }
    func plotTARCTourism(category:String){
        self.showLoader()
        var EventCountList = 0
        var latString = ""
        var lngString = ""
        
        markerClearButton.isHidden = false
        listViewBtn.isHidden = false
        listViewBtnBackView.isHidden = false
        print("\(sourceLatTrip)")
        if sourceLatTrip != 0.0 {
            if amenityTypeString == "Near my source"
            {
                if sourceTextField.text == "Current Location" || isCurrentLoc
                {
                    latString = "\(sourceLatTrip)"
                    lngString = "\((sourceLngTrip))"
                    
                }
                else
                {
                    latString = "\((sourceLatTrip))"
                    lngString = "\((sourceLngTrip))"
                }
            }
            else if amenityTypeString == "Near my destination"
            {
                latString = "\(destinationLatTrip)"
                lngString = "\(destinationLngTrip)"
                
            }
            
            
            
            let plotTARCURL = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/events/getlist"
            var tourismArray = [NSDictionary]()
            let url1 = URL(string: plotTARCURL)
            URLSession.shared.dataTask(with: url1!, completionHandler: {
                (data, response, error) in
                if(error != nil){
                    print("error")
                }else{
                    do{
                        let json : NSArray = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSArray
                        //let posts = json["posts"] as? [[String: Any]] ?? []
                        print("Response:\(json)")
                        //self.mapView.clear()
                        
                        OperationQueue.main.addOperation({
                            for services in json
                            {
                                
                                let tourismData : NSDictionary = services as! NSDictionary
                                tourismArray.append(tourismData)
                                print(tourismData.object(forKey: "EventName")!)
                                let eventName = tourismData.object(forKey: "EventName") as! String
                                if eventName == category{
                                    
                                    DispatchQueue.main.async{
                                        for markerDet in self.markerDict
                                        {
                                            let removeMarker : GMSMarker = markerDet as! GMSMarker
                                            removeMarker.map = nil;
                                        }
                                        self.markerDict.removeAllObjects()
                                        self.markersArray.removeAll()
                                        
                                        
                                        let list = tourismData.object(forKey: "list") as! NSArray
                                        
                                        for listData in list{
                                            
                                            let lat = Double((listData as AnyObject).object(forKey: "latitude") as! String)
                                            let long = Double((listData as AnyObject).object(forKey: "longitude") as! String)
                                            var distanceInMeters:Double = 0.0
                                            if lat != nil || long != nil {
                                                
                                                let coordinate0 = CLLocation(latitude: Double(latString)!, longitude: Double(lngString)!)
                                                let coordinate1 = CLLocation(latitude: lat! , longitude: long!)
                                                
                                                distanceInMeters = coordinate0.distance(from: coordinate1) // result is in meters
                                            }
                                            else {
                                                distanceInMeters = Double(self.sliderValue*1000000) + 20.0
                                            }
                                            
                                            let radius = self.sliderValue*1000000
                                            
                                            if Int(distanceInMeters) <= radius{
                                                
                                                var title = (listData as AnyObject).object(forKey: "title") as! String
                                                let StringRemoveSpace =  title.components(separatedBy: .whitespacesAndNewlines)
                                                let StringJoin = (StringRemoveSpace.filter { $0 != "" }.joined(separator: " "))
                                                //  print(StringJoin.joinWithSeparator(" "))
                                                // title = String(title.filter { !" \t".contains($0) })
                                                title = StringJoin.decodeEnt()
                                                
                                                self.markersArray.append(State(name: title, long: CLLocationDegrees(lat!) , lat: CLLocationDegrees(long!), placeId:" ", distanceVale: "", formattedAddress: (listData as AnyObject).object(forKey: "summary") as! String, type: "plotTARCTourism", imageUrl: (listData as AnyObject).object(forKey: "imgurl") as! String))
                                                
                                            }
                                            
                                            
                                        }
                                        
                                        
                                        if self.markersArray.count > 0{
                                            for state in self.markersArray {
                                                let state_marker = GMSMarker()
                                                let latMaker = state.lat
                                                let lngMaker = state.long
                                                print(latMaker,lngMaker)
                                                state_marker.position = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
                                                state_marker.title = state.name
                                                state_marker.icon = UIImage(named: category)
                                                state_marker.snippet = state.formattedAddress
                                                state_marker.map = self.mapBackgorundView
                                                self.markerDict.add(state_marker)
                                                self.hideLoader()
                                            }
                                        }else{
                                            self.hideLoader()
                                            let alertViewController = NYAlertViewController()
                                            // Set a title and message
                                            alertViewController.title = "Events"
                                            alertViewController.message = "There are no events available for this location."
                                            
                                            // Customize appearance as desired
                                            alertViewController.buttonCornerRadius = 20.0
                                            alertViewController.view.tintColor = self.view.tintColor
                                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                            alertViewController.swipeDismissalGestureEnabled = true
                                            alertViewController.backgroundTapDismissalGestureEnabled = true
                                            alertViewController.buttonColor = UIColor.red
                                            // Add alert actions
                                            
                                            
                                            let cancelAction = NYAlertAction(
                                                title: "OK",
                                                style: .cancel,
                                                handler: { (action: NYAlertAction!) -> Void in
                                                    
                                                    self.dismiss(animated: true, completion: nil)
                                            }
                                            )
                                            
                                            alertViewController.addAction(cancelAction)
                                            
                                            // Present the alert view controller
                                            self.present(alertViewController, animated: true, completion: nil)
                                            
                                        }
                                        
                                        self.blurEffectView.isHidden = true;
                                        self.collectionview.removeFromSuperview()
                                        
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                }
                                    
                                else{
                                    
                                    EventCountList =  EventCountList + 1
                                }
                                
                            }
                            if json.count == EventCountList {
                                self.hideLoader()
                                print("There are no events available for this location ")
                                let alertViewController = NYAlertViewController()
                                // Set a title and message
                                alertViewController.title = "Events"
                                alertViewController.message = "There are no events available for this location."
                                
                                // Customize appearance as desired
                                alertViewController.buttonCornerRadius = 20.0
                                alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.swipeDismissalGestureEnabled = true
                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                alertViewController.buttonColor = UIColor.red
                                // Add alert actions
                                
                                
                                let cancelAction = NYAlertAction(
                                    title: "OK",
                                    style: .cancel,
                                    handler: { (action: NYAlertAction!) -> Void in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                }
                                )
                                
                                alertViewController.addAction(cancelAction)
                                
                                // Present the alert view controller
                                self.present(alertViewController, animated: true, completion: nil)
                                
                            }
                        })
                        
                        
                    }catch let error as NSError{
                        print("error:\(error)")
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Places"
                        alertViewController.message = "Server Error! Please try again later!"
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                    }
                }
            }).resume()
            
            
        }
        else{
            self.hideLoader()
            self.view.makeToast("Location access was disabled. Please enable Location permissions for TARC.")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == autofillTableView1 || tableView == autofillTableView
        {
            if responseVarAddress.count != 0 {
                return responseVarAddress.count + SavedAddressArray.count
            }
            else if AddressSearch.count != 0 {
                return AddressSearch.count + SavedAddressArray.count
            }
            else{
                return 0
            }
            
        }
        else
        {
            return 12
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(responseVarAddress)
        
        // create a new cell if needed or reuse an old one
        if tableView == autofillTableView {
            let SavedTripCount = SavedAddressArray.count
            print(SavedTripCount)
            
            
            let cell = autofillTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MyTicketTableViewCell
            tableView.separatorColor = UIColor.clear;
            cell.separatorInset = .zero
            var place :GMSPlace
            
            
            cell.fromLable.frame = CGRect.init(x: 5, y: 0, width: cell.contentView.size.width - 5, height: 58)
            cell.fromLable.textColor = UIColor.black
            cell.fromLable.font = UIFont.setTarcRegular(size: 16.0)
            cell.fromLable.lineBreakMode = .byWordWrapping
            cell.fromLable.numberOfLines = 3
            cell.contentView.addSubview(cell.fromLable)
            
            
            //            cell.toLable.frame = CGRect.init(x: 5, y: 35, width: cell.frame.size.width, height: 24)
            //            cell.toLable.textColor = UIColor.gray
            //            cell.toLable.font = UIFont.systemFont(ofSize: 12.0)
            //            cell.contentView.addSubview(cell.toLable)
            
            cell.fromTextLable.frame = CGRect.init(x: 0, y: 59, width: cell.frame.size.width, height: 1)
            cell.fromTextLable.backgroundColor = .black
            cell.contentView.addSubview(cell.fromTextLable)
            if indexPath.row < SavedTripCount
            {
                
                let dict = SavedAddressArray[indexPath.row] as NSDictionary
                                let TitleName = dict["Title"] as? String
                                let priceName = dict["Address"] as? String
                
                cell.fromLable.text = "\((TitleName)!) - \((priceName)!)"

            }
                else
            {
            if responseVarAddress.count != 0 {
                let indexRow = indexPath.row
                let currentRow = indexRow - SavedTripCount

                if responseVarAddress.count > currentRow
                {
                    //               // place = autoFillArray[indexPath.row] as! GMSPlace
                    // let AddressString = responseVarAddress[indexPath.row].FullAddress!.components(separatedBy: .punctuationCharacters).joined().components(separatedBy: " ").filter{!$0.isEmpty}
                    
                    
                    cell.fromLable.text = "\(responseVarAddress[currentRow].FullAddress!)"
                    //
                    
                    print((responseVarAddress[currentRow].FullAddress)!)
                }
            }
            else if AddressSearch.count != 0{
                let indexRow = indexPath.row
                let currentRow = indexRow - SavedTripCount
                cell.fromTextLable.backgroundColor = .gray
                cell.fromLable.text = "\(AddressSearch[currentRow].description!)"
                cell.fromTextLable.frame = CGRect.init(x: 0, y: 58, width: cell.frame.size.width, height: 2)
            }
        }
            
            return cell
        }
        else if tableView == autofillTableView1
        {
            let SavedTripCount = SavedAddressArray.count
                     print(SavedTripCount)
            
            let cell = autofillTableView1.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MyTicketTableViewCell
            tableView.separatorColor = UIColor.clear;
            cell.separatorInset = .zero
            var place :GMSPlace
            
            
            cell.fromLable.frame = CGRect.init(x: 5, y: 0, width: cell.contentView.size.width - 5, height: 58)
            cell.fromLable.textColor = UIColor.black
            cell.fromLable.font = UIFont.setTarcRegular(size: 16.0)
            cell.fromLable.lineBreakMode = .byWordWrapping
            cell.fromLable.numberOfLines = 3
            cell.contentView.addSubview(cell.fromLable)
            
            
            //            cell.toLable.frame = CGRect.init(x: 0, y: 27, width: cell.frame.size.width, height: 27)
            //            cell.toLable.textColor = UIColor.gray
            //            cell.toLable.font = UIFont.systemFont(ofSize: 15.0)
            //            cell.contentView.addSubview(cell.toLable)
            
            cell.fromTextLable.frame = CGRect.init(x: 0, y: 59, width: cell.frame.size.width, height: 1)
            cell.fromTextLable.backgroundColor = .black
            cell.contentView.addSubview(cell.fromTextLable)
            if indexPath.row < SavedTripCount
               {
                   
                   let dict = SavedAddressArray[indexPath.row] as NSDictionary
                                   let TitleName = dict["Title"] as? String
                                   let priceName = dict["Address"] as? String
                   
                   cell.fromLable.text = "\((TitleName)!) - \((priceName)!)"

               }
            else
            {
            if responseVarAddress.count != 0 {
                let indexRow = indexPath.row
                let currentRow = indexRow - SavedTripCount
                if responseVarAddress.count > currentRow
                {
                    //               // place = autoFillArray[indexPath.row] as! GMSPlace
                    // let AddressString = responseVarAddress[indexPath.row].FullAddress!.components(separatedBy: .punctuationCharacters).joined().components(separatedBy: " ").filter{!$0.isEmpty}
                    
                    
                    cell.fromLable.text = "\(responseVarAddress[currentRow].FullAddress!)"
                    //
                    
                    print((responseVarAddress[indexPath.row].FullAddress)!)
                }
            }
            else if AddressSearch.count != 0{
                let indexRow = indexPath.row
                             let currentRow = indexRow - SavedTripCount
                
                cell.fromLable.text = "\(AddressSearch[currentRow].description!)"
            }
            
            }
            return cell
        }
        else
        {
            let cell:UITableViewCell = self.menuTabelView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
            tableView.separatorColor = UIColor.clear;
            cell.separatorInset = .zero
            cell.textLabel?.font = UIFont.setTarcRegular(size: 17)
            if indexPath.row == 0
            {
                cell.textLabel?.text = "Dashboard"
            }
            else if indexPath.row == 1
            {
                cell.textLabel?.text = "My Ticket"
            }
            else if indexPath.row == 2
            {
                cell.textLabel?.text = "Real-Time Schedules"
            }
            else if indexPath.row == 3
                {
                    cell.textLabel?.text = "Alerts / News"
                }
                
            else if indexPath.row == 4
            {
                cell.textLabel?.text = "Saved Trips"
            }
            else if indexPath.row == 5
            {
                cell.textLabel?.text = "Calendar Events"
            }
            else if indexPath.row == 6
            {
                cell.textLabel?.text = "Preferences"
            }
            else if indexPath.row == 7
            {
                
                
                cell.textLabel?.text = "Support"
                
            }
            else if indexPath.row == 8
            {
                cell.textLabel?.text = "Privacy Policy"
            }
                else if indexPath.row == 9
                           {
                               cell.textLabel?.text = "Feedback"
                           }
            else if indexPath.row == 10
            {
                cell.textLabel?.text = "Help"
            }
           

            else if indexPath.row == 11
                       {
                           cell.textLabel?.text = "Logout"
                       }
            let lineLabel = UILabel()
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                lineLabel.frame = CGRect.init(x: 0, y: 63, width: cell.contentView.frame.size.width, height: 1)
            }
            else
            {
                lineLabel.frame = CGRect.init(x: 0, y: 43, width: cell.contentView.frame.size.width, height: 1)
            }
            lineLabel.backgroundColor = UIColor.black
            cell.contentView.addSubview(lineLabel)
            return cell
        }
        // set the text from the data model
        
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        //        self.responseVarAddress.removeAll()
        //        self.AddressSearch.removeAll()
        
        if tableView == autofillTableView {
            
             let SavedTripCount = SavedAddressArray.count
                        print(SavedTripCount)

             if indexPath.row < SavedTripCount
            {
                
                let dict = SavedAddressArray[indexPath.row] as NSDictionary
                        let priceName = dict["Address"] as? String
                let latitudeString = dict["latitude"] as? String
                let lotitudeString = dict["longtitude"] as? String
                let placeIDString = dict["PlaceID"] as? String

                                 
                                 let PlaceLatDouble = Double(latitudeString!)
                                 
                                 
                                 let PlaceLongDouble = Double(lotitudeString!)
                                 
                                 DispatchQueue.main.async {
                                     //  self.sourcePlace = (self.autoFillArray[indexPath.row] as? GMSPlace)
                                     print(self.sourcePlace?.coordinate ?? "nil")
                                     
                                     
                                     let latMaker = PlaceLatDouble
                                     let lngMaker = PlaceLongDouble
                                     print(latMaker!,lngMaker!)
                                     self.source_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                                     self.source_marker.title = priceName
                                     
                                     self.source_marker.icon = UIImage(named: "source_Marker")
                                     self.source_marker.map = self.mapBackgorundView
                                     self.sourceLatTrip = PlaceLatDouble!
                                     self.sourceLngTrip = PlaceLongDouble!
                                    self.sourceStringTrip = placeIDString!
                                     self.isCurrentLoc = false;
                                     self.sourceTextField.text = priceName
                                     self.sourceTextField.resignFirstResponder()
                                     //  self.destinationTextField.becomeFirstResponder()
                                     
                                    self.markersArray.append(State(name: priceName!, long: CLLocationDegrees(PlaceLatDouble!), lat: CLLocationDegrees(PlaceLongDouble!), placeId:" ", distanceVale: "", formattedAddress:  priceName!, type: "", imageUrl: ""))
                                     
                                     
                                     
                                     self.resultBackView.isHidden = true
                                     self.CheckFavTrip()
                                     var camera = GMSCameraPosition()
                                     if UIDevice.current.userInterfaceIdiom == .pad{
                                         camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 12.0)
                                     }
                                         
                                     else
                                     {
                                         camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 14.0)
                                     }
                                     self.mapBackgorundView?.animate(to: camera)
                                     self.typeString = "bus_station"
                                     self.getbusStops(type: self.typeString)
                                     
                                 }

            }
            else
             {
                
                let indexRow = indexPath.row
                               let currentRow = indexRow - SavedTripCount
                
            if self.responseVarAddress.count > currentRow || self.AddressSearch.count > currentRow
            {
                
                if self.responseVarAddress.count != 0 {
                    
                    //                    let PlaceLat = self.responseVarAddress[indexPath.row].Lat
                    //                    let PlaceLong = self.responseVarAddress[indexPath.row].Long
                    guard let PlaceLat = self.responseVarAddress[currentRow].Lat else { return }
                    
                    let PlaceLatDouble = Double(PlaceLat)
                    
                    guard let PlaceLong = self.responseVarAddress[currentRow].Long else { return }
                    
                    let PlaceLongDouble = Double(PlaceLong)
                    
                    DispatchQueue.main.async {
                        //  self.sourcePlace = (self.autoFillArray[indexPath.row] as? GMSPlace)
                        print(self.sourcePlace?.coordinate ?? "nil")
                        
                        
                        let latMaker = PlaceLatDouble
                        let lngMaker = PlaceLongDouble
                        print(latMaker!,lngMaker!)
                        self.source_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                        self.source_marker.title = self.responseVarAddress[currentRow].FullAddress
                        
                        self.source_marker.icon = UIImage(named: "source_Marker")
                        self.source_marker.map = self.mapBackgorundView
                        self.sourceLatTrip = PlaceLatDouble!
                        self.sourceLngTrip = PlaceLongDouble!
                        self.sourceStringTrip = self.responseVarAddress[currentRow].placeID!
                        self.isCurrentLoc = false;
                        self.sourceTextField.text = self.responseVarAddress[currentRow].FullAddress
                        self.sourceTextField.resignFirstResponder()
                        //  self.destinationTextField.becomeFirstResponder()
                        
                        self.markersArray.append(State(name: self.responseVarAddress[currentRow].FullAddress!, long: CLLocationDegrees(PlaceLatDouble!), lat: CLLocationDegrees(PlaceLongDouble!), placeId:" ", distanceVale: "", formattedAddress:  self.responseVarAddress[currentRow].FullAddress!, type: "", imageUrl: ""))
                        
                        
                        
                        self.resultBackView.isHidden = true
                        self.CheckFavTrip()
                        var camera = GMSCameraPosition()
                        if UIDevice.current.userInterfaceIdiom == .pad{
                            camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 12.0)
                        }
                            
                        else
                        {
                            camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 14.0)
                        }
                        self.mapBackgorundView?.animate(to: camera)
                        self.typeString = "bus_station"
                        self.getbusStops(type: self.typeString)
                        
                    }
                    
                }
                else if self.AddressSearch.count != 0 {
                    let Place_ID = AddressSearch[currentRow].place_id
                    //  pPlaceID = "ChIJXbmAjccVrjsRlf31U1ZGpDM"
                    self.placesClient.lookUpPlaceID(Place_ID!, callback: { (place, error) -> Void in
                        
                        if let error = error {
                            print("lookup place id query error: \(error.localizedDescription)")
                            return
                        }
                        
                        if let place = place {
                            print("Place name \(place.name)")
                            print("Place address \(place.formattedAddress!)")
                            print("Place placeID \(place.placeID)")
                            // print("Place attributions \((place.attributions)!)")
                            print("\(place.coordinate.latitude)")
                            print("\(place.coordinate.longitude)")
                            self.sourcePlace = place
                            DispatchQueue.main.async {
                                //  self.sourcePlace = (self.autoFillArray[indexPath.row] as? GMSPlace)
                                print(self.sourcePlace?.coordinate ?? "nil")
                                
                                
                                let latMaker = self.sourcePlace?.coordinate.latitude
                                let lngMaker = self.sourcePlace?.coordinate.longitude
                                print(latMaker!,lngMaker!)
                                self.source_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                                self.source_marker.title = self.sourcePlace?.name
                                
                                self.source_marker.icon = UIImage(named: "source_Marker")
                                self.source_marker.map = self.mapBackgorundView
                                self.sourceLatTrip = (self.sourcePlace?.coordinate.latitude)!
                                self.sourceLngTrip = (self.sourcePlace?.coordinate.longitude)!
                                self.sourceStringTrip = (self.sourcePlace?.placeID)!
                                self.isCurrentLoc = false;
                                self.sourceTextField.text = self.sourcePlace?.name
                                self.sourceTextField.resignFirstResponder()
                                //  self.destinationTextField.becomeFirstResponder()
                                
                                self.markersArray.append(State(name: self.sourcePlace!.name!, long: CLLocationDegrees((self.sourcePlace?.coordinate.latitude)!), lat: CLLocationDegrees((self.sourcePlace?.coordinate.longitude)!), placeId:" ", distanceVale: "", formattedAddress:  (self.sourcePlace?.name)!, type: "", imageUrl: ""))
                                
                                
                                
                                self.resultBackView.isHidden = true
                                self.CheckFavTrip()
                                var camera = GMSCameraPosition()
                                if UIDevice.current.userInterfaceIdiom == .pad{
                                    camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 12.0)
                                }
                                    
                                else
                                {
                                    camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 14.0)
                                }
                                self.mapBackgorundView?.animate(to: camera)
                                self.typeString = "bus_station"
                                // self.getPlaces(type: self.typeString)
                                self.getbusStops(type: self.typeString)
                                
                            }
                            
                        } else {
                            print("No place details for \((Place_ID)!)")
                        }
                    })
                }
                
                
            }
        }
        }
        else if tableView == autofillTableView1
        {
            
            let SavedTripCount = SavedAddressArray.count
                         print(SavedTripCount)

              if indexPath.row < SavedTripCount
             {
                
                
                let dict = SavedAddressArray[indexPath.row] as NSDictionary
                              let priceName = dict["Address"] as? String
                      let latitudeString = dict["latitude"] as? String
                      let lotitudeString = dict["longtitude"] as? String
                      let placeIDString = dict["PlaceID"] as? String
                
                                  
                                  let PlaceLatDouble = Double(latitudeString!)
                                  
                                  
                                  let PlaceLongDouble = Double(lotitudeString!)
                                  
                                  
                                  
                                  DispatchQueue.main.async {
                                      // self.destinationPlace = (self.autoFillArray[indexPath.row] as! GMSPlace)
                                      
                                      
                                      self.destinationLatTrip = PlaceLatDouble!
                                      self.destinationLngTrip = PlaceLongDouble!
                                    self.destinationStringTrip = placeIDString!
                                      // print(self.destinationPlace?.coordinate ?? "nil")
                                      self.destinationTextField.text = priceName
                                      self.destinationTextField.resignFirstResponder()
                                      self.resultBackView1.isHidden = true
                                      
                                      
                                      let latMaker = PlaceLatDouble
                                      let lngMaker = PlaceLongDouble
                                      print(latMaker!,lngMaker!)
                                      self.destination_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                                      self.destination_marker.icon = UIImage(named: "destination_Marker")
                                      self.destination_marker.map = self.mapBackgorundView
                                      self.CheckFavTrip()
                                      self.destination_marker.title = priceName

                                      //                        self.markersArray.append(State(name: self.responseVarAddress[indexPath.row].FullAddress!, long: CLLocationDegrees(PlaceLatDouble!), lat: CLLocationDegrees(PlaceLongDouble!), placeId:" ", distanceVale: "", formattedAddress:  (self.responseVarAddress[indexPath.row].FullAddress)!, type: "", imageUrl: ""))
                                      
                                      
                                      let camera = GMSCameraPosition.camera(withLatitude:latMaker!, longitude: lngMaker!, zoom: 14.0)
                                      self.mapBackgorundView?.animate(to: camera)
                                  }

             }
             else
              {
            let indexRow = indexPath.row
            let currentRow = indexRow - SavedTripCount
            
            if self.responseVarAddress.count > currentRow || self.AddressSearch.count > currentRow
                
            {
                if self.responseVarAddress.count != 0  {
                    
                    guard let PlaceLat = self.responseVarAddress[currentRow].Lat else { return }
                    
                    let PlaceLatDouble = Double(PlaceLat)
                    
                    guard let PlaceLong = self.responseVarAddress[currentRow].Long else { return }
                    
                    let PlaceLongDouble = Double(PlaceLong)
                    
                    
                    
                    DispatchQueue.main.async {
                        // self.destinationPlace = (self.autoFillArray[indexPath.row] as! GMSPlace)
                        
                        
                        self.destinationLatTrip = PlaceLatDouble!
                        self.destinationLngTrip = PlaceLongDouble!
                        self.destinationStringTrip = self.responseVarAddress[currentRow].placeID!
                        // print(self.destinationPlace?.coordinate ?? "nil")
                        self.destinationTextField.text = self.responseVarAddress[currentRow].FullAddress
                        self.destinationTextField.resignFirstResponder()
                        self.resultBackView1.isHidden = true
                        
                        
                        let latMaker = PlaceLatDouble
                        let lngMaker = PlaceLongDouble
                        print(latMaker!,lngMaker!)
                        self.destination_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                        print(self.responseVarAddress[currentRow].FullAddress)
                        self.destination_marker.icon = UIImage(named: "destination_Marker")
                        self.destination_marker.map = self.mapBackgorundView
                        self.CheckFavTrip()
                        self.destination_marker.title = self.responseVarAddress[currentRow].FullAddress

                        //                        self.markersArray.append(State(name: self.responseVarAddress[indexPath.row].FullAddress!, long: CLLocationDegrees(PlaceLatDouble!), lat: CLLocationDegrees(PlaceLongDouble!), placeId:" ", distanceVale: "", formattedAddress:  (self.responseVarAddress[indexPath.row].FullAddress)!, type: "", imageUrl: ""))
                        
                        
                        let camera = GMSCameraPosition.camera(withLatitude:latMaker!, longitude: lngMaker!, zoom: 14.0)
                        self.mapBackgorundView?.animate(to: camera)
                    }
                }
                else if self.AddressSearch.count != 0 {
                    let Place_ID = AddressSearch[currentRow].place_id
                    //  pPlaceID = "ChIJXbmAjccVrjsRlf31U1ZGpDM"
                    self.placesClient.lookUpPlaceID(Place_ID!, callback: { (place, error) -> Void in
                        
                        if let error = error {
                            print("lookup place id query error: \(error.localizedDescription)")
                            return
                        }
                        
                        if let place = place {
                            print("Place name \(place.name)")
                            print("Place address \(place.formattedAddress!)")
                            print("Place placeID \(place.placeID)")
                            // print("Place attributions \((place.attributions)!)")
                            print("\(place.coordinate.latitude)")
                            print("\(place.coordinate.longitude)")
                            self.destinationPlace = place
                            DispatchQueue.main.async {
                                
                                //self.destinationPlace = (self.autoFillArray[indexPath.row] as! GMSPlace)
                                self.destinationLatTrip = (self.destinationPlace?.coordinate.latitude)!
                                self.destinationLngTrip = (self.destinationPlace?.coordinate.longitude)!
                                self.destinationStringTrip = (self.destinationPlace?.placeID)!
                                print(self.destinationPlace?.coordinate ?? "nil")
                                self.destinationTextField.text = self.destinationPlace?.name
                                self.destinationTextField.resignFirstResponder()
                                self.resultBackView1.isHidden = true
                                
                                
                                let latMaker = self.destinationPlace?.coordinate.latitude
                                let lngMaker = self.destinationPlace?.coordinate.longitude
                                print(latMaker!,lngMaker!)
                                self.destination_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                                print(self.destinationPlace?.name)
                                self.destination_marker.title = self.destinationPlace?.name
                                self.destination_marker.icon = UIImage(named: "destination_Marker")
                                self.destination_marker.map = self.mapBackgorundView
                                self.CheckFavTrip()
                                
                                self.markersArray.append(State(name: self.destinationPlace!.name!, long: CLLocationDegrees((self.destinationPlace?.coordinate.latitude)!), lat: CLLocationDegrees((self.destinationPlace?.coordinate.longitude)!), placeId:" ", distanceVale: "", formattedAddress:  (self.destinationPlace?.name)!, type: "", imageUrl: ""))
                                
                                
                                let camera = GMSCameraPosition.camera(withLatitude:latMaker!, longitude: lngMaker!, zoom: 14.0)
                                self.mapBackgorundView?.animate(to: camera)
                            }
                        }
                        
                    })
                }
            }
            
        }
        }
        else
        {
            if indexPath.row == 0
            {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                nextViewController.isRedirectFromLogin = false
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
               
           else if indexPath.row == 1{
               isMenuClicked = false
               self.OnMenuClicked()


              let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                          let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                                          self.navigationController?.pushViewController(nextViewController, animated: true)
               
           }
            else if indexPath.row == 2
            {
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else if indexPath.row == 3 {
                self.isBackAmenities = false
                self.closebuttonAction(sender: nil)
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
                else if indexPath.row == 4
                {
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            else if indexPath.row == 5
            {
                self.isBackAmenities = false
                self.closebuttonAction(sender: nil)
                self.getCalenderActivities()
            }
               
            else if indexPath.row == 6{
                isMenuClicked = false
                self.OnMenuClicked()
                self.preferencesBtnAction()
            }
            else if indexPath.row == 7{
                isMenuClicked = false
                
                self.OnMenuClicked()
                
                
                 let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                          let supportTripVC =  mainStoryboard.instantiateViewController(withIdentifier: "supportIndexViewController") as! supportIndexViewController
                          self.navigationController!.pushViewController(supportTripVC, animated: true)
                
                
            }


                else if indexPath.row == 8{
                                isMenuClicked = false
                                self.OnMenuClicked()

                
                                if let url = URL(string: "https://www.ridetarc.org/privacy-policy/") {
                                    let vc: SFSafariViewController
                
                                    if #available(iOS 11.0, *) {
                                        let config = SFSafariViewController.Configuration()
                                        config.entersReaderIfAvailable = false
                                        vc = SFSafariViewController(url: url, configuration: config)
                                    } else {
                                        vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
                                    }
                                    vc.delegate = self as? SFSafariViewControllerDelegate
                                    present(vc, animated: true)
                                }
                             
                                
                            }
                else if indexPath.row == 9
                           {
                               isMenuClicked = false
                               
                               self.OnMenuClicked()
                               
                               
                               let storyboard = UIStoryboard(name: "Main", bundle: nil)
                               let vc = storyboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                               // vc.newsObj = newsObj
                               self.present(vc, animated: true, completion: nil)
                               
                               
                           }
            else if indexPath.row == 10{
                isMenuClicked = false
                self.OnMenuClicked()
                
                OnBoardMsg.pageHeadings = ["Depart", "Places", "Preferences", "Add to Favorites"]
                OnBoardMsg.pageImages = ["HelpDepartNow","helpPlaces","helpPreferences","FavDeactive"]
                OnBoardMsg.pageSubHeadings = ["You can edit your departure time here.","Find Restaurants, Banks, City Services, etc.","Edit preferences for your route options.","Your favorite routes are stored and accessible here."]
                OnBoardMsg.Buttoncolor = "#297EC3"
                let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
                
                
                if let walkthroughViewController = storyboard.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController {
                    
                    present(walkthroughViewController, animated: true, completion: nil)
                }
                
                
                
                
            }
           
            else if indexPath.row == 11
            {
                let alertViewController = NYAlertViewController()
                
                // Set a title and message
                alertViewController.title = "Logout"
                alertViewController.message = "Are you sure you would like to logout?"
                
                // Customize appearance as desired
                alertViewController.buttonCornerRadius = 20.0
                alertViewController.view.tintColor = self.view.tintColor
                
                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                alertViewController.swipeDismissalGestureEnabled = true
                alertViewController.backgroundTapDismissalGestureEnabled = true
                alertViewController.buttonColor = UIColor.red
                // Add alert actions
                let activateAction = NYAlertAction(
                    title: "Yes",
                    style: .default,
                    handler: { (action: NYAlertAction!) -> Void in
                        self.showLoader()
                        self.dismiss(animated: true, completion: nil)
                        
                        let userDefaults1 = UserDefaults.standard
                        let accessToken = userDefaults1.value(forKey: "accessToken")
                        let url1 = "\(APIUrl.ZIGTARCAPI)User/Logout"
                        let parameters1: Parameters = [
                            "Token": accessToken!,
                        ]
                        Alamofire.request(url1, method: .post, parameters: parameters1, encoding: URLEncoding.default)
                            .responseObject{ (response: DataResponse<Postpayment>) in
                                switch response.result {
                                case .success:
                                    print(response.result.value?.message ?? "nil")
                                    let userDefaults = UserDefaults.standard
                                    userDefaults.removeObject(forKey: "accessToken")
                                    userDefaults.removeObject(forKey: "userName")
                                    userDefaults.removeObject(forKey: "userPhoto")
                                    userDefaults.removeObject(forKey: "userEmail")
                                    userDefaults.removeObject(forKey: "feedback")
                                    userDefaults.removeObject(forKey: "isLyft")
                                    userDefaults.removeObject(forKey: "isUber")
                                    userDefaults.removeObject(forKey: "isCab")
                                    userDefaults.removeObject(forKey: "IsUserLogin")
                                    userDefaults.removeObject(forKey: "ProfileID")
                                   // userDefaults.removeSuite(named: "ProFileNameUser")
                                    userDefaults.removeObject(forKey: "SavedDictionary")
                                     userDefaults.removeObject(forKey: "phoneNumber")
                                    userDefaults.removeObject(forKey: "EmergencyAlert")
                                     userDefaults.removeObject(forKey: "ShowEmergencyAlert")
                                                              

                                    
                                    
                                    userDefaults.synchronize()
                                    let realm = try! Realm()
                                    try! realm.write {
                                        realm.deleteAll()
                                    }
                                    
                                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WithOutLoginMainViewController") as! WithOutLoginMainViewController
                                    self.navigationController?.pushViewController(nextViewController, animated: true)
                                    self.hideLoader()
                                case .failure(let error):
                                    self.showErrorDialogBox(viewController: self)
                                    self.hideLoader()
                                    print(error.localizedDescription)
                                }
                                
                        }
                }
                )
                let cancelAction = NYAlertAction(
                    title: "No",
                    style: .cancel,
                    handler: { (action: NYAlertAction!) -> Void in
                        self.dismiss(animated: true, completion: nil)
                }
                )
                alertViewController.addAction(activateAction)
                alertViewController.addAction(cancelAction)
                
                // Present the alert view controller
                self.present(alertViewController, animated: true, completion: nil)
                
                
                
                
                
                
            }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //locations.co
        
        location = locations.last
        
        let locationString = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
        // let locationString = "38.2526647, -85.75845570000001"
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json"
        
        let parameters: Parameters = [
            "latlng": locationString,
            "key": Key.GoogleAPIKey
        ]
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<MyLocationAddress>) in
                print(response)
                //to get status code
                switch response.result {
                case .success:
                    
                    self.CheckFavTrip()
                    let CurrentAddressArray = response.result.value?.results![0]
                    print(CurrentAddressArray ?? "nil")
                    
                    self.sourceStringTrip = (CurrentAddressArray?.placeId)!
                    self.sourceLatTrip = Double((CurrentAddressArray?.geometry?.location?.lat)!)
                    self.sourceLngTrip = Double((CurrentAddressArray?.geometry?.location?.lng)!)
                    self.sourceTextField.text = (CurrentAddressArray?.formattedAddress)!
                    // self.source_marker = GMSMarker()
                    let addressComponents = CurrentAddressArray?.addressComponents
                    for addressComponentArray in addressComponents!{
                        if (addressComponentArray.types?.contains("postal_code"))! {
                            var postalode:Int = 0
                            if addressComponentArray.longName != nil {
                                postalode = Int(addressComponentArray.longName!)!
                            }
                            else {
                                postalode = Int(addressComponentArray.shortName!)!
                            }
                            if postalode < 40018 || postalode > 40299 {
                                self.CorrectRegin = false
                            }
                            print(self.CorrectRegin)
                        }
                        else if (addressComponentArray.types?.contains("administrative_area_level_2"))! {
                            var county = ""
                            if addressComponentArray.longName != nil {
                                county = (addressComponentArray.longName!)
                            }
                            else {
                                county = (addressComponentArray.shortName!)
                            }
                            if county != "Jefferson County" {
                                self.CorrectRegin = false
                            }
                            print(self.CorrectRegin)
                        }
                        else if (addressComponentArray.types?.contains("administrative_area_level_1"))! {
                            var State = ""
                            if addressComponentArray.longName != nil {
                                State = (addressComponentArray.longName!)
                                if State != "Kentucky"{
                                    self.CorrectRegin = false
                                }
                            }
                            else {
                                State = (addressComponentArray.shortName!)
                                if State != "KY" {
                                    self.CorrectRegin = false
                                }
                            }
                            
                            print(self.CorrectRegin)
                        }
                    }
                    
                    if !self.CorrectRegin {
                        //                        self.sourcePlaceID = "ChIJEdVbsxoLaYgRMv1xICi009Q"
                        //                        self.sourceLat = comVar.LouisvilleLat
                        //                        self.sourceLng = comVar.LouisvilleLng
                        //                        self.view.makeToast("Address range set to Louisville KY")
                        //                        self.sourceAddressText = "Louisville, KY, USA"
                        //                        self.SourceTextBox.text = "Louisville, KY, USA"
                        //
                        self.sourceStringTrip = "ChIJEdVbsxoLaYgRMv1xICi009Q"
                        self.sourceLatTrip = comVar.LouisvilleLat
                        self.sourceLngTrip = comVar.LouisvilleLng
                        self.sourceTextField.text = "Louisville, KY, USA"
                        self.view.makeToast("Address range set to Louisville KY")
                    }
                    
                    self.source_marker.position = CLLocationCoordinate2D(latitude: self.sourceLatTrip, longitude: self.sourceLngTrip)
                    self.source_marker.title = self.sourcePlace?.name
                    self.source_marker.icon = UIImage(named: "source_Marker")
                    self.source_marker.map = self.mapBackgorundView
                    
                    self.typeString = "bus_station"
                    //self.getPlaces(type: self.typeString)
                    self.getbusStops(type: self.typeString)
                    
                case .failure(let error):
                    print(error)
                    
                    
                    self.showErrorDialogBox(viewController: self)
                    print(error.localizedDescription)
                }
        }
        
        
        
        
        let camera = GMSCameraPosition.camera(withLatitude: 38.2526647, longitude: -85.75845570000001, zoom: 17.0)
        mapBackgorundView.isMyLocationEnabled = true
        mapBackgorundView?.animate(to: camera)
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        
        
        mapBackgorundView.bringSubview(toFront: amenityBtn)
        mapBackgorundView.bringSubview(toFront: listViewBtn)
        mapBackgorundView.bringSubview(toFront: listBackView)
        
        mapBackgorundView.bringSubview(toFront: scheduleLaterBtn)
        mapBackgorundView.bringSubview(toFront: preferenceBtn)
        mapBackgorundView.bringSubview(toFront: self.FavButton)
        mapBackgorundView.bringSubview(toFront: planTripBtn)
        
        
        
        let manager = CLLocationManager()
        manager.startUpdatingLocation()
        
        //        let lat = manager.location?.coordinate.latitude
        //        let long = manager.location?.coordinate.longitude
        
        
        
        let initialLocation = CLLocationCoordinate2D(latitude: 38.07298162949971, longitude: -85.89949781914278)
        let otherLocation = CLLocationCoordinate2D(latitude: 38.32695396305016 , longitude: -85.42179288599452)
        autoFillbounds = GMSCoordinateBounds(coordinate: initialLocation, coordinate: otherLocation)
        
        
        // Set up the autocomplete filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .geocode
        filter.country = "USA"
        // Create the fetcher.
        fetcher = GMSAutocompleteFetcher(bounds: autoFillbounds, filter: filter)
        fetcher?.delegate = self
        
        
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.CheckFavTrip()
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
            resultBackView.isHidden = true
            resultBackView1.isHidden = true
        } else {
            if textField.returnKeyType == .go {
                resultBackView.isHidden = true
                resultBackView1.isHidden = true
                // loginBtnAction()
            }
            textField.resignFirstResponder()
            return true;
        }
        
        
        
        
        
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //  self.CheckFavTrip()
        destinationTextField.resignFirstResponder()
        sourceTextField.resignFirstResponder()
        resultBackView.isHidden = true
        resultBackView1.isHidden = true
    }
    func locationWithBearing(bearing:Double, distanceMeters:Double, origin:CLLocationCoordinate2D) -> CLLocationCoordinate2D {
        let distRadians = distanceMeters / (6372797.6)
        
        let rbearing = bearing * Double.pi / 180.0
        
        let lat1 = origin.latitude * Double.pi / 180
        let lon1 = origin.longitude * Double.pi / 180
        let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(rbearing))
        let lon2 = lon1 + atan2(sin(rbearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))
        return CLLocationCoordinate2D(latitude: lat2 * 180 / Double.pi, longitude: lon2 * 180 / Double.pi)
    }
    //
    //    @IBAction func SourceAddressBox(_ sender: UITextField) {
    //
    //
    //        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    //        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchAddressViewController") as! SearchAddressViewController
    //        self.navigationController?.pushViewController(nextViewController, animated: true)
    //
    //    }
    
    
    func alertOnboardingSkipped(_ currentStep: Int, maxStep: Int) {
        print("Onboarding skipped the \(currentStep) step and the max step he saw was the number \(maxStep)")
    }
    
    func alertOnboardingCompleted() {
        print("Onboarding completed!")
    }
    
    func alertOnboardingNext(_ nextStep: Int) {
        print("Next step triggered! \(nextStep)")
    }
    
    
    
    @IBAction func SwapButtonFunction(_ sender: Any) {
        
        let sourceY = sourceTextField.frame.origin.y as! CGFloat
        let destinationY = destinationTextField.frame.origin.y as! CGFloat
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.sourceTextField.frame = CGRect.init(x: self.sourceTextField.frame.origin.x, y: destinationY, width: self.sourceTextField.frame.width, height: self.sourceTextField.frame.height)
            self.destinationTextField.frame = CGRect.init(x: self.destinationTextField.frame.origin.x, y: sourceY, width: self.destinationTextField.frame.width, height: self.destinationTextField.frame.height)
            print("Arun After Swipe Souce\(destinationY) and destination \(sourceY)")
            
            
            
            
            print("***Swipe")
        }) { finish in
            let source = self.sourceTextField.text as! NSString
            let destination = self.destinationTextField.text as! NSString
            
            self.sourceTextField.text = destination as String
            self.destinationTextField.text = source as String
            
            let latSourceMaker =  self.destination_marker.position.latitude
            let lngSourceMaker = self.destination_marker.position.longitude
            
            let latMaker = self.source_marker.position.latitude
            let lngMaker = self.source_marker.position.longitude
            
            let sLatTrip = self.sourceLatTrip
            let sLngTrip = self.sourceLngTrip
            
            let dLatTrip = self.destinationLatTrip
            let dLngTrip = self.destinationLngTrip
            
            let sourcePlaceID = self.sourceStringTrip
            let destinationPlacecID = self.destinationStringTrip
            
            self.destination_marker.position = CLLocationCoordinate2D(latitude: latMaker, longitude: lngMaker)
            self.source_marker.position = CLLocationCoordinate2D(latitude: latSourceMaker, longitude: lngSourceMaker)
            self.sourceLatTrip = dLatTrip
            self.sourceLngTrip = dLngTrip
            self.destinationLatTrip = sLatTrip
            self.destinationLngTrip = sLngTrip
            self.sourceStringTrip = destinationPlacecID
            self.destinationStringTrip = sourcePlaceID
            self.CheckFavTrip()

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // Change `2.0` to the desired number of seconds.
                // Code you want to be delayed
                
                self.sourceTextField.frame = CGRect.init(x: self.sourceTextField.frame.origin.x, y: sourceY, width: self.sourceTextField.frame.width, height: self.sourceTextField.frame.height)
                self.destinationTextField.frame = CGRect.init(x: self.destinationTextField.frame.origin.x, y: destinationY, width: self.destinationTextField.frame.width, height: self.destinationTextField.frame.height)
                
            }
        }
        
        
        print("***Ended")
    }
    @IBAction func NavNearMyLine(_ sender: Any) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "WhatsNearMeMapViewController") as! WhatsNearMeMapViewController
        // vc.newsObj = newsObj
        self.present(vc, animated: true, completion: nil)
        
        
    }
}




extension TripPlannerViewController: GMSAutocompleteFetcherDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            return 64;
        }
        else
        {
            if tableView == autofillTableView || tableView == autofillTableView1
            {
                return 60;
            }
            else
            {
                return 44;
            }
        }
    }
    
    
    func didFailAutocompleteWithError(_ error: Error) {
        self.showErrorDialogBox(viewController: self)
        print(error.localizedDescription)
    }
    
    func setupBubble () {
        let win = APPDELEGATE.window!
        
        bubble = BubbleControl (size: CGSize(width: 80, height: 80))
        bubble.image = UIImage (named: "male-user-profile-picture.png")
        
        
        bubble.didNavigationBarButtonPressed = {
            print("pressed in nav bar")
            self.bubble.popFromNavBar()
        }
        
       
        
        
        
        let max: CGFloat = win.h - 250
        
        
        let actionController = YoutubeActionController()
        
        
        
        bubble.contentView = actionController
        // bubble.contentView?.present(actionController, animated: true, completion: nil)
        win.addSubview(bubble)
    }
    
    @objc func OnBackClicked() {
        // self.navigationController?.popViewController(animated: true)
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
        nextViewController.isRedirectFromLogin = false
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    func DataBaseAutocompleteFunc(SearchString:String){
        if SearchString != "" {
            self.autofillTableView.isHidden = false
            self.autofillTableView1.isHidden = false
            self.responseVarAddress.removeAll()
            self.AddressSearch.removeAll()
            let SearchStringSpace = SearchString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Map/GetAutoComplete?address=\((SearchStringSpace)!)", method: .get, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<DataBaseAutoComplete>) in
                    //print(response)
                    switch response.result {
                    case .success:
                        
                        
                        if  response.result.value!.Message == "OK" {
                            if (response.result.value?.Count)! > 0 && (response.result.value?.address!) != nil {
                                self.responseVarAddress = response.result.value!.address!
                                
                            }
                            else{
                                self.googleAutoComplete(SearchAddress: SearchString)
                            }
                            
                        }
                        else{
                            self.googleAutoComplete(SearchAddress: SearchString)
                        }
                        
                        
                        
                        if(self.responseVarAddress.count > 5)
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: self.sourceTextField.frame.size.width, height: 250)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: self.destinationTextField.frame.size.width, height: 250)
                            
                        }
                        else
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: Int(self.sourceTextField.frame.size.width), height: self.responseVarAddress.count*50)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: Int(self.destinationTextField.frame.size.width), height: self.responseVarAddress.count*50)
                        }
                        self.autofillTableView.reloadData()
                        self.autofillTableView1.reloadData()
                        
                    case .failure(let error):
                        print(error)
                        self.googleAutoComplete(SearchAddress: SearchString);
                        
                    }
            }
            
        }
        else{
            self.autofillTableView.removeFromSuperview()
            self.autofillTableView1.removeFromSuperview()
            self.autofillTableView.isHidden = true
            self.autofillTableView1.isHidden = true
        }
    }
    
    func googleAutoComplete(SearchAddress:String){
        let SearchAddressSpace = SearchAddress.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        responseVarAddress.removeAll()
        AddressSearch.removeAll()
        print("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(Key.GoogleAPIKey)&components=country:USA&input=\((SearchAddressSpace)!)")
        Alamofire.request("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(Key.GoogleAPIKey)&components=country:USA&input=\((SearchAddressSpace)!)", method: .get, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<GoogleAutoComplete>) in
                //print(response)
                switch response.result {
                    
                case .success:
                    
                    if response.result.value?.Status == "OK"{
                        
                        self.AddressSearch = response.result.value!.predictions!
                        if(self.AddressSearch.count > 5)
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: self.sourceTextField.frame.size.width, height: 250)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: self.destinationTextField.frame.size.width, height: 250)
                            
                        }
                        else
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: Int(self.sourceTextField.frame.size.width), height: self.AddressSearch.count*50)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: Int(self.destinationTextField.frame.size.width), height: self.AddressSearch.count*50)
                        }
                        self.autofillTableView.reloadData()
                        self.autofillTableView1.reloadData()
                    }
                    
                case .failure(let error):
                    print(error)
                    
                    
                }
        }
        
    }
    
    
    
    
    
}



extension UIScreen {
    
    enum SizeType: CGFloat {
        case Unknown = 0.0
        case iPhone4 = 960.0
        case iPhone5 = 1136.0
        case iPhone6 = 1334.0
        case iPhone6Plus = 2208.0
        case iPhoneX = 2436.0
        case iPadAir2andiPadMini4 = 2048.0
        case iPadPro105 = 2224.0
        case iPadPro129 = 2732.0
        
    }
    
    var sizeType: SizeType {
        let height = nativeBounds.height
        guard let sizeType = SizeType(rawValue: height) else { return .Unknown }
        return sizeType
    }
    
    
}
extension String{
    func decodeEnt() -> String{
        let encodedData = self.data(using: String.Encoding.utf8)!
        let attributedOptions: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.documentType.rawValue): NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.characterEncoding.rawValue): String.Encoding.utf8.rawValue
        ]
        var attributedString = NSAttributedString()
        do
        {
            attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
        }catch {
            print("Error: \(error)")
        }
        return attributedString.string
    }
}
extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter
    }()
}
extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    
    
    
}

extension String {
    var words: [String] {
        var words: [String] = []
        enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) { word,_,_,_ in
            guard let word = word else { return }
            words.append(word)
        }
        return words
    }
}


