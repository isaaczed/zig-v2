//
//  paymentViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 03/10/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import SwiftyJSON
import XLActionController
import SCLAlertView
import Toast_Swift

let kEnvironment = "CERT"

// Refer to dev portal -> 'My APIs' page -> app -> API Key
// Sandbox==>
//let KApiKey = "HOtd8S5xuunjKAdF2pRHy5UDPlaLRaTE"
//Live
let KApiKey = "mozAcPE7xyR2KNzr7rBzhE2XlkHVw3R0"

// Refer to dev portal -> 'My APIs' page -> app -> Api Secret
//Sandbox
//let KApiSecret = "28f15d86b31897f7469242a06912ab372140c89753c9f46d6dc0cf1f04d5af97"
//live
let KApiSecret = "d23c805c4f410cd4192da20ed736f48196e3bf76ab93865da4d656a5f1cfffcc"

// Refer to dev portal -> 'My Merchants' page -> SANDBOX -> Token
//SANDBOX ==> fdoa-0b238c40ef08f1856223eca494f27a2ffd5dd33b02ce64e6
// Live ==> fdoa-9b8fc857ad735e67cb62589d8c0b83bc7ad6d3a8e3fd1a91
var KToken = "fdoa-0b238c40ef08f1856223eca494f27a2ffd5dd33b02ce64e6"

// use following url to get token (POST) and proceed for transactions
//SandBox
var KURL = "https://api-cert.payeezy.com/v1/transactions/tokens"
//Live
//let KURL = "https://api.payeezy.com/v1/transactions/tokens"

//https://api.payeezy.com
//https://api-cert.payeezy.com
// use following url to get token (GET) and proceed for transactions
var SURL = "https://api-cert.payeezy.com/v1/securitytokens?"

// use following url for transactions
var PURL = "https://api-cert.payeezy.com/v1/transactions"

class paymentViewController: UIViewController, UIAlertViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var customerName: UITextField!
    @IBOutlet var customerCardno: UITextField!
    @IBOutlet var expireDate: UITextField!
    @IBOutlet var cvvnumber: UITextField!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var secondBigTitle: UILabel!


    @IBOutlet var customerNameLabel: UILabel!
    @IBOutlet var customerCardnoLabel: UILabel!
    @IBOutlet var expireDateLabel: UILabel!
    @IBOutlet var cvvnumberLabel: UILabel!
    @IBOutlet var JCBView: UIImageView!
    @IBOutlet var DineClubView: UIImageView!
    @IBOutlet var visaImageView: UIImageView!
    @IBOutlet var masterImageView: UIImageView!
    @IBOutlet var americanExpressView: UIImageView!
    @IBOutlet var discoverView: UIImageView!
    @IBOutlet var savePaymentButton: UIButton!
    @IBOutlet var paymentBackView: UIView!
    var loaderView = UIView()
    var paymentApiBool = Bool()
    var cardType: String?
    var savePaymentBool = Bool()
    var fareID: Int?
    var backgroundBlurView = UIView()
    @IBOutlet var makePaymentButton: UIButton!
  //  @IBOutlet var undo: UIButton!
    var fdTokenValue: String?
    var ticketType: String?
    var noofTickets: String?
    var ticketPrice: String?
    var AlreadyPaymentAdded = Bool()
    var paymentClickedBool = Bool()
    var totalPrice: String?

    var alreadyCardString: String?
    var icredit_card: [AnyHashable: Any]?
    var menuButton = UIBarButtonItem()
    var purchaseView = UIView()
    var bottomView = UIView()
    let addTickeValue: NSMutableArray = []
    var firstTextfield = UITextField()
    var secondTextfield = UITextField()
    var thirdTextfield = UITextField()
    var okayButton = UIButton()
    var LivepaymentBool = Bool()
    var ta_tokenGlobal = NSString()
    var waitingLoader = UIImageView()

    @IBOutlet var stateAddress: UITextField!
    @IBOutlet var stateAddressLabel: UILabel!
    @IBOutlet var zipCode: UITextField!
    @IBOutlet var zipcodeLabel: UILabel!
    @IBOutlet var city: UITextField!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var state: UITextField!
    @IBOutlet var cityLabel: UILabel!
    var paymentButton = SwipeButtonView()

    var timerEmail: Timer?
    var myPickerView: UIPickerView!
    var pickerData = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC","DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
//    override func viewDidDisappear(_ animated: Bool) {
//        self.hideLoader()
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
       // print(ticketPrice)
        let userDefaults1 = UserDefaults.standard
        LivepaymentBool = userDefaults1.bool(forKey: "Livepayment")
       // print(LivepaymentBool)
        ta_tokenGlobal = "NOIW"
        if LivepaymentBool == true
            {
            KToken = "fdoa-9b8fc857ad735e67cb62589d8c0b83bc7ad6d3a8e3fd1a91"
            KURL = "https://api.payeezy.com/v1/transactions/tokens"
            SURL = "https://api.payeezy.com/v1/securitytokens?"
            PURL = "https://api.payeezy.com/v1/transactions"
            ta_tokenGlobal = "EHHZ"
        }

        paymentClickedBool = false
        makePaymentButton.addTarget(self, action: #selector(makePayment), for: .touchUpInside)
        self.title = ""
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationItem.title = " "
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
            style: UIBarButtonItemStyle.done,
            target: self, action: #selector(OnBackClicked))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.setHidesBackButton(true, animated: true);
        //self.view.layoutIfNeeded()

        menuButton = UIBarButtonItem(title: "View Tickets",
            style: UIBarButtonItemStyle.plain,
            target: self,
            action: #selector(OnBackClicked))


        self.navigationItem.rightBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem?.tintColor = color.hexStringToUIColor(hex: "#EFAC40")

        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        customerCardno.tag = 101
        expireDate.tag = 102
        stateAddress.tag = 104
        city.tag = 105
        state.tag = 106
        zipCode.tag = 107
        customerName?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        cvvnumber?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        customerCardno?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        expireDate?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        stateAddress?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        city?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        state?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        zipCode?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
            for: .editingChanged)

        savePaymentBool = true
        savePaymentButton.addTarget(self, action: #selector(savePaymentAction), for: .touchUpInside)
        customerName.delegate = self
        expireDate.delegate = self
        cvvnumber.delegate = self
        customerCardno.delegate = self
        stateAddress.delegate = self
        zipCode.delegate = self
        state.delegate = self
        city.delegate = self
        customerName.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        customerName.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        customerName.layer.shadowOpacity = 1.0
        customerName.layer.shadowRadius = 0.0
        customerName.layer.masksToBounds = false
        customerName.layer.cornerRadius = 4.0

        customerCardno.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        customerCardno.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        customerCardno.layer.shadowOpacity = 1.0
        customerCardno.layer.shadowRadius = 0.0
        customerCardno.layer.masksToBounds = false
        customerCardno.layer.cornerRadius = 4.0

        expireDate.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        expireDate.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        expireDate.layer.shadowOpacity = 1.0
        expireDate.layer.shadowRadius = 0.0
        expireDate.layer.masksToBounds = false
        expireDate.layer.cornerRadius = 4.0

        cvvnumber.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        cvvnumber.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        cvvnumber.layer.shadowOpacity = 1.0
        cvvnumber.layer.shadowRadius = 0.0
        cvvnumber.layer.masksToBounds = false
        cvvnumber.layer.cornerRadius = 4.0

        stateAddress.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        stateAddress.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        stateAddress.layer.shadowOpacity = 1.0
        stateAddress.layer.shadowRadius = 0.0
        stateAddress.layer.masksToBounds = false
        stateAddress.layer.cornerRadius = 4.0

        zipCode.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        zipCode.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        zipCode.layer.shadowOpacity = 1.0
        zipCode.layer.shadowRadius = 0.0
        zipCode.layer.masksToBounds = false
        zipCode.layer.cornerRadius = 4.0

        state.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        state.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        state.layer.shadowOpacity = 1.0
        state.layer.shadowRadius = 0.0
        state.layer.masksToBounds = false
        state.layer.cornerRadius = 4.0

        city.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        city.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        city.layer.shadowOpacity = 1.0
        city.layer.shadowRadius = 0.0
        city.layer.masksToBounds = false
        city.layer.cornerRadius = 4.0


        makePaymentButton.layer.cornerRadius = 5
        makePaymentButton.layer.masksToBounds = true
        makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
        makePaymentButton.isUserInteractionEnabled = false
        closeButton.addTarget(self, action: #selector(OnBackClicked), for: .touchUpInside)

        if alreadyCardString == "true"
            {
            self.view.backgroundColor = UIColor.init(red: 237 / 255.0, green: 237 / 255.0, blue: 237 / 255.0, alpha: 1.0)
            paymentBackView.backgroundColor = UIColor.init(red: 237 / 255.0, green: 237 / 255.0, blue: 237 / 255.0, alpha: 1.0)

            secondBigTitle.text = "Your Purchase"
            makePaymentButton.isHidden = true

            visaImageView.isHidden = true
            masterImageView.isHidden = true
            americanExpressView.isHidden = true
            discoverView.isHidden = true
            DineClubView.isHidden = true
            JCBView.isHidden = true

            customerCardnoLabel.isHidden = true
            customerNameLabel.isHidden = true
            expireDateLabel.isHidden = true
            cvvnumberLabel.isHidden = true


            customerName.isHidden = true
            customerCardno.isHidden = true
            expireDate.isHidden = true
            cvvnumber.isHidden = true

            stateAddress.isHidden = true
            stateAddressLabel.isHidden = true
            zipCode.isHidden = true
            zipcodeLabel.isHidden = true
            state.isHidden = true
            stateLabel.isHidden = true
            city.isHidden = true
            cityLabel.isHidden = true

            savePaymentButton.isHidden = true

            alreadyPaymentDesign()
        }
        else
        {
            self.view.backgroundColor = UIColor.white
            paymentBackView.backgroundColor = UIColor.white

        }
//        customerNameLabel.isHidden = true
//        customerCardno.isHidden = true
//        expireDate.isHidden = true
//        cvvnumber.isHidden = true
        // Do any additional setup after loading the view.
    }
    func pickUp(_ textField: UITextField) {

        // UIPickerView
        self.myPickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView

        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()

        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "next", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar

    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        stateLabel.text = "State"

        state.text = pickerData[row]
    }
    //MARK:- TextFiled Delegate


    @objc func doneClick() {
        state.resignFirstResponder()
    }
    @objc func cancelClick() {
        state.resignFirstResponder()
        zipCode.becomeFirstResponder()
    }
    @objc func savePaymentAction()
    {
        if savePaymentBool == true
            {
            savePaymentButton.setImage(UIImage.init(named: "checkbox_out"), for: .normal)
            savePaymentBool = false
        }
        else
        {
            savePaymentButton.setImage(UIImage.init(named: "checkbox_in"), for: .normal)

            savePaymentBool = true
        }
    }
    func purchaseViewDesign()
    {
        purchaseView = UIView()
        purchaseView.frame = CGRect.init(x: 10, y: secondBigTitle.frame.maxY + 70, width: self.view.frame.size.width - 20, height: 100)
        purchaseView.backgroundColor = UIColor.clear
        self.view .addSubview(purchaseView)

        let itemLabel = UILabel()
        itemLabel.frame = CGRect.init(x: 5, y: 5, width: purchaseView.frame.size.width / 2, height: 15)
        itemLabel.text = "   Item"
        itemLabel.font = UIFont.setTarcBold(size: 12)
        itemLabel.textColor = UIColor.init(red: 142 / 255, green: 139 / 255, blue: 142 / 255, alpha: 1.0)

        purchaseView .addSubview(itemLabel)
        let widthnew = purchaseView.frame.size.width / 2
        let qtyLabel = UILabel()
        qtyLabel.frame = CGRect.init(x: itemLabel.frame.maxX + 0, y: 5, width: widthnew / 2, height: 15)
        qtyLabel.text = "Qty"
        qtyLabel.textColor = UIColor.init(red: 142 / 255, green: 139 / 255, blue: 142 / 255, alpha: 1.0)

        qtyLabel.font = UIFont.setTarcBold(size: 12)
        purchaseView .addSubview(qtyLabel)

        let fareLabel = UILabel()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            // It's an iPhone
            fareLabel.frame = CGRect.init(x: qtyLabel.frame.maxX + 25, y: 5, width: widthnew / 2, height: 15)
            break
        case .pad:
            fareLabel.frame = CGRect.init(x: qtyLabel.frame.maxX + 130, y: 5, width: widthnew / 2, height: 15)

            break
            // It's an iPad
        case .unspecified: break
            // Uh, oh! What could it be?
        case .tv: break

        case .carPlay: break

        }
        fareLabel.text = "Fare"
        fareLabel.textColor = UIColor.init(red: 142 / 255, green: 139 / 255, blue: 142 / 255, alpha: 1.0)

        fareLabel.font = UIFont.setTarcBold(size: 12)
        purchaseView .addSubview(fareLabel)

        let rateView = UIView ()
        rateView.frame = CGRect.init(x: 5, y: fareLabel.frame.maxY + 10, width: purchaseView.frame.size.width - 10, height: 50)
        rateView.backgroundColor = UIColor.white
        rateView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        rateView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        rateView.layer.shadowOpacity = 0.5
        rateView.layer.shadowRadius = 0.5
        rateView.layer.masksToBounds = false
        rateView.layer.cornerRadius = 4.0
        purchaseView .addSubview(rateView)

        let itemValueLabel = UILabel()
        itemValueLabel.frame = CGRect.init(x: 5, y: 15, width: purchaseView.frame.size.width / 2, height: 20)
        itemValueLabel.text = ticketType
        itemValueLabel.textColor = UIColor.init(red: 0 / 255, green: 123 / 255, blue: 223 / 255, alpha: 1.0)
        itemValueLabel.font = UIFont.setTarcBold(size: 15)
        rateView .addSubview(itemValueLabel)
        let qtyValueLabel = UILabel()
        qtyValueLabel.frame = CGRect.init(x: itemLabel.frame.maxX + 0, y: 15, width: widthnew / 2, height: 20)
        qtyValueLabel.text = noofTickets
        qtyValueLabel.font = UIFont.setTarcRegular(size: 15)
        rateView .addSubview(qtyValueLabel)
        var newPriceString = String()
        newPriceString = (removeString(text: ticketPrice!) as NSString) as String

        let count: Double? = Double(noofTickets!) // firstText is UITextField
        let priceValue: Double? = Double(newPriceString)!
        let totalValue = priceValue! * count!
        print(totalValue)
        let fareValueLabel = UILabel()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            // It's an iPhone
            if UIScreen.main.sizeType == .iPhone5 {
                fareValueLabel.frame = CGRect.init(x: qtyLabel.frame.maxX + 10, y: 15, width: widthnew / 2, height: 20)

            }
            else
            {
                fareValueLabel.frame = CGRect.init(x: qtyLabel.frame.maxX + 20, y: 15, width: widthnew / 2, height: 20)
            }
            break
        case .pad:
            fareValueLabel.frame = CGRect.init(x: qtyLabel.frame.maxX + 120, y: 15, width: widthnew / 2, height: 20)

            break
            // It's an iPad
        case .unspecified: break
            // Uh, oh! What could it be?
        case .tv: break

        case .carPlay: break

        }
        let aStr = String(format: "$%.2f", totalValue)
        totalPrice = String(format: "$%.2f", totalValue)
        fareValueLabel.text = aStr
        fareValueLabel.font = UIFont.setTarcRegular(size: 15)
        rateView .addSubview(fareValueLabel)

    }
    func alreadyPaymentDesign()
    {
        purchaseViewDesign()
        let height = self.view.frame.size.height / 1.9
        bottomView = UIView()


        let defaults = UserDefaults.standard
//        let savedDictionary = defaults.object(forKey: "SavedDictionary") as? [String: Any] ?? [String: Any]()
//        print(savedDictionary)
        let savedDictionarydata = defaults.object(forKey: "SavedDictionary") as? Data
        var savedDictionary = NSDictionary()
        if savedDictionarydata != nil
            {
            savedDictionary = NSKeyedUnarchiver.unarchiveObject(with: savedDictionarydata!) as! [String: Any] as NSDictionary
            if savedDictionary.count != 0
                {
                self.AlreadyPaymentAdded = true
            }
            }
            else
            {
                self.AlreadyPaymentAdded = false
            }

            if AlreadyPaymentAdded == true
                {
                bottomView.frame = CGRect.init(x: 0, y: self.view.frame.maxY - height, width: self.view.frame.size.width, height: self.view.frame.size.height / 1.9)

            }
            else
            {
                bottomView.frame = CGRect.init(x: 0, y: self.view.frame.maxY - 200, width: self.view.frame.size.width, height: 200)

            }
            bottomView.backgroundColor = UIColor.white
            self.view .addSubview(bottomView)

            let totalLabel = UILabel()
            totalLabel.frame = CGRect.init(x: 15, y: 10, width: 150, height: 30)
            totalLabel.text = "Total"
            totalLabel.textAlignment = .left
            totalLabel.font = UIFont.setTarcHeavy(size: 20)
            bottomView .addSubview(totalLabel)
            let count: Double? = Double(noofTickets!) // firstText is UITextField
            var newPriceString = String()
            newPriceString = (removeString(text: ticketPrice!) as NSString) as String
            let priceValue: Double? = Double(newPriceString)!
            let totalValue = priceValue! * count!
            print(totalValue)
            let priceLabel = UILabel()
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                // It's an iPhone
                if UIScreen.main.sizeType == .iPhone5 {
                    priceLabel.frame = CGRect.init(x: bottomView.frame.maxX - 170, y: 10, width: 140, height: 30)

                }
                else
                {
                    priceLabel.frame = CGRect.init(x: bottomView.frame.maxX - 160, y: 10, width: 140, height: 30)
                }
                break
            case .pad:
                priceLabel.frame = CGRect.init(x: bottomView.frame.maxX - 180, y: 10, width: 140, height: 30)

                break
                // It's an iPad
            case .unspecified: break
                // Uh, oh! What could it be?
            case .tv: break

            case .carPlay: break

            }
            priceLabel.textAlignment = .right
            let aStr = String(format: "$%.2f", totalValue)

            priceLabel.text = aStr
            priceLabel.font = UIFont.setTarcHeavy(size: 20)
            bottomView .addSubview(priceLabel)

            let firstLine = UILabel()
            firstLine.frame = CGRect.init(x: 40, y: priceLabel.frame.maxY + 10, width: bottomView.frame.size.width - 80, height: 1)
            firstLine.backgroundColor = UIColor.lightGray
            bottomView .addSubview(firstLine)
            if AlreadyPaymentAdded == true
                {
                let paymentView = UIView()
                paymentView.frame = CGRect.init(x: 15, y: firstLine.frame.maxY + 10, width: self.view.frame.size.width - 50, height: 125)
                paymentView.backgroundColor = UIColor.white
                bottomView .addSubview(paymentView)
//        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(myTapAction))
//        mytapGestureRecognizer.numberOfTapsRequired = 1
//        paymentView.addGestureRecognizer(mytapGestureRecognizer)


                let paymentMethodLabel = UILabel()
                paymentMethodLabel.frame = CGRect.init(x: 0, y: 5, width: paymentView.frame.size.width, height: 20)
                paymentMethodLabel.textColor = UIColor.init(red: 142 / 255, green: 139 / 255, blue: 142 / 255, alpha: 1.0)
                paymentMethodLabel.text = "Payment method"
                paymentMethodLabel.font = UIFont.setTarcRegular(size: 17)
                paymentView .addSubview(paymentMethodLabel)

                let paymentArrow = UIButton()
                paymentArrow.frame = CGRect.init(x: paymentView.frame.maxX - 60, y: 5, width: 40, height: 40)
                paymentArrow.setImage(UIImage.init(named: "Right-Arrow"), for: .normal)
                paymentView .addSubview(paymentArrow)
                paymentMethodLabel.bringSubview(toFront: paymentArrow)
                paymentArrow.isHidden = true
                let cardView = UIView()
                cardView.frame = CGRect.init(x: 0, y: paymentMethodLabel.frame.maxY + 10, width: self.view.frame.size.width - 40, height: 60)
                cardView.backgroundColor = UIColor.white



                let shadowSize: CGFloat = 5.0
                let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                    y: -shadowSize / 2,
                    width: cardView.frame.size.width + shadowSize,
                    height: cardView.frame.size.height + shadowSize))
                cardView.layer.masksToBounds = false
                cardView.layer.shadowColor = UIColor.black.cgColor
                cardView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
                cardView.layer.shadowOpacity = 0.2
                cardView.layer.shadowPath = shadowPath.cgPath



//            cardView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
//                   cardView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//                   cardView.layer.shadowOpacity = 0.5
//                   cardView.layer.shadowRadius = 0.5
//                   cardView.layer.masksToBounds = false
//                   cardView.layer.cornerRadius = 4.0
                paymentView .addSubview(cardView)
                let CustomerUniqueNo = savedDictionary["CustomerUniqueNo"] as! NSString
                let cardType = savedDictionary["type"] as! NSString
              
                let cardImage = UIImageView()
                cardImage.frame = CGRect.init(x: 5, y: 20, width: 30, height: 30)
                if cardType == "visa"
                    {
                    cardImage.image = UIImage.init(named: "visa_icon")
                }
                else if cardType == "Mastercard"
                    {
                    cardImage.image = UIImage.init(named: "mastercard_icon")

                }
                else if cardType == "American Express"
                    {
                    cardImage.image = UIImage.init(named: "americanexpress_icon")

                }
                else if cardType == "discover"
                    {
                    cardImage.image = UIImage.init(named: "Discover")
                }
                else if cardType == "JCB"
                    {
                    cardImage.image = UIImage.init(named: "JCB")
                }
                else if cardType == "Diners Club"
                    {
                    cardImage.image = UIImage.init(named: "Diners Club")
                }
                cardImage.contentMode = .scaleAspectFit
                cardView .addSubview(cardImage)


                let trimmedString: String = (CustomerUniqueNo as NSString).substring(from: max(CustomerUniqueNo.length - 4, 0))


                let cardNo = UILabel()
                cardNo.frame = CGRect.init(x: cardImage.frame.maxX + 5, y: 0, width: cardView.frame.size.width - 80, height: 60)
                if cardType == "American Express"
                    {
                    cardNo.text = "AMX **** **** ****\(trimmedString)"
                }
                else
                {
                    cardNo.text = "\(cardType) **** **** ****\(trimmedString)"

                }
                cardNo.font = UIFont.setTarcRegular(size: 20)
                cardView .addSubview(cardNo)

                    let clearButton = UIButton()
                    clearButton.frame = CGRect.init(x: cardNo.frame.maxX + 0, y: 10, width: 35, height: 35)
                    clearButton.setImage(UIImage.init(named: "paymentDelete"), for: .normal)
                    clearButton.addTarget(self, action: #selector(clearActionPayment), for: .touchUpInside)
                    cardView .addSubview(clearButton)
                    cardNo.bringSubview(toFront: clearButton)
                let alertLabel = UILabel()
                alertLabel.frame = CGRect.init(x: 5, y: cardView.frame.maxY + 5, width: paymentView.frame.size.width - 10, height: 15)
                alertLabel.text = "This card will be charged for your purchase"
                alertLabel.font = UIFont.setTarcRegular(size: 11)
                paymentView .addSubview(alertLabel)

                let addCardButton = UIButton()
                addCardButton.frame = CGRect.init(x: 20, y: paymentView.frame.maxY + 10, width: bottomView.frame.size.width - 40, height: 40)
                addCardButton .setTitle(" Change payment method", for: .normal)
                addCardButton.setImage(UIImage.init(named: "chooser-button-input"), for: .normal)
                addCardButton.backgroundColor = UIColor.clear
                addCardButton.layer.cornerRadius = 5
                addCardButton.layer.masksToBounds = true
                addCardButton.titleLabel?.font = UIFont.setTarcRegular(size: 17)

                addCardButton.setTitleColor(UIColor.black, for: .normal)
                bottomView .addSubview(addCardButton)
                addCardButton.addTarget(self, action: #selector(addPaymentAction), for: .touchUpInside)


                paymentButton = SwipeButtonView()
                switch UIDevice.current.userInterfaceIdiom {
                case .phone:
                    // It's an iPhone
                    if UIScreen.main.sizeType == .iPhone5 {
                        paymentButton.frame = CGRect.init(x: 20, y: addCardButton.frame.maxY + 20, width: bottomView.frame.size.width - 40, height: 40)

                    }
                    else
                    {
                        paymentButton.frame = CGRect.init(x: 20, y: addCardButton.frame.maxY + 20, width: bottomView.frame.size.width - 40, height: 50)

                    }
                    if UIScreen.main.sizeType == .iPhone5 {
                        cardNo.font = UIFont.setTarcRegular(size: 15)

                    }
                    else
                    {
                        cardNo.font = UIFont.setTarcRegular(size: 20)

                    }

                    break
                case .pad:

                    var ypos = self.view.frame.size.height / 1.9
                    ypos = ypos - 70
                    paymentButton.frame = CGRect.init(x: 20, y: ypos, width: bottomView.frame.size.width - 40, height: 50)
                    cardNo.font = UIFont.setTarcRegular(size: 25)

                    break
                    // It's an iPad
                case .unspecified: break
                    // Uh, oh! What could it be?
                case .tv: break

                case .carPlay: break

                }
                //   paymentButton .setTitle("Proceed to Payment", for: .normal)
                paymentButton.backgroundColor = UIColor.red
                paymentButton.layer.cornerRadius = 5
                paymentButton.layer.masksToBounds = true
                bottomView .addSubview(paymentButton)
                // paymentButton.addTarget(self, action: #selector(paymentAction), for: .touchUpInside)
                paymentButton.hint = "Slide To Pay"
                paymentButton.image = UIImage.init(named: "Right arrow")

                paymentButton.handleAction { (isFinished) in
                    if isFinished {
                        self.paymentAction()
                        self.paymentButton.updateHint(text: "Process")



                    } else {
                        self.paymentButton.updateHint(text: "Slide To Pay")
                    }
                }
            }
            else
            {

                let addCardButton = UIButton()
                addCardButton.frame = CGRect.init(x: 20, y: firstLine.frame.maxY + 5, width: bottomView.frame.size.width - 40, height: 40)
                addCardButton .setTitle(" Add New Card", for: .normal)
                addCardButton.titleLabel?.font = UIFont.setTarcRegular(size: 17)
                addCardButton.setImage(UIImage.init(named: "chooser-button-input"), for: .normal)
                addCardButton.backgroundColor = UIColor.clear
                addCardButton.layer.cornerRadius = 5
                addCardButton.layer.masksToBounds = true
                addCardButton.setTitleColor(UIColor.black, for: .normal)
                bottomView .addSubview(addCardButton)
                addCardButton.addTarget(self, action: #selector(addPaymentAction), for: .touchUpInside)

                let paymentButton = UIButton()

                paymentButton.frame = CGRect.init(x: 20, y: addCardButton.frame.maxY + 20, width: bottomView.frame.size.width - 40, height: 40)


                paymentButton .setTitle("Proceed to Payment", for: .normal)
                paymentButton.titleLabel?.font = UIFont.setTarcRegular(size: 17)

                paymentButton.backgroundColor = UIColor.red
                paymentButton.layer.cornerRadius = 5
                paymentButton.layer.masksToBounds = true
                paymentButton.addTarget(self, action: #selector(paymentAction), for: .touchUpInside)
                bottomView .addSubview(paymentButton)
            }

        }
        //recognizer: UITapGestureRecognizer
        @objc func myTapAction() {
            print("my tap")
            backgroundBlurView = UIView()
            backgroundBlurView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            backgroundBlurView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            self.view .addSubview(backgroundBlurView)
            paymentBackView.bringSubview(toFront: backgroundBlurView)

            let cvvScreen = UIView ()
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                // It's an iPhone
                cvvScreen.frame = CGRect.init(x: 30, y: self.view.frame.height / 2 - 100, width: self.view.frame.size.width - 60, height: 200)

                break
            case .pad:
                var xpos = self.view.frame.size.width / 2
                xpos = xpos / 2
                cvvScreen.frame = CGRect.init(x: xpos, y: self.view.frame.height / 2 - 100, width: self.view.frame.size.width / 2, height: 200)


                break
                // It's an iPad
            case .unspecified: break
                // Uh, oh! What could it be?
            case .tv: break

            case .carPlay: break

            }
            cvvScreen.backgroundColor = UIColor.white
            cvvScreen.layer.cornerRadius = 5
            cvvScreen.layer.masksToBounds = true
            backgroundBlurView .addSubview(cvvScreen)

            let enterCVVLabel = UILabel()
            enterCVVLabel.frame = CGRect.init(x: 10, y: 5, width: cvvScreen.frame.size.width, height: 20)
            enterCVVLabel.text = "Enter CVV"
            enterCVVLabel.font = UIFont.setTarcBold(size: 14)
            enterCVVLabel.textAlignment = .left
            cvvScreen .addSubview(enterCVVLabel)

            let closeBtn = UIButton()
            closeBtn.frame = CGRect.init(x: enterCVVLabel.frame.maxX - 60, y: 5, width: 30, height: 30)
            closeBtn.setImage(UIImage.init(named: "CloseButton"), for: .normal)
            closeBtn.addTarget(self, action: #selector(closeCvvAction), for: .touchUpInside)
            cvvScreen .addSubview(closeBtn)
            closeBtn.bringSubview(toFront: enterCVVLabel)

            let defaults = UserDefaults.standard
            let savedDictionarydata = defaults.object(forKey: "SavedDictionary") as? Data
            let savedDictionary: Dictionary? = NSKeyedUnarchiver.unarchiveObject(with: savedDictionarydata!) as! [String: Any]
//        let savedDictionary = defaults.object(forKey: "SavedDictionary") as? [String: Any] ?? [String: Any]()
            print(savedDictionary)

            if savedDictionary!.count != 0
                {
                let cardView = UIView()
                cardView.frame = CGRect.init(x: 0, y: enterCVVLabel.frame.maxY + 10, width: cvvScreen.frame.size.width, height: 50)
                cardView.backgroundColor = UIColor.clear
                cvvScreen .addSubview(cardView)
                let CustomerUniqueNo = savedDictionary!["CustomerUniqueNo"] as! NSString
                let cardType = savedDictionary!["type"] as! NSString
                let cardImage = UIImageView()
                cardImage.frame = CGRect.init(x: 10, y: 15, width: 30, height: 30)
                if cardType == "visa"
                    {
                    cardImage.image = UIImage.init(named: "visa_icon")
                }
                else if cardType == "Mastercard"
                    {
                    cardImage.image = UIImage.init(named: "mastercard_icon")

                }
                else if cardType == "American Express"
                    {
                    cardImage.image = UIImage.init(named: "americanexpress_icon")

                }
                cardView .addSubview(cardImage)


                let trimmedString: String = (CustomerUniqueNo as NSString).substring(from: max(CustomerUniqueNo.length - 4, 0))


                let cardNo = UILabel()
                cardNo.frame = CGRect.init(x: cardImage.frame.maxX + 5, y: 0, width: cardView.frame.size.width - 40, height: 50)
                cardNo.text = "\(cardType) **** **** ****\(trimmedString)"
                cardNo.font = UIFont.setTarcRegular(size: 15)
                cardView .addSubview(cardNo)


                firstTextfield = UITextField()
                firstTextfield.frame = CGRect.init(x: cvvScreen.frame.size.width / 2 - 80, y: cardView.frame.maxY + 5, width: 50, height: 50)
                firstTextfield.backgroundColor = UIColor.clear
                firstTextfield.textAlignment = .center
                firstTextfield.tag = 300
                firstTextfield.keyboardType = .numberPad
                firstTextfield.isSecureTextEntry = true
                firstTextfield.delegate = self
                cvvScreen.addSubview(firstTextfield)

                secondTextfield = UITextField()
                secondTextfield.frame = CGRect.init(x: firstTextfield.frame.maxX + 10, y: cardView.frame.maxY + 5, width: 50, height: 50)
                secondTextfield.backgroundColor = UIColor.clear
                secondTextfield.delegate = self
                secondTextfield.textAlignment = .center
                secondTextfield.isSecureTextEntry = true
                secondTextfield.tag = 301
                secondTextfield.keyboardType = .numberPad
                cvvScreen.addSubview(secondTextfield)

                thirdTextfield = UITextField()
                thirdTextfield.frame = CGRect.init(x: secondTextfield.frame.maxX + 10, y: cardView.frame.maxY + 5, width: 50, height: 50)
                thirdTextfield.backgroundColor = UIColor.clear
                thirdTextfield.tag = 302
                thirdTextfield.textAlignment = .center
                thirdTextfield.delegate = self
                thirdTextfield.isSecureTextEntry = true
                thirdTextfield.keyboardType = .numberPad
                cvvScreen.addSubview(thirdTextfield)


                firstTextfield.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                    for: .editingChanged)

                secondTextfield.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                    for: .editingChanged)

                thirdTextfield.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                    for: .editingChanged)



                let firstLine = UILabel()
                firstLine.frame = CGRect.init(x: cvvScreen.frame.size.width / 2 - 80, y: firstTextfield.frame.maxY + 0, width: 50, height: 1)
                firstLine.backgroundColor = UIColor.lightGray
                cvvScreen.addSubview(firstLine)

                let secondLine = UILabel()
                secondLine.frame = CGRect.init(x: firstTextfield.frame.maxX + 10, y: secondTextfield.frame.maxY + 0, width: 50, height: 1)
                secondLine.backgroundColor = UIColor.lightGray
                cvvScreen.addSubview(secondLine)

                let thirdLine = UILabel()
                thirdLine.frame = CGRect.init(x: secondTextfield.frame.maxX + 10, y: thirdTextfield.frame.maxY + 0, width: 50, height: 1)
                thirdLine.backgroundColor = UIColor.lightGray
                cvvScreen.addSubview(thirdLine)

                okayButton = UIButton()
                switch UIDevice.current.userInterfaceIdiom {
                case .phone:
                    // It's an iPhone
                    okayButton.frame = CGRect.init(x: 30, y: thirdLine.frame.maxY + 15, width: cvvScreen.frame.size.width - 60, height: 40)
                    break
                case .pad:
                    okayButton.frame = CGRect.init(x: 120, y: thirdLine.frame.maxY + 15, width: cvvScreen.frame.size.width - 250, height: 40)

                    break
                    // It's an iPad
                case .unspecified: break
                    // Uh, oh! What could it be?
                case .tv: break

                case .carPlay: break

                }
                okayButton .setTitle("Buy", for: .normal)
                okayButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                okayButton.isUserInteractionEnabled = false
                okayButton.layer.cornerRadius = 5
                okayButton.layer.masksToBounds = true
                okayButton .addTarget(self, action: #selector(okayAction), for: .touchUpInside)
                cvvScreen .addSubview(okayButton)



            }
            }
            @objc func clearActionPayment()
            {
                
                 let appearance = SCLAlertView.SCLAppearance(
                                showCloseButton: false
                            )
                            let alertView = SCLAlertView(appearance: appearance)
                            alertView.addButton("YES"){
                
                                self.bottomView.removeFromSuperview()
             print("NEw Item")
             let defaults = UserDefaults.standard
          defaults.removeObject(forKey: "SavedDictionary")
                                defaults.synchronize()
             //SavedDictionary
                                self.AlreadyPaymentAdded = false
                     let height = self.view.frame.size.height / 1.9
                                self.bottomView = UIView()
                                self.bottomView.frame = CGRect.init(x: 0, y: self.view.frame.maxY - 200, width: self.view.frame.size.width, height: 200)
                                self.bottomView.backgroundColor = UIColor.white
                                self.view .addSubview(self.bottomView)

                         let totalLabel = UILabel()
                         totalLabel.frame = CGRect.init(x: 15, y: 10, width: 150, height: 30)
                         totalLabel.text = "Total"
                         totalLabel.textAlignment = .left
                         totalLabel.font = UIFont.setTarcHeavy(size: 20)
                                self.bottomView .addSubview(totalLabel)
                                let count: Double? = Double(self.noofTickets!) // firstText is UITextField
                         var newPriceString = String()
                                newPriceString = (self.removeString(text: self.ticketPrice!) as NSString) as String
                         let priceValue: Double? = Double(newPriceString)!
                         let totalValue = priceValue! * count!
                         print(totalValue)
                         let priceLabel = UILabel()
                         switch UIDevice.current.userInterfaceIdiom {
                         case .phone:
                             // It's an iPhone
                             if UIScreen.main.sizeType == .iPhone5 {
                                priceLabel.frame = CGRect.init(x: self.bottomView.frame.maxX - 170, y: 10, width: 140, height: 30)

                             }
                             else
                             {
                                priceLabel.frame = CGRect.init(x: self.bottomView.frame.maxX - 160, y: 10, width: 140, height: 30)
                             }
                             break
                         case .pad:
                            priceLabel.frame = CGRect.init(x: self.bottomView.frame.maxX - 180, y: 10, width: 140, height: 30)

                             break
                             // It's an iPad
                         case .unspecified: break
                             // Uh, oh! What could it be?
                         case .tv: break

                         case .carPlay: break

                         }
                         priceLabel.textAlignment = .right
                         let aStr = String(format: "$%.2f", totalValue)

                         priceLabel.text = aStr
                         priceLabel.font = UIFont.setTarcHeavy(size: 20)
                                self.bottomView .addSubview(priceLabel)

                         let firstLine = UILabel()
                                firstLine.frame = CGRect.init(x: 40, y: priceLabel.frame.maxY + 10, width: self.bottomView.frame.size.width - 80, height: 1)
                         firstLine.backgroundColor = UIColor.lightGray
                                self.bottomView .addSubview(firstLine)
                         

                             let addCardButton = UIButton()
                                addCardButton.frame = CGRect.init(x: 20, y: firstLine.frame.maxY + 5, width: self.bottomView.frame.size.width - 40, height: 40)
                             addCardButton .setTitle(" Add New Card", for: .normal)
                             addCardButton.titleLabel?.font = UIFont.setTarcRegular(size: 17)
                             addCardButton.setImage(UIImage.init(named: "chooser-button-input"), for: .normal)
                             addCardButton.backgroundColor = UIColor.clear
                             addCardButton.layer.cornerRadius = 5
                             addCardButton.layer.masksToBounds = true
                             addCardButton.setTitleColor(UIColor.black, for: .normal)
                                self.bottomView .addSubview(addCardButton)
                                addCardButton.addTarget(self, action: #selector(self.addPaymentAction), for: .touchUpInside)

                             let paymentButton = UIButton()

                                paymentButton.frame = CGRect.init(x: 20, y: addCardButton.frame.maxY + 20, width: self.bottomView.frame.size.width - 40, height: 40)


                             paymentButton .setTitle("Proceed to Payment", for: .normal)
                             paymentButton.titleLabel?.font = UIFont.setTarcRegular(size: 17)

                             paymentButton.backgroundColor = UIColor.red
                             paymentButton.layer.cornerRadius = 5
                             paymentButton.layer.masksToBounds = true
                                paymentButton.addTarget(self, action: #selector(self.paymentAction), for: .touchUpInside)
                                self.bottomView .addSubview(paymentButton)
                                
                            }
                            alertView.addButton("NO"){
                            }
                            
                            alertView.showWarning("Remove Card", subTitle: "Are you sure you want to remove the card ?")

                
                
                
            
                           

                
            }
            @objc func closeCvvAction()
            {
                backgroundBlurView.isHidden = true
            }
            @objc func okayAction()
            {
                backgroundBlurView.isHidden = true
                if firstTextfield.text != "" && secondTextfield.text != "" && thirdTextfield.text != ""
                    {

                }


            }
            @objc func addPaymentAction()
            {


                alreadyCardString = "false"
                self.view.backgroundColor = UIColor.white
                secondBigTitle.text = "Payment method"
                makePaymentButton.isHidden = false
                visaImageView.isHidden = false
                masterImageView.isHidden = false
                americanExpressView.isHidden = false
                discoverView.isHidden = false
                DineClubView.isHidden = false
                JCBView.isHidden = false
                customerCardnoLabel.isHidden = false
                customerNameLabel.isHidden = false
                expireDateLabel.isHidden = false
                cvvnumberLabel.isHidden = false
                customerName.isHidden = false
                customerCardno.isHidden = false
                expireDate.isHidden = false
                cvvnumber.isHidden = false
                savePaymentButton.isHidden = false
                stateAddressLabel.isHidden = false
                stateAddress.isHidden = false
                zipCode.isHidden = false
                state.isHidden = false
                stateLabel.isHidden = false
                city.isHidden = false
                cityLabel.isHidden = false
                zipcodeLabel.isHidden = false
                purchaseView.isHidden = true
                bottomView.isHidden = true
                self.view.backgroundColor = UIColor.white
                paymentBackView.backgroundColor = UIColor.white
            }
            @objc func paymentAction()
            {
                if AlreadyPaymentAdded == true
                    {
                    // myTapAction()



                    self.showLoader()
                    let cvvString = "\(firstTextfield.text!)\(secondTextfield.text!)\(thirdTextfield.text!)" as NSString
                    let defaults = UserDefaults.standard
//                  let savedDictionary = defaults.object(forKey: "SavedDictionary") as? [String: Any] ?? [String: Any]()
//                  print(savedDictionary)
                    let savedDictionarydata = defaults.object(forKey: "SavedDictionary") as? Data
                    let savedDictionary: Dictionary? = NSKeyedUnarchiver.unarchiveObject(with: savedDictionarydata!) as! [String: Any]


                    if savedDictionary!.count != 0
                        {

                        icredit_card = [
                            "type": savedDictionary!["type"] as! NSString,
                            "cardholder_name": savedDictionary!["cardholder_name"] as! NSString,
                            "card_number": savedDictionary!["CustomerUniqueNo"] as! NSString,
                            "exp_date": savedDictionary!["expDate"] as! NSString,
                            "cvv": cvvString as NSString
                        ]
                        fdTokenValue = savedDictionary!["CustomerUniqueNo"] as! String
                        print(icredit_card)
                    }
                    var datanil = Data()

                    AlreadypurchaseVoidTransaction(storeData: datanil)

                    }
                    else
                    {
                        self.view.backgroundColor = UIColor.white
                        secondBigTitle.text = "Payment method"
                        makePaymentButton.isHidden = false
                        visaImageView.isHidden = false
                        masterImageView.isHidden = false
                        americanExpressView.isHidden = false
                        discoverView.isHidden = false
                        DineClubView.isHidden = false
                        JCBView.isHidden = false
                        customerCardnoLabel.isHidden = false
                        customerNameLabel.isHidden = false
                        expireDateLabel.isHidden = false
                        cvvnumberLabel.isHidden = false
                        customerName.isHidden = false
                        customerCardno.isHidden = false
                        expireDate.isHidden = false
                        cvvnumber.isHidden = false
                        stateAddressLabel.isHidden = false
                        stateAddress.isHidden = false
                        zipCode.isHidden = false
                        state.isHidden = false
                        stateLabel.isHidden = false
                        city.isHidden = false
                        cityLabel.isHidden = false
                        zipcodeLabel.isHidden = false
                        savePaymentButton.isHidden = false

                        purchaseView.isHidden = true
                        bottomView.isHidden = true
                        self.view.backgroundColor = UIColor.white
                        paymentBackView.backgroundColor = UIColor.white


                    }
                }
                @objc func OnBackClicked() {
                    if alreadyCardString == "false"
                        {
                        if paymentClickedBool == true
                            {
                            self.navigationController?.popViewController(animated: true)

                        }
                        else
                        {
                            alreadyCardString = "true"
                            self.view.backgroundColor = UIColor.init(red: 237 / 255.0, green: 237 / 255.0, blue: 237 / 255.0, alpha: 1.0)
                            paymentBackView.backgroundColor = UIColor.init(red: 237 / 255.0, green: 237 / 255.0, blue: 237 / 255.0, alpha: 1.0)

                            secondBigTitle.text = "Your Purchase"
                            makePaymentButton.isHidden = true

                            visaImageView.isHidden = true
                            masterImageView.isHidden = true
                            americanExpressView.isHidden = true
                            discoverView.isHidden = true
                            DineClubView.isHidden = true
                            JCBView.isHidden = true
                            customerCardnoLabel.isHidden = true
                            customerNameLabel.isHidden = true
                            expireDateLabel.isHidden = true
                            cvvnumberLabel.isHidden = true
                            stateAddressLabel.isHidden = true
                            stateAddress.isHidden = true
                            zipCode.isHidden = true
                            zipcodeLabel.isHidden = true
                            state.isHidden = true
                            stateLabel.isHidden = true
                            city.isHidden = true
                            cityLabel.isHidden = true
                            customerName.isHidden = true
                            customerCardno.isHidden = true
                            expireDate.isHidden = true
                            cvvnumber.isHidden = true

                            savePaymentButton.isHidden = true

                            alreadyPaymentDesign()
                        }
                    }
                    else
                    {
                        self.navigationController?.popViewController(animated: true)

                    }
                }
                @objc func makePayment() {



                    paymentClickedBool = true
                    var customerCardString = NSString()
                    customerCardString = ""
                    if customerCardno.text != ""
                        {
                        customerCardString = removeSpecialCharsFromString(text: customerCardno.text!) as NSString
                    }
                    print(customerCardString)

                    var customerExpireString = NSString()
                    customerExpireString = ""
                    if customerCardno.text != ""
                        {
                        customerExpireString = removeSpecialCharsFromString(text: expireDate.text!) as NSString
                    }
                    print(customerExpireString)
                    if cardType == nil
                        {
                        cardType = "Fake"
                    }
                    icredit_card = [
                        "type": cardType!,
                        "cardholder_name": customerName.text! as NSString,
                        "card_number": customerCardString,
                        "exp_date": customerExpireString,
                        "cvv": cvvnumber.text! as NSString,
                        "Street": stateAddress.text! as NSString,
                        "Zip": zipCode.text! as NSString,
                        "State": state.text! as NSString,
                        "City": city.text! as NSString
                    ]
                    print(icredit_card)

                    let datanil = Data()
                    self.purchaseVoidTransaction(storeData: datanil)
                }
                func removeSpecialCharsFromString(text: String) -> String {
                    let okayChars: Set<Character> =
                        Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890".characters)
                    return String(text.characters.filter { okayChars.contains($0) })
                }
                func removeString(text: String) -> String {
                    let okayChars: Set<Character> =
                        Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890.".characters)
                    return String(text.characters.filter { okayChars.contains($0) })
                }
                func removeAllString(text: String) -> String {
                    let okayChars: Set<Character> =
                        Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890".characters)
                    return String(text.characters.filter { okayChars.contains($0) })
                }

                @objc func textFieldDidChange(textField: UITextField) {
                    print(textField.text)
                    if textField.tag == 100
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            customerNameLabel.text = "Name on Card"

                        }
                        else
                        {
                            customerNameLabel.text = ""
                        }

                    }
                    else if textField.tag == 101
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            customerCardnoLabel.text = "Credit card Number"
                        }
                        else
                        {
                            customerCardnoLabel.text = ""
                        }

                    }
                    else if textField.tag == 102
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            expireDateLabel.text = "MM/YY"
                        }
                        else
                        {
                            expireDateLabel.text = ""
                        }
                    }
                    else if textField.tag == 103
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            cvvnumberLabel.text = "CVV"
                        }
                        else
                        {
                            cvvnumberLabel.text = ""
                        }
                    }
                    else if textField.tag == 104
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            stateAddressLabel.text = "Street Address"

                        }
                        else
                        {
                            stateAddressLabel.text = ""
                        }

                    }
                    else if textField.tag == 105
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            cityLabel.text = "City"
                        }
                        else
                        {
                            cityLabel.text = ""
                        }
                    }
                    else if textField.tag == 106
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            stateLabel.text = "State"
                        }
                        else
                        {
                            stateLabel.text = ""
                        }
                    }
                    else if textField.tag == 107
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            zipcodeLabel.text = "ZIP"
                        }
                        else
                        {
                            zipcodeLabel.text = ""
                        }
                    }
                    else if textField.tag == 300
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            secondTextfield.becomeFirstResponder()
                        }
                    }
                    else if textField.tag == 301
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            thirdTextfield.becomeFirstResponder()

                        }
                        else
                        {
                            firstTextfield.becomeFirstResponder()
                        }
                    }
                    else if textField.tag == 302
                        {
                        if (textField.text?.characters.count)! > 0
                            {
                            thirdTextfield.resignFirstResponder()

                        }
                        else
                        {
                            secondTextfield.becomeFirstResponder()
                        }
                    }
                    if firstTextfield.text != "" && secondTextfield.text != "" && thirdTextfield.text != ""
                        {
                        okayButton.backgroundColor = UIColor.red
                        okayButton.isUserInteractionEnabled = true
                    }

                    if customerName.text?.isEmpty == false && customerCardno.text?.isEmpty == false && expireDate.text?.isEmpty == false && cvvnumber.text?.isEmpty == false && stateAddress.text?.isEmpty == false && zipCode.text?.isEmpty == false && state.text?.isEmpty == false && city.text?.isEmpty == false
                        {
                        makePaymentButton.backgroundColor = UIColor.red
                        makePaymentButton.isUserInteractionEnabled = true
                    }
                    else
                    {
                        makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                        makePaymentButton.isUserInteractionEnabled = false
                    }

                }
    func textFieldDidEndEditing(_ textField: UITextField) {
         if customerName.text?.isEmpty == false && customerCardno.text?.isEmpty == false && expireDate.text?.isEmpty == false && cvvnumber.text?.isEmpty == false && stateAddress.text?.isEmpty == false && zipCode.text?.isEmpty == false && state.text?.isEmpty == false && city.text?.isEmpty == false
                               {
                               makePaymentButton.backgroundColor = UIColor.red
                               makePaymentButton.isUserInteractionEnabled = true
                           }
                           else
                           {
                               makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                               makePaymentButton.isUserInteractionEnabled = false
                           }
    }
                func textFieldDidBeginEditing(_ textField: UITextField) {
                    if textField.tag == 106
                        {
                        self.pickUp(textField)

                    }
                }
                func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

                    if textField.tag == 101
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            customerCardnoLabel.text = "Credit card Number"
                        }
                        else
                        {
                            customerCardnoLabel.text = ""
                            textField.rightView?.frame = CGRect(x: 0, y: 5, width: 0, height: 0)
                            textField.rightViewMode = .never
                        }

                    }
                    else if textField.tag == 102
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            expireDateLabel.text = "MM/YY"
                        }
                        else
                        {
                            expireDateLabel.text = ""
                        }
                    }
                    else if textField.tag == 104
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            stateAddressLabel.text = "Street Address"
                        }
                        else
                        {
                            stateAddressLabel.text = ""
                        }
                    }
                    else if textField.tag == 105
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            cityLabel.text = "City"
                        }
                        else
                        {
                            cityLabel.text = ""
                        }
                    }
                    else if textField.tag == 106
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            stateLabel.text = "State"
                        }
                        else
                        {
                            stateLabel.text = ""
                        }
                    }
                    else if textField.tag == 107
                        {
                        if (textField.text?.characters.count)! > 1
                            {
                            zipcodeLabel.text = "ZIP"
                        }
                        else
                        {
                            zipcodeLabel.text = ""
                        }
                    }

                    if textField == customerCardno
                        {

                        if textField.text!.characters.count == 1
                            {


                            if textField.text == "4"
                                {
                                cardType = "visa"
                                textField.rightView = UIImageView(image: UIImage.init(named: "visa_icon"))

                            }
                            else if textField.text == "5" || textField.text == "2"
                                {
                                cardType = "Mastercard"
                                textField.rightView = UIImageView(image: UIImage.init(named: "mastercard_icon"))

                            }
                            else if textField.text == "6"
                                {
                                cardType = "discover"
                                textField.rightView = UIImageView(image: UIImage.init(named: "Discover"))

                            }
                            else
                            {
                                cardType = "fake"
                                textField.rightView = UIImageView(image: UIImage.init(named: "visas_icon"))

                            }
                        }

                        if textField.text!.characters.count == 2
                            {
                            if textField.text == "34" || textField.text == "37"
                                {
                                cardType = "American Express"
                                textField.rightView = UIImageView(image: UIImage.init(named: "americanexpress_icon"))
                            }
                            else if textField.text == "35"
                                {
                                cardType = "JCB"
                                textField.rightView = UIImageView(image: UIImage.init(named: "JCB"))
                            }
                            else if textField.text == "30" || textField.text == "36" || textField.text == "38" || textField.text == "39"
                                {
                                cardType = "Diners Club"
                                textField.rightView = UIImageView(image: UIImage.init(named: "Diners Club"))
                            }
                        }




                        if (textField.text?.characters.count)! > 1
                            {
                            textField.rightView?.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
                            textField.rightViewMode = .always
                        }
                        else
                        {
                            textField.rightView?.frame = CGRect(x: 0, y: 5, width: 0, height: 0)
                            textField.rightViewMode = .never
                        }

                        let maxNumberOfCharacters = 19

                        // only allow numerical characters
                        guard string.characters.flatMap({ Int(String($0)) }).count ==
                            string.characters.count else {
                                return false

                        }

                        let text = textField.text ?? ""

                        if string.characters.count == 0 {
                            textField.text = String(text.characters.dropLast()).chunkFormatted()
                        }
                        else {
                            let newText = String((text + string).characters
                                    .filter({ $0 != "-" }).prefix(maxNumberOfCharacters))
                            textField.text = newText.chunkFormatted()
                        }

                        return false
                    }
                    else if textField == expireDate
                        {


                        let maxNumberOfCharacters = 4

                        // only allow numerical characters
                        guard string.characters.flatMap({ Int(String($0)) }).count ==
                            string.characters.count else { return false }

                        let text = textField.text ?? ""

                        if string.characters.count == 0 {
                            textField.text = String(text.characters.dropLast()).exprieFormatted()
                        }
                        else {
                            let newText = String((text + string).characters
                                    .filter({ $0 != "/" }).prefix(maxNumberOfCharacters))
                            textField.text = newText.exprieFormatted()
                        }

                        return false
                    }
                    else if textField == zipCode
                        {
                        let maxLength = 5
                        let currentString: NSString = textField.text! as NSString
                        let newString: NSString =
                            currentString.replacingCharacters(in: range, with: string) as NSString
                        return newString.length <= maxLength
                    }
                    else if textField == cvvnumber
                        {
                        let maxLength = 4
                        let currentString: NSString = textField.text! as NSString
                        let newString: NSString =
                            currentString.replacingCharacters(in: range, with: string) as NSString
                        return newString.length <= maxLength
                    }
                    else if textField.tag == 300
                        {
                        let maxLength = 1
                        let currentString: NSString = textField.text! as NSString
                        let newString: NSString =
                            currentString.replacingCharacters(in: range, with: string) as NSString
                        return newString.length <= maxLength
                    }
                    else if textField.tag == 301
                        {
                        let maxLength = 1
                        let currentString: NSString = textField.text! as NSString
                        let newString: NSString =
                            currentString.replacingCharacters(in: range, with: string) as NSString

                        return newString.length <= maxLength
                    }
                    else if textField.tag == 302
                        {
                        let maxLength = 1
                        let currentString: NSString = textField.text! as NSString
                        let newString: NSString =
                            currentString.replacingCharacters(in: range, with: string) as NSString

                        return newString.length <= maxLength
                    }
                    else
                    {
                        return true
                    }
                }
                func setPaddingView(strImgname: String, txtField: UITextField) {
                    let imageView = UIImageView(image: UIImage(named: strImgname))
                    imageView.frame = CGRect(x: 0, y: 5, width: imageView.image!.size.width, height: imageView.image!.size.height)
                    let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
                    paddingView.addSubview(imageView)
                    txtField.leftViewMode = .always
                    txtField.leftView = paddingView
                }


                func purchaseVoidTransaction(storeData: Data) {
                    print(storeData)
                    self.showLoader()
                    let valueEntered = NSDecimalNumber(string: "20")

                    if !(valueEntered == NSDecimalNumber.notANumber) {


                        // Test credit card info
                        let credit_card = icredit_card

                        let myClient = PayeezySDK(apiKey: KApiKey, apiSecret: KApiSecret, merchantToken: KToken, url: PURL)
                        //print(credit_card)
                        var profileID = Int()
                        var finalMercentID = NSString()

                        let userDefaults1 = UserDefaults.standard
                        if userDefaults1.value(forKey: "userIdString") == nil
                            {
                          

                            let accessToken = userDefaults1.value(forKey: "accessToken")

                            let getUserId = "\(APIUrl.ZIGTARCAPI)User/api/GetUserId?Token=\(accessToken!)"

                            Alamofire.request(getUserId, method: .post, parameters: nil, encoding: JSONEncoding.default)
                                .responseJSON { response in
                                    switch response.result {
                                    case .success:


                                        if (response.result.value != nil) {

                                            let responseVar = response.result.value
                                            let json = JSON(responseVar!)
                                            print(json.rawString()!)
                                            let useridString = json.rawString()! as! String
                                            let userid = Int(useridString)
                                            let userDefaults = UserDefaults.standard
                                            userDefaults.set(userid, forKey: "userIdString")


                                            var userIDString: Int?

                                            if userDefaults1.integer(forKey: "userIdString") != nil {
                                                userIDString = userDefaults1.integer(forKey: "userIdString")

                                            }
                                            else
                                            {
                                                userIDString = 0

                                            }
                                            let xInt: Int? = userIDString

                                            profileID = xInt!
                                            print("\(profileID)")
                                            let string0 = String(format: "%06d", profileID) // returns "00"
                                            print(string0)

                                            var tenDigitNumber: String {
                                                var result = ""
                                                repeat {
                                                    // Create a string with a random number 0...9999
                                                    let arcrandom = arc4random_uniform(1000000000)
                                                    result = String(format: "%10d", arcrandom)
                                                } while result.count < 10
                                                return result
                                            }
                                            let randomInt = Int.random(in: 1..<5)
                                            print(randomInt)
                                            
                                            print("\(tenDigitNumber)")
                                            let a: Int! = Int(self.noofTickets!)
                                            let ticketCount = String(format: "%d", a) // returns "00"
                                            var trimmedString = tenDigitNumber.trimmingCharacters(in: .whitespaces)
                                            print(trimmedString)
                                            if trimmedString.count > 8
                                                {
                                                trimmedString = String(trimmedString.prefix(8))

                                            }
                                            if trimmedString.count <= 7
                                            {
                                                let randomInt = Int.random(in: 1..<9)
                                                print(randomInt)
                                                 let string0 = "\(trimmedString)\(randomInt)"
                                                trimmedString = string0
                                            }
                                            if self.LivepaymentBool == true
                                                {
                                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)0" as NSString

                                            }
                                            else
                                            {
                                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)1" as NSString

                                            }
                                            print("tenDigitNumber==>\(finalMercentID)")


                                            //
                                            //fdTokenValue credit_card!["exp_date"] as! String credit_card!["type"] as! String fdTokenValue cardholder_name (credit_card!["exp_date"] as! String)
                                            print("HELLOARUNNEWPAYMENT\(finalMercentID)")
                                            self.totalPrice = self.removeAllString(text: self.totalPrice!)
                                            // "Zip":zipCode.text! as NSString,

                                            print((credit_card!["Street"] as! String))
                                            print((credit_card!["City"] as! String))
                                            print((credit_card!["State"] as! String))
                                            print((credit_card!["Zip"] as! String))

                                            myClient!.submitAuthorizeTransactionL2(withCreditCardDetails: self.totalPrice, transactionType: "purchase", transactionTag: "", merchantRefForProcessing: finalMercentID as String, pMethod: "credit_card", currencyCode: "USD", cardtype: (credit_card!["type"] as! String), cardHolderName: (credit_card!["cardholder_name"] as! String), cardNumber: (credit_card!["card_number"] as! String), cardExpirymMonthAndYear: (credit_card!["exp_date"] as! String), cardCVV: (credit_card!["cvv"] as! String), tax1Amount: "0.0", tax1Number: "", tax2Amount: "0.0", tax2Number: "", customerRef: finalMercentID as String, street: (credit_card!["Street"] as! String), city: (credit_card!["City"] as! String), stateProvince: (credit_card!["State"] as! String), zipPostalCode: (credit_card!["Zip"] as! String), country: "us") { dict, error in



                                                var authStatusMessage: String? = nil
                                                //dump(dict)
                                                if error == nil {
                                                    self.hideLoader()
                                                    if let objectFirst = dict?["gateway_message"]
                                                        {
                                                        let gateWaymessage = dict?["gateway_message"] as! String

                                                        if gateWaymessage == "Transaction Normal"
                                                            {
                                                            if let object = dict?["transaction_type"], let object1 = dict?["transaction_id"], let object2 = dict?["transaction_tag"], let object3 = dict?["correlation_id"], let object4 = dict?["bank_resp_code"] {
                                                                authStatusMessage = "Transaction Successful\rType:\(object)\rTransaction ID:\(object1)\rTransaction Tag:\(object2)\rCorrelation Id:\(object3)\rBank Response Code:\(object4)"

                                                                if self.savePaymentBool == true
                                                                    {
                                                                    let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                                                    let result = dict?["token"] as? [AnyHashable: Any]
                                                                    let token_data = result?["token_data"] as! NSDictionary
                                                                    self.fdTokenValue = token_data["value"] as! String
                                                                    print(self.fdTokenValue)


                                                                    let dict: [String: Any] = ["CustomerUniqueNo": self.fdTokenValue!, "type": (credit_card!["type"] as! String), "expDate": (credit_card!["exp_date"] as! String), "cardholder_name": (credit_card!["cardholder_name"] as! String)]
                                                                    let defaults = UserDefaults.standard
                                                                    let dataExample: Data = NSKeyedArchiver.archivedData(withRootObject: dict)
                                                                    defaults.set(dataExample, forKey: "SavedDictionary")
                                                                }


                                                                self.postPaymentDetailApi(paymentDict: dict as! NSDictionary, mercentID: finalMercentID)
                                                                let transaction_id = dict!["transaction_id"] as! NSString

                                                                self.buyTicketAction(ticetDict: dict! as NSDictionary, TokenID: finalMercentID, TransactionId: transaction_id)
                                                            }
                                                            else
                                                            {
                                                                self.hideLoader()

                                                                let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                                                print(commonerrorUserInfo)
                                                                var transaction_status = NSString()
                                                                var correlation_id = NSString()

                                                                if commonerrorUserInfo!["transaction_status"] != nil {
                                                                    transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                                                }
                                                                else
                                                                {
                                                                    transaction_status = ""

                                                                }

                                                                if commonerrorUserInfo!["correlation_id"] != nil {
                                                                    correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                                                }
                                                                else
                                                                {
                                                                    correlation_id = ""

                                                                }
                                                                print(correlation_id)
                                                                print(transaction_status)
                                                                //bank_message
                                                                var message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"

                                                                let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                                let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                                                let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "Null"
                                                                let amount = commonerrorUserInfo!.value(forKey: "amount") ?? "Null"
                                                                let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                                                let method = commonerrorUserInfo!.value(forKey: "method") as! NSString
                                                                let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                                                let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                                                let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                                                let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"


                                                                let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message"]
                                                                let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message]

                                                                let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                                                print(errorDict)
                                                                self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)
                                                                if (message as! String) == "Null" {
                                                                    message = gateway_message
                                                                }

                                                                self.customerCardno.text = ""
                                                                self.customerName.text = ""
                                                                self.cvvnumber.text = ""
                                                                self.expireDate.text = ""
                                                                self.state.text = ""
                                                                self.city.text = ""
                                                                self.stateAddress.text = ""
                                                                self.zipCode.text = ""
                                                                self.customerCardnoLabel.text = ""
                                                                self.customerNameLabel.text = ""
                                                                self.cvvnumberLabel.text = ""
                                                                self.expireDateLabel.text = ""
                                                                self.stateAddressLabel.text = ""
                                                                self.cityLabel.text = ""
                                                                self.stateLabel.text = ""
                                                                self.zipcodeLabel.text = ""
                                                                self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                                                self.makePaymentButton.isUserInteractionEnabled = false
                                                                self.alertForOffilne(message: message as! NSString)

                                                            }
                                                        }
                                                        else
                                                        {
                                                            let gateWaymessage = dict?["gateway_message"] as! String
                                                            //                            self.view.makeToast(gateWaymessage)
                                                            self.hideLoader()

                                                            let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                                            print(commonerrorUserInfo)
                                                            var transaction_status = NSString()
                                                            var correlation_id = NSString()

                                                            if commonerrorUserInfo!["transaction_status"] != nil {
                                                                transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                                            }
                                                            else
                                                            {
                                                                transaction_status = ""

                                                            }

                                                            if commonerrorUserInfo!["correlation_id"] != nil {
                                                                correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                                            }
                                                            else
                                                            {
                                                                correlation_id = ""

                                                            }
                                                            print(correlation_id)
                                                            print(transaction_status)
                                                            //bank_message
                                                            let message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                            let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                            let avsString = commonerrorUserInfo!.value(forKey: "avs") ?? "Null"
                                                            let cvv2String = commonerrorUserInfo!.value(forKey: "cvv2") ?? "Null"
                                                            let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                                            let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "USD"
                                                            let amount = commonerrorUserInfo!.value(forKey: "amount") ?? "Null"
                                                            let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                                            let method = commonerrorUserInfo!.value(forKey: "method") ?? "Null"
                                                            let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                                            let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                                            let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                                            let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"


                                                            let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message", "avs", "cvv2"]
                                                            let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message, avsString, cvv2String]

                                                            let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                                            print(errorDict)
                                                            self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)



                                                            self.alertForOffilne(message: gateWaymessage as NSString)
                                                            self.customerCardno.text = ""
                                                            self.customerName.text = ""
                                                            self.cvvnumber.text = ""
                                                            self.expireDate.text = ""
                                                            self.state.text = ""
                                                            self.city.text = ""
                                                            self.stateAddress.text = ""
                                                            self.zipCode.text = ""
                                                            self.customerCardnoLabel.text = ""
                                                            self.customerNameLabel.text = ""
                                                            self.cvvnumberLabel.text = ""
                                                            self.expireDateLabel.text = ""
                                                            self.stateAddressLabel.text = ""
                                                            self.cityLabel.text = ""
                                                            self.stateLabel.text = ""
                                                            self.zipcodeLabel.text = ""
                                                            self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                                            self.makePaymentButton.isUserInteractionEnabled = false


                                                        }
                                                    }
                                                    else
                                                    {
                                                        self.hideLoader()

                                                        let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                                        print(commonerrorUserInfo)
                                                        var transaction_status = NSString()
                                                        var correlation_id = NSString()

                                                        if commonerrorUserInfo!["transaction_status"] != nil {
                                                            transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                                        }
                                                        else
                                                        {
                                                            transaction_status = ""

                                                        }

                                                        if commonerrorUserInfo!["correlation_id"] != nil {
                                                            correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                                        }
                                                        else
                                                        {
                                                            correlation_id = ""

                                                        }
                                                        print(correlation_id)
                                                        print(transaction_status)
                                                        //bank_message
                                                        var message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                        let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                        let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                                        let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "Null"
                                                        let amount = commonerrorUserInfo!.value(forKey: "amount") ?? "Null"
                                                        let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                                        let method = commonerrorUserInfo!.value(forKey: "method") as! NSString
                                                        let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                                        let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                                        let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                                        let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"
                                                        let avsString = commonerrorUserInfo!.value(forKey: "avs") ?? "Null"
                                                        let cvv2String = commonerrorUserInfo!.value(forKey: "cvv2") ?? "Null"

                                                        let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message", "avs", "cvv2"]
                                                        let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message, avsString, cvv2String]

                                                        let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                                        print(errorDict)
                                                        self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)

                                                        if (message as! String) == "Null" {
                                                            message = gateway_message


                                                        }

                                                        self.alertForOffilne(message: message as! NSString)
                                                        self.customerCardno.text = ""
                                                        self.customerName.text = ""
                                                        self.cvvnumber.text = ""
                                                        self.expireDate.text = ""
                                                        self.state.text = ""
                                                        self.city.text = ""
                                                        self.stateAddress.text = ""
                                                        self.zipCode.text = ""
                                                        self.customerCardnoLabel.text = ""
                                                        self.customerNameLabel.text = ""
                                                        self.cvvnumberLabel.text = ""
                                                        self.expireDateLabel.text = ""
                                                        self.stateAddressLabel.text = ""
                                                        self.cityLabel.text = ""
                                                        self.stateLabel.text = ""
                                                        self.zipcodeLabel.text = ""
                                                        self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                                        self.makePaymentButton.isUserInteractionEnabled = false
                                                    }
                                                } else {
                                                    self.hideLoader()

                                                    let errorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo["Error"] as? NSDictionary
                                                    let commonerrorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo as? NSDictionary
                                                    print(commonerrorUserInfo)
                                                    print(errorUserInfo)
                                                    var transaction_status = NSString()
                                                    var correlation_id = NSString()

                                                    if commonerrorUserInfo!["transaction_status"] != nil {
                                                        transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                                    }
                                                    else
                                                    {
                                                        transaction_status = ""

                                                    }
                                                    if commonerrorUserInfo!["correlation_id"] != nil {
                                                        correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                                    }
                                                    else
                                                    {
                                                        correlation_id = ""

                                                    }

                                                    print(correlation_id)
                                                    print(transaction_status)

                                                    var messageArray = NSArray()
                                                    if errorUserInfo!["messages"] != nil {
                                                        messageArray = errorUserInfo?.value(forKey: "messages") as! NSArray
                                                    }
                                                    else
                                                    {
                                                        messageArray = NSArray()
                                                    }
                                                    var descriptionAlertString = NSString()
                                                    if messageArray.count > 0 || messageArray != nil {
                                                        for index in 0..<messageArray.count {




                                                            print(messageArray)
                                                            let messageDict = messageArray.object(at: index) as! NSDictionary
                                                            print(messageDict)
                                                            let descriptionString = messageDict.value(forKey: "description") as! NSString
                                                            let codeString = messageDict.value(forKey: "code") as! NSString
                                                            authStatusMessage = "Error was encountered processing transaction: \(error.debugDescription.description)"


                                                            let keys = ["correlation_id", "transaction_status", "description", "code", "transaction_id"]
                                                            let values = [correlation_id, transaction_status, descriptionString, codeString, finalMercentID]

                                                            let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))


                                                            if index > 0
                                                                {
                                                                descriptionAlertString = "\(descriptionAlertString) and \(descriptionString) " as! NSString

                                                            }
                                                            else
                                                            {
                                                                descriptionAlertString = "\(descriptionString)" as! NSString
                                                            }
                                                            print(errorDict)
                                                            self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)

                                                        }
                                                    }
                                                    self.alertForOffilne(message: descriptionAlertString)
                                                    self.customerCardno.text = ""
                                                    self.customerName.text = ""
                                                    self.cvvnumber.text = ""
                                                    self.expireDate.text = ""
                                                    self.state.text = ""
                                                    self.city.text = ""
                                                    self.stateAddress.text = ""
                                                    self.zipCode.text = ""
                                                    self.customerCardnoLabel.text = ""
                                                    self.customerNameLabel.text = ""
                                                    self.cvvnumberLabel.text = ""
                                                    self.expireDateLabel.text = ""
                                                    self.stateAddressLabel.text = ""
                                                    self.cityLabel.text = ""
                                                    self.stateLabel.text = ""
                                                    self.zipcodeLabel.text = ""
                                                    self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                                    self.makePaymentButton.isUserInteractionEnabled = false
                                                }


                                            }





                                        }
                                    case .failure(let error):
                                        print(error)

                                        //
                                        var newPriceString = String()
                                        newPriceString = (self.removeString(text: self.ticketPrice!) as NSString) as String

                                        let count: Double? = Double(self.noofTickets!) // firstText is UITextField
                                        let priceValue: Double? = Double(newPriceString)!
                                        let totalValue = priceValue! * count!
                                        print(totalValue)

                                        print("\(self.ticketType)")

                                        let priceString = String(format: "%.2f", priceValue!)
                                        let totalPriceString = String(format: "%.2f", totalValue)
                                        print("\(priceString)")
                                        print("\(totalPriceString)")
                                        var pref = UserDefaults.standard
                                        self.addTickeValue.removeAllObjects()

                                        let BuyTicket_Amount = priceString
                                        let BuyTicket_TripId = "4"
                                        let BuyTicket_RouteId = self.ticketType!
                                        let BuyTicket_ProfileId = (pref.object(forKey: "ProfileID")) as? Int
                                        let BuyTicket_FromAddress = "S 6th @ W Liberty"
                                        let BuyTicket_DestinationAddress = "4th @ Ormsby"
                                        let a: Int! = Int(self.noofTickets!) // firstText is UITextField

                                        for i in 0 ..< a {

                                            let Addticketlist: NSMutableDictionary = [:]
                                            //                    print(BuyTicket_ProfileId)
                                            //                    print(fareID)
                                            Addticketlist.setValue("\((BuyTicket_Amount))", forKey: "Amount")
                                            Addticketlist.setValue("\((BuyTicket_RouteId))", forKey: "RouteId")
                                            Addticketlist.setValue("\((BuyTicket_TripId))", forKey: "TripId")
                                            Addticketlist.setValue(BuyTicket_ProfileId, forKey: "ProfileId")
                                            Addticketlist.setValue("\((BuyTicket_FromAddress))", forKey: "FromAddress")
                                            Addticketlist.setValue("\((BuyTicket_DestinationAddress))", forKey: "DestinationAddress")
                                            Addticketlist.setValue(self.fareID, forKey: "Fareid")




                                            self.addTickeValue.add(Addticketlist)
                                        }
                                        print("\(self.addTickeValue)")
                                        self.showLoader()
                                        let Timestamp = Date()
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                                        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
                                        let utctimeStamp = dateFormatter.string(from: Timestamp)

                                        let headers = [
                                            "Content-Type": "application/json",
                                        ]



                                        var fourDigitNumber: String {
                                            var result = ""
                                            repeat {
                                                // Create a string with a random number 0...9999
                                                result = String(format: "%05d", arc4random_uniform(1000000))
                                            } while result.count < 6
                                            return result
                                        }
                                        var usernameString = NSString()
                                        var emailString = NSString()
                                        var phoneString = NSString()

                                        let userDefaults1 = UserDefaults.standard

                                        if userDefaults1.value(forKey: "userName") == nil
                                            {
                                            usernameString = ""
                                        }
                                        else
                                        {
                                            usernameString = userDefaults1.value(forKey: "userName") as! NSString
                                        }
                                        if userDefaults1.value(forKey: "userEmail") == nil
                                            {
                                            emailString = ""
                                        }
                                        else
                                        {
                                            emailString = userDefaults1.value(forKey: "userEmail") as! NSString
                                        }

                                        if userDefaults1.value(forKey: "phoneNumber") == nil
                                            {
                                            phoneString = ""
                                        }
                                        else
                                        {
                                            phoneString = userDefaults1.value(forKey: "phoneNumber") as! NSString
                                        }


                                        let Sendbody: [String: Any] = [
                                            "Subject": "Userid Not available",
                                            "Username": usernameString,
                                            "Emailid": emailString,
                                            "Phoneno": phoneString,
                                            "Profileid": BuyTicket_ProfileId!,
                                            "Ticketcount": self.noofTickets!,
                                            "Transactionid": "",
                                            "Txn_referenceno": "",
                                            "Tickettype": self.ticketType!,
                                            "Token": pref.object(forKey: "accessToken")!,
                                            "Totalamount": totalPriceString,
                                            "Transactiondate": utctimeStamp,
                                            "Content": "",

                                        ]

                                        let SendFailedMailbody: [String: Any] = [
                                            "To": "isaac@zeddigital.net",
                                            "Subject": "Three API Failed",
                                            "Content": "Userid Not available",
                                            "Cc": "",
                                            "Bcc": "",
                                            "From": "iOS Support",
                                            "emailfrom": "info@ridetarc.org",


                                        ]
                                        Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                            .responseJSON { response in
                                                switch response.result {
                                                case .success:


                                                    if (response.result.value != nil) {

                                                        let responseVar = response.result.value
                                                        let json = JSON(responseVar!)
                                                        print(json)
                                                        if json["Message"] == "Ok" {


                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")

                                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                            let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)


                                                            self.hideLoader()


                                                        } else {
                                                            self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                                                                                                                  self.view.makeToast("\(json["Message"])")
//                                                                                                                                                  self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                                                                                                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                                                                                                                let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                                                                                                                self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                                                                                                              self.hideLoader()
                                                        }

                                                    } else {
                                                        self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                                                                                                              self.view.makeToast("Please Try Again")
//                                                                                                                                              self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                                                                                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                                                                                                            let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                                                                                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                                                                                                               self.hideLoader()
                                                    }
                                                case .failure(let error):
                                                    print(error)
                                                    self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                                                                                                          self.view.makeToast("Please Try Again!")
//                                                                                                                                          self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                                                                                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                                                                                                        let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                                                                                                        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                                                                                                           self.hideLoader()
                                                }
                                        }


                                        self.hideLoader()
                                    }
                            }







                            //
                        }
                        else
                        {
                            var userIDString: Int?

                            if userDefaults1.integer(forKey: "userIdString") != nil {
                                userIDString = userDefaults1.integer(forKey: "userIdString")

                            }
                            else
                            {
                                userIDString = 0

                            }
                            let xInt: Int? = userIDString

                            profileID = xInt!
                            print("\(profileID)")
                            let string0 = String(format: "%06d", profileID) // returns "00"
                            print(string0)

                            var tenDigitNumber: String {
                                var result = ""
                                repeat {
                                    // Create a string with a random number 0...9999
                                    let arcrandom = arc4random_uniform(1000000000)
                                    result = String(format: "%10d", arcrandom)
                                } while result.count < 10
                                return result
                            }
                            print("\(tenDigitNumber)")
                            let a: Int! = Int(self.noofTickets!)
                            let ticketCount = String(format: "%d", a) // returns "00"
                            var trimmedString = tenDigitNumber.trimmingCharacters(in: .whitespaces)
                            print(trimmedString)
                            if trimmedString.count > 8
                                {
                                trimmedString = String(trimmedString.prefix(8))

                            }
                            if trimmedString.count <= 7
                            {
                                let randomInt = Int.random(in: 1..<9)
                                print(randomInt)
                                 let string0 = "\(trimmedString)\(randomInt)"
                                trimmedString = string0
                            }
                          
                            if LivepaymentBool == true
                                {
                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)0" as NSString

                            }
                            else
                            {
                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)1" as NSString

                            }
                            print("tenDigitNumber==>\(finalMercentID)")

                            //fdTokenValue credit_card!["exp_date"] as! String credit_card!["type"] as! String fdTokenValue cardholder_name (credit_card!["exp_date"] as! String)
                            print("HELLOARUNNEWPAYMENT\(finalMercentID)")
                            totalPrice = removeAllString(text: totalPrice!)
                            // "Zip":zipCode.text! as NSString,

                            print((credit_card!["Street"] as! String))
                            print((credit_card!["City"] as! String))
                            print((credit_card!["State"] as! String))
                            print((credit_card!["Zip"] as! String))

                            myClient!.submitAuthorizeTransactionL2(withCreditCardDetails: totalPrice, transactionType: "purchase", transactionTag: "", merchantRefForProcessing: finalMercentID as String, pMethod: "credit_card", currencyCode: "USD", cardtype: (credit_card!["type"] as! String), cardHolderName: (credit_card!["cardholder_name"] as! String), cardNumber: (credit_card!["card_number"] as! String), cardExpirymMonthAndYear: (credit_card!["exp_date"] as! String), cardCVV: (credit_card!["cvv"] as! String), tax1Amount: "", tax1Number: "", tax2Amount: "", tax2Number: "", customerRef: finalMercentID as String, street: (credit_card!["Street"] as! String), city: (credit_card!["City"] as! String), stateProvince: (credit_card!["State"] as! String), zipPostalCode: (credit_card!["Zip"] as! String), country: "us") { dict, error in



                                var authStatusMessage: String? = nil
                                if error == nil {
                                    self.hideLoader()
                                    if let objectFirst = dict?["gateway_message"]
                                        {
                                        let gateWaymessage = dict?["gateway_message"] as! String

                                        if gateWaymessage == "Transaction Normal"
                                            {
                                            if let object = dict?["transaction_type"], let object1 = dict?["transaction_id"], let object2 = dict?["transaction_tag"], let object3 = dict?["correlation_id"], let object4 = dict?["bank_resp_code"] {
                                                authStatusMessage = "Transaction Successful\rType:\(object)\rTransaction ID:\(object1)\rTransaction Tag:\(object2)\rCorrelation Id:\(object3)\rBank Response Code:\(object4)"

                                                if self.savePaymentBool == true
                                                    {
                                                    let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                                    let result = dict?["token"] as? [AnyHashable: Any]
                                                    let token_data = result?["token_data"] as! NSDictionary
                                                    self.fdTokenValue = token_data["value"] as! String
                                                    print(self.fdTokenValue)


                                                    let dict: [String: Any] = ["CustomerUniqueNo": self.fdTokenValue!, "type": (credit_card!["type"] as! String), "expDate": (credit_card!["exp_date"] as! String), "cardholder_name": (credit_card!["cardholder_name"] as! String)]
                                                    let defaults = UserDefaults.standard
                                                    let dataExample: Data = NSKeyedArchiver.archivedData(withRootObject: dict)
                                                    defaults.set(dataExample, forKey: "SavedDictionary")
                                                }


                                                self.postPaymentDetailApi(paymentDict: dict as! NSDictionary, mercentID: finalMercentID)
                                                let transaction_id = dict!["transaction_id"] as! NSString

                                                self.buyTicketAction(ticetDict: dict! as NSDictionary, TokenID: finalMercentID, TransactionId: transaction_id)
                                            }
                                            else
                                            {
                                                self.hideLoader()

                                                let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                                print(commonerrorUserInfo)
                                                var transaction_status = NSString()
                                                var correlation_id = NSString()

                                                if commonerrorUserInfo!["transaction_status"] != nil {
                                                    transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                                }
                                                else
                                                {
                                                    transaction_status = ""

                                                }

                                                if commonerrorUserInfo!["correlation_id"] != nil {
                                                    correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                                }
                                                else
                                                {
                                                    correlation_id = ""

                                                }
                                                print(correlation_id)
                                                print(transaction_status)
                                                //bank_message
                                                var message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"

                                                let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                                let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "Null"
                                                let amount = commonerrorUserInfo!.value(forKey: "amount") ?? "Null"
                                                let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                                let method = commonerrorUserInfo!.value(forKey: "method") as! NSString
                                                let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                                let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                                let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                                let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"


                                                let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message"]
                                                let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message]

                                                let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                                print(errorDict)
                                                self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)
                                                if (message as! String) == "Null" {
                                                    message = gateway_message
                                                }

                                                self.customerCardno.text = ""
                                                self.customerName.text = ""
                                                self.cvvnumber.text = ""
                                                self.expireDate.text = ""
                                                self.state.text = ""
                                                self.city.text = ""
                                                self.stateAddress.text = ""
                                                self.zipCode.text = ""
                                                self.customerCardnoLabel.text = ""
                                                self.customerNameLabel.text = ""
                                                self.cvvnumberLabel.text = ""
                                                self.expireDateLabel.text = ""
                                                self.stateAddressLabel.text = ""
                                                self.cityLabel.text = ""
                                                self.stateLabel.text = ""
                                                self.zipcodeLabel.text = ""
                                                self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                                self.makePaymentButton.isUserInteractionEnabled = false
                                                self.alertForOffilne(message: message as! NSString)

                                            }
                                        }
                                        else
                                        {
                                            let gateWaymessage = dict?["gateway_message"] as! String
                                            //                            self.view.makeToast(gateWaymessage)
                                            self.hideLoader()

                                            let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                            print(commonerrorUserInfo)
                                            var transaction_status = NSString()
                                            var correlation_id = NSString()

                                            if commonerrorUserInfo!["transaction_status"] != nil {
                                                transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                            }
                                            else
                                            {
                                                transaction_status = ""

                                            }

                                            if commonerrorUserInfo!["correlation_id"] != nil {
                                                correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                            }
                                            else
                                            {
                                                correlation_id = ""

                                            }
                                            print(correlation_id)
                                            print(transaction_status)
                                            //bank_message
                                            let message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                            let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                            let avsString = commonerrorUserInfo!.value(forKey: "avs") ?? "Null"
                                            let cvv2String = commonerrorUserInfo!.value(forKey: "cvv2") ?? "Null"
                                            let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                            let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "USD"
                                            let amount = commonerrorUserInfo!.value(forKey: "amount") ?? "Null"
                                            let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                            let method = commonerrorUserInfo!.value(forKey: "method") ?? "Null"
                                            let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                            let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                            let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                            let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"


                                            let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message", "avs", "cvv2"]
                                            let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message, avsString, cvv2String]

                                            let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                            print(errorDict)
                                            self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)



                                            self.alertForOffilne(message: gateWaymessage as NSString)
                                            self.customerCardno.text = ""
                                            self.customerName.text = ""
                                            self.cvvnumber.text = ""
                                            self.expireDate.text = ""
                                            self.state.text = ""
                                            self.city.text = ""
                                            self.stateAddress.text = ""
                                            self.zipCode.text = ""
                                            self.customerCardnoLabel.text = ""
                                            self.customerNameLabel.text = ""
                                            self.cvvnumberLabel.text = ""
                                            self.expireDateLabel.text = ""
                                            self.stateAddressLabel.text = ""
                                            self.cityLabel.text = ""
                                            self.stateLabel.text = ""
                                            self.zipcodeLabel.text = ""
                                            self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                            self.makePaymentButton.isUserInteractionEnabled = false


                                        }
                                    }
                                    else
                                    {
                                        self.hideLoader()

                                        let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                        print(commonerrorUserInfo)
                                        var transaction_status = NSString()
                                        var correlation_id = NSString()

                                        if commonerrorUserInfo!["transaction_status"] != nil {
                                            transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                        }
                                        else
                                        {
                                            transaction_status = ""

                                        }

                                        if commonerrorUserInfo!["correlation_id"] != nil {
                                            correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                        }
                                        else
                                        {
                                            correlation_id = ""

                                        }
                                        print(correlation_id)
                                        print(transaction_status)
                                        //bank_message
                                        var message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                        let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                        let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                        let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "Null"
                                        let amount = commonerrorUserInfo!.value(forKey: "amount") ?? "Null"
                                        let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                        let method = commonerrorUserInfo!.value(forKey: "method") as! NSString
                                        let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                        let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                        let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                        let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"
                                        let avsString = commonerrorUserInfo!.value(forKey: "avs") ?? "Null"
                                        let cvv2String = commonerrorUserInfo!.value(forKey: "cvv2") ?? "Null"

                                        let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message", "avs", "cvv2"]
                                        let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message, avsString, cvv2String]

                                        let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                        print(errorDict)
                                        self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)

                                        if (message as! String) == "Null" {
                                            message = gateway_message


                                        }

                                        self.alertForOffilne(message: message as! NSString)
                                        self.customerCardno.text = ""
                                        self.customerName.text = ""
                                        self.cvvnumber.text = ""
                                        self.expireDate.text = ""
                                        self.state.text = ""
                                        self.city.text = ""
                                        self.stateAddress.text = ""
                                        self.zipCode.text = ""
                                        self.customerCardnoLabel.text = ""
                                        self.customerNameLabel.text = ""
                                        self.cvvnumberLabel.text = ""
                                        self.expireDateLabel.text = ""
                                        self.stateAddressLabel.text = ""
                                        self.cityLabel.text = ""
                                        self.stateLabel.text = ""
                                        self.zipcodeLabel.text = ""
                                        self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                        self.makePaymentButton.isUserInteractionEnabled = false
                                    }
                                } else {
                                    self.hideLoader()

                                    let errorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo["Error"] as? NSDictionary
                                    let commonerrorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo as? NSDictionary
                                    print(commonerrorUserInfo)
                                    print(errorUserInfo)
                                    var transaction_status = NSString()
                                    var correlation_id = NSString()

                                    if commonerrorUserInfo!["transaction_status"] != nil {
                                        transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                    }
                                    else
                                    {
                                        transaction_status = ""

                                    }
                                    if commonerrorUserInfo!["correlation_id"] != nil {
                                        correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                    }
                                    else
                                    {
                                        correlation_id = ""

                                    }

                                    print(correlation_id)
                                    print(transaction_status)

                                    var messageArray = NSArray()
                                    if errorUserInfo!["messages"] != nil {
                                        messageArray = errorUserInfo?.value(forKey: "messages") as! NSArray
                                    }
                                    else
                                    {
                                        messageArray = NSArray()
                                    }
                                    var descriptionAlertString = NSString()
                                    if messageArray.count > 0 || messageArray != nil {
                                        for index in 0..<messageArray.count {




                                            print(messageArray)
                                            let messageDict = messageArray.object(at: index) as! NSDictionary
                                            print(messageDict)
                                            let descriptionString = messageDict.value(forKey: "description") as! NSString
                                            let codeString = messageDict.value(forKey: "code") as! NSString
                                            authStatusMessage = "Error was encountered processing transaction: \(error.debugDescription.description)"


                                            let keys = ["correlation_id", "transaction_status", "description", "code", "transaction_id"]
                                            let values = [correlation_id, transaction_status, descriptionString, codeString, finalMercentID]

                                            let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))


                                            if index > 0
                                                {
                                                descriptionAlertString = "\(descriptionAlertString) and \(descriptionString) " as! NSString

                                            }
                                            else
                                            {
                                                descriptionAlertString = "\(descriptionString)" as! NSString
                                            }
                                            print(errorDict)
                                            self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)

                                        }
                                    }
                                    self.alertForOffilne(message: descriptionAlertString)
                                    self.customerCardno.text = ""
                                    self.customerName.text = ""
                                    self.cvvnumber.text = ""
                                    self.expireDate.text = ""
                                    self.state.text = ""
                                    self.city.text = ""
                                    self.stateAddress.text = ""
                                    self.zipCode.text = ""
                                    self.customerCardnoLabel.text = ""
                                    self.customerNameLabel.text = ""
                                    self.cvvnumberLabel.text = ""
                                    self.expireDateLabel.text = ""
                                    self.stateAddressLabel.text = ""
                                    self.cityLabel.text = ""
                                    self.stateLabel.text = ""
                                    self.zipcodeLabel.text = ""
                                    self.makePaymentButton.backgroundColor = UIColor.init(red: 228 / 255, green: 145 / 255, blue: 164 / 255, alpha: 1.0)
                                    self.makePaymentButton.isUserInteractionEnabled = false
                                }


                            }


                        }

                    }
                }

                func AlreadypurchaseVoidTransaction(storeData: Data) {
                    print(storeData)

                    let valueEntered = NSDecimalNumber(string: "20")

                    if !(valueEntered == NSDecimalNumber.notANumber) {


                        // Test credit card info
                        let credit_card = icredit_card

                        let myClient = PayeezySDK(apiKey: KApiKey, apiSecret: KApiSecret, merchantToken: KToken, url: PURL)
//print(credit_card)
                        var profileID = Int()
                        var finalMercentID = NSString()

                        let userDefaults1 = UserDefaults.standard
                        if userDefaults1.value(forKey: "userIdString") == nil
                            {


                            let accessToken = userDefaults1.value(forKey: "accessToken")

                            let getUserId = "\(APIUrl.ZIGTARCAPI)User/api/GetUserId?Token=\(accessToken!)"

                            Alamofire.request(getUserId, method: .post, parameters: nil, encoding: JSONEncoding.default)
                                .responseJSON { response in
                                    switch response.result {
                                    case .success:


                                        if (response.result.value != nil) {

                                            let responseVar = response.result.value
                                            let json = JSON(responseVar!)
                                            print(json.rawString()!)
                                            let useridString = json.rawString()! as! String
                                            let userid = Int(useridString)
                                            let userDefaults = UserDefaults.standard
                                            userDefaults.set(userid, forKey: "userIdString")
                                            var userIDString: Int?

                                            if userDefaults1.integer(forKey: "userIdString") != nil {
                                                userIDString = userDefaults1.integer(forKey: "userIdString")

                                            }
                                            else
                                            {
                                                userIDString = 0

                                            }
                                            let xInt: Int? = userIDString

                                            profileID = xInt!
                                            print("\(profileID)")
                                            let string0 = String(format: "%06d", profileID) // returns "00"
                                            print(string0)

                                            var tenDigitNumber: String {
                                                var result = ""
                                                repeat {
                                                    // Create a string with a random number 0...9999
                                                    let arcrandom = arc4random_uniform(1000000000)
                                                    result = String(format: "%10d", arcrandom)
                                                } while result.count < 10
                                                return result
                                            }
                                            print("\(tenDigitNumber)")
                                            let a: Int! = Int(self.noofTickets!)
                                            let ticketCount = String(format: "%d", a) // returns "00"
                                            var trimmedString = tenDigitNumber.trimmingCharacters(in: .whitespaces)
                                            print(trimmedString)
                                            if trimmedString.count > 8
                                                {
                                                trimmedString = String(trimmedString.prefix(8))

                                            }
                                            if self.LivepaymentBool == true
                                                {
                                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)0" as NSString

                                            }
                                            else
                                            {
                                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)1" as NSString

                                            }
                                            print("tenDigitNumber==>\(finalMercentID)")
                                            print("HELLOARUNNEWPAYMENT\(finalMercentID)")
                                            self.totalPrice = self.removeAllString(text: self.totalPrice!)

                                            myClient!.submitAuthorizePurchaseTransaction(withCreditCardDetails: (credit_card!["cvv"] as! String), cardExpMMYY: (credit_card!["exp_date"] as! String), cardNumber: self.fdTokenValue, cardHolderName: (credit_card!["cardholder_name"] as! String), cardType: (credit_card!["type"] as! String), currencyCode: "USD", totalAmount: self.totalPrice, merchantRef: finalMercentID as String, transactionType: "purchase", token_type: "FDToken", method: "token") { dict, error in

                                                var authStatusMessage: String? = nil
                                                dump(dict)
                                                if error == nil {
                                                    self.hideLoader()

                                                    if let object = dict?["transaction_type"], let object1 = dict?["transaction_id"], let object2 = dict?["transaction_tag"], let object3 = dict?["correlation_id"], let object4 = dict?["bank_resp_code"] {
                                                        authStatusMessage = "Transaction Successful\rType:\(object)\rTransaction ID:\(object1)\rTransaction Tag:\(object2)\rCorrelation Id:\(object3)\rBank Response Code:\(object4)"
                                                        print(storeData)
                                                        if storeData.isEmpty == false
                                                            {
                                                            if self.savePaymentBool == true
                                                                {
                                                                let defaults = UserDefaults.standard
                                                                defaults.set(storeData, forKey: "SavedDictionary")
                                                            }

                                                        }
                                                        self.postPaymentDetailApi(paymentDict: dict as! NSDictionary, mercentID: finalMercentID)
                                                        let transaction_id = dict!["transaction_id"] as! NSString

                                                        self.buyTicketAction(ticetDict: dict! as NSDictionary, TokenID: finalMercentID, TransactionId: transaction_id)
                                                    }
                                                    else
                                                    {
                                                        self.hideLoader()

                                                        let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                                        print(commonerrorUserInfo)
                                                        var transaction_status = NSString()
                                                        var correlation_id = NSString()

                                                        if commonerrorUserInfo!["transaction_status"] != nil {
                                                            transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                                        }
                                                        else
                                                        {
                                                            transaction_status = ""

                                                        }

                                                        if commonerrorUserInfo!["correlation_id"] != nil {
                                                            correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                                        }
                                                        else
                                                        {
                                                            correlation_id = ""

                                                        }
                                                        print(correlation_id)
                                                        print(transaction_status)
                                                        //bank_message
                                                        var message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                        let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                                        let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                                        let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "Null"
                                                        let amount = commonerrorUserInfo!.value(forKey: "amount") as! NSString
                                                        let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                                        let method = commonerrorUserInfo!.value(forKey: "method") ?? "Null"
                                                        let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                                        let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                                        let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                                        let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"

                                                        let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message", "avs", "cvv2"]
                                                        let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message, "", ""]

                                                        let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                                        print(errorDict)
                                                        self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)


                                                        if (message as! String) == "Null" {
                                                            message = gateway_message
                                                        }

                                                        self.alertForOffilne(message: message as! NSString)
                                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                        let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)



                                                    }
                                                } else {
                                                    self.hideLoader()

                                                    let errorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo["Error"] as? NSDictionary
                                                    let commonerrorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo as? NSDictionary

                                                    var transaction_status = NSString()
                                                    var correlation_id = NSString()

                                                    if commonerrorUserInfo!["transaction_status"] != nil {
                                                        transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                                    }
                                                    else
                                                    {
                                                        transaction_status = ""

                                                    }
                                                    if commonerrorUserInfo!["correlation_id"] != nil {
                                                        correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                                    }
                                                    else
                                                    {
                                                        correlation_id = ""

                                                    }

                                                    print(correlation_id)
                                                    print(transaction_status)
                                                    var messageArray = NSArray()
                                                    if errorUserInfo!["messages"] != nil {
                                    messageArray = errorUserInfo?.value(forKey: "messages") as! NSArray
                                                    }
                                                    else
                                                    {
                                                        messageArray = NSArray()
                                                    }
                                                    var descriptionAlertString = NSString()
                                                    if messageArray.count > 0 || messageArray != nil {
                                                        for index in 0..<messageArray.count {




                                                            print(messageArray)
                                                            let messageDict = messageArray.object(at: index) as! NSDictionary
                                                            print(messageDict)
                                                            let descriptionString = messageDict.value(forKey: "description") as! NSString
                                                            let codeString = messageDict.value(forKey: "code") as! NSString
                                                            //finalMercentID
                                                            authStatusMessage = "Error was encountered processing transaction: \(error.debugDescription.description)"


                                                            let keys = ["correlation_id", "transaction_status", "description", "code", "transaction_id"]
                                                            let values = [correlation_id, transaction_status, descriptionString, codeString, finalMercentID]

                                                            let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                                            if index > 0
                                                                {
                                                                descriptionAlertString = "\(descriptionAlertString) and \(descriptionString) " as! NSString

                                                            }
                                                            else
                                                            {
                                                                descriptionAlertString = "\(descriptionString)" as! NSString
                                                            }
                                                            print(errorDict)
                                                            self.postfailedPaymentDetailApi(paymentDict: errorDict as NSDictionary, mercentID: finalMercentID)

                                                        }
                                                    }

                                                    self.alertForOffilne(message: descriptionAlertString)
                                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                    let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                    self.navigationController!.pushViewController(tripPlannerViewController, animated: true)



                                                }


                                            }



                                        }
                                    case .failure(let error):
                                        print(error)

                                        //
                                        var newPriceString = String()
                                        newPriceString = (self.removeString(text: self.ticketPrice!) as NSString) as String

                                        let count: Double? = Double(self.noofTickets!) // firstText is UITextField
                                        let priceValue: Double? = Double(newPriceString)!
                                        let totalValue = priceValue! * count!
                                        print(totalValue)

                                        print("\(self.ticketType)")

                                        let priceString = String(format: "%.2f", priceValue!)
                                        let totalPriceString = String(format: "%.2f", totalValue)
                                        print("\(priceString)")
                                        print("\(totalPriceString)")
                                        var pref = UserDefaults.standard
                                        self.addTickeValue.removeAllObjects()

                                        let BuyTicket_Amount = priceString
                                        let BuyTicket_TripId = "4"
                                        let BuyTicket_RouteId = self.ticketType!
                                        let BuyTicket_ProfileId = (pref.object(forKey: "ProfileID")) as? Int
                                        let BuyTicket_FromAddress = "S 6th @ W Liberty"
                                        let BuyTicket_DestinationAddress = "4th @ Ormsby"
                                        let a: Int! = Int(self.noofTickets!) // firstText is UITextField

                                        for i in 0 ..< a {

                                            let Addticketlist: NSMutableDictionary = [:]
                                            //                    print(BuyTicket_ProfileId)
                                            //                    print(fareID)
                                            Addticketlist.setValue("\((BuyTicket_Amount))", forKey: "Amount")
                                            Addticketlist.setValue("\((BuyTicket_RouteId))", forKey: "RouteId")
                                            Addticketlist.setValue("\((BuyTicket_TripId))", forKey: "TripId")
                                            Addticketlist.setValue(BuyTicket_ProfileId, forKey: "ProfileId")
                                            Addticketlist.setValue("\((BuyTicket_FromAddress))", forKey: "FromAddress")
                                            Addticketlist.setValue("\((BuyTicket_DestinationAddress))", forKey: "DestinationAddress")
                                            Addticketlist.setValue(self.fareID, forKey: "Fareid")




                                            self.addTickeValue.add(Addticketlist)
                                        }
                                        print("\(self.addTickeValue)")
                                        self.showLoader()
                                        let Timestamp = Date()
                                        let dateFormatter = DateFormatter()
                                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                                        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
                                        let utctimeStamp = dateFormatter.string(from: Timestamp)

                                        let headers = [
                                            "Content-Type": "application/json",
                                        ]



                                        var fourDigitNumber: String {
                                            var result = ""
                                            repeat {
                                                // Create a string with a random number 0...9999
                                                result = String(format: "%05d", arc4random_uniform(1000000))
                                            } while result.count < 6
                                            return result
                                        }
                                        var usernameString = NSString()
                                        var emailString = NSString()
                                        var phoneString = NSString()

                                        let userDefaults1 = UserDefaults.standard

                                        if userDefaults1.value(forKey: "userName") == nil
                                            {
                                            usernameString = ""
                                        }
                                        else
                                        {
                                            usernameString = userDefaults1.value(forKey: "userName") as! NSString
                                        }
                                        if userDefaults1.value(forKey: "userEmail") == nil
                                            {
                                            emailString = ""
                                        }
                                        else
                                        {
                                            emailString = userDefaults1.value(forKey: "userEmail") as! NSString
                                        }

                                        if userDefaults1.value(forKey: "phoneNumber") == nil
                                            {
                                            phoneString = ""
                                        }
                                        else
                                        {
                                            phoneString = userDefaults1.value(forKey: "phoneNumber") as! NSString
                                        }


                                        let SendFailedMailbody: [String: Any] = [
                                            "To": "isaac@zeddigital.net",
                                            "Subject": "Three API Failed",
                                            "Content": "Userid Not available",
                                            "Cc": "",
                                            "Bcc": "",
                                            "From": "iOS Support",
                                            "emailfrom": "info@ridetarc.org",
                                        ]
                                        let Sendbody: [String: Any] = [
                                            "Subject": "Userid Not available",
                                            "Username": usernameString,
                                            "Emailid": emailString,
                                            "Phoneno": phoneString,
                                            "Profileid": BuyTicket_ProfileId!,
                                            "Ticketcount": self.noofTickets!,
                                            "Transactionid": "",
                                            "Txn_referenceno": "",
                                            "Tickettype": self.ticketType!,
                                            "Token": pref.object(forKey: "accessToken")!,
                                            "Totalamount": totalPriceString,
                                            "Transactiondate": utctimeStamp,
                                            "Content": "",
                                        ]
                                        Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                            .responseJSON { response in
                                                switch response.result {
                                                case .success:


                                                    if (response.result.value != nil) {

                                                        let responseVar = response.result.value
                                                        let json = JSON(responseVar!)
                                                        print(json)
                                                        if json["Message"] == "Ok" {


                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")

                                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                            let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)


                                                            self.hideLoader()


                                                        } else {

                                                            self.view.makeToast("\(json["Message"])")
                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")

                                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                            let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                            self.hideLoader()
                                                        }

                                                    } else {
                                                        self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                                                                                                          self.view.makeToast("Please Try Again")
//                                                                                                                                          self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                                                                                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                                                                                                        let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                                                                                                        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                                                                                                           self.hideLoader()
                                                    }
                                                case .failure(let error):
                                                    print(error)
                                                    self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                                                                                                      self.view.makeToast("Please Try Again!")
//                                                                                                                                      self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                                                                                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                                                                                                    let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                                                                                                    self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                                                                                                       self.hideLoader()
                                                }
                                        }


                                        self.hideLoader()
                                    }
                            }



                        }
                        else
                        {
                            var userIDString: Int?

                            if userDefaults1.integer(forKey: "userIdString") != nil {
                                userIDString = userDefaults1.integer(forKey: "userIdString")

                            }
                            else
                            {
                                userIDString = 0

                            }
                            let xInt: Int? = userIDString

                            profileID = xInt!
                            print("\(profileID)")
                            let string0 = String(format: "%06d", profileID) // returns "00"
                            print(string0)

                            var tenDigitNumber: String {
                                var result = ""
                                repeat {
                                    // Create a string with a random number 0...9999
                                    let arcrandom = arc4random_uniform(1000000000)
                                    result = String(format: "%10d", arcrandom)
                                } while result.count < 10
                                return result
                            }
                            print("\(tenDigitNumber)")
                            let a: Int! = Int(self.noofTickets!)
                            let ticketCount = String(format: "%d", a) // returns "00"
                            var trimmedString = tenDigitNumber.trimmingCharacters(in: .whitespaces)
                            print(trimmedString)
                            if trimmedString.count > 8
                                {
                                trimmedString = String(trimmedString.prefix(8))

                            }
                            if trimmedString.count <= 7
                            {
                                let randomInt = Int.random(in: 1..<9)
                                print(randomInt)
                                 let string0 = "\(trimmedString)\(randomInt)"
                                trimmedString = string0
                            }
                            if LivepaymentBool == true
                                {
                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)0" as NSString

                            }
                            else
                            {
                                finalMercentID = "9\(string0)\(ticketCount)\(trimmedString)1" as NSString

                            }
                            print("tenDigitNumber==>\(finalMercentID)")

                        }
                        print("HELLOARUNNEWPAYMENT\(finalMercentID)")
                        totalPrice = removeAllString(text: totalPrice!)

                        myClient!.submitAuthorizePurchaseTransaction(withCreditCardDetails: (credit_card!["cvv"] as! String), cardExpMMYY: (credit_card!["exp_date"] as! String), cardNumber: fdTokenValue, cardHolderName: (credit_card!["cardholder_name"] as! String), cardType: (credit_card!["type"] as! String), currencyCode: "USD", totalAmount: totalPrice, merchantRef: finalMercentID as String, transactionType: "purchase", token_type: "FDToken", method: "token") { dict, error in

                            var authStatusMessage: String? = nil
                            dump(dict)
                            if error == nil {
                                self.hideLoader()

                                if let object = dict?["transaction_type"], let object1 = dict?["transaction_id"], let object2 = dict?["transaction_tag"], let object3 = dict?["correlation_id"], let object4 = dict?["bank_resp_code"] {
                                    authStatusMessage = "Transaction Successful\rType:\(object)\rTransaction ID:\(object1)\rTransaction Tag:\(object2)\rCorrelation Id:\(object3)\rBank Response Code:\(object4)"
                                    print(storeData)
                                    if storeData.isEmpty == false
                                        {
                                        if self.savePaymentBool == true
                                            {
                                            let defaults = UserDefaults.standard
                                            defaults.set(storeData, forKey: "SavedDictionary")
                                        }

                                    }
                                    self.postPaymentDetailApi(paymentDict: dict as! NSDictionary, mercentID: finalMercentID)
                                    let transaction_id = dict!["transaction_id"] as! NSString

                                    self.buyTicketAction(ticetDict: dict! as NSDictionary, TokenID: finalMercentID, TransactionId: transaction_id)
                                }
                                else
                                {
                                    self.hideLoader()

                                    let commonerrorUserInfo: NSDictionary? = dict as? NSDictionary
                                    print(commonerrorUserInfo)
                                    var transaction_status = NSString()
                                    var correlation_id = NSString()

                                    if commonerrorUserInfo!["transaction_status"] != nil {
                                        transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                    }
                                    else
                                    {
                                        transaction_status = ""

                                    }

                                    if commonerrorUserInfo!["correlation_id"] != nil {
                                        correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                    }
                                    else
                                    {
                                        correlation_id = ""

                                    }
                                    print(correlation_id)
                                    print(transaction_status)
//bank_message
                                    var message = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                    let descriptionString = commonerrorUserInfo!.value(forKey: "bank_message") ?? "Null"
                                    let codeString = commonerrorUserInfo!.value(forKey: "bank_resp_code") ?? "Null"
                                    let currency = commonerrorUserInfo!.value(forKey: "currency") ?? "Null"
                                    let amount = commonerrorUserInfo!.value(forKey: "amount") as! NSString
                                    let transaction_tag = commonerrorUserInfo!.value(forKey: "transaction_tag") ?? "Null"

                                    let method = commonerrorUserInfo!.value(forKey: "method") ?? "Null"
                                    let gateway_resp_code = commonerrorUserInfo!.value(forKey: "gateway_resp_code") ?? "Null"
                                    let validation_status = commonerrorUserInfo!.value(forKey: "validation_status") ?? "Null"
                                    let transaction_type = commonerrorUserInfo!.value(forKey: "transaction_type") ?? "Null"
                                    let gateway_message = commonerrorUserInfo!.value(forKey: "gateway_message") ?? "Null"

                                    let keys = ["correlation_id", "transaction_status", "description", "code", "currency", "amount", "transaction_tag", "method", "gateway_resp_code", "validation_status", "transaction_type", "gateway_message", "avs", "cvv2"]
                                    let values = [correlation_id, transaction_status, descriptionString, codeString, currency, amount, transaction_tag, method, gateway_resp_code, validation_status, transaction_type, gateway_message, "", ""]

                                    let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                    print(errorDict)
                                    self.postfailedPaymentDetailApi(paymentDict: errorDict as! NSDictionary, mercentID: finalMercentID)


                                    if (message as! String) == "Null" {
                                        message = gateway_message
                                    }

                                    self.alertForOffilne(message: message as! NSString)
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                    self.navigationController!.pushViewController(tripPlannerViewController, animated: true)



                                }
                            } else {
                                self.hideLoader()

                                let errorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo["Error"] as? NSDictionary
                                let commonerrorUserInfo: NSDictionary? = ((error as Any) as! NSError).userInfo as? NSDictionary

                                var transaction_status = NSString()
                                var correlation_id = NSString()

                                if commonerrorUserInfo!["transaction_status"] != nil {
                                    transaction_status = commonerrorUserInfo!.value(forKey: "transaction_status") as! NSString

                                }
                                else
                                {
                                    transaction_status = ""

                                }
                                if commonerrorUserInfo!["correlation_id"] != nil {
                                    correlation_id = commonerrorUserInfo!.value(forKey: "correlation_id") as! NSString

                                }
                                else
                                {
                                    correlation_id = ""

                                }

                                print(correlation_id)
                                print(transaction_status)
                                var messageArray = NSArray()
                                if errorUserInfo!["messages"] != nil {
                                    messageArray = errorUserInfo?.value(forKey: "messages") as! NSArray
                                }
                                else
                                {
                                    messageArray = NSArray()
                                }
                                var descriptionAlertString = NSString()
                                if messageArray.count > 0 || messageArray != nil {
                                    for index in 0..<messageArray.count {




                                        print(messageArray)
                                        let messageDict = messageArray.object(at: index) as! NSDictionary
                                        print(messageDict)
                                        let descriptionString = messageDict.value(forKey: "description") as! NSString
                                        let codeString = messageDict.value(forKey: "code") as! NSString
//finalMercentID
                                        authStatusMessage = "Error was encountered processing transaction: \(error.debugDescription.description)"


                                        let keys = ["correlation_id", "transaction_status", "description", "code", "transaction_id"]
                                        let values = [correlation_id, transaction_status, descriptionString, codeString, finalMercentID]

                                        let errorDict = Dictionary(uniqueKeysWithValues: zip(keys, values))

                                        if index > 0
                                            {
                                            descriptionAlertString = "\(descriptionAlertString) and \(descriptionString) " as! NSString

                                        }
                                        else
                                        {
                                            descriptionAlertString = "\(descriptionString)" as! NSString
                                        }
                                        print(errorDict)
                                        self.postfailedPaymentDetailApi(paymentDict: errorDict as NSDictionary, mercentID: finalMercentID)

                                    }
                                }

                                self.alertForOffilne(message: descriptionAlertString)
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                self.navigationController!.pushViewController(tripPlannerViewController, animated: true)



                            }


                        }
                    }
                }
                func testConvert(_ something: Any) {
                    guard let dict = something as? [AnyHashable: Any] else {
                        print("\(something) couldn't be converted to Dictionary")
                        return
                    }
                    print("\(something) successfully converted to Dictionary")
                }
                func convertToDictionary(text: String) -> [String: Any]? {
                    if let data = text.data(using: .utf8) {
                        do {
                            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                    return nil
                }
                func alertForOffilne(message: NSString)
                {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)

                    alertView.addButton("Okay") {
                        //ComValue.internetalertstatus = true

                    }

                    alertView.showError("Payment failed", subTitle: message as String)
                }
                func alertForOffilneTicket(Title: String, message: NSString)
                {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)

                    alertView.addButton("Okay") {
                        //ComValue.internetalertstatus = true

                    }

                    alertView.showError(Title, subTitle: message as String)
                }


                func buyTicketAction(ticetDict: NSDictionary, TokenID: NSString, TransactionId: NSString)
                {


                    var newPriceString = String()
                    newPriceString = (removeString(text: ticketPrice!) as NSString) as String

                    let count: Double? = Double(noofTickets!) // firstText is UITextField
                    let priceValue: Double? = Double(newPriceString)!
                    let totalValue = priceValue! * count!
                    print(totalValue)

                    print("\(ticketType)")

                    let priceString = String(format: "%.2f", priceValue!)
                    let totalPriceString = String(format: "%.2f", totalValue)
                    print("\(priceString)")
                    print("\(totalPriceString)")
                    var pref = UserDefaults.standard
                    addTickeValue.removeAllObjects()

                    let BuyTicket_Amount = priceString
                    let BuyTicket_TripId = "4"
                    let BuyTicket_RouteId = ticketType!
                    let BuyTicket_ProfileId = (pref.object(forKey: "ProfileID")) as? Int
                    let BuyTicket_FromAddress = "S 6th @ W Liberty"
                    let BuyTicket_DestinationAddress = "4th @ Ormsby"
                    let a: Int! = Int(self.noofTickets!) // firstText is UITextField

                    for i in 0 ..< a {

                        let Addticketlist: NSMutableDictionary = [:]
                        //                    print(BuyTicket_ProfileId)
                        //                    print(fareID)
                        Addticketlist.setValue("\((BuyTicket_Amount))", forKey: "Amount")
                        Addticketlist.setValue("\((BuyTicket_RouteId))", forKey: "RouteId")
                        Addticketlist.setValue("\((BuyTicket_TripId))", forKey: "TripId")
                        Addticketlist.setValue(BuyTicket_ProfileId, forKey: "ProfileId")
                        Addticketlist.setValue("\((BuyTicket_FromAddress))", forKey: "FromAddress")
                        Addticketlist.setValue("\((BuyTicket_DestinationAddress))", forKey: "DestinationAddress")
                        Addticketlist.setValue(fareID, forKey: "Fareid")




                        addTickeValue.add(Addticketlist)
                    }
                    print("\(addTickeValue)")
                    self.showLoader()
                    let Timestamp = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    dateFormatter.timeZone = TimeZone(abbreviation: "EST")
                    let utctimeStamp = dateFormatter.string(from: Timestamp)

                    let headers = [
                        "Content-Type": "application/json",
                    ]



                    var fourDigitNumber: String {
                        var result = ""
                        repeat {
                            // Create a string with a random number 0...9999
                            result = String(format: "%05d", arc4random_uniform(1000000))
                        } while result.count < 6
                        return result
                    }
                    var usernameString = NSString()
                    var emailString = NSString()
                    var phoneString = NSString()

                    let userDefaults1 = UserDefaults.standard

                    if userDefaults1.value(forKey: "userName") == nil
                        {
                        usernameString = ""
                    }
                    else
                    {
                        usernameString = userDefaults1.value(forKey: "userName") as! NSString
                    }
                    if userDefaults1.value(forKey: "userEmail") == nil
                        {
                        emailString = ""
                    }
                    else
                    {
                        emailString = userDefaults1.value(forKey: "userEmail") as! NSString
                    }

                    if userDefaults1.value(forKey: "phoneNumber") == nil
                        {
                        phoneString = ""
                    }
                    else
                    {
                        phoneString = userDefaults1.value(forKey: "phoneNumber") as! NSString
                    }

                    let Sendbody: [String: Any] = [
                        "Subject": "Payment Success. But Ticket Generate Failed",
                        "Username": usernameString,
                        "Emailid": emailString,
                        "Phoneno": phoneString,
                        "Profileid": BuyTicket_ProfileId!,
                        "Ticketcount": noofTickets!,
                        "Transactionid": TransactionId,
                        "Txn_referenceno": TokenID,
                        "Tickettype": ticketType!,
                        "Token": pref.object(forKey: "accessToken")!,
                        "Totalamount": totalPriceString,
                        "Transactiondate": utctimeStamp,
                        "Content": "",

                    ]

                    let SendFailedMailbody: [String: Any] = [
                        "To": "isaac@zeddigital.net",
                        "Subject": "Three API Failed",
                        "Content": "\(TokenID),\(TransactionId)",
                        "Cc": "",
                        "Bcc": "",
                        "From": "iOS Support",
                        "emailfrom": "info@ridetarc.org",


                    ]

                    // JSON Body
                    let body: [String: Any] = [
                        "Token": pref.object(forKey: "accessToken")!,
                        "FromAddress": "Louisville, KY, USA",
                        "DestinationAddress": "1257 S 3rd St, Louisville, KY 40203, USA",
                        "TotalAmount": totalPriceString,
                        "PlaceId": "45",
                        "TransactionDate": utctimeStamp,
                        "Tickets": addTickeValue,
                        "TransactionId": TokenID,
                        "Message": "Ticket Take in IOS App"
                    ]
                    print(body)
                    print(APIUrl.BuyTicket)
                    if isReachable()
                        {

                        Alamofire.request(APIUrl.BuyTicket, method: .post, parameters: body, encoding: JSONEncoding.default)
                            .responseJSON { response in
                                switch response.result {
                                case .success:

                                    //shinychange
                                    if (response.result.value != nil) {

                                        let responseVar = response.result.value
                                        let json = JSON(responseVar!)
                                        print(json)
                                        if json["Message"] == "Ok" {


                                            TicketSave.TicketDataUpload(CountForRow: 1, Expiry: false, completion: { (SuccessMessage, Count) in

                                                if SuccessMessage {
                                                    self.hideLoader()
                                                    let appearance = SCLAlertView.SCLAppearance(
                                                        showCloseButton: false
                                                    )
                                                    // self.postPaymentDetailApi(paymentDict: ticetDict)
                                                    var alertView = SCLAlertView(appearance: appearance)

                                                    alertView.addButton("Close") {
                                                        // self.checkLocationwithoutLogin()
                                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                        let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
                                                        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)

                                                    }
                                                    alertView.showSuccess(" PAYMENT SUCCESS", subTitle: "Thank you for purchasing tickets")



                                                }
                                                else {



                                                    // self.alertForOffilne(message: "Ticket Screen Can't load. Try again!")

                                                    self.alertForOffilneTicket(Title: "Ticket screen cannot be loaded", message: "Your payment is successful, but ticket screen cannot be loaded. Please reopen the app and try again!  ")

                                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                    let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                    self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                    self.hideLoader()


                                                    //  self.view.makeToast("Thank you for purchasing tickets!", position: .top)


                                                    // self.view.makeToast("Thank you for Buying Ticket! This is pilot Version only July 1 - July 14", duration: 5)
                                                }
                                                // self.hideLoader()
                                            })





                                        } else {
                                            //
                                            //  self.view.makeToast("\(json["Message"])")

                                            // send mail
                                            Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                                .responseJSON { response in
                                                    switch response.result {
                                                    case .success:


                                                        if (response.result.value != nil) {

                                                            let responseVar = response.result.value
                                                            let json = JSON(responseVar!)
                                                            print(json)
                                                            if json["Message"] == "Ok" {


                                                                self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")

                                                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                                self.navigationController!.pushViewController(tripPlannerViewController, animated: true)


                                                                self.hideLoader()


                                                            } else {
                                                                self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                                self.view.makeToast("\(json["Message"])")
//                                                                self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                              let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                              let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                              self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                            self.hideLoader()
                                                            }

                                                        } else {
                                                            self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                            self.view.makeToast("Please Try Again")
//                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                          let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                          let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                          self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                             self.hideLoader()
                                                        }
                                                    case .failure(let error):
                                                        print(error)
                                                        self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//
//                                                        self.view.makeToast("Please Try Again!")
//                                                        self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                      let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                      let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                      self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                         self.hideLoader()
                                                    }
                                            }

                                        }

                                    } else {

                                        // self.view.makeToast("Please Try Again")

                                        // send mail
                                        Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                            .responseJSON { response in
                                                switch response.result {
                                                case .success:


                                                    if (response.result.value != nil) {

                                                        let responseVar = response.result.value
                                                        let json = JSON(responseVar!)
                                                        print(json)
                                                        //shinychange
                                                        if json["Message"] == "Ok" {

                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")

                                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                            let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)


                                                            self.hideLoader()


                                                        } else {
                                                            self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                            self.view.makeToast("\(json["Message"])")
//                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                          let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                          let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                          self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                         self.hideLoader()
                                                        }

                                                    } else {
                                                        self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//                                                        self.view.makeToast("Please Try Again")
//                                                        self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                      let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                      let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                      self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//
//                                                     self.hideLoader()
                                                    }
                                                case .failure(let error):
                                                    print(error)
                                                    self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
//
//                                                    self.view.makeToast("Please Try Again!")
//                                                    self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")
//
//                                                                                                  let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                                                                                  let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//                                                                                                  self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
//                                                     self.hideLoader()
                                                }
                                        }

                                    }
                                case .failure(let error):
                                    print(error)

                                    //


                                    // send mail
                                    Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                        .responseJSON { response in
                                            switch response.result {
                                            case .success:


                                                if (response.result.value != nil) {

                                                    let responseVar = response.result.value
                                                    let json = JSON(responseVar!)
                                                    print(json)
                                                    if json["Message"] == "Ok" {

                                                        self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")

                                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                        let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                        self.hideLoader()



                                                    } else {
                                                        self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                        //  self.view.makeToast("\(json["Message"])")

//                                               self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")




                                                    }

                                                } else {
                                                    self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                    // self.view.makeToast("Please Try Again")
//                                                    self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")




                                                }
                                            case .failure(let error):
                                                print(error)
                                                self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)

                                                //self.view.makeToast("Please Try Again!")
//                                                self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")



                                            }
                                    }
                                    // self.view.makeToast("Please Try Again!")

                                }
                        }

                    }
                    else
                    {
                        self.hideLoader()

                        let userdefaults = UserDefaults.standard
                        userdefaults.set(Sendbody, forKey: "SaveLocalMail")
                        userdefaults.set(SendFailedMailbody, forKey: "SaveLocalFailedMail")

                        timerEmail = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(offlineCard), userInfo: nil, repeats: true)


                        offlineDesign()
                    }
                }
                func stopTimerTest() {
                    print("OFFLINE==>Invalid")
                    if self.timerEmail != nil {
                        self.timerEmail!.invalidate()
                        self.timerEmail = nil
                    }


                }
                func sendEmailFailed(sendMailedparameter: [String: Any])
                {

                    if isReachable()
                        {
                        Alamofire.request(APIUrl.sendEmailFailureMail, method: .post, parameters: sendMailedparameter, encoding: JSONEncoding.default)
                            .responseJSON { response in
                                switch response.result {
                                case .success:


                                    if (response.result.value != nil) {

                                        let responseVar = response.result.value
                                        let json = JSON(responseVar!)
                                        print(json)
                                        //  if json["Message"] == "Ok" {
                                        self.hideLoader()

                                        self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")

                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)




//                        }else{
//
//                            print("OFFLINE==>failure")
//
//                        }

                                    } else {

                                        print("OFFLINE==>failure")

                                    }
                                case .failure(let error):
                                    print(error)
                                    print("OFFLINE==>OFFLINE")

                                }
                        }
                    }
                }
                @objc func offlineCard()
                {
                    let userdefaults = UserDefaults.standard
                    print("OFFLINE==>ENTER")
                    let returnValue: [String: Any] = (UserDefaults.standard.object(forKey: "SaveLocalMail") as? [String: Any])!
                    let failedreturnValue: [String: Any] = (UserDefaults.standard.object(forKey: "SaveLocalFailedMail") as? [String: Any])!
                    if isReachable()
                        {
                        Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: returnValue, encoding: JSONEncoding.default)
                            .responseJSON { response in
                                switch response.result {
                                case .success:


                                    if (response.result.value != nil) {

                                        let responseVar = response.result.value
                                        let json = JSON(responseVar!)
                                        print(json)
                                        if json["Message"] == "Ok" {
                                            print("OFFLINE==>SUCCESS")
                                            UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")

                                            UserDefaults.standard.removeObject(forKey: "SaveLocalMail")
                                            self.stopTimerTest()
                                            self.alertForOffilneTicket(Title: "Ticket Not Generated ", message: "Your payment is successful, but ticket has not been generated. Please contact Support. ")

                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)





                                        } else {

                                            print("OFFLINE==>failure")
                                            self.stopTimerTest()

                                            self.sendEmailFailed(sendMailedparameter: failedreturnValue)
                                            UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")

                                        }

                                    } else {

                                        print("OFFLINE==>failure")
                                        self.stopTimerTest()

                                        self.sendEmailFailed(sendMailedparameter: failedreturnValue)
                                        UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")

                                    }
                                case .failure(let error):
                                    print(error)
                                    print("OFFLINE==>OFFLINE")
                                    self.stopTimerTest()

                                    self.sendEmailFailed(sendMailedparameter: failedreturnValue)
                                    UserDefaults.standard.removeObject(forKey: "SaveLocalFailedMail")

                                }
                        }
                    }
                }

                func offlineDesign()
                {
                    let appearance = SCLAlertView.SCLAppearance(

                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Internet Setting") {
                        // ComValue.internetalertstatus = true

                        //                let window = UIApplication.shared.keyWindow!
                        //
                        //                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        //                let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
                        //
                        //
                        //                var rootViewController = window.rootViewController as! UINavigationController
                        //                rootViewController.pushViewController(tripPlannerViewController, animated: true)

                    }
                    alertView.addButton("No") {
                        //ComValue.internetalertstatus = true
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }

                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                // Checking for setting is opened or not
                                print("Setting is opened: \(success)")
                            })
                        }
                    }

                    alertView.showInfo("No Internet Connection", subTitle: "Please enable Internet access for TARC in your iOS settings")
                    //        }
                    //            else
                    //            {
                    //                print("*******OFfline ticket purchase*******")
                    //            }
                }
                func postPaymentDetailApi(paymentDict:NSDictionary,mercentID:NSString)
                    {
                        // JSON Body
                        print(paymentDict)
                        var retrieval_ref_no = NSString()
                        var cvv2 = NSString()
                        var amountString = NSString()
                        var transactionTypeString = NSString()
                        var currency = NSString()
                        var transaction_id = NSString()
                        var correlation_id = NSString()
                        var bank_message = NSString()
                        var gateway_resp_code = NSString()
                        var transaction_status = NSString()
                        var bank_resp_code = NSString()
                        var method = NSString()
                              var transaction_tag = NSString()
                              var validation_status = NSString()
                              var gateway_message = NSString()
                       var avs = NSString()

                        if paymentDict["amount"] != nil
                        {
                         amountString = paymentDict["amount"] as! NSString
                        }
                        else
                        {
                           amountString = ""
                        }
                        
                        
                        if paymentDict["transaction_type"] != nil
                        {
                         transactionTypeString = paymentDict["transaction_type"] as! NSString
                        }
                        else
                        {
                            transactionTypeString = ""
                        }
                        
                        
                        if paymentDict["currency"] != nil
                        {
                         currency = paymentDict["currency"] as! NSString
                        }
                        else
                        {
                            currency = ""
                        }
                        
                        
                        if paymentDict["transaction_id"] != nil
                        {
                         transaction_id = paymentDict["transaction_id"] as! NSString
                        }
                        else
                        {
                            transaction_id = ""
                        }
                        
                        
                        if paymentDict["correlation_id"] != nil
                        {
                         correlation_id = paymentDict["correlation_id"] as! NSString
                        }
                        else
                        {
                            correlation_id = ""
                        }
                        
                        
                        if paymentDict["bank_message"] != nil
                        {
                         bank_message = paymentDict["bank_message"] as! NSString
                            if bank_message == "Do Not Honor"{
                                bank_message = "Do Not Honor. Please have the cardholder contact their Issuer for additional information."
                            }
                        }
                        else
                        {
                            bank_message = ""
                        }
                        
                        
                        if paymentDict["transaction_status"] != nil
                        {
                         transaction_status = paymentDict["transaction_status"] as! NSString
                        }
                        else
                        {
                            transaction_status = ""
                        }
                        
                        if paymentDict["gateway_resp_code"] != nil
                        {
                         gateway_resp_code = paymentDict["gateway_resp_code"] as! NSString
                        }
                        else
                        {
                            gateway_resp_code = ""
                        }
                        
                        if paymentDict["retrieval_ref_no"] != nil
                        {
                            retrieval_ref_no = paymentDict["retrieval_ref_no"] as! NSString
                        }
                        else
                        {
                           retrieval_ref_no = ""
                        }
                       
                        if paymentDict["bank_resp_code"] != nil
                        {
                         bank_resp_code = paymentDict["bank_resp_code"] as! NSString
                        }
                        else
                        {
                           bank_resp_code = ""
                        }
                        
                      

                        if paymentDict["validation_status"] != nil
                               {
                         validation_status = paymentDict["validation_status"] as! NSString
                        }
                        else
                        {
                            validation_status = ""
                        }
                        if paymentDict["gateway_message"] != nil
                               {
                         gateway_message = paymentDict["gateway_message"] as! NSString
                        }
                       else
                        {
                            gateway_message = ""
                        }

                        if paymentDict["method"] != nil
                               {
                         method = paymentDict["method"] as! NSString
                        }
                        else
                        {
                            method = ""
                        }
                        if paymentDict["transaction_tag"] != nil
                               {
                         transaction_tag = paymentDict["transaction_tag"] as! NSString
                        
                        }
                        else
                        {
                            transaction_tag = ""
                        }
                        if paymentDict["cvv2"] != nil
                               {
                                 cvv2 = paymentDict["cvv2"] as! NSString
                        }
                        else
                        {
                             cvv2 = ""
                        }
                        if paymentDict["avs"] != nil
                            {
                            avs = paymentDict["avs"] as! NSString
                            }
                            else
                               {
                                    avs = ""
                               }

                        let userDefaults1 = UserDefaults.standard
                        let  userIDString = userDefaults1.integer(forKey: "userIdString")
                        var BuyTicket_ProfileId = Int()
                       if userDefaults1.value(forKey: "ProfileID") == nil
                        {
                            BuyTicket_ProfileId = 0
                        }
                        else
                        {
                            BuyTicket_ProfileId = (userDefaults1.object(forKey: "ProfileID")) as! Int
                        }
                        let profileID = String(BuyTicket_ProfileId)
                        let Timestamp = Date()
                                               let dateFormatter = DateFormatter()
                                               dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
                                               dateFormatter.timeZone = NSTimeZone.default
                                               let utctimeStamp = dateFormatter.string(from: Timestamp)
                        
                //

                            let body: [String : Any] = [
                                "Amount": amountString,
                                "Transcationtype": transactionTypeString,
                                "Currency": currency,
                                "Txn_id": transaction_id,
                                "Correlation_id": correlation_id,
                                "Bankmessage": bank_message,
                                "Txnstatus": transaction_status,
                                "Gatewayrespcode":gateway_resp_code,
                                "Retrival_ref_no":retrieval_ref_no,
                                "Bank_resp_code":bank_resp_code,
                                "Validation_status":validation_status,
                                "Gateway_message":gateway_message,
                                "Method":method,
                                "Txntag":transaction_tag,
                                "Error_code":"",
                                "Error_description":"",
                                "Status_code":bank_resp_code,
                                "Userid":userIDString,
                                "Profileid":profileID,
                                "Ticketid":"0",
                                "Createddate":utctimeStamp,
                                "Updateddate":utctimeStamp,
                                "Cvv2":cvv2,
                                "Avs":avs,
                                "Specialpayment":"",
                                "Txn_ref_no":mercentID
                            ]
                            print(body)
                            print(APIUrl.paymentPostURL)
                        var usernameString = NSString()
                                                                                                                               var emailString = NSString()
                                                                                                                               var phoneString = NSString()


                                                                                                                                if userDefaults1.value(forKey: "userName") == nil
                                                                                                                                {
                                                                                                                                   usernameString = ""
                                                                                                                               }
                                                                                                                               else
                                                                                                                                {
                                                                                                                                   usernameString = userDefaults1.value(forKey: "userName")  as! NSString
                                                                                                                               }
                                                                                                                               if userDefaults1.value(forKey: "userEmail") == nil
                                                                                                                                               {
                                                                                                                                                  emailString = ""
                                                                                                                                              }
                                                                                                                                              else
                                                                                                                                               {
                                                                                                                                                  emailString = userDefaults1.value(forKey: "userEmail")  as! NSString
                                                                                                                                              }
                                                                                                                               
                                                                                                                               if userDefaults1.value(forKey: "phoneNumber") == nil
                                                                                                                                                          {
                                                                                                                                                             phoneString = ""
                                                                                                                                                         }
                                                                                                                                                         else
                                                                                                                                                          {
                                                                                                                                                             phoneString = userDefaults1.value(forKey: "phoneNumber")  as! NSString
                                                                                                                                                         }
                        let SendFailedMailbody: [String : Any] = [
                                                                                                  "To": "isaac@zeddigital.net",
                                                                                                  "Subject":"Three API Failed",
                                                                                                  "Content":"Reference API Failed",
                                                                                                  "Cc":"",
                                                                                                  "Bcc":"",
                                                                                                  "From":"iOS Support",
                                                                                                  "emailfrom":"info@ridetarc.org",
                                                                                              ]
                        let Sendbody: [String : Any] = [
                                                                                               "Subject": "Reference API Failed",
                                                                                               "Username":usernameString,
                                                                                               "Emailid":emailString,
                                                                                               "Phoneno":phoneString,
                                                                                               "Profileid":BuyTicket_ProfileId,
                                                                                               "Ticketcount":self.noofTickets!,
                                                                                               "Transactionid":"",
                                                                                               "Txn_referenceno":"",
                                                                                               "Tickettype":self.ticketType!,
                                                                                               "Token":mercentID,
                                                                                               "Totalamount":amountString,
                                                                                               "Transactiondate":utctimeStamp,
                                                                                               "Content":"Reference API Failed",
                                                                      ]
                        
                        
                            Alamofire.request(APIUrl.paymentPostURL, method: .post, parameters: body, encoding: JSONEncoding.default)
                                .responseJSON { response in
                                    switch response.result {
                                    case .success:
                                        
                                      
                                        if (response.result.value != nil) {
                                            
                                            let responseVar = response.result.value
                                            let json = JSON(responseVar!)
                                            print(json)
                                            if json["Message"] == "Ok" {
                                                
                //                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                                      let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
                //                                      self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                
                                                
                                                
                                                
                                                
                                            }else{
                                              
                                                
                                                                                            Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                                                                                .responseJSON { response in
                                                                                                    switch response.result {
                                                                                                    case .success:
                                                                                                        
                                                                                                        
                                                                                                        if (response.result.value != nil) {
                                                                                                            
                                                                                                            let responseVar = response.result.value
                                                                                                            let json = JSON(responseVar!)
                                                                                                            print(json)
                                                                                                            if json["Message"] == "Ok" {
                                                                                                                
                                                                                                                
                                                                                                                self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                                                                                                                
                                                                                                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                                                let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                                                                                self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                                                                                
                                                                                                                
                                                                                                                 self.hideLoader()
                                                                                                                
                                                                                                                
                                                                                                            }else{
                                                                                                                 self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                   
                                                                                                            }
                                                                                                            
                                                                                                        }else{
                                                                                                             self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                 
                                                                                                        }
                                                                                                    case .failure(let error):
                                                                                                        print(error)
                                                                                                         self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                
                                                                                                    }
                                                                                            }
                                                
                                                
                //
                //                              //  self.view.makeToast("\(json["Message"])")
                //                                self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                //
                //                                                                                               let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                                                                                               let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                //                                                                                               self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                //
                //
                //                                                                                                self.hideLoader()
                                            }
                                            
                                        }else{
                                           
                                                                                                       Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                                                                                            .responseJSON { response in
                                                                                                                switch response.result {
                                                                                                                case .success:
                                                                                                                    
                                                                                                                    
                                                                                                                    if (response.result.value != nil) {
                                                                                                                        
                                                                                                                        let responseVar = response.result.value
                                                                                                                        let json = JSON(responseVar!)
                                                                                                                        print(json)
                                                                                                                        if json["Message"] == "Ok" {
                                                                                                                            
                                                                                                                            
                                                                                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                                                                                                                            
                                                                                                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                                                            let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                                                                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                                                                                            
                                                                                                                            
                                                                                                                             self.hideLoader()
                                                                                                                            
                                                                                                                            
                                                                                                                        }else{
                                                                                                                             self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                               
                                                                                                                        }
                                                                                                                        
                                                                                                                    }else{
                                                                                                                         self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                             
                                                                                                                    }
                                                                                                                case .failure(let error):
                                                                                                                    print(error)
                                                                                                                     self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                            
                                                                                                                }
                                                                                                        }
                                            
                                            
                                           // self.view.makeToast("Please Try Again")
                //                            self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                //
                //                                                                                           let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                                                                                           let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                //                                                                                           self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                //
                //
                //                                                                                            self.hideLoader()
                                        }
                                    case .failure(let error):
                                        print(error)
                                                                                                   Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                                                                                        .responseJSON { response in
                                                                                                            switch response.result {
                                                                                                            case .success:
                                                                                                                
                                                                                                                
                                                                                                                if (response.result.value != nil) {
                                                                                                                    
                                                                                                                    let responseVar = response.result.value
                                                                                                                    let json = JSON(responseVar!)
                                                                                                                    print(json)
                                                                                                                    if json["Message"] == "Ok" {
                                                                                                                        
                                                                                                                        
                                                                                                                        self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                                                                                                                        
                                                                                                                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                                                        let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                                                                                        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                                                                                        
                                                                                                                        
                                                                                                                         self.hideLoader()
                                                                                                                        
                                                                                                                        
                                                                                                                    }else{
                                                                                                                         self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                           
                                                                                                                    }
                                                                                                                    
                                                                                                                }else{
                                                                                                                     self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                         
                                                                                                                }
                                                                                                            case .failure(let error):
                                                                                                                print(error)
                                                                                                                 self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                        
                                                                                                            }
                                                                                                    }
                                     //   self.view.makeToast("Please Try Again!")
                //                        self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                //
                //                                                                                       let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                //                                                                                       let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                //                                                                                       self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                //
                //
                //                                                                                        self.hideLoader()
                                    }
                            }
                    }
                    
                    func postfailedPaymentDetailApi(paymentDict:NSDictionary,mercentID:NSString)
                        {
                            // JSON Body
                            print(paymentDict)
                            
                            
                            let correlation_id = paymentDict["correlation_id"] as! NSString
                           let transaction_status = paymentDict["transaction_status"] as! NSString
                            let description = paymentDict["description"] as! NSString
                            let code = paymentDict["code"] as! NSString
                                    
                            
                            let currency = paymentDict["currency"] ?? "USD"
                            let amount = paymentDict["amount"] ?? "NUll"
                            let transaction_tag = paymentDict["transaction_tag"] ?? "NUll"
                            let method = paymentDict["method"] ?? "NUll"
                            let gateway_resp_code = paymentDict["gateway_resp_code"] ?? "NUll"
                            let validation_status = paymentDict["validation_status"] ?? "NUll"
                            let transaction_type = paymentDict["transaction_type"] ?? "NUll"
                            let gateway_message = paymentDict["gateway_message"] ?? "NUll"

                            let cvv2String = paymentDict["cvv2"] ?? "-"
                            let avsString = paymentDict["avs"]  ?? "-"

                            let userDefaults1 = UserDefaults.standard
                            let  userIDString = userDefaults1.integer(forKey: "userIdString")
                            let BuyTicket_ProfileId = (userDefaults1.object(forKey: "ProfileID")) as! Int
                            let profileID = String(BuyTicket_ProfileId)
                            let Timestamp = Date()
                                                   let dateFormatter = DateFormatter()
                                                   dateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
                                                   dateFormatter.timeZone = NSTimeZone.default
                                                   let utctimeStamp = dateFormatter.string(from: Timestamp)
                            
                    //

                                let body: [String : Any] = [
                                    "Amount": totalPrice!,
                                    "Transcationtype": transaction_type,
                                    "Currency": currency,
                                    "Txn_id": "",
                                    "Correlation_id": correlation_id,
                                    "Bankmessage": description,
                                    "Txnstatus": transaction_status,
                                    "Gatewayrespcode":gateway_resp_code,
                                    "Retrival_ref_no":"",
                                    "Bank_resp_code":code,
                                    "Validation_status":validation_status,
                                    "Gateway_message":gateway_message,
                                    "Method":method,
                                    "Txntag":transaction_tag,
                                    "Error_code":code,
                                    "Error_description":description,
                                    "Status_code":code,
                                    "Userid":userIDString,
                                    "Profileid":profileID,
                                    "Createddate":utctimeStamp,
                                    "Updateddate":utctimeStamp,
                                    "Cvv2":cvv2String,
                                    "Avs":avsString,
                                    "Specialpayment":"",
                                    "Txn_ref_no":mercentID
                                ]
                                print(body)
                                print(APIUrl.paymentPostURL)
                            var usernameString = NSString()
                                                                                                                                   var emailString = NSString()
                                                                                                                                   var phoneString = NSString()


                                                                                                                                    if userDefaults1.value(forKey: "userName") == nil
                                                                                                                                    {
                                                                                                                                       usernameString = ""
                                                                                                                                   }
                                                                                                                                   else
                                                                                                                                    {
                                                                                                                                       usernameString = userDefaults1.value(forKey: "userName")  as! NSString
                                                                                                                                   }
                                                                                                                                   if userDefaults1.value(forKey: "userEmail") == nil
                                                                                                                                                   {
                                                                                                                                                      emailString = ""
                                                                                                                                                  }
                                                                                                                                                  else
                                                                                                                                                   {
                                                                                                                                                      emailString = userDefaults1.value(forKey: "userEmail")  as! NSString
                                                                                                                                                  }
                                                                                                                                   
                                                                                                                                   if userDefaults1.value(forKey: "phoneNumber") == nil
                                                                                                                                                              {
                                                                                                                                                                 phoneString = ""
                                                                                                                                                             }
                                                                                                                                                             else
                                                                                                                                                              {
                                                                                                                                                                 phoneString = userDefaults1.value(forKey: "phoneNumber")  as! NSString
                                                                                                                                                             }
                            let SendFailedMailbody: [String : Any] = [
                                                                                                      "To": "isaac@zeddigital.net",
                                                                                                      "Subject":"Three API Failed",
                                                                                                      "Content":"Reference failed API Failed",
                                                                                                      "Cc":"",
                                                                                                      "Bcc":"",
                                                                                                      "From":"iOS Support",
                                                                                                      "emailfrom":"info@ridetarc.org",
                                                                                                  ]
                            let Sendbody: [String : Any] = [
                                                                                                   "Subject": "Reference failed API Failed",
                                                                                                   "Username":usernameString,
                                                                                                   "Emailid":emailString,
                                                                                                   "Phoneno":phoneString,
                                                                                                   "Profileid":BuyTicket_ProfileId,
                                                                                                   "Ticketcount":self.noofTickets!,
                                                                                                   "Transactionid":"",
                                                                                                   "Txn_referenceno":"",
                                                                                                   "Tickettype":self.ticketType!,
                                                                                                   "Token":mercentID,
                                                                                                   "Totalamount":totalPrice!,
                                                                                                   "Transactiondate":utctimeStamp,
                                                                                                   "Content":"",
                                                                          ]
                            

                                Alamofire.request(APIUrl.paymentPostURL, method: .post, parameters: body, encoding: JSONEncoding.default)
                                    .responseJSON { response in
                                        switch response.result {
                                        case .success:
                                            
                                          
                                            if (response.result.value != nil) {
                                                
                                                let responseVar = response.result.value
                                                let json = JSON(responseVar!)
                                                print(json)
                                                if json["Message"] == "Ok" {
                                                    
                    // let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    // let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
                    // self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }else{
                                                                                                             Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                                                                                                  .responseJSON { response in
                                                                                                                      switch response.result {
                                                                                                                      case .success:
                                                                                                                          
                                                                                                                          
                                                                                                                          if (response.result.value != nil) {
                                                                                                                              
                                                                                                                              let responseVar = response.result.value
                                                                                                                              let json = JSON(responseVar!)
                                                                                                                              print(json)
                                                                                                                              if json["Message"] == "Ok" {
                                                                                                                                  
                                                                                                                                  
                                                                                                                                  self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                                                                                                                                  
                                                                                                                                  let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                                                                  let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                                                                                                  self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                                                                                                  
                                                                                                                                  
                                                                                                                                   self.hideLoader()
                                                                                                                                  
                                                                                                                                  
                                                                                                                              }else{
                                                                                                                                   self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                     
                                                                                                                              }
                                                                                                                              
                                                                                                                          }else{
                                                                                                                               self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                   
                                                                                                                          }
                                                                                                                      case .failure(let error):
                                                                                                                          print(error)
                                                                                                                           self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                  
                                                                                                                      }
                                                                                                              }
                                                   // self.view.makeToast("\(json["Message"])")
                                                }
                                                
                                            }else{
                                               
                                              //  self.view.makeToast("Please Try Again")
                                                                                                           Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                                                                                                .responseJSON { response in
                                                                                                                    switch response.result {
                                                                                                                    case .success:
                                                                                                                        
                                                                                                                        
                                                                                                                        if (response.result.value != nil) {
                                                                                                                            
                                                                                                                            let responseVar = response.result.value
                                                                                                                            let json = JSON(responseVar!)
                                                                                                                            print(json)
                                                                                                                            if json["Message"] == "Ok" {
                                                                                                                                
                                                                                                                                
                                                                                                                                self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                                                                                                                                
                                                                                                                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                                                                let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                                                                                                self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                                                                                                
                                                                                                                                
                                                                                                                                 self.hideLoader()
                                                                                                                                
                                                                                                                                
                                                                                                                            }else{
                                                                                                                                 self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                   
                                                                                                                            }
                                                                                                                            
                                                                                                                        }else{
                                                                                                                             self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                 
                                                                                                                        }
                                                                                                                    case .failure(let error):
                                                                                                                        print(error)
                                                                                                                         self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                                
                                                                                                                    }
                                                                                                            }
                                            }
                                        case .failure(let error):
                                            print(error)
                                            
                                           // self.view.makeToast("Please Try Again!")
                                                                                                       Alamofire.request(APIUrl.sendFailureMail, method: .post, parameters: Sendbody, encoding: JSONEncoding.default)
                                                                                                            .responseJSON { response in
                                                                                                                switch response.result {
                                                                                                                case .success:
                                                                                                                    
                                                                                                                    
                                                                                                                    if (response.result.value != nil) {
                                                                                                                        
                                                                                                                        let responseVar = response.result.value
                                                                                                                        let json = JSON(responseVar!)
                                                                                                                        print(json)
                                                                                                                        if json["Message"] == "Ok" {
                                                                                                                            
                                                                                                                            
                                                                                                                            self.alertForOffilneTicket(Title: "Ticket Not Generated", message: "Your payment is successful, but ticket has not been generated. Please contact Support.")
                                                                                                                            
                                                                                                                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                                                                                            let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                                                                                                                            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                                                                                                                            
                                                                                                                            
                                                                                                                             self.hideLoader()
                                                                                                                            
                                                                                                                            
                                                                                                                        }else{
                                                                                                                             self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                               
                                                                                                                        }
                                                                                                                        
                                                                                                                    }else{
                                                                                                                         self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                             
                                                                                                                    }
                                                                                                                case .failure(let error):
                                                                                                                    print(error)
                                                                                                                     self.sendEmailFailed(sendMailedparameter: SendFailedMailbody)
                                            
                                                                                                                }
                                                                                                        }
                                        }
                                }
                        }

                

                func showLoader()
                {
                    DispatchQueue.main.async {
                        self.view.endEditing(true)
                        self.loaderView = UIView()
                        self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height + 120)
                        self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(1.0)
                        let window = UIApplication.shared.keyWindow!




                        self.waitingLoader = UIImageView()
                        self.waitingLoader.frame = CGRect.init(x: self.view.frame.size.width / 2 - 20, y: self.view.frame.size.height / 3, width: 40, height: 40)
                        self.waitingLoader.image = UIImage.init(named: "waitLoader")
                        //self.waitingLoader.startRotating()
                        self.loaderView.addSubview(self.waitingLoader)

                        let pleaseWaitLabel = UILabel()
                        pleaseWaitLabel.frame = CGRect.init(x: 20, y: self.waitingLoader.frame.maxY + 10, width: self.view.frame.size.width - 40, height: 25)
                        pleaseWaitLabel.text = "Please Wait"
                        pleaseWaitLabel.textAlignment = .center
                        pleaseWaitLabel.numberOfLines = 0
                        pleaseWaitLabel.textColor = UIColor.red
                        pleaseWaitLabel.font = UIFont.setTarcRegular(size: 18)
                        self.loaderView.addSubview(pleaseWaitLabel)

                        let dialogLabel = UILabel()
                        dialogLabel.frame = CGRect.init(x: 20, y: pleaseWaitLabel.frame.maxY + 0, width: self.view.frame.size.width - 40, height: 55)
                        dialogLabel.text = "We are processing your payment, do not click away from this screen."
                        dialogLabel.textAlignment = .center
                        dialogLabel.numberOfLines = 0
                        dialogLabel.font = UIFont.setTarcRegular(size: 15)
                        self.loaderView.addSubview(dialogLabel)

                        let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width / 2) - 40, y: dialogLabel.frame.maxY + 5, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0))
                        activityLoader.startAnimating()
                        window.addSubview(self.loaderView)
                        self.loaderView.addSubview(activityLoader)

                        let payeezyImage = UIImageView()
                        payeezyImage.frame = CGRect.init(x: self.view.frame.size.width / 2 - 20, y: self.view.frame.maxY - 80, width: 60, height: 60)
                        payeezyImage.image = UIImage.init(named: "payeezee_image")
                        //self.waitingLoader.startRotating()
                        self.loaderView.addSubview(payeezyImage)


                    }
                }
                func hideLoader()
                {
                    DispatchQueue.main.async {
                        self.waitingLoader.stopRotating()

                        self.loaderView.removeFromSuperview()

                    }
                }
            }
            extension String {
                func chunkFormatted(withChunkSize chunkSize: Int = 4,
                    withSeparator separator: Character = "-") -> String {
                    return characters.filter { $0 != separator }.chunk(n: chunkSize)
                        .map { String($0) }.joined(separator: String(separator))
                }
                func exprieFormatted(withChunkSize chunkSize: Int = 2,
                    withSeparator separator: Character = "/") -> String {
                    return characters.filter { $0 != separator }.chunk(n: chunkSize)
                        .map { String($0) }.joined(separator: String(separator))
                }
            }
            extension Collection {
                public func chunk(n: IndexDistance) -> [SubSequence] {
                    var res: [SubSequence] = []
                    var i = startIndex
                    var j: Index
                    while i != endIndex {
                        j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
                        res.append(self[i..<j])
                        i = j
                    }
                    return res
                }
            }


            extension UIView {

                func startRotating(duration: CFTimeInterval = 3, repeatCount: Float = Float.infinity, clockwise: Bool = true) {

                    if self.layer.animation(forKey: "transform.rotation.z") != nil {
                        return
                    }

                    let animation = CABasicAnimation(keyPath: "transform.rotation.z")
                    let direction = clockwise ? 1.0 : -1.0
                    animation.toValue = NSNumber(value: .pi * 2 * direction)
                    animation.duration = duration
                    animation.isCumulative = true
                    animation.repeatCount = repeatCount
                    self.layer.add(animation, forKey: "transform.rotation.z")
                }

                func stopRotating() {

                    self.layer.removeAnimation(forKey: "transform.rotation.z")

                }

            }

