//
//  mapsandSchudle.swift
//  ZIG
//
//  Created by Isaac on 07/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper

class mapsandSchudle: Mappable {
    var routeName: String?
    var routenameid: Int?
    var LineName:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        routeName <- map["RouteName"]
        routenameid <- map["RouteID"]
        LineName <- map["LineName"]
    }
    
    
}

class mapsandSchudleList:Mappable {
    var Title:String?
    var RouteID:String?
   // var TripDetails:[TripDetailsSch]?
    var TripTitle : String?
    var TripId :Int?
    var RouteColor:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        Title <- map["TripHeadsign"]
        RouteID <- map["RouteID"]
        TripTitle <- map["TripDetails.TripTitle"]
        TripId <- map["TripDetails.TripId"]
        RouteColor <- map["TripDetails.RouteColor"]
    }
    
    class TripDetailsSch: Mappable {
        var tripTitle : String?
        var TripId :Int?
        var RouteColor:String?
        required init?(map: Map) {
            
        }
        
         func mapping(map: Map) {
            tripTitle <- map["TripTitle"]
            TripId <- map["TripId"]
            RouteColor <- map["RouteColor"]
            
            
        }
        
        
    }
}


