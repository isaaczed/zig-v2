//
//  SuggRouteNewViewController.swift
//  ZIG
//
//  Created by Isaac on 06/03/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import GoogleMaps
import UberRides
import LyftSDK
import Foundation
import NYAlertViewController
import NVActivityIndicatorView
import SwiftGifOrigin
import UberCore
import SwiftyJSON
import XLActionController
import SafariServices
import SCLAlertView

class SuggRouteNewViewController: UIViewController,UITableViewDelegate,UIScrollViewDelegate, UITableViewDataSource {
    let assistiveTouchSOS = AssistiveTouch()
    var isBuyAll = false
    var ii = 0
    var AmenititesTypeTrip:String?
    var ameniticsTitle:String?
    var profuctid = ""
    let loggedIn = TokenManager.fetchToken()
    var sortedResults = [[String:Any]]()
    var CompleteValueStatus = ""
    let transitScrollView = UIScrollView()
    var screenheight:Int?
    var backgroundBlurView1 = UIView()
    var tankrealTimeTimer : Timer!
    var transitImageViewX = 0,transitImageViewY = 0,transitImageViewWidth = 0,transitImageViewHeight = 0,doneBtnY = 0
    let priceTotal = UILabel()
    let MytarcPricetotal = UILabel()
    var amenityViewX = 0,amenityViewY = 0,amenityViewWidth = 0,amenityViewHeight = 0,uberButtonX = 0, clearMarkerX = 0, clearMarkerY = 0, clearMarkerWidth = 80, clearMarkerHeight = 40
    var closeBtnWidth = 0, profileImageSize = 0, ticketTableviewHeight = 0, uberBtnWidth = 0
    var transitCount = 0
    var stepArrayFare :NSMutableArray = []
    var MyTarcFare :NSMutableArray = []
    var FareStringEXP = 2.75
    var ticketMenuCheck = true
    var FareStringLoc = 1.75
    var TotalFar = 0.0
    var MyTarcCardTotalFare = 0.0
    var subTotalFare = 0.0
    var MyTarcCardTotalFareArray:[Double] = [0.0]
    var backgroundBlurView = UIView()
    var stepArray :NSMutableArray = []
    var menuBackView = UIView()
    @IBOutlet weak var TopPanelRoute: UILabel!
    @IBOutlet weak var SourceLabelAddress: UILabel!
    @IBOutlet weak var DestinationLabelAddress: UILabel!
    var sourcePlaceID:String?
    var DestinationPlaceID:String?
    var prevArravaltime = ""
    var firstwalking:Bool = false
    var myStringafd = ""
    var dataPrevRespone:DataResponse<SuggestDirectionaClass>!
    var dataPrevBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var indexValue = 0
    var uberIndex = 0
    var menuButton = UIBarButtonItem()
    var secIndex = ""
    var uberSecIndex = ""
    var uberAddedIndex = 0
    var walkingdeparttime = ""
    var realtimeDataResponse:DataResponse<TransitRouteClass>!
    var loaderView = UIView()
    var isUberAdded : Bool = false
    var ticketMenuX = 0,ticketMenuY = 0,ticketMenuWidth = 0,ticketMenuHeight = 0
    var menuTabelView :UITableView = UITableView()
    let cellReuseIdentifier = "cell"
    
    var googlereponseDirection = [[String:Any]]()
    
    var sourceAddressLat:Double?
    var DestinationLat:Double?
    var SourceAddressString:String?
    var DestinationAddressString:String?
    
    let TotalFareLabel = UIView()
    var sourceAddressLong:Double?
    var DestinationLong:Double?
    var ubertotalprice:Double = 0.0
    
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidDisappear(_ animated: Bool) {
         NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }
    override func viewDidLoad() {
        
        analytics.GetPageHitCount(PageName: "Route Description")
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
        super.viewDidLoad()
        self.title = "DIRECTIONS"
        
        self.showLoader()
        self.navigationController?.navigationBar.isHidden = false
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        menuButton = UIBarButtonItem(title: "View Fares",
                                     style: UIBarButtonItemStyle.plain,
                                     target: self,
                                     action: #selector(OnMenuClicked))
        self.navigationItem.rightBarButtonItem = menuButton
        menuBackView.frame = CGRect.init(x: 35, y: -2000, width: self.view.frame.width-80, height: self.view.frame.height-120)
        menuBackView.backgroundColor = UIColor.white
        self.navigationItem.rightBarButtonItem?.tintColor = color.hexStringToUIColor(hex: "#EFAC40")
        
        self.view.addSubview(menuBackView)
        menuBackView.layer.borderWidth = 2
        menuBackView.layer.borderColor = UIColor.init(red:225/255.0, green:225/255.0, blue:225/255.0, alpha: 1.0).cgColor
        let formatter = DateFormatter()
        
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone(abbreviation: "EST")
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "HH:mm a"
        // again convert your date to string
        walkingdeparttime = formatter.string(from: yourDate!)
        
        // Do any additional setup after loading the view.
        let dataleg = dataPrevRespone.result.value?.routes![0].legs
        for legarray in dataleg! {
            
            SourceLabelAddress.text = legarray.startAddress
            DestinationLabelAddress.text = legarray.endAddress
        }
        if UIScreen.main.sizeType == .iPhone4{
            ticketMenuX = 0
            ticketMenuY = 0
            ticketMenuWidth = 0
            ticketMenuHeight = 0
            transitImageViewX = 0
            transitImageViewY = 0
            transitImageViewWidth = 0
            transitImageViewHeight = 0
        }else if UIScreen.main.sizeType == .iPhone5{
            ticketMenuX = 10
            ticketMenuY = 15
            ticketMenuWidth = 20
            ticketMenuHeight = 150
            closeBtnWidth = 40
            profileImageSize = 30
            ticketTableviewHeight = 154
            uberBtnWidth = 260
            transitImageViewX = 20
            transitImageViewY = 30
            transitImageViewWidth = 35
            transitImageViewHeight = 35
        }else if UIScreen.main.sizeType == .iPhone6{
            ticketMenuX = 20
            ticketMenuY = 20
            ticketMenuWidth = 40
            ticketMenuHeight = 200
            closeBtnWidth = 50
            profileImageSize = 50
            ticketTableviewHeight = 180
            uberBtnWidth = 260
            transitImageViewX = 15
            transitImageViewY = 25
            transitImageViewWidth = 45
            transitImageViewHeight = 45
        }else if UIScreen.main.sizeType == .iPhone6Plus{
            ticketMenuX = 20
            ticketMenuY = 20
            ticketMenuWidth = 40
            ticketMenuHeight = 220
            closeBtnWidth = 60
            profileImageSize = 50
            ticketTableviewHeight = 200
            uberBtnWidth = 280
            uberButtonX = 20
            transitImageViewX = 10
            transitImageViewY = 25
            transitImageViewWidth = 55
            transitImageViewHeight = 55
        }
        else if UIScreen.main.nativeBounds.height == 1920 {
            print("iPhone 6plus or 7plus or 8plus")
            ticketMenuX = 20
            ticketMenuY = 20
            ticketMenuWidth = 40
            ticketMenuHeight = 220
            closeBtnWidth = 60
            profileImageSize = 50
            ticketTableviewHeight = 200
            uberBtnWidth = 280
            uberButtonX = 20
            transitImageViewX = 10
            transitImageViewY = 25
            transitImageViewWidth = 55
            transitImageViewHeight = 55
            
        }
        else if UIScreen.main.nativeBounds.height == 2688 {
            print("iPhone XS max")
            ticketMenuX = 20
            ticketMenuY = 20
            ticketMenuWidth = 40
            ticketMenuHeight = 200
            closeBtnWidth = 70
            profileImageSize = 50
            ticketTableviewHeight = 210
            uberBtnWidth = 260
            uberButtonX = 10
            transitImageViewX = 10
            transitImageViewY = 25
            transitImageViewWidth = 45
            transitImageViewHeight = 45
            doneBtnY = 40
        }
        else if UIScreen.main.nativeBounds.height == 1792 {
            print("iPhone Xr max")
            ticketMenuX = 20
            ticketMenuY = 20
            ticketMenuWidth = 40
            ticketMenuHeight = 200
            closeBtnWidth = 70
            profileImageSize = 50
            ticketTableviewHeight = 210
            uberBtnWidth = 260
            uberButtonX = 10
            transitImageViewX = 10
            transitImageViewY = 25
            transitImageViewWidth = 45
            transitImageViewHeight = 45
            doneBtnY = 40
            
        }
        
        
        else if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
            ticketMenuX = 200
            ticketMenuY = 250
            ticketMenuWidth = 400
            ticketMenuHeight = 500
            closeBtnWidth = 40
            profileImageSize = 40
            ticketTableviewHeight = 250
            uberBtnWidth = 260
            transitImageViewX = 25
            transitImageViewY = 25
            transitImageViewWidth = 45
            transitImageViewHeight = 45
            doneBtnY = 0
        }else if UIScreen.main.sizeType == .iPadPro105{
            ticketMenuX = 200
            ticketMenuY = 250
            ticketMenuWidth = 400
            ticketMenuHeight = 500
            closeBtnWidth = 40
            profileImageSize = 40
            ticketTableviewHeight = 250
            uberBtnWidth = 260
            transitImageViewX = 25
            transitImageViewY = 25
            transitImageViewWidth = 45
            transitImageViewHeight = 45
            doneBtnY = 0
        }else if UIScreen.main.sizeType == .iPadPro129{
            ticketMenuX = 200
            ticketMenuY = 250
            ticketMenuWidth = 400
            ticketMenuHeight = 500
            closeBtnWidth = 70
            profileImageSize = 60
            ticketTableviewHeight = 200
            uberBtnWidth = 260
            uberButtonX = 10
            transitImageViewX = 25
            transitImageViewY = 25
            transitImageViewWidth = 45
            transitImageViewHeight = 45
            doneBtnY = 0
        }else if UIScreen.main.sizeType == .iPhoneX{
            ticketMenuX = 20
            ticketMenuY = 20
            ticketMenuWidth = 40
            ticketMenuHeight = 200
            closeBtnWidth = 70
            profileImageSize = 50
            ticketTableviewHeight = 210
            uberBtnWidth = 260
            uberButtonX = 10
            transitImageViewX = 10
            transitImageViewY = 25
            transitImageViewWidth = 45
            transitImageViewHeight = 45
            doneBtnY = 40
        }
        
        
        gettoppanel()
        
       
        getDirectionFromGoogle()
        
        
        
   
        let assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

        assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
        assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
        view.addSubview(assistiveTouch)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
        tapGesture.numberOfTapsRequired = 1
        assistiveTouchSOS.addGestureRecognizer(tapGesture)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
        longPress.minimumPressDuration = 1.0
        self.assistiveTouchSOS.addGestureRecognizer(longPress)
        assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
        assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
        //view.addSubview(assistiveTouchSOS)
        
        
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
          if gesture.state == UIGestureRecognizerState.began {
              UIDevice.vibrate()
              print("Long Press")
              currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                         if Success{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                          print("\(Address),\(LatsosLat),\(Longsoslong)")
                          MapsScheduleVC.CurrentlocationAddressString = Address
                          MapsScheduleVC.latSOS = LatsosLat
                          MapsScheduleVC.longSOS = Longsoslong
                        self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                 //self.CurrentlocationAddress.text = Address
                             }
                         }
              
          }
      }
    
      @objc func tappedSOS(sender: UIButton) {
       
          let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                  // action here
          }
                  
          SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
         
      }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    @objc func tapped(sender: UIButton) {
        print("\(sender) has been touched")
        
        
        let actionController = TwitterActionController()
        
        actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
            
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        if comVar.DateImplementation {
            
            actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                

                
            }))
            
        }
        
        actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        actionController.headerData = "Quick View"
        present(actionController, animated: true, completion: nil)
        
        
        
        
    }
    func gettoppanel()
    {
        
        var TransitType:[String] = ["Trip Planner"]
        var AmenititesType:String = self.AmenititesTypeTrip ?? "Bus_station"
        var TotalWalkingTime = 0
        var TotalTransitTime = 0
        var TotalWalkingDistance = 0
        var TotalTransitDistance = 0
        var NumberOfTransit = 0
        var TotalTripTime:Int?
        var TotalTripDistance:Int?
        var TotalCost:Double?
        var category:String?
        
        
        //TransitType.removeAll()
        print("Bus 1")
        let durationLabel = UILabel()
        
        let fareLabel = UILabel()
        let TotalMyTarcFareLabel = UILabel()
        let mytarccard = UILabel()
        let Regular = UILabel()
        
        let valueResult = dataPrevRespone.result.value
        let routesPlan = valueResult?.routes![self.indexValue].legs
        //let stepValue = routesPlan![0].steps![indexPath.row]
        
        
        //transitScrollView.delegate = self
        transitScrollView.backgroundColor = UIColor.clear
        transitScrollView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
        // transitScrollView.showsHorizontalScrollIndicator = false
        var durationString = routesPlan![0].duration?.value
        durationLabel.frame = CGRect.init(x: 10, y: 15, width: 95, height: 30)
        
        var fareCount : Double = 0.00
        var MyCardfareCount : Double = 0.00
        var distValue = 0
        var UberLyftWaitingTime = 3
        durationLabel.textColor = UIColor.black
        durationLabel.font = UIFont.setTarcRegular(size: 16.0)
        durationLabel.textAlignment = NSTextAlignment.center
        
          //TransitType?.append("")
        var indexValue = 0
        var prevArrowFrame = CGRect()
        for stepValue in routesPlan![0].steps!
        {
            
            
            
            
            let transitImage = UIImageView()
            let arrowImage = UIImageView()
            let MyTarcButton = UIImageView()
            let busName = UILabel()
            let busTimingLable = UILabel()
            let distanceValue = (stepValue.distance?.value)!
            let durationvaluewalking = (stepValue.duration?.value)!
            
            
           
            let userDefauls = UserDefaults.standard
            var cabSource:String?
            var cabDestination:String?
            
           // if
            
            if distanceValue > 805 && userDefauls.bool(forKey: "isCab") && stepValue.travelMode == "WALKING"
            {
                
                let ridesClient = RidesClient()
                let pickupLocation = CLLocation(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
                let dropoffLocation = CLLocation(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
                let builder = RideParametersBuilder()
                builder.pickupLocation = pickupLocation
                builder.dropoffLocation = dropoffLocation
                if userDefauls.bool(forKey: "isUber"){
                    
                    TransitType.append("Uber")
                    
                    ridesClient.fetchTimeEstimates(pickupLocation: pickupLocation) { (estimates, response) in
                        if estimates.count != 0 {
                        // dump(estimates)
                        UberLyftWaitingTime = estimates[0].estimate!
                        print(estimates[0].estimate!)
                        }
                    }
                    ridesClient.fetchPriceEstimates(pickupLocation: pickupLocation, dropoffLocation: dropoffLocation, completion: {priceEstimats, response in
                        print(priceEstimats.count)
                        //dump(priceEstimats)
                        if priceEstimats.count != 0 {
                        durationString = priceEstimats[0].duration! + UberLyftWaitingTime
                        
                        
                       // print(distanceValue)
                        print(routesPlan![0].steps!.count == 1 && stepValue.travelMode == "WALKING")
                        if routesPlan![0].steps!.count == 1 && stepValue.travelMode == "WALKING" {
                            
                            distValue =  durationString!
                            
                        }
                        else{
                            distValue = distValue + durationString! - durationvaluewalking
                        }
                        
                        
                        let hourString:Int = distValue / 3600
                        let minString:Int = (distValue % 3600)/60
                        if hourString>0
                        {
                            DispatchQueue.main.async {
                                durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                                durationLabel.textColor = UIColor.black
                                durationLabel.font = UIFont.setTarcRegular(size: 16.0)
                                durationLabel.textAlignment = NSTextAlignment.center
                                //cell.contentView.addSubview(durationLabel)
                                self.transitScrollView.addSubview(durationLabel)
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                durationLabel.text = String(describing: "\(minString) min")
                                durationLabel.textColor = UIColor.black
                                durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                durationLabel.textAlignment = NSTextAlignment.center
                                //   cell.contentView.addSubview(durationLabel)
                                self.transitScrollView.addSubview(durationLabel)
                            }
                        }
                        }
                    })
                }
                else{
                    TransitType.append("Lyft")
                    let pickup = CLLocationCoordinate2D(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
                    let destination = CLLocationCoordinate2D(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
                    
                    
                    durationLabel.textColor = UIColor.black
                    durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                    durationLabel.textAlignment = NSTextAlignment.center
                    //   cell.contentView.addSubview(durationLabel)
                    self.transitScrollView.addSubview(durationLabel)
                    
                    LyftAPI.ETAs(to: pickup) { result in
                        
                        if result.error == nil {
                        //                                    result.value?.forEach { eta in
                        //                                        print("ETA for \(eta.rideKind.rawValue): \(eta.minutes) min")
                        //                                    }
                        UberLyftWaitingTime = result.value![0].minutes
                        print("\(UberLyftWaitingTime) sec-isaac")
                        }
                        else{
                            
                        }
                    }
                    LyftAPI.costEstimates(from: pickup, to: destination, rideKind: .Standard) { result in
                        if result.error == nil{
                        let costEstimate = result.value![0]
                        print("Min: \(costEstimate.estimate!.minEstimate.amount)$")
                        print("Max: \(costEstimate.estimate!.maxEstimate.amount)$")
                        print("Distance: \(costEstimate.estimate!.distanceMiles) miles")
                        print("Duration: \(costEstimate.estimate!.durationSeconds) sec")
                        // dump(costEstimate)
                        durationString = costEstimate.estimate!.durationSeconds
                        
                        
                        print(distanceValue)
                        if routesPlan![0].steps!.count == 1 && stepValue.travelMode == "WALKING" {
                            distValue =   durationString!
                        }
                        else{
                            distValue = distValue + durationString! - durationvaluewalking
                        }
                        
                        let hourString:Int = distValue / 3600
                        let minString:Int = ((distValue % 3600)/60) + UberLyftWaitingTime
                        if hourString>0
                        {
                            DispatchQueue.main.async {
                                durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                              
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                durationLabel.text = String(describing: "\(minString) min")
                               
                            }
                        }
                        
                        }
                        else{
                            
                        }
                        
                    }
                }
            }
            else
            {
                
                // distValue = distValue + (stepValue.duration?.value)!
                distValue = durationString!
                let hourString:Int = distValue / 3600
                let minString:Int = (distValue % 3600)/60
                if hourString>0
                {
                    DispatchQueue.main.async {
                        durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                        durationLabel.textColor = UIColor.black
                        durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                        durationLabel.textAlignment = NSTextAlignment.center
                        //   cell.contentView.addSubview(durationLabel)
                        self.transitScrollView.addSubview(durationLabel)
                    }
                }
                else
                {
                    DispatchQueue.main.async {
                        durationLabel.text = String(describing: "\(minString) min")
                        durationLabel.textColor = UIColor.black
                        durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                        durationLabel.textAlignment = NSTextAlignment.center
                        //cell.contentView.addSubview(durationLabel)
                        self.transitScrollView.addSubview(durationLabel)
                    }
                }
            }
            if indexValue == 0
            {
                cabSource = self.SourceAddressString
                 let newString = stepValue.htmlInstructions!.replacingOccurrences(of: "Walk to", with: "")
                cabDestination = newString
                
                transitImage.frame = CGRect.init(x: 120*(indexValue+1), y: 15, width: 24, height: 24)
                
            }
            else if indexValue == routesPlan![0].steps!.count - 1{
                 transitImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width+10, y: 15, width: 24, height: 24)
                
                cabDestination = self.DestinationAddressString
                let newString =  routesPlan![0].steps![indexValue-1].htmlInstructions!.replacingOccurrences(of: "Bus towards", with: "")
                cabSource = newString
            }
            else
            {
                let newString1 =  routesPlan![0].steps![indexValue-1].htmlInstructions!.replacingOccurrences(of: "Bus towards", with: "")
                cabSource = newString1
                let newString = stepValue.htmlInstructions!.replacingOccurrences(of: "Walk to", with: "")
                cabDestination = newString
                transitImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width+10, y: 15, width: 24, height: 24)
            }
            if stepValue.travelMode == "TRANSIT"
            {
                NumberOfTransit = NumberOfTransit + 1
                let userDefaults1 = UserDefaults.standard
                
                // let fareValue : Double = userDefaults1.value(forKey: "generalFare") as! Double
//
//                let ArrivalTimeEpo = stepValue.transitDetails?.arrivalTime?.value
//                let DepaRTTimeEpo = stepValue.transitDetails?.departureTime?.value
//
                
               let TotalTransitTimeAnalytics = stepValue.duration?.value!
                TotalTransitTime = TotalTransitTime + TotalTransitTimeAnalytics!
                
                TotalTransitDistance = TotalTransitDistance + (stepValue.distance?.value!)!
               
                
                
                if stepValue.transitDetails?.line?.shortName  != nil {
                    if (stepValue.transitDetails?.line?.shortName!.contains("X"))!  {
                        
                        // self.TotalFar = self.fareStringEXP + self.TotalFar
                        fareCount = fareCount + self.FareStringEXP
                        //MyCardfareCount =   self.fareStringExpMyCard
                        
                    }
                    else{
                        // self.TotalFar = self.FareStringLocal + self.TotalFar
                        fareCount = fareCount + self.FareStringLoc
                        //MyCardfareCount = MyCardfareCount + self.fareStringLocalMycard
                    }
                }
                    
                else{
                    // self.TotalFar = self.FareStringLocal + self.TotalFar
                    fareCount = fareCount + self.FareStringLoc
                    //MyCardfareCount = MyCardfareCount + self.fareStringLocalMycard
                }
                
                
                
                let urlString = "https:"+(stepValue.transitDetails?.line?.vehicle?.icon)!
                let url = URL(string: "https:"+(stepValue.transitDetails?.line?.vehicle?.icon)!)
                
                
                
                
                if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/subway2.png"
                {
                    transitImage.image = UIImage(named: "train_Transit")
                }
                else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/rail2.png"
                {
                    transitImage.image = UIImage(named: "train_Transit")
                }
                else if  urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/bus2.png"
                {
                    if stepValue.transitDetails?.line?.agencies![0].name == "Greater Cleveland Regional Transit Authority"
                    {
                        transitImage.image = UIImage(named: "tank_bus")
                    }
                    else
                    {
                        transitImage.image = UIImage(named: "bus_Transit")
                    }
                }
                else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/tram2.png"
                {
                    transitImage.image = UIImage(named: "tram_transit")
                }
                else
                {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    transitImage.image = UIImage(data: data!)
                }
                // TotalTransitTime = stepValue.transitDetails?.
                busName.frame = CGRect.init(x: transitImage.frame.origin.x + transitImage.frame.size.width, y: 15, width: 30, height: 30)
                busName.text = stepValue.transitDetails?.line?.shortName
                busName.frame.size = busName.intrinsicContentSize
                busName.textColor = UIColor.black
                busName.font = UIFont.setTarcBold(size: 14.0)
                busName.textAlignment = NSTextAlignment.center
                transitScrollView.addSubview(busName)
                
                busTimingLable.frame = CGRect.init(x: transitImage.frame.origin.x, y: 45, width: 100, height: 30)
                busTimingLable.text = stepValue.transitDetails?.departureTime?.text
                busTimingLable.frame.size = busTimingLable.intrinsicContentSize
                busTimingLable.textColor = UIColor.black
                busTimingLable.font = UIFont.setTarcBold(size: 12.0)
                busTimingLable.textAlignment = NSTextAlignment.center
                transitScrollView.addSubview(busTimingLable)
                
                // let dep_epoh =
                let arr_epoh = stepValue.transitDetails?.arrivalTime?.value!
                let CurrentDate = NSDate().timeIntervalSince1970
                let departdatetime =  dateconvertForOneday(Datevalue: CurrentDate)
                let arrival_time =  dateconvertForOneday(Datevalue: arr_epoh!)
                print(departdatetime)
                print(arrival_time)
                print(routesPlan ?? "defaut")
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd/MM/yyyy"
                let firstDate = formatter.date(from: departdatetime)
                let secondDate = formatter.date(from: arrival_time)
                
                let formattera = DateComponentsFormatter()
                formattera.allowedUnits = [.day]
                formattera.unitsStyle = .full
                var DateCalculation = formattera.string(from: firstDate!, to: secondDate!)!

                
                
                if firstDate?.compare(secondDate!) == .orderedAscending {
                    print("First Date is smaller then second date")
                    let plusoneday = UILabel()
                    print("*********** +1 day ***********")
                    if DateCalculation.contains("days") {
                        //Str
                        DateCalculation = DateCalculation.replacingOccurrences(of: "days", with: "day", options:
                            NSString.CompareOptions.literal, range: nil)
                        plusoneday.text = "+\(DateCalculation)"
                    }
                    else{
                        plusoneday.text = "+\(DateCalculation)"
                    }
                    plusoneday.frame = CGRect.init(x: busTimingLable.frame.origin.x + 55, y: 45, width: 100, height: 30)
                    plusoneday.frame.size = plusoneday.intrinsicContentSize
                    plusoneday.textColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
                    plusoneday.font = UIFont.setTarcBold(size: 12.0)
                    plusoneday.textAlignment = NSTextAlignment.center
                    transitScrollView.addSubview(plusoneday)
                }
            }
                
            else
            {
                let distanceValue = (stepValue.distance?.value)!
                print((stepValue.distance?.value)!)
                let userDefaults = UserDefaults.standard
                if distanceValue > 805 && userDefaults.bool(forKey: "isCab")
                {
                    //  isUberAdded = true
                    if userDefaults.bool(forKey: "isUber"){
                        transitImage.image = UIImage(named:"uber_Car")
                        uberSecIndex = stepValue.htmlInstructions!
                        uberAddedIndex = self.indexValue+1
                        
                        
                        let ridesClient = RidesClient()
                        //let htmlinstraction
                        let pickupaddress = ""
                        let pickupLocation = CLLocation(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
                        let dropoffLocation = CLLocation(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
                        let builder = RideParametersBuilder()
                        builder.pickupLocation = pickupLocation
                        builder.dropoffLocation = dropoffLocation
                        
                        ridesClient.fetchPriceEstimates(pickupLocation: pickupLocation, dropoffLocation: dropoffLocation, completion: {priceEstimats, response in
                            print(priceEstimats.count)
                            if priceEstimats.count != 0{
                            let uberProduct = priceEstimats[0]
                            
                            print(uberProduct.estimate!)
                            // dump(uberProduct)
                                let totaltriptime = uberProduct.duration
                                print(uberProduct.distance!)
                                print("************************************")
                                let totaltripDistance = Int(uberProduct.distance! * 1609.344)
                                let onlyUberPrice =  Double(uberProduct.lowEstimate!)
                            fareCount = fareCount + Double(uberProduct.lowEstimate!)
                            MyCardfareCount = MyCardfareCount + Double(uberProduct.lowEstimate!)
                            let fareString = String(format: "%.2f", fareCount)
                            let mycardfarestring =  String(format: "%.2f", MyCardfareCount)
                            DispatchQueue.main.async {
                                fareLabel.text = "$\(fareString)"
                                TotalMyTarcFareLabel.text = "$\(mycardfarestring)"
                                
                                
                                analytics.getActivity(Activity_Type: "Uber", Source_Address: cabSource!, Destination_Address: cabDestination!, Amenitites: self.ameniticsTitle!, Source_placeid: self.sourcePlaceID!, Destination_placeid: self.DestinationPlaceID!, Total_walktime: 0, Total_walkingdistance: 0, Total_transitdistance: Double(totaltripDistance), NumberofTransit: 1, Total_triptime: totaltriptime!, Total_transittime: totaltriptime!, Total_tripdistance: Double(totaltripDistance), highcost: onlyUberPrice, Application: 1, Firstmile: 1, Amenitites_Category: self.AmenititesTypeTrip!)
                                
                                
                            }
                            
                            }
                            
                        })
                        
                        
                        
                    }else{
                        transitImage.image = UIImage(named:"lyft_preference")
                        uberSecIndex = stepValue.htmlInstructions!
                        uberAddedIndex = self.indexValue+1
                        
                        let pickup = CLLocationCoordinate2D(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
                        let destination = CLLocationCoordinate2D(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
                        
                        
                        // let locationD = CLLocationCoordinate2D(latitude: 37.7833, longitude: -122.4167)
                        
                        
                        
                        
                        LyftAPI.costEstimates(from: pickup, to: destination, rideKind: .Standard) { result in
                            if result.error == nil {
                            result.value?.forEach { costEstimate in
                                print("Min: \(costEstimate.estimate!.minEstimate.amount)$")
                                print("Max: \(costEstimate.estimate!.maxEstimate.amount)$")
                                print("Distance: \(costEstimate.estimate!.distanceMiles) miles")
                                print("Duration: \(costEstimate.estimate!.durationSeconds/60) minutes")
                                // dump(costEstimate)
                                let costEstimate = costEstimate.estimate!.minEstimate.amount
                                fareCount = fareCount + Double(truncating: costEstimate as NSNumber)
                                MyCardfareCount = MyCardfareCount + Double(truncating: costEstimate as NSNumber)
                                let mycardfarestringlyft =  String(format: "%.2f", MyCardfareCount)
                                
                                let fareString = String(format: "%.2f", fareCount)
                                DispatchQueue.main.async {
                                    fareLabel.text = "$\(fareString)"
                                    TotalMyTarcFareLabel.text = "$\(mycardfarestringlyft)"
                                    
                                    
                                  
                                    
                                    
                                }
                                
                            }
                            
                            let Lyftdistance = result.value![0].estimate!.distanceMiles * 1609.344
                            let LyftTime = result.value![0].estimate!.durationSeconds
                            let LyftCost = "\(result.value![0].estimate!.minEstimate.amount)"
                           let LyftcostDouble = Double(LyftCost)
                            let LyftdistanceInt = Int(Lyftdistance)
                            
                            
                            analytics.getActivity(Activity_Type: "Lyft", Source_Address: cabSource!, Destination_Address: cabDestination!,Amenitites: self.ameniticsTitle!, Source_placeid: self.sourcePlaceID!, Destination_placeid: self.DestinationPlaceID!, Total_walktime: 0, Total_walkingdistance: 0, Total_transitdistance: Double(LyftdistanceInt), NumberofTransit: 1, Total_triptime: LyftTime, Total_transittime: LyftTime, Total_tripdistance: Double(LyftdistanceInt), highcost: LyftcostDouble!, Application: 1, Firstmile: 1, Amenitites_Category: self.AmenititesTypeTrip!)
                            
                        }
                            else{
                                
                               
                                
                                comFunc.getLyftAPIFirst(SourceLat: Double((stepValue.startLocation?.lat)!), SourceLong: Double((stepValue.startLocation?.lng)!), DestinationLat:  Double((stepValue.endLocation?.lat)!), DestinationLong: Double((stepValue.endLocation?.lng)!), completion: { (Price,maxprice, Min, Mile, CheckBool) in
                                    
                                    
                                    
                                    if CheckBool {
                                        
                                        let costEstimate = Price
                                        fareCount = fareCount + Double(truncating: costEstimate as NSNumber)
                                        MyCardfareCount = MyCardfareCount + Double(truncating: costEstimate as NSNumber)
                                        let mycardfarestringlyft =  String(format: "%.2f", MyCardfareCount)
                                        
                                        let fareString = String(format: "%.2f", fareCount)
                                        DispatchQueue.main.async {
                                            fareLabel.text = "$\(fareString)"
                                            TotalMyTarcFareLabel.text = "$\(mycardfarestringlyft)"
                                        
                                        
                                    }
                                    }
                                })
                                
                                
                                
                            }
                        }
                    }
                    
                }
                else
                {
                    print("Walking Time \(durationvaluewalking)")
                    print("walking distance \(distanceValue)")
                    print("**************walking***************")
                    TotalWalkingTime = TotalWalkingTime + durationvaluewalking
                    TotalWalkingDistance = TotalWalkingDistance + distanceValue
                    transitImage.image = UIImage(named:"walking_Icon" )
                }
            }
            transitScrollView.addSubview(transitImage)
            if indexValue+1 < (routesPlan![0].steps?.count)!
            {
                print(stepValue.transitDetails?.line?.vehicle?.icon ?? "nil" , stepValue.transitDetails?.line?.shortName ?? "nil" )
                if stepValue.travelMode == "TRANSIT" && stepValue.transitDetails?.line?.shortName != nil
                {
                    arrowImage.frame = CGRect.init(x: busName.frame.origin.x + busName.frame.size.width+10, y: 15, width: 24, height: 24)
                }
                else
                {
                    arrowImage.frame = CGRect.init(x: transitImage.frame.origin.x + transitImage.frame.size.width+10, y: 15, width: 24, height: 24)
                }
                
                arrowImage.image = UIImage(named:"Right-Arrow")
                prevArrowFrame = arrowImage.frame
                transitScrollView.addSubview(arrowImage)
            }
            self.transitScrollView.contentSize = CGSize(width: transitImage.frame.origin.x+transitImage.frame.size.width+30, height: 80)
            indexValue = indexValue+1
            
        }
        print("fare Count : \(fareCount)")
        TopPanelRoute.addSubview(transitScrollView)
        TotalTripTime = TotalWalkingTime + TotalTransitTime
        TotalTripDistance = TotalWalkingDistance + TotalTransitDistance
        TotalCost = fareCount
        print(TransitType)
         print("\(TotalWalkingTime) Total Walking Time")
        print("\(TotalTransitTime) Total Transit Time")
        print("\(TotalWalkingDistance) Total Walking Distance")
         print("\(TotalTransitDistance) Total Walking Distance")
        print("\(NumberOfTransit) Number of Transit")
    
        
        analytics.getActivity(Activity_Type: "Trip Planner", Source_Address: self.SourceAddressString!, Destination_Address: self.DestinationAddressString!, Amenitites: self.ameniticsTitle!, Source_placeid: self.sourcePlaceID!, Destination_placeid: self.DestinationPlaceID!, Total_walktime: TotalWalkingTime, Total_walkingdistance: TotalWalkingDistance, Total_transitdistance: Double(TotalTransitDistance), NumberofTransit: NumberOfTransit, Total_triptime: TotalTripTime!, Total_transittime: TotalTransitTime, Total_tripdistance: Double(TotalTripDistance!), highcost: TotalCost!, Application: 2, Firstmile: 0, Amenitites_Category: AmenititesType)
        
        
        
        
    }
    func getDirectionFromGoogle(){
        
        googlereponseDirection.removeAll()
        let valueResult = dataPrevRespone.result.value
        let routesPlan = valueResult?.routes![indexValue].legs
        let stepValueArray = routesPlan![0].steps!
        // print(Any)
        for (index,stepValue) in stepValueArray.enumerated() {
            
            
            
            DirectionCollect(Passingdata: stepValue,index:index, completion: { (success, content) in
                
                print(success)
                
                print(content!)
                print(self.googlereponseDirection)
                
                self.completeFunction(ArrayCount: "\(success)")
                // self.googlereponseDirection.add(content!)
                //print(self.googlereponseDirection)
                
            })
            
            
            
            
        }
    }
    func DirectionCollect(Passingdata:SuggestStep,index:Int,completion : @escaping (Bool, Any?) -> Void) {
        var routeNoColor = ""
        
        let valueResult = dataPrevRespone.result.value
        let routesPlan = valueResult?.routes![indexValue].legs
        // let stepValueArray = routesPlan![0].steps!
        //       // print(Any)
        //        for (index,Passingdata) in stepValueArray.enumerated() {
        //
        
        
        if Passingdata.travelMode == "TRANSIT"
        {
           //"\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/
            
            let url1 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetTripSchedulemobileapi"
            
            let parameters1: Parameters = [
                "Headsign": Passingdata.transitDetails?.headsign ?? "nil",
                "Routeid": Passingdata.transitDetails?.line?.shortName ?? "nil",
                "StartStop": Passingdata.transitDetails?.departureStop?.name ?? "nil",
                "EndStop": Passingdata.transitDetails?.arrivalStop?.name ?? "nil",
                "DepartureTime": Passingdata.transitDetails?.departureTime?.text ?? "nil",
                "StartStopLat": Passingdata.transitDetails?.departureStop?.location?.lat ?? "nil",
                "StartStopLong": Passingdata.transitDetails?.departureStop?.location?.lng ?? "nil",
                "EndStopLat": Passingdata.transitDetails?.arrivalStop?.location?.lat ?? "nil",
                "EndStopLong": Passingdata.transitDetails?.arrivalStop?.location?.lng ?? "nil",
                //"Day": "Friday"
            ]
            
            
            Alamofire.request(url1, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<TransitRouteClass>) in
                    switch response.result {
                    case .success:
                        print(response.result.value?.triplist as Any)
                        if response.result.value?.triplist != nil{
                            if !(response.result.value?.triplist?.isEmpty)!
                            {
                                let googlereponseDirectionList : NSMutableDictionary = [:]
                                var waitingTravel = ""
                                self.realtimeDataResponse = response
                                
                                let DepartureStop = (self.realtimeDataResponse.result.value?.triplist![0].departureStop)!
                                let DepartTimeRealTime = (self.realtimeDataResponse.result.value?.triplist![0].departureTime)!
                                
                                let ArrivalStopRealtime = (self.realtimeDataResponse.result.value?.triplist![0].arrivalStop)!
                                let ArrivalTimeRealTime = (self.realtimeDataResponse.result.value?.triplist![0].arrivalTime)!
                                
                                let arrival_time = (Passingdata.transitDetails?.arrivalTime?.text!)!
                                let departure_time = (Passingdata.transitDetails?.departureTime?.text!)!
                                
                                let departdateNew = self.unixtodate(unixtimeInterval:(Passingdata.transitDetails?.departureTime?.value!)!)
                                let arivalDateNew = self.unixtodate(unixtimeInterval:(Passingdata.transitDetails?.arrivalTime?.value!)!)
                                print(departdateNew)
                                
                                let FirstAddressGoogle = Passingdata.htmlInstructions!
                                var SecondDescription = "Departure: \(DepartureStop) - \(self.DateFormatcheck(Data24Hr: DepartTimeRealTime)) (\(departdateNew)) "
                                var ArrivalAddress = "Arrival: \(ArrivalStopRealtime) - \(self.DateFormatcheck(Data24Hr: ArrivalTimeRealTime)) (\(arivalDateNew))"
                                let TransitRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                                let TransitTime = "\(departure_time) - \(arrival_time)"
                                print(SecondDescription)
                                
                                if index > 1{
                                    if index == 0 {
                                        
                                        if Passingdata.travelMode == routesPlan![0].steps![index+1].travelMode {
                                            
                                            let Firstbus = (routesPlan![0].steps![index].transitDetails?.arrivalTime?.value)!
                                            let secondBus = (routesPlan![0].steps![index+1].transitDetails?.departureTime?.value)!
                                            let waitingtimeResult = self.datasubraction(arrivalDate: Firstbus, DepartDate: secondBus, Walkingmin: 0)
                                            waitingTravel = "Wait time \(waitingtimeResult)"
                                        }
                                        
                                        
                                    }else{
                                        if Passingdata.travelMode == routesPlan![0].steps![index-1].travelMode {
                                            
                                            let Firstbus = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.value)!
                                            let secondBus = (routesPlan![0].steps![index].transitDetails?.departureTime?.value)!
                                            let waitingtimeResult = self.datasubraction(arrivalDate: Firstbus, DepartDate: secondBus, Walkingmin: 0)
                                            waitingTravel = "Wait time \(waitingtimeResult)"
                                        }
                                    }
                                    
                                }else{
                                    waitingTravel = ""
                                }
                                
                                
                                
                                if self.realtimeDataResponse.result.value?.triplist![0].routeColor != nil {
                                    routeNoColor = "#\((self.realtimeDataResponse.result.value?.triplist![0].routeColor)!)"
                                    
                                }
                                else{
                                    if Passingdata.transitDetails?.line?.color != nil{
                                        routeNoColor = (Passingdata.transitDetails?.line?.color)!
                                    }else{
                                        routeNoColor = "#000000"
                                    }
                                    
                                }
                                
                                
                                let url2 = "\(APIUrl.ZIGTARCAPI)Trip/GetRealTimeVehicleandNextStops"
                                let parameters2: Parameters = [
                                    "TripID" : self.realtimeDataResponse.result.value?.triplist![0].tripID ?? "nil",
                                    "ArrivalTime" : self.realtimeDataResponse.result.value?.triplist![0].arrivalTime ?? "nil",
                                    "DepartureTime" : self.realtimeDataResponse.result.value?.triplist![0].departureTime ?? "nil",
                                    "DepartureStopName" : self.realtimeDataResponse.result.value?.triplist![0].departureStop ?? "nil",
                                    "ArrivalStopName" : self.realtimeDataResponse.result.value?.triplist![0].arrivalStop ?? "nil",
                                    
                                    ]
                                print(Passingdata.transitDetails?.line?.shortName)
                                
                                Alamofire.request(url2, method: .get, parameters: parameters2, encoding: URLEncoding.default)
                                    .responseObject{ (response: DataResponse<RealTimeRootClass>) in
                                        switch response.result {
                                        case .success:
                                            print("Lat \(String(describing: response.result.value?.lat) )" as Any)
                                            if response.result.value != nil && response.result.value?.vehicle != nil
                                            {
                                                let travelingTime = Passingdata.distance?.value
                                                let WalkingTimeArrival = Passingdata.transitDetails?.arrivalTime?.value
                                                
                                                if (response.result.value?.vehicle)! && response.result.value?.eTAminutes != "passed" {
                                                    let trippstatus = (response.result.value?.tripStatus)!
                                                    var nextBusstopRealtime = ""
                                                    if response.result.value?.busStops?.nextStops?.count != 0 {
                                                        nextBusstopRealtime = "Next Stop: \((response.result.value?.busStops?.nextStops![0].stopName)!)"
                                                    }
                                                    print(Passingdata.transitDetails?.line?.shortName!)
                                                    
                                                    if response.result.value?.departureTime != nil {
                                                        SecondDescription = "Departure: \(DepartureStop) - \((response.result.value?.departureTime)!) (\(departdateNew))"
                                                        
                                                    }
                                                    if response.result.value?.arrivalTime != nil{
                                                        ArrivalAddress = "Arrival: \(ArrivalStopRealtime) - \((response.result.value?.arrivalTime)!) (\(arivalDateNew))"
                                                    }
                                                    
                                                    googlereponseDirectionList.setValue("true", forKey: "RealTime")
                                                    googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")
                                                    
                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                    googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                                    googlereponseDirectionList.setValue(trippstatus, forKey: "TripStatus")
                                                    googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                    googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                                    googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                                    googlereponseDirectionList.setValue(nextBusstopRealtime, forKey: "NextStop")
                                                    googlereponseDirectionList.setValue(travelingTime, forKey: "TravelingTime")
                                                    googlereponseDirectionList.setValue(WalkingTimeArrival, forKey: "WalkingCalArrival")
                                                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")
                                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String : Any])
                                                    self.ii = self.ii + 1
                                                    
                                                    if AnalyticsVar.ActivityID != nil {
//                                                        let tripID = Int((self.realtimeDataResponse.result.value?.triplist![0].tripID)!)
//                                                let Activity_Type = AnalyticsVar.ActivityID
//
//                                let ETA_arrival_time = response.result.value?.arrivalTime
//
//                                let ETA_depature_time = response.result.value?.departureTime
//                                let Delay = trippstatus
//                                let stop_id = response.result.value?.stopID
//                                let stop_name = ArrivalAddress
//
//            analytics.GetTransitRunningStatus(trip_id: tripID!, ETA_arrival_time: ETA_arrival_time!, ETA_depature_time: ETA_depature_time!, Delay: trippstatus, stop_id: stop_id!, stop_name: stop_name, Activityid: Activity_Type!)
                                                    
                                
                                }
                                                    
                                                    completion(true,googlereponseDirectionList)
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                }
                                                    
                                                else{
                                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                    googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")
                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                    googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                                    googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                    googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                                    googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                    googlereponseDirectionList.setValue(travelingTime, forKey: "TravelingTime")
                                                    googlereponseDirectionList.setValue(WalkingTimeArrival, forKey: "WalkingCalArrival")
                                                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")
                                                    
                                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String : Any])
                                                    self.ii = self.ii + 1
                                                    completion(false,googlereponseDirectionList)
                                                }
                                            }
                                            else{
                                                
                                                
                                                googlereponseDirectionList.setValue("falses", forKey: "RealTime")
                                                googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")
                                                googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                                
                                                googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                                googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                                googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                                                self.googlereponseDirection.append(googlereponseDirectionList as! [String : Any])
                                                self.ii = self.ii + 1
                                                completion(false,googlereponseDirectionList)
                                                
                                            }
                                            
                                            
                                        case .failure(let error):
                                            
                                            print(error)
                                            googlereponseDirectionList.setValue("falses", forKey: "RealTime")
                                            googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")
                                            googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                            googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                            googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                            
                                            googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                            googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                            googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                            googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                            googlereponseDirectionList.setValue("", forKey: "NextStop")
                                            googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                            googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                                            self.googlereponseDirection.append(googlereponseDirectionList as! [String : Any])
                                            self.ii = self.ii + 1
                                            completion(false,googlereponseDirectionList)
                                            
                                            
                                            
                                        }
                                        
                                }
                                
                            }
                            else{
                                let departdateNew = self.unixtodate(unixtimeInterval:(Passingdata.transitDetails?.departureTime?.value!)!)
                                let arivalDateNew = self.unixtodate(unixtimeInterval:(Passingdata.transitDetails?.arrivalTime?.value!)!)
                                let googlereponseDirectionList : NSMutableDictionary = [:]
                                
                                let arrival_time = (Passingdata.transitDetails?.arrivalTime?.text!)!
                                let departure_time = (Passingdata.transitDetails?.departureTime?.text!)!
                                var google_color = ""
                                
                                let FirstAddressGoogle = Passingdata.htmlInstructions!
                                let SecondDescription = "Departure: \((Passingdata.transitDetails?.departureStop?.name)!) - \((Passingdata.transitDetails?.departureTime?.text)!) (\(departdateNew))"
                                
                                let ArrivalAddress = "Arrival: \((Passingdata.transitDetails?.arrivalStop!.name)!) - \((Passingdata.transitDetails?.arrivalTime?.text)!) (\(arivalDateNew))"
                                var TransitRouteNo  = ""
                                if Passingdata.transitDetails?.line?.shortName != nil {
                                    TransitRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                                }
                                else {
                                    TransitRouteNo = "\((Passingdata.transitDetails?.line?.name)!)"
                                }
                                let TransitTime = "\(departure_time) - \(arrival_time)"
                                print(SecondDescription)
                                
                                if Passingdata.transitDetails!.line?.color != nil {
                                    google_color = (Passingdata.transitDetails!.line?.color)!
                                }
                                else{
                                    google_color = "\(color.hexStringToUIColor(hex: "#000000"))"
                                    
                                }
                                
                                googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                googlereponseDirectionList.setValue("\(google_color)", forKey: "RouteLineColor")
                                googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                
                                googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                                self.googlereponseDirection.append(googlereponseDirectionList as! [String : Any])
                                self.ii = self.ii + 1
                                completion(false,googlereponseDirectionList)
                            }
                        }
                        
                        
                    case .failure(let error):
                        let departdateNew = self.unixtodate(unixtimeInterval:(Passingdata.transitDetails?.departureTime?.value!)!)
                        let arivalDateNew = self.unixtodate(unixtimeInterval:(Passingdata.transitDetails?.arrivalTime?.value!)!)
                        let googlereponseDirectionList : NSMutableDictionary = [:]
                        
                        let arrival_time = (Passingdata.transitDetails?.arrivalTime?.text!)!
                        let departure_time = (Passingdata.transitDetails?.departureTime?.text!)!
                        var google_color = ""
                        
                        let FirstAddressGoogle = Passingdata.htmlInstructions!
                        let SecondDescription = "Departure: \((Passingdata.transitDetails?.departureStop?.name)!) - \((Passingdata.transitDetails?.departureTime?.text)!) (\(departdateNew))"
                        
                        let ArrivalAddress = "Arrival: \((Passingdata.transitDetails?.arrivalStop!.name)!) - \((Passingdata.transitDetails?.arrivalTime?.text)!) (\(arivalDateNew))"
                        var TransitRouteNo  = ""
                        if Passingdata.transitDetails?.line?.shortName != nil {
                            TransitRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                        }
                        else {
                            TransitRouteNo = "\((Passingdata.transitDetails?.line?.name)!)"
                        }
                        let TransitTime = "\(departure_time) - \(arrival_time)"
                        print(SecondDescription)
                        
                        if Passingdata.transitDetails!.line?.color != nil {
                            google_color = (Passingdata.transitDetails!.line?.color)!
                        }
                        else{
                            google_color = "\(color.hexStringToUIColor(hex: "#000000"))"
                            
                        }
                        
                        googlereponseDirectionList.setValue("false", forKey: "RealTime")
                        googlereponseDirectionList.setValue("\(google_color)", forKey: "RouteLineColor")
                        googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                        googlereponseDirectionList.setValue("", forKey: "TripStatus")
                        googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                        
                        googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                        googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                        googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                        googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                        googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                        googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                        googlereponseDirectionList.setValue("", forKey: "NextStop")
                        googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                        googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                        self.googlereponseDirection.append(googlereponseDirectionList as! [String : Any])
                        self.ii = self.ii + 1
                        completion(false,googlereponseDirectionList)
                        
                        
                        print(error)
                    }
                    
                    
            }
            
        }
        else{
            let googlereponseDirectionList : NSMutableDictionary = [:]
            var waitingTravel  = ""
            let userDefaults = UserDefaults.standard
            let distanceValue = (Passingdata.distance?.value)!
            let DurationWalking = (Passingdata.duration?.value)!
            var waitingtime = ""
            
            print((Passingdata.distance?.value)!)
            var durationString = 0
            var getstime = ""
            var uberwaitingtime = ""
            var arrivaltime:Double = 0.0
            
            
            
            var TransitTime = ""
            
            
            let walkingmin = (Passingdata.duration?.text)!
            let walkingmiles = (Passingdata.distance?.text)!
            let walkingdistancevalue:Double = Double((Passingdata.distance?.value)!)
            let caloriesburn = (walkingdistancevalue * 1.25) / 20
            let caloriesburnint:Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
            // cell.TransitRouteNo.isHidden = true
            // cell.TransitTime.frame.origin.
            // let walkingarriaval = walkingdeparttime
            if index == 0 {
                
                let walkingstart = (routesPlan![0].steps![index+1].transitDetails?.departureTime?.value)!
                let walkingminepoch = (Passingdata.duration?.value)
                let walkingmincal = walkingminepoch
                waitingTravel = "No wait time."
                let getstime = dateconvert(Datevalue: walkingstart,WalkingTimeinterval:walkingmincal!, addsub: "sub")
                TransitTime = "\(getstime)"
                arrivaltime = walkingstart
                //cell.TransitTime.text = "\(getstime)"
            }
            else if index ==  (dataPrevRespone.result.value?.routes![indexValue].legs![0].steps?.count)!-1{
                
                let walkingstart = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.value)!
                let walkingminepoch = (Passingdata.duration?.value)
                let walkingmincal = walkingminepoch
                let getstime = dateconvert(Datevalue: walkingstart,WalkingTimeinterval:walkingmincal!, addsub: "add")
                //cell.TransitTime.text = "\(getstime)"
                TransitTime = "\(getstime)"
                arrivaltime = walkingstart
                
                waitingTravel = "No wait time."
            }
            else{
                var walkstringtoint = ""
                //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                let walkingminepoch = (Passingdata.duration?.value)
                let walkingmincal = walkingminepoch
                if walkingmin.contains("mins"){
                    walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                }
                else{
                    walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                }
                let walkminInt:Int = Int(walkstringtoint)! * 60
                print(walkminInt)
                let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.value)!
                let getstime = self.dateconvert(Datevalue: walkingend,WalkingTimeinterval:walkingmincal!, addsub: "add")
                TransitTime = "\(getstime)"
                arrivaltime = walkingend
                let waitingbus = (routesPlan![0].steps![index+1].transitDetails?.departureTime?.value)!
                
                let waitingtimeResult = datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                waitingTravel = "Wait time \(waitingtimeResult)"
                
            }
            
            let FirstAddressGoogle = Passingdata.htmlInstructions
            let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"
            
            googlereponseDirectionList.setValue("false", forKey: "RealTime")
            googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
            googlereponseDirectionList.setValue("", forKey: "TripStatus")
            googlereponseDirectionList.setValue("\(index)", forKey: "Index")
            googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
            googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
            googlereponseDirectionList.setValue("", forKey: "RouteLine")
            googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
            googlereponseDirectionList.setValue("", forKey: "NextStop")
            googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
            googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
            googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
            googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")
            
            
            self.googlereponseDirection.append(googlereponseDirectionList as! [String : Any])
            
            print(googlereponseDirectionList)
            self.ii = self.ii + 1
            completion(false,googlereponseDirectionList)
            
            
            
            
        }
        
    }
    
    func completeFunction(ArrayCount:String){
        let valueResult = dataPrevRespone.result.value
        let routesPlan = valueResult?.routes![indexValue].legs
        let stepValueArraycount = routesPlan![0].steps?.count
        
        if stepValueArraycount == self.ii {
            
            // let dict = self.googlereponseDirection
            
            
            self.googlereponseDirection.sort(by: { ($0["Index"] as! String) < $1["Index"] as! String })
            
            print(googlereponseDirection)
            print("&*&*&*&*&*&*&*")
            self.hideLoader()
            
            self.tableView.reloadData()
        }
        
        
    }
    @objc func OnBackClicked() {
        self.navigationController?.popViewController(animated: true)
        
        if(tankrealTimeTimer != nil)
        {
            tankrealTimeTimer.invalidate()
        }
    }
    
    @objc func OnMenuClicked(){
        
        if ticketMenuCheck{
            menuButton.isEnabled = false
            ticketMenuCheck = false
        }
        
           analytics.GetPageHitCount(PageName: "Buy Ticket")
        
        //doneBtn.isHidden = true
        let valueResult = dataPrevRespone.result.value
        let routesPlan = valueResult?.routes![indexValue].legs
        backgroundBlurView1.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        backgroundBlurView1.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(backgroundBlurView1)
        // let stepValue = routesPlan![0].steps![indexPath.row]
        stepArray.removeAllObjects()
        stepArrayFare.removeAllObjects()
        
        for stepValue in routesPlan![0].steps!
        {
            let distanceValue = (stepValue.distance?.value)!
            if stepValue.travelMode == "TRANSIT"
            {
                self.subTotalFare = 0.0
                
                transitCount = transitCount + 1
                if stepValue.transitDetails?.line?.shortName != nil
                {
                    if stepValue.transitDetails?.line?.agencies![0].name == "Transit Authority of River City"
                    {
                        stepArray.add("Transit \(stepValue.transitDetails?.line?.shortName ?? "TARC") ")
                        
                        if stepValue.transitDetails?.line?.shortName  != nil{
                            if (stepValue.transitDetails?.line?.shortName?.contains("X"))! {
                                
                                stepArrayFare.add("$2.75")
                                MyTarcFare.add("$2.50")
                                self.TotalFar = self.TotalFar + 2.75
                                self.MyTarcCardTotalFare =  2.50
                                self.MyTarcCardTotalFareArray.append(2.50)
                            }
                            else{
                                stepArrayFare.add("$1.75")
                                MyTarcFare.add("$1.50")
                                self.TotalFar = self.TotalFar + 1.75
                                self.MyTarcCardTotalFare =  1.50
                                self.MyTarcCardTotalFareArray.append(1.50)
                            }
                        }
                        else{
                            stepArrayFare.add("$1.75")
                            MyTarcFare.add("$1.50")
                            self.TotalFar = self.TotalFar + 1.75
                            self.MyTarcCardTotalFare =  1.50
                            self.MyTarcCardTotalFareArray.append(1.50)
                        }
                    }
                    else
                    {
                        stepArray.add("Transit \(stepValue.transitDetails?.line?.shortName ?? "TARC")")
                        if stepValue.transitDetails?.line?.shortName  != nil {
                            if (stepValue.transitDetails?.line?.shortName?.contains("X"))! {
                                
                                stepArrayFare.add("$2.75")
                                MyTarcFare.add("$2.50")
                                self.TotalFar = self.TotalFar + 2.75
                                self.MyTarcCardTotalFare = 2.50
                                self.MyTarcCardTotalFareArray.append(2.50)
                            }
                            else{
                                stepArrayFare.add("$1.75")
                                MyTarcFare.add("$1.50")
                                self.TotalFar = self.TotalFar + 1.75
                                self.MyTarcCardTotalFare =  1.50
                                self.MyTarcCardTotalFareArray.append(1.50)
                            }
                        }
                        else{
                            stepArrayFare.add("$1.75")
                            MyTarcFare.add("$1.50")
                            self.TotalFar = self.TotalFar + 1.75
                            self.MyTarcCardTotalFare =  1.50
                            self.MyTarcCardTotalFareArray.append(1.50)
                        }
                    }
                }
                else if  stepValue.transitDetails?.line?.name != nil
                {
                    if stepValue.transitDetails?.line?.agencies![0].name == "Transit Authority of River City"
                    {
                        stepArray.add("Transit \(stepValue.transitDetails?.line?.name ?? "TARC")" )
                        if stepValue.transitDetails?.line?.shortName  != nil {
                            if (stepValue.transitDetails?.line?.shortName?.contains("X"))!  {
                                
                                stepArrayFare.add("$2.75")
                                MyTarcFare.add("$2.50")
                                self.TotalFar = self.TotalFar + 2.75
                                self.MyTarcCardTotalFare =   2.50
                                self.MyTarcCardTotalFareArray.append(2.50)
                            }
                            else{
                                stepArrayFare.add("$1.75")
                                MyTarcFare.add("$1.50")
                                self.TotalFar = self.TotalFar + 1.75
                                self.MyTarcCardTotalFare = 1.50
                                self.MyTarcCardTotalFareArray.append(1.50)
                            }
                        }
                            
                        else{
                            stepArrayFare.add("$1.75")
                            MyTarcFare.add("$1.50")
                            self.TotalFar = self.TotalFar + 1.75
                            self.MyTarcCardTotalFare = 1.50
                            self.MyTarcCardTotalFareArray.append(1.50)
                        }
                    }
                    else
                    {
                        stepArray.add("Transit \(stepValue.transitDetails?.line?.name ?? "TARC")" )
                        if stepValue.transitDetails?.line?.shortName  != nil {
                            if (stepValue.transitDetails?.line?.shortName?.contains("X"))! {
                                
                                stepArrayFare.add("$2.75")
                                MyTarcFare.add("$2.50")
                                self.TotalFar = self.TotalFar + 2.75
                                self.MyTarcCardTotalFare = 2.50
                                self.MyTarcCardTotalFareArray.append(2.50)
                            }
                            else{
                                stepArrayFare.add("$1.75")
                                MyTarcFare.add("$1.50")
                                self.TotalFar = self.TotalFar + 1.75
                                self.MyTarcCardTotalFare = 1.50
                                self.MyTarcCardTotalFareArray.append(1.50)
                            }
                        }
                        else{
                            stepArrayFare.add("$1.75")
                            MyTarcFare.add("$1.50")
                            self.TotalFar = self.TotalFar + 1.75
                            self.MyTarcCardTotalFare = 1.50
                            self.MyTarcCardTotalFareArray.append(1.50)
                        }
                    }
                }
                else
                {
                    stepArray.add("Ticket for this route")
                }
                
                
            }
            else if distanceValue > 805 && UserDefaults.standard.bool(forKey: "isCab")
            {
                
                stepArray.add("Transit Uber")
                stepArrayFare.add("0.0")
                MyTarcFare.add("0.0")
                
            }
        }
        
        print("Transit Count:\(transitCount)")
        UIView.animate(withDuration: 1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        if UIDevice.current.userInterfaceIdiom == .pad
                        {
                            self.menuBackView.frame = CGRect.init(x: Int((self.view.frame.size.width/2))-self.ticketMenuX, y:Int((self.view.frame.size.height/3))-self.ticketMenuY , width: self.ticketMenuWidth, height: Int(self.view.frame.size.height)-self.ticketMenuY)
                            
                        }
                        else
                        {
                            
                            self.menuBackView.frame = CGRect.init(x: self.ticketMenuX, y: self.ticketMenuY, width: Int(self.view.frame.size.width) - self.ticketMenuWidth, height: Int(self.view.frame.size.height)-70)
                            
                            
                        }
                        self.backgroundBlurView1.addSubview(self.menuBackView)
                        
        }, completion: { (finished) -> Void in
            let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.closeBtnWidth, height: self.closeBtnWidth))
            closeButton.setImage(UIImage(named: "closeBtn"), for: .normal)
            closeButton.addTarget(self, action: #selector(self.closebuttonAction), for: .touchUpInside)
            self.menuBackView.addSubview(closeButton)
            
            let profileImage = UIImageView()
            profileImage.frame = CGRect.init(x: (Int(self.menuBackView.frame.size.width/2)) - self.profileImageSize-10, y: self.profileImageSize, width: self.profileImageSize * 2, height: self.profileImageSize * 2)
            let userDefaults1 = UserDefaults.standard
            profileImage.layer.borderWidth=1.0
            profileImage.layer.masksToBounds = false
            profileImage.layer.borderColor = UIColor.black.cgColor
            profileImage.layer.cornerRadius = profileImage.frame.size.height/2
            profileImage.clipsToBounds = true
            // self.menuBackView.addSubview(profileImage)
            
            let userNameLabel = UILabel()
            userNameLabel.frame = CGRect.init(x: 0, y: self.profileImageSize-20, width: Int(self.menuBackView.frame.width), height: 20)
            let usernameString : String = userDefaults1.value(forKey: "userName") as? String ?? " "
            if usernameString != ""
            {
                userNameLabel.text = usernameString.uppercased()
            }
            else
            {
                userNameLabel.text = ""
            }
            userNameLabel.textColor = UIColor.black
            userNameLabel.font = UIFont.setTarcRegular(size: 18.0)
            userNameLabel.textAlignment = NSTextAlignment.center
            self.menuBackView.addSubview(userNameLabel)
            // self.menuTableView = UITableView()
            self.menuTabelView.register(TripSelectionTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
            if UIScreen.main.sizeType == .iPhone5{
                self.screenheight = 140
            }
            else {
                self.screenheight = 150
                
            }
            self.menuTabelView.frame = CGRect.init(x: 0, y: Int(userNameLabel.frame.size.height + userNameLabel.frame.origin.y + 10), width: Int(self.menuBackView.frame.width), height: Int(self.menuBackView.frame.height)-self.screenheight!)
            self.menuTabelView.delegate = self
            self.menuTabelView.dataSource = self
            self.menuTabelView.backgroundColor = UIColor.white
            self.menuBackView.addSubview(self.menuTabelView)
            
            
            self.TotalFareLabel.backgroundColor = UIColor.black
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.TotalFareLabel.frame = CGRect.init(x: 0, y: Int(self.menuBackView.frame.size.height + userNameLabel.frame.origin.y - 90), width: Int(self.menuBackView.frame.width), height: 60)
            }
            else{
                self.TotalFareLabel.frame = CGRect.init(x: 0, y: Int(self.menuBackView.frame.size.height + userNameLabel.frame.origin.y - 80), width: Int(self.menuBackView.frame.width), height: 60)
            }
            self.menuBackView.addSubview(self.TotalFareLabel)
            let BuyNowLbl = UILabel()
            BuyNowLbl.text = "Visit MyTARC"
            BuyNowLbl.textColor = UIColor.white
            BuyNowLbl.textAlignment = .center
            BuyNowLbl.font = UIFont.setTarcBold(size: 16.0)
            BuyNowLbl.frame = CGRect.init(x: Int(self.TotalFareLabel.frame.origin.x), y:2, width: Int(self.TotalFareLabel.frame.width), height: Int(self.TotalFareLabel.frame.height))
            self.TotalFareLabel.addSubview(BuyNowLbl)
            let PaymentMethod = UILabel()
            let creditcardLbl = UILabel()
            PaymentMethod.text = "Credit Card"
            creditcardLbl.text = "XXXX XXXX XXXX 8843"
            
            let Creditcardview = UIView()
            let totalPriceView = UIView()
            
            let Totallbl = UILabel()
            
            totalPriceView.frame = CGRect.init(x: 0, y: Int(self.menuBackView.frame.size.height + userNameLabel.frame.origin.y - 140), width: Int(self.menuBackView.frame.width), height: 60)
            totalPriceView.backgroundColor = UIColor.lightGray
            self.menuBackView.addSubview(totalPriceView)
            
            Creditcardview.frame = CGRect.init(x: 0, y: Int(self.menuBackView.frame.size.height + userNameLabel.frame.origin.y - 140), width: Int(self.menuBackView.frame.width), height: 60)
            Creditcardview.backgroundColor = UIColor.white
            // self.menuBackView.addSubview(Creditcardview)
            
            
            
            PaymentMethod.frame = CGRect.init(x: Int(Creditcardview.frame.origin.x) + 20 , y:2, width: 100, height: Int(Creditcardview.frame.height))
            creditcardLbl.frame = CGRect.init(x: Int(PaymentMethod.frame.origin.x + PaymentMethod.frame.width + 30), y:2, width: 200, height: Int(PaymentMethod.frame.height))
            
            Creditcardview.addSubview(creditcardLbl)
            Creditcardview.addSubview(PaymentMethod)
            let MytarcImage = UIImageView()
            
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                Totallbl.frame = CGRect.init(x:20, y: 0, width: totalPriceView.frame.size.width-140, height: totalPriceView.frame.size.height)
                self.priceTotal.frame = CGRect.init(x: totalPriceView.frame.size.width-190, y: 0, width: 60, height:totalPriceView.frame.size.height)
                
                self.MytarcPricetotal.frame = CGRect.init(x: totalPriceView.frame.size.width-120, y: 0, width: 60, height: totalPriceView.frame.size.height)
                MytarcImage.frame = CGRect.init(x: totalPriceView.frame.size.width-60, y: 10, width: 40, height: 30)
                
            }
            else
            {
                Totallbl.frame = CGRect.init(x:30, y: 0, width: totalPriceView.frame.size.width-140, height: totalPriceView.frame.size.height)
                self.priceTotal.frame = CGRect.init(x: totalPriceView.frame.size.width-200, y: 0, width: 120, height: totalPriceView.frame.size.height)
                self.MytarcPricetotal.frame = CGRect.init(x: totalPriceView.frame.size.width-120, y: 0, width: 120, height: totalPriceView.frame.size.height)
                
                MytarcImage.frame = CGRect.init(x: totalPriceView.frame.size.width-60, y: 10, width: 50, height: 30)
                
            }
            MytarcImage.image = UIImage(named: "MyTarc")
            totalPriceView.addSubview(MytarcImage)
            Totallbl.text = "Total"
            Totallbl.font = UIFont.setTarcBold(size: 16.0)
            totalPriceView.addSubview(Totallbl)
            totalPriceView.addSubview(self.MytarcPricetotal)
            self.MytarcPricetotal.font =  UIFont.setTarcBold(size: 16.0)
            self.priceTotal.font =  UIFont.setTarcBold(size: 16.0)
            totalPriceView.addSubview(self.priceTotal)
            
            let uberActionFun = UITapGestureRecognizer(target: self, action: #selector(self.tapUberButtonCon(_:)))
            uberActionFun.numberOfTapsRequired = 1
            
            self.TotalFareLabel.addGestureRecognizer(uberActionFun)
            
        })
    }
    @objc func closebuttonAction(sender: UIButton!) {
        print("Button tapped")
        menuButton.isEnabled = true
        // doneBtn.isHidden = false
        backgroundBlurView1.removeFromSuperview()
        UIView.animate(withDuration: 1,
                       delay: 0.1,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        
                        self.menuBackView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.size.width-80, height: self.view.frame.size.height-120)
        }, completion: { (finished) -> Void in
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == menuTabelView {
            return stepArray.count
        }
        else
        {
            let resultRes = dataPrevRespone.result.value?.routes![indexValue].legs![0].steps?.count
            return googlereponseDirection.count;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // print("\(googlereponseDirection)-hepsi")
        //  let cell = tableView.cellForRow(at: indexPath) as! SuggrouteCellfstepValue
        
        if tableView == menuTabelView {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! TripSelectionTableViewCell
            tableView.separatorColor = UIColor.clear;
            //cell.separatorInset = .zero
            //
            if indexPath.row == stepArray.count{
                // cell.backgroundColor = UIColor.black
                
            }
            if indexPath.row < stepArray.count{
                
                print("Step Array: \(stepArray[indexPath.row] as! String)")
                if stepArray[indexPath.row] as! String == "Transit Uber"
                {
                    
                    let behavior = RideRequestViewRequestingBehavior(presentingViewController: self)
                    // Optional, defaults to using the user’s current location for pickup
                    
                    let builder = RideParametersBuilder()
                    
                    //                        let valueResult = self.dataPrevRespone.result.value
                    //                        let routesPlan = valueResult?.routes![self.indexValue].legs
                    let resultRes = dataPrevRespone.result.value?.routes![indexValue].legs![0];
                    for stepValue in resultRes!.steps!
                    {
                        cell.prepareForReuse()
                        print("Sec Index ---> \(secIndex)")
                        let distanceValue = (stepValue.distance?.value)!
                        print((stepValue.distance?.value)!)
                        if stepValue.htmlInstructions! == secIndex && distanceValue > 805
                        {
                            if UserDefaults.standard.bool(forKey: "isUber"){
                                // if loggedIn?.tokenString == nil {
                                let pickupLocation = CLLocation(latitude: CLLocationDegrees((stepValue.startLocation?.lat)!), longitude: CLLocationDegrees((stepValue.startLocation?.lng)!))
                                let dropoffLocation = CLLocation(latitude: CLLocationDegrees((stepValue.endLocation?.lat)!), longitude: CLLocationDegrees((stepValue.endLocation?.lng)!))
                                
                                
                                
                                builder.pickupLocation = pickupLocation
                                builder.dropoffLocation = dropoffLocation
                                builder.dropoffNickname = "Drop Location"
                                
                                
                                let ridesClient = RidesClient()
                                ridesClient.fetchProducts(pickupLocation: pickupLocation) { products, response in
                                    guard let uberX = products.filter({$0.productGroup == .uberX}).first else {
                                        // Handle error, UberX does not exist at this location
                                        print("error")
                                        return
                                    }
                                    print(uberX.productID!)
                                    
                                    print(self.profuctid)
                                    builder.productID = uberX.productID
                                    ridesClient.fetchRideRequestEstimate(parameters: builder.build(), completion: { rideEstimate, response in
                                        //                        guard let rideEstimate = rideEstimate else {
                                        //                            print("error")
                                        //                            // Handle error, unable to get an ride request estimate
                                        //                            return
                                        //                        }
                                        DispatchQueue.main.async {
                                            print(rideEstimate?.fare! ?? " empty  ")
                                            builder.upfrontFare = rideEstimate?.fare
                                            
                                           
                                            let rideParameters = builder.build()
                                            let requestingBehavior = DeeplinkRequestingBehavior(fallbackType: .mobileWeb)
                                            
                                            cell.uberBtn = RideRequestButton(rideParameters: rideParameters, requestingBehavior: requestingBehavior)
                                            // let uberbutton = RideRequestButton(rideParameters: builder.build(), requestingBehavior: behavior)
                                            //                    let uberbutton = RideRequestButton()
                                            if UIDevice.current.userInterfaceIdiom == .phone
                                            {
                                                cell.uberBtn.frame = CGRect.init(x: 10+self.uberButtonX, y: 0, width: Int(self.menuBackView.frame.width-50) , height: 40)
                                            }
                                            else
                                            {
                                                cell.uberBtn.frame = CGRect.init(x: Int((cell.contentView.frame.size.width/2)-130), y: 0, width: self.uberBtnWidth , height: 40)
                                            }
                                            cell.uberBtn.rideParameters = builder.build()
                                            cell.uberBtn.loadRideInformation()
                                            cell.contentView.addSubview(cell.uberBtn)
                                            
                                           
                                            
                                            
                                        }
                                        
                                    })
                                    
                                }
                               
                                let rideParameters = builder.build()
                                let requestingBehavior = DeeplinkRequestingBehavior(fallbackType: .mobileWeb)
                                
                                cell.uberBtn = RideRequestButton(rideParameters: rideParameters, requestingBehavior: requestingBehavior)
                                // let uberbutton = RideRequestButton(rideParameters: builder.build(), requestingBehavior: behavior)
                                //                    let uberbutton = RideRequestButton()
                                if UIDevice.current.userInterfaceIdiom == .phone
                                {
                                    cell.uberBtn.frame = CGRect.init(x: 10+self.uberButtonX, y: 0, width: Int(self.menuBackView.frame.width-130) , height: 40)
                                }
                                else
                                {
                                    cell.uberBtn.frame = CGRect.init(x: Int((cell.contentView.frame.size.width/2)-130), y: 0, width: self.uberBtnWidth-130 , height: 40)
                                }
                                cell.uberBtn.rideParameters = builder.build()
                                cell.uberBtn.loadRideInformation()
                                cell.contentView.addSubview(cell.uberBtn)
                                
                                
//                                if UIDevice.current.userInterfaceIdiom == .phone
//                                {
//                                    //
//                                    cell.lyftButton.frame = CGRect.init(x: 10, y: 0, width:  Int(self.menuBackView.frame.width-120) , height: 40)
//                                }
//                                else
//                                {
//                                    //                                    cell.lyftButton.frame = CGRect.init(x: Int((cell.contentView.frame.size.width/2)-130), y: 0, width: Int(self.uberBtnWidth) , height: 40)
//                                    cell.lyftButton.frame = CGRect.init(x: 10, y: 0, width:  Int(self.menuBackView.frame.width-120) , height: 40)
//                                }
                                
                                let LyftFare = UILabel()
                                
                                LyftFare.text = "$6 - $8"
                                
                                LyftFare.textColor = UIColor.white
                                LyftFare.frame  = CGRect.init(x: cell.uberBtn.frame.maxX + 10, y: 0, width: 90, height: 40)
                                //  BirdFare.text = "\(NewfareString)"
                                //  UberFare.frame  = CGRect.init(x: (cell.transitScrollView.contentSize.width), y: 0, width: 80, height: 40)
                                LyftFare.font = UIFont.setTarcBold(size: 18.0)
                                LyftFare.textAlignment = NSTextAlignment.center
                                LyftFare.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
                                LyftFare.layer.cornerRadius = 5
                                LyftFare.layer.masksToBounds = true
                                cell.contentView.addSubview(LyftFare)
                                

                            }else{
                                
                                let pickupLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees((stepValue.startLocation?.lat)!), longitude: CLLocationDegrees((stepValue.startLocation?.lng)!))
                                let dropoffLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees((stepValue.endLocation?.lat)!), longitude: CLLocationDegrees((stepValue.endLocation?.lng)!))
                                cell.lyftButton = LyftButton()
                                if UIDevice.current.userInterfaceIdiom == .phone
                                {
                                    //
                                    cell.lyftButton.frame = CGRect.init(x: 10, y: 0, width:  Int(self.menuBackView.frame.width-120) , height: 40)
                                }
                                else
                                {
//                                    cell.lyftButton.frame = CGRect.init(x: Int((cell.contentView.frame.size.width/2)-130), y: 0, width: Int(self.uberBtnWidth) , height: 40)
                                     cell.lyftButton.frame = CGRect.init(x: 10, y: 0, width:  Int(self.menuBackView.frame.width-120) , height: 40)
                                }
                                
                                
                                let LyftFare = UILabel()
                                LyftFare.text = "$6 - $8"
                                comFunc.getLyftAPIFirst(SourceLat: Double((stepValue.startLocation?.lat)!), SourceLong: Double((stepValue.startLocation?.lng)!), DestinationLat: Double((stepValue.endLocation?.lat)!), DestinationLong: Double((stepValue.endLocation?.lng)!), completion: { (Price,maxprice, TravelMin, Mile, CheckBool) in
                                    if CheckBool {
                                            LyftFare.text = "$\((Price)) - $\((maxprice))"
                                    }
                                })
                                
                                
                                
                                
                                
                                LyftFare.textColor = UIColor.white
                                LyftFare.frame  = CGRect.init(x: cell.lyftButton.frame.maxX + 10, y: 0, width: 90, height: 40)
                                //  BirdFare.text = "\(NewfareString)"
                                //  UberFare.frame  = CGRect.init(x: (cell.transitScrollView.contentSize.width), y: 0, width: 80, height: 40)
                                LyftFare.font = UIFont.setTarcBold(size: 18.0)
                                LyftFare.textAlignment = NSTextAlignment.center
                                LyftFare.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
                                LyftFare.layer.cornerRadius = 5
                                LyftFare.layer.masksToBounds = true
                                cell.contentView.addSubview(LyftFare)
                                
                                cell.lyftButton.style = .hotPink
                                cell.lyftButton.configure(pickup: pickupLocation, destination: dropoffLocation)
                                cell.contentView.addSubview(cell.lyftButton)
                                
                                
                            }
                        }
                        
                        
                    }
                    
                }
                else
                {
                    
                    
                    
                    if UIDevice.current.userInterfaceIdiom == .phone
                    {
                        cell.transitNameLabel.frame = CGRect.init(x:20, y: 0, width: cell.contentView.frame.size.width-140, height: cell.contentView.frame.size.height)
                        cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width-190, y: 0, width: 60, height: cell.contentView.frame.size.height)
                        cell.mytarcFare.frame = CGRect.init(x: cell.contentView.frame.size.width-120, y: 0, width: 60, height: cell.contentView.frame.size.height)
                        cell.mytarcImage.frame = CGRect.init(x: cell.contentView.frame.size.width-60, y: 10, width: 40, height: 30)
                    }
                    else
                    {
                        cell.transitNameLabel.frame = CGRect.init(x:30, y: 0, width: cell.contentView.frame.size.width-120, height: cell.contentView.frame.size.height)
                        cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width-190, y: 0, width: 120, height: cell.contentView.frame.size.height)
                        cell.mytarcFare.frame = CGRect.init(x: cell.contentView.frame.size.width-120, y: 0, width: 120, height: cell.contentView.frame.size.height)
                        cell.mytarcImage.frame = CGRect.init(x: cell.contentView.frame.size.width-70, y: 10, width: 50, height: 30)
                    }
                    cell.transitNameLabel.text = stepArray[indexPath.row] as? String
                    cell.transitNameLabel.textColor = UIColor.black
                    cell.transitNameLabel.textAlignment = NSTextAlignment.left
                    cell.transitNameLabel.font = UIFont.setTarcRegular(size: 15.0)
                    cell.contentView.addSubview(cell.transitNameLabel)
                    
                    let userDefaults1 = UserDefaults.standard
                    
              
                    
                    cell.transitFareLabel.text =  stepArrayFare[indexPath.row] as? String
                    cell.transitFareLabel.textColor = UIColor.black
                    cell.transitFareLabel.textAlignment = NSTextAlignment.left
                    cell.transitFareLabel.font = UIFont.setTarcRegular(size: 15.0)
                    cell.contentView.addSubview(cell.transitFareLabel)
                    
                    
                    cell.mytarcFare.text =  MyTarcFare[indexPath.row] as? String
                    cell.mytarcFare.textColor = UIColor.black
                    cell.mytarcFare.textAlignment = NSTextAlignment.left
                    cell.mytarcFare.font = UIFont.setTarcRegular(size: 15.0)
                    cell.contentView.addSubview(cell.mytarcFare)
                    
                    
                    cell.mytarcImage.image = UIImage(named: "MyTarc")
                    cell.contentView.addSubview(cell.mytarcImage)
                    
   
                }
                let TotalUbernadTransit = self.TotalFar + self.ubertotalprice
                //  cell.transitFareLabel.text =
                print(self.MyTarcCardTotalFare)
                self.priceTotal.text = String(format: "$%.2f",TotalUbernadTransit)
                
                let mytarcWithUber = self.MyTarcCardTotalFare + self.ubertotalprice
                self.MytarcPricetotal.text = String(format: "$%.2f",mytarcWithUber)
            }
            else
            {
                
                var countFare = 0
                for stepValue in stepArray
                {
                    if stepValue as! String == "Transit Uber"
                    {
                        print("Uber")
                    }
                    else
                    {
                        countFare = countFare+1
                    }
                }
                if UIDevice.current.userInterfaceIdiom == .phone
                {
                    cell.transitNameLabel.frame = CGRect.init(x:20, y: 0, width: cell.contentView.frame.size.width-140, height: cell.contentView.frame.size.height)
                    cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width-190, y: 0, width: 60, height: cell.contentView.frame.size.height)
                    cell.mytarcFare.frame = CGRect.init(x: cell.contentView.frame.size.width-120, y: 0, width: 60, height: cell.contentView.frame.size.height)
                    cell.mytarcImage.frame = CGRect.init(x: cell.contentView.frame.size.width-60, y: 10, width: 40, height: 30)
                }
                else
                {
                    cell.transitNameLabel.frame = CGRect.init(x:30, y: 0, width: cell.contentView.frame.size.width-140, height: cell.contentView.frame.size.height)
                    cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width-200, y: 0, width: 120, height: cell.contentView.frame.size.height)
                    cell.mytarcFare.frame = CGRect.init(x: cell.contentView.frame.size.width-120, y: 0, width: 120, height: cell.contentView.frame.size.height)
                    cell.mytarcImage.frame = CGRect.init(x: cell.contentView.frame.size.width-60, y: 10, width: 50, height: 30)
                }
                if countFare == 0
                {
                    cell.transitNameLabel.text = "No Tickets"
                }
                else if countFare == 1
                {
                    cell.transitNameLabel.text = "Buy ticket"
                }
                else
                {
                    cell.transitNameLabel.text = "Transit Total"
                }
                // self.ubertotalprice = self.ubertotalprice + rideEstimate.fare!.value!
                
                let TotalUbernadTransit = self.TotalFar + self.ubertotalprice
                //  cell.transitFareLabel.text =
                print(self.MyTarcCardTotalFare)
                self.priceTotal.text = String(format: "$%.2f",TotalUbernadTransit)
                //cell.contentView.addSubview(cell.transitFareLabel)
                let mytarcWithUber = self.MyTarcCardTotalFare + self.ubertotalprice
                self.MytarcPricetotal.text = String(format: "$%.2f",mytarcWithUber)
                
                
                cell.transitNameLabel.textColor = UIColor.white
                cell.transitNameLabel.textAlignment = NSTextAlignment.left
                cell.transitNameLabel.font = UIFont.setTarcBold(size: 17.0)
                // cell.contentView.addSubview(cell.transitNameLabel)
                let userDefaults1 = UserDefaults.standard
               // let fareValue : Double = userDefaults1.value(forKey: "generalFare") as! Double
                //                let fareString = String(format: "$%.2f", Double(countFare) * fareValue)
                let fareString = String(format: "$%.2f",self.TotalFar)
                cell.transitFareLabel.text = fareString
                cell.transitFareLabel.textColor = UIColor.white
                cell.transitFareLabel.textAlignment = NSTextAlignment.left
                cell.transitFareLabel.font = UIFont.setTarcBold(size: 17.0)
                // cell.contentView.addSubview(cell.transitFareLabel)
                if MyTarcCardTotalFareArray.count > 0 {
                    self.MyTarcCardTotalFare = MyTarcCardTotalFareArray.max()!
                    
                }
                else{
                    self.MyTarcCardTotalFare = 0.0
                }
                let fareStringMyTarc = String(format: "$%.2f",self.MyTarcCardTotalFare)
                cell.mytarcFare.text = fareStringMyTarc
                cell.mytarcFare.textColor = UIColor.white
                cell.mytarcFare.textAlignment = NSTextAlignment.left
                cell.mytarcFare.font = UIFont.setTarcBold(size: 17.0)
                //cell.contentView.addSubview(cell.mytarcFare)
                
                cell.mytarcImage.image = UIImage(named: "MyTarc")
                // cell.contentView.addSubview(cell.mytarcImage)
                
            }
            return cell
        }
        else{
            print("\(googlereponseDirection) - !@#$%^&*()")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "SuggrouteCell", for: indexPath) as! SuggrouteCell
            let valueResult = dataPrevRespone.result.value
            let routesPlan = valueResult?.routes![indexValue].legs
            let stepValue = routesPlan![0].steps![indexPath.row]
            //  dump(stepValue)
            print(routesPlan![0].steps?.count)
            print(googlereponseDirection.count)
            //            if routesPlan![0].steps?.count != googlereponseDirection.count {
            //                getDirectionFromGoogle()
            //            }
            cell.SecondDescription.text = ""
            
            if googlereponseDirection.count > 0{
                print(indexPath.row)
                if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "TRANSIT"{
                    cell.FullDescription.text = googlereponseDirection[indexPath.row]["FirstAddress"] as? String
                    cell.SecondDescription.text = googlereponseDirection[indexPath.row]["SecondAddress"] as? String
                    cell.TransitRouteNo.text = googlereponseDirection[indexPath.row]["RouteLine"] as? String
                    cell.TransitTime.text = googlereponseDirection[indexPath.row]["Timing"] as? String
                    cell.TransitWalkingImg.image = UIImage(named: (googlereponseDirection[indexPath.row]["TransitImage"] as? String)!)
                    
                    cell.arrivalLabel.text = googlereponseDirection[indexPath.row]["ArrivalStop"] as? String
                  //  cell.arrivalLabel.frame = CGRect.init(x: cell.SecondDescription.frame.origin.x, y: cell.SecondDescription.frame.origin.y + 30, width: cell.SecondDescription.frame.width, height: cell.SecondDescription.frame.height)
                    //cell.arrivalLabel.textColor = cell.SecondDescription.textColor
                    //cell.arrivalLabel.font = cell.SecondDescription.font
                   // cell.arrivalLabel.textAlignment = cell.SecondDescription.textAlignment
                    
                   // cell.arrivalLabel.lineBreakMode = .byWordWrapping
                   // cell.arrivalLabel.numberOfLines = 2
                    
                    //cell.contentView.addSubview(cell.arrivalLabel)
                    cell.NextStopRealtime.text = ""
                    
                    cell.NextStopRealtime.text = googlereponseDirection[indexPath.row]["NextStop"] as? String
                   // cell.NextStopRealtime.frame =  CGRect.init(x: cell.arrivalLabel.frame.origin.x, y: cell.arrivalLabel.frame.origin.y + 20, width: cell.arrivalLabel.frame.width, height: cell.arrivalLabel.frame.height)
                    //cell.NextStopRealtime.textColor = cell.arrivalLabel.textColor
                    //cell.NextStopRealtime.font = cell.arrivalLabel.font
//                    cell.NextStopRealtime.textAlignment = cell.arrivalLabel.textAlignment
//                    cell.contentView.addSubview(cell.NextStopRealtime)
                    let tripstatus = googlereponseDirection[indexPath.row]["TripStatus"] as? String
                    if (tripstatus?.contains("late"))!{
                        cell.tripstatus.textColor = color.hexStringToUIColor(hex: "#B60202")
                    }
                    else{
                        cell.tripstatus.textColor = color.hexStringToUIColor(hex: "#02B64B")
                    }
                    cell.tripstatus.text = googlereponseDirection[indexPath.row]["TripStatus"] as? String
                    
                    let routecolor = (googlereponseDirection[indexPath.row]["RouteLineColor"] as? String)!
                    cell.TransitRouteNo.textColor = color.hexStringToUIColor(hex: routecolor)
                    
                    print((googlereponseDirection[indexPath.row]["RealTime"]! as? String)!)
                    print((googlereponseDirection[indexPath.row]["RealTime"]! as? String)! == "true")
                   // cell.reailtimeicon.frame = CGRect.init(x: cell.TransitRouteNo.frame.origin.x+cell.TransitRouteNo.frame.width-30, y: cell.TransitRouteNo.frame.origin.y - 15, width:30, height: 30)
                    
                    if (googlereponseDirection[indexPath.row]["RealTime"]! as? String)! == "true"{
                        
                        cell.reailtimeicon.image = UIImage.gif(name: "wifi")
                        
                        //cell.contentView.addSubview(cell.reailtimeicon)
                    }
                    cell.traveltimeWaitTime.text = googlereponseDirection[indexPath.row]["WalkingWaitingTime"] as? String

                    
                    getalertTitle(RouteNo: (googlereponseDirection[indexPath.row]["RouteLine"] as? String)!){heading,AlertMsg,SuccessValue in
                        if SuccessValue {
                            cell.AlertImage.isHidden = false
                            if let Buttonimage = UIImage(named: "Alert") {
                                cell.AlertImage.setImage(Buttonimage, for: .normal)
                            }
                            
                      
                       // cell.AlertImage.frame = CGRect.init(x: cell.traveltimeWaitTime.frame.origin.x, y: cell.traveltimeWaitTime.frame.origin.y + 15, width: 30, height:30)
                       // cell.contentView.addSubview(cell.AlertImage)
                        
                            
                        
//                        cell.AlertText.textColor = cell.NextStopRealtime.textColor
//                        cell.AlertText.font = cell.NextStopRealtime.font
//                        cell.AlertText.textAlignment = cell.NextStopRealtime.textAlignment
                        cell.AlertText.text = heading
                       // cell.contentView.addSubview(cell.AlertText)
                            
                let AlertScreen = MyTapGesture(target: self, action:  #selector (self.AlertScreenFunc (_:)))
                   
                      cell.AlertText.addGestureRecognizer(AlertScreen)
                    cell.AlertImage.addGestureRecognizer(AlertScreen)
                            AlertScreen.title = (self.googlereponseDirection[indexPath.row]["RouteLine"] as? String)!
                            
                        }
                        else{
                            cell.AlertImage.isHidden = true
                            cell.AlertText.text = ""
                        }
                    }
                        
                        
//                        else {
//                             cell.AlertImage.isHidden = false
//                            if let Buttonimage = UIImage(named: "Alert") {
//                                cell.AlertImage.setImage(Buttonimage, for: .normal)
//                            }
//
//
//                           // cell.AlertImage.frame = CGRect.init(x: cell.traveltimeWaitTime.frame.origin.x, y: cell.traveltimeWaitTime.frame.origin.y + 15, width: 30, height:30)
//                            cell.contentView.addSubview(cell.AlertImage)
//
//
//
//                            cell.AlertText.text = heading
//                            //cell.contentView.addSubview(cell.AlertText)
//
//                            let AlertScreen = MyTapGesture(target: self, action:  #selector (self.AlertScreenFunc (_:)))
//
//                            cell.AlertText.addGestureRecognizer(AlertScreen)
//                            cell.AlertImage.addGestureRecognizer(AlertScreen)
//                            AlertScreen.title = (self.googlereponseDirection[indexPath.row]["RouteLine"] as? String)!
//
//
//                        }
                    
                    //cell.AlertText.text = getalertTitle(RouteNo: (googlereponseDirection[indexPath.row]["RouteLine"] as? String)!)
                    
                    
                } else{
                    
                    let userDefaults = UserDefaults.standard
                    cell.traveltimeWaitTime.text = ""
                    cell.arrivalLabel.text = ""
                    cell.NextStopRealtime.text = ""
                    cell.AlertImage.isHidden = true
                    cell.AlertText.text = ""
                    var UberLyftWaitingTime = 0
                    let distanceValue = googlereponseDirection[indexPath.row]["TravelingTime"] as? Int
                    //print((stepValue.distance?.value)!)
                    var durationString = 0
                    if distanceValue! > 805 && userDefaults.bool(forKey: "isCab")
                    {
                        if userDefaults.bool(forKey: "isUber"){
                            //cell.FullDescription.text =
                            
                            let aString = googlereponseDirection[indexPath.row]["FirstAddress"] as? String
                            isUberAdded = true
                            let newString = aString!.replacingOccurrences(of: "Walk", with: "Board Uber")
                            print(newString)
                            cell.FullDescription.text = newString
                            cell.TransitWalkingImg.image = UIImage(named:"uber_Car")
                            let ridesClient = RidesClient()
                            let pickupLocation = CLLocation(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
                            let dropoffLocation = CLLocation(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
                            let builder = RideParametersBuilder()
                            builder.pickupLocation = pickupLocation
                            builder.dropoffLocation = dropoffLocation
                            ridesClient.fetchTimeEstimates(pickupLocation: pickupLocation) { (estimates, response) in
                                
                                // dump(estimates)
                                if estimates.count > 0{
                                    UberLyftWaitingTime = estimates[0].estimate!
                                }
                                else{
                                    UberLyftWaitingTime = 420
                                }
                                // print(estimates[0].estimate!)
                            }
                             cell.SecondDescription.text = "Estimate Fare : $6 - $8"
                            ridesClient.fetchPriceEstimates(pickupLocation: pickupLocation, dropoffLocation: dropoffLocation, completion: {priceEstimats, response in
                                print(priceEstimats.count)
                                
                                if priceEstimats.count != 0 {
                                
                                durationString = priceEstimats[0].duration!
                                
                                if indexPath.row == 0 {
                                    
                                    let uberstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                    
                                    let getstime = self.dateconvert(Datevalue: uberstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "sub")
                                    DispatchQueue.main.async {
                                        cell.TransitTime.text = "\(getstime)"
                                        cell.SecondDescription.text = "Travel time \(self.hourminConverter(distValue: durationString)), Wait time \(self.hourminConverter(distValue: UberLyftWaitingTime))"
                                        cell.arrivalLabel.text = "Estimate Fare : $6 - $8"
                                    }
                                    
                                }
                                    
                                else if indexPath.row == self.googlereponseDirection.count-1{
                                    print(indexPath.row == self.googlereponseDirection.count-1)
                                    
                                    let uberstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                    let getstime = self.dateconvert(Datevalue: uberstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "add")
                                    DispatchQueue.main.async {
                                        cell.TransitTime.text = "\(getstime)"
                                        cell.SecondDescription.text = "Travel time \(self.hourminConverter(distValue: durationString)), Wait time \(self.hourminConverter(distValue: UberLyftWaitingTime))"
                                        cell.arrivalLabel.text = "Estimate Fare : $6 - $7"
                                    }
                                }
                                else{
                                    let uberstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                    let getstime = self.dateconvert(Datevalue: uberstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "add")
                                    DispatchQueue.main.async {
                                        cell.TransitTime.text = "\(getstime)"
                                        cell.SecondDescription.text = "Travel time \(self.hourminConverter(distValue: durationString)), Wait time \(self.hourminConverter(distValue: UberLyftWaitingTime))"
                                         cell.arrivalLabel.text = "Estimate Fare : $6 - $8"
                                    }
                                }
                                
                                
                                }
                                else{
                                    
                                }
                                
                            })
                            
                            
                        }else{
                            
                            let aString = googlereponseDirection[indexPath.row]["FirstAddress"] as? String
                            isUberAdded = true
                            let newString = aString!.replacingOccurrences(of: "Walk", with: "Board Lyft")
                            
                            cell.FullDescription.text = newString
                            cell.TransitWalkingImg.image = UIImage(named:"lyft_preference")
                            
                            
                            let pickupLocation = CLLocationCoordinate2D(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
                            let dropoffLocation = CLLocationCoordinate2D(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
                            
                            
                            LyftAPI.ETAs(to: pickupLocation) { result in
                                if result.error == nil {
                                UberLyftWaitingTime = result.value![0].minutes
                                print("\(UberLyftWaitingTime) sec-isaac")
                                }
                                else {
                                    UberLyftWaitingTime = 300
                                }
                            }
                            
                            LyftAPI.costEstimates(from: pickupLocation, to: dropoffLocation, rideKind: .Standard) { result in
                                if result.error == nil {
                                result.value?.forEach { costEstimate in
                                    print("Min: \(costEstimate.estimate!.minEstimate.amount)$")
                                    print("Max: \(costEstimate.estimate!.maxEstimate.amount)$")
                                    print("Distance: \(costEstimate.estimate!.distanceMiles) miles")
                                    print("Duration: \(costEstimate.estimate!.durationSeconds/60) minutes")
                                    durationString = costEstimate.estimate!.durationSeconds
                                    
                                    if indexPath.row == 0 {
                                        let Lyftstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                        
                                        let getstime = self.dateconvert(Datevalue: Lyftstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "sub")
                                        cell.TransitTime.text = "\(getstime)"
                                        
                                    }
                                    else if indexPath.row ==  self.googlereponseDirection.count{
                                        let Lyftstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                        let getstime = self.dateconvert(Datevalue: Lyftstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "add")
                                        cell.TransitTime.text = "\(getstime)"
                                    }
                                    else{
                                        let Lyftstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                        let getstime = self.dateconvert(Datevalue: Lyftstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "add")
                                        DispatchQueue.main.async {
                                            cell.TransitTime.text = "\(getstime)"
                                            cell.SecondDescription.text = "Travel time \(self.hourminConverter(distValue: durationString)), Waiting Time \(self.hourminConverter(distValue: UberLyftWaitingTime))"
                                        }
                                    }
                                    
                                    
                                }
                                }
                                else{
                                    
//                                    let pickupLocation = CLLocationCoordinate2D(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
//                                    let dropoffLocation = CLLocationCoordinate2D(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
//
//
                                    
                                    comFunc.getLyftAPIFirst(SourceLat: Double((stepValue.startLocation?.lat)!), SourceLong: Double((stepValue.startLocation?.lng)!), DestinationLat: Double((stepValue.endLocation?.lat)!), DestinationLong: Double((stepValue.endLocation?.lng)!), completion: { (Price,maxprice, TravelMin, Mile, CheckBool) in
                                        if CheckBool {
                                            
                                            durationString = TravelMin
                                            
                                            if indexPath.row == 0 {
                                                let Lyftstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                                
                                                let getstime = self.dateconvert(Datevalue: Lyftstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "sub")
                                                cell.TransitTime.text = "\(getstime)"
                                                cell.SecondDescription.text = "Travel time \(self.hourminConverter(distValue: durationString)), Waiting Time \(self.hourminConverter(distValue: UberLyftWaitingTime))"
                                                cell.arrivalLabel.text = "Estimate Fare : $\((Price)) - $\((maxprice))"
                                                
                                            }
                                            else if indexPath.row ==  self.googlereponseDirection.count{
                                                let Lyftstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                                let getstime = self.dateconvert(Datevalue: Lyftstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "add")
                                                cell.TransitTime.text = "\(getstime)"
                                                cell.SecondDescription.text = "Travel time \(self.hourminConverter(distValue: durationString)), Waiting Time \(self.hourminConverter(distValue: UberLyftWaitingTime))"
                                                cell.arrivalLabel.text = "Estimate Fare : $\((Price)) - $\((maxprice))"
                                            }
                                            else{
                                                let Lyftstart = self.googlereponseDirection[indexPath.row]["WalkingCalArrival"] as? Double
                                                let getstime = self.dateconvert(Datevalue: Lyftstart!,WalkingTimeinterval:durationString+UberLyftWaitingTime, addsub: "add")
                                                DispatchQueue.main.async {
                                                    cell.TransitTime.text = "\(getstime)"
                                                    cell.SecondDescription.text = "Travel time \(self.hourminConverter(distValue: durationString)), Waiting Time \(self.hourminConverter(distValue: UberLyftWaitingTime))"
                                                    cell.arrivalLabel.text = "Estimate Fare : $\((Price)) - $\((maxprice))"
                                                    
                                                }
                                            }
                                            
                                            
                                        }
                                    })
                                    
                                    
                                }
                            }
                            
                            
                            
                        }
                        
                        
                        
                    }
                    else{
                        
                        
                        cell.TransitRouteNo.text = ""
                        //cell.reailtimeicon.image = nil
                        cell.tripstatus.text = ""
                        cell.NextStopRealtime.text = ""
                        //cell.TransitTime.frame.origin.y -= 60
                        cell.TransitTime.frame = CGRect.init(x: cell.TransitRouteNo.frame.origin.x, y: cell.TransitRouteNo.frame.origin.y, width:cell.TransitTime.frame.width, height: cell.TransitTime.frame.height)
                        cell.FullDescription.text = googlereponseDirection[indexPath.row]["FirstAddress"] as? String
                        cell.SecondDescription.text = googlereponseDirection[indexPath.row]["SecondAddress"] as? String
                        cell.TransitTime.text = googlereponseDirection[indexPath.row]["Timing"] as? String
                        cell.TransitWalkingImg.image = UIImage(named: (googlereponseDirection[indexPath.row]["TransitImage"] as? String)!)
                        cell.traveltimeWaitTime.text = ""
                        cell.arrivalLabel.frame = CGRect.init(x: cell.SecondDescription.frame.origin.x, y: cell.SecondDescription.frame.origin.y + 15, width: cell.SecondDescription.frame.width, height: cell.SecondDescription.frame.height)
                        cell.arrivalLabel.textColor = cell.SecondDescription.textColor
                        cell.arrivalLabel.font = cell.SecondDescription.font
                        cell.arrivalLabel.textAlignment = cell.SecondDescription.textAlignment
                        cell.arrivalLabel.text = googlereponseDirection[indexPath.row]["WalkingWaitingTime"] as? String
                        
                        cell.contentView.addSubview(cell.arrivalLabel)
                        
                        
                        
                    }
                }
                
                
                
            }

            return cell
        }
    }
   @objc func AlertScreenFunc(_ sender:MyTapGesture){
    print(sender.title)
    getalertTitle(RouteNo: sender.title) { (AlertList, AlertString, AlertBool) in
        if AlertBool {
            let alertViewController = NYAlertViewController()
            alertViewController.title = "Alert"
            alertViewController.message = AlertList
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            
            
            let cancelAction = NYAlertAction(
                title: "OK",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    
                    self.dismiss(animated: true, completion: nil)
            }
            )
            
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != menuTabelView{
            
            print("click")
            let distanceValue = googlereponseDirection[indexPath.row]["TravelingTime"] as? Int
            //print((stepValue.distance?.value)!)
            let userDefaults = UserDefaults.standard
            if distanceValue! > 805 && userDefaults.bool(forKey: "isCab")
            {
                let valueResult = dataPrevRespone.result.value
                let routesPlan = valueResult?.routes![indexValue].legs
                let stepValue = routesPlan![0].steps![indexPath.row]
                
                if userDefaults.bool(forKey: "isUber"){
                    let pickupLocation = CLLocation(latitude: Double((stepValue.startLocation?.lat)!), longitude:Double((stepValue.startLocation?.lng)!))
                    let dropoffLocation = CLLocation(latitude: Double((stepValue.endLocation?.lat)!), longitude:Double((stepValue.endLocation?.lng)!))
                    print("\(pickupLocation) and \(dropoffLocation)")
                    
                    
                    let builder = RideParametersBuilder()
                    builder.pickupLocation = pickupLocation
                    builder.dropoffLocation = dropoffLocation
                  //  builder.dropoffNickname = "UberHQ"
//                    builder.dropoffAddress = "1455 Market Street, San Francisco, California"
                    let rideParameters = builder.build()
                    
                    let deeplink = RequestDeeplink(rideParameters: rideParameters, fallbackType: .mobileWeb)
                    deeplink.execute()
                    
                }
                else
                {
                    
                    let app2Url: URL = URL(string: "lyft://")!
                    
                    if UIApplication.shared.canOpenURL(app2Url) {
                        open(scheme: "lyft://ridetype?id=lyft&pickup[latitude]=\(Double((stepValue.startLocation?.lat)!))&pickup[longitude]=\(Double((stepValue.startLocation?.lng)!))&destination[latitude]=\(Double((stepValue.endLocation?.lat)!))&destination[longitude]=\(Double((stepValue.endLocation?.lng)!))")
                    }
                    else{
                        
                        UIApplication.shared.openURL(NSURL(string: "https://www.lyft.com/signup/SDKSIGNUP")! as URL)
                        
                    }
                    
                  
                }
                
            }
            
        }else{
            if indexPath.row == 0
            {
                //
                
                
            }
            else if indexPath.row <= stepArray.count
            {
                self.openPassportAppLink()
                isBuyAll = false
                //
                
            }
            else
            {
                var countFare = 0
                for stepValue in stepArray
                {
                    if stepValue as! String == "Transit Uber"
                    {
                        print("Uber")
                    }
                    else
                    {
                        countFare = countFare+1
                    }
                }
                if countFare > 0
                {
                    self.openPassportAppLink()
                    isBuyAll = true
                    
                    
                }
                else
                {
                    let alertViewController = NYAlertViewController()
                    alertViewController.title = "Ticket"
                    alertViewController.message = "There is no ticket for this trip"
                    
                    // Customize appearance as desired
                    alertViewController.buttonCornerRadius = 20.0
                    alertViewController.view.tintColor = self.view.tintColor
                    alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                    alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.swipeDismissalGestureEnabled = true
                    alertViewController.backgroundTapDismissalGestureEnabled = true
                    alertViewController.buttonColor = UIColor.red
                    // Add alert actions
                    
                    
                    let cancelAction = NYAlertAction(
                        title: "OK",
                        style: .cancel,
                        handler: { (action: NYAlertAction!) -> Void in
                            
                            self.dismiss(animated: true, completion: nil)
                    }
                    )
                    
                    alertViewController.addAction(cancelAction)
                    
                    // Present the alert view controller
                    self.present(alertViewController, animated: true, completion: nil)
                }
                
            }
        }
    }
    func lyftInstalled() -> Bool {
    return UIApplication.shared.canOpenURL(URL(string: "lyft://")!)
    }
    
    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == menuTabelView {
            return 60;
     }
        else{
            let valueResult = dataPrevRespone.result.value
            let routesPlan = valueResult?.routes![indexValue].legs
            let stepValueArray = routesPlan![0].steps!
            if stepValueArray[indexPath.row].travelMode == "TRANSIT" {
       // if self.
                if UIDevice.current.userInterfaceIdiom == .pad
                {
            return 130
                }
                else{
                    return 190
                }
            }
            else if stepValueArray[indexPath.row].travelMode == "WALKING"  {
                return 115
            }
            else{
                return 140
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @objc func openPassportAppLink(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

    
    func dateconvert(Datevalue:Double,WalkingTimeinterval:Int,addsub:String) -> String {
        var date = Date(timeIntervalSince1970: Datevalue)
        if addsub == "add"
        {
            date = date.addingTimeInterval(TimeInterval(WalkingTimeinterval))
        }
        else if(addsub == "sub"){
            date = date.addingTimeInterval(-TimeInterval(WalkingTimeinterval))
        }
        else {
            date = date.addingTimeInterval(-TimeInterval(WalkingTimeinterval))
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "h:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date as Date)
        return strDate
    }
    func DateFormatcheck(Data24Hr:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        
        dateFormatter.dateFormat = "H:mm:ss"
            if let inDate = dateFormatter.date(from: Data24Hr) {
                dateFormatter.dateFormat = "h:mm a"
                let outTime = dateFormatter.string(from:inDate)
                print("in \(time)")
                print("out \(outTime)")
              //  Data24Hr = outTime
            
            return outTime
        }
            else{
                
                return Data24Hr
                
        }
        
       // return Date12Hr
    }
    @objc func TankRunTimedCode()  {
        
        self.tableView.reloadData()
        
    }
   
    func datasubraction(arrivalDate:Double,DepartDate:Double,Walkingmin:Int) -> String{
        let datetime1 = Date(timeIntervalSince1970: arrivalDate)
        let datetime2 = Date(timeIntervalSince1970: DepartDate)
        
        
        var elapsedTime = datetime2.timeIntervalSince(datetime1)
        print(elapsedTime)
        elapsedTime = elapsedTime - Double(Walkingmin)
        print(elapsedTime)
        elapsedTime = elapsedTime * 1000
        print(elapsedTime)
        let result = convertTime(miliseconds:Int(elapsedTime))
        print(result)
        return result
    }
    func convertTime(miliseconds: Int) -> String {
        
        var seconds: Int = 0
        var minutes: Int = 0
        var hours: Int = 0
        var days: Int = 0
        var secondsTemp: Int = 0
        var minutesTemp: Int = 0
        var hoursTemp: Int = 0
        
        if miliseconds < 1000 {
            return ""
        } else if miliseconds < 1000 * 60 {
            seconds = miliseconds / 1000
            return "\(seconds) sec"
        } else if miliseconds < 1000 * 60 * 60 {
            secondsTemp = miliseconds / 1000
            minutes = secondsTemp / 60
            seconds = (miliseconds - minutes * 60 * 1000) / 1000
            return "\(minutes) min"
        } else if miliseconds < 1000 * 60 * 60 * 24 {
            minutesTemp = miliseconds / 1000 / 60
            hours = minutesTemp / 60
            minutes = (miliseconds - hours * 60 * 60 * 1000) / 1000 / 60
            seconds = (miliseconds - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
            return "\(hours) hr, \(minutes) min"
        } else {
            hoursTemp = miliseconds / 1000 / 60 / 60
            days = hoursTemp / 24
            hours = (miliseconds - days * 24 * 60 * 60 * 1000) / 1000 / 60 / 60
            minutes = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000) / 1000 / 60
            seconds = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
            return "\(days) days, \(hours) hr, \(minutes) min"
        }
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    func hourminConverter(distValue:Int) -> String{
        let hourString:Int = distValue / 3600
        let minString:Int = (distValue % 3600)/60
        if hourString>0
        {
            return "\(hourString) hr \(minString) min"
            
        }
        else
        {
            return "\(minString) min"
        }
    }
    func dateconvertForOneday(Datevalue:Double) -> String {
        let date = Date(timeIntervalSince1970: Datevalue)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd/MM/yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
//    @objc func tapUberButton(_ sender: UITapGestureRecognizer){
//
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "UberDetailsViewController") as! UberDetailsViewController
//        //nextViewController.sourceaddress = self.sou
//
//
//
//
//        nextViewController.SourceAddressString = self.SourceAddressString
//        nextViewController.DestinationAddressString = self.DestinationAddressString
//        nextViewController.sourceAddressLat = self.sourceAddressLat
//        nextViewController.DestinationLat = self.DestinationLat
//
//        nextViewController.sourceAddressLong = self.sourceAddressLong
//        nextViewController.DestinationLong = self.DestinationLong
//
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//
//    }
    @objc func tapUberButtonCon(_ sender: UITapGestureRecognizer){
        if profuctid != "" {
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "UberViewController") as! UberViewController
//            //nextViewController.sourceaddress = self.sou
//
//
//            print(self.profuctid)
//
//            nextViewController.SourceAddressString = self.SourceAddressString
//            nextViewController.DestinationAddressString = self.DestinationAddressString
//            nextViewController.sourceAddressLat = self.sourceAddressLat
//            nextViewController.DestinationLat = self.DestinationLat
//            nextViewController.ProductID = self.profuctid
//            nextViewController.sourceAddressLong = self.sourceAddressLong
//            nextViewController.DestinationLong = self.DestinationLong
//
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
        else {

            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    func unixtodate(unixtimeInterval:Double) -> String{
        let date = Date(timeIntervalSince1970: unixtimeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "MM-dd-yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    func convertDateFormatter(date1: String,Date2:String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mma"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "EST") as TimeZone!
        dateFormatter.locale = NSLocale.current
        let start = dateFormatter.date(from: date1)!
       
        let enddt = dateFormatter.date(from: Date2)!
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([ .second])
        let datecomponenets = calendar.dateComponents(unitFlags, from: start, to: enddt)
        let seconds = datecomponenets.second
        print("Seconds: \((seconds)!)")
       
        
        return seconds!
    }
    func getalertTitle(RouteNo: String, completion: @escaping (String,String,Bool) -> ()) {
        
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Alerts/GetRealtimeAlerts?routeID=\(RouteNo)", method: .get, encoding: JSONEncoding.default) .responseJSON { response in
            switch response.result{
            //switch result {
               
                case .success:
               // print(response)
                let responseVar = response.result.value
                let json = JSON(responseVar!)
               
                let successvalu:Bool?
                if json.count == 0 {
                    successvalu = false
                }
                else {
                     successvalu = true
                }
               
                completion("\(json[0])","\(json["LineAlert"])", successvalu!)
            case .failure(let error):
                print("Request failed with error: \(error)")
                }
        }
        
        
        
        
        
        
        
    }
    
}
class SuggrouteCell: UITableViewCell {
     @IBOutlet weak var NextStopRealtime: UILabel!
    @IBOutlet weak var arrivalLabel: UILabel!
    //let arrivalLabel = UILabel()
    //let NextStopRealtime = UILabel()
    // let tripstatus = UILabel()
   // let reailtimeicon = UIImageView()
    @IBOutlet weak var TransitTime: UILabel!
    @IBOutlet weak var SecondDescription: UILabel!
    @IBOutlet weak var reailtimeicon: UIImageView!
     @IBOutlet weak var traveltimeWaitTime: UILabel!
    @IBOutlet weak var TransitRouteNo: UILabel!
    @IBOutlet weak var TransitWalkingImg: UIImageView!
    @IBOutlet weak var FullDescription: UILabel!
   // let traveltimeWaitTime = UILabel()
   // let AlertImage = UIButton()
    //let AlertText = UILabel()
    
    @IBOutlet weak var AlertText: UILabel!
    @IBOutlet weak var AlertImage: UIButton!
    @IBOutlet weak var tripstatus: UILabel!
    
    // @IBOutlet weak var FullDescription: UITextView!
    // @IBOutlet weak var TransitOrWalking: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        //Do reset here
        TransitTime?.text = ""
        SecondDescription?.text = ""
        TransitWalkingImg?.image = nil
        FullDescription.text = ""
        arrivalLabel.text  = ""
        NextStopRealtime.text = ""
        tripstatus.text = ""
        TransitRouteNo.text = ""
        // reailtimeicon.image = nil
        traveltimeWaitTime.text = ""
        traveltimeWaitTime.font = UIFont.setTarcLight(size: 12)
        SecondDescription.font = UIFont.setTarcLight(size: 12)

        AlertText.text = ""
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
}
extension UITextView {
    
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(1, topOffset)
        contentOffset.y = -positiveTopOffset
    }
    
}




class MyTapGesture: UITapGestureRecognizer {
    var title = String()
}
