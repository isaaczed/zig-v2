//
//	MyLocationAddress.swift
//
//	Create by Isaac on 21/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class MyLocationAddress : NSObject, NSCoding, Mappable{

	var results : [MyResult]?
	var status : String?


	class func newInstance(map: Map) -> Mappable?{
		return MyLocationAddress()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		results <- map["results"]
		status <- map["status"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         results = aDecoder.decodeObject(forKey: "results") as? [MyResult]
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if results != nil{
			aCoder.encode(results, forKey: "results")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
