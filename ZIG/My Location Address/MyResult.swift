//
//	MyResult.swift
//
//	Create by Isaac on 21/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class MyResult : NSObject, NSCoding, Mappable{

	var addressComponents : [MyAddressComponent]?
	var formattedAddress : String?
	var geometry : MyGeometry?
	var placeId : String?
	var types : [String]?


	class func newInstance(map: Map) -> Mappable?{
		return MyResult()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		addressComponents <- map["address_components"]
		formattedAddress <- map["formatted_address"]
		geometry <- map["geometry"]
		placeId <- map["place_id"]
		types <- map["types"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addressComponents = aDecoder.decodeObject(forKey: "address_components") as? [MyAddressComponent]
         formattedAddress = aDecoder.decodeObject(forKey: "formatted_address") as? String
         geometry = aDecoder.decodeObject(forKey: "geometry") as? MyGeometry
         placeId = aDecoder.decodeObject(forKey: "place_id") as? String
         types = aDecoder.decodeObject(forKey: "types") as? [String]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if addressComponents != nil{
			aCoder.encode(addressComponents, forKey: "address_components")
		}
		if formattedAddress != nil{
			aCoder.encode(formattedAddress, forKey: "formatted_address")
		}
		if geometry != nil{
			aCoder.encode(geometry, forKey: "geometry")
		}
		if placeId != nil{
			aCoder.encode(placeId, forKey: "place_id")
		}
		if types != nil{
			aCoder.encode(types, forKey: "types")
		}

	}

}
