//
//	MyGeometry.swift
//
//	Create by Isaac on 21/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper

  
class MyGeometry : NSObject, NSCoding, Mappable{

	var bounds : MyBound?
	var location : MyNortheast?
	var locationType : String?
	var viewport : MyBound?


	class func newInstance(map: Map) -> Mappable?{
		return MyGeometry()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		bounds <- map["bounds"]
		location <- map["location"]
		locationType <- map["location_type"]
		viewport <- map["viewport"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bounds = aDecoder.decodeObject(forKey: "bounds") as? MyBound
         location = aDecoder.decodeObject(forKey: "location") as? MyNortheast
         locationType = aDecoder.decodeObject(forKey: "location_type") as? String
         viewport = aDecoder.decodeObject(forKey: "viewport") as? MyBound

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if bounds != nil{
			aCoder.encode(bounds, forKey: "bounds")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if locationType != nil{
			aCoder.encode(locationType, forKey: "location_type")
		}
		if viewport != nil{
			aCoder.encode(viewport, forKey: "viewport")
		}

	}

}
