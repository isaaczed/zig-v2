//
//	MyAddressComponent.swift
//
//	Create by Isaac on 21/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class MyAddressComponent : NSObject, NSCoding, Mappable{

	var longName : String?
	var shortName : String?
	var types : [String]?


	class func newInstance(map: Map) -> Mappable?{
		return MyAddressComponent()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		longName <- map["long_name"]
		shortName <- map["short_name"]
		types <- map["types"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         longName = aDecoder.decodeObject(forKey: "long_name") as? String
         shortName = aDecoder.decodeObject(forKey: "short_name") as? String
         types = aDecoder.decodeObject(forKey: "types") as? [String]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if longName != nil{
			aCoder.encode(longName, forKey: "long_name")
		}
		if shortName != nil{
			aCoder.encode(shortName, forKey: "short_name")
		}
		if types != nil{
			aCoder.encode(types, forKey: "types")
		}

	}

}
