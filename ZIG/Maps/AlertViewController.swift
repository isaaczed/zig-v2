//
//  AlertViewController.swift
//  ZIG
//
//  Created by Isaac on 17/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import UIKit
import Alamofire
import XLActionController
import NVActivityIndicatorView
import Toast_Swift
import SafariServices
import SCLAlertView

class AlertViewController: UIViewController {
    var AlertData = [[String:Any]]()
    var eventNews = [[String:Any]]()
   var loaderView = UIView()
let assistiveTouchSOS = AssistiveTouch()
    let headerTitles : NSMutableArray  = ["News and Events","Alerts"]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        analytics.GetPageHitCount(PageName: "Alerts/Events")

//        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
//        offline.updateUserInterface(withoutLogin: false)
        tableView.estimatedRowHeight = 103
        tableView.rowHeight = UITableViewAutomaticDimension
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        
        //self.navigationController?.title = "TRIP PLANNER"
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
       
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
               let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
       backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
         self.navigationItem.title = "ALERTS / EVENTS"
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        getallalert()
        // Do any additional setup after loading the view.
   
        let assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

        assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
        assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
        view.addSubview(assistiveTouch)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
               tapGesture.numberOfTapsRequired = 1
               assistiveTouchSOS.addGestureRecognizer(tapGesture)
               let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
               longPress.minimumPressDuration = 1.0
               self.assistiveTouchSOS.addGestureRecognizer(longPress)
               assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
               assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
               //view.addSubview(assistiveTouchSOS)
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
           if gesture.state == UIGestureRecognizerState.began {
               UIDevice.vibrate()
               print("Long Press")
               currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                          if Success{
                         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                         let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                           print("\(Address),\(LatsosLat),\(Longsoslong)")
                           MapsScheduleVC.CurrentlocationAddressString = Address
                           MapsScheduleVC.latSOS = LatsosLat
                           MapsScheduleVC.longSOS = Longsoslong
                         self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                  //self.CurrentlocationAddress.text = Address
                              }
                          }
               
           }
       }
     
       @objc func tappedSOS(sender: UIButton) {
        
           let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                   // action here
           }
                   
           SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
          
       }
//    @objc func statusManager(_ notification: NSNotification) {
//        offline.updateUserInterface(withoutLogin: false)
//    }
    @objc func tapped(sender: UIButton) {
        print("\(sender) has been touched")
        
        
        let actionController = TwitterActionController()
        
        actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        if comVar.DateImplementation {
            
            actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
                
                
                
            }))
            
        }
        
        
        actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
            
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
//            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        actionController.headerData = "Quick View"
        present(actionController, animated: true, completion: nil)
        
        
        
        
    }
    
    @objc func OnBackClicked() {
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is LoginNewViewController {
                // ... and so on
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else{
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        // self.navigationController?.popViewController(animated: true)
        
        
    }
    override func viewDidDisappear(_ animated: Bool) {
         NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }
    func getallalert(){
        self.showLoader()
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Alerts/GetAllLineAlerts", method: .get, encoding: URLEncoding.default)
            .responseArray{ (response: DataResponse<[AlertDetails]>) in
                switch response.result {
                case .success:
                    print(response.result.value!)
                    if response.result.value != nil {
                    if response.result.value!.count > 0
                    {
                        let AlertDetailsList = response.result.value
                        let alertVal : NSMutableDictionary = [:]
                        for alertarray in AlertDetailsList! {
                            
                            if alertarray.Lineno != nil && alertarray.Lineno != "" {
                                
                                
                                
                                
                                alertVal.setValue(alertarray.Lineno ?? " ", forKey: "LineNo")
                                alertVal.setValue(alertarray.LineAlert ?? " ", forKey: "LineAlert")
                                alertVal.setValue(alertarray.LineHeader ?? " ", forKey: "LineHeader")
                                alertVal.setValue(alertarray.RouteLong ?? " ", forKey: "RouteLong")
                                alertVal.setValue(alertarray.RouteShort ?? " ", forKey: "RouteShort")
                                self.AlertData.append(alertVal as! [String : Any])
                                
                                
                                
                            }
                            else {
                                
                                alertVal.setValue(alertarray.Lineno ?? " ", forKey: "LineNo")
                                alertVal.setValue(alertarray.LineAlert ?? " ", forKey: "LineAlert")
                                alertVal.setValue(alertarray.LineHeader ?? " ", forKey: "LineHeader")
                               self.eventNews.append(alertVal as! [String : Any])
                            }
                        }
                        self.hideLoader()
                        self.tableView.reloadData()
                  //  self.DataArray =  self.eventNews + self.AlertData
                  dump(self.AlertData)
                        

                    }
                    else{
                         self.hideLoader()
                        self.view.makeToast("No Alert Today!")
                        }
                    }
                    else{
                         self.hideLoader()
                         self.view.makeToast("Server Error please try again")
                    }
                case .failure(let error):
                    print(error)
                     self.hideLoader()
                     self.view.makeToast("Server Error please try again")
                  //  self.getallalert()
                    
                    
                }
        }
    
        
    }
   // func getallAler
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AlertViewController:UITableViewDelegate,UITableViewDataSource {
     func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
         
            if self.eventNews.count == 0 {
                return 1
            }
            else {
        return self.eventNews.count
            
            }
            
        }
        else{
            if self.AlertData.count == 0 {
                return 1
            }
            else {
            return self.AlertData.count
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlertCell",
                                                     for: indexPath) as? AlertCell
            else { return UITableViewCell() }
        cell.prepareForReuse()
       cell.selectionStyle = .none;
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowRadius = 5
        //cell.layer.cornerRadius = 20

        cell.layer.shadowOpacity = 0.40
        cell.layer.masksToBounds = false;
        cell.clipsToBounds = false;

        
       
       // cell.contentView.setCardView(view: cell.contentView)
         if indexPath.section == 0 {
            if self.eventNews.count == 0 {
                cell.AlertTitle.text = ""
                cell.ImageViewAlert.image = nil
                cell.AlertDescription.text = ""
                
                cell.nodatalabel.frame = CGRect.init(x: 0, y: 0, width: cell.bounds.size.width, height: cell.bounds.size.height + 40)
                cell.nodatalabel.text = "No Events Today"
                cell.nodatalabel.textColor = UIColor.black
                cell.nodatalabel.textAlignment = .center
                cell.nodatalabel.sizeToFit()
                cell.backgroundView = cell.nodatalabel;
               
                
                
            }
            else{
            cell.ImageViewAlert.image = UIImage(named: "NEWS")
            cell.AlertTitle.textColor = color.hexStringToUIColor(hex: "#297EC3")
                
                cell.AlertTitle.text = self.eventNews[indexPath.row]["LineHeader"] as? String
                
            cell.AlertDescription.text = self.eventNews[indexPath.row]["LineAlert"] as? String
            }
        }
         else{
            if self.AlertData.count == 0 {
                 cell.AlertTitle.text = ""
                cell.ImageViewAlert.image = nil
                    cell.AlertDescription.text = ""
                     cell.nodatalabel.frame = CGRect.init(x: 0, y: 0, width: cell.bounds.size.width, height: cell.bounds.size.height + 40)
                    cell.nodatalabel.text = "No Alert Today"
                    cell.nodatalabel.textColor = UIColor.black
                    cell.nodatalabel.textAlignment = .center
                    cell.nodatalabel.sizeToFit()
                    cell.backgroundView = cell.nodatalabel
                    
                    
                    
                }
            else {
                cell.nodatalabel.text = ""
            cell.AlertTitle.textColor = color.hexStringToUIColor(hex: "#cf152d")
            cell.ImageViewAlert.image = UIImage(named: "AlertDash")
            
            // cell.AlertTitle.text = self.AlertData[indexPath.row]["LineHeader"] as? String
                let longName = self.AlertData[indexPath.row]["RouteLong"] as? String
                let ShortName = self.AlertData[indexPath.row]["RouteShort"] as? String
                        
                cell.AlertTitle.text = "#RiderAlert \nRoute #\((ShortName)!) - \((longName)!)"
              cell.AlertDescription.text = self.AlertData[indexPath.row]["LineAlert"] as? String
            }
        }
        
        
        
        return cell
    }
  
  
  
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        let headerView  = UIView()
        headerView.backgroundColor = color.hexStringToUIColor(hex: "#DBDBDB")
        
        let headerLabel = UILabel()
        headerLabel.frame = CGRect.init(x: 20, y: 10, width: 250, height: 40)
        headerLabel.font = UIFont.setTarcBold(size: 18.0)
        headerLabel.textColor = UIColor.black
        headerLabel.textAlignment = NSTextAlignment.left
        headerView.addSubview(headerLabel)
        
        if headerTitles[section] as? String == "Alerts" {
            
            headerLabel.text = "Alerts"
            
        }
        else{
            
            headerLabel.text = "News and Events"
        }
        
        
        return headerView
    }
    
    
    func showLoader()
    {
        
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(1.0)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }

}


class AlertCell:UITableViewCell {
    @IBOutlet weak var AlertTitle: UILabel!
    @IBOutlet weak var AlertDescription: UILabel!
    @IBOutlet weak var ImageViewAlert: UIImageView!
    var nodatalabel = UILabel()
    override func prepareForReuse() {
        super.prepareForReuse()
        AlertTitle.text = ""
        AlertDescription.text = ""
        ImageViewAlert.image = nil
        nodatalabel.text = ""
        
        
    }
    
}



extension UIScrollView {
    func showEmptyListMessage(_ message:String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.bounds.size.width, height: self.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.setTarcRegular(size: 15)
        messageLabel.sizeToFit()
        
        if let `self` = self as? UITableView {
            self.backgroundView = messageLabel
            self.separatorStyle = .none
        } else if let `self` = self as? UICollectionView {
            self.backgroundView = messageLabel
        }
    }
}
