//
//  MapViewController.swift
//  PullUpControllerDemo
//
//  Created by Mario on 03/11/2017.
//  Copyright © 2017 Mario. All rights reserved.
//
//
//  MapViewController.swift
//  PullUpControllerDemo
//
//  Created by Mario on 03/11/2017.
//  Copyright © 2017 Mario. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import Toast_Swift


class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate{
    var instanceOfVCA:MapsScheduleViewController!
    @IBOutlet private weak var mapView: MKMapView!
    var mylocation = CLLocationCoordinate2D(latitude: 38.2526647, longitude: -85.75845570000001)
    var locationManager = CLLocationManager()
    var lat = 0.0
    var lng = 0.0
    var timer:Timer!
    //@IBOutlet private weak var widthSlider: UISlider!
    //  @IBOutlet private weak var initialStateSegmentedControl: UISegmentedControl!
    var initialStateSegmentedControl:UISegmentedControl!
    private var originalPullUpControllerViewSize: CGSize = .zero
    var scheduleParts = [ShapesSchedule]()
     var coordinateArray: [CLLocationCoordinate2D] = []
    private func makeSearchViewControllerIfNeeded() -> SearchViewController {
        let currentPullUpController = childViewControllers
            .filter({ $0 is SearchViewController })
            .first as? SearchViewController
        let pullUpController: SearchViewController = currentPullUpController ?? UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        // if initialStateSegmentedControl.selectedSegmentIndex == 0 {
      //  pullUpController.initialState = .expanded
        //  } else {
        // pullUpController.initialState = .expanded
        // }
        
        if originalPullUpControllerViewSize == .zero {
            originalPullUpControllerViewSize = pullUpController.view.bounds.size
        }
        return pullUpController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: mylocation, span: span)
        mapView.setRegion(region, animated: true)
        addPullUpController()
        self.realtime()
        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.realtime), userInfo: nil, repeats: true)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        
        //self.navigationController?.title = "TRIP PLANNER"
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
//        menuButton = UIBarButtonItem(image: UIImage(named: "Menu_Icon"),
//                                     style: UIBarButtonItemStyle.done ,
//                                     target: self, action: #selector(OnMenuClicked))
//        self.navigationItem.rightBarButtonItem = menuButton
//        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.view.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
       backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    override func viewDidDisappear(_ animated: Bool) {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        
    }
    private func addPullUpController() {
        let pullUpController = makeSearchViewControllerIfNeeded()
        _ = pullUpController.view // call pullUpController.viewDidLoad()
        addPullUpController(pullUpController,
                            initialStickyPointOffset: pullUpController.initialPointOffset,
                            animated: true)
    }
    
    func Shape(TripID:Int){
         coordinateArray = []
        let RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetRealtimetripDetails?tripID=\(TripID)"
        Alamofire.request(RouteUrl, method: .get, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<RealTimeTripDetailsTripID>) in
                switch response.result {
                case .success:
                    if  response.result.value != nil {
                        
                        RealTimeSchedule.SelectedScheduleRouteColor = response.result.value?.RouteColor ?? "000000"
                        // print(response.result.value?.RouteColor!)
                        
                        if response.result.value!.Shapes != nil {
                            self.scheduleParts = response.result.value!.Shapes!
                            dump(response.result.value)
                            for shapelistlatlng in self.scheduleParts{
                                
                                self.lat = Double(shapelistlatlng.shapeLat!) ?? 0
                                self.lng = Double(shapelistlatlng.ShapeLong!) ?? 0
                                // print("\(self.latit) \(self.longti)")
                                //let destination =
                                
                                self.coordinateArray.append(CLLocationCoordinate2D(latitude: self.lat,longitude: self.lng))
                                
                                
                            }
                            
                            let apolyline = MKPolyline(coordinates: self.coordinateArray, count: self.coordinateArray.count)
                            self.mapView.add(apolyline)
                            
                            if let first = self.mapView.overlays.first {
                                let rect = self.mapView.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
                                self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                            }
                            
                        }
                        else{
                            self.view.makeToast("Server Error! Please try again later!")
                            
                        }
                    }
                case .failure(let error):
                    print(error)
                }
        }
        
        
    }
    
    
    func zoom(to:Int) {
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
       // let region = MKCoordinateRegion(center: location, span: span)
        print(to)
      //  realtime()
        
       
        
        //GetRealtimeLineDetailsfromTripID
        //RealTimeRootClass
        coordinateArray = []
        
        mapView.removeOverlays(mapView.overlays)
        let RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetRealtimetripDetails?tripID=\(to)"
        Alamofire.request(RouteUrl, method: .get, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<RealTimeTripDetailsTripID>) in
                switch response.result {
                case .success:
                    if  response.result.value != nil {
                    
                        RealTimeSchedule.SelectedScheduleRouteColor = response.result.value?.RouteColor ?? "000000"
                       // print(response.result.value?.RouteColor!)
                        
                        if response.result.value!.Shapes != nil {
                    self.scheduleParts = response.result.value!.Shapes!
                    dump(response.result.value)
                        for shapelistlatlng in self.scheduleParts{
                            
                            self.lat = Double(shapelistlatlng.shapeLat!) ?? 0
                            self.lng = Double(shapelistlatlng.ShapeLong!) ?? 0
                            // print("\(self.latit) \(self.longti)")
                            //let destination =
                            
                            self.coordinateArray.append(CLLocationCoordinate2D(latitude: self.lat,longitude: self.lng))
                            
                            
                        }
                        
                        let apolyline = MKPolyline(coordinates: self.coordinateArray, count: self.coordinateArray.count)
                        self.mapView.add(apolyline)
                            
                            if let first = self.mapView.overlays.first {
                                let rect = self.mapView.overlays.reduce(first.boundingMapRect, {MKMapRectUnion($0, $1.boundingMapRect)})
                                self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                            }
                    
                        }
                        else{
                            self.view.makeToast("Server Error! Please try again later!")
                            
                        }
                    }
                case .failure(let error):
                    print(error)
                }
        }
        
        
        
    }
    
    @IBAction private func addButtonTapped() {
        guard
            childViewControllers.filter({ $0 is SearchViewController }).count == 0
            else { return }
        addPullUpController()
    }
    @objc func OnBackClicked() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction private func removeButtonTapped() {
        let pullUpController = makeSearchViewControllerIfNeeded()
        removePullUpController(pullUpController, animated: true)
    }
    
    
    @IBAction func CloseButton(_ sender: Any) {
        //(parent as? realtimeSchduleViewController)?.OnMenuClicked()
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    @objc func realtime(){
        
       //
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        let RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeBusesforRouteID?RouteID=\(RealTimeSchedule.SelectedSchedule)"
        print(RouteUrl)
        Alamofire.request(RouteUrl, method: .get, encoding: URLEncoding.default)
            .responseArray{ (response: DataResponse<[RealTimeScheduleBus]>) in
                switch response.result {
                case .success:
                    if response.result.value != nil {
                        let Realtimedata = response.result.value
                        if response.result.value!.count > 0 {
                
//                            let filterData = Realtimedata!.filter { $0.tripID == TripID }
//                            print(TripID)
//                        dump(filterData)
                            
                            
                            
                            for RealtimeDataList in Realtimedata! {
                                if RealtimeDataList.vehicle! {
                                let nextBustop = RealtimeDataList.nextstops![0].stopName ?? " "
                                
                                let RealTimeBus = MKPointAnnotation()
                                RealTimeBus.title = "Trip ID: \((RealtimeDataList.tripID)!)"
                                RealTimeBus.subtitle = "Next Stop: \(nextBustop)"
                                RealTimeBus.coordinate = CLLocationCoordinate2D(latitude: RealtimeDataList.lat!, longitude: RealtimeDataList.lng!)
                                self.mapView.addAnnotation(RealTimeBus)
                                }
                                
                            }
                            
                        
                        
                    
                    }
                        
                    }
                    
                case .failure(let error):
                    print(error)
                }
        }
        
        
        
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        
        let polylineRender = MKPolylineRenderer(overlay: overlay)
        polylineRender.strokeColor = color.hexStringToUIColor(hex: "#\(RealTimeSchedule.SelectedScheduleRouteColor)")
        polylineRender.lineWidth = 5
        
        return polylineRender
        
        
        
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        
        let reuseId = "Tarc"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.image = UIImage(named:"bus_Transit")
            // mapView.showAnnotations(mapView.annotations, animated: true)
            anView!.canShowCallout = true
            // showAnnotations(mapView.annotations, animated: true)
            
        }
        else {
            anView!.annotation = annotation
        }
        //mapView.showAnnotations(mapView.annotations, animated: true)
        return anView
    }
    
}

