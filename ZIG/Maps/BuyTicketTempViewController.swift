//
//  BuyTicketTempViewController.swift
//  ZIG
//
//  Created by Isaac on 28/06/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import SwiftyJSON
import XLActionController
import SCLAlertView
import CoreBluetooth
class BuyTicketTempViewController: UIViewController,CBCentralManagerDelegate {
   let assistiveTouchSOS = AssistiveTouch()
    var cellId = "TicketdashboardCell"
    var loaderView = UIView()
    var alertView = SCLAlertView()
    var dashBoardArray = NSArray()
    let addTickeValue : NSMutableArray = []
    var centralManager: CBCentralManager!
var isClickFromViewDid = Bool()
    @IBOutlet weak var GetTicket: UIButton!
      var menuButton = UIBarButtonItem()
    @IBOutlet weak var CollectionviewTicket: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        isClickFromViewDid = true
        makeArray()
        centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: false])
        centralManager.delegate = self
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        
        //self.navigationController?.title = "TRIP PLANNER"
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        //self.view.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
       backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.title = " Buy tickets"
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        menuButton = UIBarButtonItem(title: "Dashboard",
                                     style: UIBarButtonItemStyle.plain,
                                     target: self,
                                     action: #selector(OpenAction))
        self.navigationItem.rightBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem?.tintColor = color.hexStringToUIColor(hex: "#EFAC40")
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        secondViewDesign()
        let assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2 - 50, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

        assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
        assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
        view.addSubview(assistiveTouch)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
        tapGesture.numberOfTapsRequired = 1
        assistiveTouchSOS.addGestureRecognizer(tapGesture)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
        longPress.minimumPressDuration = 1.0
        self.assistiveTouchSOS.addGestureRecognizer(longPress)
        assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
        assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
       // view.addSubview(assistiveTouchSOS)


        
        // Do any additional setup after loading the view.
//        CollectionviewTicket.backgroundColor = UIColor.clear
//        CollectionviewTicket.delegate = self
//        CollectionviewTicket.dataSource = self
//        CollectionviewTicket.collectionViewLayout = layout
//        CollectionviewTicket.bounces = false
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
           if gesture.state == UIGestureRecognizerState.began {
               UIDevice.vibrate()
               print("Long Press")
               currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                          if Success{
                         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                         let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                           print("\(Address),\(LatsosLat),\(Longsoslong)")
                           MapsScheduleVC.CurrentlocationAddressString = Address
                           MapsScheduleVC.latSOS = LatsosLat
                           MapsScheduleVC.longSOS = Longsoslong
                         self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                  //self.CurrentlocationAddress.text = Address
                              }
                          }
               
           }
       }
     
       @objc func tappedSOS(sender: UIButton) {
        
           let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                   // action here
           }
                   
           SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
          
       }
    override func viewDidDisappear(_ animated: Bool) {
        centralManager.delegate = nil
    }
    @objc func OpenAction(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
        nextViewController.isRedirectFromLogin = false
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func tapped(sender: UIButton) {
        print("\(sender) has been touched")
        
        
        let actionController = TwitterActionController()
        
        actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        if comVar.DateImplementation {
            
            actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                
               
                
            }))
            
        }
        
        actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
            
            //            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
            //            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }))
        
        
        
        actionController.headerData = "Quick View"
        present(actionController, animated: true, completion: nil)
        
        
        
        
    }
    

    @IBAction func Buyticket(_ sender: Any) {
     
        buyTicketAction()

    }
    func offlineDesign()
    {
        let appearance = SCLAlertView.SCLAppearance(
            
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Internet Setting"){
            // ComValue.internetalertstatus = true
          
//                let window = UIApplication.shared.keyWindow!
//
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
//
//
//                var rootViewController = window.rootViewController as! UINavigationController
//                rootViewController.pushViewController(tripPlannerViewController, animated: true)
           
        }
        alertView.addButton("No"){
            //ComValue.internetalertstatus = true
            guard let settingsUrl = URL(string:UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    // Checking for setting is opened or not
                    print("Setting is opened: \(success)")
                })
            }
        }
        
        alertView.showInfo("No Internet Connection", subTitle: "Please enable Internet access for TARC in your iOS settings")
        //        }
        //            else
        //            {
        //                print("*******OFfline ticket purchase*******")
        //            }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     */    func makeArray() -> Void {
        
        
        dashBoardArray = [
            [
                "title" : "My Tickets",
                "image" : "icons8-two-tickets-64"
            ]
//            [
//                "title" : "Trip Plan",
//                "image" : "icons8-map-pinpoint-64"
//            ]
//            ],
//            [
//                "title" : "Alerts",
//                "image" : "icons8-error-64"
//            ]
            
        ]
        
    }
    @objc func OnBackClicked()
    {
        
                self.navigationController?.popViewController(animated: true)
        
        // self.navigationController?.popViewController(animated: true)
        
        
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    func secondViewDesign()
    {
        
        let collectionViewSize = self.view.frame.size.width
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        // layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        layout.itemSize = CGSize(width: collectionViewSize/3.4, height: collectionViewSize/3.4)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone: break
        // It's an iPhone
        case .pad:
            // heightTopView.constant = 140
            
            // YpostionTopView.constant = 100
            layout.sectionInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
            layout.itemSize = CGSize(width: collectionViewSize/3.6, height: collectionViewSize/3.6)
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = 30
            layout.minimumInteritemSpacing = 5
            break
        // It's an iPad
        case .unspecified: break
        // Uh, oh! What could it be?
        case .tv: break
            
        case .carPlay: break
            
        }
        
        CollectionviewTicket.backgroundColor = UIColor.clear

        CollectionviewTicket.collectionViewLayout = layout
        CollectionviewTicket.bounces = false
        
        
        //        let toplayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //        toplayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //
        //        toplayout.itemSize = CGSize(width: topCollectionView.contentInset.left * 2, height: topCollectionView.contentInset.left * 2)
        //        toplayout.scrollDirection = .horizontal
        //
        //
        //        topCollectionView.collectionViewLayout = toplayout
        
        
        
    }
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("central.state is .unknown")
        case .resetting:
            print("central.state is .resetting")
        case .unsupported:
            print("central.state is .unsupported")
        case .unauthorized:
            print("central.state is .unauthorized")
        case .poweredOff:
            print("central.state is .poweredOff")
            if isClickFromViewDid == true
            {
                isClickFromViewDid = false
                checkBluetoothDevice()

            }
        case .poweredOn:
            print("central.state is .poweredOn")
            
            
            
        }
    }
    func buyTicketAction()
    {
        
        if isReachable()
        {
//            
//           // @IBAction func payUsingPayAnywhere() {
//            var url : URL = URL(string: "payanywhere://payment/?chargeAmount=1.00&externalNotification=true&customerTransactionId=232211232&returnExtras=true")!;
//                UIApplication.shared.open(url, options: [:], completionHandler: { (opened : Bool) in
//                    print(opened)
//                });
//            
//            
//           // }
//            
          

            
            
            var pref = UserDefaults.standard
            addTickeValue.removeAllObjects()
            
            let BuyTicket_Amount = "1.75"
            let BuyTicket_TripId = "4"
            let BuyTicket_RouteId = "4"
            let BuyTicket_ProfileId = (pref.object(forKey: "ProfileID")) as? Int
            let BuyTicket_FromAddress =  "S 6th @ W Liberty"
            let BuyTicket_DestinationAddress = "4th @ Ormsby"
            
            let Addticketlist : NSMutableDictionary = [:]
            print(BuyTicket_ProfileId)
            
            Addticketlist.setValue("\((BuyTicket_Amount))", forKey: "Amount")
            Addticketlist.setValue("\((BuyTicket_RouteId))", forKey: "RouteId")
            Addticketlist.setValue("\((BuyTicket_TripId))", forKey: "TripId")
            Addticketlist.setValue(BuyTicket_ProfileId, forKey: "ProfileId")
            Addticketlist.setValue("\((BuyTicket_FromAddress))", forKey: "FromAddress")
            Addticketlist.setValue("\((BuyTicket_DestinationAddress))", forKey: "DestinationAddress")
            
            
            
            
            addTickeValue.add(Addticketlist)
            
            print("\(addTickeValue)")
            self.showLoader()
            let Timestamp = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "EST")
            let utctimeStamp = dateFormatter.string(from: Timestamp)
            
            let headers = [
                "Content-Type":"application/json",
            ]
            
            
            
            var fourDigitNumber: String {
                var result = ""
                repeat {
                    // Create a string with a random number 0...9999
                    result = String(format:"%05d", arc4random_uniform(1000000) )
                } while result.count < 6
                return result
            }
            
            
            
            // JSON Body
            let body: [String : Any] = [
                "Token": pref.object(forKey: "accessToken")!,
                "FromAddress": "Louisville, KY, USA",
                "DestinationAddress": "1257 S 3rd St, Louisville, KY 40203, USA",
                "TotalAmount": "1.75",
                "PlaceId": "45",
                "TransactionDate": utctimeStamp,
                "Tickets": addTickeValue,
                "TransactionId": "ios\(fourDigitNumber)",
                "Message": "Ticket Take in IOS App"
            ]
            print(body)
            print(APIUrl.BuyTicket)
            Alamofire.request(APIUrl.BuyTicket, method: .post, parameters: body, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        
                        
                        if (response.result.value != nil) {
                            
                            let responseVar = response.result.value
                            let json = JSON(responseVar!)
                            print(json)
                            if json["Message"] == "Ok" {
                                
                                
                                TicketSave.TicketDataUpload(CountForRow: 1, Expiry: false, completion: { (SuccessMessage,Count) in
                                    
                                    if SuccessMessage {
                                        
                                        self.view.makeToast("Thank you for purchasing tickets!", position: .top)
                                        
                                        
                                    }
                                    else{
                                        
                                        self.view.makeToast("Thank you for purchasing tickets!", position: .top)
                                        
                                        
                                        // self.view.makeToast("Thank you for Buying Ticket! This is pilot Version only July 1 - July 14", duration: 5)
                                    }
                                    self.hideLoader()
                                })
                                
                                
                                
                                
                                
                            }else{
                                self.hideLoader()
                                self.view.makeToast("\(json["Message"])")
                            }
                            
                        }else{
                            self.hideLoader()
                            self.view.makeToast("Please Try Again")
                            
                        }
                    case .failure(let error):
                        print(error)
                        self.hideLoader()
                        self.view.makeToast("Please Try Again!")
                        
                    }
            }
        }
        else
        {
            offlineDesign()
        }
    }
    func checkBluetoothDevice()
    {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        alertView.hideView()
        alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("Bluetooth Settings"){
            //ComValue.internetalertstatus = true
            guard let settingsUrl = URL(string:UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    // Checking for setting is opened or not
                    print("Setting is opened: \(success)")
                    
                })
            }
        }
        alertView.addButton("Close"){
            // self.checkLocationwithoutLogin()
        }
        alertView.showWarning(" Bluetooth", subTitle: "Please enable Bluetooth access for TARC in your settings.")
        
    }
    
}
extension BuyTicketTempViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dashBoardArray.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
            self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
        }
        else if indexPath.row == 1
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
            self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
        }
        else if indexPath.row == 2
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
            self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TicketdashboardCell
        
        cell.backgroundColor = UIColor.white
        let dict = dashBoardArray.object(at: indexPath.row)as! NSDictionary
        let TitleName = dict.value(forKey: "title") as! NSString
        let imageName = dict.value(forKey: "image") as! NSString
        
        cell.dashboardTitle.text = TitleName as String
        cell.dahboardImage.image = UIImage.init(named: imageName as String)
        
        cell.layer.cornerRadius = 5
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowRadius = 2
        cell.layer.shadowOpacity = 0.25
        cell.layer.shadowOffset = CGSize(width: 1, height: 1);
        cell.layer.masksToBounds = false
        cell.clipsToBounds = false
        print(TitleName as String)
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            break
        case .pad:
            cell.dashboardTitle.font = UIFont.setTarcHeavy(size: 24.0)
            break
        case .unspecified: break
        case .tv: break
        case .carPlay: break
            
        }
        
        return cell
        
}
   
    
}
class TicketdashboardCell: UICollectionViewCell {
    
    @IBOutlet weak var dashboardTitle: UILabel!
    @IBOutlet weak var dahboardImage: UIImageView!
}
