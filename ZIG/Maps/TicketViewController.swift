//
//  TicketViewController.swift
//  ZIG
//
//  Created by Isaac on 24/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//


import UIKit
import Alamofire
import Toast_Swift
import RealmSwift
import NVActivityIndicatorView
import XLActionController
import SwiftyJSON
import CoreBluetooth
import XLActionController
import SCLAlertView
class TicketViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,CBCentralManagerDelegate {
    
   
    var expandedIndexPath:IndexPath?
   var loaderView = UIView()
    let realm = try! Realm()
    var menuButton = UIBarButtonItem()
    var dashBoardArray = NSArray()
    var centralManager: CBCentralManager!
    var isDidSelectBool = Bool()
    var isFromViewdidLoad = Bool()
    var Reloadtimer : Timer?
    var timer : Timer?
    var assistiveTouch = AssistiveTouch()
    let assistiveTouchSOS = AssistiveTouch()

    var alertView = SCLAlertView()
     var ticket_listwithExpire = try! Realm().objects(TicketRealmModelNewNew.self).sorted(byKeyPath: "CreatedDate", ascending: false)
    var ticket_list = try! Realm().objects(TicketRealmModelNewNew.self).filter("Status != 0").sorted(byKeyPath: "CreatedDate", ascending: false)
    
  
    
    @IBOutlet weak var TicketTableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return ticket_list.count
    }
        @objc func tapped(sender: UIButton) {
            print("\(sender) has been touched")
            
            
            let actionController = TwitterActionController()
            
            actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            if comVar.DateImplementation {
                
                actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                    
    //                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    //                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
    //                self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                               let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                               self.navigationController?.pushViewController(nextViewController, animated: true)
                               
                               
                }))
                
            }
            
            
            actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            
            actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
                
   let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
              let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
              self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            
            
            actionController.headerData = "Quick View"
            present(actionController, animated: true, completion: nil)
            
            
            
            
        }
    
    override func viewDidAppear(_ animated: Bool) {
         TicketTableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool){
        TicketTableView.reloadData()
        isFromViewdidLoad = true
         timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timerfunc), userInfo: nil, repeats: true)
        //if AssistiveTouchBool == true{
                   assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

                   assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
                   assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
                   let window = UIApplication.shared.keyWindow!
                   window.addSubview(assistiveTouch)
                 //  AssistiveTouchBool = false
              // }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
        tapGesture.numberOfTapsRequired = 1
        assistiveTouchSOS.addGestureRecognizer(tapGesture)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
        longPress.minimumPressDuration = 1.0
        self.assistiveTouchSOS.addGestureRecognizer(longPress)
        assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
        assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
       // view.addSubview(assistiveTouchSOS)
        //TicketTableView.scroll(to: .top, animated: true)
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
           if gesture.state == UIGestureRecognizerState.began {
               UIDevice.vibrate()
               print("Long Press")
               currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                          if Success{
                         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                         let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                           print("\(Address),\(LatsosLat),\(Longsoslong)")
                           MapsScheduleVC.CurrentlocationAddressString = Address
                           MapsScheduleVC.latSOS = LatsosLat
                           MapsScheduleVC.longSOS = Longsoslong
                         self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                  //self.CurrentlocationAddress.text = Address
                              }
                          }
               
           }
       }
     
       @objc func tappedSOS(sender: UIButton) {
        
           let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                   // action here
           }
                   
           SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
          
       }
    override func viewDidDisappear(_ animated: Bool) {
        
        if self.timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
        centralManager.delegate = nil
    }
    
    @objc func timerfunc()
    {
        centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: false])
        centralManager.delegate = self
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusTicketCell",
                                                     for: indexPath) as? BusTicketCell
            else { return UITableViewCell() }
        
        cell.Foreground.layer.cornerRadius = 5
        cell.LeftView.roundCorners(corners: [.topLeft, .bottomLeft], radius: 5.0)
        cell.selectionStyle = .none
        
        if indexPath.row < self.ticket_list.count
                   {
        var ticketDataIndArray: TicketRealmModelNewNew
        ticketDataIndArray = self.ticket_list[indexPath.row] as TicketRealmModelNewNew
        
       // if ticketDataIndArray.RemainingTime > 7200 {
        
        let myDate = ticketDataIndArray.ExpriyDate
        print(myDate.count)
        
                    var Activated_Date = ticketDataIndArray.ActivatedDate
        print(Activated_Date)
                    
                    if Activated_Date == "2000-05-05T03:00:00" || Activated_Date == "2000-05-05T02:00:00" || ticketDataIndArray.Status == 1{
                        
                        Activated_Date = ticketDataIndArray.CreatedDate
                        cell.SourceLabel.text = "PURCHASED DATE"
                        
                    }
                    else{
                        cell.SourceLabel.text = "ACTIVATED DATE"
                    }
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(abbreviation: "EST")
        dateFormatter.timeZone = NSTimeZone(name: "EST") as TimeZone?
        if myDate.count == 23 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }
        else if myDate.count == 19{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
            else if myDate.count == 24{
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            }
        else if myDate.count == 21{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
        }
        else if myDate.count == 22{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        }
        let ExpiryDatetime = dateFormatter.date(from:myDate)!
               //dateFormatter.timeZone = NSTimeZone(name: "EDT") as TimeZone?
               dateFormatter.dateFormat = "E, MMM d, yyyy hh:mm a"
               let ExpiryDatetimeString = dateFormatter.string(from:ExpiryDatetime)
                    
                    let dateFormatter2 = DateFormatter()
                        //dateFormatter.timeZone = TimeZone(abbreviation: "EST")
                        // dateFormatter2.timeZone = NSTimeZone(name: "PT") as TimeZone?
                           if Activated_Date.count == 23 {
                               dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                           }
                           else if Activated_Date.count == 19{
                               dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                           }
                           else if Activated_Date.count == 21{
                               dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
                           }
                           else if Activated_Date.count == 22{
                               dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
                           }
        
       
                    
        let ActivationDatetime = dateFormatter2.date(from:Activated_Date)!
            //  dateFormatter2.timeZone = NSTimeZone(name: "EDT") as TimeZone?
              dateFormatter2.dateFormat = "E, MMM d, yyyy hh:mm a"
              let ActivationDatetimeString = dateFormatter2.string(from:ActivationDatetime)
     
        
        cell.TicketID.text = "#\(ticketDataIndArray.TicketID)"
        cell.RouteID.text = "\(ticketDataIndArray.RouteID)"
//        cell.Date.text = dateString
//        cell.Time.text = TimeString
        cell.Source.text = ActivationDatetimeString
        cell.Destination.text = ExpiryDatetimeString
        cell.ActivateBtn.tag = ticketDataIndArray.TicketID
        let fareTicket = String(format: "%.2f", ticketDataIndArray.Amount)
        cell.Fare.text = "$\(fareTicket)"
       cell.ActivateBtn.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
        if ticketDataIndArray.Status == 1{
            
            cell.Status.text = "New"
            cell.ActivateBtn.setTitle("Click to activate onboard", for: .normal)
             cell.ActivateBtn.backgroundColor = color.hexStringToUIColor(hex: "#2A7FC3")
            cell.LeftView.backgroundColor = color.hexStringToUIColor(hex: "#2A7FC3")
           
            
        }
        else if ticketDataIndArray.Status == 2 {
            cell.Status.text = "Active"
            cell.ActivateBtn.setTitle("View QR Code", for: .normal)
            cell.ActivateBtn.backgroundColor = color.hexStringToUIColor(hex: "#57BB2F")
            cell.LeftView.backgroundColor = color.hexStringToUIColor(hex: "#57BB2F")
        }
        else if ticketDataIndArray.Status == 3 {
            
            cell.Status.text = "Validated"
            cell.ActivateBtn.setTitle("View QR Code", for: .normal)
            cell.ActivateBtn.backgroundColor = color.hexStringToUIColor(hex: "#E53638")
            cell.LeftView.backgroundColor = color.hexStringToUIColor(hex: "#E53638")
            
        }
       // }
        
    }
        
        return cell
    }
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isReachable()
        {
        if isDidSelectBool == true
        {
        expandedIndexPath = indexPath
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "BusTicketCell",
                                                     for: indexPath) as? BusTicketCell
            else { return }
        
        let userAccessToken = UserDefaults.standard.object(forKey: "accessToken")
            if indexPath.row < self.ticket_list.count
            {
        var ticketDataIndArray: TicketRealmModelNewNew
        ticketDataIndArray = self.ticket_list[indexPath.row] as TicketRealmModelNewNew
        comVar.timer = ticketDataIndArray.RemainingTime
         comVar.ticketId = ticketDataIndArray.TicketID
        comVar.status = ticketDataIndArray.Status
        comVar.ExpiryDate = ticketDataIndArray.ExpriyDate
        
        if (ticketDataIndArray.Status == 1) {
            
            self.showLoader()
            
          
            //            // JSON Body
            let body: [String : Any] = [
                "Token": userAccessToken!,
                "TicketId": ticketDataIndArray.TicketID
            ]
            
            print(APIUrl.TicketActivateURL)
            print(body)
            
            Alamofire.request(APIUrl.TicketActivateURL, method: .post,parameters: body, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        if (response.result.error == nil) {
                            
                            print(JSON(response.result.value!))
//                            let ticketup = try! Realm().objects(TicketRealmModelNewNew.self).filter("TicketID == %@", ticketDataIndArray.TicketID).first
//                            try! self.realm.write {
//
//                                ticketup?.Status = 2
//                            }
                            self.view.makeToast("#\(ticketDataIndArray.TicketID) Ticket Activated")
                            TicketSave.TicketDataUpload(CountForRow: 0, Expiry: false, completion: { (SuccessMessage ,Count) in
                                if SuccessMessage {
                                    
                            
                            
                            print("success")
                            UserDefaults.standard.set(true, forKey: "BIBOSelectionFlag")
                          
                          
                            self.TicketTableView.reloadData()
                           // self.TicketTableView.scroll(to: .top, animated: true)
                            self.hideLoader()
                           
                                    }
                                                            })
                            
                        }
                        else {
                            debugPrint("HTTP Request failed: \(String(describing: response.result.error))")
                            self.hideLoader()
                            self.view.makeToast("Server Error! Please try again")
                        }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Server Error! Please try again")
                        self.hideLoader()
                    }
            }
            
        }
        else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
            // vc.newsObj = newsObj
            self.present(vc, animated: true, completion: nil)
            
        }
        
        }
            else
            {
                self.showLoader()
                 TicketSave.TicketDataUpload(CountForRow: 0, Expiry: true, completion: { (SuccessMessage,Count) in
                    if SuccessMessage {
                        self.TicketTableView.reloadData()
                    }
                    else
                    {
                        self.TicketTableView.reloadData()
                    }
                })
            }
        }
        else
        {
            checkBluetoothDevice()
        }
        }
        else
        {
        checkInternetDevice()
        }
    }
        func checkInternetDevice()
        {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            alertView.hideView()
            alertView = SCLAlertView(appearance: appearance)
            
            alertView.addButton("Internet Setting"){
                //ComValue.internetalertstatus = true
                guard let settingsUrl = URL(string:UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        // Checking for setting is opened or not
                        print("Setting is opened: \(success)")
                        
                    })
                }
            }
            alertView.addButton("Close"){
                // self.checkLocationwithoutLogin()
            }
            alertView.showWarning(" Internet", subTitle: "Please enable Internet access for TARC in your settings.")
            
        }
    func checkBluetoothDevice()
    {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        alertView.hideView()
        alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("Bluetooth Setting"){
            //ComValue.internetalertstatus = true
            guard let settingsUrl = URL(string:UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    // Checking for setting is opened or not
                    print("Setting is opened: \(success)")
                    
                })
            }
        }
        alertView.addButton("Close"){
            // self.checkLocationwithoutLogin()
        }
        alertView.showWarning(" Bluetooth", subTitle: "Please enable Bluetooth access for TARC in your settings.")
        
    }
    @objc func OnBackClicked()
        {
            if let vcs = self.navigationController?.viewControllers {
                let previousVC = vcs[vcs.count - 2]
                if previousVC is TicketViewController {
                    // ... and so on
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
                  else  if previousVC is paymentViewController {
                                      // ... and so on
                                      let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                      let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                                      self.navigationController?.pushViewController(nextViewController, animated: false)
                                  }
                else{
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
            // self.navigationController?.popViewController(animated: true)
            
            
        }
        
    
    @objc func buttonSelected(sender: UIButton){
        
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.TicketTableView)
        let indexPath = self.TicketTableView.indexPathForRow(at: buttonPosition)
        
         let cell = self.TicketTableView.cellForRow(at: indexPath!) as! BusTicketCell
        let userAccessToken = UserDefaults.standard.object(forKey: "accessToken")
        comVar.ticketId = sender.tag
        var ticketDataIndArray: TicketRealmModelNewNew
        if indexPath!.row < ticket_list.count {
        ticketDataIndArray = self.ticket_list[indexPath!.row] as TicketRealmModelNewNew
        comVar.timer = ticketDataIndArray.RemainingTime
        comVar.status = ticketDataIndArray.Status
         comVar.ExpiryDate = ticketDataIndArray.ExpriyDate
        if (cell.ActivateBtn!.currentTitle == "Click to activate onboard") {
            
            if isReachable()
            {
            self.showLoader()
            
            print(indexPath!)
            print(sender.tag)
            
//            // JSON Body
            let body: [String : Any] = [
                "Token": userAccessToken!,
                "TicketId": sender.tag
            ]
           

            Alamofire.request(APIUrl.TicketActivateURL, method: .post,parameters: body, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        
                    if (response.result.error == nil) {
//
//                        let ticketup = try! Realm().objects(TicketRealmModelNewNew.self).filter("TicketID == %@", sender.tag).first
//                        try! self.realm.write {
//
//                             ticketup?.Status = 2
//                        }
                        
                        
                         self.view.makeToast("#\(sender.tag) Ticket Activated")
                        TicketSave.TicketDataUpload(CountForRow: 0, Expiry: false, completion: { (SuccessMessage ,Count) in
                                                     if SuccessMessage {
                                                         
                                                 
                                                 
                                                 print("success")
                                                 UserDefaults.standard.set(true, forKey: "BIBOSelectionFlag")
                                               
                                               
                                                 self.TicketTableView.reloadData()
                                                // self.TicketTableView.scroll(to: .top, animated: true)
                                                 self.hideLoader()
                                                
                                                         }
                                                                                 })
                        
                        
//                        print("success")
//                        UserDefaults.standard.set(true, forKey: "BIBOSelectionFlag")

//                        //self.refreshHandler()
//                        TicketSave.TicketDataUpload(completion: { (SuccessMessage,Count) in
//                            if SuccessMessage {
                        
                        
//
                               
                             //   self.TicketTableView.reloadData()
                        //self.TicketTableView.scroll(to: .top, animated: true)
                              //  self.hideLoader()
//                            }
//                            else{
//                                self.hideLoader()
//                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                let vc1 = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//
//                                 self.navigationController?.pushViewController(vc1, animated: true)
//
//                            }
//
//                        })
//

                    }
                    else {
                        debugPrint("HTTP Request failed: \(String(describing: response.result.error))")
                         self.hideLoader()
                         self.view.makeToast("Something went wrong please try again")
                    }
                    case .failure(let error):
                        print(error)
                        self.view.makeToast("Something went wrong please try again")
                         self.hideLoader()
                    }
            }
            }
        }
        else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
            // vc.newsObj = newsObj
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if Reloadtimer != nil {
            Reloadtimer?.invalidate()
            Reloadtimer = nil
        }
         assistiveTouch.removeFromSuperview()
        assistiveTouchSOS.removeFromSuperview()
        TicketTableView.delegate = nil
        TicketTableView.dataSource = nil
        UserDefaults.standard.set(false, forKey: "TickViewController")

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        TicketTableView.estimatedRowHeight = 177
//        TicketTableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
        showLoader()
        UserDefaults.standard.set(true, forKey: "TickViewController")
analytics.GetPageHitCount(PageName: "Ticket List")
        
              TicketSave.TicketDataUpload(CountForRow: 4, Expiry: false, completion: { (SuccessMessage,Count) in
              
                self.hideLoader()
                
            }
            )
        
        
        print(ticket_list)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        
        //self.navigationController?.title = "TRIP PLANNER"
        
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
       
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
       // self.view.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
       backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
                self.navigationItem.title = "My Tickets"
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        menuButton = UIBarButtonItem(title: "Dashboard",
                                     style: UIBarButtonItemStyle.plain,
                                     target: self,
                                     action: #selector(OpenAction))
        self.navigationItem.rightBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem?.tintColor = color.hexStringToUIColor(hex: "#EFAC40")
        
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        
        
        
        
      Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(ticketdataload), userInfo: nil, repeats: false)
        
        
     
       Reloadtimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(refreshdata), userInfo: nil, repeats: true)
        
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(ticketdataloadNotification), name: NSNotification.Name(rawValue: "load"), object: nil)
        
        
        
        
    }
    
    @objc func OpenAction(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
        nextViewController.isRedirectFromLogin = false
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @objc func ticketdataloadNotification(){
        
        print("Reloaded ...... From Appdelegate ")
        

        ticket_list = try! Realm().objects(TicketRealmModelNewNew.self).filter("Status != 0").sorted(byKeyPath: "CreatedDate", ascending: false)
        
        
        
        
        TicketTableView.reloadData()
       // TicketTableView.scroll(to: .top, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
        // vc.newsObj = newsObj
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func ticketdataload(){
        
        print("Reloaded ......\(Date()) ")
        
        ticket_list = try! Realm().objects(TicketRealmModelNewNew.self).filter("Status != 0").sorted(byKeyPath: "CreatedDate", ascending: false)
        
        
        
        TicketTableView.reloadData()
       // TicketTableView.scroll(to: .top, animated: true)
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
        
    }
    @objc func refreshdata(){
        let CheckValidateTicket = try! Realm().objects(TicketRealmModelNewNew.self).filter("Status != 1")
        print(CheckValidateTicket)
        if CheckValidateTicket.count > 0 {
//        TicketSave.TicketDataUpload(CountForRow: 0, Expiry: true, completion: { (SuccessMessage,Count) in
////        if SuccessMessage {
////            print("datasaved \(Data())")
////            print("Reloaded ......**************************************************************************************\(Date()) ")
////
////            self.ticket_list = try! Realm().objects(TicketRealmModelNewNew.self).filter("Status != 0").sorted(byKeyPath: "CreatedDate", ascending: false)
////
////
////
////            self.TicketTableView.reloadData()
////            }
//
//        }
//        )
              self.TicketTableView.reloadData()
            
        }
        
    }
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("central.state is .unknown")
        case .resetting:
            print("central.state is .resetting")
        case .unsupported:
            print("central.state is .unsupported")
        case .unauthorized:
            print("central.state is .unauthorized")
        case .poweredOff:
            print("central.state is .poweredOff")
            isDidSelectBool = false
            if isFromViewdidLoad == true
            {
                isFromViewdidLoad = false
                checkBluetoothDevice()
            }
        case .poweredOn:
            print("central.state is .poweredOn")
            isDidSelectBool = true

            
            
        }
    }
}
class BusTicketCell: UITableViewCell {
    @IBOutlet weak var SourceLabel: UILabel!
    @IBOutlet weak var DestinationLabel: UILabel!
    @IBOutlet weak var Foreground: UIView!
    @IBOutlet weak var LeftView: UIView!
    @IBOutlet weak var Source: UILabel!
    @IBOutlet weak var Destination: UILabel!
    @IBOutlet weak var Fare: UILabel!
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var TicketID: UILabel!
    @IBOutlet weak var RouteID: UILabel!
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var Time: UILabel!
    @IBOutlet weak var ActivateBtn: UIButton!
    
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
//extension UITableView {
//
//    public func reloadData(_ completion: @escaping ()->()) {
//        UIView.animate(withDuration: 0, animations: {
//            self.reloadData()
//        }, completion:{ _ in
//            completion()
//        })
//    }
//
//    func scroll(to: scrollsTo, animated: Bool) {
//        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
//            let numberOfSections = self.numberOfSections
//            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
//            switch to{
//            case .top:
//                if numberOfRows > 0 {
//                    let indexPath = IndexPath(row: 0, section: 0)
//                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
//                }
//                break
//            case .bottom:
//                if numberOfRows > 0 {
//                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
//                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
//                }
//                break
//            }
//        }
//    }
//
//    enum scrollsTo {
//        case top,bottom
//    }
//}
