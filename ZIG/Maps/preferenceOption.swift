//
//	TokenGet.swift


import Foundation 
import ObjectMapper


class preferenceOption : NSObject, NSCoding, Mappable{

	var Message : AnyObject?
	var Bus : Bool?
	var Cab : Bool?
	var Bike : Bool?
	var Bikeshare : Bool?
    var Cabshare : Bool?
    var TokenId : String?



	class func newInstance(map: Map) -> Mappable?{
		return preferenceOption()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		Message <- map["Message"]
		Bus <- map["Bus"]
		Cab <- map["Cab"]
		Bike <- map["Bike"]
		Bikeshare <- map["Bikeshare"]
        Cabshare <- map["Cabshare"]
        TokenId <- map["TokenId"]

        
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         Message = aDecoder.decodeObject(forKey: "Message") as? AnyObject
         Bus = aDecoder.decodeObject(forKey: "Bus") as? Bool
         Cab = aDecoder.decodeObject(forKey: "Cab") as? Bool
         Bike = aDecoder.decodeObject(forKey: "Bike") as? Bool
         Bikeshare = aDecoder.decodeObject(forKey: "Bikeshare") as? Bool
         Cabshare = aDecoder.decodeObject(forKey: "Cabshare") as? Bool
         TokenId = aDecoder.decodeObject(forKey: "TokenId") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if Message != nil{
			aCoder.encode(Message, forKey: "Message")
		}
	
		if Bus != nil{
			aCoder.encode(Bus, forKey: "Bus")
		}
		if Cab != nil{
			aCoder.encode(Cab, forKey: "Cab")
		}
		if Bike != nil{
			aCoder.encode(Bike, forKey: "Bike")
		}
		if Bikeshare != nil{
			aCoder.encode(Bikeshare, forKey: "Bikeshare")
		}
        if Cabshare != nil{
            aCoder.encode(Cabshare, forKey: "Cabshare")
        }
        if TokenId != nil{
            aCoder.encode(TokenId, forKey: "TokenId")
        }
	}

}
