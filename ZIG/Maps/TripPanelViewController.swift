//
//  WhatsNearMePanelViewController.swift
//  ZIG
//
//  Created by Isaac on 30/08/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import MapKit
import PullUpController
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import SafariServices
import SwiftyJSON
import XLActionController
import CoreLocation
import LyftSDK
import UberCore
import UberRides
import AlamofireObjectMapper
import ObjectMapper
import Foundation
import SwiftGifOrigin

class TripPanelViewController: PullUpController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    var RealTimeData: Timer!
    var timer1: Timer!
    var currentPage = 0
    var previousPage = 0
    var templat = 0.0
    var templong = 0.0
    @IBOutlet weak var AlertButton: UIButton!
    var RealTimeTransitOptions: NSMutableArray = []
    enum InitialState {
        case contracted
        case expanded
    }
    var Bike_BikeShareHeight: CGFloat?
    var Bike_WalkingHeight: CGFloat?






    @IBOutlet weak var FareSceen: UIView!
    var DisplayType = ""
    var BikeShareCountNeg = 0
    var RideShareCountNeg = 0
    var BikeShareCount = 0
    var RideShareCount = 0

    // @IBOutlet weak var BuyTicketBtn: UIButton!
    @IBOutlet weak var TripTravlingTime: UILabel!
    @IBOutlet weak var TotalMinTrip: UILabel!

    @IBOutlet weak var TransitPrice: UILabel!
    @IBOutlet weak var TypeOfShare: UILabel!
    @IBOutlet weak var RideBikeSharePrice: UILabel!
    @IBOutlet weak var TotalRegularFare: UILabel!

    @IBOutlet weak var MyTarcTransitPrice: UILabel!
    @IBOutlet weak var MyTarcRideBikeShare: UILabel!
    @IBOutlet weak var TotalMytarcFare: UILabel!

    var BikeRideShare: Bool = false
    var RideShareBool: Bool = false

    var RidescrollView = UIScrollView()
    let scrollView = UIScrollView()
    @IBOutlet weak var BuyTicketBtn: UIButton!
    var TotalMinTripForTravel = 0
    var TransitPriceForTotal = ""
    var TransitTimeForTrip = ""
    var totalTableCellCount = 0
    var BikeShareEnable: Bool = true
    var RideShare: Bool = true
    var timer: Timer?
    var loaderView = UIView()
    var backgroundBlurView = UIView()
    var routeTitleString = NSString()
    var routeString = NSString()
    var SchudleArrayList = [[String: Any]]()
    var SchudleArrayValue: NSMutableDictionary = [:]
    var bikefromaddress: String?
    var biketoAddess: String?
    var SourceAddressString: String?
    var DestinationAddressString: String?
    var NearByItems = [[String: Any]]()
    var selectedLine: Bool = false
    var DataWalkingResponseLouveloStart: DataResponse<SuggestWalkingDirectionalClass>!
    var DataWalkingResponseLouveloEnd: DataResponse<SuggestWalkingDirectionalClass>!
    var louveloBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var initialState: InitialState = .expanded
    var LineSearching = [mapsandSchudleList]()
    var scheduleParts = [BusStopsSchedule]()
    var searching = false
    var firstTimeScrollDisable = true

    var walkingfromaddress: String?
    var walkingtoAddess: String?
    var AmenititesTypeTrip: String?
    var ameniticsTitle: String?
    var BikeCost: Int?
    var isLimeredirect = NSString()

    var databikeResponseFromDirection: DataResponse<SuggestBikeDirectionalClass>!
    var dataWalkingDirection: DataResponse<SuggestWalkingDirectionalClass>!

    var LimeDatalistapicount: Int?
    var LimeDataWalkingResponse: DataResponse<SuggestWalkingDirectionalClass>!

    var LimedataBikeResponse: DataResponse<SuggestBikeDirectionalClass>!

    var LimeWalkingDuration = 0

    var indexValue = 0
    var ii = 0
    var firstMileBool = Bool()
    var TransitRegularCount = [String]()
    var TransitExpressCount = [String]()
    var Stepdatails = [SuggestwalkingStep]()
    var StepdatailsBike = [SuggestBikeStep]()
    var BikeStationFromAddress: String?
    var SourceAddress: String?
    var SourcePlaceID: String?
    var DestinationAddress: String?
    var DestinationPlaceID: String?
    @IBOutlet weak var FavButton: UIButton!

    //var dataWalkingDirection:DataResponse<SuggestWalkingDirectionalClass>!
    // MARK: - IBOutlets


    @IBOutlet private weak var visualEffectView: UIVisualEffectView!
    @IBOutlet private weak var searchBoxContainerView: UIView!
    @IBOutlet private weak var searchSeparatorView: UIView! {
        didSet {
            searchSeparatorView.layer.cornerRadius = searchSeparatorView.frame.height / 2
        }
    }
    @IBOutlet private weak var firstPreviewView: UIView!
    @IBOutlet private weak var secondPreviewView: UIView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var destinationView: UIView!

    //new
    var dataPrevRespone: DataResponse<SuggestDirectionaClass>!
    var dataPrevBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var googlereponseDirection = [[String: Any]]()
    var TempgooglereponseDirection = [[String: Any]]()
    var RealtimeData = [[String: Any]]()
    var isUberAdded: Bool = false

    //
    var realtimeDataResponse: DataResponse<TransitRouteClass>!
    var ticketMenuX = 0, ticketMenuY = 0, ticketMenuWidth = 0, ticketMenuHeight = 0
    var menuTabelView: UITableView = UITableView()
    let cellReuseIdentifier = "cell"
    let TotalPriceLabel = UILabel()
    var TotalBikeShare = 0.0
    let TotalRideShare = 0.0
    var sourceAddressLat: Double?
    var DestinationLat: Double?
//       var SourceAddressString:String?
//       var DestinationAddressString:String?

    let TotalFareLabel = UIView()
    var sourceAddressLong: Double?
    var DestinationLong: Double?
    var ubertotalprice: Double = 0.0
    // var ii = 0

    //
    var firstMiletableView: UITableView!
    var route_list = [mapsandSchudleList]()
    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return searchBoxContainerView?.frame.height ?? 0
        case .expanded:
            return pullUpControllerPreferredSize.height
        }
    }

    @IBOutlet weak var SourceAddressLbl: UILabel!
    @IBOutlet weak var DestinationAddressLbl: UILabel!

    private var locations = [(title: String, location: CLLocationCoordinate2D)]()

    public var portraitSize: CGSize = .zero
    public var landscapeFrame: CGRect = .zero

    @IBOutlet weak var conditionOneLbl: UILabel!
    @IBOutlet weak var regulartextLabel: UILabel!
    @IBOutlet weak var mytractextLabel: UILabel!
    @IBOutlet weak var totaltextLabel: UILabel!
    @IBOutlet weak var transittextLabel: UILabel!
    @IBOutlet weak var conditionTwoLbl: UILabel!

    // ipad
    var transittextLabelnew =  UILabel()
                  var secondLabelnew =  UILabel()
                  var totalfareLabelnew =  UILabel()
                  var transitfareregularipad =  UILabel()
                  var transitfaremytracipad =  UILabel()
                  var secondregularfare =  UILabel()
                  var secondmytarcfare =  UILabel()
                  var totalregularfareipad =  UILabel()
                var totalmytarcfareipad =  UILabel()
                    var buyButtonIpad = UIButton()
    var regularipadLabel = UILabel()
                          var mytracipadLabel = UILabel()
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        print("\(DisplayType) DisplayType")
            ipadDesign()
        if SourceDestination.DisplayType == "Transit" {
            
            
            //timer1 = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(downloadTimer), userInfo: nil, repeats: true)
            //    Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(self.TankRunTimedCode), userInfo: nil, repeats: true)
            let valueResult = dataPrevRespone.result.value
            let routesPlan = valueResult?.routes![indexValue].legs
            let stepValueArraycount = routesPlan![0].steps?.count
            totalTableCellCount = routesPlan![0].steps!.count
            //tableView.estimatedRowHeight = 230
            //tableView.rowHeight = UITableViewAutomaticDimension
            SourceAddressLbl.text = SourceDestination.SourceAddress
            DestinationAddressLbl.text = SourceDestination.DestinationAddress
            //        PDF.layer.zPosition = 1;
            //        UIApplication.shared.keyWindow!.bringSubview(toFront: searchBoxContainerView)
            //        self.searchBoxContainerView.sendSubview(toBack: firstPreviewView)
            
            
               if UIDevice.current.userInterfaceIdiom == .pad
               {
                BuyTicketBtn.frame = CGRect(x: self.view.frame.width - 120, y: 53, width: 100, height: 30)
             

                }

        }
        else {
            //  FareSceen.subviews.forEach({ $0.removeFromSuperview() })

            for v in FareSceen.subviews {
                if v is UILabel {
                    v.removeFromSuperview()
                }
            }

            TotalPriceLabel.frame = CGRect(x: self.view.frame.width - 120, y: 33, width: 100, height: 30)

            TotalPriceLabel.textAlignment = .right
            TotalPriceLabel.font = UIFont.setTarcSemiBold(size: 28.0)
//                 FareSceen.addSubview(TotalPriceLabel)
//            TotalPriceLabel.bringSubview(toFront: FareSceen)
            self.view.addSubview(TotalPriceLabel)
            tableView.estimatedRowHeight = 1600
            tableView.rowHeight = UITableViewAutomaticDimension
//

        }
        // tableView.register(UINib(nibName: "WalkingCell", bundle: nil), forCellReuseIdentifier: WalkingCell)
        tableView.register(UINib(nibName: "WalkingCell", bundle: nil), forCellReuseIdentifier: "WalkingCell")
        tableView.register(UINib(nibName: "WalkingCellBikeShare", bundle: nil), forCellReuseIdentifier: "WalkingCellBikeShare")
        tableView.register(UINib(nibName: "BikeCellBikeShare", bundle: nil), forCellReuseIdentifier: "BikeCellBikeShare")
        //

        routeTitleString = ""


        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
            height: secondPreviewView.frame.maxY)
        landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 300)

        tableView.attach(to: self)

        var routeIDREMOVEzero = RealTimeSchedule.SelectedSchedule
        while routeIDREMOVEzero.hasPrefix("0") {
            routeIDREMOVEzero.remove(at: routeIDREMOVEzero.startIndex)
        }

        if SourceDestination.DisplayType == "Transit" {
            getDirectionFromGoogle()
        }
        else if SourceDestination.DisplayType == "Bike" {

            getBikeDirection()




        }
        else if SourceDestination.DisplayType == "LouVelo" {

            getBicycle()

        }

        pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)


        }
    func ipadDesign()
    {
        
        if UIDevice.current.userInterfaceIdiom == .pad
                      {
                       conditionOneLbl.font = UIFont.setTarcRegular(size: 15)
                       conditionTwoLbl.font = UIFont.setTarcRegular(size: 15)
                        let xpostion = transittextLabel.frame.origin.x
                        print(xpostion)
                        //searchBoxContainerView
                        transittextLabel.isHidden = true
                        totaltextLabel.isHidden = true
                        regulartextLabel.isHidden = true
                        mytractextLabel.isHidden = true
                      

                         regularipadLabel = UILabel()
                        regularipadLabel.frame =  CGRect.init(x: self.view.frame.size.width-120, y: 5, width: 50, height: 15)
                        regularipadLabel.text = "Regular"
                        regularipadLabel.font = UIFont.setTarcBold(size: 13)
                        regularipadLabel.textColor = UIColor.init(red: 41/255, green: 126/255, blue: 196/255, alpha: 1.0)
                        searchBoxContainerView .addSubview(regularipadLabel)
                        
                         mytracipadLabel = UILabel()
                        mytracipadLabel.frame =  CGRect.init(x: regularipadLabel.frame.maxX+5, y: 5, width: 70, height: 15)
                        mytracipadLabel.text = "   TARC "
                        mytracipadLabel.font = UIFont.setTarcBold(size: 13)
                        mytracipadLabel.textColor = UIColor.init(red: 255/255, green: 59/255, blue: 48/255, alpha: 1.0)

                        searchBoxContainerView .addSubview(mytracipadLabel)
                        
                         transittextLabelnew =  UILabel()
                        transittextLabelnew.frame = CGRect.init(x: self.view.frame.size.width-200, y: regularipadLabel.frame.maxY+10, width: 80, height: 15)
                        transittextLabelnew.text = "Transit Fare"
                        transittextLabelnew.font = UIFont.setTarcSemiBold(size: 13)
                        transittextLabelnew.textColor = UIColor.init(red: 41/255, green: 126/255, blue: 196/255, alpha: 1.0)

                        searchBoxContainerView .addSubview(transittextLabelnew)
                        
                         secondLabelnew =  UILabel()
                        secondLabelnew.frame = CGRect.init(x: self.view.frame.size.width-200, y: transittextLabelnew.frame.maxY+5, width: 80, height: 15)
                        secondLabelnew.font = UIFont.setTarcSemiBold(size: 13)
                        secondLabelnew.textColor = UIColor.init(red: 41/255, green: 126/255, blue: 196/255, alpha: 1.0)

                        searchBoxContainerView .addSubview(secondLabelnew)
                        
                         totalfareLabelnew =  UILabel()
                        totalfareLabelnew.frame = CGRect.init(x: self.view.frame.size.width-200, y: secondLabelnew.frame.maxY+5, width: 80, height: 15)
                        totalfareLabelnew.text = "Total Fare:"
                        totalfareLabelnew.font = UIFont.setTarcBold(size: 13)
                        searchBoxContainerView .addSubview(totalfareLabelnew)
                      
                        
                        
                         transitfareregularipad =  UILabel()
                        transitfareregularipad.frame = CGRect.init(x:transittextLabelnew.frame.maxX+5, y: regularipadLabel.frame.maxY+10, width: 50, height: 15)
                        transitfareregularipad.font = UIFont.setTarcSemiBold(size: 13)
                        transitfareregularipad.textAlignment = .left
                        searchBoxContainerView .addSubview(transitfareregularipad)
                        
                         transitfaremytracipad =  UILabel()
                        transitfaremytracipad.frame = CGRect.init(x: transitfareregularipad.frame.maxX+5, y: regularipadLabel.frame.maxY+10, width: 50, height: 15)
                        transitfaremytracipad.font = UIFont.setTarcSemiBold(size: 13)
                        transitfaremytracipad.textAlignment = .left
                        searchBoxContainerView .addSubview(transitfaremytracipad)
                        
                         secondregularfare =  UILabel()
                        secondregularfare.frame = CGRect.init(x:secondLabelnew.frame.maxX+5, y: transitfareregularipad.frame.maxY+5, width: 50, height: 15)
                        secondregularfare.font = UIFont.setTarcSemiBold(size: 13)
                        searchBoxContainerView .addSubview(secondregularfare)
                        
                         secondmytarcfare =  UILabel()
                        secondmytarcfare.frame = CGRect.init(x: secondregularfare.frame.maxX+5, y: transitfareregularipad.frame.maxY+5, width: 50, height: 15)
                        secondmytarcfare.font = UIFont.setTarcSemiBold(size: 13)
                        searchBoxContainerView .addSubview(secondmytarcfare)
                        
                         totalregularfareipad =  UILabel()
                        totalregularfareipad.frame = CGRect.init(x:totalfareLabelnew.frame.maxX+5, y: secondregularfare.frame.maxY+5, width: 50, height: 15)
                        totalregularfareipad.font = UIFont.setTarcBold(size: 13)
                        totalregularfareipad.textColor = UIColor.init(red: 41/255, green: 126/255, blue: 196/255, alpha: 1.0)

                        searchBoxContainerView .addSubview(totalregularfareipad)
                        
                         totalmytarcfareipad =  UILabel()
                        totalmytarcfareipad.frame = CGRect.init(x: totalregularfareipad.frame.maxX+5, y: secondregularfare.frame.maxY+5, width: 50, height: 15)
                        totalmytarcfareipad.font = UIFont.setTarcBold(size: 13)
                        totalmytarcfareipad.textColor = UIColor.init(red: 255/255, green: 59/255, blue: 48/255, alpha: 1.0)

                        searchBoxContainerView .addSubview(totalmytarcfareipad)
                        
                        
                         buyButtonIpad = UIButton()
                        buyButtonIpad.frame = CGRect(x: self.view.frame.width - 140, y: totalmytarcfareipad.frame.maxY+10, width: 120, height: 30)
                        buyButtonIpad.backgroundColor = UIColor.red
                        searchBoxContainerView .addSubview(buyButtonIpad)
                        buyButtonIpad.addTarget(self, action: #selector(OpenApp(_:)), for: .touchUpInside)
                        
                        
                        TransitPrice.isHidden = true
                        MyTarcTransitPrice.isHidden = true
                        
                        RideBikeSharePrice.isHidden = true
                        MyTarcRideBikeShare.isHidden = true
                       
                        TotalMytarcFare.isHidden = true
                        TotalRegularFare.isHidden = true
                        TypeOfShare.isHidden = true
                        BuyTicketBtn.isHidden = true

                       }
    }
        func getBicycle() {
            BuyTicketBtn.backgroundColor = color.hexStringToUIColor(hex: "#8DC63F")

            BuyTicketBtn.setTitle("Get LouVelo", for: .normal)
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                buyButtonIpad.backgroundColor = color.hexStringToUIColor(hex: "#8DC63F")

                           buyButtonIpad.setTitle("Get LouVelo", for: .normal)
                transittextLabelnew.isHidden = true
                secondLabelnew.isHidden = true
                totalfareLabelnew.isHidden = true
                regularipadLabel.isHidden = true
                mytracipadLabel.isHidden = true
            }
            let googlereponseDirectionList: NSMutableDictionary = [:]

            let StartWalkingDirection = DataWalkingResponseLouveloStart.result.value?.route?.first?.legs![0]
            let BikeDirection = louveloBikeResponse.result.value?.routes?.first?.legs![0]
            let EndWalkingDirection = DataWalkingResponseLouveloEnd.result.value?.route?.first?.legs![0]





            SourceAddressLbl.text = "\((StartWalkingDirection?.start_address)!)"
            DestinationAddressLbl.text = "\((EndWalkingDirection!.end_address)!)"





            var StartInstrationArray = [String]()
            for StartStepsArray in StartWalkingDirection!.steps! {

                var StartWithoutHTMLStep = StartStepsArray.htmlInstructions?.withoutHtml
                StartWithoutHTMLStep = "\u{2794} \((StartWithoutHTMLStep)!)"
                StartInstrationArray.append("\((StartWithoutHTMLStep)!)")


            }
            print("InstrationArray Count - \(StartInstrationArray.count)")
            Bike_WalkingHeight = CGFloat(StartInstrationArray.count * 10) + 124.0
            let StartHtmlInstration = StartInstrationArray.joined(separator: "\n")

            let CurrentTime = Date().timeIntervalSince1970
            let Startwalkingmin = (StartWalkingDirection!.duration?.text)!
            let Startwalkingmiles = (StartWalkingDirection!.distance?.text)!
            let Startwalkingdistancevalue: Double = Double((StartWalkingDirection!.distance?.value)!)
            let Startcaloriesburn = (Startwalkingdistancevalue * 1.25) / 20
            let Startcaloriesburnint: Int = Int(Startcaloriesburn.rounded(.toNearestOrAwayFromZero))
            // cell.TransitRouteNo.isHidden = true
            // cell.TransitTime.frame.origin.
            // let walkingarriaval = walkingdeparttime
            let StartstartdatewalkingTime = dateconvert(Datevalue: CurrentTime, WalkingTimeinterval: 0, addsub: "add")





            let FirstAddressGoogle = "Walk to \((StartWalkingDirection?.end_address)!). "
            let SecondDescription = "About \(Startwalkingmin), \(Startwalkingmiles) (\(Startcaloriesburnint) Calories)"

            googlereponseDirectionList.setValue("false", forKey: "RealTime")
            googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
            googlereponseDirectionList.setValue("", forKey: "TripStatus")
            googlereponseDirectionList.setValue("0", forKey: "Index")
            googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
            googlereponseDirectionList.setValue("\(StartstartdatewalkingTime)", forKey: "Timing")
            googlereponseDirectionList.setValue("", forKey: "RouteLine")
            googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
            googlereponseDirectionList.setValue("", forKey: "NextStop")
            googlereponseDirectionList.setValue("Bike_Walking", forKey: "ModeOFTransit")
            googlereponseDirectionList.setValue(StartWalkingDirection!.distance?.value, forKey: "TravelingTime")
            googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
            googlereponseDirectionList.setValue("\((StartHtmlInstration))", forKey: "WalkingWaitingTime")

            googlereponseDirectionList.setValue(StartWalkingDirection!.start_location!.lat!, forKey: "SourceLat")
            googlereponseDirectionList.setValue(StartWalkingDirection!.start_location!.lng!, forKey: "SourceLng")
            googlereponseDirectionList.setValue(StartWalkingDirection!.end_location!.lat!, forKey: "DestinationLat")
            googlereponseDirectionList.setValue(StartWalkingDirection!.end_location!.lng!, forKey: "DestinationLng")
            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

            googlereponseDirectionList.removeAllObjects()

            let vehicleStation = BikeDirection?.startAddress
            let CO2Emmision = (Double((BikeDirection?.distance!.value)!) * 0.0000419426)
            let test = String(format: "%.2f", CO2Emmision)
            let AboutContent = "About \((BikeDirection?.duration?.text)!), \((BikeDirection?.distance?.text)!) \(test) lbs Co2"
            let Color = "#8DC63F"
            let VImage = "Louvelomap"

            var BikeInstrationArray = [String]()
            for BikeStepsArray in BikeDirection!.steps! {

                var BikeWithoutHTMLStep = BikeStepsArray.htmlInstructions?.withoutHtml
                BikeWithoutHTMLStep = "\u{2794} \((BikeWithoutHTMLStep)!)"
                BikeInstrationArray.append("\((BikeWithoutHTMLStep)!)")


            }
            Bike_BikeShareHeight = CGFloat(BikeInstrationArray.count * 17) + 175.0
            print("InstrationArray Count - \(BikeInstrationArray.count)")
            let BikeHtmlInstration = BikeInstrationArray.joined(separator: "\n")

            let sectominCalWithFare: Double = (Double((BikeDirection?.duration!.value)! / 60).rounded(.toNearestOrAwayFromZero))
            print(sectominCalWithFare)

            let louvelodivision = sectominCalWithFare / 30
            let roundedupValue = louvelodivision.rounded(.up)
            let totalAmount = roundedupValue * 3.5

            let BikeshareFare = String(format: "$%.2f", totalAmount)

            let StartTimeBike = dateconvert(Datevalue: CurrentTime, WalkingTimeinterval: (StartWalkingDirection?.duration!.value)!, addsub: "add")
            let Totalmin = ((StartWalkingDirection?.duration!.value)!) + ((BikeDirection?.duration!.value)!)
            // TotalMinTrip.text = convertTime(miliseconds: Totalmin*1000)
            let EndBike = dateconvert(Datevalue: CurrentTime, WalkingTimeinterval: Totalmin, addsub: "add")

            googlereponseDirectionList.setValue("1", forKey: "Index")
            googlereponseDirectionList.setValue("Bike_BikeShare", forKey: "ModeOFTransit")
            googlereponseDirectionList.setValue("\(isLimeredirect)", forKey: "vehicleType")
            googlereponseDirectionList.setValue("TotalBirdTime", forKey: "TotalTime")
            googlereponseDirectionList.setValue("\(Color)", forKey: "vehicleColor")
            googlereponseDirectionList.setValue("\(VImage)", forKey: "vehicleTypeImage")
            googlereponseDirectionList.setValue("\(BikeHtmlInstration)", forKey: "WalkingInstructions")
            googlereponseDirectionList.setValue("", forKey: "WalkingAboutandCaloties")
            googlereponseDirectionList.setValue("", forKey: "WaitingTime")
            googlereponseDirectionList.setValue("\(EndBike)", forKey: "WakingStartTime")
            googlereponseDirectionList.setValue("Pickup your \(isLimeredirect) Vehicle from", forKey: "VehiclePickupLocation")
            googlereponseDirectionList.setValue("\((vehicleStation)!)", forKey: "vehicleStation")
            googlereponseDirectionList.setValue("Get \(isLimeredirect) Estimated Fare:\(BikeshareFare)", forKey: "Fare")
            googlereponseDirectionList.setValue("\((AboutContent))", forKey: "WalkingAboutandCo2")
            googlereponseDirectionList.setValue("\(StartTimeBike)", forKey: "vehicleTime")

            googlereponseDirectionList.setValue(BikeDirection?.startLocation?.lat, forKey: "SourceLat")
            googlereponseDirectionList.setValue(BikeDirection?.startLocation?.lng, forKey: "SourceLng")
            googlereponseDirectionList.setValue(BikeDirection?.endLocation?.lat, forKey: "DestinationLat")

            googlereponseDirectionList.setValue(BikeDirection?.endLocation?.lng, forKey: "DestinationLng")

            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

            googlereponseDirectionList.removeAllObjects()


            // let CurrentTime = Date().timeIntervalSince1970
            let Endwalkingmin = (EndWalkingDirection!.duration?.text)!
            let Endwalkingmiles = (EndWalkingDirection!.distance?.text)!
            let Endwalkingdistancevalue: Double = Double((EndWalkingDirection!.distance?.value)!)
            let Endcaloriesburn = (Endwalkingdistancevalue * 1.25) / 20
            let Endcaloriesburnint: Int = Int(Endcaloriesburn.rounded(.toNearestOrAwayFromZero))
            // cell.TransitRouteNo.isHidden = true
            // cell.TransitTime.frame.origin.
            // let walkingarriaval = walkingdeparttime
            let totaltraveltimelouLast = ((StartWalkingDirection?.duration!.value)!) + ((BikeDirection?.duration!.value)!) + ((EndWalkingDirection?.duration!.value)!)
            let endstartdatewalkingTime = dateconvert(Datevalue: CurrentTime, WalkingTimeinterval: totaltraveltimelouLast, addsub: "add")

            let EndFirstAddressGoogle = "Walk to \((EndWalkingDirection?.end_address)!). "
            let EndSecondDescription = "About \(Endwalkingmin), \(Endwalkingmiles) (\(Endcaloriesburnint) Calories)"
            var EndInstrationArray = [String]()
            for EndstepsArray in EndWalkingDirection!.steps! {

                var EndWithoutHTMLStep = EndstepsArray.htmlInstructions?.withoutHtml
                EndWithoutHTMLStep = "\u{2794} \((EndWithoutHTMLStep)!)"
                EndInstrationArray.append("\((EndWithoutHTMLStep)!)")


            }
            print("InstrationArray Count - \(StartInstrationArray.count)")
            Bike_WalkingHeight = CGFloat(StartInstrationArray.count * 10) + 124.0
            let EndHtmlInstration = EndInstrationArray.joined(separator: "\n")

            googlereponseDirectionList.setValue("false", forKey: "RealTime")
            googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
            googlereponseDirectionList.setValue("", forKey: "TripStatus")
            googlereponseDirectionList.setValue("0", forKey: "Index")
            googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
            googlereponseDirectionList.setValue(EndFirstAddressGoogle, forKey: "FirstAddress")
            googlereponseDirectionList.setValue(EndSecondDescription, forKey: "SecondAddress")
            googlereponseDirectionList.setValue("\(endstartdatewalkingTime)", forKey: "Timing")
            googlereponseDirectionList.setValue("", forKey: "RouteLine")
            googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
            googlereponseDirectionList.setValue("", forKey: "NextStop")
            googlereponseDirectionList.setValue("Bike_Walking", forKey: "ModeOFTransit")
            googlereponseDirectionList.setValue(EndWalkingDirection!.distance?.value, forKey: "TravelingTime")
            googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
            googlereponseDirectionList.setValue("\((EndHtmlInstration))", forKey: "WalkingWaitingTime")

            googlereponseDirectionList.setValue(EndWalkingDirection!.start_location!.lat!, forKey: "SourceLat")
            googlereponseDirectionList.setValue(EndWalkingDirection!.start_location!.lng!, forKey: "SourceLng")
            googlereponseDirectionList.setValue(EndWalkingDirection!.end_location!.lat!, forKey: "DestinationLat")
            googlereponseDirectionList.setValue(EndWalkingDirection!.end_location!.lng!, forKey: "DestinationLng")

            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])



            //        @IBOutlet weak var TripTravlingTime: UILabel!
            //        @IBOutlet weak var TotalMinTrip: UILabel!
            //        @IBOutlet weak var TransitPrice: UILabel!


            TripTravlingTime.text = "\(StartstartdatewalkingTime) - \(endstartdatewalkingTime)"
            let totaltraveltimelou = ((EndWalkingDirection?.duration!.value)!) + ((BikeDirection?.duration!.value)!) + ((EndWalkingDirection?.duration!.value)!)



            TotalMinTrip.text = convertTime(miliseconds: totaltraveltimelou * 1000)
            TotalPriceLabel.text = BikeshareFare
            TotalPriceLabel.textColor = color.hexStringToUIColor(hex: "#FF0300")
            //  TransitPrice.text = "\(BikeshareFare)"
            // MyTarcTransitPrice.text = ""

        }
        func getBikeDirection() {
            let googlereponseDirectionList: NSMutableDictionary = [:]
            let WalkingDirection = dataWalkingDirection.result.value?.route?.first?.legs![0]

            var waitingTravel = ""


            var InstrationArray = [String]()
            for StepsArray in WalkingDirection!.steps! {

                var WithoutHTMLStep = StepsArray.htmlInstructions?.withoutHtml
                WithoutHTMLStep = "\u{2794} \((WithoutHTMLStep)!)"
                InstrationArray.append("\((WithoutHTMLStep)!)")


            }
            print("InstrationArray Count - \(InstrationArray.count)")
            Bike_WalkingHeight = CGFloat(InstrationArray.count * 10) + 124.0
            let HtmlInstration = InstrationArray.joined(separator: "\n")




            // var waitingtime = ""

            print((WalkingDirection!.distance?.value)!)
            var durationString = 0
            var getstime = ""
            var uberwaitingtime = ""
            var arrivaltime: Double = 0.0



            //var TransitTime = ""

            let CurrentTime = Date().timeIntervalSince1970
            let walkingmin = (WalkingDirection!.duration?.text)!
            let walkingmiles = (WalkingDirection!.distance?.text)!
            let walkingdistancevalue: Double = Double((WalkingDirection!.distance?.value)!)
            let caloriesburn = (walkingdistancevalue * 1.25) / 20
            let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
            // cell.TransitRouteNo.isHidden = true
            // cell.TransitTime.frame.origin.
            // let walkingarriaval = walkingdeparttime
            let startdatewalkingTime = dateconvert(Datevalue: CurrentTime, WalkingTimeinterval: 0, addsub: "add")




            let FirstAddressGoogle = "Walk to \((WalkingDirection?.end_address)!). "
            let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

            googlereponseDirectionList.setValue("false", forKey: "RealTime")
            googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
            googlereponseDirectionList.setValue("", forKey: "TripStatus")
            googlereponseDirectionList.setValue("0", forKey: "Index")
            googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
            googlereponseDirectionList.setValue("\(startdatewalkingTime)", forKey: "Timing")
            googlereponseDirectionList.setValue("", forKey: "RouteLine")
            googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
            googlereponseDirectionList.setValue("", forKey: "NextStop")
            googlereponseDirectionList.setValue("Bike_Walking", forKey: "ModeOFTransit")
            googlereponseDirectionList.setValue(WalkingDirection!.distance?.value, forKey: "TravelingTime")
            googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
            googlereponseDirectionList.setValue("\((HtmlInstration))", forKey: "WalkingWaitingTime")

            googlereponseDirectionList.setValue(WalkingDirection!.start_location!.lat!, forKey: "SourceLat")
            googlereponseDirectionList.setValue(WalkingDirection!.start_location!.lng!, forKey: "SourceLng")
            googlereponseDirectionList.setValue(WalkingDirection!.end_location!.lat!, forKey: "DestinationLat")
            googlereponseDirectionList.setValue(WalkingDirection!.end_location!.lng!, forKey: "DestinationLng")
            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
            print("\(WalkingDirection!.start_location!.lat!) \(WalkingDirection!.start_location!.lng!), \(WalkingDirection!.end_location!.lat!) \(WalkingDirection!.end_location!.lng!)")

            //        @IBOutlet weak var TripTravlingTime: UILabel!
            //        @IBOutlet weak var TotalMinTrip: UILabel!
            //        @IBOutlet weak var TransitPrice: UILabel!



            //tableView.reloadData()

            let BirdDirection = databikeResponseFromDirection.result.value?.routes?.first?.legs![0]

            SourceAddressLbl.text = "\((WalkingDirection?.start_address)!)"
            DestinationAddressLbl.text = "\((BirdDirection?.endAddress)!)"



            var BikeInstrationArray = [String]()
            for BikeStepsArray in BirdDirection!.steps! {

                var BikeWithoutHTMLStep = BikeStepsArray.htmlInstructions?.withoutHtml
                BikeWithoutHTMLStep = "\u{2794} \((BikeWithoutHTMLStep)!)"
                BikeInstrationArray.append("\((BikeWithoutHTMLStep)!)")


            }
            Bike_BikeShareHeight = CGFloat(BikeInstrationArray.count * 17) + 175.0
            print("InstrationArray Count - \(BikeInstrationArray.count)")
            let BikeHtmlInstration = BikeInstrationArray.joined(separator: "\n")
            var Color = ""
            var VImage = ""

//        @IBOutlet weak var TripTravlingTime: UILabel!
//        @IBOutlet weak var TotalMinTrip: UILabel!
//        @IBOutlet weak var TransitPrice: UILabel!
            let Totalmin = ((WalkingDirection?.duration!.value)!) + ((BirdDirection?.duration!.value)!)
            TotalMinTrip.text = convertTime(miliseconds: Totalmin * 1000)


            let BirdTransitDuration = BirdDirection?.duration!.value
            let StartTimeBike = dateconvert(Datevalue: CurrentTime, WalkingTimeinterval: (WalkingDirection?.duration!.value)!, addsub: "add")
            let EndBike = dateconvert(Datevalue: CurrentTime, WalkingTimeinterval: Totalmin, addsub: "add")

            TripTravlingTime.text = "\(startdatewalkingTime) - \(EndBike)"

            var BikeshareFare = ""
            if isLimeredirect == "Lime" {
                Color = "#69DF39"
                VImage = "Lime"
                BuyTicketBtn.backgroundColor = color.hexStringToUIColor(hex: "#69DF39")

                BuyTicketBtn.setTitle("Get Lime", for: .normal)
                if UIDevice.current.userInterfaceIdiom == .pad
                           {
                            buyButtonIpad.backgroundColor = color.hexStringToUIColor(hex: "#69DF39")

                                           buyButtonIpad.setTitle("Get Lime", for: .normal)
                            transittextLabelnew.isHidden = true
                                           secondLabelnew.isHidden = true
                                           totalfareLabelnew.isHidden = true
                                           regularipadLabel.isHidden = true
                                           mytracipadLabel.isHidden = true
                }
                let sectominCalWithFares: Double = (Double(BirdTransitDuration! / 60).rounded(.toNearestOrAwayFromZero))

                let BirdBike_fare_Cals = ((sectominCalWithFares * 0.27) + 1)

                BikeshareFare = String(format: "$%.2f", BirdBike_fare_Cals)

            }
            else if isLimeredirect == "Bird" {
                BuyTicketBtn.backgroundColor = color.hexStringToUIColor(hex: "#000000")

                BuyTicketBtn.setTitle("Get Bird", for: .normal)
                if UIDevice.current.userInterfaceIdiom == .pad
                                          {
                                            buyButtonIpad.backgroundColor = color.hexStringToUIColor(hex: "#000000")

                                                          buyButtonIpad.setTitle("Get Bird", for: .normal)
                                            transittextLabelnew.isHidden = true
                                                           secondLabelnew.isHidden = true
                                                           totalfareLabelnew.isHidden = true
                                                           regularipadLabel.isHidden = true
                                                           mytracipadLabel.isHidden = true

                }
                Color = "#000000"
                VImage = "Bird-1"

                let durationStringADD = BirdTransitDuration
                let sectominCalWithFare: Double = (Double(durationStringADD! / 60).rounded(.toNearestOrAwayFromZero))

                let BirdBike_fare_Cal = ((sectominCalWithFare * 0.15) + 1)
                BikeshareFare = String(format: "$%.2f", BirdBike_fare_Cal)


            }
            else if isLimeredirect == "Bolt" {
                Color = "#F7DD4B"
                VImage = "Bolt"
                BuyTicketBtn.backgroundColor = color.hexStringToUIColor(hex: "#F7DD4B")

                BuyTicketBtn.setTitle("Get Bolt", for: .normal)
                if UIDevice.current.userInterfaceIdiom == .pad
                    {
                        buyButtonIpad.backgroundColor = color.hexStringToUIColor(hex: "#F7DD4B")

                                      buyButtonIpad.setTitle("Get Bolt", for: .normal)
                        transittextLabelnew.isHidden = true
                                       secondLabelnew.isHidden = true
                                       totalfareLabelnew.isHidden = true
                                       regularipadLabel.isHidden = true
                                       mytracipadLabel.isHidden = true
                }
                let durationStringADD = BirdTransitDuration
                let sectominCalWithFare: Double = (Double(durationStringADD! / 60).rounded(.toNearestOrAwayFromZero))

                let BirdBike_fare_Cal = (sectominCalWithFare * 0.35)
                BikeshareFare = String(format: "$%.2f", BirdBike_fare_Cal)


                BikeshareFare = String(format: "$%.2f", BirdBike_fare_Cal)
            }
            TransitPrice.text = "\(BikeshareFare)"
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                transitfareregularipad.text = "\(BikeshareFare)"

            }
            let CO2Emmision = (Double((BirdDirection?.distance!.value)!) * 0.0000419426)
            let test = String(format: "%.2f", CO2Emmision)
            let vehicleStation = databikeResponseFromDirection.result.value?.routes?.first?.legs![0].startAddress
            let AboutContent = "About \((BirdDirection?.duration?.text)!), \((BirdDirection?.distance?.text)!) \(test) lbs Co2"
            googlereponseDirectionList.setValue("1", forKey: "Index")
            googlereponseDirectionList.setValue("Bike_BikeShare", forKey: "ModeOFTransit")
            googlereponseDirectionList.setValue("\(isLimeredirect)", forKey: "vehicleType")
            googlereponseDirectionList.setValue("TotalBirdTime", forKey: "TotalTime")
            googlereponseDirectionList.setValue("\(Color)", forKey: "vehicleColor")
            googlereponseDirectionList.setValue("\(VImage)", forKey: "vehicleTypeImage")
            googlereponseDirectionList.setValue("\(BikeHtmlInstration)", forKey: "WalkingInstructions")
            googlereponseDirectionList.setValue("", forKey: "WalkingAboutandCaloties")
            googlereponseDirectionList.setValue("", forKey: "WaitingTime")
            googlereponseDirectionList.setValue("\(EndBike)", forKey: "WakingStartTime")
            googlereponseDirectionList.setValue("Pickup your \(isLimeredirect) Vehicle from", forKey: "VehiclePickupLocation")
            googlereponseDirectionList.setValue("\((vehicleStation)!)", forKey: "vehicleStation")
            googlereponseDirectionList.setValue("Get \(isLimeredirect) Estimated Fare:\(BikeshareFare)", forKey: "Fare")
            googlereponseDirectionList.setValue("\((AboutContent))", forKey: "WalkingAboutandCo2")
            googlereponseDirectionList.setValue("\(StartTimeBike)", forKey: "vehicleTime")

            googlereponseDirectionList.setValue(BirdDirection?.startLocation?.lat, forKey: "SourceLat")
            googlereponseDirectionList.setValue(BirdDirection?.startLocation?.lng, forKey: "SourceLng")
            googlereponseDirectionList.setValue(BirdDirection?.endLocation?.lat, forKey: "DestinationLat")
            googlereponseDirectionList.setValue(BirdDirection?.endLocation?.lng, forKey: "DestinationLng")
            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

            print("\((BirdDirection?.startLocation?.lng)!) \((BirdDirection?.startLocation?.lat)!), \((BirdDirection?.endLocation?.lat)!) \((BirdDirection?.endLocation?.lng)!)")
            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

            print(googlereponseDirection)

            TotalPriceLabel.text = BikeshareFare
            TotalPriceLabel.textColor = color.hexStringToUIColor(hex: "#FF0300")
        }

        func completeFunction(ArrayCount: String) {
            var RegularTransitFare = 0.0
            var ExpressTransitFare = 0.0
            let valueResult = dataPrevRespone.result.value
            let routesPlan = valueResult?.routes![indexValue].legs
            let stepValueArraycount = routesPlan![0].steps?.count
            // totalTableCellCount =  totalTableCellCount
            print("completeFunction")
            print("\(totalTableCellCount) , \(self.ii)")
            if totalTableCellCount == self.ii {

                // let dict = self.googlereponseDirection

                var StartTripTime = ""
                var EndTripTime = ""
                self.googlereponseDirection.sort(by: { ($0["Index"] as! String) < $1["Index"] as! String })
                //dump(googlereponseDirection)
                (parent as? TripMapViewController)?.zoom(to: "1")
                print("************$*********")
                print(googlereponseDirection)
                TempgooglereponseDirection = googlereponseDirection
                print("&*&*&*&*&*&*&*")
                // TotalMinTripForTravel

                let tripMinTotal = convertTime(miliseconds: TotalMinTripForTravel * 1000)

                if googlereponseDirection.first!["ModeOFTransit"] as? String == "TRANSIT" {

                    StartTripTime = (googlereponseDirection.first!["TransitDepartTime"] as? String)!

                }
                else if googlereponseDirection.first!["ModeOFTransit"] as? String == "Walking" {
                    //Timing
                    StartTripTime = (googlereponseDirection.first!["Timing"] as? String)!

                }
                else if googlereponseDirection.first!["ModeOFTransit"] as? String == "BikeShare" {

                    StartTripTime = (googlereponseDirection.first!["Timing"] as? String)!

                }
                else if googlereponseDirection.first!["ModeOFTransit"] as? String == "RideShare" {

                    StartTripTime = (googlereponseDirection.first!["Timing"] as? String)!

                }


                if googlereponseDirection.last!["ModeOFTransit"] as? String == "TRANSIT" {

                    EndTripTime = (googlereponseDirection.last!["TransitDepartTime"] as? String)!

                }
                else if googlereponseDirection.last!["ModeOFTransit"] as? String == "Walking" {

                    EndTripTime = (googlereponseDirection.last!["Timing"] as? String)!
                }
                else if googlereponseDirection.last!["ModeOFTransit"] as? String == "BikeShare" {

                    EndTripTime = (googlereponseDirection.last!["Timing"] as? String)!
                }
                else if googlereponseDirection.last!["ModeOFTransit"] as? String == "RideShare" {
                    EndTripTime = (googlereponseDirection.last!["Timing"] as? String)!

                }

                TripTravlingTime.text = "\(StartTripTime) - \(EndTripTime)"

                if googlereponseDirection.last!["ModeOFTransit"] as? String == "RideShare" && googlereponseDirection.first!["ModeOFTransit"] as? String == "RideShare" {
                    // googlereponseDirection.removeAll()

                    //googlereponseDirection = TempgooglereponseDirection

                }
                TotalMinTrip.text = tripMinTotal
                RegularTransitFare = Double(TransitRegularCount.count) * 1.75
                ExpressTransitFare = Double(TransitExpressCount.count) * 2.75
                var MyFareforTrip = 0.0
                if TransitRegularCount.count > 0 && TransitExpressCount.count > 0 {
                    MyFareforTrip = 1.50

                }
                else if TransitRegularCount.count == 0 && TransitExpressCount.count > 0 {
                    MyFareforTrip = 1.50
                }
                else if TransitRegularCount.count > 0 && TransitExpressCount.count == 0 {
                    MyFareforTrip = 1.50
                }

                let FareforTrip = (RegularTransitFare) + (ExpressTransitFare)
                let totalFare = String(format: "$%.2f", FareforTrip)

                TransitPrice.text = totalFare
                if UIDevice.current.userInterfaceIdiom == .pad
                {
                   transitfareregularipad.text = totalFare
                }
                // let mytarcTotal =
                MyTarcTransitPrice.text = String(format: "$%.2f", MyFareforTrip)
                if UIDevice.current.userInterfaceIdiom == .pad
                {
                    transitfaremytracipad.text = String(format: "$%.2f", MyFareforTrip)

                }
//                if BikeRideShare {
//
//                TypeOfShare.text = "       "
//
//                }
//                else if RideShareBool {
//
//                TypeOfShare.text = "        "
//
//
//                }
//                else {
//                    TypeOfShare.isHidden = true
//                    RideBikeSharePrice.isHidden = true
//                    MyTarcRideBikeShare.isHidden = true
//
//                }

                let totalBike = TotalBikeShare.rounded(.toNearestOrAwayFromZero)
                let totalPriceRegular = FareforTrip + totalBike
                let MyTarctotalPriceRegular = MyFareforTrip + totalBike


                TotalRegularFare.text = String(format: "$%.2f", totalPriceRegular)

                TotalMytarcFare.text = String(format: "$%.2f", MyTarctotalPriceRegular)
                if UIDevice.current.userInterfaceIdiom == .pad
                {
                    totalmytarcfareipad.text = String(format: "$%.2f", MyTarctotalPriceRegular)
                    totalregularfareipad.text = String(format: "$%.2f", totalPriceRegular)
                }
                self.hideLoader()
                print(RealtimeData)
                //googlereponseDirection.removeAll()
                if RealTimeData == nil {
                    RealTimeData = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.RealtimePanelRefresh), userInfo: nil, repeats: true)
                    //Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(downloadTimer), userInfo: nil, repeats: true)
                }
                
              //  googlereponseDirection
                self.tableView.reloadData()

            }


        }
    override func viewWillDisappear(_ animated: Bool) {
        if RealTimeData != nil {
            RealTimeData = nil
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        if RealTimeData != nil {
            RealTimeData = nil
        }
    }
        @objc func RealtimePanelRefresh() {

            let RealtimeDataFromTimer = RealtimeData
            if RealtimeDataFromTimer.count > 0 {
                for realtimedataList in RealtimeDataFromTimer {
                    //realtimeDataResponse = transState as! DataResponse<TransitRouteClass>
                    let url2 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealTimeVehicleandNextStops"
                    let parameters2: Parameters = [
                        "TripID": (realtimedataList["TripID"] as? String)!,
                        "ArrivalTime": (realtimedataList["ArrivalTime"] as? String)!,
                        "DepartureTime": (realtimedataList["DepartureTime"] as? String)!,
                        "DepartureStopName": (realtimedataList["DepartureStopName"] as? String)!,
                        "ArrivalStopName": (realtimedataList["ArrivalStopName"] as? String)!

                    ]


                    Alamofire.request(url2, method: .get, parameters: parameters2, encoding: URLEncoding.default)
                        .responseObject { (response: DataResponse<RealTimeRootClass>) in
                            switch response.result {
                            case .success:
                                if response.result.value != nil {
                                    if response.result.value?.eTAminutes != nil {
                                        if (response.result.value?.vehicle)! && response.result.value?.eTAminutes != "passed" {


                                            let findIndex = (realtimedataList["IndexRow"] as? Int)!
                                            /// cell.TransitOntimeStatus.text = (realtimedataList["TripID"] as? String)!//
                                            let ETA = "\((response.result.value?.eTAminutes)!)" as NSString

                                            let TransitStatus = "\((response.result.value?.tripStatus)!)" as NSString

                                            var NextStopAddress = ""
                                            if response.result.value?.busStops?.nextStops?.count != 0 {
                                                NextStopAddress = "Next Stop: \((response.result.value?.busStops?.nextStops![0].stopName)!)"
                                            }
                                            // let NextStopAddress = "\((response.result.value!.busStops?.nextStops![0].stopName)!)"
                                            let Realtime = "\(response.result.value!.vehicle!)"
//
//                                let ETA = "\(findIndex)"
//                                let TransitStatus = "On Time"
//                                let NextStopAddress = "Louisvello"
//                                let Realtime = "true"
//


                                            self.googlereponseDirection[findIndex]["ETA"] = "\(ETA)"
                                            self.googlereponseDirection[findIndex]["TripStatus"] = TransitStatus
                                            self.googlereponseDirection[findIndex]["NextStop"] = NextStopAddress
                                            self.googlereponseDirection[findIndex]["RealTime"] = "\(Realtime)"

                                            let indexPath = NSIndexPath(row: findIndex, section: 0)
                                            self.tableView.reloadRows(at: [(indexPath as IndexPath)], with: UITableView.RowAnimation.none)



                                            //  print("\((response.result.value?.routeShortName)!) - Realtime")
                                        } else {
                                            print("No Realtime Bus trippanel 1")
                                        }

                                    } else {
                                        print("No Realtime bus trippanel 2")
                                    }
                                }
                                else {
                                    print("No Realtime bus trippanel 3")
                                }
                            case .failure(let error):

                                print(error)
                            }
                    }


                }

            }
        }
        func getDirectionFromGoogle() {


            BuyTicketBtn.setTitle("Buy Ticket", for: .normal)
            if UIDevice.current.userInterfaceIdiom == .pad
            {
                buyButtonIpad.setTitle("Buy Ticket", for: .normal)

            }
            BikeShareCount = 0
            RideShareCount = 0
            RideShareCountNeg = 0
            BikeShareCountNeg = 0
            googlereponseDirection.removeAll()
            let valueResult = dataPrevRespone.result.value
            let routesPlan = valueResult?.routes![indexValue].legs
            let stepValueArray = routesPlan![0].steps!
            // print(Any)
            for (index, stepValue) in stepValueArray.enumerated() {



                DirectionCollect(Passingdata: stepValue, index: index, completion: { (success, content) in

                    print(success)

                    print(content!)
                    // print(self.googlereponseDirection)

                    self.completeFunction(ArrayCount: "\(success)")
                    // self.googlereponseDirection.add(content!)
                    print(self.googlereponseDirection)

                })




            }
        }
        func BikeRideShareCompletedCheck(BikeShareArrayList: [[String: Any]], Type: String, completion: @escaping (Bool, String, String, String, String, Int, Double, Double, Double, Double, Double, Double, Double, Double, String) -> Void) {

            var totalApiFaild = 0
            print("BikeRideShareCompletedCheck Bird - \((SourceDestination.birdListApi?.count)!)")
            print("BikeRideShareCompletedCheck Lime- \(SourceDestination.LimeListApi?.count)")
            print("BikeRideShareCompletedCheck Bolt- \(SourceDestination.boltListApi?.count)")
            dump(SourceDestination.boltListApi)
            if (SourceDestination.birdListApi!.count != 0 && SourceDestination.boltListApi!.count != 0 && SourceDestination.LimeListApi!.count != 0) {

                totalApiFaild = 3
            }


            else if (SourceDestination.birdListApi!.count != 0 && SourceDestination.boltListApi!.count != 0 && SourceDestination.LimeListApi!.count == 0) {

                totalApiFaild = 2
            }
            else if (SourceDestination.birdListApi!.count != 0 && SourceDestination.boltListApi!.count == 0 && SourceDestination.LimeListApi!.count != 0) {

                totalApiFaild = 2
            }



            else if (SourceDestination.birdListApi!.count == 0 && SourceDestination.boltListApi!.count != 0 && SourceDestination.LimeListApi!.count != 0) {

                totalApiFaild = 2
            }



            else if (SourceDestination.birdListApi!.count == 0 && SourceDestination.boltListApi!.count != 0 && SourceDestination.LimeListApi!.count == 0) {

                totalApiFaild = 1
            }
            else if (SourceDestination.birdListApi!.count == 0 && SourceDestination.boltListApi!.count == 0 && SourceDestination.LimeListApi!.count != 0) {

                totalApiFaild = 1
            }
            else if (SourceDestination.birdListApi!.count != 0 && SourceDestination.boltListApi!.count == 0 && SourceDestination.LimeListApi!.count == 0) {

                totalApiFaild = 1
            }



            else {

                totalApiFaild = 0

            }


            // TotalBikeShare = TotalBikeShare + (BikeShareArrayList[0]["vehiclefare"] as? Double)!

            let firstBikeShareAddress = (BikeShareArrayList[0]["WalkingInstructions"] as? String)!
            let SecondDescription = (BikeShareArrayList[0]["WalkingAboutandCaloties"] as? String)!
            let WaitTimeWalking = (BikeShareArrayList[0]["WaitingTime"] as? String)!
                 let RouteColor = (BikeShareArrayList[0]["vehicleColor"] as? String)!
            let waitStartTime = (BikeShareArrayList[0]["WakingStartTime"] as? String)!
            let BikeShareTime = BikeShareArrayList[0]["TotalTime"] as? Int
            let SourceWalkingLat = (BikeShareArrayList[0]["WalkingSourceLat"] as? Double)!
            let SourceWalkingLng = (BikeShareArrayList[0]["WalkingSourceLng"] as? Double)!
            let DestinationWalkingLat = (BikeShareArrayList[0]["WalkingDestinationLat"] as? Double)!
            let DestinationWalkingLng = (BikeShareArrayList[0]["WalkingDestinationLng"] as? Double)!
            let SourceBikeLat = (BikeShareArrayList[0]["BikeShareSourceLat"] as? Double)!
            let SourceBikeLng = (BikeShareArrayList[0]["BikeShareSourceLng"] as? Double)!
            let DestinationBikeLat = (BikeShareArrayList[0]["BikeShareDestinationLat"] as? Double)!
            let DestinationBikeLng = (BikeShareArrayList[0]["BikeShareDestinationLng"] as? Double)!
            self.TotalMinTripForTravel = self.TotalMinTripForTravel + BikeShareTime!
            let TimingForBikeShare = (BikeShareArrayList[0]["vehicleTime"] as? String)!
            print("\(BikeShareCount) BikeShareCount - \(Type)")
            print("\(RideShareCount) RideShareCount - \(Type)")
            print("**************\(Type)")
            print("\(BikeShareCount) == \(totalApiFaild)")
            print("**************\(Type)")

            if Type == "Bike" && BikeShareCount >= totalApiFaild {
                var BirdFare = [Double]()
                
                
                (parent as? TripMapViewController)?.DrawPolyline(SourceLat: SourceBikeLat, SourceLng: SourceBikeLng, DestinationLat: DestinationBikeLat, DestinationLng: DestinationBikeLng, Type: "BikeShare", TripColor: RouteColor)
                for Count in 0...BikeShareArrayList.count - 1 {
                    let bikeshareprice = (BikeShareArrayList[Count]["vehiclefare"] as? Double)!
                    // print(bikeshareprice)
                    BirdFare.append(bikeshareprice)
                }
                // BirdFare.sort(){$0 < $1}
                print(BirdFare)
                TotalBikeShare = TotalBikeShare + BirdFare[0]

                // for BikeShareArrayList
                print(BikeRideShare)
                               if BikeRideShare {
                                    if BirdFare.count > 0 {
                                       // TypeOfShare.isHidden = false
                                        let totalvalue = TotalBikeShare.rounded(.toNearestOrAwayFromZero)
                                    TypeOfShare.text = "Bikeshare:"
                                    RideBikeSharePrice.text = String(format: "$%.2f", totalvalue)
                                    MyTarcRideBikeShare.text = String(format: "$%.2f", totalvalue)
                                        if UIDevice.current.userInterfaceIdiom == .pad
                                        {
                                            secondLabelnew.text = "Bikeshare:"
                                            secondregularfare.text = String(format: "$%.2f", totalvalue)
                                           secondmytarcfare.text = String(format: "$%.2f", totalvalue)
                                        }
                                        
                                    }
                //
                //                    TypeOfShare.text = ""
                //                                       RideBikeSharePrice.text = ""
                //                                       MyTarcRideBikeShare.text = ""
                              }
           
                else {
                   // TypeOfShare.isHidden = true
                    RideBikeSharePrice.isHidden = true
                    MyTarcRideBikeShare.isHidden = true
                    if UIDevice.current.userInterfaceIdiom == .pad
                    {
                        secondLabelnew.text = ""
                        secondregularfare.isHidden = true
                       secondmytarcfare.isHidden = true
                    }

                }








                completion(true, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime!, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare)

            }
            else if Type == "Cab" && RideShareCount >= 2 {
                var BirdFare = [Double]()
                for Count in 0...BikeShareArrayList.count - 1 {
                    let bikeshareprice = (BikeShareArrayList[Count]["vehiclefare"] as? Double)!
                    // print(bikeshareprice)
                    BirdFare.append(bikeshareprice)
                }
                // BirdFare.sort(){$0 < $1}
                print(BirdFare)
                TotalBikeShare = TotalBikeShare + BirdFare[0]

                // for BikeShareArrayList
                print(BikeRideShare)
                if RideShareBool {
                    if BirdFare.count > 0 {
                    TypeOfShare.text = "Rideshare"
                    RideBikeSharePrice.text = String(format: "$%.2f", TotalBikeShare)
                    MyTarcRideBikeShare.text = String(format: "$%.2f", TotalBikeShare)
                        
                        if UIDevice.current.userInterfaceIdiom == .pad
                        {
                            secondLabelnew.text = "Rideshare"
                            secondregularfare.text = String(format: "$%.2f", TotalBikeShare)
                           secondmytarcfare.text = String(format: "$%.2f", TotalBikeShare)
                        }

                    }
                    else{
                        //TypeOfShare.text = ""
                        RideBikeSharePrice.text = ""
                        MyTarcRideBikeShare.text = ""
                        if UIDevice.current.userInterfaceIdiom == .pad
                        {
                        secondregularfare.text = ""
                        secondmytarcfare.text = ""
                        }
                    }
                }
                    
                else {
                   // TypeOfShare.isHidden = true
                    RideBikeSharePrice.isHidden = true
                    MyTarcRideBikeShare.isHidden = true
                    if UIDevice.current.userInterfaceIdiom == .pad
                    {
                    secondregularfare.isHidden = true
                    secondmytarcfare.isHidden = true
                    }

                }

                completion(true, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime!, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare)

            }
            else {

                completion(false, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime!, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare)
            }



        }
    func getalert(Route:String) -> Bool {
    
    
        
        
    return true
    }
        func DirectionCollect(Passingdata: SuggestStep, index: Int, completion: @escaping (Bool, Any?) -> Void) {
            BikeShareCount = 0
            RideShareCount = 0
            RideShareCountNeg = 0
            BikeShareCountNeg = 0
            var routeNoColor = ""
            let valueResult = dataPrevRespone.result.value
            let routesPlan = valueResult?.routes![indexValue].legs
//String(format: "$%.2f", fareValue)
            if Passingdata.travelMode == "TRANSIT"
                {
                let polylineString = Passingdata.polyline!.points
                    var TarcRouteNo = ""
                    if Passingdata.transitDetails?.line?.shortName != nil {
                                                           TarcRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                                                       }
                                                       else {
                                                           TarcRouteNo = "\((Passingdata.transitDetails?.line?.name)!)"
                                                       }
                    
                    
                    if (TarcRouteNo.contains("X")) {
                    TransitExpressCount.append("Transit")
                }
                else
                {
                    TransitRegularCount.append("Transit")
                }
                    
                var CO2Emmision = (Double(Passingdata.distance!.value!) * 0.0000419426)
                let test = String(format: "%.2f", CO2Emmision)
                let textcount = test.count
                print(CO2Emmision)
                let totalcount = textcount + 7
                CO2Emmision = getAmountRounded(CO2Emmision, withRoundOff: Int(5))
                let font: UIFont? = UIFont(name: "Helvetica", size: 12)
                let fontSuper: UIFont? = UIFont(name: "Helvetica", size: 8)
                let Transitco2: NSMutableAttributedString = NSMutableAttributedString(string: "\(test) lbs CO2", attributes: [.font: font!])
                Transitco2.setAttributes([.font: fontSuper!, .baselineOffset: -8], range: NSRange(location: totalcount, length: 1))

                // cell.dashboardTitle.attributedText = attString
                print("Isaaaaaaa start \(Date())")
                //  print(Transitco2)
                //  Transitco2 = "\(Passingdata.distance?.text) \(Transitco2)"
                //print("isaacraja")
                TotalMinTripForTravel = TotalMinTripForTravel + Passingdata.duration!.value!
                let url1 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetTripSchedulemobileapi"

                let parameters1: Parameters = [
                    "Headsign": Passingdata.transitDetails?.headsign ?? "nil",
                    "Routeid": Passingdata.transitDetails?.line?.shortName ?? "nil",
                    "StartStop": Passingdata.transitDetails?.departureStop?.name ?? "nil",
                    "EndStop": Passingdata.transitDetails?.arrivalStop?.name ?? "nil",
                    "DepartureTime": Passingdata.transitDetails?.departureTime?.text ?? "nil",
                    "StartStopLat": Passingdata.transitDetails?.departureStop?.location?.lat ?? "nil",
                    "StartStopLong": Passingdata.transitDetails?.departureStop?.location?.lng ?? "nil",
                    "EndStopLat": Passingdata.transitDetails?.arrivalStop?.location?.lat ?? "nil",
                    "EndStopLong": Passingdata.transitDetails?.arrivalStop?.location?.lng ?? "nil",
                    //"Day": "Friday"
                ]
                print("\(APIUrl.ZIGTARCAPI)Trip/GetTripSchedulemobileapi?Headsign=\((Passingdata.transitDetails?.headsign)!)&Routeid=\((Passingdata.transitDetails?.line?.shortName)!)&StartStop=\((Passingdata.transitDetails?.departureStop?.name)!)&EndStop=\((Passingdata.transitDetails?.arrivalStop?.name)!)&DepartureTime=\((Passingdata.transitDetails?.departureTime?.text)!)&StartStopLat=\((Passingdata.transitDetails?.departureStop?.location?.lat)!)&StartStopLong=\((Passingdata.transitDetails?.departureStop?.location?.lng)!)&EndStopLat=\((Passingdata.transitDetails?.arrivalStop?.location?.lat)!)&EndStopLong=\((Passingdata.transitDetails?.arrivalStop?.location?.lng)!)")

                Alamofire.request(url1, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                    .responseObject { (response: DataResponse<TransitRouteClass>) in
                        switch response.result {
                        case .success:
                            // if response.result.value.

                            self.RealTimeTransitOptions.add(response)
                            print("Isaaaaaaa 1 \(Date())")
                            print(response.result.value?.triplist as Any)
                            print("Isaaaaaaa 1 \(Date())")
                            if response.result.value?.triplist != nil {
                                print("Isaaaaaaa 1 \(Date())")
                                if !(response.result.value?.triplist?.isEmpty)!
                                    {
                                    print("Isaaaaaaa 1 \(Date())")
                                    let googlereponseDirectionList: NSMutableDictionary = [:]
                                    print("Isaaaaaaa 1 \(Date())")
                                    var waitingTravel = ""
                                    print("Isaaaaaaa 1 \(Date())")
                                    self.realtimeDataResponse = response
                                    print("Isaaaaaaa 1 \(Date())")

                                    let DepartureStop = (self.realtimeDataResponse.result.value?.triplist![0].departureStop)!
                                    print("Isaaaaaaa 1 \(Date())")
                                    let DepartTimeRealTime = (self.realtimeDataResponse.result.value?.triplist![0].departureTime)!
                                    print("Isaaaaaaa 1 \(Date())")

                                    let ArrivalStopRealtime = (self.realtimeDataResponse.result.value?.triplist![0].arrivalStop)!
                                    print("Isaaaaaaa 1 \(Date())")
                                    let ArrivalTimeRealTime = (self.realtimeDataResponse.result.value?.triplist![0].arrivalTime)!
                                    print("Isaaaaaaa 1 \(Date())")

                                    let arrival_time = (Passingdata.transitDetails?.arrivalTime?.text!)!
                                    print("Isaaaaaaa 1 \(Date())")
                                    let departure_time = (Passingdata.transitDetails?.departureTime?.text!)!
                                    print("Isaaaaaaa 1 \(Date())")

                                    let departdateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.departureTime?.value!)!)
                                    print("Isaaaaaaa 1 \(Date())")
                                    let arivalDateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.arrivalTime?.value!)!)
                                    print(departdateNew)
                                    print("Isaaaaaaa 1 \(Date())")

                                    var TransitArrivalTime = "\(self.DateFormatcheck(Data24Hr: ArrivalTimeRealTime))"
                                    print("Isaaaaaaa 1 \(Date())")
                                    var TransitDepartTime = "\(self.DateFormatcheck(Data24Hr: DepartTimeRealTime))"
                                    print("Isaaaaaaa 1 \(Date())")



                                    let FirstAddressGoogle = Passingdata.htmlInstructions!
                                    print("Isaaaaaaa 1 \(Date())")
                                    var SecondDescription = "\(DepartureStop) - (\(departdateNew)) "
                                    print("Isaaaaaaa 1 \(Date())")
                                    var ArrivalAddress = "\(ArrivalStopRealtime) - (\(arivalDateNew))"
                                    print("Isaaaaaaa 1 \(Date())")
                                    let TransitRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                                    print("Isaaaaaaa 1 \(Date())")
                                    let TransitTime = "\(departure_time) - \(arrival_time)"
                                    print("Isaaaaaaa 1 \(Date())")
                                    print(SecondDescription)
                                    print("Isaaaaaaa 1 \(Date())")

                                    if index > 1 {
                                        if index == 0 {

                                            if Passingdata.travelMode == routesPlan![0].steps![index + 1].travelMode {

                                                let Firstbus = (routesPlan![0].steps![index].transitDetails?.arrivalTime?.value)!
                                                let secondBus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                let waitingtimeResult = self.datasubraction(arrivalDate: Firstbus, DepartDate: secondBus, Walkingmin: 0)
                                                waitingTravel = "Wait time \(waitingtimeResult)"
                                            }


                                        } else {
                                            if Passingdata.travelMode == routesPlan![0].steps![index - 1].travelMode {

                                                let Firstbus = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                let secondBus = (routesPlan![0].steps![index].transitDetails?.departureTime?.value)!
                                                let waitingtimeResult = self.datasubraction(arrivalDate: Firstbus, DepartDate: secondBus, Walkingmin: 0)
                                                waitingTravel = "Wait time \(waitingtimeResult)"
                                            }
                                        }

                                    } else {
                                        waitingTravel = ""
                                    }
                                    print("Isaaaaaaa 1 \(Date())")


                                    if self.realtimeDataResponse.result.value?.triplist![0].routeColor != nil {
                                        routeNoColor = "#\((self.realtimeDataResponse.result.value?.triplist![0].routeColor)!)"

                                    }
                                    else {
                                        if Passingdata.transitDetails?.line?.color != nil {
                                            routeNoColor = (Passingdata.transitDetails?.line?.color)!
                                        } else {
                                            routeNoColor = "#000000"
                                        }

                                    }


                                    let RealtimeDataList: NSMutableDictionary = [:]
                                    RealtimeDataList.setValue("\((self.realtimeDataResponse.result.value?.triplist![0].tripID)!)", forKey: "TripID")
                                    RealtimeDataList.setValue("\((self.realtimeDataResponse.result.value?.triplist![0].arrivalTime)!)", forKey: "ArrivalTime")
                                    RealtimeDataList.setValue("\((self.realtimeDataResponse.result.value?.triplist![0].departureTime)!)", forKey: "DepartureTime")
                                    RealtimeDataList.setValue("\((self.realtimeDataResponse.result.value?.triplist![0].departureStop)!)", forKey: "DepartureStopName")
                                    RealtimeDataList.setValue("\((self.realtimeDataResponse.result.value?.triplist![0].arrivalStop)!)", forKey: "ArrivalStopName")

                                    RealtimeDataList.setValue(index, forKey: "IndexRow")

                                    self.RealtimeData.append(RealtimeDataList as! [String: Any])
//                                    RealtimePanelRefresh(TripID:self.realtimeDataResponse.result.value?.triplist, ArrivalTime:self.realtimeDataResponse.result.value?.triplist![0].arrivalTime, DepartureTime: self.realtimeDataResponse.result.value?.triplist![0].departureTime, DepartureStopName: self.realtimeDataResponse.result.value?.triplist![0].departureStop, ArrivalStopName: self.realtimeDataResponse.result.value?.triplist![0].arrivalStop, CellRow: index )
//

                                    let url2 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealTimeVehicleandNextStops"
                                    let parameters2: Parameters = [
                                        "TripID": self.realtimeDataResponse.result.value?.triplist![0].tripID ?? "nil",
                                        "ArrivalTime": self.realtimeDataResponse.result.value?.triplist![0].arrivalTime ?? "nil",
                                        "DepartureTime": self.realtimeDataResponse.result.value?.triplist![0].departureTime ?? "nil",
                                        "DepartureStopName": self.realtimeDataResponse.result.value?.triplist![0].departureStop ?? "nil",
                                        "ArrivalStopName": self.realtimeDataResponse.result.value?.triplist![0].arrivalStop ?? "nil",

                                    ]
                                    print("Isaaaaaa 2\(Passingdata.transitDetails?.line?.shortName!) \(Date())")

                                    Alamofire.request(url2, method: .get, parameters: parameters2, encoding: URLEncoding.default)
                                        .responseObject { (response: DataResponse<RealTimeRootClass>) in
                                            switch response.result {
                                            case .success:

                                                if response.result.value != nil && response.result.value?.vehicle != nil
                                                    {
                                                    let travelingTime = Passingdata.distance?.value
                                                    let WalkingTimeArrival = Passingdata.transitDetails?.arrivalTime?.value

                                                    if (response.result.value?.vehicle)! && response.result.value?.eTAminutes != "passed" {

                                                        let ETAMin = response.result.value?.eTAminutes




                                                        let trippstatus = (response.result.value?.tripStatus)!
                                                        var nextBusstopRealtime = ""
                                                        if response.result.value?.busStops?.nextStops?.count != 0 {
                                                            nextBusstopRealtime = "Next Stop: \((response.result.value?.busStops?.nextStops![0].stopName)!)"
                                                        }
                                                        print(Passingdata.transitDetails?.line?.shortName!)

                                                        if response.result.value?.departureTime != nil {

                                                            // TransitArrivalTime = "\((response.result.value?.arrivalTime)!)"
                                                            TransitDepartTime = "\((response.result.value?.departureTime)!)"

                                                            SecondDescription = "\(DepartureStop) - (\(departdateNew))"

                                                        }
                                                        if response.result.value?.arrivalTime != nil {
                                                            TransitArrivalTime = "\(self.DateFormatcheck(Data24Hr: ArrivalTimeRealTime))"
                                                            ArrivalAddress = "\(ArrivalStopRealtime) - (\(arivalDateNew))"
                                                        }
                                                        googlereponseDirectionList.setValue("\((polylineString)!)", forKey: "polylineString")
                                                        //var polylineString
                                                        googlereponseDirectionList.setValue(ETAMin, forKey: "ETA")
                                                        googlereponseDirectionList.setValue(TransitArrivalTime, forKey: "TransitArrivalTime")
                                                        googlereponseDirectionList.setValue(TransitDepartTime, forKey: "TransitDepartTime")

                                                        googlereponseDirectionList.setValue(Transitco2, forKey: "TransitCo2Cal")

                                                        googlereponseDirectionList.setValue("true", forKey: "RealTime")
                                                        googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")

                                                        googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                        googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                                        googlereponseDirectionList.setValue(trippstatus, forKey: "TripStatus")
                                                        googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                                        googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                        googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                        googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                        googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                                        googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                                        googlereponseDirectionList.setValue(nextBusstopRealtime, forKey: "NextStop")
                                                        googlereponseDirectionList.setValue(travelingTime, forKey: "TravelingTime")
                                                        googlereponseDirectionList.setValue(WalkingTimeArrival, forKey: "WalkingCalArrival")
                                                        googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")
                                                        googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                        print("Isaaaaaa 3 \(Passingdata.transitDetails?.line?.shortName!) \(Date())")
                                                        print("Before Added \(self.ii)")
                                                        self.ii = self.ii + 1
                                                        print("After Added \(self.ii)")





                                                        if AnalyticsVar.ActivityID != nil {
                                                            //                                                        let tripID = Int((self.realtimeDataResponse.result.value?.triplist![0].tripID)!)
                                                            //                                                let Activity_Type = AnalyticsVar.ActivityID
                                                            //
                                                            //                                let ETA_arrival_time = response.result.value?.arrivalTime
                                                            //
                                                            //                                let ETA_depature_time = response.result.value?.departureTime
                                                            //                                let Delay = trippstatus
                                                            //                                let stop_id = response.result.value?.stopID
                                                            //                                let stop_name = ArrivalAddress
                                                            //
                                                            //            analytics.GetTransitRunningStatus(trip_id: tripID!, ETA_arrival_time: ETA_arrival_time!, ETA_depature_time: ETA_depature_time!, Delay: trippstatus, stop_id: stop_id!, stop_name: stop_name, Activityid: Activity_Type!)


                                                        }

                                                        completion(true, googlereponseDirectionList)









                                                    }

                                                    else {
                                                        print("Isaaaaaa 3 \(Passingdata.transitDetails?.line?.shortName!) \(Date())")
                                                        googlereponseDirectionList.setValue("", forKey: "ETA")
                                                        googlereponseDirectionList.setValue(TransitArrivalTime, forKey: "TransitArrivalTime")
                                                        googlereponseDirectionList.setValue(TransitDepartTime, forKey: "TransitDepartTime")
                                                        googlereponseDirectionList.setValue(Transitco2, forKey: "TransitCo2Cal")

                                                        googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                        googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")
                                                        googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                        googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                        googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                                        googlereponseDirectionList.setValue("\((polylineString)!)", forKey: "polylineString")
                                                        googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                                        googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                        googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                        googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                        googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                                        googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                                        googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                        googlereponseDirectionList.setValue(travelingTime, forKey: "TravelingTime")
                                                        googlereponseDirectionList.setValue(WalkingTimeArrival, forKey: "WalkingCalArrival")
                                                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")
                                                        googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                                                        self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                        print("Before Added \(self.ii)")
                                                        self.ii = self.ii + 1
                                                        print("After Added \(self.ii)")
                                                        completion(false, googlereponseDirectionList)
                                                    }
                                                }
                                                else {
                                                    print("Isaaaaaa 4 \(Passingdata.transitDetails?.line?.shortName!) \(Date())")
                                                    googlereponseDirectionList.setValue(TransitArrivalTime, forKey: "TransitArrivalTime")
                                                    googlereponseDirectionList.setValue(TransitDepartTime, forKey: "TransitDepartTime")
                                                    googlereponseDirectionList.setValue(Transitco2, forKey: "TransitCo2Cal")

                                                    googlereponseDirectionList.setValue("falses", forKey: "RealTime")
                                                    googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")
                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                    googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                                    googlereponseDirectionList.setValue("\((polylineString)!)", forKey: "polylineString")

                                                    googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                    googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                                    googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                    googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                    googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                    print("Before Added \(self.ii)")
                                                    self.ii = self.ii + 1
                                                    print("After Added \(self.ii)")
                                                    completion(false, googlereponseDirectionList)

                                                }


                                            case .failure(let error):

                                                print(error)
                                                googlereponseDirectionList.setValue(TransitArrivalTime, forKey: "TransitArrivalTime")
                                                googlereponseDirectionList.setValue(TransitDepartTime, forKey: "TransitDepartTime")
                                                googlereponseDirectionList.setValue(Transitco2, forKey: "TransitCo2Cal")

                                                googlereponseDirectionList.setValue("falses", forKey: "RealTime")
                                                googlereponseDirectionList.setValue("\(routeNoColor)", forKey: "RouteLineColor")
                                                googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                                googlereponseDirectionList.setValue("\((polylineString)!)", forKey: "polylineString")

                                                googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                                googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                                googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                                                googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                print("Before Added \(self.ii)")
                                                self.ii = self.ii + 1
                                                print("After Added \(self.ii)")
                                                completion(false, googlereponseDirectionList)



                                            }

                                    }

                                }
                                else {
                                    let departdateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.departureTime?.value!)!)
                                    let arivalDateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.arrivalTime?.value!)!)
                                    let googlereponseDirectionList: NSMutableDictionary = [:]

                                    let arrival_time = (Passingdata.transitDetails?.arrivalTime?.text!)!
                                    let departure_time = (Passingdata.transitDetails?.departureTime?.text!)!
                                    var google_color = ""

                                    let FirstAddressGoogle = Passingdata.htmlInstructions!

                                    let SecondDescription = "\((Passingdata.transitDetails?.departureStop?.name)!) - (\(departdateNew))"

                                    let ArrivalAddress = "\((Passingdata.transitDetails?.arrivalStop!.name)!) - (\(arivalDateNew))"


                                    let TransitArrivalTime = "\((Passingdata.transitDetails?.arrivalTime?.text)!)"
                                    let TransitDepartTime = "\((Passingdata.transitDetails?.departureTime?.text)!) "



                                    var TransitRouteNo = ""
                                    if Passingdata.transitDetails?.line?.shortName != nil {
                                        TransitRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                                    }
                                    else {
                                        TransitRouteNo = "\((Passingdata.transitDetails?.line?.name)!)"
                                    }
                                    let TransitTime = "\(departure_time) - \(arrival_time)"
                                    print(SecondDescription)

                                    if Passingdata.transitDetails!.line?.color != nil {
                                        google_color = (Passingdata.transitDetails!.line?.color)!
                                    }
                                    else {
                                        google_color = "\(color.hexStringToUIColor(hex: "#000000"))"

                                    }
                                    googlereponseDirectionList.setValue(TransitArrivalTime, forKey: "TransitArrivalTime")
                                    googlereponseDirectionList.setValue(TransitDepartTime, forKey: "TransitDepartTime")
                                    googlereponseDirectionList.setValue(Transitco2, forKey: "TransitCo2Cal")

                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                    googlereponseDirectionList.setValue("\(google_color)", forKey: "RouteLineColor")
                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                    googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                    googlereponseDirectionList.setValue("\((polylineString)!)", forKey: "polylineString")

                                    googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                    googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                    googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                    googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                    googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                    print("Before Added \(self.ii)")
                                    self.ii = self.ii + 1
                                    print("After Added \(self.ii)")
                                    completion(false, googlereponseDirectionList)







                                }
                            }
                            else {
                                let departdateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.departureTime?.value!)!)
                                let arivalDateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.arrivalTime?.value!)!)
                                let googlereponseDirectionList: NSMutableDictionary = [:]

                                let arrival_time = (Passingdata.transitDetails?.arrivalTime?.text!)!
                                let departure_time = (Passingdata.transitDetails?.departureTime?.text!)!
                                var google_color = ""

                                let FirstAddressGoogle = Passingdata.htmlInstructions!

                                let SecondDescription = "\((Passingdata.transitDetails?.departureStop?.name)!) - (\(departdateNew))"

                                let ArrivalAddress = "\((Passingdata.transitDetails?.arrivalStop!.name)!) - (\(arivalDateNew))"


                                let TransitArrivalTime = "\((Passingdata.transitDetails?.arrivalTime?.text)!)"
                                let TransitDepartTime = "\((Passingdata.transitDetails?.departureTime?.text)!) "



                                var TransitRouteNo = ""
                                if Passingdata.transitDetails?.line?.shortName != nil {
                                    TransitRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                                }
                                else {
                                    TransitRouteNo = "\((Passingdata.transitDetails?.line?.name)!)"
                                }
                                let TransitTime = "\(departure_time) - \(arrival_time)"
                                print(SecondDescription)

                                if Passingdata.transitDetails!.line?.color != nil {
                                    google_color = (Passingdata.transitDetails!.line?.color)!
                                }
                                else {
                                    google_color = "\(color.hexStringToUIColor(hex: "#000000"))"

                                }
                                googlereponseDirectionList.setValue(TransitArrivalTime, forKey: "TransitArrivalTime")
                                googlereponseDirectionList.setValue(TransitDepartTime, forKey: "TransitDepartTime")
                                googlereponseDirectionList.setValue(Transitco2, forKey: "TransitCo2Cal")

                                googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                googlereponseDirectionList.setValue("\(google_color)", forKey: "RouteLineColor")
                                googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                                googlereponseDirectionList.setValue("\((polylineString)!)", forKey: "polylineString")

                                googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                                googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                                googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                                googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                                self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                print("Before Added \(self.ii)")
                                self.ii = self.ii + 1
                                print("After Added \(self.ii)")
                                completion(false, googlereponseDirectionList)
                            }

                        case .failure(let error):
                            let departdateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.departureTime?.value!)!)
                            let arivalDateNew = self.unixtodate(unixtimeInterval: (Passingdata.transitDetails?.arrivalTime?.value!)!)
                            let googlereponseDirectionList: NSMutableDictionary = [:]

                            let arrival_time = (Passingdata.transitDetails?.arrivalTime?.text!)!
                            let departure_time = (Passingdata.transitDetails?.departureTime?.text!)!
                            var google_color = ""

                            let FirstAddressGoogle = Passingdata.htmlInstructions!


                            let TransitArrivalTime = "\((Passingdata.transitDetails?.arrivalTime?.text)!)"
                            let TransitDepartTime = "\((Passingdata.transitDetails?.departureTime?.text)!)"

                            let SecondDescription = "\((Passingdata.transitDetails?.departureStop?.name)!) - (\(departdateNew))"

                            let ArrivalAddress = "\((Passingdata.transitDetails?.arrivalStop!.name)!) - (\(arivalDateNew))"
                            var TransitRouteNo = ""
                            if Passingdata.transitDetails?.line?.shortName != nil {
                                TransitRouteNo = "\((Passingdata.transitDetails?.line?.shortName)!)"
                            }
                            else {
                                TransitRouteNo = "\((Passingdata.transitDetails?.line?.name)!)"
                            }
                            let TransitTime = "\(departure_time) - \(arrival_time)"
                            print(SecondDescription)

                            if Passingdata.transitDetails!.line?.color != nil {
                                google_color = (Passingdata.transitDetails!.line?.color)!
                            }
                            else {
                                google_color = "\(color.hexStringToUIColor(hex: "#000000"))"

                            }

                            googlereponseDirectionList.setValue(TransitArrivalTime, forKey: "TransitArrivalTime")
                            googlereponseDirectionList.setValue(TransitDepartTime, forKey: "TransitDepartTime")
                            googlereponseDirectionList.setValue(Transitco2, forKey: "TransitCo2Cal")

                            googlereponseDirectionList.setValue("false", forKey: "RealTime")
                            googlereponseDirectionList.setValue("\(google_color)", forKey: "RouteLineColor")
                            googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                            googlereponseDirectionList.setValue("", forKey: "TripStatus")
                            googlereponseDirectionList.setValue("TRANSIT", forKey: "ModeOFTransit")
                            googlereponseDirectionList.setValue("\((polylineString)!)", forKey: "polylineString")

                            googlereponseDirectionList.setValue("bus_Transit", forKey: "TransitImage")
                            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                            googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                            googlereponseDirectionList.setValue(TransitRouteNo, forKey: "RouteLine")
                            googlereponseDirectionList.setValue(ArrivalAddress, forKey: "ArrivalStop")
                            googlereponseDirectionList.setValue("", forKey: "NextStop")
                            googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                            googlereponseDirectionList.setValue(Passingdata.transitDetails?.arrivalTime?.value, forKey: "WalkingCalArrival")
                            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                            print("Before Added \(self.ii)")
                            self.ii = self.ii + 1
                            print("After Added \(self.ii)")
                            completion(false, googlereponseDirectionList)


                            print(error)
                        }


                }

            }


            else {

                let userDefaultsForBike = UserDefaults.standard



                if userDefaultsForBike.bool(forKey: "Bikeshare") && Passingdata.distance!.value! > 500 {
                    self.BikeShareCount = 0
                    var TempNegtiveBikeShareCount = 0

                    var BikeShareArray = [[String: Any]]()
                    let googlereponseDirectionList: NSMutableDictionary = [:]



                    if (SourceDestination.BirdStationDistance != 99999 && SourceDestination.birdListApi != nil) && SourceDestination.birdListApi?.count != 0
                        {
                        
                            print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                   self.BikeRideShare = true
                                                                                                            print("After BikeRideShareBool \(self.BikeRideShare) ")
                        // print("\(SourceDestination.BirdStationDistance) BirdStationDistanc")
                        var TransitTime = ""
                        var arrivaltime: Double = 0.0
                        var waitingTravel = ""
                        var TotalBirdTime: Int?
                        var TotalBirdDistance: Int?
                        let userDefaults = UserDefaults.standard
                        let BirdLat: Double?
                        var BirdLong: Double?
                        var BirdWalkingDistance: Double?
                        var BirdWalkingDuration: Double?
                        var BirdWalkingRoute: MKRoute?
                        var BirdTransitRoute: MKRoute?
                        var BirdTransitDistance: Double?
                        var BirdTransitDuration: Double?
                        var BirdlatFinal: Double?
                        var BirdlongFinal: Double?



                        let BikeShareArrayList: NSMutableDictionary = [:]

                        let distanceValue = (Passingdata.distance?.value)!
                        let DurationWalking = (Passingdata.duration?.value)!

                        // let BirdDistance = getBirdDistance(sourceLat: Passingdata.startLocation!.lat!, sourceLng: Passingdata.startLocation!.lng!)


                        var BirdCheckBikeCount = 0
                        var BirddistanceInMeters: Double?
                        var BirdSourceDistance = 0.0
                        var BirdtempDistance = 0.0
                        let birdListApi = SourceDestination.birdListApi
                        let BirdSourcelatlatlong = CLLocation(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!)

                        for BirdBike in (birdListApi)! {
                            let BirdBikeLat = BirdBike.bike_lat
                            let BirdBikeLng = BirdBike.bike_lon

                            print("\(BirdBikeLat),\(BirdBikeLng) -> Bird LatLng")

                            //  DispatchQueue.main.async {
                            if BirdCheckBikeCount == 0 {

                                let Birddestinationlatlong = CLLocation(latitude: BirdBikeLat!, longitude: BirdBikeLng!)
                                BirddistanceInMeters = BirdSourcelatlatlong.distance(from: Birddestinationlatlong)
                                BirdSourceDistance = (BirddistanceInMeters! * 100).rounded() / 100
                                BirdCheckBikeCount = BirdCheckBikeCount + 1

                                print("\(BirdSourceDistance)")
                                BirdlatFinal = BirdBikeLat
                                BirdlongFinal = BirdBikeLng
                                print(BirdlongFinal)
                                print(BirdlatFinal)
                                print(BirdSourceDistance)




                            }
                            else {
                                self.templat = BirdBikeLat!
                                self.templong = BirdBikeLng!
                                let Birddestinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                BirddistanceInMeters = BirdSourcelatlatlong.distance(from: Birddestinationlatlong)
                                BirdtempDistance = (BirddistanceInMeters! * 100).rounded() / 100
                                print("\(BirdtempDistance) < \(BirdSourceDistance)")
                                if BirdtempDistance < BirdSourceDistance {

                                    BirdSourceDistance = BirdtempDistance
                                    print(BirdtempDistance)
                                    BirdCheckBikeCount = BirdCheckBikeCount + 1
                                    BirdlatFinal = BirdBikeLat
                                    BirdlongFinal = BirdBikeLng

                                    print(BirdlongFinal)
                                    print(BirdlatFinal)
                                    print(BirdSourceDistance)

                                }
                                else {
                                    BirdCheckBikeCount = BirdCheckBikeCount + 1
                                }
                            }


                        }


                        let BirdWalkingrequest = MKDirections.Request()
                        BirdWalkingrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!), addressDictionary: nil))

                        BirdWalkingrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: BirdlatFinal!, longitude: BirdlongFinal!), addressDictionary: nil))
                        BirdWalkingrequest.requestsAlternateRoutes = true

                        BirdWalkingrequest.transportType = .walking


                        let BirdWalkingdirections = MKDirections(request: BirdWalkingrequest)

                        BirdWalkingdirections.calculate { [unowned self] BirdWalkingresponse, error in
                            guard let unwrappedResponse = BirdWalkingresponse else { return }

                            BirdWalkingRoute = unwrappedResponse.routes[0]
                            BirdWalkingDistance = BirdWalkingRoute?.distance
                            BirdWalkingDuration = BirdWalkingRoute?.expectedTravelTime


                            var BirdWalkingMiles = ""
                            if Int(BirdWalkingDistance!) > 243 {

                                let CalculateDistance = (Double(BirdWalkingDuration!) / 1609.344)
                                let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                BirdWalkingMiles = "\(CalculateDistanceTest) miles"
                            }
                            else {
                                let CalculateDistance = (Double(BirdWalkingDuration!) * 3.281)
                                let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                BirdWalkingMiles = "\(CalculateDistanceTest) ft"

                            }

                            let caloriesburn = (BirdWalkingDuration! * 1.25) / 20
                            let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                            let BirdWalkingTimeMin = self.convertTime(miliseconds: Int(BirdWalkingDuration! * 1000))




                            let WalkingAboutandCaloties = "About \(BirdWalkingTimeMin), \(BirdWalkingMiles) (\(caloriesburnint) Calories)"




                            let walkingInstration = "Walk to \((BirdWalkingRoute?.name)!)"

                            print("\((walkingInstration)) Waling to Bird")


                            let walkingmin = (Passingdata.duration?.text)!










                            let BirdTransitrequest = MKDirections.Request()
                            BirdTransitrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: BirdlatFinal!, longitude: BirdlongFinal!), addressDictionary: nil))
                            BirdTransitrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.endLocation!.lat!, longitude: Passingdata.endLocation!.lng!), addressDictionary: nil))
                            BirdTransitrequest.requestsAlternateRoutes = true

                            BirdTransitrequest.transportType = .automobile

                            var BikeShareTime = ""
                            var BikeShareEndTime = ""

                            let BirdTransitirections = MKDirections(request: BirdTransitrequest)

                            BirdTransitirections.calculate { [unowned self] BirdTransitresponse, error in
                                guard let BirdTransitResponse = BirdTransitresponse else { return }

                                BirdTransitRoute = BirdTransitResponse.routes.first
                                BirdTransitDuration = BirdTransitRoute!.expectedTravelTime
                                if index == 0 {

                                    let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                    // let walkingminepoch = (Passingdata.duration?.value)
                                    let walkingmincal = BirdWalkingDuration! + (BirdTransitDuration!)
                                    waitingTravel = "No wait time."
                                    let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(walkingmincal), addsub: "sub")
                                    TransitTime = "\(getstime)"
                                    arrivaltime = walkingstart
                                    print(walkingstart)
                                    print(BirdWalkingDuration)

                                    BikeShareTime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(BirdTransitDuration!), addsub: "sub")

                                    BikeShareEndTime = "0"
                                }

                                else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                    let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                    let walkingminepoch = (Passingdata.duration?.value)
                                    let walkingmincal = BirdWalkingDuration
                                    let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(walkingmincal!), addsub: "add")
                                    //cell.TransitTime.text = "\(getstime)"
                                    TransitTime = "\(getstime)"
                                    arrivaltime = walkingstart

                                    waitingTravel = "No wait time."
                                    print("Calculateissue")
                                    print(walkingstart)
                                    print(walkingminepoch)


                                    BikeShareTime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(BirdTransitDuration!) + Int(BirdWalkingDuration!), addsub: "add")

                                    BikeShareEndTime = "1"

                                }
                                else {

                                    var walkstringtoint = ""
                                    //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                    let walkingminepoch = (Passingdata.duration?.value)
                                    let walkingmincal = BirdWalkingDuration
                                    if walkingmin.contains("mins") {
                                        walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                    }
                                    else {
                                        walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                    }
                                    let walkminInt: Int = Int(walkstringtoint)! * 60

                                    let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                    print(walkminInt)
                                    print(walkingminepoch)
                                    print(walkingend)
                                    let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: Int(walkingmincal!), addsub: "add")

                                    TransitTime = "\(getstime)"
                                    arrivaltime = walkingend
                                    let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                    let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                    waitingTravel = "Wait time \(waitingtimeResult)"

                                    BikeShareTime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: Int(BirdTransitDuration!) + Int(BirdWalkingDuration!), addsub: "add")
                                    BikeShareEndTime = "2"

                                }



                                BirdTransitDistance = BirdTransitRoute?.distance


                                print("\(BirdWalkingDistance) BirdWalkingDistance")
                                print("\(BirdWalkingDuration) BirdWalkingDuration")
                                print("\(BirdTransitDistance) BirdTransitDistance")
                                print("\(BirdTransitDuration) BirdTransitDuration")


                                let durationStringADD = BirdTransitDuration
                                let sectominCalWithFare: Double = (Double(durationStringADD! / 60).rounded(.toNearestOrAwayFromZero))
                                print(((sectominCalWithFare * 0.15) + 1))
                                let BirdBike_fare_Cal = ((sectominCalWithFare * 0.15) + 1)


                                let totalvalue = BirdBike_fare_Cal.rounded(.toNearestOrAwayFromZero)
                                 let totalString = String(format: "$%.2f", totalvalue)

                               // let totalString = String(format: "$%.2f", BirdBike_fare_Cal)




                                let BirdTransitTimeMin = self.convertTime(miliseconds: Int(BirdTransitDuration! * 1000))

                                var BirdTransitMiles = ""

                                if Int(BirdTransitDistance!) > 243 {

                                    let CalculateDistance = (Double(BirdTransitDistance!) / 1609.344)
                                    let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                    BirdTransitMiles = "\(CalculateDistanceTest) miles"
                                }
                                else {
                                    let CalculateDistance = (Double(BirdTransitDistance!) * 3.281)
                                    let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                    BirdTransitMiles = "\(CalculateDistanceTest) ft"

                                }

                                var CO2Emmision = (Double(BirdTransitDistance!) * 0.0000419426)
                                let test = String(format: "%.2f", CO2Emmision)
                                let textcount = test.count
                                print(CO2Emmision)
                                let totalcount = textcount + 7
                                CO2Emmision = self.getAmountRounded(CO2Emmision, withRoundOff: Int(5))
                                let font: UIFont? = UIFont(name: "Helvetica", size: 12)
                                let fontSuper: UIFont? = UIFont(name: "Helvetica", size: 8)

                                let BirdTransitco2: NSMutableAttributedString = NSMutableAttributedString(string: "\(test) lbs CO2", attributes: [.font: font!])
                                BirdTransitco2.setAttributes([.font: fontSuper!, .baselineOffset: -8], range: NSRange(location: totalcount, length: 1))

                                // cell.dashboardTitle.attributedText = attString
                                print("\(BirdTransitco2) Isaaaaaaa")



                                let WalkingAboutandCo2 = "About \(BirdTransitTimeMin), \(BirdTransitMiles) \(test) lbs CO2"

                                TotalBirdTime = Int(BirdWalkingDuration!) + Int(BirdTransitDuration!)
                                TotalBirdDistance = Int(BirdWalkingDistance!) + Int(BirdTransitDistance!)

                                print("\(TotalBirdTime) TotalBirdTime")
                                print("\(TotalBirdDistance) TotalBirdDistance")
                                print("\(distanceValue) distanceValue")
                                print("\(DurationWalking) DurationWalking")
                                print("\(BirdlatFinal!),\(BirdlongFinal!)")

                                //  self.TotalMinTripForTravel = self.TotalMinTripForTravel + (TotalBirdTime!)

                                let location = CLLocation(latitude: BirdlatFinal!, longitude: BirdlongFinal!)

                                var ReveseGeoCodeAddress = "";
                                location.geocode { placemark, error in
                                    if let error = error as? CLError {
                                        print("CLError:", error)
                                        return
                                    } else if let placemark = placemark?.first {
                                        // you should always update your UI in the main thread
                                        DispatchQueue.main.async {
                                            //  update UI here
                                            ReveseGeoCodeAddress = "\((placemark.name)!), \((placemark.locality)!), \((placemark.administrativeArea)!), \((placemark.postalCode)!), \((placemark.isoCountryCode)!)"



                                            // dateconvert(Datevalue: <#T##Double#>, WalkingTimeinterval: BirdTransitDuration, addsub: "add")

                                            if Int(BirdSourceDistance) < Int(500) && Int(distanceValue) > Int(BirdSourceDistance) && TotalBirdTime! <= Int(DurationWalking) {

//                                            (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: Passingdata.startLocation!.lat!, SourceLng: Passingdata.startLocation!.lng!, DestinationLat:BirdlatFinal!, DestinationLng: BirdlongFinal!, Type: "Walking")
//                                         //   print("Bird Lat \(Passingdata.startLocation!.lat!),\(Passingdata.startLocation!.lng!),\(BirdlongFinal!),\(BirdlongFinal!)")
//                                            (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: BirdlatFinal!, SourceLng: BirdlongFinal!, DestinationLat: Passingdata.endLocation!.lat!, DestinationLng: Passingdata.endLocation!.lng!, Type: "Bird")
//
                                                //   TypeOfShare.text = "Bikeshare"

                                               print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                                      self.BikeRideShare = true
                                                            print("After BikeRideShareBool \(self.BikeRideShare) ")

                                                BikeShareArrayList.setValue(BirdBike_fare_Cal, forKey: "vehiclefare")
                                                BikeShareArrayList.setValue("Bird", forKey: "vehicleType")
                                                BikeShareArrayList.setValue(TotalBirdTime, forKey: "TotalTime")
                                                BikeShareArrayList.setValue("#000000", forKey: "vehicleColor")
                                                BikeShareArrayList.setValue("Bird-1", forKey: "vehicleTypeImage")
                                                BikeShareArrayList.setValue("\(walkingInstration)", forKey: "WalkingInstructions")
                                                BikeShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCaloties")
                                                BikeShareArrayList.setValue("\(waitingTravel)", forKey: "WaitingTime")
                                                BikeShareArrayList.setValue("\(TransitTime)", forKey: "WakingStartTime")
                                                BikeShareArrayList.setValue("Pickup your Bird Vehicle from", forKey: "VehiclePickupLocation")
                                                BikeShareArrayList.setValue("\(ReveseGeoCodeAddress)", forKey: "vehicleStation")
                                                BikeShareArrayList.setValue("Get Bird estimated fare:\(totalString)", forKey: "Fare")
                                                BikeShareArrayList.setValue("\((WalkingAboutandCo2))", forKey: "WalkingAboutandCo2")
                                                BikeShareArrayList.setValue("\(BikeShareTime)", forKey: "vehicleTime")

                                                BikeShareArrayList.setValue("\(BikeShareEndTime)", forKey: "BikeShareEndTime")

                                                BikeShareArrayList.setValue(BirdlatFinal!, forKey: "BikeShareSourceLat")
                                                BikeShareArrayList.setValue(BirdlongFinal!, forKey: "BikeShareSourceLng")
                                                BikeShareArrayList.setValue(Passingdata.endLocation!.lat!, forKey: "BikeShareDestinationLat")
                                                BikeShareArrayList.setValue(Passingdata.endLocation!.lng!, forKey: "BikeShareDestinationLng")


                                                BikeShareArrayList.setValue(Passingdata.startLocation!.lat!, forKey: "WalkingSourceLat")
                                                BikeShareArrayList.setValue(Passingdata.startLocation!.lng!, forKey: "WalkingSourceLng")
                                                BikeShareArrayList.setValue(BirdlatFinal!, forKey: "WalkingDestinationLat")
                                                BikeShareArrayList.setValue(BirdlongFinal!, forKey: "WalkingDestinationLng")



                                                BikeShareArray.append(BikeShareArrayList as! [String: Any])
                                                self.BikeShareCount = self.BikeShareCount + 1

                                                self.BikeRideShareCompletedCheck(BikeShareArrayList: BikeShareArray, Type: "Bike", completion: { (success, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare) in

                                                    if success {

                                                        googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                        googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                        googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                        googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                        googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                        googlereponseDirectionList.setValue("\((firstBikeShareAddress))", forKey: "FirstAddress")
                                                        googlereponseDirectionList.setValue("\((SecondDescription))", forKey: "SecondAddress")
                                                        googlereponseDirectionList.setValue("\((waitStartTime))", forKey: "Timing")
                                                        googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                        googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                        googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                        googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                        googlereponseDirectionList.setValue("Passingdata", forKey: "TravelingTime")
                                                        googlereponseDirectionList.setValue("arrivaltime", forKey: "WalkingCalArrival")
                                                        googlereponseDirectionList.setValue("\((WaitTimeWalking))", forKey: "WalkingWaitingTime")

                                                        googlereponseDirectionList.setValue(SourceWalkingLat, forKey: "SourceLat")
                                                        googlereponseDirectionList.setValue(SourceWalkingLng, forKey: "SourceLng")
                                                        googlereponseDirectionList.setValue(DestinationWalkingLat, forKey: "DestinationLat")
                                                        googlereponseDirectionList.setValue(DestinationWalkingLng, forKey: "DestinationLng")
                                                        googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


                                                        self.totalTableCellCount = self.totalTableCellCount + 1
                                                        self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                        print(googlereponseDirectionList)
                                                        print("Before Added \(self.ii)")
                                                        self.ii = self.ii + 2

                                                        print("After Added \(self.ii)")

                                                        googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                        googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                        googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                        googlereponseDirectionList.setValue("\(Double(index) + 0.1)", forKey: "Index")
                                                        googlereponseDirectionList.setValue("Bolt4", forKey: "TransitImage")
                                                        googlereponseDirectionList.setValue("", forKey: "FirstAddress")
                                                        googlereponseDirectionList.setValue("", forKey: "SecondAddress")
                                                        googlereponseDirectionList.setValue("\((TimingForBikeShare))", forKey: "Timing")
                                                        googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                        googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                        googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                        googlereponseDirectionList.setValue("BikeShare", forKey: "ModeOFTransit")
                                                        googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                                                        googlereponseDirectionList.setValue("", forKey: "TravelingTime")
                                                        googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
                                                        googlereponseDirectionList.setValue("", forKey: "WalkingWaitingTime")
                                                        googlereponseDirectionList.setValue(BikeShareArray, forKey: "BikeShareArray")

                                                        googlereponseDirectionList.setValue(SourceBikeLat, forKey: "SourceLat")
                                                        googlereponseDirectionList.setValue(SourceBikeLng, forKey: "SourceLng")
                                                        googlereponseDirectionList.setValue(DestinationBikeLat, forKey: "DestinationLat")
                                                        googlereponseDirectionList.setValue(DestinationBikeLng, forKey: "DestinationLng")






                                                        self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                        print(googlereponseDirectionList)
                                                        // self.ii = self.ii + 1
                                                        completion(false, googlereponseDirectionList)





                                                    }


                                                    else {
         print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                           if !self.BikeRideShare {
                                                                                               self.BikeRideShare = false
                                                                                           }
                                                                                            print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                        print ("BirdNotAdded \(Date())")
                                                        print("BeforeBikeShareCount Bird Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                        TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                                        self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                                        print(" afterBikeShareCount Bird Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                        if self.BikeShareCountNeg == 3 {

                                                            // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                                            print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                                            let googlereponseDirectionList: NSMutableDictionary = [:]
                                                            var waitingTravel = ""
                                                            let userDefaults = UserDefaults.standard
                                                            let distanceValue = (Passingdata.distance?.value)!
                                                            let DurationWalking = (Passingdata.duration?.value)!
                                                            var waitingtime = ""

                                                            print((Passingdata.distance?.value)!)
                                                            var durationString = 0
                                                            var getstime = ""
                                                            var uberwaitingtime = ""
                                                            var arrivaltime: Double = 0.0



                                                            var TransitTime = ""


                                                            let walkingmin = (Passingdata.duration?.text)!
                                                            let walkingmiles = (Passingdata.distance?.text)!
                                                            let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                                            let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                                            let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                                            // cell.TransitRouteNo.isHidden = true
                                                            // cell.TransitTime.frame.origin.
                                                            // let walkingarriaval = walkingdeparttime
                                                            if index == 0 {

                                                                let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                let walkingminepoch = (Passingdata.duration?.value)
                                                                let walkingmincal = walkingminepoch
                                                                waitingTravel = "No wait time."
                                                                let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                                                TransitTime = "\(getstime)"
                                                                arrivaltime = walkingstart
                                                                //cell.TransitTime.text = "\(getstime)"
                                                            }
                                                            else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                                                let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                let walkingminepoch = (Passingdata.duration?.value)
                                                                let walkingmincal = walkingminepoch
                                                                let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                //cell.TransitTime.text = "\(getstime)"
                                                                TransitTime = "\(getstime)"
                                                                arrivaltime = walkingstart

                                                                waitingTravel = "No wait time."
                                                            }
                                                            else {
                                                                var walkstringtoint = ""
                                                                //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                                                let walkingminepoch = (Passingdata.duration?.value)
                                                                let walkingmincal = walkingminepoch
                                                                if walkingmin.contains("mins") {
                                                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                                                }
                                                                else {
                                                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                                                }
                                                                let walkminInt: Int = Int(walkstringtoint)! * 60
                                                                print(walkminInt)
                                                                let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                TransitTime = "\(getstime)"
                                                                arrivaltime = walkingend
                                                                let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                                                let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                                                waitingTravel = "Wait time \(waitingtimeResult)"

                                                            }


                                                            let FirstAddressGoogle = Passingdata.htmlInstructions
                                                            let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                                            googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                            googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                            googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                            googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                            googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                            googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                            googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                            googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                            googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                            googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                            googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                            googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                                            googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                                            googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                                            googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                                            googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                                            googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")


                                                            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


                                                            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                            print(googlereponseDirectionList)
                                                            print("Before Added \(self.ii)")
                                                            self.ii = self.ii + 1
                                                            print("After Added \(self.ii)")
                                                            completion(false, googlereponseDirectionList)


                                                        }

                                                    }


                                                })



                                            }
                                            else {
                                                print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                   if !self.BikeRideShare {
                                                                                       self.BikeRideShare = false
                                                                                   }
                                                                                    print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                print ("BirdNotAdded \(Date())")
                                                print("BeforeBikeShareCount Bird Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                                self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                                print(" afterBikeShareCount Bird Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                if self.BikeShareCountNeg == 3 {

                                                    // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                                    print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                                    let googlereponseDirectionList: NSMutableDictionary = [:]
                                                    var waitingTravel = ""
                                                    let userDefaults = UserDefaults.standard
                                                    let distanceValue = (Passingdata.distance?.value)!
                                                    let DurationWalking = (Passingdata.duration?.value)!
                                                    var waitingtime = ""

                                                    print((Passingdata.distance?.value)!)
                                                    var durationString = 0
                                                    var getstime = ""
                                                    var uberwaitingtime = ""
                                                    var arrivaltime: Double = 0.0



                                                    var TransitTime = ""


                                                    let walkingmin = (Passingdata.duration?.text)!
                                                    let walkingmiles = (Passingdata.distance?.text)!
                                                    let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                                    let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                                    let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                                    // cell.TransitRouteNo.isHidden = true
                                                    // cell.TransitTime.frame.origin.
                                                    // let walkingarriaval = walkingdeparttime
                                                    if index == 0 {

                                                        let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                        let walkingminepoch = (Passingdata.duration?.value)
                                                        let walkingmincal = walkingminepoch
                                                        waitingTravel = "No wait time."
                                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                                        TransitTime = "\(getstime)"
                                                        arrivaltime = walkingstart
                                                        //cell.TransitTime.text = "\(getstime)"
                                                    }
                                                    else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                                        let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                        let walkingminepoch = (Passingdata.duration?.value)
                                                        let walkingmincal = walkingminepoch
                                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                        //cell.TransitTime.text = "\(getstime)"
                                                        TransitTime = "\(getstime)"
                                                        arrivaltime = walkingstart

                                                        waitingTravel = "No wait time."
                                                    }
                                                    else {
                                                        var walkstringtoint = ""
                                                        //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                                        let walkingminepoch = (Passingdata.duration?.value)
                                                        let walkingmincal = walkingminepoch
                                                        if walkingmin.contains("mins") {
                                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                                        }
                                                        else {
                                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                                        }
                                                        let walkminInt: Int = Int(walkstringtoint)! * 60
                                                        print(walkminInt)
                                                        let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                        let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                        TransitTime = "\(getstime)"
                                                        arrivaltime = walkingend
                                                        let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                                        let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                                        waitingTravel = "Wait time \(waitingtimeResult)"

                                                    }


                                                    let FirstAddressGoogle = Passingdata.htmlInstructions
                                                    let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                    googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                    googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                    googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                    googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")



                                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                    print(googlereponseDirectionList)
                                                    print("Before Added \(self.ii)")
                                                    self.ii = self.ii + 1
                                                    print("After Added \(self.ii)")
                                                    completion(false, googlereponseDirectionList)


                                                }

                                            }

                                        }
                                    }
                                }




                            }


                        }
                    }
                    else {
                        print("Before BikeRideShareBool \(self.BikeRideShare) ")
                        if !self.BikeRideShare {
                            self.BikeRideShare = false
                                                           }
                                                            print("After BikeRideShareBool \(self.BikeRideShare) ")
                        print ("BirdNotAdded \(Date())")
                        print("BeforeBikeShareCount Bird Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                        TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                        self.BikeShareCountNeg = TempNegtiveBikeShareCount

                        print(" afterBikeShareCount Bird Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                        if self.BikeShareCountNeg == 3 {

                            // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                            print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                            let googlereponseDirectionList: NSMutableDictionary = [:]
                            var waitingTravel = ""
                            let userDefaults = UserDefaults.standard
                            let distanceValue = (Passingdata.distance?.value)!
                            let DurationWalking = (Passingdata.duration?.value)!
                            var waitingtime = ""

                            print((Passingdata.distance?.value)!)
                            var durationString = 0
                            var getstime = ""
                            var uberwaitingtime = ""
                            var arrivaltime: Double = 0.0



                            var TransitTime = ""


                            let walkingmin = (Passingdata.duration?.text)!
                            let walkingmiles = (Passingdata.distance?.text)!
                            let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                            let caloriesburn = (walkingdistancevalue * 1.25) / 20
                            let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                            // cell.TransitRouteNo.isHidden = true
                            // cell.TransitTime.frame.origin.
                            // let walkingarriaval = walkingdeparttime
                            if index == 0 {

                                let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                let walkingminepoch = (Passingdata.duration?.value)
                                let walkingmincal = walkingminepoch
                                waitingTravel = "No wait time."
                                let getstime = dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                TransitTime = "\(getstime)"
                                arrivaltime = walkingstart
                                //cell.TransitTime.text = "\(getstime)"
                            }
                            else if index == (dataPrevRespone.result.value?.routes![indexValue].legs![0].steps?.count)! - 1 {

                                let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                let walkingminepoch = (Passingdata.duration?.value)
                                let walkingmincal = walkingminepoch
                                let getstime = dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                //cell.TransitTime.text = "\(getstime)"
                                TransitTime = "\(getstime)"
                                arrivaltime = walkingstart

                                waitingTravel = "No wait time."
                            }
                            else {
                                var walkstringtoint = ""
                                //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                let walkingminepoch = (Passingdata.duration?.value)
                                let walkingmincal = walkingminepoch
                                if walkingmin.contains("mins") {
                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                }
                                else {
                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                }
                                let walkminInt: Int = Int(walkstringtoint)! * 60
                                print(walkminInt)
                                let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                TransitTime = "\(getstime)"
                                arrivaltime = walkingend
                                let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                let waitingtimeResult = datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                waitingTravel = "Wait time \(waitingtimeResult)"

                            }


                            let FirstAddressGoogle = Passingdata.htmlInstructions
                            let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                            googlereponseDirectionList.setValue("false", forKey: "RealTime")
                            googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                            googlereponseDirectionList.setValue("", forKey: "TripStatus")
                            googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                            googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                            googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                            googlereponseDirectionList.setValue("", forKey: "RouteLine")
                            googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                            googlereponseDirectionList.setValue("", forKey: "NextStop")
                            googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                            googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                            googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                            googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                            googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                            googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                            googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                            googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")

                            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")



                            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                            print(googlereponseDirectionList)
                            print("Before Added \(self.ii)")
                            self.ii = self.ii + 1
                            print("After Added \(self.ii)")
                            completion(false, googlereponseDirectionList)


                        }

                    }
                    let delay = 3.0 + Double(index)
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                            if (SourceDestination.LimeStationDistance != 99999 && SourceDestination.LimeListApi != nil) && SourceDestination.LimeListApi?.count != 0 {
                                  print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                         self.BikeRideShare = true
                                                        print("After BikeRideShareBool \(self.BikeRideShare) ")
                                print("\(SourceDestination.LimeStationDistance) LimeStationDistance")
                                var TotalLimeTime: Int?
                                var TotalLimeDistance: Int?
                                let userDefaults = UserDefaults.standard
                                let LimeLat: Double?
                                var LimeLong: Double?
                                var LimeWalkingDistance: Double?
                                var LimeWalkingDuration: Double?
                                var LimeWalkingRoute: MKRoute?
                                var LimeTransitRoute: MKRoute?
                                var LimeTransitDistance: Double?
                                var LimeTransitDuration: Double?
                                var LimelatFinal: Double?
                                var LimelongFinal: Double?
                                var TransitTime = ""
                                var arrivaltime: Double = 0.0
                                var waitingTravel = ""
                                let googlereponseDirectionList: NSMutableDictionary = [:]

                                let BikeShareArrayList: NSMutableDictionary = [:]

                                let distanceValue = (Passingdata.distance?.value)!
                                let DurationWalking = (Passingdata.duration?.value)!

                                var LimeCheckBikeCount = 0
                                var LimedistanceInMeters: Double?
                                var LimeSourceDistance = 0.0
                                var LimetempDistance = 0.0
                                let LimeListApi = SourceDestination.LimeListApi
                                let LimeSourcelatlatlong = CLLocation(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!)


                                for LimeBike in (LimeListApi)! {
                                    guard let LatLime = LimeBike.bike_lat else { return }
                                    guard let LongLime = LimeBike.bike_lon else { return }


                                    let LimeBikeLat = Double(LatLime)
                                    let LimeBikeLng = Double(LongLime)

                                    print("\(LimeBikeLat),\(LimeBikeLng) -> Lime LatLng")

                                    //  DispatchQueue.main.async {
                                    if LimeCheckBikeCount == 0 {

                                        let Limedestinationlatlong = CLLocation(latitude: LimeBikeLat!, longitude: LimeBikeLng!)
                                        LimedistanceInMeters = LimeSourcelatlatlong.distance(from: Limedestinationlatlong)
                                        LimeSourceDistance = (LimedistanceInMeters! * 100).rounded() / 100
                                        LimeCheckBikeCount = LimeCheckBikeCount + 1

                                        print("\(LimeSourceDistance)")
                                        LimelatFinal = LimeBikeLat
                                        LimelongFinal = LimeBikeLng
                                        print(LimelongFinal)
                                        print(LimelatFinal)
                                        print(LimeSourceDistance)




                                    }
                                    else {
                                        self.templat = LimeBikeLat!
                                        self.templong = LimeBikeLng!
                                        let Limedestinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                        LimedistanceInMeters = LimeSourcelatlatlong.distance(from: Limedestinationlatlong)
                                        LimetempDistance = (LimedistanceInMeters! * 100).rounded() / 100
                                        print("\(LimetempDistance) < \(LimeSourceDistance)")
                                        if LimetempDistance < LimeSourceDistance {

                                            LimeSourceDistance = LimetempDistance
                                            print(LimetempDistance)
                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                            LimelatFinal = LimeBikeLat
                                            LimelongFinal = LimeBikeLng

                                            print(LimelongFinal)
                                            print(LimelatFinal)
                                            print(LimeSourceDistance)

                                        }
                                        else {
                                            LimeCheckBikeCount = LimeCheckBikeCount + 1
                                        }
                                    }


                                }


                                let LimeWalkingrequest = MKDirections.Request()
                                LimeWalkingrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!), addressDictionary: nil))

                                LimeWalkingrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: LimelatFinal!, longitude: LimelongFinal!), addressDictionary: nil))
                                LimeWalkingrequest.requestsAlternateRoutes = true

                                LimeWalkingrequest.transportType = .walking

                                let LimeWalkingdirections = MKDirections(request: LimeWalkingrequest)

                                LimeWalkingdirections.calculate { [unowned self] LimeWalkingresponse, error in
                                    guard let unwrappedResponse = LimeWalkingresponse else { return }

                                    LimeWalkingRoute = unwrappedResponse.routes[0]
                                    LimeWalkingDistance = LimeWalkingRoute?.distance
                                    LimeWalkingDuration = LimeWalkingRoute?.expectedTravelTime

                                    //(self.parent as? TripMapViewController)?.WalkingDrawPolyLine(WalkingRoutePolyline: (LimeWalkingRoute?.polyline)!)


                                    var LimeWalkingMiles = ""
                                    if Int(LimeWalkingDistance!) > 243 {

                                        let CalculateDistance = (Double(LimeWalkingDuration!) / 1609.344)
                                        let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                        LimeWalkingMiles = "\(CalculateDistanceTest) miles"
                                    }
                                    else {
                                        let CalculateDistance = (Double(LimeWalkingDuration!) * 3.281)
                                        let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                        LimeWalkingMiles = "\(CalculateDistanceTest) ft"

                                    }



                                    let caloriesburn = (LimeWalkingDistance! * 1.25) / 20
                                    let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                    let LimeWalkingTimeMin = self.convertTime(miliseconds: Int(LimeWalkingDuration! * 1000))




                                    let WalkingAboutandCaloties = "About \(LimeWalkingTimeMin), \(LimeWalkingMiles) (\(caloriesburnint) Calories)"




                                    let walkingInstration = "Walk to \((LimeWalkingRoute?.name)!)"

                                    print("\((walkingInstration)) Waling to Lime")

                                    let walkingmin = (Passingdata.duration?.text)!


                                    let LimeTransitrequest = MKDirections.Request()
                                    LimeTransitrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: LimelatFinal!, longitude: LimelongFinal!), addressDictionary: nil))
                                    LimeTransitrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.endLocation!.lat!, longitude: Passingdata.endLocation!.lng!), addressDictionary: nil))
                                    LimeTransitrequest.requestsAlternateRoutes = true

                                    LimeTransitrequest.transportType = .automobile


                                    let LimeTransitirections = MKDirections(request: LimeTransitrequest)

                                    LimeTransitirections.calculate { [unowned self] LimeTransitresponse, error in
                                        guard let LimeTransitResponse = LimeTransitresponse else { return }
                                        var BikeShareTime = ""
                                        var BikeShareEndTime = ""
                                        LimeTransitRoute = LimeTransitResponse.routes.first
                                        LimeTransitDistance = LimeTransitRoute?.distance
                                        LimeTransitDuration = LimeTransitRoute!.expectedTravelTime
                                        if index == 0 {

                                            let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                            // let walkingminepoch = (Passingdata.duration?.value)
                                            let walkingmincal = LimeWalkingDuration! + LimeTransitDuration!
                                            waitingTravel = "No wait time."
                                            let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(walkingmincal), addsub: "sub")
                                            TransitTime = "\(getstime)"
                                            arrivaltime = walkingstart

                                            BikeShareTime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(LimeTransitDuration!), addsub: "sub")
                                            BikeShareEndTime = "0"
                                        }

                                        else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                            let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                            let walkingminepoch = (Passingdata.duration?.value)
                                            let walkingmincal = LimeWalkingDuration
                                            let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(walkingmincal!), addsub: "add")
                                            //cell.TransitTime.text = "\(getstime)"
                                            TransitTime = "\(getstime)"
                                            arrivaltime = walkingstart

                                            waitingTravel = "No wait time."
                                            BikeShareTime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(LimeTransitDuration!) + Int(LimeWalkingDuration!), addsub: "add")
                                            BikeShareEndTime = "1"
                                        }
                                        else {

                                            var walkstringtoint = ""
                                            //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                            let walkingminepoch = (Passingdata.duration?.value)
                                            let walkingmincal = LimeWalkingDuration
                                            if walkingmin.contains("mins") {
                                                walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                            }
                                            else {
                                                walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                            }
                                            let walkminInt: Int = Int(walkstringtoint)! * 60
                                            print(walkminInt)
                                            let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                            let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: Int(walkingmincal!), addsub: "add")
                                            TransitTime = "\(getstime)"
                                            arrivaltime = walkingend
                                            let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                            let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                            waitingTravel = "Wait time \(waitingtimeResult)"

                                            BikeShareTime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: Int(LimeTransitDuration!) + Int(LimeWalkingDuration!), addsub: "add")
                                            BikeShareEndTime = "2"

                                        }







                                        print("\((LimeWalkingDistance)!) LimeWalkingDistance")
                                        print("\((LimeWalkingDuration)!) LimeWalkingDuration")
                                        print("\((LimeTransitDistance)!) LimeTransitDistance")
                                        print("\((LimeTransitDuration)!) LimeTransitDuration")

                                        TotalLimeTime = Int(LimeWalkingDuration!) + Int(LimeTransitDuration!)
                                        TotalLimeDistance = Int(LimeWalkingDistance!) + Int(LimeTransitDistance!)

                                        print("\((TotalLimeTime)!) TotalLimeTime")
                                        print("\((TotalLimeDistance)!) TotalLimeDistance")
                                        print("\(distanceValue) distanceValue")
                                        print("\(DurationWalking) DurationWalking")
                                        print("\(LimelatFinal!),\(LimelongFinal!)")


                                        let sectominCalWithFares: Double = (Double(LimeTransitDuration! / 60).rounded(.toNearestOrAwayFromZero))

                                        let BirdBike_fare_Cals = ((sectominCalWithFares * 0.27) + 1)

                                                let totalvalue = BirdBike_fare_Cals.rounded(.toNearestOrAwayFromZero)
                                         let totalString = String(format: "$%.2f", totalvalue)



                                        //  self.TotalMinTripForTravel = self.TotalMinTripForTravel + (TotalLimeTime!)
                                        let LimeTransitTimeMin = self.convertTime(miliseconds: Int(LimeTransitDuration! * 1000))


                                        var LimeTransitMiles = ""

                                        if Int(LimeTransitDistance!) > 243 {

                                            let CalculateDistance = (Double(LimeTransitDistance!) / 1609.344)
                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                            LimeTransitMiles = "\(CalculateDistanceTest) miles"
                                        }
                                        else {
                                            let CalculateDistance = (Double(LimeTransitDistance!) * 3.281)
                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                            LimeTransitMiles = "\(CalculateDistanceTest) ft"

                                        }

                                        var CO2Emmision = (Double(LimeTransitDistance!) * 0.0000419426)
                                        let test = String(format: "%.2f", CO2Emmision)
                                        let textcount = test.count
                                        print(CO2Emmision)
                                        let totalcount = textcount + 7
                                        CO2Emmision = self.getAmountRounded(CO2Emmision, withRoundOff: Int(5))
                                        let font: UIFont? = UIFont(name: "Helvetica", size: 12)
                                        let fontSuper: UIFont? = UIFont(name: "Helvetica", size: 8)

                                        let LimeTransitco2: NSMutableAttributedString = NSMutableAttributedString(string: "\(test) lbs CO2", attributes: [.font: font!])
                                        LimeTransitco2.setAttributes([.font: fontSuper!, .baselineOffset: -8], range: NSRange(location: totalcount, length: 1))

                                        // cell.dashboardTitle.attributedText = attString
                                        print("\(LimeTransitco2) Isaaaaaaa")



                                        let WalkingAboutandCo2 = "About \(LimeTransitTimeMin), \(LimeTransitMiles) \(test) lbs CO2"



                                        let location = CLLocation(latitude: LimelatFinal!, longitude: LimelongFinal!)

                                        var ReveseGeoCodeAddress = "";
                                        location.geocode { placemark, error in
                                            if let error = error as? CLError {
                                                print("CLError:", error)
                                                return
                                            } else if let placemark = placemark?.first {
                                                // you should always update your UI in the main thread
                                                DispatchQueue.main.async {
                                                    //  update UI here
                                                    ReveseGeoCodeAddress = "\((placemark.name)!), \((placemark.locality)!), \((placemark.administrativeArea)!), \((placemark.postalCode)!), \((placemark.isoCountryCode)!)"
                                                    print("name:", placemark.name ?? "unknown")

                                                    print("address1:", placemark.thoroughfare ?? "unknown")
                                                    print("address2:", placemark.subThoroughfare ?? "unknown")
                                                    print("neighborhood:", placemark.subLocality ?? "unknown")
                                                    print("city:", placemark.locality ?? "unknown")

                                                    print("state:", placemark.administrativeArea ?? "unknown")
                                                    print("subAdministrativeArea:", placemark.subAdministrativeArea ?? "unknown")
                                                    print("zip code:", placemark.postalCode ?? "unknown")
                                                    print("country:", placemark.country ?? "unknown", terminator: "\n\n")

                                                    print("isoCountryCode:", placemark.isoCountryCode ?? "unknown")
                                                    print("region identifier:", placemark.region?.identifier ?? "unknown")

                                                    print("timezone:", placemark.timeZone ?? "unknown", terminator: "\n\n")









                                                    if Int(LimeSourceDistance) < Int(500) && Int(distanceValue) > Int(LimeSourceDistance) && TotalLimeTime! <= Int(DurationWalking) {

                                                        //(self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: Passingdata.startLocation!.lat!, SourceLng: Passingdata.startLocation!.lng!, DestinationLat: LimelatFinal!, DestinationLng: LimelongFinal!, Type: "Walking")

                                                        // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: LimelatFinal!, SourceLng: LimelongFinal!, DestinationLat: Passingdata.endLocation!.lat!, DestinationLng: Passingdata.endLocation!.lng!, Type: "Lime")
                                                        print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                        self.BikeRideShare = true
                                                                                 print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                        BikeShareArrayList.setValue(BirdBike_fare_Cals, forKey: "vehiclefare")
                                                        BikeShareArrayList.setValue(TotalLimeTime, forKey: "TotalTime")
                                                        BikeShareArrayList.setValue("Lime", forKey: "vehicleType")
                                                        BikeShareArrayList.setValue("#69DF39", forKey: "vehicleColor")
                                                        BikeShareArrayList.setValue("Lime", forKey: "vehicleTypeImage")
                                                        BikeShareArrayList.setValue("\(walkingInstration)", forKey: "WalkingInstructions")
                                                        BikeShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCaloties")
                                                        BikeShareArrayList.setValue("\(waitingTravel)", forKey: "WaitingTime")
                                                        BikeShareArrayList.setValue("\(TransitTime)", forKey: "WakingStartTime")
                                                        BikeShareArrayList.setValue("Pickup your Lime Vehicle from", forKey: "VehiclePickupLocation")
                                                        BikeShareArrayList.setValue("\(ReveseGeoCodeAddress)", forKey: "vehicleStation")
                                                        BikeShareArrayList.setValue("Get Lime estimated fare:\(totalString)", forKey: "Fare")
                                                        BikeShareArrayList.setValue("\(WalkingAboutandCo2)", forKey: "WalkingAboutandCo2")
                                                        BikeShareArrayList.setValue("\(BikeShareEndTime)", forKey: "BikeShareEndTime")
                                                        BikeShareArrayList.setValue("\(BikeShareTime)", forKey: "vehicleTime")


                                                        BikeShareArrayList.setValue(LimelatFinal!, forKey: "BikeShareSourceLat")
                                                        BikeShareArrayList.setValue(LimelongFinal!, forKey: "BikeShareSourceLng")
                                                        BikeShareArrayList.setValue(Passingdata.endLocation!.lat!, forKey: "BikeShareDestinationLat")
                                                        BikeShareArrayList.setValue(Passingdata.endLocation!.lng!, forKey: "BikeShareDestinationLng")


                                                        BikeShareArrayList.setValue(Passingdata.startLocation!.lat!, forKey: "WalkingSourceLat")
                                                        BikeShareArrayList.setValue(Passingdata.startLocation!.lng!, forKey: "WalkingSourceLng")
                                                        BikeShareArrayList.setValue(LimelatFinal!, forKey: "WalkingDestinationLat")
                                                        BikeShareArrayList.setValue(LimelongFinal!, forKey: "WalkingDestinationLng")

                                                        BikeShareArray.append(BikeShareArrayList as! [String: Any])
                                                        self.BikeShareCount = self.BikeShareCount + 1
                                                        self.BikeRideShareCompletedCheck(BikeShareArrayList: BikeShareArray, Type: "Bike", completion: { (success, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare) in

                                                            if success {


                                                                //  }
                                                                googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                                googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                                googlereponseDirectionList.setValue("\((firstBikeShareAddress))", forKey: "FirstAddress")
                                                                googlereponseDirectionList.setValue("\((SecondDescription))", forKey: "SecondAddress")
                                                                googlereponseDirectionList.setValue("\((waitStartTime))", forKey: "Timing")
                                                                googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                                googlereponseDirectionList.setValue("Passingdata", forKey: "TravelingTime")
                                                                googlereponseDirectionList.setValue("arrivaltime", forKey: "WalkingCalArrival")
                                                                googlereponseDirectionList.setValue("\((WaitTimeWalking))", forKey: "WalkingWaitingTime")

                                                                googlereponseDirectionList.setValue(SourceWalkingLat, forKey: "SourceLat")
                                                                googlereponseDirectionList.setValue(SourceWalkingLng, forKey: "SourceLng")
                                                                googlereponseDirectionList.setValue(DestinationWalkingLat, forKey: "DestinationLat")
                                                                googlereponseDirectionList.setValue(DestinationWalkingLng, forKey: "DestinationLng")


                                                                self.totalTableCellCount = self.totalTableCellCount + 1
                                                                googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                                print(googlereponseDirectionList)
                                                                print("Before Added \(self.ii)")
                                                                self.ii = self.ii + 2

                                                                print("After Added \(self.ii)")

                                                                googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                googlereponseDirectionList.setValue("\(Double(index) + 0.1)", forKey: "Index")
                                                                googlereponseDirectionList.setValue("Bolt4", forKey: "TransitImage")
                                                                googlereponseDirectionList.setValue("", forKey: "FirstAddress")
                                                                googlereponseDirectionList.setValue("", forKey: "SecondAddress")
                                                                googlereponseDirectionList.setValue("\((TimingForBikeShare))", forKey: "Timing")
                                                                googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                googlereponseDirectionList.setValue("BikeShare", forKey: "ModeOFTransit")
                                                               googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                                                                googlereponseDirectionList.setValue("", forKey: "TravelingTime")
                                                                googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
                                                                googlereponseDirectionList.setValue("", forKey: "WalkingWaitingTime")
                                                                googlereponseDirectionList.setValue(BikeShareArray, forKey: "BikeShareArray")

                                                                googlereponseDirectionList.setValue(SourceBikeLat, forKey: "SourceLat")
                                                                googlereponseDirectionList.setValue(SourceBikeLng, forKey: "SourceLng")
                                                                googlereponseDirectionList.setValue(DestinationBikeLat, forKey: "DestinationLat")
                                                                googlereponseDirectionList.setValue(DestinationBikeLng, forKey: "DestinationLng")






                                                                self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                                print(googlereponseDirectionList)
                                                                // self.ii = self.ii + 1
                                                                completion(false, googlereponseDirectionList)




                                                                

                                                            }
                                                            else {
                                                              print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                                   if !self.BikeRideShare {
                                                                                                       self.BikeRideShare = false
                                                                                                   }
                                                                                                    print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                                print ("LimeNotAdded \(Date())")
                                                                print("BeforeBikeShareCount Lime Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                                TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                                                self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                                                print(" afterBikeShareCount Lime Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                                if self.BikeShareCountNeg == 3 {

                                                                    // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                                                    print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                                                    let googlereponseDirectionList: NSMutableDictionary = [:]
                                                                    var waitingTravel = ""
                                                                    let userDefaults = UserDefaults.standard
                                                                    let distanceValue = (Passingdata.distance?.value)!
                                                                    let DurationWalking = (Passingdata.duration?.value)!
                                                                    var waitingtime = ""

                                                                    print((Passingdata.distance?.value)!)
                                                                    var durationString = 0
                                                                    var getstime = ""
                                                                    var uberwaitingtime = ""
                                                                    var arrivaltime: Double = 0.0



                                                                    var TransitTime = ""


                                                                    let walkingmin = (Passingdata.duration?.text)!
                                                                    let walkingmiles = (Passingdata.distance?.text)!
                                                                    let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                                                    let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                                                    let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                                                    // cell.TransitRouteNo.isHidden = true
                                                                    // cell.TransitTime.frame.origin.
                                                                    // let walkingarriaval = walkingdeparttime
                                                                    if index == 0 {

                                                                        let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                        let walkingminepoch = (Passingdata.duration?.value)
                                                                        let walkingmincal = walkingminepoch
                                                                        waitingTravel = "No wait time."
                                                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                                                        TransitTime = "\(getstime)"
                                                                        arrivaltime = walkingstart
                                                                        //cell.TransitTime.text = "\(getstime)"
                                                                    }
                                                                    else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                                                        let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                        let walkingminepoch = (Passingdata.duration?.value)
                                                                        let walkingmincal = walkingminepoch
                                                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                        //cell.TransitTime.text = "\(getstime)"
                                                                        TransitTime = "\(getstime)"
                                                                        arrivaltime = walkingstart

                                                                        waitingTravel = "No wait time."
                                                                    }
                                                                    else {
                                                                        var walkstringtoint = ""
                                                                        //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                                                        let walkingminepoch = (Passingdata.duration?.value)
                                                                        let walkingmincal = walkingminepoch
                                                                        if walkingmin.contains("mins") {
                                                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                                                        }
                                                                        else {
                                                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                                                        }
                                                                        let walkminInt: Int = Int(walkstringtoint)! * 60
                                                                        print(walkminInt)
                                                                        let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                        let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                        TransitTime = "\(getstime)"
                                                                        arrivaltime = walkingend
                                                                        let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                                                        let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                                                        waitingTravel = "Wait time \(waitingtimeResult)"

                                                                    }


                                                                    let FirstAddressGoogle = Passingdata.htmlInstructions
                                                                    let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                                    googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                    googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                                    googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                                    googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                                                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")


                                                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


                                                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                                    print(googlereponseDirectionList)
                                                                    print("Before Added \(self.ii)")
                                                                    self.ii = self.ii + 1
                                                                    print("After Added \(self.ii)")
                                                                    completion(false, googlereponseDirectionList)


                                                                }

                                                            }



                                                        })


                                                    }

                                                    else {
                                                        print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                           if !self.BikeRideShare {
                                                                                               self.BikeRideShare = false
                                                                                           }
                                                                                            print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                        print ("LimeNotAdded \(Date())")
                                                        print("BeforeBikeShareCount Lime Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                        TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                                        self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                                        print(" afterBikeShareCount Lime Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                        if self.BikeShareCountNeg == 3 {

                                                            // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                                            print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                                            let googlereponseDirectionList: NSMutableDictionary = [:]
                                                            var waitingTravel = ""
                                                            let userDefaults = UserDefaults.standard
                                                            let distanceValue = (Passingdata.distance?.value)!
                                                            let DurationWalking = (Passingdata.duration?.value)!
                                                            var waitingtime = ""

                                                            print((Passingdata.distance?.value)!)
                                                            var durationString = 0
                                                            var getstime = ""
                                                            var uberwaitingtime = ""
                                                            var arrivaltime: Double = 0.0



                                                            var TransitTime = ""


                                                            let walkingmin = (Passingdata.duration?.text)!
                                                            let walkingmiles = (Passingdata.distance?.text)!
                                                            let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                                            let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                                            let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                                            // cell.TransitRouteNo.isHidden = true
                                                            // cell.TransitTime.frame.origin.
                                                            // let walkingarriaval = walkingdeparttime
                                                            if index == 0 {

                                                                let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                let walkingminepoch = (Passingdata.duration?.value)
                                                                let walkingmincal = walkingminepoch
                                                                waitingTravel = "No wait time."
                                                                let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                                                TransitTime = "\(getstime)"
                                                                arrivaltime = walkingstart
                                                                //cell.TransitTime.text = "\(getstime)"
                                                            }
                                                            else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                                                let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                let walkingminepoch = (Passingdata.duration?.value)
                                                                let walkingmincal = walkingminepoch
                                                                let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                //cell.TransitTime.text = "\(getstime)"
                                                                TransitTime = "\(getstime)"
                                                                arrivaltime = walkingstart

                                                                waitingTravel = "No wait time."
                                                            }
                                                            else {
                                                                var walkstringtoint = ""
                                                                //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                                                let walkingminepoch = (Passingdata.duration?.value)
                                                                let walkingmincal = walkingminepoch
                                                                if walkingmin.contains("mins") {
                                                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                                                }
                                                                else {
                                                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                                                }
                                                                let walkminInt: Int = Int(walkstringtoint)! * 60
                                                                print(walkminInt)
                                                                let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                TransitTime = "\(getstime)"
                                                                arrivaltime = walkingend
                                                                let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                                                let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                                                waitingTravel = "Wait time \(waitingtimeResult)"

                                                            }


                                                            let FirstAddressGoogle = Passingdata.htmlInstructions
                                                            let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                                            googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                            googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                            googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                            googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                            googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                            googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                            googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                            googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                            googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                            googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                            googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                            googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                            googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                            googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                                            googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                                            googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                                            googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                                            googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                                            googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")


                                                            googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


                                                            self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                            print(googlereponseDirectionList)
                                                            print("Before Added \(self.ii)")
                                                            self.ii = self.ii + 1
                                                            print("After Added \(self.ii)")
                                                            completion(false, googlereponseDirectionList)


                                                        }

                                                    }
                                                }
                                            }
                                        }


                                    }


                                }











                            }

                            else {
                                 print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                   if !self.BikeRideShare {
                                                                       self.BikeRideShare = false
                                                                   }
                                                                    print("After BikeRideShareBool \(self.BikeRideShare) ")
                                print ("LimeNotAdded \(Date())")
                                print("BeforeBikeShareCount Lime Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                print(" afterBikeShareCount Lime Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                if self.BikeShareCountNeg == 3 {

                                    // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                    print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                    let googlereponseDirectionList: NSMutableDictionary = [:]
                                    var waitingTravel = ""
                                    let userDefaults = UserDefaults.standard
                                    let distanceValue = (Passingdata.distance?.value)!
                                    let DurationWalking = (Passingdata.duration?.value)!
                                    var waitingtime = ""

                                    print((Passingdata.distance?.value)!)
                                    var durationString = 0
                                    var getstime = ""
                                    var uberwaitingtime = ""
                                    var arrivaltime: Double = 0.0



                                    var TransitTime = ""


                                    let walkingmin = (Passingdata.duration?.text)!
                                    let walkingmiles = (Passingdata.distance?.text)!
                                    let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                    let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                    let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                    // cell.TransitRouteNo.isHidden = true
                                    // cell.TransitTime.frame.origin.
                                    // let walkingarriaval = walkingdeparttime
                                    if index == 0 {

                                        let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                        let walkingminepoch = (Passingdata.duration?.value)
                                        let walkingmincal = walkingminepoch
                                        waitingTravel = "No wait time."
                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                        TransitTime = "\(getstime)"
                                        arrivaltime = walkingstart
                                        //cell.TransitTime.text = "\(getstime)"
                                    }
                                    else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                        let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                        let walkingminepoch = (Passingdata.duration?.value)
                                        let walkingmincal = walkingminepoch
                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                        //cell.TransitTime.text = "\(getstime)"
                                        TransitTime = "\(getstime)"
                                        arrivaltime = walkingstart

                                        waitingTravel = "No wait time."
                                    }
                                    else {
                                        var walkstringtoint = ""
                                        //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                        let walkingminepoch = (Passingdata.duration?.value)
                                        let walkingmincal = walkingminepoch
                                        if walkingmin.contains("mins") {
                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                        }
                                        else {
                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                        }
                                        let walkminInt: Int = Int(walkstringtoint)! * 60
                                        print(walkminInt)
                                        let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                        let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                        TransitTime = "\(getstime)"
                                        arrivaltime = walkingend
                                        let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                        let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                        waitingTravel = "Wait time \(waitingtimeResult)"

                                    }


                                    let FirstAddressGoogle = Passingdata.htmlInstructions
                                    let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                    googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                    googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                    googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                    googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")
                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")




                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                    print(googlereponseDirectionList)
                                    print("Before Added \(self.ii)")
                                    self.ii = self.ii + 1
                                    print("After Added \(self.ii)")
                                    completion(false, googlereponseDirectionList)


                                }

                            }

                        })
                    let delays = 4.0 + Double(index)
                    DispatchQueue.main.asyncAfter(deadline: .now() + delays, execute: {
                            if SourceDestination.BoltStationDistance != 99999 && SourceDestination.boltListApi != nil && SourceDestination.boltListApi?.count != 0 {
                                print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                       self.BikeRideShare = true
                                                                                                                print("After BikeRideShareBool \(self.BikeRideShare) ")
                                print("\(SourceDestination.BoltStationDistance) BoltStationDistance")
                                var TotalBoltTime: Int?
                                var TotalBoltDistance: Int?
                                let userDefaults = UserDefaults.standard
                                let BoltLat: Double?
                                var BoltLong: Double?
                                var BoltWalkingDistance: Double?
                                var BoltWalkingDuration: Double?
                                var BoltWalkingRoute: MKRoute?
                                var BoltTransitRoute: MKRoute?
                                var BoltTransitDistance: Double?
                                var BoltTransitDuration: Double?
                                var BoltlatFinal: Double?
                                var BoltlongFinal: Double?
                                var TransitTime = ""
                                var arrivaltime: Double = 0.0
                                var waitingTravel = ""

                                let googlereponseDirectionList: NSMutableDictionary = [:]

                                let BikeShareArrayList: NSMutableDictionary = [:]

                                let distanceValue = (Passingdata.distance?.value)!
                                let DurationWalking = (Passingdata.duration?.value)!

                                var BoltCheckBikeCount = 0
                                var BoltdistanceInMeters: Double?
                                var BoltSourceDistance = 0.0
                                var BolttempDistance = 0.0
                                let BoltListApi = SourceDestination.boltListApi
                                let BoltSourcelatlatlong = CLLocation(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!)


                                for BoltBike in (BoltListApi)! {
                                    guard let LatBolt = BoltBike.bike_lat else { return }
                                    guard let LongBolt = BoltBike.bike_lon else { return }
                                    let BoltBikeLat = Double(LatBolt)
                                    let BoltBikeLng = Double(LongBolt)

                                    print("\(BoltBikeLat),\(BoltBikeLng) -> Bolt LatLng")

                                    //  DispatchQueue.main.async {
                                    if BoltCheckBikeCount == 0 {

                                        let Boltdestinationlatlong = CLLocation(latitude: BoltBikeLat!, longitude: BoltBikeLng!)
                                        BoltdistanceInMeters = BoltSourcelatlatlong.distance(from: Boltdestinationlatlong)
                                        BoltSourceDistance = (BoltdistanceInMeters! * 100).rounded() / 100
                                        BoltCheckBikeCount = BoltCheckBikeCount + 1

                                        print("\(BoltSourceDistance)")
                                        BoltlatFinal = BoltBikeLat
                                        BoltlongFinal = BoltBikeLng
                                        print(BoltlongFinal)
                                        print(BoltlatFinal)
                                        print(BoltSourceDistance)




                                    }
                                    else {
                                        self.templat = BoltBikeLat!
                                        self.templong = BoltBikeLng!
                                        let Boltdestinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                                        BoltdistanceInMeters = BoltSourcelatlatlong.distance(from: Boltdestinationlatlong)
                                        BolttempDistance = (BoltdistanceInMeters! * 100).rounded() / 100
                                        print("\(BolttempDistance) < \(BoltSourceDistance)")
                                        if BolttempDistance < BoltSourceDistance {

                                            BoltSourceDistance = BolttempDistance
                                            print(BolttempDistance)
                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                            BoltlatFinal = BoltBikeLat
                                            BoltlongFinal = BoltBikeLng

                                            print(BoltlongFinal)
                                            print(BoltlatFinal)
                                            print(BoltSourceDistance)

                                        }
                                        else {
                                            BoltCheckBikeCount = BoltCheckBikeCount + 1
                                        }
                                    }


                                }

                                if BoltlatFinal != nil {
                                    let BoltWalkingrequest = MKDirections.Request()
                                    BoltWalkingrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!), addressDictionary: nil))

                                    BoltWalkingrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: BoltlatFinal!, longitude: BoltlongFinal!), addressDictionary: nil))
                                    BoltWalkingrequest.requestsAlternateRoutes = true

                                    BoltWalkingrequest.transportType = .walking

                                    let BoltWalkingdirections = MKDirections(request: BoltWalkingrequest)

                                    BoltWalkingdirections.calculate { [unowned self] BoltWalkingresponse, error in
                                        guard let unwrappedResponse = BoltWalkingresponse else { return }

                                        BoltWalkingRoute = unwrappedResponse.routes[0]
                                        BoltWalkingDistance = BoltWalkingRoute?.distance
                                        BoltWalkingDuration = (BoltWalkingRoute?.expectedTravelTime)



                                        var BoltWalkingMiles = ""
                                        if Int(BoltWalkingDistance!) > 243 {

                                            let CalculateDistance = (Double(BoltWalkingDuration!) / 1609.344)
                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                            BoltWalkingMiles = "\(CalculateDistanceTest) miles"
                                        }
                                        else {
                                            let CalculateDistance = (Double(BoltWalkingDuration!) * 3.281)
                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                            BoltWalkingMiles = "\(CalculateDistanceTest) ft"

                                        }



                                        let caloriesburn = (BoltWalkingDistance! * 1.25) / 20
                                        let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                        let BoltWalkingTimeMin = self.convertTime(miliseconds: Int(BoltWalkingDuration! * 1000))




                                        let WalkingAboutandCaloties = "About \(BoltWalkingTimeMin), \(BoltWalkingMiles) (\(caloriesburnint) Calories)"


                                        let walkingInstration = "Walk to \((BoltWalkingRoute?.name)!)"

                                        print("\((walkingInstration)) Waling to Bolt")

                                        let walkingmin = (Passingdata.duration?.text)!








                                        let BoltTransitrequest = MKDirections.Request()
                                        BoltTransitrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: BoltlatFinal!, longitude: BoltlongFinal!), addressDictionary: nil))
                                        BoltTransitrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.endLocation!.lat!, longitude: Passingdata.endLocation!.lng!), addressDictionary: nil))
                                        BoltTransitrequest.requestsAlternateRoutes = true

                                        BoltTransitrequest.transportType = .automobile

                                        print(BoltTransitrequest.source?.name)

                                        let BoltStationAddress = BoltTransitrequest.source?.name
                                        let BoltTransitirections = MKDirections(request: BoltTransitrequest)

                                        BoltTransitirections.calculate { [unowned self] BoltTransitresponse, error in
                                            guard let BoltTransitResponse = BoltTransitresponse else { return }

                                            BoltTransitRoute = BoltTransitResponse.routes.first
                                            var BikeShareTime = ""
                                            var BikeShareEndTime = ""
                                            let BoltVehicleAddress = BoltTransitRoute?.steps
                                            BoltTransitDistance = BoltTransitRoute?.distance
                                            BoltTransitDuration = BoltTransitRoute!.expectedTravelTime



                                            if index == 0 {

                                                let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                // let walkingminepoch = (Passingdata.duration?.value)
                                                let walkingmincal = BoltWalkingDuration! + BoltTransitDuration!
                                                waitingTravel = "No wait time."
                                                let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(walkingmincal), addsub: "sub")
                                                TransitTime = "\(getstime)"
                                                arrivaltime = walkingstart
                                                BikeShareTime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(BoltTransitDuration!), addsub: "sub")
                                                BikeShareEndTime = "0"
                                            }

                                            else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                                let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                let walkingminepoch = (Passingdata.duration?.value)
                                                let walkingmincal = BoltWalkingDuration
                                                let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(walkingmincal!), addsub: "add")
                                                //cell.TransitTime.text = "\(getstime)"
                                                TransitTime = "\(getstime)"
                                                arrivaltime = walkingstart

                                                waitingTravel = "No wait time."
                                                BikeShareTime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: Int(BoltTransitDuration!), addsub: "add")
                                                BikeShareEndTime = "1"
                                            }
                                            else {

                                                var walkstringtoint = ""
                                                //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                                let walkingminepoch = (Passingdata.duration?.value)
                                                let walkingmincal = BoltWalkingDuration
                                                if walkingmin.contains("mins") {
                                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                                }
                                                else {
                                                    walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                                }
                                                let walkminInt: Int = Int(walkstringtoint)! * 60
                                                print(walkminInt)
                                                let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: Int(walkingmincal!), addsub: "add")
                                                TransitTime = "\(getstime)"
                                                arrivaltime = walkingend
                                                let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                                let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                                waitingTravel = "Wait time \(waitingtimeResult)"

                                                BikeShareTime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: Int(BoltTransitDuration!) + Int(BoltWalkingDuration!), addsub: "add")
                                                BikeShareEndTime = "2"

                                            }




                                            print("\(BoltWalkingDistance) BoltWalkingDistance")
                                            print("\(BoltWalkingDuration) BoltWalkingDuration")
                                            print("\(BoltTransitDistance) BoltTransitDistance")
                                            print("\(BoltTransitDuration) BoltTransitDuration")

                                            TotalBoltTime = Int(BoltWalkingDuration!) + Int(BoltTransitDuration!)
                                            TotalBoltDistance = Int(BoltWalkingDistance!) + Int(BoltTransitDistance!)

                                            print("\(TotalBoltTime) TotalBoltTime")
                                            print("\(TotalBoltDistance) TotalBoltDistance")
                                            print("\(distanceValue) distanceValue")
                                            print("\(DurationWalking) DurationWalking")
                                            print("\(BoltlatFinal!),\(BoltlongFinal!)")


                                            let durationStringADD = BoltTransitDuration
                                            let sectominCalWithFare: Double = (Double(durationStringADD! / 60).rounded(.toNearestOrAwayFromZero))
                                            print(((sectominCalWithFare * 0.15) + 1))
                                            let BirdBike_fare_Cal = (sectominCalWithFare * 0.35)

                                            let totalvalue = BirdBike_fare_Cal.rounded(.toNearestOrAwayFromZero)
                                            let totalString = String(format: "$%.2f", totalvalue)
                                           // let valueResultss = self.LimedataBikeResponse.result.value



                                            // self.TotalMinTripForTravel = self.TotalMinTripForTravel + (TotalBoltTime!)
                                            let BoltTransitTimeMin = self.convertTime(miliseconds: Int(BoltTransitDuration! * 1000))

                                            var BoltTransitMiles = ""

                                            if Int(BoltTransitDistance!) > 243 {

                                                let CalculateDistance = (Double(BoltTransitDistance!) / 1609.344)
                                                let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                                BoltTransitMiles = "\(CalculateDistanceTest) miles"
                                            }
                                            else {
                                                let CalculateDistance = (Double(BoltTransitDistance!) * 3.281)
                                                let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                                BoltTransitMiles = "\(CalculateDistanceTest) ft"

                                            }
                                            var CO2Emmision = (Double(BoltTransitDistance!) * 0.0000419426)
                                            let test = String(format: "%.2f", CO2Emmision)
                                            let textcount = test.count
                                            print(CO2Emmision)
                                            let totalcount = textcount + 7
                                            CO2Emmision = self.getAmountRounded(CO2Emmision, withRoundOff: Int(5))
                                            let font: UIFont? = UIFont(name: "Helvetica", size: 12)
                                            let fontSuper: UIFont? = UIFont(name: "Helvetica", size: 8)

                                            let BoltTransitco2: NSMutableAttributedString = NSMutableAttributedString(string: "\(test) lbs CO2", attributes: [.font: font!])
                                            BoltTransitco2.setAttributes([.font: fontSuper!, .baselineOffset: -8], range: NSRange(location: totalcount, length: 1))

                                            // cell.dashboardTitle.attributedText = attString
                                            print("\(BoltTransitco2) Isaaaaaaa")



                                            let WalkingAboutandCo2 = "About \(BoltTransitTimeMin), \(BoltTransitMiles) \(test) lbs CO2"




                                            let location = CLLocation(latitude: BoltlatFinal!, longitude: BoltlongFinal!)

                                            var ReveseGeoCodeAddress = "";
                                            location.geocode { placemark, error in
                                                if let error = error as? CLError {
                                                    print("CLError:", error)
                                                    return
                                                } else if let placemark = placemark?.first {
                                                    // you should always update your UI in the main thread
                                                    DispatchQueue.main.async {
                                                        //  update UI here
                                                        ReveseGeoCodeAddress = "\((placemark.name)!), \((placemark.locality)!), \((placemark.administrativeArea)!), \((placemark.postalCode)!), \((placemark.isoCountryCode)!)"
                                                        print("name:", placemark.name ?? "unknown")

                                                        print("address1:", placemark.thoroughfare ?? "unknown")
                                                        print("address2:", placemark.subThoroughfare ?? "unknown")
                                                        print("neighborhood:", placemark.subLocality ?? "unknown")
                                                        print("city:", placemark.locality ?? "unknown")

                                                        print("state:", placemark.administrativeArea ?? "unknown")
                                                        print("subAdministrativeArea:", placemark.subAdministrativeArea ?? "unknown")
                                                        print("zip code:", placemark.postalCode ?? "unknown")
                                                        print("country:", placemark.country ?? "unknown", terminator: "\n\n")

                                                        print("isoCountryCode:", placemark.isoCountryCode ?? "unknown")
                                                        print("region identifier:", placemark.region?.identifier ?? "unknown")

                                                        print("timezone:", placemark.timeZone ?? "unknown", terminator: "\n\n")










                                                        if Int(BoltSourceDistance) < Int(500) && Int(distanceValue) > Int(BoltSourceDistance) {
                                                           print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                                                   self.BikeRideShare = true
                                                                                                                                            print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                            //(self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: Passingdata.startLocation!.lat!, SourceLng: Passingdata.startLocation!.lng!, DestinationLat: BoltlatFinal!, DestinationLng: BoltlongFinal!, Type: "Walking")
                                                            //  (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: BoltlatFinal!, SourceLng: BoltlongFinal!, DestinationLat: Passingdata.endLocation!.lat!, DestinationLng: Passingdata.endLocation!.lng!, Type: "Bolt")
                                                            BikeShareArrayList.setValue(BirdBike_fare_Cal, forKey: "vehiclefare")
                                                            BikeShareArrayList.setValue(TotalBoltTime, forKey: "TotalTime")
                                                            BikeShareArrayList.setValue("Bolt", forKey: "vehicleType")
                                                            BikeShareArrayList.setValue("#F7DD4B", forKey: "vehicleColor")
                                                            BikeShareArrayList.setValue("Bolt", forKey: "vehicleTypeImage")
                                                            BikeShareArrayList.setValue("\(walkingInstration)", forKey: "WalkingInstructions")
                                                            BikeShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCaloties")
                                                            BikeShareArrayList.setValue("\(waitingTravel)", forKey: "WaitingTime")
                                                            BikeShareArrayList.setValue("\(TransitTime)", forKey: "WakingStartTime")

                                                            BikeShareArrayList.setValue("Pickup your Bolt Vehicle from", forKey: "VehiclePickupLocation")
                                                            BikeShareArrayList.setValue("\(ReveseGeoCodeAddress)", forKey: "vehicleStation")
                                                            BikeShareArrayList.setValue("Get Bolt estimated fare:\(totalString)", forKey: "Fare")
                                                            BikeShareArrayList.setValue("\(WalkingAboutandCo2)", forKey: "WalkingAboutandCo2")
                                                            BikeShareArrayList.setValue("\(BikeShareTime)", forKey: "vehicleTime")
                                                            BikeShareArrayList.setValue("\(BikeShareEndTime)", forKey: "BikeShareEndTime")

                                                            BikeShareArrayList.setValue(BoltlatFinal!, forKey: "BikeShareSourceLat")
                                                            BikeShareArrayList.setValue(BoltlongFinal!, forKey: "BikeShareSourceLng")
                                                            BikeShareArrayList.setValue(Passingdata.endLocation!.lat!, forKey: "BikeShareDestinationLat")
                                                            BikeShareArrayList.setValue(Passingdata.endLocation!.lng!, forKey: "BikeShareDestinationLng")


                                                            BikeShareArrayList.setValue(Passingdata.startLocation!.lat!, forKey: "WalkingSourceLat")
                                                            BikeShareArrayList.setValue(Passingdata.startLocation!.lng!, forKey: "WalkingSourceLng")
                                                            BikeShareArrayList.setValue(BoltlatFinal!, forKey: "WalkingDestinationLat")
                                                            BikeShareArrayList.setValue(BoltlongFinal!, forKey: "WalkingDestinationLng")

                                                            BikeShareArray.append(BikeShareArrayList as! [String: Any])

                                                            self.BikeShareCount = self.BikeShareCount + 1
                                                            self.BikeRideShareCompletedCheck(BikeShareArrayList: BikeShareArray, Type: "Bike", completion: { (success, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare) in

                                                                if success {
                                                                    print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                                                           self.BikeRideShare = true
                                                                                                                                                    print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                                    googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                                    googlereponseDirectionList.setValue("\((firstBikeShareAddress))", forKey: "FirstAddress")
                                                                    googlereponseDirectionList.setValue("\((SecondDescription))", forKey: "SecondAddress")
                                                                    googlereponseDirectionList.setValue("\((waitStartTime))", forKey: "Timing")
                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                    googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                                    googlereponseDirectionList.setValue("Passingdata", forKey: "TravelingTime")
                                                                    googlereponseDirectionList.setValue("arrivaltime", forKey: "WalkingCalArrival")
                                                                    googlereponseDirectionList.setValue("\((WaitTimeWalking))", forKey: "WalkingWaitingTime")

                                                                    googlereponseDirectionList.setValue(SourceWalkingLat, forKey: "SourceLat")
                                                                    googlereponseDirectionList.setValue(SourceWalkingLng, forKey: "SourceLng")
                                                                    googlereponseDirectionList.setValue(DestinationWalkingLat, forKey: "DestinationLat")
                                                                    googlereponseDirectionList.setValue(DestinationWalkingLng, forKey: "DestinationLng")

                                                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")

                                                                    self.totalTableCellCount = self.totalTableCellCount + 1
                                                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                                    print(googlereponseDirectionList)
                                                                    print("Before Added \(self.ii)")
                                                                    self.ii = self.ii + 2

                                                                    print("After Added \(self.ii)")

                                                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                    googlereponseDirectionList.setValue("\(Double(index) + 0.1)", forKey: "Index")
                                                                    googlereponseDirectionList.setValue("Bolt4", forKey: "TransitImage")
                                                                    googlereponseDirectionList.setValue("", forKey: "FirstAddress")
                                                                    googlereponseDirectionList.setValue("", forKey: "SecondAddress")
                                                                    googlereponseDirectionList.setValue("\((TimingForBikeShare))", forKey: "Timing")
                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                    googlereponseDirectionList.setValue("BikeShare", forKey: "ModeOFTransit")
                                                                    googlereponseDirectionList.setValue("", forKey: "TravelingTime")
                                                                    googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
                                                                    googlereponseDirectionList.setValue("", forKey: "WalkingWaitingTime")
                                                                    googlereponseDirectionList.setValue(BikeShareArray, forKey: "BikeShareArray")

                                                                    googlereponseDirectionList.setValue(SourceBikeLat, forKey: "SourceLat")
                                                                    googlereponseDirectionList.setValue(SourceBikeLng, forKey: "SourceLng")
                                                                    googlereponseDirectionList.setValue(DestinationBikeLat, forKey: "DestinationLat")
                                                                    googlereponseDirectionList.setValue(DestinationBikeLng, forKey: "DestinationLng")


                                                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")




                                                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                                    print(googlereponseDirectionList)
                                                                    // self.ii = self.ii + 1
                                                                    completion(false, googlereponseDirectionList)





                                                                }

                                                                else {
                                                                 print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                                       if !self.BikeRideShare {
                                                                                                           self.BikeRideShare = false
                                                                                                       }
                                                                                                        print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                                    print ("BoltNotAdded \(Date())")
                                                                    print("BeforeBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")

                                                                    TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                                                    self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                                                    print(" afterBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                                    if self.BikeShareCountNeg == 3 {

                                                                        // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                                                        print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                                                        let googlereponseDirectionList: NSMutableDictionary = [:]
                                                                        var waitingTravel = ""
                                                                        let userDefaults = UserDefaults.standard
                                                                        let distanceValue = (Passingdata.distance?.value)!
                                                                        let DurationWalking = (Passingdata.duration?.value)!
                                                                        var waitingtime = ""

                                                                        print((Passingdata.distance?.value)!)
                                                                        var durationString = 0
                                                                        var getstime = ""
                                                                        var uberwaitingtime = ""
                                                                        var arrivaltime: Double = 0.0



                                                                        var TransitTime = ""


                                                                        let walkingmin = (Passingdata.duration?.text)!
                                                                        let walkingmiles = (Passingdata.distance?.text)!
                                                                        let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                                                        let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                                                        let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                                                        // cell.TransitRouteNo.isHidden = true
                                                                        // cell.TransitTime.frame.origin.
                                                                        // let walkingarriaval = walkingdeparttime
                                                                        if index == 0 {

                                                                            let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                            let walkingminepoch = (Passingdata.duration?.value)
                                                                            let walkingmincal = walkingminepoch
                                                                            waitingTravel = "No wait time."
                                                                            let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                                                            TransitTime = "\(getstime)"
                                                                            arrivaltime = walkingstart
                                                                            //cell.TransitTime.text = "\(getstime)"
                                                                        }
                                                                        else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                                                            let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                            let walkingminepoch = (Passingdata.duration?.value)
                                                                            let walkingmincal = walkingminepoch
                                                                            let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                            //cell.TransitTime.text = "\(getstime)"
                                                                            TransitTime = "\(getstime)"
                                                                            arrivaltime = walkingstart

                                                                            waitingTravel = "No wait time."
                                                                        }
                                                                        else {
                                                                            var walkstringtoint = ""
                                                                            //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                                                            let walkingminepoch = (Passingdata.duration?.value)
                                                                            let walkingmincal = walkingminepoch
                                                                            if walkingmin.contains("mins") {
                                                                                walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                                                            }
                                                                            else {
                                                                                walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                                                            }
                                                                            let walkminInt: Int = Int(walkstringtoint)! * 60
                                                                            print(walkminInt)
                                                                            let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                            let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                            TransitTime = "\(getstime)"
                                                                            arrivaltime = walkingend
                                                                            let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                                                            let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                                                            waitingTravel = "Wait time \(waitingtimeResult)"

                                                                        }


                                                                        let FirstAddressGoogle = Passingdata.htmlInstructions
                                                                        let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                                                        googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                        googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                        googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                        googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                                        googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                                        googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                                        googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                                        googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                                        googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                        googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                        googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                        googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                                        googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                                        googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                                                        googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                                                        googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                                                        googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                                                        googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                                                        googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")


                                                                        googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


                                                                        self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                                        print(googlereponseDirectionList)
                                                                        print("Before Added \(self.ii)")
                                                                        self.ii = self.ii + 1
                                                                        print("After Added \(self.ii)")
                                                                        completion(false, googlereponseDirectionList)


                                                                    }

                                                                }


                                                            })


                                                        }



                                                        else {
                                                       print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                                               if !self.BikeRideShare {
                                                                                                   self.BikeRideShare = false
                                                                                               }
                                                                                                print("After BikeRideShareBool \(self.BikeRideShare) ")
                                                            print ("BoltNotAdded \(Date())")
                                                            print("BeforeBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                            TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                                            self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                                            print(" afterBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                                            if self.BikeShareCountNeg == 3 {

                                                                // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                                                print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                                                let googlereponseDirectionList: NSMutableDictionary = [:]
                                                                var waitingTravel = ""
                                                                let userDefaults = UserDefaults.standard
                                                                let distanceValue = (Passingdata.distance?.value)!
                                                                let DurationWalking = (Passingdata.duration?.value)!
                                                                var waitingtime = ""

                                                                print((Passingdata.distance?.value)!)
                                                                var durationString = 0
                                                                var getstime = ""
                                                                var uberwaitingtime = ""
                                                                var arrivaltime: Double = 0.0



                                                                var TransitTime = ""


                                                                let walkingmin = (Passingdata.duration?.text)!
                                                                let walkingmiles = (Passingdata.distance?.text)!
                                                                let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                                                let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                                                let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                                                // cell.TransitRouteNo.isHidden = true
                                                                // cell.TransitTime.frame.origin.
                                                                // let walkingarriaval = walkingdeparttime
                                                                if index == 0 {

                                                                    let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                    let walkingminepoch = (Passingdata.duration?.value)
                                                                    let walkingmincal = walkingminepoch
                                                                    waitingTravel = "No wait time."
                                                                    let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                                                    TransitTime = "\(getstime)"
                                                                    arrivaltime = walkingstart
                                                                    //cell.TransitTime.text = "\(getstime)"
                                                                }
                                                                else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                                                    let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                    let walkingminepoch = (Passingdata.duration?.value)
                                                                    let walkingmincal = walkingminepoch
                                                                    let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                    //cell.TransitTime.text = "\(getstime)"
                                                                    TransitTime = "\(getstime)"
                                                                    arrivaltime = walkingstart

                                                                    waitingTravel = "No wait time."
                                                                }
                                                                else {
                                                                    var walkstringtoint = ""
                                                                    //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                                                    let walkingminepoch = (Passingdata.duration?.value)
                                                                    let walkingmincal = walkingminepoch
                                                                    if walkingmin.contains("mins") {
                                                                        walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                                                    }
                                                                    else {
                                                                        walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                                                    }
                                                                    let walkminInt: Int = Int(walkstringtoint)! * 60
                                                                    print(walkminInt)
                                                                    let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                    let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                                                    TransitTime = "\(getstime)"
                                                                    arrivaltime = walkingend
                                                                    let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                                                    let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                                                    waitingTravel = "Wait time \(waitingtimeResult)"

                                                                }


                                                                let FirstAddressGoogle = Passingdata.htmlInstructions
                                                                let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                                                googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                                googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                                                googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                                                googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                                                googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                                                googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                                                googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                                                googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                                                googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                                                googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                                                googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                                                googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                                                googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")

                                                                googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")



                                                                self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                                                print(googlereponseDirectionList)
                                                                print("Before Added \(self.ii)")
                                                                self.ii = self.ii + 1
                                                                print("After Added \(self.ii)")
                                                                completion(false, googlereponseDirectionList)


                                                            }

                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                                else {
                                    print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                    if !self.BikeRideShare {
                                        self.BikeRideShare = false
                                    }
                                     print("After BikeRideShareBool \(self.BikeRideShare) ")
                                    print ("BoltNotAdded \(Date())")
                                    print("BeforeBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                    TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                    self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                    print(" afterBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                    if self.BikeShareCountNeg == 3 {

                                        // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                        print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                        let googlereponseDirectionList: NSMutableDictionary = [:]
                                        var waitingTravel = ""
                                        let userDefaults = UserDefaults.standard
                                        let distanceValue = (Passingdata.distance?.value)!
                                        let DurationWalking = (Passingdata.duration?.value)!
                                        var waitingtime = ""

                                        print((Passingdata.distance?.value)!)
                                        var durationString = 0
                                        var getstime = ""
                                        var uberwaitingtime = ""
                                        var arrivaltime: Double = 0.0



                                        var TransitTime = ""


                                        let walkingmin = (Passingdata.duration?.text)!
                                        let walkingmiles = (Passingdata.distance?.text)!
                                        let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                        let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                        let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                        // cell.TransitRouteNo.isHidden = true
                                        // cell.TransitTime.frame.origin.
                                        // let walkingarriaval = walkingdeparttime
                                        if index == 0 {

                                            let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                            let walkingminepoch = (Passingdata.duration?.value)
                                            let walkingmincal = walkingminepoch
                                            waitingTravel = "No wait time."
                                            let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                            TransitTime = "\(getstime)"
                                            arrivaltime = walkingstart
                                            //cell.TransitTime.text = "\(getstime)"
                                        }
                                        else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                            let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                            let walkingminepoch = (Passingdata.duration?.value)
                                            let walkingmincal = walkingminepoch
                                            let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                            //cell.TransitTime.text = "\(getstime)"
                                            TransitTime = "\(getstime)"
                                            arrivaltime = walkingstart

                                            waitingTravel = "No wait time."
                                        }
                                        else {
                                            var walkstringtoint = ""
                                            //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                            let walkingminepoch = (Passingdata.duration?.value)
                                            let walkingmincal = walkingminepoch
                                            if walkingmin.contains("mins") {
                                                walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                            }
                                            else {
                                                walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                            }
                                            let walkminInt: Int = Int(walkstringtoint)! * 60
                                            print(walkminInt)
                                            let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                            let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                            TransitTime = "\(getstime)"
                                            arrivaltime = walkingend
                                            let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                            let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                            waitingTravel = "Wait time \(waitingtimeResult)"

                                        }


                                        let FirstAddressGoogle = Passingdata.htmlInstructions
                                        let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                        googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                        googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                        googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                        googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                        googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                        googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                        googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                        googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                        googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                        googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                        googlereponseDirectionList.setValue("", forKey: "NextStop")
                                        googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                        googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                        googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                        googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                        googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                        googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                        googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                        googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")

                                        googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")



                                        self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                        print(googlereponseDirectionList)
                                        print("Before Added \(self.ii)")
                                        self.ii = self.ii + 1
                                        print("After Added \(self.ii)")
                                        completion(false, googlereponseDirectionList)


                                    }

                                }
                            }

                            else {
                              print("Before BikeRideShareBool \(self.BikeRideShare) ")
                                                                   if !self.BikeRideShare {
                                                                       self.BikeRideShare = false
                                                                   }
                                                                    print("After BikeRideShareBool \(self.BikeRideShare) ")
                                print ("BoltNotAdded \(Date())")
                                print("BeforeBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                TempNegtiveBikeShareCount = TempNegtiveBikeShareCount + 1
                                self.BikeShareCountNeg = TempNegtiveBikeShareCount

                                print(" afterBikeShareCount Bolt Else \(self.BikeShareCount) \(self.BikeShareCountNeg)")
                                if self.BikeShareCountNeg == 3 {

                                    // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                                    print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                                    let googlereponseDirectionList: NSMutableDictionary = [:]
                                    var waitingTravel = ""
                                    let userDefaults = UserDefaults.standard
                                    let distanceValue = (Passingdata.distance?.value)!
                                    let DurationWalking = (Passingdata.duration?.value)!
                                    var waitingtime = ""

                                    print((Passingdata.distance?.value)!)
                                    var durationString = 0
                                    var getstime = ""
                                    var uberwaitingtime = ""
                                    var arrivaltime: Double = 0.0



                                    var TransitTime = ""


                                    let walkingmin = (Passingdata.duration?.text)!
                                    let walkingmiles = (Passingdata.distance?.text)!
                                    let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                                    let caloriesburn = (walkingdistancevalue * 1.25) / 20
                                    let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                                    // cell.TransitRouteNo.isHidden = true
                                    // cell.TransitTime.frame.origin.
                                    // let walkingarriaval = walkingdeparttime
                                    if index == 0 {

                                        let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                        let walkingminepoch = (Passingdata.duration?.value)
                                        let walkingmincal = walkingminepoch
                                        waitingTravel = "No wait time."
                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                                        TransitTime = "\(getstime)"
                                        arrivaltime = walkingstart
                                        //cell.TransitTime.text = "\(getstime)"
                                    }
                                    else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                                        let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                        let walkingminepoch = (Passingdata.duration?.value)
                                        let walkingmincal = walkingminepoch
                                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                        //cell.TransitTime.text = "\(getstime)"
                                        TransitTime = "\(getstime)"
                                        arrivaltime = walkingstart

                                        waitingTravel = "No wait time."
                                    }
                                    else {
                                        var walkstringtoint = ""
                                        //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                                        let walkingminepoch = (Passingdata.duration?.value)
                                        let walkingmincal = walkingminepoch
                                        if walkingmin.contains("mins") {
                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                                        }
                                        else {
                                            walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                                        }
                                        let walkminInt: Int = Int(walkstringtoint)! * 60
                                        print(walkminInt)
                                        let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                        let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                                        TransitTime = "\(getstime)"
                                        arrivaltime = walkingend
                                        let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                                        let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                                        waitingTravel = "Wait time \(waitingtimeResult)"

                                    }


                                    let FirstAddressGoogle = Passingdata.htmlInstructions
                                    let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                    googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                    googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                                    googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                                    googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")


                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")


                                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                                    print(googlereponseDirectionList)
                                    print("Before Added \(self.ii)")
                                    self.ii = self.ii + 1
                                    print("After Added \(self.ii)")
                                    completion(false, googlereponseDirectionList)


                                }

                            }


                        })

                }
                else if userDefaultsForBike.bool(forKey: "Cabshare") && Passingdata.distance!.value! > 805 {
                    var UberFar = "Get Uber Estimate Fare : $6 - $8"
                    comFunc.getLyftAPIFirst(SourceLat: Passingdata.startLocation!.lat!, SourceLong: Passingdata.startLocation!.lng!, DestinationLat: Passingdata.endLocation!.lat!, DestinationLong:Passingdata.endLocation!.lng!) { (MinPrice, MaxPrice, TravelMin, travelMile, Status) in
                                                      if Status {
                                                          //
                                                          var overallDistance = 0.0
                                                          let minutes = TravelMin / 60
                                                          let LyftTravelTime = minutes ;
                                                          print(LyftTravelTime)
                    overallDistance = self.distance(lat1:  Passingdata.startLocation!.lat!, lon1: Passingdata.startLocation!.lng!, lat2: Passingdata.endLocation!.lat!, lon2: Passingdata.endLocation!.lng!, unit: "M")
                                                          print(overallDistance)
                                                          var calculateMiles = travelMile / 1609.344
                                                          // calculateMiles = 2.3
                                                          if calculateMiles == 1000
                                                              {
                                                              calculateMiles = overallDistance
                                                          }
                                                          var estimatetime = Double(LyftTravelTime)
                                                          if TravelMin == 300
                                                              {
                                                              estimatetime = calculateMiles / 20
                                                          }
                                                          var timeCalculation = estimatetime * 0.28
                                                          print(timeCalculation)
                                                          let calculateoverallMiles = calculateMiles * 0.80
                                                          print(calculateoverallMiles)
                                                          var overallTotal = calculateoverallMiles + 2.30 + timeCalculation
                                                          print(overallTotal)
                                                          overallTotal = overallTotal.rounded(.toNearestOrAwayFromZero)
                                                          // var minimumfareFloat  = NSString()
                                                          var minimumfare = overallTotal - 1
                                                          if minimumfare <= 5.79
                                                              {
                                                              minimumfare = 5.80
                                                              overallTotal = minimumfare
                                                          }
                                                          let minimumfareFloat = Int(minimumfare)
                                                          let maximumfareFloat = overallTotal + 1
                                                          // var maxString  = NSString()
                                                          //let maxString = Int(maximumfareFloat)
                                                          let minString = minimumfare.rounded(.toNearestOrAwayFromZero)
                                                      let maxString = maximumfareFloat.rounded(.toNearestOrAwayFromZero)
                                UberFar = "Get Uber Estimate Fare : $\(minString) - $\(maxString)"
                                                          //firstLabel.text = "$\(minString) - $\(maxString)"
                                                          print(MinPrice)
                                                          print(MaxPrice)
                                                        var CabSharecount = 0
                                                        print("************* RideShare \(Date())")
                                                        print("Before Zero RideShareCount \(self.RideShareCount)")
                                                        self.RideShareCount = 0
                                                        print("After Zero RideShareCount \(self.RideShareCount)")
                                                        var RideShareArray = [[String: Any]]()
                                                        let googlereponseDirectionList: NSMutableDictionary = [:]
                                                        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                                                                if userDefaultsForBike.bool(forKey: "UberOn")
                                                                    {
                                                                    self.RideShareBool = true
                                                                    print("************* Uber \(Date())")
                                                                    let RideShareArrayList: NSMutableDictionary = [:]
                                                                    let waitingTravel = ""
                                                                    let TransitTime = ""
                                                                    let ReveseGeoCodeAddress = ""
                                                                    let WalkingAboutandCo2 = ""
                                                                    let BikeShareTime = ""
                                                                    let BikeShareEndTime = ""
                                                                    let aString = Passingdata.htmlInstructions
                                                                    self.isUberAdded = true
                                                                    let walkingInstration = aString!.replacingOccurrences(of: "Walk to", with: "")
                                                                    let Riderequest = MKDirections.Request()
                                                                    Riderequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!), addressDictionary: nil))
                                                                    Riderequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.endLocation!.lat!, longitude: Passingdata.endLocation!.lng!), addressDictionary: nil))
                                                                    Riderequest.requestsAlternateRoutes = true
                                                                    Riderequest.transportType = .automobile
                                                                    let Ridedirections = MKDirections(request: Riderequest)
                                                                    Ridedirections.calculate { [unowned self] response, error in
                                                                        guard let RideResponse = response else { return }
                                                                        //for Rideroute in RideResponse.routes {
                                                                        let UberRoute = RideResponse.routes.first!
                                                                        let UberDistance = UberRoute.distance
                                                                        let UberDuration = (UberRoute.expectedTravelTime)
                                                                        let UberDutationTime = self.convertTime(miliseconds: Int(UberDuration * 1000))
                                                                        var UberMiles = ""
                                                                        if Int(UberDistance) > 243 {
                                                                            let CalculateDistance = (Double(UberDistance) / 1609.344)
                                                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                                                            UberMiles = "\(CalculateDistanceTest) miles"
                                                                        }
                                                                        else {
                                                                            let CalculateDistance = (Double(UberDistance) * 3.281)
                                                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                                                            UberMiles = "\(CalculateDistanceTest) ft"
                                                                        }
                                                                        let Waitingtime = Int.random(in: 1..<6)
                                                                        let WalkingAboutandCaloties = "About \(UberDutationTime), \(UberMiles), \(Waitingtime) min Wait time"
                                                                        var uberTime = ""
                                                                      
                                                                        
                                                                        
                                                                        if index == 0 {
                                                                            let uberstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                            uberTime = self.dateconvert(Datevalue: uberstart, WalkingTimeinterval: Int(UberDuration) + Waitingtime, addsub: "sub")
                                                                        }
                                                                        else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {
                                                                            let uberstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                            uberTime = self.dateconvert(Datevalue: uberstart, WalkingTimeinterval: Int(UberDuration) + Waitingtime, addsub: "add")
                                                                          //  UberFar = "Get Uber Estimate Fare : $6 - $8"
                                                                        }
                                                                        else {
                                                                            let uberstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                            uberTime = self.self.dateconvert(Datevalue: uberstart, WalkingTimeinterval: Int(UberDuration) + Waitingtime, addsub: "add")
                                                                          //  UberFar = "Get Uber Estimate Fare : $6 - $8"
                                                                        }
                                                                        RideShareArrayList.setValue(6.0, forKey: "vehiclefare")
                                                                        RideShareArrayList.setValue("Uber", forKey: "vehicleType")
                                                                        RideShareArrayList.setValue(Int(UberDuration) + Waitingtime, forKey: "TotalTime")
                                                                        RideShareArrayList.setValue("#000000", forKey: "vehicleColor")
                                                                        RideShareArrayList.setValue("Uber-1", forKey: "vehicleTypeImage")
                                                                        RideShareArrayList.setValue("", forKey: "WalkingInstructions")
                                                                        RideShareArrayList.setValue("", forKey: "WalkingAboutandCaloties")
                                                                        RideShareArrayList.setValue("\(waitingTravel)", forKey: "WaitingTime")
                                                                        RideShareArrayList.setValue("\(TransitTime)", forKey: "WakingStartTime")
                                                                        RideShareArrayList.setValue("Board your Uber to", forKey: "VehiclePickupLocation")
                                                                        RideShareArrayList.setValue("\(walkingInstration)", forKey: "vehicleStation")
                                                                        RideShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCo2")
                                                                        RideShareArrayList.setValue("\(uberTime)", forKey: "vehicleTime")
                                                                        RideShareArrayList.setValue("", forKey: "BikeShareEndTime")
                                                                        RideShareArrayList.setValue("\(UberFar)", forKey: "Fare")
                                                                        RideShareArrayList.setValue(Passingdata.startLocation!.lat!, forKey: "BikeShareSourceLat")
                                                                        RideShareArrayList.setValue(Passingdata.startLocation!.lng!, forKey: "BikeShareSourceLng")
                                                                        RideShareArrayList.setValue(Passingdata.endLocation!.lat!, forKey: "BikeShareDestinationLat")
                                                                        RideShareArrayList.setValue(Passingdata.endLocation!.lng!, forKey: "BikeShareDestinationLng")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLat")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLng")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLat")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLng")
                                                                        RideShareArray.append(RideShareArrayList as! [String: Any])
                                                                        print("before adding rideshare \(self.RideShareCount)")
                                                                        CabSharecount = CabSharecount + 1
                                                                        self.RideShareCount = CabSharecount
                                                                        print("adter adding rideshare \(self.RideShareCount)")
                                                                        self.BikeRideShareCompletedCheck(BikeShareArrayList: RideShareArray, Type: "Cab", completion: { (success, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare) in
                                                                            if success {
                                                                                googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                                googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                                googlereponseDirectionList.setValue("\((index))", forKey: "Index")
                                                                                googlereponseDirectionList.setValue("Bolt4", forKey: "TransitImage")
                                                                                googlereponseDirectionList.setValue("", forKey: "FirstAddress")
                                                                                googlereponseDirectionList.setValue("", forKey: "SecondAddress")
                                                                                googlereponseDirectionList.setValue("\((TimingForBikeShare))", forKey: "Timing")
                                                                                googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                                googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                                googlereponseDirectionList.setValue("RideShare", forKey: "ModeOFTransit")
                                                                                googlereponseDirectionList.setValue("", forKey: "TravelingTime")
                                                                                googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
                                                                                googlereponseDirectionList.setValue("", forKey: "WalkingWaitingTime")
                                                                                googlereponseDirectionList.setValue(RideShareArray, forKey: "BikeShareArray")
                                                                                googlereponseDirectionList.setValue(SourceBikeLat, forKey: "SourceLat")
                                                                                googlereponseDirectionList.setValue(SourceBikeLng, forKey: "SourceLng")
                                                                                googlereponseDirectionList.setValue(DestinationBikeLat, forKey: "DestinationLat")
                                                                                googlereponseDirectionList.setValue(DestinationBikeLng, forKey: "DestinationLng")
                                                                                googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                                                print(googlereponseDirectionList)
                                                                                self.ii = self.ii + 1
                                                                                completion(false, googlereponseDirectionList)
                                                                            }
                                                                        })
                                                                    }
                                                                    // print(userDefaultsForBike.bool(forKey: "LyftOn"))
                                                                }
                                                            })
                                                        let delays = 2.0 + Double(index)
                                                        DispatchQueue.main.asyncAfter(deadline: .now() + delays, execute: {
                                                                if userDefaultsForBike.bool(forKey: "LyftOn")
                                                                    {
                                                                    self.RideShareBool = true
                                                                    print("************* Lyft \(Date())")
                                                                    let RideShareArrayList: NSMutableDictionary = [:]
                                                                    let aString = Passingdata.htmlInstructions
                                                                    let walkingInstration = aString!.replacingOccurrences(of: "Walk to", with: "")
                                                                    comFunc.getLyftAPIFirst(SourceLat: Passingdata.startLocation!.lat!, SourceLong: Passingdata.startLocation!.lng!, DestinationLat: Passingdata.endLocation!.lat!, DestinationLong: Passingdata.endLocation!.lng!) { (MinPrice, MaxPrice, TravelMin, travelMile, Status) in
                                                                        if Status {
                                                                            let LyftPrice = "Get Lyft Estimate Fare : $\(MinPrice) - $\(MaxPrice)"
                                                                            let lyftTime = self.convertTime(miliseconds: TravelMin * 1000)
                                                                            let Waitingtime = Int.random(in: 1..<6)
                                                                            let WalkingAboutandCaloties = "About \(lyftTime), \(travelMile / 1609.344) miles, \(Waitingtime) min Wait time"
                                                                            var LyftTime = ""
                                                                            if index == 0 {
                                                                                let Lyftstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                                LyftTime = self.dateconvert(Datevalue: Lyftstart, WalkingTimeinterval: Int(TravelMin) + Waitingtime, addsub: "sub")
                                                                            }
                                                                            else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {
                                                                                let Lyftstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                                LyftTime = self.dateconvert(Datevalue: Lyftstart, WalkingTimeinterval: Int(TravelMin) + Waitingtime, addsub: "add")
                                                                            }
                                                                            else {
                                                                                let Lyftstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                                LyftTime = self.self.dateconvert(Datevalue: Lyftstart, WalkingTimeinterval: Int(TravelMin) + Waitingtime, addsub: "add")
                                                                            }
                                                                            // completion(travelPrice,travelMaxPrice,travelMin!,travelMile,true)
                                                                            RideShareArrayList.setValue(MinPrice, forKey: "vehiclefare")
                                                                            RideShareArrayList.setValue("Lyft", forKey: "vehicleType")
                                                                            RideShareArrayList.setValue(Int(TravelMin) + Waitingtime, forKey: "TotalTime")
                                                                            RideShareArrayList.setValue("#EB428D", forKey: "vehicleColor")
                                                                            RideShareArrayList.setValue("Lyft", forKey: "vehicleTypeImage")
                                                                            RideShareArrayList.setValue("", forKey: "WalkingInstructions")
                                                                            RideShareArrayList.setValue("", forKey: "WalkingAboutandCaloties")
                                                                            RideShareArrayList.setValue("", forKey: "WaitingTime")
                                                                            RideShareArrayList.setValue("", forKey: "WakingStartTime")
                                                                            RideShareArrayList.setValue("Board your Lyft to", forKey: "VehiclePickupLocation")
                                                                            RideShareArrayList.setValue("\(walkingInstration)", forKey: "vehicleStation")
                                                                            RideShareArrayList.setValue("\(LyftPrice)", forKey: "Fare")
                                                                            RideShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCo2")
                                                                            RideShareArrayList.setValue("\(LyftTime)", forKey: "vehicleTime")
                                                                            RideShareArrayList.setValue("", forKey: "BikeShareEndTime")
                                                                            RideShareArrayList.setValue(Passingdata.startLocation!.lat!, forKey: "BikeShareSourceLat")
                                                                            RideShareArrayList.setValue(Passingdata.startLocation!.lng!, forKey: "BikeShareSourceLng")
                                                                            RideShareArrayList.setValue(Passingdata.endLocation!.lat!, forKey: "BikeShareDestinationLat")
                                                                            RideShareArrayList.setValue(Passingdata.endLocation!.lng!, forKey: "BikeShareDestinationLng")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLat")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLng")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLat")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLng")
                                                                            RideShareArray.append(RideShareArrayList as! [String: Any])
                                                                            print("before adding rideshare \(self.RideShareCount)")
                                                                            CabSharecount = CabSharecount + 1
                                                                            self.RideShareCount = CabSharecount
                                                                            print("adter adding rideshare \(self.RideShareCount)")
                                                                            self.BikeRideShareCompletedCheck(BikeShareArrayList: RideShareArray, Type: "Cab", completion: { (success, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare) in
                                                                                if success {
                                                                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                                                    googlereponseDirectionList.setValue("Bolt4", forKey: "TransitImage")
                                                                                    googlereponseDirectionList.setValue("", forKey: "FirstAddress")
                                                                                    googlereponseDirectionList.setValue("", forKey: "SecondAddress")
                                                                                    googlereponseDirectionList.setValue("\((TimingForBikeShare))", forKey: "Timing")
                                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                                    googlereponseDirectionList.setValue("RideShare", forKey: "ModeOFTransit")
                                                                                    googlereponseDirectionList.setValue("", forKey: "TravelingTime")
                                                                                    googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
                                                                                    googlereponseDirectionList.setValue("", forKey: "WalkingWaitingTime")
                                                                                    googlereponseDirectionList.setValue(RideShareArray, forKey: "BikeShareArray")
                                                                                    googlereponseDirectionList.setValue(SourceBikeLat, forKey: "SourceLat")
                                                                                    googlereponseDirectionList.setValue(SourceBikeLng, forKey: "SourceLng")
                                                                                    googlereponseDirectionList.setValue(DestinationBikeLat, forKey: "DestinationLat")
                                                                                    googlereponseDirectionList.setValue(DestinationBikeLng, forKey: "DestinationLng")
                                                                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                                                    print(googlereponseDirectionList)
                                                                                    self.ii = self.ii + 1
                                                                                    completion(false, googlereponseDirectionList)
                                                                                }
                                                                            })
                                                                        }
                                                                    }
                                                                }
                                                            })
                                                      }
                        else
                                                      {
                                                        var CabSharecount = 0
                                                        print("************* RideShare \(Date())")
                                                        print("Before Zero RideShareCount \(self.RideShareCount)")
                                                        self.RideShareCount = 0
                                                        print("After Zero RideShareCount \(self.RideShareCount)")
                                                        var RideShareArray = [[String: Any]]()
                                                        let googlereponseDirectionList: NSMutableDictionary = [:]
                                                        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                                                                if userDefaultsForBike.bool(forKey: "UberOn")
                                                                    {
                                                                    self.RideShareBool = true
                                                                    print("************* Uber \(Date())")
                                                                    let RideShareArrayList: NSMutableDictionary = [:]
                                                                    let waitingTravel = ""
                                                                    let TransitTime = ""
                                                                    let ReveseGeoCodeAddress = ""
                                                                    let WalkingAboutandCo2 = ""
                                                                    let BikeShareTime = ""
                                                                    let BikeShareEndTime = ""
                                                                    let aString = Passingdata.htmlInstructions
                                                                    self.isUberAdded = true
                                                                    let walkingInstration = aString!.replacingOccurrences(of: "Walk to", with: "")
                                                                    let Riderequest = MKDirections.Request()
                                                                    Riderequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.startLocation!.lat!, longitude: Passingdata.startLocation!.lng!), addressDictionary: nil))
                                                                    Riderequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Passingdata.endLocation!.lat!, longitude: Passingdata.endLocation!.lng!), addressDictionary: nil))
                                                                    Riderequest.requestsAlternateRoutes = true
                                                                    Riderequest.transportType = .automobile
                                                                    let Ridedirections = MKDirections(request: Riderequest)
                                                                    Ridedirections.calculate { [unowned self] response, error in
                                                                        guard let RideResponse = response else { return }
                                                                        //for Rideroute in RideResponse.routes {
                                                                        let UberRoute = RideResponse.routes.first!
                                                                        let UberDistance = UberRoute.distance
                                                                        let UberDuration = (UberRoute.expectedTravelTime)
                                                                        let UberDutationTime = self.convertTime(miliseconds: Int(UberDuration * 1000))
                                                                        var UberMiles = ""
                                                                        if Int(UberDistance) > 243 {
                                                                            let CalculateDistance = (Double(UberDistance) / 1609.344)
                                                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                                                            UberMiles = "\(CalculateDistanceTest) miles"
                                                                        }
                                                                        else {
                                                                            let CalculateDistance = (Double(UberDistance) * 3.281)
                                                                            let CalculateDistanceTest = String(format: "%.2f", CalculateDistance)
                                                                            UberMiles = "\(CalculateDistanceTest) ft"
                                                                        }
                                                                        let Waitingtime = Int.random(in: 1..<6)
                                                                        let WalkingAboutandCaloties = "About \(UberDutationTime), \(UberMiles), \(Waitingtime) min Wait time"
                                                                        var uberTime = ""
                                                                      
                                                                        
                                                                        
                                                                        if index == 0 {
                                                                            let uberstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                            uberTime = self.dateconvert(Datevalue: uberstart, WalkingTimeinterval: Int(UberDuration) + Waitingtime, addsub: "sub")
                                                                        }
                                                                        else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {
                                                                            let uberstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                            uberTime = self.dateconvert(Datevalue: uberstart, WalkingTimeinterval: Int(UberDuration) + Waitingtime, addsub: "add")
                                                                          //  UberFar = "Get Uber Estimate Fare : $6 - $8"
                                                                        }
                                                                        else {
                                                                            let uberstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                            uberTime = self.self.dateconvert(Datevalue: uberstart, WalkingTimeinterval: Int(UberDuration) + Waitingtime, addsub: "add")
                                                                          //  UberFar = "Get Uber Estimate Fare : $6 - $8"
                                                                        }
                                                                        RideShareArrayList.setValue(6.0, forKey: "vehiclefare")
                                                                        RideShareArrayList.setValue("Uber", forKey: "vehicleType")
                                                                        RideShareArrayList.setValue(Int(UberDuration) + Waitingtime, forKey: "TotalTime")
                                                                        RideShareArrayList.setValue("#000000", forKey: "vehicleColor")
                                                                        RideShareArrayList.setValue("Uber-1", forKey: "vehicleTypeImage")
                                                                        RideShareArrayList.setValue("", forKey: "WalkingInstructions")
                                                                        RideShareArrayList.setValue("", forKey: "WalkingAboutandCaloties")
                                                                        RideShareArrayList.setValue("\(waitingTravel)", forKey: "WaitingTime")
                                                                        RideShareArrayList.setValue("\(TransitTime)", forKey: "WakingStartTime")
                                                                        RideShareArrayList.setValue("Board your Uber to", forKey: "VehiclePickupLocation")
                                                                        RideShareArrayList.setValue("\(walkingInstration)", forKey: "vehicleStation")
                                                                        RideShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCo2")
                                                                        RideShareArrayList.setValue("\(uberTime)", forKey: "vehicleTime")
                                                                        RideShareArrayList.setValue("", forKey: "BikeShareEndTime")
                                                                        RideShareArrayList.setValue("\(UberFar)", forKey: "Fare")
                                                                        RideShareArrayList.setValue(Passingdata.startLocation!.lat!, forKey: "BikeShareSourceLat")
                                                                        RideShareArrayList.setValue(Passingdata.startLocation!.lng!, forKey: "BikeShareSourceLng")
                                                                        RideShareArrayList.setValue(Passingdata.endLocation!.lat!, forKey: "BikeShareDestinationLat")
                                                                        RideShareArrayList.setValue(Passingdata.endLocation!.lng!, forKey: "BikeShareDestinationLng")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLat")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLng")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLat")
                                                                        RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLng")
                                                                        RideShareArray.append(RideShareArrayList as! [String: Any])
                                                                        print("before adding rideshare \(self.RideShareCount)")
                                                                        CabSharecount = CabSharecount + 1
                                                                        self.RideShareCount = CabSharecount
                                                                        print("adter adding rideshare \(self.RideShareCount)")
                                                                        self.BikeRideShareCompletedCheck(BikeShareArrayList: RideShareArray, Type: "Cab", completion: { (success, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare) in
                                                                            if success {
                                                                                googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                                googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                                googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                                googlereponseDirectionList.setValue("\((index))", forKey: "Index")
                                                                                googlereponseDirectionList.setValue("Bolt4", forKey: "TransitImage")
                                                                                googlereponseDirectionList.setValue("", forKey: "FirstAddress")
                                                                                googlereponseDirectionList.setValue("", forKey: "SecondAddress")
                                                                                googlereponseDirectionList.setValue("\((TimingForBikeShare))", forKey: "Timing")
                                                                                googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                                googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                                googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                                googlereponseDirectionList.setValue("RideShare", forKey: "ModeOFTransit")
                                                                                googlereponseDirectionList.setValue("", forKey: "TravelingTime")
                                                                                googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
                                                                                googlereponseDirectionList.setValue("", forKey: "WalkingWaitingTime")
                                                                                googlereponseDirectionList.setValue(RideShareArray, forKey: "BikeShareArray")
                                                                                googlereponseDirectionList.setValue(SourceBikeLat, forKey: "SourceLat")
                                                                                googlereponseDirectionList.setValue(SourceBikeLng, forKey: "SourceLng")
                                                                                googlereponseDirectionList.setValue(DestinationBikeLat, forKey: "DestinationLat")
                                                                                googlereponseDirectionList.setValue(DestinationBikeLng, forKey: "DestinationLng")
                                                                                googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                                                print(googlereponseDirectionList)
                                                                                self.ii = self.ii + 1
                                                                                completion(false, googlereponseDirectionList)
                                                                            }
                                                                        })
                                                                    }
                                                                    // print(userDefaultsForBike.bool(forKey: "LyftOn"))
                                                                }
                                                            })
                                                        let delays = 2.0 + Double(index)
                                                        DispatchQueue.main.asyncAfter(deadline: .now() + delays, execute: {
                                                                if userDefaultsForBike.bool(forKey: "LyftOn")
                                                                    {
                                                                    self.RideShareBool = true
                                                                    print("************* Lyft \(Date())")
                                                                    let RideShareArrayList: NSMutableDictionary = [:]
                                                                    let aString = Passingdata.htmlInstructions
                                                                    let walkingInstration = aString!.replacingOccurrences(of: "Walk to", with: "")
                                                                    comFunc.getLyftAPIFirst(SourceLat: Passingdata.startLocation!.lat!, SourceLong: Passingdata.startLocation!.lng!, DestinationLat: Passingdata.endLocation!.lat!, DestinationLong: Passingdata.endLocation!.lng!) { (MinPrice, MaxPrice, TravelMin, travelMile, Status) in
                                                                        if Status {
                                                                            let LyftPrice = "Get Lyft Estimate Fare : $\(MinPrice) - $\(MaxPrice)"
                                                                            let lyftTime = self.convertTime(miliseconds: TravelMin * 1000)
                                                                            let Waitingtime = Int.random(in: 1..<6)
                                                                            let WalkingAboutandCaloties = "About \(lyftTime), \(travelMile / 1609.344) miles, \(Waitingtime) min Wait time"
                                                                            var LyftTime = ""
                                                                            if index == 0 {
                                                                                let Lyftstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                                                                                LyftTime = self.dateconvert(Datevalue: Lyftstart, WalkingTimeinterval: Int(TravelMin) + Waitingtime, addsub: "sub")
                                                                            }
                                                                            else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {
                                                                                let Lyftstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                                LyftTime = self.dateconvert(Datevalue: Lyftstart, WalkingTimeinterval: Int(TravelMin) + Waitingtime, addsub: "add")
                                                                            }
                                                                            else {
                                                                                let Lyftstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                                                                                LyftTime = self.self.dateconvert(Datevalue: Lyftstart, WalkingTimeinterval: Int(TravelMin) + Waitingtime, addsub: "add")
                                                                            }
                                                                            // completion(travelPrice,travelMaxPrice,travelMin!,travelMile,true)
                                                                            RideShareArrayList.setValue(MinPrice, forKey: "vehiclefare")
                                                                            RideShareArrayList.setValue("Lyft", forKey: "vehicleType")
                                                                            RideShareArrayList.setValue(Int(TravelMin) + Waitingtime, forKey: "TotalTime")
                                                                            RideShareArrayList.setValue("#EB428D", forKey: "vehicleColor")
                                                                            RideShareArrayList.setValue("Lyft", forKey: "vehicleTypeImage")
                                                                            RideShareArrayList.setValue("", forKey: "WalkingInstructions")
                                                                            RideShareArrayList.setValue("", forKey: "WalkingAboutandCaloties")
                                                                            RideShareArrayList.setValue("", forKey: "WaitingTime")
                                                                            RideShareArrayList.setValue("", forKey: "WakingStartTime")
                                                                            RideShareArrayList.setValue("Board your Lyft to", forKey: "VehiclePickupLocation")
                                                                            RideShareArrayList.setValue("\(walkingInstration)", forKey: "vehicleStation")
                                                                            RideShareArrayList.setValue("\(LyftPrice)", forKey: "Fare")
                                                                            RideShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCo2")
                                                                            RideShareArrayList.setValue("\(LyftTime)", forKey: "vehicleTime")
                                                                            RideShareArrayList.setValue("", forKey: "BikeShareEndTime")
                                                                            RideShareArrayList.setValue(Passingdata.startLocation!.lat!, forKey: "BikeShareSourceLat")
                                                                            RideShareArrayList.setValue(Passingdata.startLocation!.lng!, forKey: "BikeShareSourceLng")
                                                                            RideShareArrayList.setValue(Passingdata.endLocation!.lat!, forKey: "BikeShareDestinationLat")
                                                                            RideShareArrayList.setValue(Passingdata.endLocation!.lng!, forKey: "BikeShareDestinationLng")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLat")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingSourceLng")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLat")
                                                                            RideShareArrayList.setValue(0.0, forKey: "WalkingDestinationLng")
                                                                            RideShareArray.append(RideShareArrayList as! [String: Any])
                                                                            print("before adding rideshare \(self.RideShareCount)")
                                                                            CabSharecount = CabSharecount + 1
                                                                            self.RideShareCount = CabSharecount
                                                                            print("adter adding rideshare \(self.RideShareCount)")
                                                                            self.BikeRideShareCompletedCheck(BikeShareArrayList: RideShareArray, Type: "Cab", completion: { (success, firstBikeShareAddress, SecondDescription, WaitTimeWalking, waitStartTime, BikeShareTime, SourceWalkingLat, SourceWalkingLng, DestinationWalkingLat, DestinationWalkingLng, SourceBikeLat, SourceBikeLng, DestinationBikeLat, DestinationBikeLng, TimingForBikeShare) in
                                                                                if success {
                                                                                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                                                                                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                                                                                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                                                                                    googlereponseDirectionList.setValue("Bolt4", forKey: "TransitImage")
                                                                                    googlereponseDirectionList.setValue("", forKey: "FirstAddress")
                                                                                    googlereponseDirectionList.setValue("", forKey: "SecondAddress")
                                                                                    googlereponseDirectionList.setValue("\((TimingForBikeShare))", forKey: "Timing")
                                                                                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                                                                                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                                                                                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                                                                                    googlereponseDirectionList.setValue("RideShare", forKey: "ModeOFTransit")
                                                                                    googlereponseDirectionList.setValue("", forKey: "TravelingTime")
                                                                                    googlereponseDirectionList.setValue("", forKey: "WalkingCalArrival")
                                                                                    googlereponseDirectionList.setValue("", forKey: "WalkingWaitingTime")
                                                                                    googlereponseDirectionList.setValue(RideShareArray, forKey: "BikeShareArray")
                                                                                    googlereponseDirectionList.setValue(SourceBikeLat, forKey: "SourceLat")
                                                                                    googlereponseDirectionList.setValue(SourceBikeLng, forKey: "SourceLng")
                                                                                    googlereponseDirectionList.setValue(DestinationBikeLat, forKey: "DestinationLat")
                                                                                    googlereponseDirectionList.setValue(DestinationBikeLng, forKey: "DestinationLng")
                                                                                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")
self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])
                                                                                    print(googlereponseDirectionList)
                                                                                    self.ii = self.ii + 1
                                                                                    completion(false, googlereponseDirectionList)
                                                                                }
                                                                            })
                                                                        }
                                                                    }
                                                                }
                                                            })
                        }
                                                  }
                    
                    
                }

                else {


                    // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (Passingdata.startLocation?.lat!)!, SourceLng: (Passingdata.startLocation?.lng!)!, DestinationLat: (Passingdata.endLocation?.lat!)!, DestinationLng: (Passingdata.endLocation?.lng!)!, Type: "Walking")

                    print("\(Passingdata.startLocation?.lat!), \((Passingdata.startLocation?.lng!)!), \((Passingdata.endLocation?.lat!)!), \((Passingdata.endLocation?.lng!)!)")

                    let googlereponseDirectionList: NSMutableDictionary = [:]
                    var waitingTravel = ""
                    TotalMinTripForTravel = TotalMinTripForTravel + Passingdata.duration!.value!
                    let distanceValue = (Passingdata.distance?.value)!
                    let DurationWalking = (Passingdata.duration?.value)!
                    var waitingtime = ""

                    print((Passingdata.distance?.value)!)
                    var durationString = 0
                    var getstime = ""
                    var uberwaitingtime = ""
                    var arrivaltime: Double = 0.0



                    var TransitTime = ""


                    let walkingmin = (Passingdata.duration?.text)!
                    let walkingmiles = (Passingdata.distance?.text)!
                    let walkingdistancevalue: Double = Double((Passingdata.distance?.value)!)
                    let caloriesburn = (walkingdistancevalue * 1.25) / 20
                    let caloriesburnint: Int = Int(caloriesburn.rounded(.toNearestOrAwayFromZero))
                    // cell.TransitRouteNo.isHidden = true
                    // cell.TransitTime.frame.origin.
                    // let walkingarriaval = walkingdeparttime
                    if index == 0 {

                        let walkingstart = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!
                        let walkingminepoch = (Passingdata.duration?.value)
                        let walkingmincal = walkingminepoch
                        waitingTravel = "No wait time."
                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "sub")
                        TransitTime = "\(getstime)"
                        arrivaltime = walkingstart
                        //cell.TransitTime.text = "\(getstime)"
                    }
                    else if index == (self.dataPrevRespone.result.value?.routes![self.indexValue].legs![0].steps?.count)! - 1 {

                        let walkingstart = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                        let walkingminepoch = (Passingdata.duration?.value)
                        let walkingmincal = walkingminepoch
                        let getstime = self.dateconvert(Datevalue: walkingstart, WalkingTimeinterval: walkingmincal!, addsub: "add")
                        //cell.TransitTime.text = "\(getstime)"
                        TransitTime = "\(getstime)"
                        arrivaltime = walkingstart

                        waitingTravel = "No wait time."
                    }
                    else {
                        var walkstringtoint = ""
                        //let walkingend = (routesPlan![0].steps![index-1].transitDetails?.arrivalTime?.text)!
                        let walkingminepoch = (Passingdata.duration?.value)
                        let walkingmincal = walkingminepoch
                        if walkingmin.contains("mins") {
                            walkstringtoint = (walkingmin.replacingOccurrences(of: " mins", with: ""))
                        }
                        else {
                            walkstringtoint = (walkingmin.replacingOccurrences(of: " min", with: ""))
                        }
                        let walkminInt: Int = Int(walkstringtoint)! * 60
                        print(walkminInt)
                        let walkingend = (routesPlan![0].steps![index - 1].transitDetails?.arrivalTime?.value)!
                        let getstime = self.dateconvert(Datevalue: walkingend, WalkingTimeinterval: walkingmincal!, addsub: "add")
                        TransitTime = "\(getstime)"
                        arrivaltime = walkingend
                        let waitingbus = (routesPlan![0].steps![index + 1].transitDetails?.departureTime?.value)!

                        let waitingtimeResult = self.datasubraction(arrivalDate: walkingend, DepartDate: waitingbus, Walkingmin: walkminInt)
                        waitingTravel = "Wait time \(waitingtimeResult)"

                    }


                    let FirstAddressGoogle = Passingdata.htmlInstructions
                    let SecondDescription = "About \(walkingmin), \(walkingmiles) (\(caloriesburnint) Calories)"

                    googlereponseDirectionList.setValue("false", forKey: "RealTime")
                    googlereponseDirectionList.setValue("", forKey: "RouteLineColor")
                    googlereponseDirectionList.setValue("", forKey: "TripStatus")
                    googlereponseDirectionList.setValue("\(index)", forKey: "Index")
                    googlereponseDirectionList.setValue("walking_Icon", forKey: "TransitImage")
                    googlereponseDirectionList.setValue(FirstAddressGoogle, forKey: "FirstAddress")
                    googlereponseDirectionList.setValue(SecondDescription, forKey: "SecondAddress")
                    googlereponseDirectionList.setValue(TransitTime, forKey: "Timing")
                    googlereponseDirectionList.setValue("", forKey: "RouteLine")
                    googlereponseDirectionList.setValue("", forKey: "ArrivalStop")
                    googlereponseDirectionList.setValue("", forKey: "NextStop")
                    googlereponseDirectionList.setValue("Walking", forKey: "ModeOFTransit")
                    googlereponseDirectionList.setValue(Passingdata.distance?.value, forKey: "TravelingTime")
                    googlereponseDirectionList.setValue(arrivaltime, forKey: "WalkingCalArrival")
                    googlereponseDirectionList.setValue(waitingTravel, forKey: "WalkingWaitingTime")

                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lat!, forKey: "SourceLat")
                    googlereponseDirectionList.setValue(Passingdata.startLocation!.lng!, forKey: "SourceLng")
                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lat, forKey: "DestinationLat")
                    googlereponseDirectionList.setValue(Passingdata.endLocation!.lng, forKey: "DestinationLng")

                    googlereponseDirectionList.setValue("0", forKey: "ScrollIndex")



                    self.googlereponseDirection.append(googlereponseDirectionList as! [String: Any])

                    print(googlereponseDirectionList)
                    print("Before Added \(self.ii)")
                    self.ii = self.ii + 1
                    print("After Added \(self.ii)")
                    completion(false, googlereponseDirectionList)


                }
            }


        }










        func getLimeDisatance(sourceLat: Double, sourceLng: Double) -> Double
        {
            var BoltCheckBikeCount = 0
            var distanceInMeters: Double?
            var SourceDistance = 0.0
            var tempDistance = 0.0
            var LimeListApi = SourceDestination.LimeListApi
            let Sourcelatlatlong = CLLocation(latitude: sourceLat, longitude: sourceLng)
            for LimeBike in (LimeListApi)! {
                let BirdBikeLat = LimeBike.bike_lat! as NSString
                let BirdBikeLng = LimeBike.bike_lon! as NSString
                let BoltLatitude = Double("\(BirdBikeLat)")
                let BoltLontitude = Double("\(BirdBikeLng)")

                if BoltCheckBikeCount == 0 {

                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    SourceDistance = (distanceInMeters! * 100).rounded() / 100
                    BoltCheckBikeCount = BoltCheckBikeCount + 1

                    print("\(SourceDistance)")




                }
                else {
                    self.templat = BoltLatitude!
                    self.templong = BoltLontitude!
                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    tempDistance = (distanceInMeters! * 100).rounded() / 100
                    if tempDistance < SourceDistance {

                        SourceDistance = tempDistance
                        print(tempDistance)
                        BoltCheckBikeCount = BoltCheckBikeCount + 1

                    }
                    else {
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                    }
                }





            }

            return SourceDistance
        }
        func getBoltDistance(sourceLat: Double, sourceLng: Double) -> Double
        {
            var BoltCheckBikeCount = 0
            var distanceInMeters: Double?
            var SourceDistance = 0.0
            var tempDistance = 0.0
            var boltListApi = SourceDestination.boltListApi
            let Sourcelatlatlong = CLLocation(latitude: sourceLat, longitude: sourceLng)
            for LimeBike in (boltListApi)! {
                let BirdBikeLat = LimeBike.bike_lat! as NSString
                let BirdBikeLng = LimeBike.bike_lon! as NSString
                let BoltLatitude = Double("\(BirdBikeLat)")
                let BoltLontitude = Double("\(BirdBikeLng)")

                if BoltCheckBikeCount == 0 {

                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    SourceDistance = (distanceInMeters! * 100).rounded() / 100
                    BoltCheckBikeCount = BoltCheckBikeCount + 1

                    print("\(SourceDistance)")




                }
                else {
                    self.templat = BoltLatitude!
                    self.templong = BoltLontitude!
                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    tempDistance = (distanceInMeters! * 100).rounded() / 100
                    if tempDistance < SourceDistance {

                        SourceDistance = tempDistance
                        print(tempDistance)
                        BoltCheckBikeCount = BoltCheckBikeCount + 1

                    }
                    else {
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                    }
                }





            }

            return SourceDistance
        }



        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()

            view.layer.cornerRadius = 12
        }

        override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
            //        print("will move to \(stickyPoint)")
        }

        override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
            //        print("did move to \(stickyPoint)")
        }

        override func pullUpControllerDidDrag(to point: CGFloat) {
            //        print("did drag to \(point)")
        }


        // MARK: - PullUpController

        override var pullUpControllerPreferredSize: CGSize {
            return portraitSize
        }

        override var pullUpControllerPreferredLandscapeFrame: CGRect {
            return landscapeFrame
        }

        override var pullUpControllerMiddleStickyPoints: [CGFloat] {
            print("\(firstPreviewView.frame.maxY)")
            print("\(searchBoxContainerView.frame.maxY)")

            switch initialState {
            case .contracted:
                return [firstPreviewView.frame.maxY]
            case .expanded:
                return [searchBoxContainerView.frame.maxY, firstPreviewView.frame.maxY]
            }
        }

        override var pullUpControllerBounceOffset: CGFloat {
            return 20
        }

        override func pullUpControllerAnimate(action: PullUpController.Action,
            withDuration duration: TimeInterval,
            animations: @escaping () -> Void,
            completion: ((Bool) -> Void)?) {
            switch action {
            case .move:
                UIView.animate(withDuration: 0.3,
                    delay: 0,
                    usingSpringWithDamping: 0.7,
                    initialSpringVelocity: 0,
                    options: .curveEaseInOut,
                    animations: animations,
                    completion: completion)
            default:
                UIView.animate(withDuration: 0.3,
                    animations: animations,
                    completion: completion)
            }
        }





        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

            if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "Bike_BikeShare" {
                return Bike_BikeShareHeight!
            }
            else if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "Bike_Walking" {
                return Bike_WalkingHeight! // for dynamic cell height

            }
            else if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "RideShare" || googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "BikeShare" {
                return 124.0+35.0 // for dynamic cell height

            }
            else {
             
                return 124.0
            }



        }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//   if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "Bike_BikeShare" || googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "Bike_Walking"{
//        return UITableViewAutomaticDimension
//        }
//        else{
//            return 124.0
//        }
//
//    }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            print(googlereponseDirection.count)
            return googlereponseDirection.count
        }
    func deg2rad(deg: Double) -> Double {
            return deg * M_PI / 180
        }
        ///////////////////////////////////////////////////////////////////////
        ///  This function converts radians to decimal degrees              ///
        ///////////////////////////////////////////////////////////////////////
        func rad2deg(rad: Double) -> Double {
            return rad * 180.0 / M_PI
        }
      func distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double, unit: String) -> Double {
          let theta = lon1 - lon2
          var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta))
          dist = acos(dist)
          dist = rad2deg(rad: dist)
          dist = dist * 60 * 1.1515
          if (unit == "K") {
              dist = dist * 1.609344
          }
          else if (unit == "N") {
              dist = dist * 0.8684
          }
          return dist
      }

        func showLoader()
        {

            DispatchQueue.main.async {
                self.loaderView = UIView()
                self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height + 120)
                self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
                let window = UIApplication.shared.keyWindow!

                let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width / 2) - 40, y: (self.view.frame.size.height / 2) - 40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0))
                activityLoader.startAnimating()
                window.addSubview(self.loaderView)
                self.loaderView.addSubview(activityLoader)
            }

        }
        func hideLoader()
        {
            DispatchQueue.main.async {
                self.loaderView.removeFromSuperview()
            }
        }
    func getalertTitle(RouteNo: String, completion: @escaping (String,String,Bool) -> ()) {
        
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Alerts/GetRealtimeAlerts?routeID=\(RouteNo)", method: .get, encoding: JSONEncoding.default) .responseJSON { response in
            switch response.result{
            //switch result {
               
                case .success:
               // print(response)
                let responseVar = response.result.value
                let json = JSON(responseVar!)
               
                let successvalu:Bool?
                if json.count == 0 {
                    successvalu = false
                }
                else {
                     successvalu = true
                }
               
                completion("\(json[0])","\(json["LineAlert"])", successvalu!)
            case .failure(let error):
                print("Request failed with error: \(error)")
                }
        }
        
        
        
        
        
        
        
    }
        @objc func downloadTimer() {
            print("print Print")
            // access timer.userInfo here
        }
                func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

                    if googlereponseDirection.count > 0 {
                        print("************ Not Zero Transit ************")
                        print(googlereponseDirection[indexPath.row])
                        //let cell = tableView.dequeueReusableCell(withIdentifier: "TripResultCell", for: indexPath) as! TripResultCell

                        if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "TRANSIT" {

                            print("************ Transit ************")




                            let cell = tableView.dequeueReusableCell(withIdentifier: "TripResultCell", for: indexPath) as! TripResultCell
                            print(indexPath.row)
                            //   if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "TRANSIT"{
                            getalertTitle(RouteNo: (googlereponseDirection[indexPath.row]["RouteLine"] as? String)!){heading,AlertMsg,SuccessValue in
                                                   if SuccessValue {
                                                    
                                                    cell.TransitAlertIcon.isHidden = false
                                                    
                                }
                                                   else{
                                                    
                                                    cell.TransitAlertIcon.isHidden = true
                                                    
                                }
                            }

                            cell.TransitPanel.isHidden = false
                            let routecolor = (googlereponseDirection[indexPath.row]["RouteLineColor"] as? String)!

                            //  (self.parent as? TripMapViewController)?.drawPolylineTransit(polylinePoint: ((googlereponseDirection[indexPath.row]["polylineString"] as? String)!), TransitColor: routecolor)

                            let lineLayer = CAShapeLayer()
                            lineLayer.strokeColor = color.hexStringToUIColor(hex: routecolor).cgColor
                            lineLayer.lineWidth = 2
                            // lineLayer.li
                            //  lineLayer.lineDashPattern = [6,0]
                            let path = CGMutablePath()
                            path.addLines(between: [CGPoint(x: 6, y: 10),
                                CGPoint(x: 6, y: cell.contentView.frame.height - 10)])
                            lineLayer.path = path
                            //  cell.LineView.layer.addSublayer(lineLayer)
                            cell.TransitLine.layer.addSublayer(lineLayer)

                            let Startlayer = CAShapeLayer()
                            Startlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath
                            Startlayer.fillColor = color.hexStringToUIColor(hex: routecolor).cgColor
                            cell.TransitLine.layer.addSublayer(Startlayer)

                            let Endlayer = CAShapeLayer()
                            Endlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: cell.TransitLine.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath
                            Endlayer.fillColor = color.hexStringToUIColor(hex: routecolor).cgColor
                            cell.TransitLine.layer.addSublayer(Endlayer)



                            cell.TransitDepartAddress.text = googlereponseDirection[indexPath.row]["SecondAddress"] as? String
                            //  cell.TransitArrivalAddress.text = googlereponseDirection[indexPath.row]["ArrivalStop"] as? String
                            cell.TransitNextStop.text = googlereponseDirection[indexPath.row]["NextStop"] as? String
                            cell.TransitBusStopAddress.text = googlereponseDirection[indexPath.row]["FirstAddress"] as? String
                            cell.TransitArrivalAddress.text = googlereponseDirection[indexPath.row]["ArrivalStop"] as? String
                            if (googlereponseDirection[indexPath.row]["RealTime"]! as? String)! == "true" {

                                cell.TransitRealTimeStatus.image = UIImage.gif(name: "wifi")
        //                        cell.TransitETATime.text = ""
        //                        cell.TransitMinLabel.text = ""
        //                        cell.TransitOntimeStatus.text = ""
                                cell.TransitETALabel.text = "ETA"

                                //cell.contentView.addSubview(cell.reailtimeicon)
                            } else {

                                cell.TransitRealTimeStatus.image = nil
                                cell.TransitETATime.text = ""
                                //  cell.TransitMinLabel.text = ""
                                cell.TransitOntimeStatus.text = ""
                                cell.TransitETALabel.text = ""

                            }
                            cell.LineNumber.text = googlereponseDirection[indexPath.row]["RouteLine"] as? String
                            cell.TransitArrivalTime.text = googlereponseDirection[indexPath.row]["TransitArrivalTime"] as? String
                            cell.TransitDepartTime.text = googlereponseDirection[indexPath.row]["TransitDepartTime"] as? String
                            cell.Transitco2.attributedText = googlereponseDirection[indexPath.row]["TransitCo2Cal"] as? NSAttributedString

                            cell.TransitETATime.text = googlereponseDirection[indexPath.row]["ETA"] as? String


                            let tripstatus = googlereponseDirection[indexPath.row]["TripStatus"] as? String
                            if (tripstatus?.contains("late"))! {
                                cell.TransitOntimeStatus.textColor = color.hexStringToUIColor(hex: "#B60202")
                            }
                            else {
                                cell.TransitOntimeStatus.textColor = color.hexStringToUIColor(hex: "#02B64B")
                            }
                            cell.TransitOntimeStatus.text = googlereponseDirection[indexPath.row]["TripStatus"] as? String


                            cell.TransitBusIcon.image = UIImage(named: "BusTransit")
                            cell.TransitBusIcon.setImageColor(color: color.hexStringToUIColor(hex: routecolor))

                            return cell
                        }
                        else if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "BikeShare" {

                            print("************ BikeShare ************")
print(googlereponseDirection)
                            let sourcelat = googlereponseDirection[indexPath.row]["SourceLat"] as? Double
                            let sourceLng = googlereponseDirection[indexPath.row]["SourceLng"] as? Double
                            let DestinatonLat = googlereponseDirection[indexPath.row]["DestinationLat"] as? Double
                            let DestinationLng = googlereponseDirection[indexPath.row]["DestinationLng"] as? Double
                            let ScrollIndex = googlereponseDirection[indexPath.row]["ScrollIndex"] as! String
     print(ScrollIndex)

                            //
                            let BikeShareData = googlereponseDirection[indexPath.row]["BikeShareArray"] as! [[String: Any]]
                            print(BikeShareData.count)

                            let cell = tableView.dequeueReusableCell(withIdentifier: "BikeShareCarShare", for: indexPath) as! BikeShareCarShare


                            scrollView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: cell.contentView.frame.size.height)
                            if BikeShareData.count == 3 {
                                scrollView.contentSize = CGSize.init(width: self.view.frame.size.width * 3, height: cell.contentView.frame.size.height)
                            }
                            else if BikeShareData.count == 2 {
                                scrollView.contentSize = CGSize.init(width: self.view.frame.size.width * 2, height: cell.contentView.frame.size.height)
                            }
                            else {
                                scrollView.contentSize = CGSize.init(width: self.view.frame.size.width * 1, height: cell.contentView.frame.size.height)
                            }
                            scrollView.bounces = false
                            scrollView.isPagingEnabled = true
                            scrollView.tag = indexPath.row
                            scrollView.showsVerticalScrollIndicator = false
                            scrollView.showsHorizontalScrollIndicator = false
                            scrollView.delegate = self
                            scrollView.backgroundColor = UIColor.white
                            scrollView.showsHorizontalScrollIndicator = true
                            scrollView.isPagingEnabled = true
                            cell.contentView.addSubview(scrollView)
                            cell.contentView.bringSubview(toFront: scrollView)
                            // (self.parent as? TripMapViewController).
                            let  currentPages = Int(ScrollIndex)
                            
                            (self.parent as? TripMapViewController)?.addmarkerfrom(LatforBike: (sourcelat)!, LongforBike: (sourceLng)!, vechelType: (BikeShareData[currentPages!]["vehicleType"] as? String)!, Address: (BikeShareData[currentPages!]["vehicleStation"] as? String)!)
                           
                           // (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: (sourcelat)!, SourceLng: (sourceLng)!, DestinationLat: (DestinatonLat)!, DestinationLng: (DestinationLng)!, Type: "BikeShare", TripColor: (BikeShareData[0]["vehicleColor"] as? String)!)
                            
                            for BikeShareArray in 0...BikeShareData.count - 1 {
                                // scrollView.frame.size.width*1
                                let vehicleColor = BikeShareData[BikeShareArray]["vehicleColor"] as? String
                                // let lineLayer = CAShapeLayer()
                                let Startlayer = CAShapeLayer()

                                let Endlayer = CAShapeLayer()
                                let BikeShareimageName = "BIRD4"
                                let BikeShareimage = UIImage(named: BikeShareimageName)

                                let BikeSharePickup = UILabel()
                                let BikeSharevehicleStation = UILabel()
                                let BikeShareWalkingAboutandCo2 = UILabel()
                                let BikeSharebutton = UIButton()
                                let BikeSharevehicleStartTime = UILabel()
                                let BikeShareimageView = UIImageView(image: BikeShareimage!)
                                let pageControl = UIPageControl()



                                Startlayer.fillColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                scrollView.layer.addSublayer(Startlayer)



                                Endlayer.fillColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                scrollView.layer.addSublayer(Endlayer)




                                BikeShareimageView.contentMode = .scaleAspectFit
                                scrollView.addSubview(BikeShareimageView)

                                //vehicleType






                                BikeSharePickup.textAlignment = .left //For center alignment
                                BikeSharePickup.text = BikeShareData[BikeShareArray]["VehiclePickupLocation"] as? String
                                BikeSharePickup.textColor = .darkGray
                                // lbl.backgroundColor = .lightGray//If required
                                BikeSharePickup.font = UIFont.setTarcRegular(size: 10)
                                scrollView.addSubview(BikeSharePickup)



                                BikeSharevehicleStation.textAlignment = .left //For center alignment
                                BikeSharevehicleStation.text = BikeShareData[BikeShareArray]["vehicleStation"] as? String
                                BikeSharevehicleStation.textColor = .black
                                //BikeSharevehicleStation.backgroundColor = .darkGray//If required
                                BikeSharevehicleStation.font = UIFont.boldSystemFont(ofSize: 11.0)
                                BikeSharevehicleStation.lineBreakMode = NSLineBreakMode.byWordWrapping
                                BikeSharevehicleStation.numberOfLines = 0
                                scrollView.addSubview(BikeSharevehicleStation)


                                BikeShareWalkingAboutandCo2.textAlignment = .left //For center alignment
                                BikeShareWalkingAboutandCo2.text = BikeShareData[BikeShareArray]["WalkingAboutandCo2"] as? String
                                BikeShareWalkingAboutandCo2.textColor = .darkGray
                                // BikeShareWalkingAboutandCo2.backgroundColor = .lightGray//If required
                                BikeShareWalkingAboutandCo2.font = UIFont.setTarcRegular(size: 10)
                                scrollView.addSubview(BikeShareWalkingAboutandCo2)



                                BikeSharebutton.accessibilityHint = "\((BikeShareData[BikeShareArray]["vehicleType"] as? String)!)"
                                BikeSharebutton.backgroundColor = color.hexStringToUIColor(hex: vehicleColor!)
                                BikeSharebutton.setTitle("Get \((BikeShareData[BikeShareArray]["vehicleType"] as? String)!) ", for: .normal)
                                BikeSharebutton.setTitle("\((BikeShareData[BikeShareArray]["Fare"] as? String)!)", for: .normal)
                                BikeSharebutton.titleLabel!.font = UIFont.setTarcBold(size: 12.0)
                                BikeSharebutton.tag = indexPath.row
                                BikeSharebutton.addTarget(self, action: #selector(RideAppOpen), for: .touchUpInside)
                                scrollView.addSubview(BikeSharebutton)
                                cell.contentView.bringSubview(toFront: BikeSharebutton)


                                pageControl.numberOfPages = BikeShareData.count
                                pageControl.tintColor = UIColor.red
                                pageControl.pageIndicatorTintColor = UIColor.init(red: 211/255.0, green: 211/255.0, blue: 211/255.0, alpha: 1.0)
                                pageControl.currentPageIndicatorTintColor = color.hexStringToUIColor(hex: vehicleColor!)

                                scrollView.addSubview(pageControl)
                            cell.contentView.bringSubview(toFront: pageControl)

                                
                                BikeSharevehicleStartTime.textAlignment = .left //For center alignment
                                BikeSharevehicleStartTime.text = BikeShareData[BikeShareArray]["vehicleTime"] as? String
                                BikeSharevehicleStartTime.textColor = .black
                                //BikeSharevehicleStation.backgroundColor = .darkGray//If required
                                BikeSharevehicleStartTime.font = UIFont.setTarcBold(size: 10.0)
                                scrollView.addSubview(BikeSharevehicleStartTime)

                                if BikeShareArray == 0 {
                                    let lineLayer = CAShapeLayer()
                                    lineLayer.strokeColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                    lineLayer.lineWidth = 2
                                    // lineLayer.lineDashPattern = [4, 4]
                                    let path = CGMutablePath()
                                    path.addLines(between: [CGPoint(x: 14, y: 10),
                                        CGPoint(x: 14, y: cell.contentView.frame.height - 10)])
                                    lineLayer.path = path
                                    //  cell.LineView.layer.addSublayer(lineLayer)
                                    scrollView.layer.addSublayer(lineLayer)
                                    Startlayer.path = UIBezierPath(roundedRect: CGRect(x: 9, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath
                                    Endlayer.path = UIBezierPath(roundedRect: CGRect(x: 9, y: scrollView.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath
                                    BikeShareimageView.frame = CGRect(x: 30, y: 0, width: 20, height: cell.contentView.frame.height)

                                    for BikeShareArrayList in 0...BikeShareData.count - 1 {


                                        let BirimageName = BikeShareData[BikeShareArrayList]["vehicleTypeImage"] as? String


                                        print(60 * BikeShareArrayList)
                                        if BikeShareArrayList == 0 {

                                            if BikeShareArrayList != BikeShareArray {
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                                var Birdimage = UIImage(named: BirimageName!)

                                               // Birdimage = Birdimage?.noir
                                                let BirdimageView = UIImageView(image: Birdimage!)
                                                BirdimageView.frame = CGRect(x: 60, y: 5, width: 15, height: 15)
                                                scrollView.addSubview(BirdimageView)
                                            }
                                            else {
                                                let Birdimage = UIImage(named: BirimageName!)
                                                let BirdimageView = UIImageView(image: Birdimage!)
                                                BirdimageView.frame = CGRect(x: 60, y: 5, width: 15, height: 15)
                                                scrollView.addSubview(BirdimageView)

                                            }






                                        }
                                        else if BikeShareArrayList == 1 {
                                            var Birdimage = UIImage(named: BirimageName!)
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 40)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                                Birdimage = Birdimage?.noir
                                            }
                                            scrollView.addSubview(BirdimageView)
                                        }
                                        else {
                                            var Birdimage = UIImage(named: BirimageName!)
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 80)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                                Birdimage = Birdimage?.noir
                                            }
                                            scrollView.addSubview(BirdimageView)
                                        }





                                    }

                                    BikeSharePickup.frame = CGRect(x: 60, y: 20, width: self.view.frame.width / 1.6, height: 20)
                                    BikeSharevehicleStation.frame = CGRect(x: 60, y: 35, width: self.view.frame.width / 1.6, height: 35)
                                    BikeShareWalkingAboutandCo2.frame = CGRect(x: 60, y: BikeSharevehicleStation.frame.height - 5 + BikeSharevehicleStation.frame.origin.y, width: self.view.frame.width / 1.6, height: 15)
                                    BikeSharebutton.frame = CGRect(x: 60, y: BikeShareWalkingAboutandCo2.frame.origin.y + BikeShareWalkingAboutandCo2.frame.height + 10, width: cell.contentView.frame.width / 1.6, height: 30)
                                    pageControl.frame = CGRect.init(x: 0, y: BikeSharebutton.frame.maxY+5, width: self.view.frame.size.width, height: 35)

                                    BikeSharevehicleStartTime.frame = CGRect(x: self.view.frame.width - 55, y: (cell.contentView.frame.height / 2) - 20, width: 53, height: 35)


                                }







                                else if BikeShareArray == 1 {

                                    let lineLayer = CAShapeLayer()
                                    lineLayer.strokeColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                    lineLayer.lineWidth = 2
                                    // lineLayer.lineDashPattern = [4, 4]
                                    let path = CGMutablePath()

                                    path.addLines(between: [CGPoint(x: scrollView.frame.size.width * 1 + 14, y: 10),
                                        CGPoint(x: scrollView.frame.size.width * 1 + 14, y: cell.contentView.frame.height - 10)])


                                    lineLayer.path = path
                                    //  cell.LineView.layer.addSublayer(lineLayer)
                                    scrollView.layer.addSublayer(lineLayer)


                                    Startlayer.path = UIBezierPath(roundedRect: CGRect(x: scrollView.frame.size.width * 1 + 9, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath



                                    Endlayer.path = UIBezierPath(roundedRect: CGRect(x: scrollView.frame.size.width * 1 + 9, y: scrollView.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath



                                    BikeShareimageView.frame = CGRect(x: scrollView.frame.size.width * 1 + 30, y: 0, width: 20, height: cell.contentView.frame.height)


                                    //vehicleType

                                    for BikeShareArrayList in 0...BikeShareData.count - 1 {


                                        let BirimageName = BikeShareData[BikeShareArrayList]["vehicleTypeImage"] as? String
                                        var Birdimage = UIImage(named: BirimageName!)

                                        print(60 * BikeShareArrayList)
                                        if BikeShareArrayList == 0 {

                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            BirdimageView.frame = CGRect(x: scrollView.frame.size.width * 1 + 60, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            scrollView.addSubview(BirdimageView)

                                        }
                                        else if BikeShareArrayList == 1 {
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 40) + Int(scrollView.frame.size.width)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            scrollView.addSubview(BirdimageView)
                                        }
                                        else {
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 80) + Int(scrollView.frame.size.width * 1)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            scrollView.addSubview(BirdimageView)
                                        }





                                    }



                                    BikeSharePickup.frame = CGRect(x: scrollView.frame.size.width * 1 + 60, y: 20, width: cell.contentView.frame.width / 1.6, height: 20)


                                    BikeSharevehicleStation.frame = CGRect(x: scrollView.frame.size.width * 1 + 60, y: 35, width: self.view.frame.width / 1.6, height: 35)


                                    BikeShareWalkingAboutandCo2.frame = CGRect(x: scrollView.frame.size.width * 1 + 60, y: BikeSharevehicleStation.frame.height - 5 + BikeSharevehicleStation.frame.origin.y, width: cell.contentView.frame.width / 1.6, height: 15)


                                    BikeSharebutton.frame = CGRect(x: scrollView.frame.size.width * 1 + 60, y: BikeShareWalkingAboutandCo2.frame.origin.y + BikeShareWalkingAboutandCo2.frame.height + 10, width: cell.contentView.frame.width / 1.6, height: 30)
                                    pageControl.frame = CGRect.init(x: scrollView.frame.size.width * 1, y: BikeSharebutton.frame.maxY+5, width: self.view.frame.size.width, height: 35)


                                    BikeSharevehicleStartTime.frame = CGRect(x: scrollView.frame.size.width * 1 + self.view.frame.width - 55, y: (cell.contentView.frame.height / 2) - 20, width: 53, height: 35)


                                }
                                else if BikeShareArray == 2 {

                                    let lineLayer = CAShapeLayer()
                                    lineLayer.strokeColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                    lineLayer.lineWidth = 2
                                    // lineLayer.lineDashPattern = [4, 4]
                                    let path = CGMutablePath()

                                    path.addLines(between: [CGPoint(x: scrollView.frame.size.width * 2 + 14, y: 10),
                                        CGPoint(x: scrollView.frame.size.width * 2 + 14, y: cell.contentView.frame.height - 10)])


                                    lineLayer.path = path
                                    //  cell.LineView.layer.addSublayer(lineLayer)
                                    scrollView.layer.addSublayer(lineLayer)


                                    Startlayer.path = UIBezierPath(roundedRect: CGRect(x: scrollView.frame.size.width * 2 + 9, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath



                                    Endlayer.path = UIBezierPath(roundedRect: CGRect(x: scrollView.frame.size.width * 2 + 9, y: scrollView.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath



                                    BikeShareimageView.frame = CGRect(x: scrollView.frame.size.width * 2 + 30, y: 0, width: 20, height: cell.contentView.frame.height)


                                    //vehicleType

                                    for BikeShareArrayList in 0...BikeShareData.count - 1 {


                                        let BirimageName = BikeShareData[BikeShareArrayList]["vehicleTypeImage"] as? String
                                        var Birdimage = UIImage(named: BirimageName!)

                                        print(60 * BikeShareArrayList)
                                        if BikeShareArrayList == 0 {

                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            BirdimageView.frame = CGRect(x: scrollView.frame.size.width * 2 + 60, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            scrollView.addSubview(BirdimageView)

                                        }
                                        else if BikeShareArrayList == 1 {
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 40) + Int(scrollView.frame.size.width * 2)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            scrollView.addSubview(BirdimageView)
                                        }
                                        else if BikeShareArrayList == 2{
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 80) + Int(scrollView.frame.size.width * 2)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            scrollView.addSubview(BirdimageView)
                                        }





                                    }



                                    BikeSharePickup.frame = CGRect(x: scrollView.frame.size.width * 2 + 60, y: 20, width: cell.contentView.frame.width / 1.6, height: 20)


                                    BikeSharevehicleStation.frame = CGRect(x: scrollView.frame.size.width * 2 + 60, y: 35, width: self.view.frame.width / 1.6, height: 35)


                                    BikeShareWalkingAboutandCo2.frame = CGRect(x: scrollView.frame.size.width * 2 + 60, y: BikeSharevehicleStation.frame.height - 5 + BikeSharevehicleStation.frame.origin.y, width: cell.contentView.frame.width / 1.6, height: 15)


                                    BikeSharebutton.frame = CGRect(x: scrollView.frame.size.width * 2 + 60, y: BikeShareWalkingAboutandCo2.frame.origin.y + BikeShareWalkingAboutandCo2.frame.height + 10, width: cell.contentView.frame.width / 1.6, height: 30)
                                    pageControl.frame = CGRect.init(x: scrollView.frame.size.width * 2, y: BikeSharebutton.frame.maxY+5, width: self.view.frame.size.width, height: 35)


                                    BikeSharevehicleStartTime.frame = CGRect(x: scrollView.frame.size.width * 2 + cell.contentView.frame.width - 55, y: (cell.contentView.frame.height / 2) - 20, width: 53, height: 35)


                                }

                            }

                            return cell
                        }
                        else if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "RideShare" {

                            print("************ RideShare ************ \(indexPath.row) -  \((googlereponseDirection[indexPath.row]["Index"] as? String)!)")

                            let cell = tableView.dequeueReusableCell(withIdentifier: "BikeShareCarShare", for: indexPath) as! BikeShareCarShare


                            let sourcelat = googlereponseDirection[indexPath.row]["SourceLat"] as? Double
                            print(sourcelat!)
                            print(googlereponseDirection[indexPath.row])
                            let sourceLng = googlereponseDirection[indexPath.row]["SourceLng"] as? Double
                            let DestinatonLat = googlereponseDirection[indexPath.row]["DestinationLat"] as? Double
                            let DestinationLng = googlereponseDirection[indexPath.row]["DestinationLng"] as? Double



                            //
                            let BikeShareData = googlereponseDirection[indexPath.row]["BikeShareArray"] as! [[String: Any]]
                            print(BikeShareData.count)

                            let RidescrollViewCell = UIScrollView()

                            RidescrollViewCell.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: cell.contentView.frame.size.height)
                            if BikeShareData.count == 3 {
                                RidescrollViewCell.contentSize = CGSize.init(width: self.view.frame.size.width * 3, height: cell.contentView.frame.size.height)
                            }
                            else if BikeShareData.count == 2 {
                                RidescrollViewCell.contentSize = CGSize.init(width: self.view.frame.size.width * 2, height: cell.contentView.frame.size.height)
                            }
                            else {
                                RidescrollViewCell.contentSize = CGSize.init(width: self.view.frame.size.width * 1, height: cell.contentView.frame.size.height)
                            }
                            RidescrollViewCell.bounces = false
                            RidescrollViewCell.isPagingEnabled = true
                            RidescrollViewCell.tag = indexPath.row
                            RidescrollViewCell.showsVerticalScrollIndicator = false
                            RidescrollViewCell.showsHorizontalScrollIndicator = false
                            RidescrollViewCell.delegate = self

                            RidescrollViewCell.backgroundColor = UIColor.white
                            RidescrollViewCell.showsHorizontalScrollIndicator = false
                            RidescrollViewCell.isPagingEnabled = true
                            cell.contentView.addSubview(RidescrollViewCell)
                            cell.contentView.bringSubview(toFront: RidescrollViewCell)
                            RidescrollView = RidescrollViewCell
                            
                            let ScrollIndex = googlereponseDirection[indexPath.row]["ScrollIndex"] as? String

                            let scrollIndexInt = Int(ScrollIndex!)
                            
                            (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: sourcelat!, SourceLng: sourceLng!, DestinationLat: DestinatonLat!, DestinationLng: DestinationLng!, Type: "Uber", TripColor: (BikeShareData[scrollIndexInt!]["vehicleColor"] as? String)!)
                            for BikeShareArray in 0...BikeShareData.count - 1 {
                                // scrollView.frame.size.width*1
                                let vehicleColor = BikeShareData[BikeShareArray]["vehicleColor"] as? String
                                // let lineLayer = CAShapeLayer()
                                let Startlayer = CAShapeLayer()

                                let Endlayer = CAShapeLayer()
                                let BikeShareimageName = "BIRD4"
                                let BikeShareimage = UIImage(named: BikeShareimageName)

                                let BikeSharePickup = UILabel()
                                let BikeSharevehicleStation = UILabel()
                                let BikeShareWalkingAboutandCo2 = UILabel()
                                let BikeSharebutton = UIButton()
                                let BikeSharevehicleStartTime = UILabel()
                                let BikeShareimageView = UIImageView(image: BikeShareimage!)
                                let pageControl = UIPageControl()




                                Startlayer.fillColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                RidescrollViewCell.layer.addSublayer(Startlayer)



                                Endlayer.fillColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                RidescrollViewCell.layer.addSublayer(Endlayer)




                                BikeShareimageView.contentMode = .scaleAspectFit
                                RidescrollViewCell.addSubview(BikeShareimageView)

                                //vehicleType






                                BikeSharePickup.textAlignment = .left //For center alignment
                                BikeSharePickup.text = BikeShareData[BikeShareArray]["VehiclePickupLocation"] as? String
                                BikeSharePickup.textColor = .darkGray
                                // lbl.backgroundColor = .lightGray//If required
                                BikeSharePickup.font = UIFont.setTarcRegular(size: 10)
                                RidescrollViewCell.addSubview(BikeSharePickup)



                                BikeSharevehicleStation.textAlignment = .left //For center alignment
                                BikeSharevehicleStation.text = BikeShareData[BikeShareArray]["vehicleStation"] as? String
                                BikeSharevehicleStation.textColor = .black
                                //BikeSharevehicleStation.backgroundColor = .darkGray//If required
                                BikeSharevehicleStation.font = UIFont.setTarcBold(size: 11.0)
                                BikeSharevehicleStation.lineBreakMode = NSLineBreakMode.byWordWrapping
                                BikeSharevehicleStation.numberOfLines = 0
                                RidescrollViewCell.addSubview(BikeSharevehicleStation)


                                BikeShareWalkingAboutandCo2.textAlignment = .left //For center alignment
                                BikeShareWalkingAboutandCo2.text = BikeShareData[BikeShareArray]["WalkingAboutandCo2"] as? String
                                BikeShareWalkingAboutandCo2.textColor = .darkGray
                                // BikeShareWalkingAboutandCo2.backgroundColor = .lightGray//If required
                                BikeShareWalkingAboutandCo2.font = UIFont.setTarcRegular(size: 10)
                                RidescrollViewCell.addSubview(BikeShareWalkingAboutandCo2)



                                BikeSharebutton.accessibilityHint = "\((BikeShareData[BikeShareArray]["vehicleType"] as? String)!)"
                                BikeSharebutton.backgroundColor = color.hexStringToUIColor(hex: vehicleColor!)
                                BikeSharebutton.setTitle("\((BikeShareData[BikeShareArray]["Fare"] as? String)!)", for: .normal)
                                BikeSharebutton.titleLabel!.font = UIFont.setTarcBold(size: 12.0)

                                BikeSharebutton.addTarget(self, action: #selector(RideAppOpen), for: .touchUpInside)
                                RidescrollViewCell.addSubview(BikeSharebutton)
                                cell.contentView.bringSubview(toFront: BikeSharebutton)

                                pageControl.numberOfPages = BikeShareData.count
                                pageControl.tintColor = UIColor.red
                                pageControl.pageIndicatorTintColor = UIColor.init(red: 211/255.0, green: 211/255.0, blue: 211/255.0, alpha: 1.0)
                                pageControl.currentPageIndicatorTintColor = color.hexStringToUIColor(hex: vehicleColor!)

                                RidescrollViewCell.addSubview(pageControl)
                                cell.contentView.bringSubview(toFront: RidescrollViewCell)


                                BikeSharevehicleStartTime.textAlignment = .left //For center alignment
                                BikeSharevehicleStartTime.text = BikeShareData[BikeShareArray]["vehicleTime"] as? String
                                BikeSharevehicleStartTime.textColor = .black
                                //BikeSharevehicleStation.backgroundColor = .darkGray//If required
                                BikeSharevehicleStartTime.font = UIFont.setTarcBold(size: 10.0)
                                RidescrollViewCell.addSubview(BikeSharevehicleStartTime)

                                if BikeShareArray == 0 {
                                    let lineLayer = CAShapeLayer()
                                    lineLayer.strokeColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                    lineLayer.lineWidth = 2
                                    // lineLayer.lineDashPattern = [4, 4]
                                    let path = CGMutablePath()
                                    path.addLines(between: [CGPoint(x: 14, y: 10),
                                        CGPoint(x: 14, y: cell.contentView.frame.height - 10)])
                                    lineLayer.path = path
                                    //  cell.LineView.layer.addSublayer(lineLayer)
                                    RidescrollViewCell.layer.addSublayer(lineLayer)
                                    Startlayer.path = UIBezierPath(roundedRect: CGRect(x: 9, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath
                                    Endlayer.path = UIBezierPath(roundedRect: CGRect(x: 9, y: RidescrollViewCell.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath
                                    BikeShareimageView.frame = CGRect(x: 30, y: 0, width: 20, height: cell.contentView.frame.height)

                                    for BikeShareArrayList in 0...BikeShareData.count - 1 {


                                        let BirimageName = BikeShareData[BikeShareArrayList]["vehicleTypeImage"] as? String


                                        print(60 * BikeShareArrayList)
                                        if BikeShareArrayList == 0 {

                                            if BikeShareArrayList != BikeShareArray {
                                        print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                                var Birdimage = UIImage(named: BirimageName!)

                                                Birdimage = Birdimage?.noir
                                                let BirdimageView = UIImageView(image: Birdimage!)
                                                BirdimageView.frame = CGRect(x: 60, y: 5, width: 15, height: 15)
                                                RidescrollViewCell.addSubview(BirdimageView)
                                            }
                                            else {
                                                let Birdimage = UIImage(named: BirimageName!)
                                                let BirdimageView = UIImageView(image: Birdimage!)
                                                BirdimageView.frame = CGRect(x: 60, y: 5, width: 15, height: 15)
                                                RidescrollViewCell.addSubview(BirdimageView)

                                            }






                                        }
                                        else if BikeShareArrayList == 1 {
                                            var Birdimage = UIImage(named: BirimageName!)
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 40)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                        print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                                Birdimage = Birdimage?.noir
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)
                                            BirdimageView.alpha = 0.5

                                        }
                                        else {
                                            var Birdimage = UIImage(named: BirimageName!)
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 80)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                    print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                                Birdimage = Birdimage?.noir
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)

                                        }





                                    }

                                    BikeSharePickup.frame = CGRect(x: 60, y: 20, width: cell.contentView.frame.width / 1.6, height: 20)
                                    BikeSharevehicleStation.frame = CGRect(x: 60, y: 35, width: cell.contentView.frame.width / 1.6, height: 35)
                                    BikeShareWalkingAboutandCo2.frame = CGRect(x: 60, y: BikeSharevehicleStation.frame.height - 5 + BikeSharevehicleStation.frame.origin.y, width: cell.contentView.frame.width / 1.6, height: 15)
                                    BikeSharebutton.frame = CGRect(x: 60, y: BikeShareWalkingAboutandCo2.frame.origin.y + BikeShareWalkingAboutandCo2.frame.height + 10, width: cell.contentView.frame.width / 1.6, height: 30)
                                    pageControl.frame = CGRect.init(x: 0, y: BikeSharebutton.frame.maxY+5, width: self.view.frame.size.width, height: 35)
                                    pageControl.currentPage = 0
                                    BikeSharevehicleStartTime.frame = CGRect(x: self.view.frame.width - 55, y: (cell.contentView.frame.height / 2) - 20, width: 53, height: 35)


                                }
                                else if BikeShareArray == 1 {

                                    let lineLayer = CAShapeLayer()
                                    lineLayer.strokeColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                    lineLayer.lineWidth = 2
                                    // lineLayer.lineDashPattern = [4, 4]
                                    let path = CGMutablePath()

                                    path.addLines(between: [CGPoint(x: RidescrollViewCell.frame.size.width * 1 + 14, y: 10),
                                        CGPoint(x: RidescrollViewCell.frame.size.width * 1 + 14, y: cell.contentView.frame.height - 10)])


                                    lineLayer.path = path
                                    //  cell.LineView.layer.addSublayer(lineLayer)
                                    RidescrollViewCell.layer.addSublayer(lineLayer)


                                    Startlayer.path = UIBezierPath(roundedRect: CGRect(x: RidescrollViewCell.frame.size.width * 1 + 9, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath



                                    Endlayer.path = UIBezierPath(roundedRect: CGRect(x: RidescrollViewCell.frame.size.width * 1 + 9, y: RidescrollViewCell.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath



                                    BikeShareimageView.frame = CGRect(x: RidescrollViewCell.frame.size.width * 1 + 30, y: 0, width: 20, height: cell.contentView.frame.height)


                                    //vehicleType

                                    for BikeShareArrayList in 0...BikeShareData.count - 1 {


                                        let BirimageName = BikeShareData[BikeShareArrayList]["vehicleTypeImage"] as? String
                                        var Birdimage = UIImage(named: BirimageName!)

                                        print(60 * BikeShareArrayList)
                                        if BikeShareArrayList == 0 {

                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            BirdimageView.frame = CGRect(x: RidescrollViewCell.frame.size.width * 1 + 60, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)
                                            BirdimageView.alpha = 0.5

                                        }
                                        else if BikeShareArrayList == 1 {
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 40) + Int(RidescrollViewCell.frame.size.width)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)

                                        }
                                        else {
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 80) + Int(RidescrollViewCell.frame.size.width * 1)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)
                                        }





                                    }



                                    BikeSharePickup.frame = CGRect(x: RidescrollViewCell.frame.size.width * 1 + 60, y: 20, width: cell.contentView.frame.width / 1.6, height: 20)


                                    BikeSharevehicleStation.frame = CGRect(x: RidescrollViewCell.frame.size.width * 1 + 60, y: 35, width: cell.contentView.frame.width / 1.6, height: 35)


                                    BikeShareWalkingAboutandCo2.frame = CGRect(x: RidescrollViewCell.frame.size.width * 1 + 60, y: BikeSharevehicleStation.frame.height - 5 + BikeSharevehicleStation.frame.origin.y, width: cell.contentView.frame.width / 1.6, height: 15)


                                    BikeSharebutton.frame = CGRect(x: RidescrollViewCell.frame.size.width * 1 + 60, y: BikeShareWalkingAboutandCo2.frame.origin.y + BikeShareWalkingAboutandCo2.frame.height + 10, width: cell.contentView.frame.width / 1.6, height: 30)

                                    pageControl.frame = CGRect.init(x: RidescrollViewCell.frame.size.width * 1 , y: BikeSharebutton.frame.maxY+5, width: self.view.frame.size.width, height: 35)
                                        pageControl.currentPage = 1
                                    BikeSharevehicleStartTime.frame = CGRect(x: RidescrollViewCell.frame.size.width * 1 + cell.contentView.frame.width - 55, y: (cell.contentView.frame.height / 2) - 20, width: 53, height: 35)


                                }
                                else if BikeShareArray == 2 {

                                    let lineLayer = CAShapeLayer()
                                    lineLayer.strokeColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                                    lineLayer.lineWidth = 2
                                    // lineLayer.lineDashPattern = [4, 4]
                                    let path = CGMutablePath()

                                    path.addLines(between: [CGPoint(x: RidescrollViewCell.frame.size.width * 2 + 14, y: 10),
                                        CGPoint(x: RidescrollViewCell.frame.size.width * 2 + 14, y: cell.contentView.frame.height - 10)])


                                    lineLayer.path = path
                                    //  cell.LineView.layer.addSublayer(lineLayer)
                                    RidescrollViewCell.layer.addSublayer(lineLayer)


                                    Startlayer.path = UIBezierPath(roundedRect: CGRect(x: RidescrollViewCell.frame.size.width * 2 + 9, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath



                                    Endlayer.path = UIBezierPath(roundedRect: CGRect(x: RidescrollViewCell.frame.size.width * 2 + 9, y: RidescrollViewCell.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath



                                    BikeShareimageView.frame = CGRect(x: RidescrollViewCell.frame.size.width * 2 + 30, y: 0, width: 20, height: cell.contentView.frame.height)


                                    //vehicleType

                                    for BikeShareArrayList in 0...BikeShareData.count - 1 {


                                        let BirimageName = BikeShareData[BikeShareArrayList]["vehicleTypeImage"] as? String
                                        var Birdimage = UIImage(named: BirimageName!)

                                        print(60 * BikeShareArrayList)
                                        if BikeShareArrayList == 0 {

                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            BirdimageView.frame = CGRect(x: RidescrollViewCell.frame.size.width * 2 + 60, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)

                                        }
                                        else if BikeShareArrayList == 1 {
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 40) + Int(RidescrollViewCell.frame.size.width * 2)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)
                                        }
                                        else {
                                            let BirdimageView = UIImageView(image: Birdimage!)
                                            let BikeshareX = (60 * (BikeShareArrayList + 1) - 80) + Int(RidescrollViewCell.frame.size.width * 2)
                                            BirdimageView.frame = CGRect(x: BikeshareX, y: 5, width: 15, height: 15)
                                            if BikeShareArrayList != BikeShareArray {
                                                Birdimage = Birdimage?.noir
                                                print("****************************************************************")
                                                print("\(BikeShareArrayList),\(BikeShareArray)")
                                            }
                                            RidescrollViewCell.addSubview(BirdimageView)
                                        }





                                    }



                                    BikeSharePickup.frame = CGRect(x: RidescrollViewCell.frame.size.width * 2 + 60, y: 20, width: cell.contentView.frame.width / 1.6, height: 20)


                                    BikeSharevehicleStation.frame = CGRect(x: RidescrollViewCell.frame.size.width * 2 + 60, y: 35, width: cell.contentView.frame.width / 1.6, height: 35)


                                    BikeShareWalkingAboutandCo2.frame = CGRect(x: RidescrollViewCell.frame.size.width * 2 + 60, y: BikeSharevehicleStation.frame.height - 5 + BikeSharevehicleStation.frame.origin.y, width: cell.contentView.frame.width / 1.6, height: 15)


                                    BikeSharebutton.frame = CGRect(x: RidescrollViewCell.frame.size.width * 2 + 60, y: BikeShareWalkingAboutandCo2.frame.origin.y + BikeShareWalkingAboutandCo2.frame.height + 10, width: cell.contentView.frame.width / 1.6, height: 30)
                                    pageControl.frame = CGRect.init(x: RidescrollViewCell.frame.size.width * 2 , y: BikeSharebutton.frame.maxY+5, width: self.view.frame.size.width, height: 35)
                                                                  pageControl.currentPage = 2

                                    BikeSharevehicleStartTime.frame = CGRect(x: RidescrollViewCell.frame.size.width * 2 + self.view.frame.width - 55, y: (cell.contentView.frame.height / 2) - 20, width: 53, height: 35)


                                }

                            }



                            return cell
                        }


                        else if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "Bike_BikeShare" {

                            print("************Bike RideShare ************ \(indexPath.row) -  \((googlereponseDirection[indexPath.row]["Index"] as? String)!)")
                            let sourcelat = googlereponseDirection[indexPath.row]["SourceLat"] as? Double

                            let sourceLng = googlereponseDirection[indexPath.row]["SourceLng"] as? Double
                            let DestinatonLat = googlereponseDirection[indexPath.row]["DestinationLat"] as? Double
                            let DestinationLng = googlereponseDirection[indexPath.row]["DestinationLng"] as? Double

                            print("\(sourcelat), \(sourceLng), \(DestinatonLat) \(DestinationLng)")
print(googlereponseDirection[indexPath.row])

                            (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: sourcelat!, SourceLng: sourceLng!, DestinationLat: DestinatonLat!, DestinationLng: DestinationLng!, Type: "BikeShare", TripColor: (googlereponseDirection[indexPath.row]["vehicleColor"] as? String)!)


                            let cell = tableView.dequeueReusableCell(withIdentifier: "BikeCellBikeShare", for: indexPath) as! BikeCellBikeShare

                            cell.selectionStyle = .none




                            // scrollView.frame.size.width*1
                            let vehicleColor = googlereponseDirection[indexPath.row]["vehicleColor"] as? String
                            // let lineLayer = CAShapeLayer()

                            let BikeShareimageName = "BIRD4"
                            let BikeShareimage = UIImage(named: BikeShareimageName)

                            let lineLayer = CAShapeLayer()
                            lineLayer.strokeColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                            lineLayer.lineWidth = 2
                            // lineLayer.lineDashPattern = [4, 4]
                            let path = CGMutablePath()
                            path.addLines(between: [CGPoint(x: 6, y: 10),
                                CGPoint(x: 6, y: cell.contentView.frame.height - 10)])
                            lineLayer.path = path
                            //  cell.LineView.layer.addSublayer(lineLayer)
                            cell.WalkingLineView.layer.addSublayer(lineLayer)

                            let Startlayer = CAShapeLayer()
                            Startlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath
                            Startlayer.fillColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                            cell.WalkingLineView.layer.addSublayer(Startlayer)

                            let Endlayer = CAShapeLayer()
                            Endlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: cell.contentView.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath
                            Endlayer.fillColor = color.hexStringToUIColor(hex: vehicleColor!).cgColor
                            cell.WalkingLineView.layer.addSublayer(Endlayer)


                            cell.WalkingImage.image = UIImage(named: (googlereponseDirection[indexPath.row]["vehicleType"] as? String)!)
                            //vehicleType
                            cell.PickupLine.text = googlereponseDirection[indexPath.row]["VehiclePickupLocation"] as? String
                            cell.WalkingAddress.text = googlereponseDirection[indexPath.row]["vehicleStation"] as? String
                            cell.Instrations.text = googlereponseDirection[indexPath.row]["WalkingInstructions"] as? String
                            cell.AboutMiles.text = googlereponseDirection[indexPath.row]["WalkingAboutandCo2"] as? String

                            cell.TotalMin.text = googlereponseDirection[indexPath.row]["vehicleTime"] as? String


                            cell.ActionBtn.accessibilityHint = "\((googlereponseDirection[indexPath.row]["vehicleType"] as? String)!)"
                            cell.ActionBtn.backgroundColor = color.hexStringToUIColor(hex: vehicleColor!)
                            cell.ActionBtn.setTitle("\((googlereponseDirection[indexPath.row]["Fare"] as? String)!)", for: .normal)
                            cell.ActionBtn.titleLabel!.font = UIFont.setTarcBold(size: 12.0)

                            cell.ActionBtn.addTarget(self, action: #selector(RideAppOpen), for: .touchUpInside)
                             // cell.contentView.bringSubview(toFront: cell.ActionBtn)
                            cell.departime.text = googlereponseDirection[indexPath.row]["WakingStartTime"] as? String

                          

                            return cell
                        }


                        else if googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String == "Bike_Walking" {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "WalkingCellBikeShare", for: indexPath) as! WalkingCellBikeShare
                            cell.WalkingPanel.isHidden = false
                            cell.selectionStyle = .none
                            print("************Bike WalkingCell ************")
                            // print(googlereponseDirection[indexPath.row]["SourceLat"])
                            (parent as? TripMapViewController)?.zoom(to: "1")
                            let sourcelat = googlereponseDirection[indexPath.row]["SourceLat"] as? Double
                            print(sourcelat)
                            print(indexPath.row)
                            let sourceLng = googlereponseDirection[indexPath.row]["SourceLng"] as? Double
                            let DestinatonLat = googlereponseDirection[indexPath.row]["DestinationLat"] as? Double
                            let DestinationLng = googlereponseDirection[indexPath.row]["DestinationLng"] as? Double
                            print("\((sourcelat)!), \((sourceLng)!), \((DestinatonLat)!) \((DestinationLng))")
                            (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: sourcelat!, SourceLng: sourceLng!, DestinationLat: DestinatonLat!, DestinationLng: DestinationLng!, Type: "Walking", TripColor: "#E53638")


                            //                googlereponseDirectionList.setValue("\(Passingdata.startLocation!.lat!)", forKey: "SourceLat")
                            //                                googlereponseDirectionList.setValue("\(Passingdata.startLocation!.lng!)", forKey: "SourceLng")
                            //                                googlereponseDirectionList.setValue("\(Passingdata.endLocation!.lat!)", forKey: "DestinationLat")
                            //                                googlereponseDirectionList.setValue("\(Passingdata.endLocation!.lng!)", forKey: "DestinationLng")
                            //

                            let lineLayer = CAShapeLayer()
                            lineLayer.strokeColor = UIColor.gray.cgColor
                            lineLayer.lineWidth = 2
                            lineLayer.lineDashPattern = [4, 4]
                            let path = CGMutablePath()
                            path.addLines(between: [CGPoint(x: 6, y: 10),
                                CGPoint(x: 6, y: cell.contentView.frame.height - 10)])
                            lineLayer.path = path
                            //  cell.LineView.layer.addSublayer(lineLayer)
                            cell.WalkingLineView.layer.addSublayer(lineLayer)

                            let Startlayer = CAShapeLayer()
                            Startlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath
                            Startlayer.fillColor = UIColor.gray.cgColor
                            cell.WalkingLineView.layer.addSublayer(Startlayer)

                            let Endlayer = CAShapeLayer()
                            Endlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: cell.contentView.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath
                            Endlayer.fillColor = UIColor.gray.cgColor
                            cell.WalkingLineView.layer.addSublayer(Endlayer)

                            cell.WalkingImage.image = UIImage(named: (googlereponseDirection[indexPath.row]["TransitImage"] as? String)!)
                            // cell.waitTime.text = googlereponseDirection[indexPath.row]["WalkingWaitingTime"] as? String
                            cell.Instrations.text = googlereponseDirection[indexPath.row]["WalkingWaitingTime"] as? String
                            cell.CalMiles.text = googlereponseDirection[indexPath.row]["SecondAddress"] as? String
                            cell.WalkingAddress.text = googlereponseDirection[indexPath.row]["FirstAddress"] as? String
                            cell.TotalMin.text = googlereponseDirection[indexPath.row]["Timing"] as? String






                            return cell




                        }
                        //Bike_Walking
                            else {
                                let cell = tableView.dequeueReusableCell(withIdentifier: "WalkingCell", for: indexPath) as! WalkingCell
                                cell.WalkingPanel.isHidden = false

                            print("\( googlereponseDirection[indexPath.row]["ModeOFTransit"] as? String)")

                                print("************ WalkingCell ************")
                                // print(googlereponseDirection[indexPath.row]["SourceLat"])

                                let sourcelat = googlereponseDirection[indexPath.row]["SourceLat"] as? Double
                             //   print(sourcelat)
                                print(indexPath.row)
                                let sourceLng = googlereponseDirection[indexPath.row]["SourceLng"] as? Double
                                let DestinatonLat = googlereponseDirection[indexPath.row]["DestinationLat"] as? Double
                                let DestinationLng = googlereponseDirection[indexPath.row]["DestinationLng"] as? Double

                                (self.parent as? TripMapViewController)?.DrawPolyline(SourceLat: sourcelat!, SourceLng: sourceLng!, DestinationLat: DestinatonLat!, DestinationLng: DestinationLng!, Type: "Walking", TripColor: "#E53638")

                            
                         //   (self.parent as? TripMapViewController)?.addmarkerfrom(LatforBike: (sourcelat)!, LongforBike: (sourceLng)!, vechelType: (BikeShareData[0]["vehicleType"] as? String)!, Address: (BikeShareData[0]["vehicleStation"] as? String)!)

                            
        //                googlereponseDirectionList.setValue("\(Passingdata.startLocation!.lat!)", forKey: "SourceLat")
        //                                googlereponseDirectionList.setValue("\(Passingdata.startLocation!.lng!)", forKey: "SourceLng")
        //                                googlereponseDirectionList.setValue("\(Passingdata.endLocation!.lat!)", forKey: "DestinationLat")
        //                                googlereponseDirectionList.setValue("\(Passingdata.endLocation!.lng!)", forKey: "DestinationLng")
        //
                            

                                let lineLayer = CAShapeLayer()
                                lineLayer.strokeColor = UIColor.gray.cgColor
                                lineLayer.lineWidth = 2
                                lineLayer.lineDashPattern = [4, 4]
                                let path = CGMutablePath()
                                path.addLines(between: [CGPoint(x: 6, y: 10),
                                    CGPoint(x: 6, y: cell.contentView.frame.height - 10)])
                                lineLayer.path = path
                                //  cell.LineView.layer.addSublayer(lineLayer)
                                cell.WalkingLineView.layer.addSublayer(lineLayer)

                                let Startlayer = CAShapeLayer()
                                Startlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: 3, width: 10, height: 10), cornerRadius: 50).cgPath
                                Startlayer.fillColor = UIColor.gray.cgColor
                                cell.WalkingLineView.layer.addSublayer(Startlayer)

                                let Endlayer = CAShapeLayer()
                                Endlayer.path = UIBezierPath(roundedRect: CGRect(x: 1, y: cell.WalkingLineView.frame.height - 13, width: 10, height: 10), cornerRadius: 50).cgPath
                                Endlayer.fillColor = UIColor.gray.cgColor
                                cell.WalkingLineView.layer.addSublayer(Endlayer)

                                cell.WalkingImage.image = UIImage(named: (googlereponseDirection[indexPath.row]["TransitImage"] as? String)!)
                                cell.waitTime.text = googlereponseDirection[indexPath.row]["WalkingWaitingTime"] as? String
                                cell.CalMiles.text = googlereponseDirection[indexPath.row]["SecondAddress"] as? String
                                cell.WalkingAddress.text = googlereponseDirection[indexPath.row]["FirstAddress"] as? String
                                cell.TotalMin.text = googlereponseDirection[indexPath.row]["Timing"] as? String






                                return cell




                        }
                    }
                    else {
                        print("************ Not Zero Transit ************")
                        let cell = tableView.dequeueReusableCell(withIdentifier: "TripResultCell", for: indexPath) as! TripResultCell
                        return cell

                    }







                    //cell.LineView.addSubview(circle)



                }

    @IBAction func OpenApp(_ sender: Any) {
        
        if isLimeredirect == "Bird" {

                      let app2Url: URL = URL(string: "bird://")!

                      if UIApplication.shared.canOpenURL(app2Url) {
                          UIApplication.shared.openURL(app2Url)
                      }
                      else {

                          UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/us/app/bird-enjoy-the-ride/id1260842311?_branch_match_id=607142830204928584")! as URL)



                      }

                  }
                  else if isLimeredirect == "Lime" {

                      let app2Url: URL = URL(string: "lime://")!

                      if UIApplication.shared.canOpenURL(app2Url) {
                          UIApplication.shared.openURL(app2Url)
                      }
                      else {

                          UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/limebike-your-ride-anytime/id1199780189?ls=1")! as URL)
                      }





                  }
          else if isLimeredirect == "LouVelo" {

              let app2Url: URL = URL(string: "LouVelo://")!

              if UIApplication.shared.canOpenURL(app2Url) {
                  UIApplication.shared.openURL(app2Url)
              }
              else {

                  UIApplication.shared.openURL(NSURL(string: "https://louvelo.com/members/login")! as URL)
              }





          }
                  else if isLimeredirect == "Bolt" {

                      let app2Url: URL = URL(string: "bolt://")!

                      if UIApplication.shared.canOpenURL(app2Url) {
                          UIApplication.shared.openURL(app2Url)
                      }
                      else {

                          UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/bolt-bolt-there-now/id1388810070")! as URL)
                      }
                  }
        if SourceDestination.DisplayType == "Transit"{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }
        
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            //  updatePage(scrollView: scrollView)
            return
        }

        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            if firstTimeScrollDisable == true
            {
                firstTimeScrollDisable = false
            }
            else
            {
            updateCell(scrollView: scrollView)
            }
            return
        }
        func updateCell(scrollView: UIScrollView) {

            if scrollView != RidescrollView {
                let pageWidth: CGFloat = scrollView.frame.width
                let current: CGFloat = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
                currentPage = Int(current)
                print("\(scrollView.tag) - CellUpdate")
                print("\(currentPage) - CurrentPage")
                print(googlereponseDirection[scrollView.tag])
                googlereponseDirection[scrollView.tag]["ScrollIndex"] = "0"

                if currentPage != 0
                    {
//         if currentPage == 0 {
//
//            print("CellUpdate")
//
//                      }
//        else if currentPage == 1 {

                    print("CellUpdate")
                        print(googlereponseDirection[scrollView.tag])
                    let ChangeData = googlereponseDirection[scrollView.tag]["BikeShareArray"] as! [[String: Any]]
                    //  ChangeData["count"] = 8
                        googlereponseDirection[scrollView.tag]["ScrollIndex"] = "1"
                        print(googlereponseDirection[scrollView.tag])

                    let walkingWaitingTime = ChangeData[currentPage]["WaitingTime"] as? String
                    let SecondAddress = ChangeData[currentPage]["WalkingAboutandCaloties"] as? String
                    let FirstAddress = ChangeData[currentPage]["WalkingInstructions"] as? String
                    let Timing = ChangeData[currentPage]["WakingStartTime"] as? String


//            BikeShareArrayList.setValue("Bolt", forKey: "vehicleType")
//            BikeShareArrayList.setValue("#F7DD4B", forKey: "vehicleColor")
//            BikeShareArrayList.setValue("Bolt", forKey: "vehicleTypeImage")
//            BikeShareArrayList.setValue("\(walkingInstration)", forKey: "WalkingInstructions")
//            BikeShareArrayList.setValue("\(WalkingAboutandCaloties)", forKey: "WalkingAboutandCaloties")
//            BikeShareArrayList.setValue("\(waitingTravel)", forKey: "WaitingTime")
//            BikeShareArrayList.setValue("\(TransitTime)", forKey: "WakingStartTime")
//
//            BikeShareArrayList.setValue("Pickup your Bolt Vehicle from", forKey: "VehiclePickupLocation")
//            BikeShareArrayList.setValue("\(ReveseGeoCodeAddress)", forKey: "vehicleStation")
//            BikeShareArrayList.setValue("", forKey: "Fare")
//            BikeShareArrayList.setValue("\(WalkingAboutandCo2)", forKey: "WalkingAboutandCo2")
//            BikeShareArrayList.setValue("\(BikeShareTime)", forKey: "vehicleTime")
//            BikeShareArrayList.setValue("\(BikeShareEndTime)", forKey: "BikeShareEndTime")
//

                    print("\((walkingWaitingTime)!)")
                    print("\((SecondAddress)!)")
                    print("\((FirstAddress)!)")
                    print("\((Timing)!)")
                    googlereponseDirection[scrollView.tag - 1]["WalkingWaitingTime"] = "\((walkingWaitingTime)!)"
                    googlereponseDirection[scrollView.tag - 1]["SecondAddress"] = "\((SecondAddress)!)"
                    googlereponseDirection[scrollView.tag - 1]["FirstAddress"] = "\((FirstAddress)!)"
                    googlereponseDirection[scrollView.tag - 1]["Timing"] = "\((Timing)!)"

                    let indexPath = NSIndexPath(row: scrollView.tag - 1, section: 0)
                    tableView.reloadRows(at: [(indexPath as IndexPath)], with: UITableView.RowAnimation.none)



                }
else
                {
                    print("ARUNCRASHCHECk==>SCROLL\(scrollView.tag - 1)")
                      DispatchQueue.main.async {
                    let indexPath = NSIndexPath(row: scrollView.tag - 1, section: 0)
                    
                        self.tableView.reloadRows(at: [(indexPath as IndexPath)], with: UITableView.RowAnimation.none)
                   // tableView.reloadData()
                    }
                }

            }
            else if scrollView == RidescrollView
            {
                let pageWidth: CGFloat = scrollView.frame.width
                let current: CGFloat = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
                currentPage = Int(current)
                print("\(currentPage) - CurrentPage")
                googlereponseDirection[scrollView.tag]["ScrollIndex"] = "0"

                if currentPage != 0
                                 {
                                    googlereponseDirection[scrollView.tag]["ScrollIndex"] = "1"

                }
                let indexPath = NSIndexPath(row: scrollView.tag - 1, section: 0)
                tableView.reloadRows(at: [(indexPath as IndexPath)], with: UITableView.RowAnimation.none)
               // tableView.reloadData()
            }
        }

        // MARK: - UITableViewDelegate

        @objc func RideAppOpen (sender: UIButton) {
            let section = sender.accessibilityHint
            let SenderTag = sender.tag

            let sourcelat = googlereponseDirection[SenderTag]["SourceLat"] as? Double
            let sourceLng = googlereponseDirection[SenderTag]["SourceLng"] as? Double
            let DestinatonLat = googlereponseDirection[SenderTag]["DestinationLat"] as? Double
            let DestinationLng = googlereponseDirection[SenderTag]["DestinationLng"] as? Double

            //let ResponseData = googlereponseDirection[SenderTag][]
            if section == "Bird" {

                let app2Url: URL = URL(string: "bird://")!

                if UIApplication.shared.canOpenURL(app2Url) {
                    UIApplication.shared.openURL(app2Url)
                }
                else {

                    UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/us/app/bird-enjoy-the-ride/id1260842311?_branch_match_id=607142830204928584")! as URL)



                }

            }
            else if section == "Lime" {

                let app2Url: URL = URL(string: "lime://")!

                if UIApplication.shared.canOpenURL(app2Url) {
                    UIApplication.shared.openURL(app2Url)
                }
                else {

                    UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/limebike-your-ride-anytime/id1199780189?ls=1")! as URL)
                }





            }
    else if section == "LouVelo" {

        let app2Url: URL = URL(string: "LouVelo://")!

        if UIApplication.shared.canOpenURL(app2Url) {
            UIApplication.shared.openURL(app2Url)
        }
        else {

            UIApplication.shared.openURL(NSURL(string: "https://louvelo.com/members/login")! as URL)
        }





    }
            else if section == "Bolt" {

                let app2Url: URL = URL(string: "bolt://")!

                if UIApplication.shared.canOpenURL(app2Url) {
                    UIApplication.shared.openURL(app2Url)
                }
                else {

                    UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/bolt-bolt-there-now/id1388810070")! as URL)
                }
            }
            else if section == "Uber" {
                let pickupLocation = CLLocation(latitude: sourcelat!, longitude: sourceLng!)
                let dropoffLocation = CLLocation(latitude: DestinatonLat!, longitude: DestinationLng!)
                print("\(pickupLocation) and \(dropoffLocation)")


                let builder = RideParametersBuilder()
                builder.pickupLocation = pickupLocation
                builder.dropoffLocation = dropoffLocation
                let rideParameters = builder.build()

                let deeplink = RequestDeeplink(rideParameters: rideParameters, fallbackType: .mobileWeb)
                deeplink.execute()

            }
            else if section == "Lyft" {


                let app2Url: URL = URL(string: "lyft://")!

                if UIApplication.shared.canOpenURL(app2Url) {
                    open(scheme: "lyft://ridetype?id=lyft&pickup[latitude]=\((sourcelat)!)&pickup[longitude]=\((sourceLng)!)&destination[latitude]=\((DestinatonLat)!)&destination[longitude]=\((DestinationLng)!)")


                }
                else {

                    UIApplication.shared.openURL(NSURL(string: "https://www.lyft.com/signup/SDKSIGNUP")! as URL)

                }
            }
        }
        func open(scheme: String) {
            if let url = URL(string: scheme) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }


//}


        func dateconvert(Datevalue: Double) -> String {
            let date = Date(timeIntervalSince1970: Datevalue)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "dd/MM/yyyy" //Specify your format that you want
            let strDate = dateFormatter.string(from: date)
            return strDate
        }
        func hourminConverter(distValue: Int) -> String {
            let hourString: Int = distValue / 3600
            let minString: Int = (distValue % 3600) / 60
            if hourString > 0
                {
                return "\(hourString) hr \(minString) min"

            }
            else
            {
                return "\(minString) min"
            }
        }

        func hourminConverterForUber(distValue: Int) -> String {
            let minString: Int = (distValue % 3600) / 60

            return "\(minString)"

        }

        func unixtodate(unixtimeInterval: Double) -> String {
            let date = Date(timeIntervalSince1970: unixtimeInterval)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "MM-dd-yyyy" //Specify your format that you want
            let strDate = dateFormatter.string(from: date)
            return strDate
        }

        func convertDateFormatter(date1: String, Date2: String) -> Int {

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mma"//this your string date format
            dateFormatter.timeZone = NSTimeZone(name: "EST") as TimeZone!
            dateFormatter.locale = NSLocale.current
            let start = dateFormatter.date(from: date1)!

            let enddt = dateFormatter.date(from: Date2)!
            let calendar = Calendar.current
            let unitFlags = Set<Calendar.Component>([.second])
            let datecomponenets = calendar.dateComponents(unitFlags, from: start, to: enddt)
            let seconds = datecomponenets.second
            print("Seconds: \((seconds)!)")


            return seconds!
        }
        func dateconvert(Datevalue: Double, WalkingTimeinterval: Int, addsub: String) -> String {
            var date = Date(timeIntervalSince1970: Datevalue)
            if addsub == "add"
                {
                date = date.addingTimeInterval(TimeInterval(WalkingTimeinterval))

            }
            else if(addsub == "sub") {
                date = date.addingTimeInterval(-TimeInterval(WalkingTimeinterval))
            }
            else {
                date = date.addingTimeInterval(-TimeInterval(WalkingTimeinterval))
            }
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "h:mm a" //Specify your format that you want
            let strDate = dateFormatter.string(from: date as Date)
            return strDate
        }
        func DateFormatcheck(Data24Hr: String) -> String {

            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")

            dateFormatter.dateFormat = "H:mm:ss"
            if let inDate = dateFormatter.date(from: Data24Hr) {
                dateFormatter.dateFormat = "h:mm a"
                let outTime = dateFormatter.string(from: inDate)
                print("in \(time)")
                print("out \(outTime)")
                //  Data24Hr = outTime

                return outTime
            }
            else {

                return Data24Hr

            }

            // return Date12Hr
        }
        func datasubraction(arrivalDate: Double, DepartDate: Double, Walkingmin: Int) -> String {
            let datetime1 = Date(timeIntervalSince1970: arrivalDate)
            let datetime2 = Date(timeIntervalSince1970: DepartDate)


            var elapsedTime = datetime2.timeIntervalSince(datetime1)
            print(elapsedTime)
            elapsedTime = elapsedTime - Double(Walkingmin)
            print(elapsedTime)
            elapsedTime = elapsedTime * 1000
            print(elapsedTime)
            let result = convertTime(miliseconds: Int(elapsedTime))
            print(result)
            return result
        }
        func ConvertMiles(Meter: Int) -> String {



            return ""
        }
        func convertTime(miliseconds: Int) -> String {

            var seconds: Int = 0
            var minutes: Int = 0
            var hours: Int = 0
            var days: Int = 0
            var secondsTemp: Int = 0
            var minutesTemp: Int = 0
            var hoursTemp: Int = 0

            if miliseconds < 1000 {
                return ""
            } else if miliseconds < 1000 * 60 {
                seconds = miliseconds / 1000
                return "\(seconds) sec"
            } else if miliseconds < 1000 * 60 * 60 {
                secondsTemp = miliseconds / 1000
                minutes = secondsTemp / 60
                seconds = (miliseconds - minutes * 60 * 1000) / 1000
                return "\(minutes) min \(seconds) sec"
            } else if miliseconds < 1000 * 60 * 60 * 24 {
                minutesTemp = miliseconds / 1000 / 60
                hours = minutesTemp / 60
                minutes = (miliseconds - hours * 60 * 60 * 1000) / 1000 / 60
                seconds = (miliseconds - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
                return "\(hours) hr, \(minutes) min \(seconds) sec"
            } else {
                hoursTemp = miliseconds / 1000 / 60 / 60
                days = hoursTemp / 24
                hours = (miliseconds - days * 24 * 60 * 60 * 1000) / 1000 / 60 / 60
                minutes = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000) / 1000 / 60
                seconds = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
                return "\(days) days, \(hours) hr, \(minutes) min \(seconds) sec"
            }
        }
        func getAmountRounded(_ floatAmount: Double, withRoundOff roundOff: Int) -> Double {
            var floatAmount = floatAmount
            let newCalc = String(format: "%.2f", floatAmount)
            print("\(newCalc)")
            let doubleValue = Double(newCalc) ?? 0.0 * 100.00
            var intValue = Int(String(format: "%.2f", doubleValue)) ?? 0
            let intLastDigit = intValue % roundOff
            if intLastDigit <= roundOff / 2 {
                intValue = intValue - intLastDigit
            } else {
                intValue = intValue + (roundOff - intLastDigit)
            }
            floatAmount = Double(intValue) / 100

            return floatAmount
        }

    }

    extension UIColor {

        func lighters(by percentage: CGFloat = 30.0) -> UIColor? {
            return self.adjusts(by: abs(percentage))
        }

        func darkers(by percentage: CGFloat = 30.0) -> UIColor? {
            return self.adjusts(by: -1 * abs(percentage))
        }

        func adjusts(by percentage: CGFloat = 30.0) -> UIColor? {
            var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
            if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
                return UIColor(red: min(red + percentage / 100, 1.0),
                    green: min(green + percentage / 100, 1.0),
                    blue: min(blue + percentage / 100, 1.0),
                    alpha: alpha)
            } else {
                return nil
            }
        }



    }
    class Circle: UIView {
        var strokeColor: UIColor
        var fillColor: UIColor
        init(frame: CGRect, strokeColor: UIColor, fillColor: UIColor = .clear) {
            self.strokeColor = strokeColor
            self.fillColor = fillColor
            super.init(frame: frame)
        }

        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        override func draw(_ rect: CGRect) {
            let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.width / 2, y: frame.height / 2), radius: frame.height / 2, startAngle: CGFloat(0), endAngle: CGFloat.pi * 2, clockwise: true)
            strokeColor.setStroke()
            fillColor.setFill()
            circlePath.lineWidth = 1
            circlePath.stroke()
        }

    }

    extension UIImageView {
        func setImageColor(color: UIColor) {
            let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
            self.image = templateImage
            self.tintColor = color
        }
    }
    extension CLLocation {
        func geocode(completion: @escaping (_ placemark: [CLPlacemark]?, _ error: Error?) -> Void) {
            CLGeocoder().reverseGeocodeLocation(self, completionHandler: completion)
        }
    }


    extension UIImage {
        var noir: UIImage? {
            let context = CIContext(options: nil)
            guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
            currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
            if let output = currentFilter.outputImage,
                let cgImage = context.createCGImage(output, from: output.extent) {
                return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
            }
            return nil
        }
    }

    extension String {
        public var withoutHtml: String {
            guard let data = self.data(using: .utf8) else {
                return self
            }

            let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
                    .documentType: NSAttributedString.DocumentType.html,
                    .characterEncoding: String.Encoding.utf8.rawValue
            ]

            guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
                return self
            }

            return attributedString.string
        }
    }
