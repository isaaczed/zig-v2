//
//  TicketList.swift
//  ZIG
//
//  Created by Isaac on 24/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class TicketList: Mappable {
    var Message:String?
    var count:Int?
    var Tickets:[Ticket_list]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Message <- map["Message"]
        Tickets <- map["Tickets"]
        count <- map["Count"]
    }
    
    
}
class Ticket_list: Mappable {
    var C_FromAddress:String?
    var C_DestinationAddress:String?
    var C_Amount:Double?
    var C_transaction_Date:String?
    var subset:[TicketChild]?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        C_FromAddress <- map["FromAddress"]
        C_DestinationAddress <- map["DestinationAddress"]
        C_Amount <- map["Amount"]
        C_transaction_Date <- map["TransactionDate"]
        subset <- map["Subsets"]
        
        
    }
    
    
}

class TicketChild: Mappable {
    var Amount:Float?
    var AgencyId:Int?
    var CategoryId:Int?
    var CategoryName:String?
    var AgencyName:String?
    var isActive:Bool?
    var isValid:Bool?
    var RouteId:String?
    var TripId:String?
    var ProfileName:String?
    var TicketId:Int?
    var FromAddress:String?
    var DestinationAddress:String?
    var RemainingTime:Int?
    var ProfileId:Int?
    var Expirydate:String?
    var status:Int?
    var Refund:String = ""
    var FareType:Int = 0
    var ActivatedDate:String = ""
    var Reactivatecount:Int = 0
    
    
    
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        Amount <- map["Amount"]
        AgencyId <- map["AgencyId"]
        CategoryId <- map["CategoryId"]
        CategoryName <- map["CategoryName"]
        AgencyName <- map["AgencyName"]
        isActive <- map["isActive"]
        isValid <- map["isValid"]
        RouteId <- map["RouteId"]
        TripId <- map["TripId"]
        ProfileName <- map["ProfileName"]
        TicketId <- map["TicketId"]
        FromAddress <- map["FromAddress"]
        DestinationAddress <- map["DestinationAddress"]
        RemainingTime <- map["RemainingTime"]
        ProfileId <- map["ProfileId"]
        Expirydate <- map["Expirydate"]
        status <- map["status"]
        Refund <- map["Refund"]
        FareType <- map["Fareid"]
        ActivatedDate <- map["Activateddate"]
        Reactivatecount <- map["Reactivatecount"]
        
        
    }
    
}


class TicketRealmModelNewNew: Object {
    @objc dynamic var Amount : Float = 0
    @objc dynamic var AgencyID = 0
    @objc dynamic var CategoryID = 0
    @objc dynamic var CategoryName = ""
    @objc dynamic var AgencyName = ""
    @objc dynamic var IsValid : Bool = false
    @objc dynamic var IsActive : Bool = false
    @objc dynamic var RouteID  = ""
    @objc dynamic var TripID = ""
    @objc dynamic var ProfileName = ""
    @objc dynamic var TicketID = 0
    @objc dynamic var FromAddress = ""
    @objc dynamic var DestinationAddress = ""
    @objc dynamic var RemainingTime = 0
    @objc dynamic var ProfileId = 0
    @objc dynamic var ExpriyDate = ""
    @objc dynamic var Refund = ""
    @objc dynamic var Status = 0
    @objc dynamic var CreatedDate = ""
    @objc dynamic var ActivatedDate = ""
    @objc dynamic var FareType = 0
    @objc dynamic var Reactivatecount = 0
    
    override class func primaryKey() -> String? {
        return "TicketID"
    }
    
}
