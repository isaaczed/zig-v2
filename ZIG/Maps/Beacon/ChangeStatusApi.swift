//
//  ChangeStatusApi.swift
//  ZIG
//
//  Created by Isaac on 25/06/19.
//  Copyright © 2019 Isaac. All rights reserved.
//


import Foundation
import RealmSwift
import ObjectMapper

class ChangeStatusDataNew: Object {
    @objc dynamic var id:Int = 0
    @objc dynamic var Latitude:Double = 0
    @objc dynamic var Longitude:Double = 0
    @objc dynamic var TicketId = 0
    @objc dynamic var RouteID = ""
    @objc dynamic var Message = ""
    @objc dynamic var DateValue =  ""
    @objc dynamic var isUpdated = false
    @objc dynamic var BeaconMacID = ""
    override class func primaryKey() -> String? {
        return "id"
    }
}
class ChangeStatusData: Object {
   
    @objc dynamic var Latitude:Double = 0
    @objc dynamic var Longitude:Double = 0
    @objc dynamic var TicketId = 0
    @objc dynamic var RouteID = ""
    @objc dynamic var Message = ""
    @objc dynamic var DateValue =  ""
    @objc dynamic var isUpdated = false
    @objc dynamic var BeaconMacID = ""
    override class func primaryKey() -> String? {
        return "TicketId"
    }
}




class statuschangeapiVerify: Mappable {
    var Message:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Message <- map["Message"]
    }
    
    
}
