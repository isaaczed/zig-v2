//
//  Realm.swift
//  ZIG
//
//  Created by Isaac on 24/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import RealmSwift


class BeaconRealmModel: Object {

    @objc dynamic var MacID = ""
    
    
    override class func primaryKey() -> String? {
        return "MacID"
    }
    
}


