//
//  WhatsNearMePanelViewController.swift
//  ZIG
//
//  Created by Isaac on 30/08/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import MapKit
import PullUpController
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import SafariServices
import SwiftyJSON
import XLActionController



class WhatsNearMePanelViewController: PullUpController {
    

        
        @IBOutlet weak var RouteType: UIButton!
        @IBOutlet weak var BusStopType: UIButton!
        
        enum InitialState {
            case contracted
            case expanded
        }
        var timer : Timer?
        var loaderView = UIView()
        var backgroundBlurView = UIView()
        var routeTitleString = NSString()
        var routeString = NSString()
        var SchudleArrayList = [[String:Any]]()
        var SchudleArrayValue : NSMutableDictionary = [:]
    
        
        var NearByItems = [[String:Any]]()
        var selectedLine:Bool = false
    
        var initialState: InitialState = .contracted
        var LineSearching = [mapsandSchudleList]()
        var scheduleParts = [BusStopsSchedule]()
        var searching = false
        
        // MARK: - IBOutlets
        
    
        @IBOutlet private weak var visualEffectView: UIVisualEffectView!
        @IBOutlet private weak var searchBoxContainerView: UIView!
        @IBOutlet private weak var searchSeparatorView: UIView! {
            didSet {
                searchSeparatorView.layer.cornerRadius = searchSeparatorView.frame.height/2
            }
        }
        @IBOutlet private weak var firstPreviewView: UIView!
        @IBOutlet private weak var secondPreviewView: UIView!
        @IBOutlet private weak var tableView: UITableView!
        var route_list = [mapsandSchudleList]()
        var initialPointOffset: CGFloat {
            switch initialState {
            case .contracted:
                return searchBoxContainerView?.frame.height ?? 0
            case .expanded:
                return pullUpControllerPreferredSize.height
            }
        }
        
        private var locations = [(title: String, location: CLLocationCoordinate2D)]()
        
        public var portraitSize: CGSize = .zero
        public var landscapeFrame: CGRect = .zero
        
        // MARK: - Lifecycle
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            if let lastStickyPoint = self.pullUpControllerAllStickyPoints.last {
                            self.pullUpControllerMoveToVisiblePoint(lastStickyPoint, animated: true, completion: nil)
            
                        }
            //        PDF.layer.zPosition = 1;
            //        UIApplication.shared.keyWindow!.bringSubview(toFront: searchBoxContainerView)
            //        self.searchBoxContainerView.sendSubview(toBack: firstPreviewView)
            routeTitleString = ""
            setupDataSource()
            LimesetupDataSource()
            //RouteType.addRightBorder(borderColor: UIColor.black, borderWidth: 1.0)
            portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                                  height: secondPreviewView.frame.maxY)
            landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 300)
            
            tableView.attach(to: self)
           
           
            
          
        }
    
        

        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            
            view.layer.cornerRadius = 12
        }
        
        override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
            //        print("will move to \(stickyPoint)")
        }
        
        override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
            //        print("did move to \(stickyPoint)")
        }
        
        override func pullUpControllerDidDrag(to point: CGFloat) {
            //        print("did drag to \(point)")
        }
        func setShape(TripID:Int){
            
        }
    private func LimesetupDataSource() {
        
        
         let NearByItemsList : NSMutableDictionary = [:]
        
        NearByItemsList.setValue("Bike", forKey: "TransitMode")
        NearByItemsList.setValue("Lime", forKey: "RouteName")
        NearByItemsList.setValue("7107 Astin CT Louisville, KY 40219", forKey: "Address")
        NearByItemsList.setValue("20", forKey: "BikeAvaliable")
        NearByItemsList.setValue("10", forKey: "DocksAvaliable")

        NearByItemsList.setValue("Scooter", forKey: "BikeType")
        NearByItemsList.setValue("", forKey: "DepartETA")
        NearByItemsList.setValue("", forKey: "DepartETAMetric")
        NearByItemsList.setValue(false, forKey: "RealTime")
        NearByItemsList.setValue("#69DF39", forKey: "RouteColor")
        NearByItemsList.setValue("#FFFFFF", forKey: "RouteTextColor")
        
        
        self.NearByItems.append(NearByItemsList as! [String : Any])
    }
    private func setupDataSource() {
        
        
        let NearByItemsList : NSMutableDictionary = [:]
        
        NearByItemsList.setValue("Bus", forKey: "TransitMode")
        NearByItemsList.setValue("60X", forKey: "RouteName")
        NearByItemsList.setValue("7107 Astin CT Louisville, KY 40219", forKey: "Address")
        NearByItemsList.setValue("", forKey: "BikeAvaliable")
         NearByItemsList.setValue("", forKey: "DocksAvaliable")
        NearByItemsList.setValue("", forKey: "BikeType")
        NearByItemsList.setValue("10", forKey: "DepartETA")
        NearByItemsList.setValue("min", forKey: "DepartETAMetric")
        NearByItemsList.setValue(true, forKey: "RealTime")
        NearByItemsList.setValue("#E49581", forKey: "RouteColor")
        NearByItemsList.setValue("#FFFFFF", forKey: "RouteTextColor")
        
        
        self.NearByItems.append(NearByItemsList as! [String : Any])
    }
        // MARK: - PullUpController
        
        override var pullUpControllerPreferredSize: CGSize {
            return portraitSize
        }
        
        override var pullUpControllerPreferredLandscapeFrame: CGRect {
            return landscapeFrame
        }
        
        override var pullUpControllerMiddleStickyPoints: [CGFloat] {
            switch initialState {
            case .contracted:
                return [firstPreviewView.frame.maxY]
            case .expanded:
                return [searchBoxContainerView.frame.maxY, firstPreviewView.frame.maxY]
            }
        }
        
        override var pullUpControllerBounceOffset: CGFloat {
            return 20
        }
        
        override func pullUpControllerAnimate(action: PullUpController.Action,
                                              withDuration duration: TimeInterval,
                                              animations: @escaping () -> Void,
                                              completion: ((Bool) -> Void)?) {
            switch action {
            case .move:
                UIView.animate(withDuration: 0.3,
                               delay: 0,
                               usingSpringWithDamping: 0.7,
                               initialSpringVelocity: 0,
                               options: .curveEaseInOut,
                               animations: animations,
                               completion: completion)
            default:
                UIView.animate(withDuration: 0.3,
                               animations: animations,
                               completion: completion)
            }
        }
        
    
        

    
    }
    
    // MARK: - UISearchBarDelegate
    

    
    extension WhatsNearMePanelViewController: UITableViewDataSource, UITableViewDelegate {
        
        // MARK: - UITableViewDataSource
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return NearByItems.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard
                let cell = tableView.dequeueReusableCell(withIdentifier: "NearmyLineResultCell",
                                                         for: indexPath) as? NearmyLineResultCell
                else { return UITableViewCell() }
            tableView.separatorColor = UIColor.clear;
            cell.separatorInset = .zero
            
            
            let transitmode = (NearByItems[indexPath.row]["TransitMode"] as? String)!
            let RouteNameS = (NearByItems[indexPath.row]["RouteName"] as? String)!
              let Addressfrom = (NearByItems[indexPath.row]["Address"] as? String)!
              let BikeAvaliable = (NearByItems[indexPath.row]["BikeAvaliable"] as? String)!
            let DocksAvaliable = (NearByItems[indexPath.row]["DocksAvaliable"] as? String)!
            
             let BikeType = (NearByItems[indexPath.row]["BikeType"] as? String)!
              let DepartETA = (NearByItems[indexPath.row]["DepartETA"] as? String)!
            
              let DepartETAMetric = (NearByItems[indexPath.row]["DepartETAMetric"] as? String)!
              let RealTime = (NearByItems[indexPath.row]["RealTime"] as? Bool)!
              let RouteColor = (NearByItems[indexPath.row]["RouteColor"] as? String)!
              let RouteTextColor = (NearByItems[indexPath.row]["RouteTextColor"] as? String)!
            
//            if transitmode == "Bike" {
//                cell.Transit.isHidden = true
//                cell.BikeShare.isHidden = false
//            }
//            else{
//                cell.Transit.isHidden = false
//                cell.BikeShare.isHidden = true
//            }
//
            
            cell.layer.masksToBounds = false;
            cell.clipsToBounds = false;
            
            cell.NearByLines(F_TransitMode: transitmode, F_RouteName: RouteNameS, F_Address: Addressfrom, F_BikeAvaliable: BikeAvaliable,F_DocksAvaliable:DocksAvaliable, F_BikeType: BikeType, F_DepartETA: DepartETA, F_DepartETAMetric: DepartETAMetric, F_RealTime: RealTime, F_RouteColor: RouteColor, F_RouteTextColor: RouteTextColor)
            
            
            var Routecolor = color.hexStringToUIColor(hex: (NearByItems[indexPath.row]["RouteColor"] as? String)!)
           //Routecolor = Routecolor.darker(by: 5)!
            cell.contentView.backgroundColor = Routecolor
            
            
            return cell
            
        }
        
        // MARK: - UITableViewDelegate
        
        
     
        func showLoader()
        {
            DispatchQueue.main.async {
                self.loaderView = UIView()
                self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: UIScreen.main.bounds.height)
                self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
                let window = UIApplication.shared.keyWindow!
                
                let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
                activityLoader.startAnimating()
                window.addSubview(self.loaderView)
                self.loaderView.addSubview(activityLoader)
            }
            
        }

        
        func hideLoader()
        {
            DispatchQueue.main.async {
                self.loaderView.removeFromSuperview()
            }
            
        }
        
}


extension UIColor {
    
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + percentage/100, 1.0),
                           green: min(green + percentage/100, 1.0),
                           blue: min(blue + percentage/100, 1.0),
                           alpha: alpha)
        } else {
            return nil
        }
    }
}
extension UIButton {
    
    func addRightBorder(borderColor: UIColor, borderWidth: CGFloat) {
        let border = CALayer()
        border.backgroundColor = borderColor.cgColor
        border.frame = CGRect(x: self.frame.size.width - borderWidth,y: 0, width:borderWidth, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorder(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
}
