//
//  QRCodeViewController.swift
//  ZIG
//
//  Created by Isaac on 24/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
class QRCodeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var ExpiryDatetimeNew: UILabel!
    @IBOutlet weak var STATUS: UILabel!
    @IBOutlet weak var CurrentTime: UILabel!
    @IBOutlet weak var ValidateTime: UILabel!
    @IBOutlet weak var TicketID: UILabel!
    @IBOutlet weak var QRcode: UIImageView!
    @IBOutlet weak var backgroundWhiteView: UIView!
    @IBOutlet weak var ticketImageView: UIImageView!
    @IBOutlet weak var firstView: UIView!
    var firstSetArray = NSMutableArray()
    var secondSetArray = NSMutableArray()
    var uberSelected = Bool()
    var lyftSelected = Bool()
    var isclickFromTripPlanner = Bool()
    var saveTripBool = Bool()
    var dashBoardScreen = Bool()
    var loaderView = UIView()

    var releaseDate: NSDate?
    var countdownTimer = Timer()
    var timeEnd: Date?
    var prefernceTable = UITableView()
    //var releaseDate: NSDate?
    var timer: Timer!
    var timerExpiry: Timer!
    var timeRemaining: Int?
    var carSelected = Bool()
    var bikeSelected = Bool()
    var carFirstSelected = Bool()
    var bikeFirstSelected = Bool()
    var carIcon = UIButton()
    var bikeIcon = UIButton()
    var bikeFirstIcon = UIButton()
    var carFirstIcon = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()

        makeArray()
        getCurrentTime()
        let myDate = comVar.ExpiryDate
        print(myDate.count)
        let dateFormatter = DateFormatter()
       dateFormatter.timeZone = NSTimeZone(name: "EST") as TimeZone?
        if myDate.count == 23 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        }
        else if myDate.count == 19 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        }
        else if myDate.count == 21 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
        }
        else if myDate.count == 22 {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
        }
        let Expirydate = dateFormatter.date(from: myDate)!


        print("Expiry date \(Expirydate)")
        let calendar = Calendar.current

        let hour = calendar.component(.hour, from: Expirydate)
        let minutes = calendar.component(.minute, from: Expirydate)
        let seconds = calendar.component(.second, from: Expirydate)
        let year = calendar.component(.year, from: Expirydate)
        let Month = calendar.component(.month, from: Expirydate)
        let day = calendar.component(.day, from: Expirydate)

     ///   ExpiryDateTime(Year: year, Month: Month, Day: day, Hours: hour, Min: minutes, Sec: seconds)

        //ValidateTime.text = "\(hour):\(minutes):\(seconds)"

        let ExpiryDatetime = dateFormatter.date(from:myDate)!
            //  dateFormatter.timeZone = NSTimeZone(name: "EST") as TimeZone?
              dateFormatter.dateFormat = "E, MMM d, yyyy hh:mm a"
              let ExpiryDatetimeString = dateFormatter.string(from:ExpiryDatetime)


        if comVar.status == 3 {
            //self.view.backgroundColor = UIColor.green.withAlphaComponent(0.5)
            //self.view.blink()
            STATUS.text = "VALIDATED"
            STATUS.textColor = color.hexStringToUIColor(hex: "#E53638")

            let colorTop = color.hexStringToUIColor(hex: "#96e6a1").cgColor
            let colorBottom = color.hexStringToUIColor(hex: "#009688").cgColor

            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [colorTop, colorBottom]
            gradientLayer.locations = [0.0, 1.0]
            gradientLayer.frame = self.view.bounds
            ExpiryDatetimeNew.text = "Expiry Date: \(ExpiryDatetimeString)"
            self.view.layer.insertSublayer(gradientLayer, at: 0)





            timerExpiry = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
            TicketID.text = "#\(comVar.ticketId)"
            // ValidateTime.text = "\(comVar.timer)"
            print(comVar.timer)
            timeRemaining = comVar.timer
            let image = generateQRCode(from: "\(comVar.ticketId)")
            QRcode.image = image
        }
        else if comVar.status == 2 {
            STATUS.text = "ACTIVATE"
            STATUS.textColor = color.hexStringToUIColor(hex: "#57BB2F")
//self.view.backgroundColor =
            timerExpiry = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
            TicketID.text = "#\(comVar.ticketId)"
            // ValidateTime.text = "\(comVar.timer)"
            print(comVar.timer)
            timeRemaining = comVar.timer
            let image = generateQRCode(from: "\(comVar.ticketId)")
            QRcode.image = image
             ExpiryDatetimeNew.text = "Expiry Date: \(ExpiryDatetimeString)"
            
            
            
            

            let colorTop = color.hexStringToUIColor(hex: "#57e6a1").cgColor
                       let colorBottom = color.hexStringToUIColor(hex: "#57BB2F").cgColor

                       let gradientLayer = CAGradientLayer()
                       gradientLayer.colors = [colorTop, colorBottom]
                       gradientLayer.locations = [0.0, 1.0]
                       gradientLayer.frame = self.view.bounds

                       self.view.layer.insertSublayer(gradientLayer, at: 0)


        }
        else if comVar.status == 0 {


            let colorTop = UIColor(red: 255.0 / 255.0, green: 149.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0).cgColor
            let colorBottom = UIColor(red: 253 / 255.0, green: 67.0 / 255.0, blue: 52 / 255.0, alpha: 1.0).cgColor

            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [colorTop, colorBottom]
            gradientLayer.locations = [0.0, 1.0]
            gradientLayer.frame = self.view.bounds
            //Arun
              self.view.layer.insertSublayer(gradientLayer, at:0)




            //self.view.backgroundColor = UIColor.red.withAlphaComponent(0.5)
            TicketID.text = "No Ticket"
            // ValidateTime.text = "\(comVar.timer)"Fleft
            print(comVar.timer)
            timeRemaining = comVar.timer
            //let image = generateQRCode(from: "\(comVar.ticketId)")
            QRcode.image = UIImage(named: "no-tickets")

            ValidateTime.text = "Please Buy a ticket"

        }

        else if comVar.status == 7 {
            let colorTop = UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 0.0 / 255.0, alpha: 0.5).cgColor
                    let colorBottom = UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 0.5).cgColor
            let gradientLayer = CAGradientLayer()
                       gradientLayer.colors = [colorTop, colorBottom]
                     //  gradientLayer.locations = [0.0, 1.0]
                       gradientLayer.frame = self.view.bounds
                       //Arun
                         self.view.layer.insertSublayer(gradientLayer, at:0)
        }
        //Arun
        if isclickFromTripPlanner == true
            {
                comVar.status = 7
            let userDefaults1 = UserDefaults.standard
            let accessToken = userDefaults1.value(forKey: "accessToken") as! NSString
            let url = "\(APIUrl.ZIGTARCAPI)Preferences/GetUserPreferences"
            let parameters: Parameters = [
                "TokenId": accessToken,

            ]
            self.ticketImageView.isHidden = true
            self.firstView.isHidden = true

            self.showLoader()

            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                .responseObject { (response: DataResponse<preferenceOption>) in
                    switch response.result {
                    case .success:
                        print(response.result.value ?? "nil")
                        if response.result.value?.Message != nil
                            {
                            let userDefaults1 = UserDefaults.standard
                            userDefaults1.set("true", forKey: "isBusOn")
                            userDefaults1.set(response.result.value?.Bus, forKey: "Bus")
                            userDefaults1.set(response.result.value?.Bike, forKey: "Bike")
                            userDefaults1.set(response.result.value?.Cab, forKey: "Cab")
                            userDefaults1.set(response.result.value?.Cabshare, forKey: "Cabshare")
                            userDefaults1.set(response.result.value?.Bikeshare, forKey: "Bikeshare")
                            userDefaults1.synchronize()

                            if response.result.value!.Bike != nil
                                {
                                self.bikeSelected = response.result.value!.Bike!

                            }
                            else
                            {
                                self.bikeSelected = false

                            }

                            if response.result.value!.Cab != nil
                                {
                                self.carSelected = response.result.value!.Cab!

                            }
                            else
                            {
                                self.carSelected = false

                            }
                            print(response.result.value!.Cabshare)
                            print(response.result.value!.Bikeshare)

                            if response.result.value!.Cabshare != nil
                                {
                                self.carFirstSelected = response.result.value!.Cabshare!

                            }
                            else
                            {
                                self.carFirstSelected = false

                            }

                            if response.result.value!.Bikeshare != nil
                                {
                                self.bikeFirstSelected = response.result.value!.Bikeshare!

                            }
                            else
                            {
                                self.bikeFirstSelected = false

                            }
                            print("\(self.bikeSelected) \(self.carSelected) \(self.carFirstSelected) \(self.bikeFirstSelected)")
                            self.hideLoader()
                            self.backgroundWhiteView.backgroundColor = UIColor.clear
                            self.ticketImageView.isHidden = true
                            self.firstView.isHidden = true
                            self.newPreferenceScreen()
                        }
                        else
                        {
                            
                            let userDefaults1 = UserDefaults.standard
                                                      userDefaults1.set("true", forKey: "isBusOn")
                                                      userDefaults1.set(response.result.value?.Bike, forKey: "Bike")
                                                      userDefaults1.set(response.result.value?.Cab, forKey: "Cab")
                                                      userDefaults1.set(response.result.value?.Cabshare, forKey: "Cabshare")
                                                      userDefaults1.set(response.result.value?.Bikeshare, forKey: "Bikeshare")
                            if response.result.value?.Cabshare == nil &&  response.result.value?.Cabshare == nil
                            {
                                userDefaults1.set(true, forKey: "Cabshare")

                            }
                            print(response.result.value!.Cabshare)
                            print(response.result.value!.Bikeshare)
                          
                            userDefaults1.synchronize()
                            if response.result.value!.Bike != nil
                                {
                                self.bikeSelected = response.result.value!.Bike!

                            }
                            else
                            {
                                self.bikeSelected = false

                            }

                            if response.result.value!.Cab != nil
                                {
                                self.carSelected = response.result.value!.Cab!

                            }
                            else
                            {
                                self.carSelected = false

                            }
                            if response.result.value!.Cabshare != nil
                                {
                                    self.carFirstSelected = response.result.value!.Cabshare!

                                     if response.result.value?.Cabshare == nil &&  response.result.value?.Cabshare == nil
                                     {
                                        self.carFirstSelected = true

                                    }
                                    

                            }
                            else
                            {
                                self.carFirstSelected = false
                                if response.result.value?.Cabshare == nil &&  response.result.value?.Cabshare == nil
                                                                   {
                                                                      self.carFirstSelected = true

                                                                  }
                            }

                            if response.result.value!.Bikeshare != nil
                                {
                                self.bikeFirstSelected = response.result.value!.Bikeshare!

                            }
                            else
                            {
                                self.bikeFirstSelected = false

                            }

                            print("\(self.bikeSelected) \(self.carSelected) \(self.carFirstSelected) \(self.bikeFirstSelected)")
                            self.backgroundWhiteView.backgroundColor = UIColor.clear
                            self.hideLoader()

                            self.newPreferenceScreen()
                        }
                    case .failure(let error):
                        self.hideLoader()
                        self.showAlertBox(text: "Failed to load perferences.\nPlease try again later")

                      
                        print(error)

                    }
            }




        }



        if isclickFromTripPlanner == true
            {

            let url = "\(APIUrl.ZIGTARCAPI)Preferences/GetAppPreferences?"
            let parameters: Parameters = [
                "Pin": "ZIG19",

            ]


            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                .responseObject { (response: DataResponse<preferenceGet>) in
                    switch response.result {
                    case .success:
                        print(response.result.value ?? "nil")
                        if response.result.value?.Message != nil
                            {
                            let userDefaults1 = UserDefaults.standard
                            userDefaults1.set("true", forKey: "isBusOn")
                            userDefaults1.set(response.result.value?.Bird, forKey: "isBirdOn")
                            userDefaults1.set(response.result.value?.Louvelo, forKey: "LouveloOn")
                            userDefaults1.set(response.result.value?.Louvelo, forKey: "UberOn")
                            userDefaults1.set(response.result.value?.Lyft, forKey: "LyftOn")
                            userDefaults1.set(response.result.value?.Bolt, forKey: "BoltOn")
                            userDefaults1.set(response.result.value?.Lime, forKey: "LimeOn")
                            userDefaults1.set(response.result.value?.Livepayment, forKey: "Livepayment")
                            userDefaults1.set(response.result.value?.COVID, forKey: "EmergencyAlert")
                            userDefaults1.synchronize()

                            if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                                {
                                self.uberSelected = false
                                self.lyftSelected = false
                            }
                            else if !userDefaults1.bool(forKey: "UberOn")
                                {
                                if self.uberSelected == true
                                    {
                                    self.uberSelected = false
                                }

                            }
                            else if !userDefaults1.bool(forKey: "LyftOn")
                                {
                                if self.lyftSelected == true
                                    {
                                    self.lyftSelected = false
                                }
                            }
                            self.makeArray()
                            self.prefernceTable.reloadData()
                        }
                        else
                        {

                        }
                    case .failure(let error):
                        print(error)

                    }
            }


//            self.backgroundWhiteView.backgroundColor = UIColor.clear
//            self.ticketImageView.isHidden = true
//            self.firstView.isHidden = true
//
//            self.preferenceScreen()

        }
        // Do any additional setup after loading the view.
    }
    
    func showAlertBox(text:String){
        let alertViewController = NYAlertViewController()
        // Set a title and message
        alertViewController.title = "Server Error"
        alertViewController.message = "\(text)"
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = self.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                // self.sourceTextField.becomeFirstResponder()
                self.dismiss(animated: true) {

                                      }
                self.dismiss(animated: true, completion: nil)
        }
        )
        //        alertViewController.addAction(activateAction)
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height + 120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!

            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width / 2) - 40, y: (self.view.frame.size.height / 2) - 40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    func makeArray() -> Void {
        let userDefaults1 = UserDefaults.standard

        //Bike/Scooter

        firstSetArray = [
            [
                "title": "Bus",
                "image": "bus_Transit",
                "switch": "false"
            ],
            [
                "title": "Bird",
                "image": "BirdMap",
                "switch": "false"

            ],
            [
                "title": "Louvelo",
                "image": "Louvelomap",
                "switch": "false",

            ],
            [
                "title": "Ride Share",
                "image": "ride_share_preference",
                "switch": "false",

            ]
        ]
        if !userDefaults1.bool(forKey: "isBirdOn")
            {
            firstSetArray .removeObject(at: 1)
            if !userDefaults1.bool(forKey: "LouveloOn")
                {
                firstSetArray .removeObject(at: 1)

            }
        }
        else if !userDefaults1.bool(forKey: "LouveloOn")
            {
            firstSetArray .removeObject(at: 2)

        }

        if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
            {
            firstSetArray.removeObject(at: firstSetArray.count - 1)
        }

        secondSetArray = [
            [
                "title": "UBER",
                "image": "uber_Car",
                "switch": "true"
            ],
            [
                "title": "LYFT",
                "image": "lyft_preference",
                "switch": "false"

            ]
        ]

        if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
            {
            secondSetArray = NSMutableArray()
        }
        else if !userDefaults1.bool(forKey: "UberOn")
            {
            secondSetArray .removeObject(at: 0)
        }
        else if !userDefaults1.bool(forKey: "LyftOn")
            {
            secondSetArray .removeObject(at: 1)

        }


        print("\(firstSetArray)")
        print("\(secondSetArray)")

    }
    func preferenceScreen()
    {
        let perferenceScreenback = UIView()
        perferenceScreenback.frame = CGRect.init(x: 20, y: 0, width: backgroundWhiteView.frame.size.width - 40, height: backgroundWhiteView.frame.size.height)
        perferenceScreenback.backgroundColor = UIColor.white
        backgroundWhiteView .addSubview(perferenceScreenback)

        let preferenceLabel = UILabel()
        preferenceLabel.frame = CGRect.init(x: 0, y: 10, width: backgroundWhiteView.frame.size.width, height: 50)
        preferenceLabel.text = "PREFERENCES"
        preferenceLabel.textAlignment = .center
        preferenceLabel.font = UIFont.setTarcBold(size: 18)
        perferenceScreenback.addSubview(preferenceLabel)

        let closeButton = UIButton()
        closeButton.frame = CGRect.init(x: 5, y: 10, width: 50, height: 50)
        closeButton .setImage(UIImage.init(named: "closeBtn"), for: .normal)
        closeButton.addTarget(self, action: #selector(dismissMethod), for: .touchUpInside)
        perferenceScreenback .addSubview(closeButton)
        closeButton.isAccessibilityElement = true
        closeButton.accessibilityLabel = "Perference Close"
        preferenceLabel.bringSubview(toFront: closeButton)
        prefernceTable = UITableView(frame: CGRect.init(x: 5, y: 60, width: perferenceScreenback.frame.size.width - 10, height: backgroundWhiteView.frame.size.height - 110), style: .grouped)
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
        {

            if UIScreen.main.bounds.size.height == 568.0
                {
                // iPhone 5
                prefernceTable = UITableView(frame: CGRect.init(x: 5, y: 40, width: perferenceScreenback.frame.size.width - 10, height: backgroundWhiteView.frame.size.height - 90), style: .grouped)

            }
            }

            prefernceTable.isScrollEnabled = false
            prefernceTable.backgroundColor = UIColor.white
            prefernceTable.delegate = self
            prefernceTable.dataSource = self
            perferenceScreenback .addSubview(prefernceTable)
            perferenceScreenback.bringSubview(toFront: preferenceLabel)
            prefernceTable.bringSubview(toFront: closeButton)

            backgroundWhiteView.isUserInteractionEnabled = true

            let doneButton = UIButton()
            doneButton.frame = CGRect.init(x: 10, y: prefernceTable.frame.maxY + 5, width: prefernceTable.frame.size.width - 20, height: 40)

            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
            {

                if UIScreen.main.bounds.size.height == 568.0
                    {
                    // iPhone 5
                    doneButton.frame = CGRect.init(x: 15, y: prefernceTable.frame.maxY + 10, width: prefernceTable.frame.size.width - 30, height: 30)


                }
                }
                doneButton.backgroundColor = UIColor.init(red: 247 / 255.0, green: 0 / 255, blue: 28 / 255.0, alpha: 1.0)
                doneButton.setTitle("Done", for: .normal)
                doneButton.addTarget(self, action: #selector(dismissMethod), for: .touchUpInside)
        doneButton.titleLabel!.font = UIFont.setTarcBold(size: 18.0)

                doneButton.setTitleColor(UIColor.white, for: .normal)
                perferenceScreenback .addSubview(doneButton)

            }
            @objc func dismissMethod()
            {
                var dashBoardArray = NSMutableArray()
                let userDefaults1 = UserDefaults.standard

                if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                    {
                    self.uberSelected = false
                    self.lyftSelected = false
                }
                else if !userDefaults1.bool(forKey: "UberOn")
                    {
                    if self.uberSelected == true
                        {
                        self.uberSelected = false
                    }

                }
                else if !userDefaults1.bool(forKey: "LyftOn")
                    {
                    if self.lyftSelected == true
                        {
                        self.lyftSelected = false
                    }
                }
                else
                {

                }
                dashBoardArray = [
                    [
                        "uber": true,
                        "lyft": true,
                        "Bus": true,
                        "Cab": true,
                        "Bike": true,
                        "Bikeshare": bikeFirstSelected,
                        "Cabshare": carFirstSelected
                    ]
                ]
                if saveTripBool == true
                    {


                    NotificationCenter.default.post(name: Notification.Name("perferenceSelectedSaved"), object: dashBoardArray)

                }
                else if dashBoardScreen == true
                    {
                    NotificationCenter.default.post(name: Notification.Name("perferenceSelecteddash"), object: dashBoardArray)



                }
                else
                {
                    NotificationCenter.default.post(name: Notification.Name("perferenceSelected"), object: dashBoardArray)

                }

                //
                self.dismiss(animated: true) {

                }
            }
            func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                if section == 0
                    {
                    return firstSetArray.count
                }
                else
                {
                    return secondSetArray.count

                }
            }
            func numberOfSections(in tableView: UITableView) -> Int {
                return 2
            }
            func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
                let headerView = UIView()
                headerView.frame = CGRect.init(x: 0, y: 0, width: tableView.frame.size.width, height: 30)
                let centerLabel = UILabel()
                centerLabel.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.size.width, height: 20)
                centerLabel.text = "First/ Last Mile Connectivity"
                centerLabel.font = UIFont.setTarcBold(size: 16)
                centerLabel.textAlignment = .center
                headerView .addSubview(centerLabel)
                return headerView
            }
            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                if indexPath.section == 0
                    {
                    let cell = preferenceTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    let dict = firstSetArray.object(at: indexPath.row) as! NSDictionary
                    let nameString = dict.value(forKey: "title") as! NSString
                    let imageString = dict.value(forKey: "image") as! NSString
                    cell.trainLable.text = nameString as String
                    cell.trainImage.image = UIImage.init(named: imageString as String)
                    cell.traincheckbox.isEnabled = false
                    cell.traincheckbox.setOn(true, animated: false)
                        

                    return cell
                }
                else
                {
                    let cell = preferenceTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    let dict = secondSetArray.object(at: indexPath.row) as! NSDictionary
                    let nameString = dict.value(forKey: "title") as! NSString
                    let imageString = dict.value(forKey: "image") as! NSString
                    cell.trainLable.text = nameString as String
                    cell.trainImage.image = UIImage.init(named: imageString as String)
                    cell.traincheckbox.tintColor = UIColor.red
                    cell.traincheckbox.onTintColor = UIColor.red
                    cell.traincheckbox.isEnabled = true
                    cell.trainLable.isAccessibilityElement = true
                    cell.trainLable.accessibilityLabel = nameString as String
                    cell.traincheckbox.tag = indexPath.row
                    print("ARUN CHECK==>Uber==>\(uberSelected) LYFT==>\(lyftSelected)")
                    if uberSelected == true
                        {
                        if indexPath.row == 0
                            {
                            cell.traincheckbox.setOn(true, animated: false)
                        }
                        else
                        {
                            cell.traincheckbox.setOn(false, animated: false)
                        }
                    }
                    else if uberSelected == false && lyftSelected == false
                        {
                        if indexPath.row == 0
                            {
                            cell.traincheckbox.setOn(false, animated: false)
                        }
                        else
                        {
                            cell.traincheckbox.setOn(false, animated: false)
                        }
                    }
                    else
                    {
                        if indexPath.row == 0
                            {
                            let userDefaults1 = UserDefaults.standard

                            if !userDefaults1.bool(forKey: "UberOn")
                                {
                                cell.traincheckbox.setOn(true, animated: false)

                            }
                            else
                            {
                                cell.traincheckbox.setOn(false, animated: false)

                            }

                        }
                        else
                        {
                            cell.traincheckbox.setOn(true, animated: false)

                        }
                    }

                    if indexPath.row == 0
                        {
                        let userDefaults1 = UserDefaults.standard

                        if !userDefaults1.bool(forKey: "UberOn")
                            {
                            cell.traincheckbox.addTarget(self, action: #selector(onLyftPreferenceClicked), for: .valueChanged)


                        }
                        else
                        {
                            cell.traincheckbox.addTarget(self, action: #selector(onUberPreferenceClicked), for: .valueChanged)

                        }

                    }
                    else
                    {
                        cell.traincheckbox.addTarget(self, action: #selector(onLyftPreferenceClicked), for: .valueChanged)

                    }
                    return cell
                }

            }
            @objc func onLyftPreferenceClicked()
            {
                if lyftSelected == true
                    {


                    lyftSelected = false

                }
                else
                {
                    if uberSelected == true && lyftSelected == false
                        {
                        lyftSelected = true
                        uberSelected = false
                    }
                    else
                    {
                        lyftSelected = true

                    }

                }
                prefernceTable.reloadData()
            }
            @objc func onUberPreferenceClicked()
            {
                if uberSelected == true
                    {

                    // lyftSelected = false
                    uberSelected = false


                }
                else
                {
                    if uberSelected == false && lyftSelected == true
                        {
                        lyftSelected = false
                        uberSelected = true
                    }
                    else
                    {
                        //lyftSelected = false
                        uberSelected = true
                    }


                }
                prefernceTable.reloadData()

            }
            func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 50
            }
            func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
                if section == 0
                    {
                    return 0

                }
                else
                {
                    return 30
                }
            }
            func ExpiryDateTime(Year: Int, Month: Int, Day: Int, Hours: Int, Min: Int, Sec: Int) {

                let date = NSDate()
                let calendar = Calendar.current

                let components = calendar.dateComponents([.hour, .minute, .month, .year, .day], from: date as Date)

                let currentDate = calendar.date(from: components)

                let userCalendar = Calendar.current

                // here we set the due date. When the timer is supposed to finish
                let competitionDate = NSDateComponents()
                competitionDate.year = Year
                competitionDate.month = Month
                competitionDate.day = Day
                competitionDate.hour = Hours
                competitionDate.minute = Min
                competitionDate.second = Sec
                let competitionDay = userCalendar.date(from: competitionDate as DateComponents)!

                //here we change the seconds to hours,minutes and days
                let CompetitionDayDifference = calendar.dateComponents([.day, .hour, .minute], from: currentDate!, to: competitionDay)


                //finally, here we set the variable to our remaining time
                let daysLeft = CompetitionDayDifference.day
                let hoursLeft = CompetitionDayDifference.hour
                let minutesLeft = CompetitionDayDifference.minute
                let SecLeft = CompetitionDayDifference.second

                print("day:", daysLeft ?? "N/A", "hour:", hoursLeft ?? "N/A", "minute:", minutesLeft ?? "N/A")
                var DayDispaly = ""
                if daysLeft == 0 || daysLeft == nil {
                    DayDispaly = ""
                }
                else {
                    
                     DayDispaly = "\((daysLeft)!) Days"
                }
                ValidateTime.text = "\(DayDispaly) \(hoursLeft ?? 0):\(minutesLeft ?? 0):\(SecLeft ?? 0) left to expiry"
            }
    
            func convertTime(miliseconds: Int) -> String {

                var seconds: Int = 0
                var minutes: Int = 0
                var hours: Int = 0
                var days: Int = 0
                var secondsTemp: Int = 0
                var minutesTemp: Int = 0
                var hoursTemp: Int = 0

                if miliseconds < 1000 {
                    return ""
                } else if miliseconds < 1000 * 60 {
                    seconds = miliseconds / 1000
                    return "\(seconds) sec left to expiry"
                } else if miliseconds < 1000 * 60 * 60 {
                    secondsTemp = miliseconds / 1000
                    minutes = secondsTemp / 60
                    seconds = (miliseconds - minutes * 60 * 1000) / 1000
                    return "\(minutes) min, \(seconds) sec\n left to expiry"
                } else if miliseconds < 1000 * 60 * 60 * 24 {
                    minutesTemp = miliseconds / 1000 / 60
                    hours = minutesTemp / 60
                    minutes = (miliseconds - hours * 60 * 60 * 1000) / 1000 / 60
                    seconds = (miliseconds - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
                    return "\(hours) hr, \(minutes) min, \(seconds) sec\n left to expiry"
                } else {
                    hoursTemp = miliseconds / 1000 / 60 / 60
                    days = hoursTemp / 24
                    hours = (miliseconds - days * 24 * 60 * 60 * 1000) / 1000 / 60 / 60
                    minutes = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000) / 1000 / 60
                    seconds = (miliseconds - days * 24 * 60 * 60 * 1000 - hours * 60 * 60 * 1000 - minutes * 60 * 1000) / 1000
                    return "\(days) days, \(hours) hr, \(minutes) min, \(seconds) sec\n left to expiry"
                }
            }
            @objc func timerRunning() {
                timeRemaining = timeRemaining! - 1
                
                ValidateTime.text = convertTime(miliseconds: timeRemaining! * 1000)

            }
            func generateQRCode(from string: String) -> UIImage? {
                let data = string.data(using: String.Encoding.ascii)

                if let filter = CIFilter(name: "CIQRCodeGenerator") {
                    filter.setValue(data, forKey: "inputMessage")
                    let transform = CGAffineTransform(scaleX: 10, y: 10)

                    if let output = filter.outputImage?.transformed(by: transform) {
                        return UIImage(ciImage: output)
                    }
                }

                return nil
            }

            /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

            @IBAction func CloseButton(_ sender: Any) {

                dismiss(animated: true, completion: nil)
            }
            private func getCurrentTime() {
                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.currentTime), userInfo: nil, repeats: true)
            }

            @objc func currentTime() {
                let formatter = DateFormatter()
                formatter.dateFormat = "hh:mm:ss"
                CurrentTime.text = formatter.string(from: Date())
            }

            // NEw perference screen design implementation
            func newPreferenceScreen()
            {
                
   let colorTop = UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 0.0 / 255.0, alpha: 0.5).cgColor
         let colorBottom = UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 0.5).cgColor

         let gradientLayer = CAGradientLayer()
         gradientLayer.colors = [colorTop, colorBottom]
       //  gradientLayer.locations = [0.0, 1.0]
         gradientLayer.frame = self.view.bounds
         //A
                
                let perferenceScreenback = UIView()
                perferenceScreenback.frame = CGRect.init(x: 20, y: 80, width: backgroundWhiteView.frame.size.width - 40, height: backgroundWhiteView.frame.size.height / 1.8)

                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
                {

                    if UIScreen.main.bounds.size.height == 568.0
                        {
                        // iPhone 5


                    }
                    }
                    else
                    {

                        perferenceScreenback.frame = CGRect.init(x: 20, y: 180, width: backgroundWhiteView.frame.size.width - 40, height: backgroundWhiteView.frame.size.height / 1.8)


                    }

                    perferenceScreenback.backgroundColor = UIColor.white
                    backgroundWhiteView .addSubview(perferenceScreenback)

                    let preferenceLabel = UILabel()
                    preferenceLabel.frame = CGRect.init(x: 0, y: 10, width: backgroundWhiteView.frame.size.width, height: 50)
                    preferenceLabel.text = "PREFERENCES"
                    preferenceLabel.textAlignment = .center
                    preferenceLabel.font = UIFont.setTarcBold(size: 18)
                    perferenceScreenback.addSubview(preferenceLabel)

                    let closeButton = UIButton()
                    closeButton.frame = CGRect.init(x: 5, y: 10, width: 50, height: 50)
                    closeButton .setImage(UIImage.init(named: "closeBtn"), for: .normal)
                    closeButton.addTarget(self, action: #selector(dismissMethod), for: .touchUpInside)
                    perferenceScreenback .addSubview(closeButton)
                    preferenceLabel.bringSubview(toFront: closeButton)

                    prefernceTable.isHidden = true
                    let transitIcon = UIButton()
                    transitIcon.frame = CGRect.init(x: 30, y: 95, width: 60, height: 60)
                    transitIcon.layer.cornerRadius = 30

                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
                    {

                        if UIScreen.main.bounds.size.height == 568.0
                            {
                            // iPhone 5
                            transitIcon.frame = CGRect.init(x: 30, y: 95, width: 50, height: 50)
                            transitIcon.layer.cornerRadius = 25

                        }
                        }
                        else
                        {
                        }
                        transitIcon.layer.masksToBounds = true
                        transitIcon.setImage(UIImage.init(named: "perference_transit"), for: .normal)
                        //        transitIcon.backgroundColor = UIColor.init(red: 43/255, green: 126/255, blue: 192/255, alpha: 1.0)
                        perferenceScreenback.addSubview(transitIcon)

                        carIcon = UIButton()

                        carIcon.frame = CGRect.init(x: transitIcon.frame.maxX + 25, y: 95, width: 60, height: 60)
                        carIcon.layer.cornerRadius = 30

                        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
                        {

                            if UIScreen.main.bounds.size.height == 568.0
                                {
                                // iPhone 5
                                carIcon.frame = CGRect.init(x: transitIcon.frame.maxX + 40, y: 95, width: 50, height: 50)
                                carIcon.layer.cornerRadius = 25

                            }
                            }
                            else
                            {
                            }
                            carIcon.layer.masksToBounds = true
                            if carFirstSelected == true
                                {
                                carIcon.setImage(UIImage.init(named: "perference_car_active"), for: .normal)

                            }
                            else
                            {
                                carIcon.setImage(UIImage.init(named: "perference_car_inactive"), for: .normal)

                            }
                            carIcon.tag = 600
                            carIcon.addTarget(self, action: #selector(firstMileAction), for: .touchUpInside)
                            perferenceScreenback.addSubview(carIcon)


                            bikeIcon = UIButton()
                            bikeIcon.frame = CGRect.init(x: carIcon.frame.maxX + 35, y: 95, width: 60, height: 60)
                            bikeIcon.layer.cornerRadius = 30

                            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
                            {

                                if UIScreen.main.bounds.size.height == 568.0
                                    {
                                    // iPhone 5
                                    bikeIcon.frame = CGRect.init(x: carIcon.frame.maxX + 50, y: 95, width: 50, height: 50)
                                    bikeIcon.layer.cornerRadius = 25

                                }
                                }
                                else
                                {
                                }
                                bikeIcon.layer.masksToBounds = true
                                if bikeFirstSelected == true
                                    {
                                    bikeIcon.setImage(UIImage.init(named: "perference_bike_active"), for: .normal)

                                }
                                else
                                {
                                    bikeIcon.setImage(UIImage.init(named: "perference_bike_inactive"), for: .normal)

                                }
                                bikeIcon.tag = 601
                                bikeIcon.addTarget(self, action: #selector(firstMileAction), for: .touchUpInside)
                                perferenceScreenback.addSubview(bikeIcon)

                                let transitLabel = UILabel()
                                transitLabel.frame = CGRect.init(x: 30, y: bikeIcon.frame.maxY + 0, width: 60, height: 40)
                                transitLabel.text = "Transit"
                                transitLabel.font = UIFont.setTarcRegular(size: 13)
                                transitLabel.textAlignment = .center
                                perferenceScreenback.addSubview(transitLabel)

                                let carLabel = UILabel()
                                carLabel.frame = CGRect.init(x: transitLabel.frame.maxX + 20, y: bikeIcon.frame.maxY + 0, width: 90, height: 40)
                                carLabel.text = "Rideshare"
                                carLabel.textAlignment = .center
                                carLabel.font = UIFont.setTarcRegular(size: 13)
                                perferenceScreenback.addSubview(carLabel)


                                let bikeLabel = UILabel()
                                bikeLabel.frame = CGRect.init(x: carLabel.frame.maxX , y: bikeIcon.frame.maxY + 0, width: 90, height: 40)
                                bikeLabel.text = "Bike/Scooter"
                                bikeLabel.font = UIFont.setTarcRegular(size: 13)
                                bikeLabel.textAlignment = .center

                                perferenceScreenback.addSubview(bikeLabel)




                                backgroundWhiteView.isUserInteractionEnabled = true

                                let doneButton = UIButton()
                                doneButton.frame = CGRect.init(x: 10, y: bikeLabel.frame.maxY + 15, width: perferenceScreenback.frame.size.width - 20, height: 40)

                                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
                                {

                                    if UIScreen.main.bounds.size.height == 568.0
                                        {
                                        // iPhone 5
                                        doneButton.frame = CGRect.init(x: 15, y: bikeLabel.frame.maxY + 20, width: perferenceScreenback.frame.size.width - 30, height: 40)


                                    }
                                    }
                                    doneButton.backgroundColor = UIColor.init(red: 247 / 255.0, green: 0 / 255, blue: 28 / 255.0, alpha: 1.0)
                                    doneButton.setTitle("Done", for: .normal)
                                    doneButton.addTarget(self, action: #selector(dismissMethod), for: .touchUpInside)
                doneButton.titleLabel!.font = UIFont.setTarcBold(size: 18.0)

                                    doneButton.setTitleColor(UIColor.white, for: .normal)
                                    perferenceScreenback .addSubview(doneButton)



                                }
                                @objc func firstMileAction(tag: UIButton)
                                {
                                    if tag.tag == 600
                                        {
                                        if carFirstSelected == false
                                            {

                                            carFirstSelected = true
                                            bikeFirstSelected = false

                                            bikeIcon.setImage(UIImage.init(named: "perference_bike_inactive"), for: .normal)
                                            carIcon.setImage(UIImage.init(named: "perference_car_active"), for: .normal)


                                        }
                                        else
                                        {
                                            carIcon.setImage(UIImage.init(named: "perference_car_inactive"), for: .normal)

                                            carFirstSelected = false
                                        }
                                    }
                                    else
                                    {
                                        if bikeFirstSelected == false
                                            {

                                            bikeFirstSelected = true
                                            carFirstSelected = false
                                            bikeIcon.setImage(UIImage.init(named: "perference_bike_active"), for: .normal)
                                            carIcon.setImage(UIImage.init(named: "perference_car_inactive"), for: .normal)

                                        }
                                        else
                                        {
                                            bikeIcon.setImage(UIImage.init(named: "perference_bike_inactive"), for: .normal)

                                            bikeFirstSelected = false
                                        }
                                    }
                                }
                                @objc func perferenceAction(tag: UIButton)
                                {
                                    if tag.tag == 500
                                        {
                                        if carSelected == false
                                            {
                                            carIcon.setImage(UIImage.init(named: "perference_car_active"), for: .normal)

                                            carSelected = true
                                        }
                                        else
                                        {
                                            carIcon.setImage(UIImage.init(named: "perference_car_inactive"), for: .normal)

                                            carSelected = false
                                        }
                                    }
                                    else
                                    {
                                        if bikeSelected == false
                                            {
                                            bikeIcon.setImage(UIImage.init(named: "perference_bike_active"), for: .normal)

                                            bikeSelected = true
                                        }
                                        else
                                        {
                                            bikeIcon.setImage(UIImage.init(named: "perference_bike_inactive"), for: .normal)

                                            bikeSelected = false
                                        }
                                    }
                                }
                            }
                            extension UIView {
                                func blink() {
                                    self.alpha = 0.2
                                    UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: { self.alpha = 1.0 }, completion: nil)
                                }
                            }
