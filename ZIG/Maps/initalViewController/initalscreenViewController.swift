//
//  initalscreenViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 03/07/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit

class initalscreenViewController: UIViewController {

    @IBOutlet weak var newRiderButton: UIButton!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var heightImage:NSLayoutConstraint!
    @IBOutlet weak var widthImage:NSLayoutConstraint!
    var fromAppDelegateBool = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
       backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        // Do any additional setup after loading the view.
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone: break
        // It's an iPhone
        case .pad:
    layoutDesignChange()
            break
        // It's an iPad
        case .unspecified: break
        // Uh, oh! What could it be?
        case .tv: break
            
        case .carPlay: break
            
        }
        
    }
    func layoutDesignChange()
    {
        let ypostion  = self.view.frame.maxY - 200
        let xpostion  = newRiderButton.frame.origin.x
        let width  = self.view.frame.size.width/2-xpostion*2+10
        newRiderButton.isHidden = true
        signinButton.isHidden = true
       let newRideriPad = UIButton()
        newRideriPad.frame = CGRect.init(x: xpostion, y: ypostion, width: width, height: 50)
        newRideriPad.backgroundColor = UIColor.black
        newRideriPad.setTitle("New Rider", for: .normal)
        newRideriPad.titleLabel?.font = UIFont.setTarcRegular(size: 20)
        newRideriPad.setTitleColor(UIColor.white, for: .normal)
        newRideriPad.addTarget(self, action: #selector(newRiderAction(_:)), for: .touchUpInside)
        self.view .addSubview(newRideriPad)
       
        let newsigninButton = UIButton()
        newsigninButton.frame = CGRect.init(x: newRideriPad.frame.maxX+50, y: ypostion, width: width, height: 50)
        newsigninButton.backgroundColor = UIColor.black
        newsigninButton.setTitle("Sign In", for: .normal)
        newsigninButton.titleLabel?.font = UIFont.setTarcRegular(size: 20)
        newsigninButton.setTitleColor(UIColor.white, for: .normal)
        newsigninButton.addTarget(self, action: #selector(signinAction(_:)), for: .touchUpInside)
        self.view .addSubview(newsigninButton)
        
        heightImage.constant = 250
        widthImage.constant = 250
    }
    override func viewWillDisappear(_ animated: Bool) {
    }
    @objc func OnBackClicked() {
        if fromAppDelegateBool == true
        {
            fromAppDelegateBool  =  false

            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                   let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WithOutLoginMainViewController") as! WithOutLoginMainViewController
                   self.navigationController?.pushViewController(nextViewController, animated: false)
        }
        else
        {
        self.navigationController?.popViewController(animated: true)
        }
       
    }
  
    @IBAction func newRiderAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewSignUpViewController") as! NewSignUpViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func signinAction(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
        self.navigationController?.pushViewController(nextViewController, animated: false)
    }
}
