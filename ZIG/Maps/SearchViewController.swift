//
//  SearchViewController.swift
//  PullUpControllerDemo
//
//  Created by Mario on 03/11/2017.
//  Copyright © 2017 Mario. All rights reserved.
//

import UIKit
import MapKit
import PullUpController
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import SafariServices
import SwiftyJSON
import XLActionController



class SearchViewController: PullUpController {
    
    @IBOutlet weak var AlertButton: UIButton!
    
    enum InitialState {
        case contracted
        case expanded
    }
    var timer : Timer?
    var loaderView = UIView()
    var backgroundBlurView = UIView()
var routeTitleString = NSString()
    var routeString = NSString()
    var SchudleArrayList = [[String:Any]]()
    var SchudleArrayValue : NSMutableDictionary = [:]
    @IBOutlet weak var PDF: UIButton!
    @IBOutlet weak var VisitTarcSite: UIButton!
        var AlertBoolFordetail:Bool = false
    @IBOutlet weak var weekdayButton: UIButton!
    @IBOutlet weak var saturdayButton: UIButton!
    @IBOutlet weak var sundayButton: UIButton!

    
    @IBOutlet weak var ButtonBack: UIButton!
    var selectedLine:Bool = false
    @IBOutlet weak var RouteTitle: UILabel!
    var initialState: InitialState = .contracted
    var LineSearching = [mapsandSchudleList]()
    var scheduleParts = [BusStopsSchedule]()
    var searching = false
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var searchBarLine: UISearchBar!
    @IBOutlet private weak var visualEffectView: UIVisualEffectView!
    @IBOutlet private weak var searchBoxContainerView: UIView!
    @IBOutlet private weak var searchSeparatorView: UIView! {
        didSet {
            searchSeparatorView.layer.cornerRadius = searchSeparatorView.frame.height/2
        }
    }
    @IBOutlet private weak var firstPreviewView: UIView!
    @IBOutlet private weak var secondPreviewView: UIView!
    @IBOutlet private weak var tableView: UITableView!
    var route_list = [mapsandSchudleList]()
    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return searchBoxContainerView?.frame.height ?? 0
        case .expanded:
            return pullUpControllerPreferredSize.height
        }
    }

    private var locations = [(title: String, location: CLLocationCoordinate2D)]()
    
    public var portraitSize: CGSize = .zero
    public var landscapeFrame: CGRect = .zero
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        PDF.layer.zPosition = 1;
//        UIApplication.shared.keyWindow!.bringSubview(toFront: searchBoxContainerView)
//        self.searchBoxContainerView.sendSubview(toBack: firstPreviewView)
       routeTitleString = ""
       
        checkAlert()
        ButtonBack.tintColor = UIColor.white
        ButtonBack.isHidden = true
        RouteTitle.text = RealTimeSchedule.SelectedScheduleRoute
        searchBarLine.delegate = self
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: secondPreviewView.frame.maxY)
        landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 300)
        
        tableView.attach(to: self)
        setupDataSource()
        var routeIDREMOVEzero = RealTimeSchedule.SelectedSchedule
        while routeIDREMOVEzero.hasPrefix("0") {
            routeIDREMOVEzero.remove(at: routeIDREMOVEzero.startIndex)
        }
        self.getalertTitle(RouteNo: routeIDREMOVEzero) { (Title, Content, CheckBool) in
            
            if CheckBool {
                self.AlertButton.isHidden = false
//                if let Buttonimage = UIImage(named: "Alert") {
//                    self.AlertButton.setImage(Buttonimage, for: .normal)
//                }
                let AlertScreen = MyTapGesture(target: self, action:  #selector (self.AlertScreenFunc (_:)))
                                self.AlertBoolFordetail = true
                self.AlertButton.addGestureRecognizer(AlertScreen)
                AlertScreen.title = routeIDREMOVEzero
            }
            else{
                                self.AlertBoolFordetail = false
                self.AlertButton.isHidden = true
                
            }
            
            
        }

       // self.VisitTarcSite.isHidden = true
       self.PDF.isHidden = true //newChangejan29
        self.weekdayButton.isHidden = true
        self.saturdayButton.isHidden = true
        self.sundayButton.isHidden = true
        
    }
    func checkAlert(){
        
        
        
    }
    
    func getalertTitle(RouteNo: String, completion: @escaping (String,String,Bool) -> ()) {
        
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Alerts/GetRealtimeAlerts?routeID=\(RouteNo)", method: .get, encoding: JSONEncoding.default) .responseJSON { response in
            switch response.result{
                //switch result {
                
            case .success:
                // print(response)
                let responseVar = response.result.value
                let json = JSON(responseVar!)
                
                let successvalu:Bool?
                if json.count == 0 {
                    successvalu = false
                }
                else {
                    successvalu = true
                }
                completion("\(json[0])","\(json["LineAlert"])", successvalu!)
               // completion("\(json[0])","\(json["LineAlert"])", successvalu!)
            case .failure(let error):
                print("Request failed with error: \(error)")
                completion("Failed","Please try again", false)
            }
        }
        
        
        
        
        
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layer.cornerRadius = 12
    }
    
    override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
//        print("will move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
//        print("did move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidDrag(to point: CGFloat) {
//        print("did drag to \(point)")
    }
    func setShape(TripID:Int){
        
    }
    private func setupDataSource() {
        
        
        //mapsandSchudle
        
        let RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetTripListforBuslines?RouteID=\(RealTimeSchedule.SelectedSchedule)"
        print(RouteUrl)
        Alamofire.request(RouteUrl).responseArray { (response: DataResponse<[mapsandSchudleList]>) in
            
            switch response.result {
            case .success:
                if response.result.value!.count > 0 {
            self.route_list = response.result.value!
            
            self.route_list = self.route_list.filter { $0.TripTitle != nil}
           
            
            self.tableView.reloadData()
            
            if let lastStickyPoint = self.pullUpControllerAllStickyPoints.last {
                                self.pullUpControllerMoveToVisiblePoint(lastStickyPoint, animated: true, completion: nil)
                
                            }
                }
                else{
                    self.setupDataSource()
                    
                }
                
            case .failure(let error):
                print(error)
                self.view.makeToast("Server Error! Please try again later!")
                self.setupDataSource()
                
            }
        }
        
    }
    
    // MARK: - PullUpController
    
    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return landscapeFrame
    }
    
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
        switch initialState {
        case .contracted:
            return [firstPreviewView.frame.maxY]
        case .expanded:
            return [searchBoxContainerView.frame.maxY, firstPreviewView.frame.maxY]
        }
    }
    
    override var pullUpControllerBounceOffset: CGFloat {
        return 20
    }
    
    override func pullUpControllerAnimate(action: PullUpController.Action,
                                          withDuration duration: TimeInterval,
                                          animations: @escaping () -> Void,
                                          completion: ((Bool) -> Void)?) {
        switch action {
        case .move:
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           usingSpringWithDamping: 0.7,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: animations,
                           completion: completion)
        default:
            UIView.animate(withDuration: 0.3,
                           animations: animations,
                           completion: completion)
        }
    }
    
    @IBAction func BackSchudle(_ sender: Any) {
    self.VisitTarcSite.isHidden = false
    self.PDF.isHidden = true //newChangejan29
        self.weekdayButton.isHidden = true
        self.saturdayButton.isHidden = true
        self.sundayButton.isHidden = true
        if self.AlertBoolFordetail == true
        {
            self.AlertButton.isHidden = false
        }

        selectedLine = false
         self.stopTimer()
        tableView.reloadData()
        
    }
    
    @IBAction func PDFOpen(_ sender: Any) {
        while RealTimeSchedule.SelectedSchedule.hasPrefix("0") {
            RealTimeSchedule.SelectedSchedule.remove(at: RealTimeSchedule.SelectedSchedule.startIndex)
        }
        print("https://tripplan.ridetarc.org/img/pdf/\((RealTimeSchedule.SelectedSchedule)).pdf")
        if let url = URL(string: "\(APIUrl.ZIGTARCBASEAPI)img/pdf/\((RealTimeSchedule.SelectedSchedule)).pdf") {
            let vc: SFSafariViewController
            
            if #available(iOS 11.0, *) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = false
                vc = SFSafariViewController(url: url, configuration: config)
            } else {
                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
            }
            vc.delegate = self as? SFSafariViewControllerDelegate
            present(vc, animated: true)
        }
        
        
    }
    
    
    @IBAction func MyTarcCard(_ sender: Any) {
        while RealTimeSchedule.SelectedSchedule.hasPrefix("0") {
            RealTimeSchedule.SelectedSchedule.remove(at: RealTimeSchedule.SelectedSchedule.startIndex)
        }
        print("https://tripplan.ridetarc.org/img/pdf/\((RealTimeSchedule.SelectedSchedule)).pdf")
        if let url = URL(string: "\(APIUrl.ZIGTARCBASEAPI)img/pdf/\((RealTimeSchedule.SelectedSchedule)).pdf") {
            let vc: SFSafariViewController
            
            if #available(iOS 11.0, *) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = false
                vc = SFSafariViewController(url: url, configuration: config)
            } else {
                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
            }
            vc.delegate = self as? SFSafariViewControllerDelegate
            present(vc, animated: true)
        }
        
        
    }
    
    @IBAction func realTime(_ sender: UIButton) {
        let originalString = self.RouteTitle.text! as NSString
        let urlString :String = routeTitleString.addingPercentEncoding(withAllowedCharacters: .alphanumerics)!
        print(urlString)
        let cString :String = routeTitleString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.illegalCharacters)!
        print("\(cString)")

        var typeString = NSString()
        
        if sender.tag == 1
        {
            typeString = "weekday"
        }
        else if sender.tag == 2
        {
            typeString = "saturday"
        }
        else
        {
            typeString = "sunday"

        }
       //https://www.ridetarc.org/Schedules/schedule.php?type=weekday&headsign=4th%20%26%20Main%20Downtown&routeid=4
        print("ARUNTESTINGNEW==>https://www.ridetarc.org/Schedules/report.php?type=\(typeString)&headsign=\(urlString)")
        if let url = URL(string: "https://www.ridetarc.org/Schedules/schedule.php?type=\(typeString)&headsign=\(urlString)&routeid=\(routeString)") {
            let vc: SFSafariViewController
            
            if #available(iOS 11.0, *) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = false
                vc = SFSafariViewController(url: url, configuration: config)
            } else {
                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
            }
            vc.delegate = self as? SFSafariViewControllerDelegate
            present(vc, animated: true)
        }
        
    }
    
    @objc func AlertScreenFunc(_ sender:MyTapGesture){
        print(sender.title)
        getalertTitle(RouteNo: sender.title) { (AlertList, AlertString, AlertBool) in
            if AlertBool {
                let alertViewController = NYAlertViewController()
                alertViewController.title = "Alert"
                alertViewController.message = AlertList
                
                // Customize appearance as desired
                alertViewController.buttonCornerRadius = 20.0
                alertViewController.view.tintColor = self.view.tintColor
                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                alertViewController.swipeDismissalGestureEnabled = true
                alertViewController.backgroundTapDismissalGestureEnabled = true
                alertViewController.buttonColor = UIColor.red
                // Add alert actions
                
                
                let cancelAction = NYAlertAction(
                    title: "OK",
                    style: .cancel,
                    handler: { (action: NYAlertAction!) -> Void in
                        
                        self.dismiss(animated: true, completion: nil)
                }
                )
                
                alertViewController.addAction(cancelAction)
                
                // Present the alert view controller
                self.present(alertViewController, animated: true, completion: nil)
            }
        }
        
    }
    
    func realTimePopup()
    {
        
        
    }
}

// MARK: - UISearchBarDelegate

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let lastStickyPoint = pullUpControllerAllStickyPoints.last {
            pullUpControllerMoveToVisiblePoint(lastStickyPoint, animated: true, completion: nil)
             searchBar.showsCancelButton = true
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !selectedLine {
        if searching {
            return LineSearching.count
        }
        else{
        return route_list.count
        }
        }
        else{
            return self.SchudleArrayList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell",
                                                     for: indexPath) as? SearchResultCell
            else { return UITableViewCell() }
        tableView.separatorColor = UIColor.clear;
        cell.separatorInset = .zero
        
        if !selectedLine {
              stopTimer()
              RouteTitle.text = RealTimeSchedule.SelectedScheduleRoute
            ButtonBack.isHidden = true
        if searching {
            cell.configure(Routename: LineSearching[indexPath.row].Title!, TripID: "Trip ID: \((LineSearching[indexPath.row].TripId)!)")
        }
        else {
             RouteTitle.text = RealTimeSchedule.SelectedScheduleRoute
            cell.configure(Routename: route_list[indexPath.row].Title!, TripID: "Trip ID: \((route_list[indexPath.row].TripId)!)")
            
           // setShape(TripID: route_list[indexPath.row].TripId)
            
           // (parent as? MapViewController)?.Shape(TripID: route_list[indexPath.row].TripId!)
            
        }
        }
        else{
             ButtonBack.isHidden = false
            if  SchudleArrayList.count > 0 {
           // cell.Schedule(Routename:self.scheduleParts[indexPath.row].StopName!)
            let StopID = SchudleArrayList[indexPath.row]["StopID"] as? String
            let ArrivalTimeReal = SchudleArrayList[indexPath.row]["ArravelTime"] as? String
            let StopIDforReal = "Stop ID: \((StopID)!)"
            
            let StopNameforReal = SchudleArrayList[indexPath.row]["BusStopName"] as? String
            
                let ArrivalTime = "Arrival:\((ArrivalTimeReal)!)"
//                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone)
//                {
//
//                    if  UIScreen.main.bounds.size.height == 568.0
//                    {
//                        // iPhone 5
//                       ArrivalTime = "Arrival:\((ArrivalTimeReal)!)"
//
//
//                    }
//                }
            let Status = SchudleArrayList[indexPath.row]["StatusRealTime"] as? String
            
            
            cell.Schedule(Stop: StopNameforReal!, ArivalTimeTxt: ArrivalTime, StatusTxt: Status!, StopID: StopIDforReal)
            
            }
            else{
                
            }
            //cell.Schedule(Stop: self.SchudleArrayList[indexPath.row].StopName!, ArivalTimeTxt: "Arrival: \((self.SchudleArrayList[indexPath.row].ArrivalTime)!)", StatusTxt: "Scheduled", StopID: "Stop ID: \((self.scheduleParts[indexPath.row].StopId)!)")
        }
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        view.endEditing(true)
        SchudleArrayList.removeAll()
         ButtonBack.isHidden = false
        var RouteUrl = ""
        var FirstUrl = ""
        var TripIDForRefresh = 0
        
        
        if !selectedLine {
         
            self.showLoader()
              self.RouteTitle.text = "\(route_list[indexPath.row].TripId!) - \((route_list[indexPath.row].Title)!)"
            routeTitleString = route_list[indexPath.row].Title as! NSString
            TripIDForRefresh = route_list[indexPath.row].TripId!
            routeString = route_list[indexPath.row].RouteID as! NSString
           if searching {
            RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetRealtimetripDetails?tripID=\(( LineSearching[indexPath.row].TripId)!)"
            FirstUrl =  "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeLineDetailsfromTripID?tripID=\(( LineSearching[indexPath.row].TripId)!)"
        }
           else{
            RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetRealtimetripDetails?tripID=\(( route_list[indexPath.row].TripId)!)"
            
             FirstUrl =  "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeLineDetailsfromTripID?tripID=\(( route_list[indexPath.row].TripId)!)"
        }
        print(RouteUrl)
        //Alamofire.request(RouteUrl).responseObject { (response: DataResponse<RealTimeTripDetailsTripID>) in
            
            Alamofire.request(FirstUrl, method: .get, encoding: URLEncoding.default)
                .responseArray{ (response: DataResponse<[GetRealtimeLineDetailsfromTripID]>) in
                    switch response.result {
                    case .success:
                        if response.result.value != nil {
                        print(response.result.value!.count)
                          
                            if response.result.value![0].StopName != nil  &&  response.result.value!.count > 0{
                                    
                                   // RealTimeSchedule.SelectedScheduleRouteColor = response.result.value![0].Tripviewmodel![0].RouteColor ?? "000000"
                                   let realtimedata  = response.result.value
                                var StatusRealTime = ""
                                    for RealtimedataList in realtimedata! {
                                        if RealtimedataList.status! {
                                         StatusRealTime = "On Time"
                                        }
                                        else {
                                             StatusRealTime = "Passed"
                                        }
                                        let BusStopID = "\((RealtimedataList.StopId)!)"
                                        let BusStopName = RealtimedataList.StopName
                                        let ArravelTime = RealtimedataList.ArrivalTime
                            self.SchudleArrayValue.setValue(BusStopID, forKey: "StopID")
                            self.SchudleArrayValue.setValue(BusStopName, forKey: "BusStopName")
                            self.SchudleArrayValue.setValue(ArravelTime, forKey: "ArravelTime")
                            self.SchudleArrayValue.setValue(StatusRealTime, forKey: "StatusRealTime")
                                        
                            self.SchudleArrayList.append(self.SchudleArrayValue as! [String : Any])
                                       // Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.Refreshdata(TripID: TripIDForRefresh)), userInfo: nil, repeats: true)
                                         if self.timer == nil {
                                        self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector:#selector(self.RefreshDataFirst(sender:)), userInfo: TripIDForRefresh, repeats:true)
                                        }
                                    
                                    }
                             
                                
                                 self.selectedLine = true
                                 tableView.reloadData()
                                    
                                    self.hideLoader()
                                    
                                    
                                    
                                }
                                else{
            
            
            
                        
            Alamofire.request(RouteUrl, method: .get, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<RealTimeTripDetailsTripID>) in
            switch response.result {
            case .success:
                
                if response.result.value != nil && response.result.value?.BusStop != nil {
                    if (response.result.value?.BusStop!.count)! > 0 {
                
                RealTimeSchedule.SelectedScheduleRouteColor = response.result.value?.RouteColor ?? "000000"
                
                    let BustopsList = response.result.value?.BusStop!
                
                        for RealtimedataList in BustopsList!{
                            
                            
                            
                            var StatusRealTime = ""
                            if !RealtimedataList.Status! {
                                StatusRealTime = "Scheduled"
                            }
                            else {
                                StatusRealTime = "Passed"
                            }
                            let BusStopID = "\((RealtimedataList.StopId)!)"
                            let BusStopName = RealtimedataList.StopName
                            let ArravelTime = RealtimedataList.ArrivalTime
                            self.SchudleArrayValue.setValue(BusStopID, forKey: "StopID")
                            self.SchudleArrayValue.setValue(BusStopName, forKey: "BusStopName")
                            self.SchudleArrayValue.setValue(ArravelTime, forKey: "ArravelTime")
                            self.SchudleArrayValue.setValue(StatusRealTime, forKey: "StatusRealTime")
                            
                            self.SchudleArrayList.append(self.SchudleArrayValue as! [String : Any])
//                             if self.timer == nil {
//                             self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector:#selector(self.RefreshDataFirst(sender:)), userInfo: TripIDForRefresh, repeats:true)
//                            }
                            
                        }
//                let StatusRealTime = RealtimedataList.status
//                let BusStopID = RealtimedataList.StopId
//                let BusStopName = RealtimedataList.StopName
//                let ArravelTime = RealtimedataList.ArrivalTime
//                self.SchudleArrayValue.setValue(BusStopID, forKey: "StopID")
//                self.SchudleArrayValue.setValue(BusStopName, forKey: "BusStopName")
//                self.SchudleArrayValue.setValue(ArravelTime, forKey: "ArravelTime")
//                self.SchudleArrayValue.setValue(StatusRealTime, forKey: "StatusRealTime")
//
//                self.SchudleArrayList.append(self.SchudleArrayValue as! [String : Any])
//
                self.selectedLine = true
                self.scheduleParts = response.result.value!.BusStop!
                dump(response.result.value)
                tableView.reloadData()
                           self.hideLoader()
                }
                    else{
                        self.view.makeToast("Server Error! Please try again later!")
                        self.hideLoader()
                    }
                }
                else{
                    self.view.makeToast("Server Error! Please try again later!")
                    self.hideLoader()
                }
                
                
            case .failure(let error):
                print(error)
                self.view.makeToast("Server Error! Please try again later!")
                   self.hideLoader()
            }
        }
                                
                                   self.hideLoader()
                                }
                        
                    }
                        else{
                            self.view.makeToast("Server Error! Please try again later!")
                            self.hideLoader()
                        }
                    case .failure(let error):
                        print(error)
        self.view.makeToast("Server Error! Please try again later!")
                        
                        Alamofire.request(RouteUrl, method: .get, encoding: URLEncoding.default)
                            .responseObject{ (response: DataResponse<RealTimeTripDetailsTripID>) in
                                switch response.result {
                                case .success:
                                    
                                    if response.result.value != nil && response.result.value?.BusStop != nil {
                                        if (response.result.value?.BusStop!.count)! > 0 {
                                            
                                            RealTimeSchedule.SelectedScheduleRouteColor = response.result.value?.RouteColor ?? "000000"
                                            
                                            let BustopsList = response.result.value?.BusStop!
                                            
                                            for RealtimedataList in BustopsList!{
                                                
                                                
                                                
                                                var StatusRealTime = ""
                                                if !RealtimedataList.Status! {
                                                    StatusRealTime = "Scheduled"
                                                }
                                                else {
                                                    StatusRealTime = "Passed"
                                                }
                                                let BusStopID = "\((RealtimedataList.StopId)!)"
                                                let BusStopName = RealtimedataList.StopName
                                                let ArravelTime = RealtimedataList.ArrivalTime
                                                self.SchudleArrayValue.setValue(BusStopID, forKey: "StopID")
                                                self.SchudleArrayValue.setValue(BusStopName, forKey: "BusStopName")
                                                self.SchudleArrayValue.setValue(ArravelTime, forKey: "ArravelTime")
                                                self.SchudleArrayValue.setValue(StatusRealTime, forKey: "StatusRealTime")
                                                
                                                self.SchudleArrayList.append(self.SchudleArrayValue as! [String : Any])
//                                                  if self.timer == nil {
//                                                self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector:#selector(self.RefreshDataFirst(sender:)), userInfo: TripIDForRefresh, repeats:true)
//
//                                                }
                                                
                                            }
                                           
                                            self.selectedLine = true
                                            self.scheduleParts = response.result.value!.BusStop!
                                            dump(response.result.value)
                                            tableView.reloadData()
                                            self.hideLoader()
                                        }
                                        else{
                                            self.view.makeToast("Server Error! Please try again later!")
                                            self.hideLoader()
                                        }
                                    }
                                    else{
                                        self.view.makeToast("Server Error! Please try again later!")
                                        self.hideLoader()
                                    }
                                    
                                case .failure(let error):
                                    print(error)
                                    self.view.makeToast("Server Error! Please try again later!")
                                    self.hideLoader()
                                }
                        }
                        
                        
                        
                        
                     //  self.hideLoader()
                        
                    }
        
            }
        
//            if let lastStickyPoint = pullUpControllerAllStickyPoints.last {
//                pullUpControllerMoveToVisiblePoint(lastStickyPoint, animated: true, completion: nil)
//
//            }
//
         pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)
        
         if searching {
             (parent as? MapViewController)?.zoom(to: LineSearching[indexPath.row].TripId!)
        }
         else{
        (parent as? MapViewController)?.zoom(to: route_list[indexPath.row].TripId!)
        }
//        let UIViewForPartSchudle = UIView()
//
//        UIViewForPartSchudle.frame = firstPreviewView.frame
//        UIViewForPartSchudle.backgroundColor = .black
//        view.addSubview(UIViewForPartSchudle)
            self.VisitTarcSite.isHidden = true
            self.PDF.isHidden = true
            self.weekdayButton.isHidden = false
            self.saturdayButton.isHidden = false
            self.sundayButton.isHidden = false
                        self.AlertButton.isHidden = true
        }
        
        
        
        
    }
    func stopTimer() {
        if self.timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        dump(route_list)
        LineSearching =  route_list.filter { $0.TripTitle!.lowercased().contains(searchText.lowercased()) }
        searching = true
        print(LineSearching)
        tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBarLine.text = ""
        tableView.reloadData()
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: UIScreen.main.bounds.height)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.4)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    
    @objc func RefreshDataFirst(sender: Timer) {
        SchudleArrayList.removeAll()
        print(sender.userInfo!)
           let RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetRealtimetripDetails?tripID=\((sender.userInfo)!)"
        let FirstUrl =  "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeLineDetailsfromTripID?tripID=\((sender.userInfo)!)"
        print(FirstUrl)
        Alamofire.request(FirstUrl, method: .get, encoding: URLEncoding.default)
            .responseArray{ (response: DataResponse<[GetRealtimeLineDetailsfromTripID]>) in
                switch response.result {
                case .success:
                    if response.result.value != nil {
                        print(response.result.value!.count)
                        
                        if response.result.value![0].StopName != nil  &&  response.result.value!.count > 0{
                            
                            // RealTimeSchedule.SelectedScheduleRouteColor = response.result.value![0].Tripviewmodel![0].RouteColor ?? "000000"
                            let realtimedata  = response.result.value
                            var StatusRealTime = ""
                            for RealtimedataList in realtimedata! {
                                if RealtimedataList.status! {
                                    StatusRealTime = "On Time"
                                }
                                else {
                                    StatusRealTime = "Passed"
                                }
                                let BusStopID = "\((RealtimedataList.StopId)!)"
                                let BusStopName = RealtimedataList.StopName
                                let ArravelTime = RealtimedataList.ArrivalTime
                                self.SchudleArrayValue.setValue(BusStopID, forKey: "StopID")
                                self.SchudleArrayValue.setValue(BusStopName, forKey: "BusStopName")
                                self.SchudleArrayValue.setValue(ArravelTime, forKey: "ArravelTime")
                                self.SchudleArrayValue.setValue(StatusRealTime, forKey: "StatusRealTime")
                                
                                self.SchudleArrayList.append(self.SchudleArrayValue as! [String : Any])
                                // Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.Refreshdata(TripID: TripIDForRefresh)), userInfo: nil, repeats: true)
                               
                                
                            }
                            self.selectedLine = true
                            if self.SchudleArrayList.count > 0 {
                            self.tableView.reloadData()
                            }
                            
                           // self.hideLoader()
                            
                            
                            
                        }
                        else{
                            
                        }
                        
                    }
                    else{
                        self.view.makeToast("Server Error! Please try again later!")
                        self.hideLoader()
                    }
                case .failure(let error):
                    print(error)
                    
                    
                   // self.view.makeToast("Server Error! Please try again later!")
                    
                    
                    
                    self.hideLoader()
                    
                }
                
        }
    }
    
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
        
    }
    
}


extension UIViewController {
    func presentOnRoot(`with` viewController : UIViewController){
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: false, completion: nil)
    }
}
