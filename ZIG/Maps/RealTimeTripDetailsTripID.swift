//
//  RealTimeTripDetailsTripID.swift
//  ZIG
//
//  Created by Isaac on 10/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper

class RealTimeTripDetailsTripID:Mappable {
    var TripId : Int?
    var DepartureTime:String?
    var ArrivalTime:String?
    var RouteColor:String?
    var BusStop:[BusStopsSchedule]?
    var Shapes:[ShapesSchedule]?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        
        TripId <- map["TripId"]
        DepartureTime <- map["DepartureTime"]
        ArrivalTime <- map["ArrivalTime"]
        RouteColor <- map["RouteColor"]
        BusStop <- map["BusStops"]
        Shapes <- map["Shapes"]
    
        
    }
    
    
}
class BusStopsSchedule: Mappable {
    var StopName:String?
    var BustopLat:String?
    var BustopLong:String?
    var StopId:String?
    var DepartureTime:String?
    var ArrivalTime:String?
    var StopSequence:Int?
    var Status:Bool?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        StopName <- map["StopName"]
         BustopLong <- map["Long"]
         BustopLat <- map["Lat"]
         StopId <- map["StopId"]
         DepartureTime <- map["DepartureTime"]
         ArrivalTime <- map["ArrivalTime"]
         StopSequence <- map["StopSequence"]
         Status <- map["Status"]
        
        
    }
    
    
}

class ShapesSchedule: Mappable {
    var shapeLat:String?
    var ShapeLong:String?
    var shapeID:String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        shapeLat <- map["Lat"]
        ShapeLong <- map["Long"]
        shapeID <- map["ShapeId"]
        
    }
    
    
}
