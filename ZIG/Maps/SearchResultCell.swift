//
//  SearchResultCell.swift
//  PullUpControllerDemo
//
//  Created by Mario on 04/11/2017.
//  Copyright © 2017 Mario. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {
    
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var ArivalTime: UILabel!
    @IBOutlet weak var CornerImage: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var TripIDStopID: UILabel!
    func configure(Routename: String,TripID:String) {
        titleLabel.text = "\(Routename)"
        ArivalTime.text = ""
        CornerImage.image = UIImage(named:"ScheduleBus")
        Status.text = ""
        TripIDStopID.text = TripID
        
    }
    func Schedule(Stop:String,ArivalTimeTxt:String,StatusTxt:String,StopID:String){
        
        print(ArivalTimeTxt)
        titleLabel.text = "\(Stop)"
        ArivalTime.text = "\(ArivalTimeTxt)"
        CornerImage.image = UIImage(named: "Busstop")
        Status.text = "\(StatusTxt)"
        TripIDStopID.text = StopID
        if StatusTxt.contains("On Time"){
            Status.textColor = color.hexStringToUIColor(hex: "#007f00")
        }
        else if StatusTxt.contains("Passed") {
            
            Status.textColor = color.hexStringToUIColor(hex: "#DE1738")
            
        }
        else{
            Status.textColor = color.hexStringToUIColor(hex: "#EEA33F")
        }
        // Status.textColor =
    }
}


class NearmyLineResultCell: UITableViewCell {
    
    @IBOutlet weak var TransitImage: UIImageView!
    @IBOutlet weak var ViewColor: UIView!
    
    @IBOutlet weak var TimeETA: UILabel!
    @IBOutlet weak var RouteName: UILabel!
    // @IBOutlet weak var AddressImage: UIImageView!
    @IBOutlet weak var Address: UILabel!
    
    @IBOutlet weak var TypeBikeshare: UILabel!
    @IBOutlet weak var TmeSub: UILabel!
    
    @IBOutlet weak var Transit: UIView!
    @IBOutlet weak var BikeShare: UIView!
    @IBOutlet weak var Bike_Image: UIImageView!
    @IBOutlet weak var BikeName: UILabel!
    @IBOutlet weak var BikeSourceAddress: UILabel!
    @IBOutlet weak var DocksCount: UILabel!
    @IBOutlet weak var DocksName: UILabel!
    @IBOutlet weak var BikeCount: UILabel!
    
    @IBOutlet weak var BikeNameLabel: UILabel!
    
    
    func NearByLines(F_TransitMode:String, F_RouteName:String, F_Address:String, F_BikeAvaliable:String,F_DocksAvaliable:String,F_BikeType:String,F_DepartETA:String,F_DepartETAMetric:String,F_RealTime:Bool,F_RouteColor:String,F_RouteTextColor:String){
        
        if F_TransitMode == "Bike"{
            Transit.isHidden = true
            BikeShare.isHidden = false
            BikeName.text = F_RouteName
            BikeCount.text = F_BikeAvaliable
            DocksCount.text = F_DocksAvaliable
            BikeSourceAddress.text = F_Address
            //
            //            RouteName.text = F_RouteName
            //            Address.text = F_Address
            //            TimeETA.text = F_DepartETA
            //            TmeSub.text = F_DepartETAMetric
            
            
            
        }
        else{
            Transit.isHidden = false
            BikeShare.isHidden = true
            RouteName.text = F_RouteName
            Address.text = F_Address
            TimeETA.text = F_DepartETA
            TmeSub.text = F_DepartETAMetric
        }
        
        
        
        
        
        
        //        contentView.backgroundColor = color.hexStringToUIColor(hex: "#\(F_RouteColor)")
        
        
        // Status.textColor =
    }
}
class TripResultCell: UITableViewCell {
 
   
    
    @IBOutlet weak var TransitPanel: UIView!
    @IBOutlet weak var TransitLine: UIView!
    @IBOutlet weak var TransitImage: UIImageView!
    @IBOutlet weak var TransitDepartAddress: UILabel!
    @IBOutlet weak var TransitDepartTime: UILabel!
    @IBOutlet weak var TransitBusIcon: UIImageView!
    @IBOutlet weak var TransitAlertIcon: UIImageView!
    @IBOutlet weak var LineNumber: UILabel!
    @IBOutlet weak var Transitco2: UILabel!
    @IBOutlet weak var TransitETATime: UILabel!
   // @IBOutlet weak var TransitMinLabel: UILabel!
    @IBOutlet weak var TransitETALabel: UILabel!
    @IBOutlet weak var TransitOntimeStatus: UILabel!
    @IBOutlet weak var TransitBusStopAddress: UILabel!
    @IBOutlet weak var TransitNextStop: UILabel!
    @IBOutlet weak var TransitArrivalTime: UILabel!
    @IBOutlet weak var TransitArrivalAddress: UILabel!
    @IBOutlet weak var TransitRealTimeStatus: UIImageView!

    
    
    
    
    
    
    
    
    
    func TripResult(F_TransitMode:String, F_RouteName:String, F_Address:String, F_BikeAvaliable:String,F_BikeType:String,F_DepartETA:String,F_DepartETAMetric:String,F_RealTime:Bool,F_RouteColor:String,F_RouteTextColor:String){
        


    }
}
class BikeShareCarShare: UITableViewCell {
    
    
    @IBOutlet weak var BikeShare: UIView!
    @IBOutlet weak var BikeShareLine: UIView!
    @IBOutlet weak var BikeShareImage: UIImageView!
    @IBOutlet weak var SwipeImageOne: UIImageView!
    @IBOutlet weak var SwipeImageTwo: UIImageView!
    @IBOutlet weak var SwipeImageThree: UIImageView!
    @IBOutlet weak var BikeShareRightArrow: UIImageView!
    @IBOutlet weak var BikeShareLeftArrow: UIImageView!
    @IBOutlet weak var BikeSharePickup: UILabel!
    @IBOutlet weak var BikeShareAddrss: UILabel!
    @IBOutlet weak var BikeShareCo2: UILabel!
    @IBOutlet weak var BikeShareTravelMin: UILabel!
    @IBOutlet weak var BikeShareButton: UIButton!
    var BikeShareWalkingAboutandCo2 = UILabel()
    
    
    
}

class WalkingCell:UITableViewCell {
    
    @IBOutlet weak var WalkingPanel: UIView!
    @IBOutlet weak var WalkingLineView: UIView!
    @IBOutlet weak var WalkingImage: UIImageView!
    @IBOutlet weak var CalMiles: UILabel!
    @IBOutlet weak var waitTime: UILabel!
    @IBOutlet weak var TotalMin: UILabel!
    @IBOutlet weak var WalkingAddress: UILabel!
    
    
    
    
}

class WalkingCellBikeShare:UITableViewCell {
    
    @IBOutlet weak var WalkingPanel: UIView!
    @IBOutlet weak var WalkingLineView: UIView!
    @IBOutlet weak var WalkingImage: UIImageView!
    @IBOutlet weak var CalMiles: UILabel!
    @IBOutlet weak var Instrations: UILabel!
    @IBOutlet weak var TotalMin: UILabel!
    @IBOutlet weak var WalkingAddress: UILabel!
    
    
    
    
}

class BikeCellBikeShare:UITableViewCell {
    
    @IBOutlet weak var WalkingPanel: UIView!
    @IBOutlet weak var WalkingLineView: UIView!
    @IBOutlet weak var WalkingImage: UIImageView!
    @IBOutlet weak var PickupLine: UILabel!
    @IBOutlet weak var Instrations: UILabel!
    @IBOutlet weak var TotalMin: UILabel!
    @IBOutlet weak var WalkingAddress: UILabel!
    @IBOutlet weak var AboutMiles: UILabel!
    @IBOutlet weak var ActionBtn: UIButton!
    
    @IBOutlet weak var departime: UILabel!
    
    
    
}

class PaymentCell: UITableViewCell {
    
    
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    
    
    func TripResult(F_TransitMode:String, F_RouteName:String, F_Address:String, F_BikeAvaliable:String,F_BikeType:String,F_DepartETA:String,F_DepartETAMetric:String,F_RealTime:Bool,F_RouteColor:String,F_RouteTextColor:String){
      
      
    }
}
