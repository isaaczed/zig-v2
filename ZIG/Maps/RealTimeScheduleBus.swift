//
//  RealTimeScheduleBus.swift
//  ZIG
//
//  Created by Isaac on 11/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper

class RealTimeScheduleBus: Mappable {
    var vehicle : Bool?
    var lat : Double?
    var lng : Double?
    var tripID : Int?
    var blockID : AnyObject?
    var alert : AnyObject?
    var eTAminutes : String?
    var currStatus : AnyObject?
    var routeColor : AnyObject?
    var tripTitle : AnyObject?
    var routeID : AnyObject?
    var routeShortName : AnyObject?
    var arrivalTime : String?
    var departureTime : String?
    var stopID : String?
    var stopSequence : Int?
    var status : Bool?
    var realtime : Bool?
    var busStops : RealTimeBusStopSchudle?
    var linelist : [AnyObject]?
    var tripStatus : String?
    var timeTravelled : AnyObject?
    var resetTripFlag : Bool?
    var staticArrivalTime : String?
    var staticDepartureTime : String?
    var nextstops : [RealTimeNextStopSchudle]?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        arrivalTime <- map["ArrivalTime"]
        blockID <- map["BlockID"]
        busStops <- map["BusStops"]
        departureTime <- map["DepartureTime"]
        eTAminutes <- map["ETAminutes"]
        linelist <- map["Linelist"]
        nextstops <- map["Nextstops"]
        realtime <- map["Realtime"]
        resetTripFlag <- map["ResetTripFlag"]
        routeID <- map["RouteID"]
        routeShortName <- map["RouteShortName"]
        staticArrivalTime <- map["StaticArrivalTime"]
        staticDepartureTime <- map["StaticDepartureTime"]
        stopID <- map["StopID"]
        stopSequence <- map["StopSequence"]
        timeTravelled <- map["TimeTravelled"]
        tripStatus <- map["TripStatus"]
        alert <- map["alert"]
        currStatus <- map["currStatus"]
        lat <- map["lat"]
        lng <- map["lng"]
        routeColor <- map["routeColor"]
        status <- map["status"]
        tripID <- map["tripID"]
        tripTitle <- map["tripTitle"]
        vehicle <- map["vehicle"]
    }
    
    
}
class RealTimeBusStopSchudle: Mappable {
    
    var arrivalTime : AnyObject?
    var busStops : [AnyObject]?
    var departureTime : AnyObject?
    var distance : AnyObject?
    var nextStops : [RealTimeNextStopSchudle]?
    var routeColor : AnyObject?
    var routeNumber : AnyObject?
    var routeTextColor : AnyObject?
    var shapeId : String?
    var shapes : [AnyObject]?
    var tripId : Int?
    var tripTitle : String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        arrivalTime <- map["ArrivalTime"]
        busStops <- map["BusStops"]
        departureTime <- map["DepartureTime"]
        distance <- map["Distance"]
        nextStops <- map["NextStops"]
        routeColor <- map["RouteColor"]
        routeNumber <- map["RouteNumber"]
        routeTextColor <- map["RouteTextColor"]
        shapeId <- map["ShapeId"]
        shapes <- map["Shapes"]
        tripId <- map["TripId"]
        tripTitle <- map["TripTitle"]
    }
    
    
}

class RealTimeNextStopSchudle: Mappable {
    var arrivalTime : String?
    var eTA : String?
    var stopDistance : String?
    var stopId : AnyObject?
    var stopName : String?
    var status : Bool?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        arrivalTime <- map["ArrivalTime"]
        eTA <- map["ETA"]
        stopDistance <- map["StopDistance"]
        stopId <- map["StopId"]
        stopName <- map["StopName"]
        status <- map["status"]
    }
    

}
