//
//  GetRealtimeLineDetailsfromTripID.swift
//  ZIG
//
//  Created by Isaac on 12/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper

class GetRealtimeLineDetailsfromTripID: Mappable {
    var StopName:String?
    var ETA:String?
    var ArrivalTime : String?
    var StopDistance : String?
    var StopId : String?
    var status : Bool?
    var Message:String?
    var Tripviewmodel:[RealTimeTripDetailsTripID]?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
            StopName <- map["StopName"]
            ETA <- map["ETA"]
            ArrivalTime <- map["ArrivalTime"]
            StopDistance <- map["StopDistance"]
            StopId <- map["StopId"]
            status <- map["status"]
            Tripviewmodel <- map["Tripviewmodel"]
            Message <- map["Message"]
        
        
        
        
    }
    
    
}

