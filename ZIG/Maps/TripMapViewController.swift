//
//  TripMapViewController.swift
//  ZIG
//
//  Created by Isaac on 30/08/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import Toast_Swift
import XLActionController
import SafariServices
import SwiftyJSON
import NVActivityIndicatorView
import NYAlertViewController
import LyftSDK
import UberCore
import UberRides
import AlamofireObjectMapper
import ObjectMapper
import Foundation
import SwiftGifOrigin
import NVActivityIndicatorView
import UIDropDown
import SCLAlertView

  class TripMapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource{
            let assistiveTouchSOS = AssistiveTouch()
        @IBOutlet private weak var mapView: MKMapView!
        var mylocation = CLLocationCoordinate2D(latitude: 38.2526647, longitude: -85.75845570000001)
        var locationManager = CLLocationManager()
        var lat = 0.0
        var lng = 0.0
        var realTimeTimer : Timer!
        var realtimeDataResponse:DataResponse<TransitRouteClass>!
        var tanktransitOptions : NSMutableArray = []
    var DataWalkingResponseLouveloStart:DataResponse<SuggestWalkingDirectionalClass>!
    var DataWalkingResponseLouveloEnd:DataResponse<SuggestWalkingDirectionalClass>!
    var louveloBikeResponse:DataResponse<SuggestBikeDirectionalClass>!
        //var valueResult:DataResponse<SuggestDirectionaClass>!
        var timer:Timer!
        var firstMileBool = Bool()
        var loaderView = UIView()
        var bikefromaddress:String?
        var biketoAddess:String?
    @IBOutlet weak var FavButton: UIButton!
        //@IBOutlet private weak var widthSlider: UISlider!
        //  @IBOutlet private weak var initialStateSegmentedControl: UISegmentedControl!
        var initialStateSegmentedControl:UISegmentedControl!
        private var originalPullUpControllerViewSize: CGSize = .zero
        var scheduleParts = [ShapesSchedule]()
        var coordinateArray: [CLLocationCoordinate2D] = []
        var walkingfromaddress:String?
        var walkingtoAddess:String?
        var AmenititesTypeTrip:String?
        var ameniticsTitle:String?
     var realtimeIcon = MKPointAnnotation()
        var BikeCost:Int?
        var isLimeredirect =  NSString()
        var isClickedFromLouvile = Bool()
        var databikeResponseFromDirection:DataResponse<SuggestBikeDirectionalClass>!
        var dataWalkingDirection:DataResponse<SuggestWalkingDirectionalClass>!
        
        var LimeDatalistapicount:Int?
        var LimeDataWalkingResponse:DataResponse<SuggestWalkingDirectionalClass>!
        
        var LimedataBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
        
        var LimeWalkingDuration = 0
        
        var indexValue = 0
        var Birdroute_Array = MKRoute()
        var Walkingroute_Array = MKRoute()
          var Uberroute_Array = MKRoute()
        var PolylineColor = ""
    
        var SourceAddressString:String?
        var DestinationAddressString:String?
        //MAp View
        var SourceAddressLat:Double?
        var DestinationAddressLat:Double?
        var SourceAddressLong:Double?
        var DestinationAddressLong:Double?
        var SourceAddress:String?
        var Destination:String?
        var bikeSource:String?
        var bikeSourceLat:Double?
        var BikeSourceLong:Double?
        var menuButton = UIBarButtonItem()
        var DestinationAddress:String?
        var testtt : NSMutableDictionary=[:]
        var limeroute = MKRoute()
        var route = MKRoute()
        var busroutePolyline : MKPolyline?
        var limeroutePolyline : MKPolyline?
        var sourceAnnotation = MKPointAnnotation()
        var destinationAnnotation = MKPointAnnotation()
        var transitSourceAnnotation = MKPointAnnotation()
        var transitDestinationAnnotation = MKPointAnnotation()

        var bikeAnnotation = MKPointAnnotation()
        
        // Louvile
        var SourceAddressFrom:String?
        var sourceAddressLat:Double?
        var DestinationLat:Double?
        var LouveloStationSourceLat:Double?
        var LouveloStationDestinationLat:Double?
        var LouveloSourceStationID:String?
        var LouveloSourceStationAddress:String?
        
        var sourceAddressLong:Double?
        var DestinationLong:Double?
        var LouveloStationSourceLong:Double?
        var LouveloStationDestinationLong:Double?
        var LouveloDestinationStationID:String?
        var LouveloDestinationStationAddress:String?
        
        var DataBikeWalking:DataResponse<SuggestWalkingDirectionalClass>!
        var DestinationWalking:DataResponse<SuggestWalkingDirectionalClass>!
        var dataBikeResponse: DataResponse<SuggestBikeDirectionalClass>!

        var SourceWalkingDistanceText:String?
        var sourceWakingDurationText:String?
        var SourceWalkingDistanceValue:Int?
        var sourceWakingDurationValue:Int?
        
        var LouveloWalkingDistanceText:String?
        var LouveloWakingDurationText:String?
        var LouveloWalkingDistanceValue:Int?
        var LouveloWakingDurationValue:Int?
        
        var DestinationWalkingDistanceText:String?
        var DestinationWakingDurationText:String?
        var DestinationWalkingDistanceValue:Int?
        var DestinationWakingDurationValue:Int?
        
        var StepdatailsWalking = [SuggestwalkingStep]()
        var DestinationStepdatailsWalking = [SuggestwalkingStep]()
        var StepdatailsBike = [SuggestBikeStep]()
        
        var dataPrevRespone:DataResponse<SuggestDirectionaClass>!
        var dataPrevBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
        var googlereponseDirection = [[String:Any]]()
        var isUberAdded : Bool = false
    
        var StartMarker = MKPointAnnotation()
        var EndMarker = MKPointAnnotation()
        var StartTransitMarker = MKPointAnnotation()
        var EndTransitMarker = MKPointAnnotation()
        var LouveloMarker = MKPointAnnotation()
    var boltMarker = MKPointAnnotation()
    var LouveloMarkerEnd = MKPointAnnotation()
      var BirdMarker = MKPointAnnotation()
    var LimeMarker = MKPointAnnotation()
        var DisplayType = ""
    var popupopen = true
    var blurEffectView = UIVisualEffectView()
    var amenityView = UIView()
    var amenityImageViewX = 0,amenityImageViewY = 0,amenityImageViewWidth = 0,amenityImageViewHeight = 0
    var amenityCellLabelX = 0,amenityCellLabelY = 0,amenityCellLabelWidth = 0,amenityCellLabelHeight = 0
    var amenityViewX = 0,amenityViewY = 0,amenityViewWidth = 0,amenityViewHeight = 0,uberButtonX = 0, clearMarkerX = 0, clearMarkerY = 0, clearMarkerWidth = 80, clearMarkerHeight = 40
    var collectionview: UICollectionView!
    var cellId = "Cell"
    var paybackSlider : UISlider = UISlider()
    var sliderValue = 1
    var paybackLabel : UILabel = UILabel()
    var amenityTypeString =  "Near my source"
    var isPlaceSelected = true
      var typeString = "bus_station"
      var busImageUrl = String()
    let markerClearButton = UIButton()
    var busLat = 0.0
      var busLng = 0.0
    struct State {
          let name: String
          let long: CLLocationDegrees
          let lat: CLLocationDegrees
          let placeId : String
          let formattedAddress : String
          let type : String
          let imageUrl : String
      }
    var markersArray: [State] = []
    var markerDict: NSMutableArray = []
    var redBikeStatus:DataResponse<RedBikeStatusClass>!
    var redBikeInformation: DataResponse<RedBikeClass>!
    var eventArrayList = [String]()
    var categoryNameString = NSString()
    var placesAnnotation = MKPointAnnotation()
    var annotationArray = NSArray()
    var previosmarkersArray: [State] = []
    var previousTitle = String()
    var currentTitle = NSString()
    var annoationRemove : Bool = false

        private func makeSearchViewControllerIfNeeded() -> TripPanelViewController {
            let currentPullUpController = childViewControllers
                .filter({ $0 is TripPanelViewController })
                .first as? TripPanelViewController
            
            let pullUpController: TripPanelViewController = currentPullUpController ?? UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "TripPanelViewController") as! TripPanelViewController
            
            pullUpController.sourceAddressLat = sourceAddressLat
            pullUpController.sourceAddressLong = sourceAddressLong
            pullUpController.DestinationLat = DestinationLat
            pullUpController.DestinationLong = DestinationLong
            if SourceDestination.DisplayType == "LouVelo"{
                 print("\(DisplayType)--DisplayType Louvelo")
                pullUpController.DataWalkingResponseLouveloStart = DataWalkingResponseLouveloStart
                pullUpController.DataWalkingResponseLouveloEnd = DataWalkingResponseLouveloEnd
                pullUpController.louveloBikeResponse = louveloBikeResponse
                pullUpController.isLimeredirect = isLimeredirect
                pullUpController.DisplayType = DisplayType
                
            }
            else{
          if  firstMileBool == true
            {
                pullUpController.AmenititesTypeTrip = self.AmenititesTypeTrip
                        pullUpController.ameniticsTitle = self.ameniticsTitle
                        pullUpController.dataPrevRespone = dataPrevRespone
                        pullUpController.indexValue = indexValue
                        pullUpController.firstMileBool = true
               
                pullUpController.googlereponseDirection = googlereponseDirection
            }
            else
          {
            pullUpController.bikefromaddress = bikefromaddress
            pullUpController.biketoAddess = biketoAddess
            let walkingeresponse = dataWalkingDirection.result.value?.route!
            for datawalking in walkingeresponse!{
                let datalegs = datawalking.legs!
                for datalegsdata in datalegs {
                    pullUpController.walkingfromaddress = datalegsdata.start_address!
                    pullUpController.walkingtoAddess = datalegsdata.end_address!
                }
            }
//            nextViewController.isLimeredirect = "Louvelo"
//                nextViewController.DisplayType = "Louvelo"
            pullUpController.DisplayType = DisplayType
            print("\(DisplayType)--DisplayType")
            pullUpController.AmenititesTypeTrip = self.AmenititesTypeTrip
            pullUpController.ameniticsTitle = self.ameniticsTitle
            pullUpController.BikeCost = BikeCost
            pullUpController.isLimeredirect = isLimeredirect
            pullUpController.databikeResponseFromDirection = databikeResponseFromDirection
            pullUpController.dataWalkingDirection = dataWalkingDirection
            pullUpController.firstMileBool = false
            pullUpController.DataWalkingResponseLouveloStart = DataWalkingResponseLouveloStart
            pullUpController.DataWalkingResponseLouveloEnd = DataWalkingResponseLouveloEnd
            pullUpController.louveloBikeResponse = louveloBikeResponse
            
//            var DataWalkingResponseLouveloStart:DataResponse<SuggestWalkingDirectionalClass>!
//               var DataWalkingResponseLouveloEnd:DataResponse<SuggestWalkingDirectionalClass>!
//               var louveloBikeResponse:DataResponse<SuggestBikeDirectionalClass>!
           
            if originalPullUpControllerViewSize == .zero {
                originalPullUpControllerViewSize = pullUpController.view.bounds.size
            }
            }
    }
            return pullUpController
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            self.showLoader()
            previousTitle = ""
            print(isLimeredirect)
            self.title = "Directions"
            mapView.delegate = self
            analytics.GetPageHitCount(PageName: "Direction")

            let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
            let region = MKCoordinateRegion(center: mylocation, span: span)
            mapView.setRegion(region, animated: true)
           AddMarker()
            
            if isClickedFromLouvile == true
               {
              
                addPullUpController()
            }
            else
               {
                addPullUpController()

            }
            
          
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationItem.setHidesBackButton(true, animated:true);
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
            
            self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
            self.navigationController?.navigationBar.tintColor = UIColor.white
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
            
            self.view.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
            let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                             style: UIBarButtonItemStyle.done ,
                                             target: self, action: #selector(OnBackClicked))
            backButton.accessibilityLabel = "Back"

            self.navigationItem.leftBarButtonItem = backButton
            
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
            
            let xPostion = self.view.frame.size.width/2 - 50
//                  let   whatsNearMe = UIButton()
//                                 whatsNearMe.frame = CGRect.init(x: xPostion, y: 10, width: 100, height: 40)
//                                 whatsNearMe .setTitle("Routes & Schedules", for: .normal)
//                                 whatsNearMe.setTitleColor(UIColor.white, for: .normal)
//                         whatsNearMe.setBackgroundColor(UIColor.init(red: 42/255.0, green: 127/255.0, blue: 195/255.0, alpha: 1.0), for: .normal)
//                                 whatsNearMe.titleLabel?.font  = UIFont.init(name: "Linotte-Bold", size: 14)
//                  whatsNearMe.titleLabel!.numberOfLines = 1;
//                  whatsNearMe.titleLabel!.adjustsFontSizeToFitWidth = true;
//                                 whatsNearMe.addTarget(self, action: #selector(nearbyAction), for: .touchUpInside)
//                                 view.addSubview(whatsNearMe)
                               //  mapView.bringSubview(toFront: whatsNearMe)
            
            if UIScreen.main.sizeType == .iPhone4{
       
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 20
                amenityImageViewHeight = 20
                amenityCellLabelX = 30
                amenityCellLabelY = -3
                amenityCellLabelWidth = 20
                amenityCellLabelHeight = 30
                amenityViewX = 30
                amenityViewY = 80
                amenityViewWidth = 60
                amenityViewHeight = 250
            }else if UIScreen.main.sizeType == .iPhone5{
            
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 20
                amenityImageViewHeight = 20
                amenityCellLabelX = 30
                amenityCellLabelY = -3
                amenityCellLabelWidth = 20
                amenityCellLabelHeight = 30
                amenityViewX = 30
                amenityViewY = 80
                amenityViewWidth = 60
                amenityViewHeight = 250
            }else if UIScreen.main.sizeType == .iPhone6{
         
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 30
                amenityImageViewHeight = 30
                amenityCellLabelX = 35
                amenityCellLabelY = -6
                amenityCellLabelWidth = 30
                amenityCellLabelHeight = 40
                amenityViewX = 60
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250
                uberButtonX = 5
            }else if UIScreen.main.sizeType == .iPhone6Plus{
       
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 40
                amenityImageViewHeight = 40
                amenityCellLabelX = 45
                amenityCellLabelY = 0
                amenityCellLabelWidth = 30
                amenityCellLabelHeight = 40
                amenityViewX = 60
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250
         
                uberButtonX = 20
            }
            else if UIScreen.main.nativeBounds.height == 1920 {
                print("iPhone 6plus or 7plus or 8plus")
         
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 40
                amenityImageViewHeight = 40
                amenityCellLabelX = 45
                amenityCellLabelY = 0
                amenityCellLabelWidth = 30
                amenityCellLabelHeight = 40
                amenityViewX = 60
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250
        
                uberButtonX = 20
                
            }
            else if UIScreen.main.nativeBounds.height == 2688 {
                print("iPhone XS max")
    
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 40
                amenityImageViewHeight = 40
                amenityCellLabelX = 45
                amenityCellLabelY = 0
                amenityCellLabelWidth = 30
                amenityCellLabelHeight = 40
                amenityViewX = 50
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250

                uberButtonX = 10
            }
            else if UIScreen.main.nativeBounds.height == 1792 {
                print("iPhone Xr max")
               
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 40
                amenityImageViewHeight = 40
                amenityCellLabelX = 45
                amenityCellLabelY = 0
                amenityCellLabelWidth = 30
                amenityCellLabelHeight = 40
                amenityViewX = 50
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250
                
                uberButtonX = 10
                
            }
                
            
            else if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
               
                amenityImageViewX = 70
                amenityImageViewY = 0
                amenityImageViewWidth = 60
                amenityImageViewHeight = 60
                amenityCellLabelX = 150
                amenityCellLabelY = -5
                amenityCellLabelWidth = 60
                amenityCellLabelHeight = 80
                amenityViewX = 60
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250
            }else if UIScreen.main.sizeType == .iPadPro105{
              
                amenityImageViewX = 70
                amenityImageViewY = 0
                amenityImageViewWidth = 60
                amenityImageViewHeight = 60
                amenityCellLabelX = 150
                amenityCellLabelY = -5
                amenityCellLabelWidth = 60
                amenityCellLabelHeight = 80
                amenityViewX = 60
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250
                
            }else if UIScreen.main.sizeType == .iPhoneX
            {
               
                amenityImageViewX = 0
                amenityImageViewY = 0
                amenityImageViewWidth = 40
                amenityImageViewHeight = 40
                amenityCellLabelX = 45
                amenityCellLabelY = 0
                amenityCellLabelWidth = 30
                amenityCellLabelHeight = 40
                amenityViewX = 50
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 250
                
                uberButtonX = 10
            }  else if UIScreen.main.sizeType == .iPadPro129{
               
                amenityImageViewX = 70
                amenityImageViewY = 0
                amenityImageViewWidth = 60
                amenityImageViewHeight = 60
                amenityCellLabelX = 150
                amenityCellLabelY = -5
                amenityCellLabelWidth = 60
                amenityCellLabelHeight = 80
                amenityViewX = 60
                amenityViewY = 100
                amenityViewWidth = 60
                amenityViewHeight = 180
              
                uberButtonX = 10
                
            }
            amenityView.frame = CGRect.init(x: 30, y: -2000, width: self.view.frame.size.width-60, height: self.view.frame.size.height-100)
            amenityView.backgroundColor = UIColor.clear
            
            
            

            


               
             
                   let assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
            assistiveTouch.accessibilityLabel = "Quick Menu"

                  assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
                   assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
                   mapView.addSubview(assistiveTouch)
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
            tapGesture.numberOfTapsRequired = 1
            assistiveTouchSOS.addGestureRecognizer(tapGesture)
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
            longPress.minimumPressDuration = 1.0
            self.assistiveTouchSOS.addGestureRecognizer(longPress)
            assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
            assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
          //  view.addSubview(assistiveTouchSOS)
          
      


            mapView.showsCompass = false
            CheckFavTrip()
            
        }
    @objc func nearbyAction()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let vc = storyboard.instantiateViewController(withIdentifier: "WhatsNearMeMapViewController") as! WhatsNearMeMapViewController
               // vc.newsObj = newsObj
               self.present(vc, animated: true, completion: nil)
    }
          @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
          if gesture.state == UIGestureRecognizerState.began {
              UIDevice.vibrate()
              print("Long Press")
              currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                         if Success{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                          print("\(Address),\(LatsosLat),\(Longsoslong)")
                          MapsScheduleVC.CurrentlocationAddressString = Address
                          MapsScheduleVC.latSOS = LatsosLat
                          MapsScheduleVC.longSOS = Longsoslong
                        self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                 //self.CurrentlocationAddress.text = Address
                             }
                         }
              
          }
      }
    
      @objc func tappedSOS(sender: UIButton) {
       
          let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                  // action here
          }
                  
          SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
         
      }
    
    func CheckFavTrip() {
          //print(self.sourceTextField.text != "" && self.destinationTextField.text != "")
       
        print(SourceAddressString!)
        print(DestinationAddressString!)

          if SourceAddressString! != "" && DestinationAddressString != "" {
              FavButton.isEnabled = true
              let userDefaults1 = UserDefaults.standard
              let accessToken = userDefaults1.value(forKey: "accessToken")
              let url = "\(APIUrl.ZIGTARCAPI)SaveTrips/Get"
              let parameters: Parameters = [
                  "Token": accessToken!
              ]
              Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                  .responseObject{ (response: DataResponse<SavedRootClass>) in
                      switch response.result {
                      case .success:
                          if response.result.value?.trips != nil
                          {
                              for responseData in (response.result.value?.trips)!
                              {

                                if !(responseData.sourceAddress == self.SourceAddressString! && responseData.destinationAddress == self.DestinationAddressString!)
                                  {
                                      self.FavButton.setImage(UIImage(named: "FavDeactive"), for: .normal)
                                  }
                                  else
                                  {
                                      self.FavButton.setImage(UIImage(named: "FavActive"), for: .normal)
                                      break
                                      
                                  }
                              }
                          }
                          else
                          {
                              self.FavButton.setImage(UIImage(named: "FavDeactive"), for: .normal)
                          }
                          
                      case .failure(let error):
                          print(error.localizedDescription)
                      }
              }
              
          }
          else{
              FavButton.isEnabled = false
          }
      }
    
    @objc func sliderValueDidChange(sender: UISlider!)
    {
        print("payback value: \(sender.value)")
        sliderValue = Int(sender.value)
        let userDefaults = UserDefaults.standard
        userDefaults.set(sliderValue, forKey: "distanceValue")
        userDefaults.synchronize()
        if sliderValue == 1 {
            paybackLabel.text = "\(sliderValue) mile"
        }
        else
        {
            paybackLabel.text = "\(sliderValue) miles"
        }
    }

        @objc func closebuttonAmenityAction(sender: UIButton!) {
            print("Button tapped")
            UIView.animate(withDuration: 1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            if self.collectionview != nil
                            {
                                self.collectionview.removeFromSuperview()
                                self.collectionview = nil
                            }
                            self.amenityView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.size.width-80, height: self.view.frame.size.height-140)
                            
                            
            }, completion: { (finished) -> Void in
                self.blurEffectView.isHidden = true;
    //            if self.isPlaceSelected
    //            {
    //                self.getPlaces(type: self.typeString)
    //            }
    //            else
    //            {
    //
    //                self.plotTARCTourism(category: self.typeString)
    //            }
            })
        }

        func plotTARCTourism(category:String){
            var EventCountList = 0
            let resultRes = dataPrevRespone.result.value?.routes![indexValue].legs![0];
            var latString = ""
            var lngString = ""
            print(category)
            markerClearButton.isHidden = false
            self.showLoader()
            
            if amenityTypeString == "Near my source"
            {
           // sourceAddressLat
                latString = "\((sourceAddressLat)!)"
                lngString = "\((sourceAddressLong)!)"
            }
            else if amenityTypeString == "Near my destination"
            {
                latString = "\((DestinationLat)!)"
                lngString = "\((DestinationLong)!)"
            }
            else if amenityTypeString == "Along my bus"
            {
                latString = "\(busLat)"
                lngString = "\(busLng)"
                
            }
            
            
            let plotTARCURL = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/events/getlist"
            var tourismArray = [NSDictionary]()
            let url1 = URL(string: plotTARCURL)
            URLSession.shared.dataTask(with: url1!, completionHandler: {
                (data, response, error) in
                if(error != nil){
                    print("error")
                }else{
                    do{
                        let json : NSArray = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! NSArray
                        //let posts = json["posts"] as? [[String: Any]] ?? []
                        print("Response:\(json)")
                        //self.mapView.clear()
                        print(json.count)
                        OperationQueue.main.addOperation({
                            for services in json
                            {
                                
                                let tourismData : NSDictionary = services as! NSDictionary
                                tourismArray.append(tourismData)
                                print(tourismData.object(forKey: "EventName")!)
                                let eventName = tourismData.object(forKey: "EventName") as! String
                                if eventName == category{
                                    
                                    DispatchQueue.main.async{
                                        for markerDet in self.markerDict
                                        {
                                            //APLOAD
                                         //   let removeMarker : GMSMarker = markerDet as! GMSMarker
                                          //  removeMarker.map = nil;
                                        }
                                        self.markerDict.removeAllObjects()
                                        self.markersArray.removeAll()
                                        
                                        
                                        let list = tourismData.object(forKey: "list") as! NSArray
                                        
                                        for listData in list{
                                            let lat = Double((listData as AnyObject).object(forKey: "latitude") as! String)
                                            let long = Double((listData as AnyObject).object(forKey: "longitude") as! String)
                                            print(listData)
                                            print(lat)
                                            print(long)
                                            let distanceInMeters:Double?
                                            if lat != nil || long != nil  {
                                            let coordinate0 = CLLocation(latitude: Double(latString)!, longitude: Double(lngString)!)
                                            let coordinate1 = CLLocation(latitude: lat! , longitude: long!)
                                            
                                             distanceInMeters = coordinate0.distance(from: coordinate1) // result is in meters
                                            
                                            }
                                            else {
                                                distanceInMeters = Double(self.sliderValue*1000) + 20.0
                                            }
                                            let radius = self.sliderValue*1000
                                            
                                            if Int(distanceInMeters!) <= radius{
                                                
                                                var title = (listData as AnyObject).object(forKey: "title") as! String
                                                let StringRemoveSpace =  title.components(separatedBy: .whitespacesAndNewlines)
                                                let StringJoin = (StringRemoveSpace.filter { $0 != "" }.joined(separator: " "))
                                                //  print(StringJoin.joinWithSeparator(" "))
                                                // title = String(title.filter { !" \t".contains($0) })
                                                title = StringJoin.decodeEnt()
                                                
                                                 self.markersArray.append(State(name: title, long: CLLocationDegrees(lat!) , lat: CLLocationDegrees(long!), placeId: "", formattedAddress: (listData as AnyObject).object(forKey: "summary") as! String, type: category, imageUrl: (listData as AnyObject).object(forKey: "imgurl") as! String))
                                            }
                                            
                                            
                                        }
                                        
                                        
                                        self.removeSpecificAnnotation()

                                        if self.markersArray.count > 0{
                                            
                                            for state in self.markersArray {
                                                //APHIDE
                                               // let state_marker = GMSMarker()
                                                let latMaker = state.lat
                                                let lngMaker = state.long
                                                print(latMaker,lngMaker)
    //                                            state_marker.position = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
    //                                            state_marker.title = state.name
    //                                            state_marker.icon = UIImage(named: category)
    //                                            state_marker.snippet = state.formattedAddress
    //                                            state_marker.map = self.mapBackgroundView
    //                                            self.markerDict.add(state_marker)
                                                
                                                self.categoryNameString = category as NSString
                                                self.placesAnnotation = MKPointAnnotation()
                                                self.placesAnnotation.title = self.currentTitle as String
                                                self.placesAnnotation.subtitle = "\(state.name)\n\(state.formattedAddress)"
                                                self.placesAnnotation.coordinate = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
                                                self.mapView.addAnnotation(self.placesAnnotation)
                                                
                                                
                                            }
                                            self.hideLoader()
                                        }else{
                                            if self.busLat == 0.0 && self.busLng == 0.0 &&  self.amenityTypeString == "Along my bus" {
                                                
                                                let alertViewController = NYAlertViewController()
                                                // Set a title and message
                                                alertViewController.title = "Places"
                                                alertViewController.message = "Real-time bus not available!"
                                                
                                                // Customize appearance as desired
                                                alertViewController.buttonCornerRadius = 20.0
                                                alertViewController.view.tintColor = self.view.tintColor
                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.swipeDismissalGestureEnabled = true
                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                alertViewController.buttonColor = UIColor.red
                                                // Add alert actions
                                                
                                                
                                                let cancelAction = NYAlertAction(
                                                    title: "OK",
                                                    style: .cancel,
                                                    handler: { (action: NYAlertAction!) -> Void in
                                                        
                                                        self.dismiss(animated: true, completion: nil)
                                                }
                                                )
                                                
                                                alertViewController.addAction(cancelAction)
                                                
                                                // Present the alert view controller
                                                self.present(alertViewController, animated: true, completion: nil)
                                                
                                            }
                                            else{
                                            self.hideLoader()
                                            let alertViewController = NYAlertViewController()
                                            // Set a title and message
                                            alertViewController.title = "Places"
                                            alertViewController.message = "No results found for given radius. Please increase the radius."
                                            
                                            // Customize appearance as desired
                                            alertViewController.buttonCornerRadius = 20.0
                                            alertViewController.view.tintColor = self.view.tintColor
                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                            alertViewController.swipeDismissalGestureEnabled = true
                                            alertViewController.backgroundTapDismissalGestureEnabled = true
                                            alertViewController.buttonColor = UIColor.red
                                            // Add alert actions
                                            
                                            
                                            let cancelAction = NYAlertAction(
                                                title: "OK",
                                                style: .cancel,
                                                handler: { (action: NYAlertAction!) -> Void in
                                                    
                                                    self.dismiss(animated: true, completion: nil)
                                            }
                                            )
                                            
                                            alertViewController.addAction(cancelAction)
                                            
                                            // Present the alert view controller
                                            self.present(alertViewController, animated: true, completion: nil)
                                            }
                                        }
                                        
                                        self.blurEffectView.isHidden = true
                                        if self.collectionview != nil{
                                        self.collectionview.removeFromSuperview()
                                             self.hideLoader()
                                        }
                                        }
                              
                                }
                                else{
                                    EventCountList =  EventCountList + 1
                                }
                                
                            }
                            
                            if json.count == EventCountList {
                                self.hideLoader()
                                print("There are no events available for this location ")
                                let alertViewController = NYAlertViewController()
                                // Set a title and message
                                alertViewController.title = "Events"
                                alertViewController.message = "There are no events available for this location."
                                
                                // Customize appearance as desired
                                alertViewController.buttonCornerRadius = 20.0
                                alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.swipeDismissalGestureEnabled = true
                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                alertViewController.buttonColor = UIColor.red
                                // Add alert actions
                                
                                
                                let cancelAction = NYAlertAction(
                                    title: "OK",
                                    style: .cancel,
                                    handler: { (action: NYAlertAction!) -> Void in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                }
                                )
                                
                                alertViewController.addAction(cancelAction)
                                
                                // Present the alert view controller
                                self.present(alertViewController, animated: true, completion: nil)
                                
                            }
                            
                        })
                        
                        
                    }catch let error as NSError{
                        self.hideLoader()
                        print("error:\(error)")
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Places"
                        alertViewController.message = "No results found for given radius. Please increase the radius."
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                    }
                }
            }).resume()
            
            
            
        }
    
    /// @
      @IBAction func amentiyAction() {
                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.prominent)
                blurEffectView.removeFromSuperview()
                blurEffectView = UIVisualEffectView()
                blurEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = view.bounds
                blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                self.view.addSubview(blurEffectView)
                blurEffectView.contentView.addSubview(amenityView)
                
                let closeButton = UIButton(frame: CGRect(x: 14, y: 16, width: 50, height: 50))
                closeButton.setImage(UIImage(named: "closeBtn"), for: .normal)
                closeButton.addTarget(self, action: #selector(closebuttonAmenityAction), for: .touchUpInside)
                blurEffectView.contentView.addSubview(closeButton)
                amenityView.removeFromSuperview()
                amenityView = UIView()
                print(self.amenityViewX)
                print(self.self.amenityViewY)
                print(Int(self.view.frame.size.width)-self.amenityViewWidth)
                print(Int(self.view.frame.size.height)-self.amenityViewHeight)

                                    self.amenityView.frame = CGRect.init(x: self.amenityViewX, y: self.self.amenityViewY, width: Int(self.view.frame.size.width)-self.amenityViewWidth, height: Int(self.view.frame.size.height)-self.amenityViewHeight)
        
                amenityView.backgroundColor = UIColor.clear
               blurEffectView.contentView.addSubview(amenityView)
                
                
                // Create an instance of UICollectionViewFlowLayout since you cant
                // Initialize UICollectionView without a layout
                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                layout.itemSize = CGSize(width: ((self.amenityView.frame.width-18)/2), height: 60)
                
                collectionview = UICollectionView(frame:CGRect.init(x: 4, y: 4, width: amenityView.frame.size.width-8, height: amenityView.frame.size.height-8), collectionViewLayout: layout)
                print("frame : %@",collectionview.frame)
                collectionview.dataSource = self
                collectionview.delegate = self
                collectionview.bounces = false
                collectionview.register(AmenitiesCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
                collectionview.showsVerticalScrollIndicator = false
                collectionview.backgroundColor = UIColor.clear
                amenityView.addSubview(collectionview)
                
                paybackSlider.frame = CGRect.init(x: 80, y: self.view.frame.height-70, width: self.view.frame.width-160, height: 50)
                paybackSlider.minimumValue = 1
                paybackSlider.maximumValue = 20
                paybackSlider.isContinuous = true
                paybackSlider.tintColor = UIColor.red
                let userDefaults = UserDefaults.standard
                let distanceValue = userDefaults.integer(forKey: "distanceValue")
                if distanceValue == 0
                {
                    paybackSlider.value = 1
                    
                }
                else
                {
                    paybackSlider.value = Float(distanceValue)
                }
                paybackSlider.isUserInteractionEnabled = true
                paybackSlider.addTarget(self, action: #selector(sliderValueDidChange), for: .valueChanged)
                paybackLabel.text = "\(paybackSlider.value)"
                blurEffectView.contentView.addSubview(paybackSlider)
                
                paybackLabel.frame = CGRect.init(x: paybackSlider.frame.size.width+paybackSlider.frame.origin.x+4, y: self.view.frame.height-70, width: 70, height: 50)
                sliderValue = Int(paybackSlider.value)
                if sliderValue == 1 {
                    paybackLabel.text = "\(sliderValue) mile"
                }
                else
                {
                    paybackLabel.text = "\(sliderValue) miles"
                }
                
                paybackLabel.textAlignment = NSTextAlignment.center
                paybackLabel.textColor = UIColor.black
                
                paybackLabel.font = UIFont.setTarcRegular(size: 17.0)
                blurEffectView.contentView.addSubview(paybackLabel)
                
                let radiusLabel = UILabel()
                radiusLabel.frame = CGRect.init(x: 10, y: self.view.frame.height-70, width: 70, height: 50)
                radiusLabel.text = "Radius"
                radiusLabel.textAlignment = NSTextAlignment.center
                radiusLabel.textColor = UIColor.black
                radiusLabel.font = UIFont.setTarcRegular(size: 17.0)
                blurEffectView.contentView.addSubview(radiusLabel)
                
                var drop: UIDropDown!
                if UIDevice.current.userInterfaceIdiom == .pad
                {
                    drop = UIDropDown(frame: CGRect(x: (blurEffectView.frame.size.width/2)-100, y:blurEffectView.frame.height-200 , width: 200, height: 30))
                }
                else
                {
                    drop = UIDropDown(frame: CGRect(x: (blurEffectView.frame.size.width/2)-100, y:blurEffectView.frame.height-160 , width: 200, height: 30))
                }
                drop.placeholder = amenityTypeString
                drop.animationType = .Bouncing
                drop.hideOptionsWhenSelect = true
                drop.options = ["Near my source", "Near my destination", "Along my bus"]
                drop.didSelect { (option, index) in
                    print("You just select: \(option) at index: \(index)")
                    self.amenityTypeString = option
                }
                blurEffectView.contentView.addSubview(drop)
                blurEffectView.bringSubview(toFront: drop)
                
                
                
                
                
                
            }
        func getPlaces(type: String?) {
          var  withLivebus = false
            var locationString = ""
                     markerClearButton.isHidden = false
            if DisplayType == "Transit" {
            let resultRes = dataPrevRespone.result.value?.routes![indexValue].legs![0];
                
                if amenityTypeString == "Near my source"
                         {
                            withLivebus = true
                             locationString = "\((resultRes?.startLocation?.lat)!),\((resultRes?.startLocation?.lng)!)"
                         }
                         else if amenityTypeString == "Near my destination"
                         {
                            withLivebus = true
                             locationString = "\((resultRes?.endLocation?.lat)!),\((resultRes?.endLocation?.lng)!)"
                         }
                         else if amenityTypeString == "Along my bus"
                         {
                            withLivebus = true
                             locationString = "\(busLat),\(busLng)"
                             print(locationString)
                         }
                         
                
            }
            else if DisplayType == "Bike" {
                
                 let WalkingDirection = dataWalkingDirection.result.value?.route?.first?.legs![0]
                 let BirdDirection = databikeResponseFromDirection.result.value?.routes?.first?.legs![0]
                
                if amenityTypeString == "Near my source"
                         {
                            withLivebus = true
                            locationString = "\(WalkingDirection!.start_location!.lat!), \(WalkingDirection!.start_location!.lng!)"
                         }
                         else if amenityTypeString == "Near my destination"
                         {
                            withLivebus = true
                             locationString = "\((BirdDirection?.endLocation?.lat)!),\((BirdDirection?.endLocation?.lng)!)"
                         }
                         else if amenityTypeString == "Along my bus"
                         {
                             withLivebus = false
                            
                         }
                         
                
            }
            else if DisplayType == "LouVelo" {
                
                let StartWalkingDirection = DataWalkingResponseLouveloStart.result.value?.route?.first?.legs![0]
                           let BikeDirection = louveloBikeResponse.result.value?.routes?.first?.legs![0]
                           let EndWalkingDirection = DataWalkingResponseLouveloEnd.result.value?.route?.first?.legs![0]
                if amenityTypeString == "Near my source"
                         {
                            withLivebus = true
                             locationString = "\(StartWalkingDirection!.start_location!.lat!),\(StartWalkingDirection!.start_location!.lng!)"
                         }
                         else if amenityTypeString == "Near my destination"
                         {
                            withLivebus = true
                             locationString = "\(EndWalkingDirection!.end_location!.lat!),\(EndWalkingDirection!.end_location!.lng!)"
                         }
                         else if amenityTypeString == "Along my bus"
                         {
                            
                            withLivebus = false
                         }
                         
                
            }
            
         
            if withLivebus {
         
            let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
            let parameters: Parameters = [
                "location": locationString,
                "radius": "\(sliderValue*1609)",
                "type":type ?? "park",
                "key": Key.GoogleAPIKey
            ]
            
            
            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<RootClass>) in
                    switch response.result {
                    case .success:
                        print(response)
                        //to get status code
                        if response.result.value?.status == "OK"
                        {
                            
                            let weatherResponse = response.result.value
                            print(weatherResponse!.status ?? "user nil" )
                            self.markersArray.removeAll()
                            
                            DispatchQueue.main.async {
                                // self.mapBackgroundView.clear()
                                for markerDet in self.markerDict
                                {
                                    //APHIDE
                                  //  let removeMarker : GMSMarker = markerDet as! GMSMarker
                                  //  removeMarker.map = nil;
                                    self.mapView.removeAnnotation(self.placesAnnotation)
                                }
                                
                            }
                            
                            // self.mapBackgroundView.clear()
                            if let threeDayForecast = weatherResponse?.results {
                                for forecast in threeDayForecast {
                                    print(forecast.name ?? "user nil")
                                    print(forecast.vicinity ?? "user nil")
                                    if let geometryLoc = forecast.geometry
                                    {
                                        if let latlong = geometryLoc.location
                                        {
                                            
                                            
                                            self.markersArray.append(State(name: forecast.name!, long: latlong.lat!, lat: latlong.lng!, placeId: forecast.placeId!, formattedAddress: forecast.vicinity!, type: "", imageUrl: ""))
                                        }
                                    }
                                    
                                    
                                }
                                DispatchQueue.main.async {
                            //  self.mapView.removeAnnotation(self.placesAnnotation)
                                  
                                    //markersArray

                                    
                                    self.removeSpecificAnnotation()

                                   // self.mapView.removeAnnotation()
                                    for state in self.markersArray {
                                        let latMaker = state.lat
                                        let lngMaker = state.long
                                        //APHIDE
                                      //  let state_marker = GMSMarker()
                                        
    //                                    state_marker.position = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
    //                                    state_marker.title = state.name
    //                                    state_marker.icon = UIImage(named: type!)
    //                                    state_marker.map = self.mapBackgroundView
    //                                    self.markerDict.add(state_marker)
                                        print("\(state.name)\n\(state.formattedAddress)")
                                        self.categoryNameString = type! as NSString
                                        self.placesAnnotation = MKPointAnnotation()
                                        self.placesAnnotation.title = self.currentTitle as String
                                        self.placesAnnotation.subtitle = "\(state.name)\n\(state.formattedAddress)"

                                        self.placesAnnotation.coordinate = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
                                        self.mapView.addAnnotation(self.placesAnnotation)
                                        
                                        self.previosmarkersArray = self.markersArray
                                    }
                                    
                                    self.blurEffectView.isHidden = true;
                                }
                                
                            }
                        }
                        else
                        {
                            if self.busLat == 0.0 && self.busLng == 0.0 &&  self.amenityTypeString == "Along my bus" {
                                
                                let alertViewController = NYAlertViewController()
                                // Set a title and message
                                alertViewController.title = "Places"
                                alertViewController.message = "Real-time bus not available!"
                                
                                // Customize appearance as desired
                                alertViewController.buttonCornerRadius = 20.0
                                alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.swipeDismissalGestureEnabled = true
                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                alertViewController.buttonColor = UIColor.red
                                // Add alert actions
                                
                                
                                let cancelAction = NYAlertAction(
                                    title: "OK",
                                    style: .cancel,
                                    handler: { (action: NYAlertAction!) -> Void in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                }
                                )
                                
                                alertViewController.addAction(cancelAction)
                                
                                // Present the alert view controller
                                self.present(alertViewController, animated: true, completion: nil)
                                
                            }
                            else{
                            
                            let alertViewController = NYAlertViewController()
                            // Set a title and message
                            alertViewController.title = "Places"
                            alertViewController.message = "No results found for given radius. Please increase the radius."
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                            
                            }
                        }
                    case .failure(let error):
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Places"
                        alertViewController.message = "No results found for given radius. Please increase the radius."
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        print(error)
                        
                    }
            }
            }
            else{
                self.view.makeToast("Along my bus option not available for Bike / Scooter")
            }
        }
         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                return 10
            }
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                let cell = collectionview.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath as IndexPath) as! AmenitiesCollectionViewCell
                
                let imageView = UIImageView()
                let cellName : String?
                let imageName: String?
                if indexPath.row == 0 {
                    imageName = "restaurant-ame"
                    cellName = "FOOD & DRINK"
                }
                else if indexPath.row == 1{
                    
                    imageName = "store"
                    cellName = "SHOPPING"
                }
                else if indexPath.row == 2{
                    imageView.backgroundColor = UIColor(red: 64.0/255.0, green: 224.0/255.0, blue: 208.0/255.0, alpha: 1.0)
                    imageName = "transport"
                    cellName = "TRAVEL"
                }
                else if indexPath.row == 3{
                    
                    imageName = "public_utility"
                    cellName = "SERVICES"
                }
                else if indexPath.row == 4{
                    
                    imageName = "fun_activities"
                    cellName = "ACTIVITIES"
                }
                else if indexPath.row == 5{
                    
                    imageName = "Hospital_Places"
                    cellName = "HEALTH"
                }
                else if indexPath.row == 6{
                    
                    imageName = "school_Icon"
                    cellName = "EDUCATION"
                }
                else if indexPath.row == 7{
                    
                    imageName = "public_Places"
                    cellName = "GOVERNMENT"
                }
                    //        else if indexPath.row == 8{
                    //            imageName = "parking.png"
                    //            cellName = "PARKING"
                    //        }
                            else if indexPath.row == 8
                            {
                                imageName = "louveloIconBox"
                                cellName = "LOUVELO"
                            }
                            else if indexPath.row == 9
                            {
                                imageName = "tourism_Icon"
                                cellName = "EVENTS"
                            }
                    //        else if indexPath.row == 11
                    //        {
                    //            imageName = "places_Icon"
                    //            cellName = "PLACES"
                    //        }
                else
                {
                    imageName = "social_service"
                    cellName = "SOCIAL SERVICES"
                }
                let image = UIImage(named: imageName!)
                
                cell.amenitiesImage.frame = CGRect(x: amenityImageViewX, y: amenityImageViewY, width: amenityImageViewWidth, height: amenityImageViewHeight)
                cell.amenitiesImage.image = image;
                cell.contentView.addSubview(cell.amenitiesImage)
                
                
                cell.amenitiesText.frame = CGRect.init(x: amenityCellLabelX, y: amenityCellLabelY, width: Int(cell.frame.size.width) - amenityCellLabelWidth, height: amenityCellLabelHeight)
                cell.amenitiesText.text = cellName
                cell.amenitiesText.textColor = UIColor.black
                cell.amenitiesText.textAlignment = NSTextAlignment.left
                cell.amenitiesText.font = UIFont.setTarcRegular(size: 12.0)
                cell.contentView.addSubview(cell.amenitiesText)
                return cell
            }
            
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
            {
                print("cell size : %f",((self.amenityView.frame.width-16)/2))
                return CGSize(width: ((self.amenityView.frame.width-18)/2), height: 60)
            }
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                
                if indexPath.row == 0
                {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "Bakery", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "bakery"
                        self.currentTitle = "Bakery"
                        self.getPlaces(type: "bakery")
                    }))
                    actionController.addAction(Action(ActionData(title: "Bar", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "bar"
                        self.getPlaces(type: "bar")
                        self.currentTitle = "Bar"

                    }))
                    actionController.addAction(Action(ActionData(title: "Grocery", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "grocery_or_supermarket"
                        self.getPlaces(type: "grocery_or_supermarket")
                        self.currentTitle = "Grocery"

                    }))
                    actionController.addAction(Action(ActionData(title: "Restuarant", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "restaurant"
                        self.getPlaces(type: "restaurant")
                        self.currentTitle = "Restuarant"

                    }))
                    actionController.addAction(Action(ActionData(title: "Liquor Store", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "liquor_store"
                        self.getPlaces(type: "liquor_store")
                        self.currentTitle = "Liquor Store"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cafe", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "cafe"
                        self.getPlaces(type: "cafe")
                        self.currentTitle = "Cafe"

                    }))
                    actionController.addAction(Action(ActionData(title: "Free Breakfast", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = false
                        self.typeString = "Free_Breakfast"
                        self.getPlaces(type: "Free_Breakfast")
                        self.currentTitle = "Free Breakfast"

                    }))
                    actionController.addAction(Action(ActionData(title: "Chili", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = false
                        self.typeString = "Chili"
                        self.getPlaces(type: "Chili")
                        self.currentTitle = "Chili"

                    }))
                    actionController.addAction(Action(ActionData(title: "Breweries", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = false
                        self.typeString = "Breweries"
                        self.getPlaces(type: "Breweries")
                        self.currentTitle = "Breweries"

                    }))
                    actionController.addAction(Action(ActionData(title: "Wineries & Vineyards", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = false
                        self.typeString = "Wineries_And_Vineyards"
                        self.getPlaces(type: "Wineries_And_Vineyards")
                        self.currentTitle = "Wineries & Vineyards"

                    }))
                    
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    present(actionController, animated: true, completion: nil)
                }
                else if indexPath.row == 1 {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "Book Store", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "book_store"
                        self.getPlaces(type: "book_store")
                        self.currentTitle = "Book Store"

                    }))
                    actionController.addAction(Action(ActionData(title: "Convenience store", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "convenience_store"
                        self.getPlaces(type: "convenience_store")
                        self.currentTitle = "Convenience store"

                    }))
                    actionController.addAction(Action(ActionData(title: "Car Dealer", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "car_dealer"
                        self.getPlaces(type: "car_dealer")
                        self.currentTitle = "Car Dealer"

                    }))
                    actionController.addAction(Action(ActionData(title: "Clothing", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "clothing_store"
                        self.getPlaces(type: "clothing_store")
                        self.currentTitle = "Clothing"

                    }))
                    actionController.addAction(Action(ActionData(title: "Hardware", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "hardware_store"
                        self.getPlaces(type: "hardware_store")
                        self.currentTitle = "Hardware"

                    }))
                    actionController.addAction(Action(ActionData(title: "Home Goods", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "home_goods_store"
                        self.getPlaces(type: "home_goods_store")
                        self.currentTitle = "Home Goods"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    
                    present(actionController, animated: true, completion: nil)
                }
                else if indexPath.row == 2 {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "Bus Station", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "bus_station"
                        self.getPlaces(type: "bus_station")
                        self.currentTitle = "Bus Station"

                    }))
                    actionController.addAction(Action(ActionData(title: "Lodging", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "lodging"
                        self.getPlaces(type: "lodging")
                        self.currentTitle = "Lodging"

                    }))
                    actionController.addAction(Action(ActionData(title: "Car Rental", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "car_rental"
                        self.getPlaces(type: "car_rental")
                        self.currentTitle = "Car Rental"

                    }))
                    actionController.addAction(Action(ActionData(title: "Airport", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "airport"
                        self.getPlaces(type: "airport")
                        self.currentTitle = "Airport"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    present(actionController, animated: true, completion: nil)
                }
                else if indexPath.row == 3 {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "ATM", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "atm"
                        self.getPlaces(type: "atm")
                        self.currentTitle = "ATM"

                    }))
                    actionController.addAction(Action(ActionData(title: "Bank", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "bank"
                        self.getPlaces(type: "bank")
                        self.currentTitle = "Bank"

                    }))
                    actionController.addAction(Action(ActionData(title: "Florist", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "florist"
                        self.getPlaces(type: "florist")
                        self.currentTitle = "Florist"

                    }))
                    actionController.addAction(Action(ActionData(title: "Laundry", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "laundry"
                        self.getPlaces(type: "laundry")
                        self.currentTitle = "Laundry"

                    }))
                    actionController.addAction(Action(ActionData(title: "Lawyer", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "lawyer"
                        self.getPlaces(type: "lawyer")
                        self.currentTitle = "Lawyer"

                    }))
                    actionController.addAction(Action(ActionData(title: "Post office", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "post_office"
                        self.getPlaces(type: "post_office")
                        self.currentTitle = "Post office"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    present(actionController, animated: true, completion: nil)
                }
                    
                else if indexPath.row == 4 {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "Bowling alley", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "bowling_alley"
                        self.getPlaces(type: "bowling_alley")
                        self.currentTitle = "Bowling alley"

                    }))
                    actionController.addAction(Action(ActionData(title: "Casino", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "casino"
                        self.getPlaces(type: "casino")
                        self.currentTitle = "Casino"

                    }))
                    actionController.addAction(Action(ActionData(title: "Movie Theatre", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "movie_theater"
                        self.getPlaces(type: "movie_theater")
                        self.currentTitle = "Movie Theatre"

                    }))
                    actionController.addAction(Action(ActionData(title: "Museum", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "museum"
                        self.getPlaces(type: "museum")
                        self.currentTitle = "Museum"

                    }))
                    actionController.addAction(Action(ActionData(title: "Night Club", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "night_club"
                        self.getPlaces(type: "night_club")
                        self.currentTitle = "Night Club"

                    }))
                    actionController.addAction(Action(ActionData(title: "Park", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "park"
                        self.getPlaces(type: "park")
                        self.currentTitle = "Park"

                    }))
                    actionController.addAction(Action(ActionData(title: "Gaming", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = false
                        self.typeString = "Gaming"
                        self.getPlaces(type: "Gaming")
                        self.currentTitle = "Gaming"

                    }))
                    actionController.addAction(Action(ActionData(title: "Sports", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = false
                        self.typeString = "Sports"
                        self.getPlaces(type: "Sports")
                        self.currentTitle = "Sports"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    present(actionController, animated: true, completion: nil)
                }
                else if indexPath.row == 5 {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "Hospital", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "hospital"
                        self.getPlaces(type: "hospital")
                        self.currentTitle = "Hospital"

                    }))
                    actionController.addAction(Action(ActionData(title: "Pharmacy", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "pharmacy"
                        self.getPlaces(type: "pharmacy")
                        self.currentTitle = "Pharmacy"

                    }))
                    
                    actionController.addAction(Action(ActionData(title: "Veterinary Care", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "veterinary_care"
                        self.getPlaces(type: "veterinary_care")
                        self.currentTitle = "Veterinary Care"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    present(actionController, animated: true, completion: nil)
                }
                else if indexPath.row == 6 {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "School", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "school"
                        self.getPlaces(type: "school")
                        self.currentTitle = "School"

                    }))
                    actionController.addAction(Action(ActionData(title: "University", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "university"
                        self.getPlaces(type: "university")
                        self.currentTitle = "University"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    present(actionController, animated: true, completion: nil)
                }
                else if indexPath.row == 7 {
                    let actionController = YoutubeActionController()
                    actionController.addAction(Action(ActionData(title: "City Hall", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "city_hall"
                        self.getPlaces(type: "city_hall")
                        self.currentTitle = "City Hall"

                    }))
                    actionController.addAction(Action(ActionData(title: "Court House", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "courthouse"
                        self.getPlaces(type: "courthouse")
                        self.currentTitle = "Court House"

                    }))
                    actionController.addAction(Action(ActionData(title: "Fire Station", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "fire_station"
                        self.getPlaces(type: "fire_station")
                        self.currentTitle = "Fire Station"

                    }))
                    
                    actionController.addAction(Action(ActionData(title: "Police", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
                        self.isPlaceSelected = true
                        self.typeString = "police"
                        self.getPlaces(type: "police")
                        self.currentTitle = "Police"

                    }))
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    
                    present(actionController, animated: true, completion: nil)
                }
                    //        else if(indexPath.row == 8)
                    //        {
                    //            loadKml("Parking")
                    //        }
                            else if indexPath.row == 8
                            {
                    
                                let url = "https://lou.publicbikesystem.net/ube/gbfs/v1/en/station_status"
                                let parameters: Parameters = [:]
                    
                                DispatchQueue.main.async {
                                    // self.mapBackgroundView.clear()
                                    for markerDet in self.markerDict
                                    {
                                        // APHIDE
                                      //  let removeMarker : GMSMarker = markerDet as! GMSMarker
                                      //  removeMarker.map = nil;
                                        self.mapView.removeAnnotation(self.placesAnnotation)
                                    }
                    
                                }
                    
                                Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                                    .responseObject{ (response: DataResponse<RedBikeStatusClass>) in
                                        switch response.result {
                                        case .success:
                                        print(response.result.value ?? "nil")
                                        self.redBikeStatus = response
                                        let url1 = "https://lou.publicbikesystem.net/ube/gbfs/v1/en/station_information"
                                        let parameters1: Parameters = [:]
                    
                    
                                        Alamofire.request(url1, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                                            .responseObject{ (response: DataResponse<RedBikeClass>) in
                                                switch response.result {
                                                case .success:
                                                print(response.result.value ?? "nil")
                                                self.redBikeInformation = response
                                                self.markersArray.removeAll()
                                                var redCount = 0
                                                self.removeSpecificAnnotation()

                                                for plotRedBike in (self.redBikeInformation.result.value?.data?.stations)!
                                                {
                                                    self.markersArray.append(State(name: plotRedBike.name!, long: CLLocationDegrees(plotRedBike.lon!), lat: CLLocationDegrees(plotRedBike.lat!), placeId: String(plotRedBike.stationId!), formattedAddress: plotRedBike.address!, type: "", imageUrl: ""))
                                                    //APHIDE
                                                  //  let state_marker = GMSMarker()
                                                    let latMaker = Double(plotRedBike.lat!)
                                                    let lngMaker = Double(plotRedBike.lon!)
                                                    print(latMaker,lngMaker)
//
                                                      redCount += 1
//                                                        //APHIDE
////
//                                                        self.categoryNameString = "LouveloMapIcon"
//                                                        self.placesAnnotation = MKPointAnnotation()
//                                                        if self.categoryNameString == nil || self.categoryNameString == ""
//                                                        {
//                                                           self.categoryNameString = "Louvelo"
//                                                        }
//                                                        self.placesAnnotation.title = self.categoryNameString as String
//                                            self.placesAnnotation.subtitle = "\(plotRedBike.name!)\n\(plotRedBike.address)"
//                                                        self.placesAnnotation.coordinate = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
//                                                        self.mapView.addAnnotation(self.placesAnnotation)
//
                                                        
//
                                                    self.LouveloMarker = MKPointAnnotation()
                                                    self.LouveloMarker.title = plotRedBike.address!
                                                    self.LouveloMarker.coordinate = CLLocationCoordinate2D(latitude: latMaker, longitude: lngMaker)
                                                    self.mapView.addAnnotation(self.LouveloMarker)
                    
                                                }
                                                if redCount == 0
                                                {
                                                    let alertViewController = NYAlertViewController()
                                                    // Set a title and message
                                                    alertViewController.title = "Places"
                                                    alertViewController.message = "No results found for given radius. Please increase the radius."
                    
                                                    // Customize appearance as desired
                                                    alertViewController.buttonCornerRadius = 20.0
                                                    alertViewController.view.tintColor = self.view.tintColor
                                                    alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                    alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                    alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                    alertViewController.swipeDismissalGestureEnabled = true
                                                    alertViewController.backgroundTapDismissalGestureEnabled = true
                                                    alertViewController.buttonColor = UIColor.red
                                                    // Add alert actions
                    
                    
                                                    let cancelAction = NYAlertAction(
                                                        title: "OK",
                                                        style: .cancel,
                                                        handler: { (action: NYAlertAction!) -> Void in
                    
                                                            self.dismiss(animated: true, completion: nil)
                                                    }
                                                    )
                    
                                                    alertViewController.addAction(cancelAction)
                    
                                                    // Present the alert view controller
                                                    self.present(alertViewController, animated: true, completion: nil)
                    
                                                }
                                                self.blurEffectView.isHidden = true;
                                                case .failure(let error):
                                                    self.showErrorDialogBox(viewController: self)
                                                    print(error)
                                                }
                                        }
                                        case .failure(let error):
                                        print(error)
                                            self.showErrorDialogBox(viewController: self)
                                }
                                }
                            }
                else if indexPath.row == 9{
                    let actionController = YoutubeActionController()
                    
                    if self.eventArrayList.count > 0 {
                        for Eventname in self.eventArrayList {
                            
                            actionController.addAction(Action(ActionData(title: Eventname, image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                                self.isPlaceSelected = false
                                self.typeString = Eventname
                                self.plotTARCTourism(category:Eventname)
                            }))
                        }
                    }
                    actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))
                    present(actionController, animated: true, completion: nil)
                }
                else
                {
                    //            self.isPlaceSelected = false
                    //            self.typeString = "Events"
                    //            self.getCinciPlaces(type: "Events")
        //            let actionController = YoutubeActionController()
        //            actionController.addAction(Action(ActionData(title: "Aging", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Aging"
        //                self.plotSocialServices(type: "Aging")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Aging & Disability", image: UIImage(named: "yt-add-to-playlist-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Aging&Disability"
        //                self.plotSocialServices(type: "Aging&Disability")
        //
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Crisis", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Crisis"
        //                self.plotSocialServices(type: "Crisis")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Education", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Education"
        //                self.plotSocialServices(type: "Education")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Employment", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Employment"
        //                self.plotSocialServices(type: "Employment")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Financial", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Financial"
        //                self.plotSocialServices(type: "Financial")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Health", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Health"
        //                self.plotSocialServices(type: "Health")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Housing", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Housing"
        //                self.plotSocialServices(type: "Housing")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Legal", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Legal"
        //                self.plotSocialServices(type: "Legal")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Other", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Other"
        //                self.plotSocialServices(type: "Other")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Transportation", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Transportation"
        //                self.plotSocialServices(type: "Transportation")
        //            }))
        //            actionController.addAction(Action(ActionData(title: "Veteran", image: UIImage(named: "yt-share-icon")!), style: .default, handler: { action in
        //                self.isPlaceSelected = false
        //                self.typeString = "Veteran"
        //                self.plotSocialServices(type: "Veteran")
        //            }))
        //
        //            present(actionController, animated: true, completion: nil)
        //
                }
                
            }
      @IBAction func AddFavTrip(_ sender: UIButton) {
          if self.FavButton.currentImage == UIImage(named: "FavActive") {
              let alertViewController = NYAlertViewController()
              // Set a title and message
              alertViewController.title = "Saved Trip"
              alertViewController.message = "Already saved to Favorites."
              
              // Customize appearance as desired
              alertViewController.buttonCornerRadius = 20.0
              alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
              alertViewController.swipeDismissalGestureEnabled = true
              alertViewController.backgroundTapDismissalGestureEnabled = true
              alertViewController.buttonColor = UIColor.red
              // Add alert actions
              
              
              let cancelAction = NYAlertAction(
                  title: "OK",
                  style: .cancel,
                  handler: { (action: NYAlertAction!) -> Void in
                      
                      self.dismiss(animated: true, completion: nil)
              }
              )
              
              alertViewController.addAction(cancelAction)
              
              // Present the alert view controller
              self.present(alertViewController, animated: true, completion: nil)
          }
          else {
              //switch to image-one
              
              self.showLoader()
              let userDefaults1 = UserDefaults.standard
              let accessToken = userDefaults1.value(forKey: "accessToken")
              let url1 = "\(APIUrl.ZIGTARCAPI)SaveTrips/post"
              let parameters1: Parameters = [
                  "Token": accessToken!,
                  "SourceAddress" : SourceAddressString! as NSString,
                  "sourceLat" : sourceAddressLat!,
                  "sourceLong" : sourceAddressLong!,
                  "DestinationAddress" : DestinationAddressString! as NSString,
                  "DestinationLat" : DestinationLat!,
                  "DestinationLong" : DestinationLong!
                  
              ]
              Alamofire.request(url1, method: .post, parameters: parameters1, encoding: URLEncoding.default)
                  .responseObject{ (response: DataResponse<Postpayment>) in
                      switch response.result {
                      case .success:
                          //  print(response.result.value?.message ?? "nil")
                          let image = UIImage(named: "FavActive")
                          sender.setImage(image, for: .normal)
                          self.hideLoader()
                          self.CheckFavTrip()
                          
                      case .failure(let error):
                          self.showErrorDialogBox(viewController: self)
                          print(error.localizedDescription)
                          let image = UIImage(named: "FavDeactive")
                          sender.setImage(image, for: .normal)
                          self.hideLoader()
                      }
                      
              }
              
              
              
              //        let image = UIImage(named: "FavActive")
              //sender.setImage(image, for: .normal)
          }
      }
      func showErrorDialogBox(viewController : UIViewController){
          let alertViewController = NYAlertViewController()
          alertViewController.title = "Error"
          alertViewController.message = "Server Error! Please try again later!"
          
          // Customize appearance as desired
          alertViewController.buttonCornerRadius = 20.0
          alertViewController.view.tintColor = viewController.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
          alertViewController.swipeDismissalGestureEnabled = true
          alertViewController.backgroundTapDismissalGestureEnabled = true
          alertViewController.buttonColor = UIColor.red
          // Add alert actions
          
          
          let cancelAction = NYAlertAction(
              title: "OK",
              style: .cancel,
              handler: { (action: NYAlertAction!) -> Void in
                  
                  viewController.dismiss(animated: true, completion: nil)
          }
          )
          
          alertViewController.addAction(cancelAction)
          
          // Present the alert view controller
          viewController.present(alertViewController, animated: true, completion: nil)
          
      }
      func showLoader()
      {
          
          DispatchQueue.main.async {
              self.loaderView = UIView()
              self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
              self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
              let window = UIApplication.shared.keyWindow!
              
              let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
              activityLoader.startAnimating()
              window.addSubview(self.loaderView)
              self.loaderView.addSubview(activityLoader)
          }
          
      }
      func hideLoader()
      {
          DispatchQueue.main.async {
              self.loaderView.removeFromSuperview()
          }
      }
    
    func AddMarker(){
        print(SourceDestination.DisplayType!)
        if SourceDestination.DisplayType == "Transit" {
         let valueResult = dataPrevRespone.result.value
         let indexvalueRow = indexValue
        let routesPlan = valueResult?.routes![indexvalueRow].legs
        let StepArray = routesPlan![0].steps!
        
        let StartAddressLat = routesPlan![0].startLocation?.lat
        let StartAddressLng = routesPlan![0].startLocation?.lng
        let StartAddress = routesPlan![0].startAddress
        
        let EndAddressLat = routesPlan![0].endLocation?.lat
        let EndAddressLng = routesPlan![0].endLocation?.lng
        let EndAddress = routesPlan![0].endAddress
               
        
        StartMarker = MKPointAnnotation()
        StartMarker.title = StartAddress
        StartMarker.coordinate = CLLocationCoordinate2D(latitude: StartAddressLat!, longitude: StartAddressLng!)
        self.mapView.addAnnotation(StartMarker)
        
        EndMarker = MKPointAnnotation()
        EndMarker.title = EndAddress
        EndMarker.coordinate = CLLocationCoordinate2D(latitude: EndAddressLat!, longitude: EndAddressLng!)
        self.mapView.addAnnotation(EndMarker)

        for Steps in StepArray {
        
            if Steps.travelMode == "TRANSIT" {
                let TransitSourceLat = Steps.startLocation?.lat
                let TransitSourceLng = Steps.startLocation?.lng
                let TransitDestinationLat = Steps.endLocation?.lat
                let TransitDestinationLng = Steps.endLocation?.lng
                let Title = "#\((Steps.transitDetails?.line?.shortName) ?? "") - \((Steps.transitDetails?.line?.name)!)"
                let Polyline = (Steps.polyline?.points)!
                let colorTransit = Steps.transitDetails?.line?.color ?? "#000000"
                drawPolylineTransit(polylinePoint: Polyline, TransitColor: colorTransit)
                StartTransitMarker = MKPointAnnotation()
                StartTransitMarker.title = Title
                StartTransitMarker.subtitle = "Arrival Time:  \((Steps.transitDetails?.arrivalTime?.text)!)"
                StartTransitMarker.coordinate = CLLocationCoordinate2D(latitude: TransitSourceLat!, longitude: TransitSourceLng!)
                self.mapView.addAnnotation(StartTransitMarker)
 
                EndTransitMarker = MKPointAnnotation()
                EndTransitMarker.title = Title
                EndTransitMarker.subtitle = "Arrival Time:  \((Steps.transitDetails?.departureTime!.text)!)"
                EndTransitMarker.coordinate = CLLocationCoordinate2D(latitude: TransitDestinationLat!, longitude: TransitDestinationLng!)
                self.mapView.addAnnotation(EndTransitMarker)
                
                 
                
                 
                     let url1 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetTripSchedulemobileapi"
                     
                     let parameters1: Parameters = [
                         "Headsign": Steps.transitDetails?.headsign ?? "nil",
                         "Routeid": Steps.transitDetails?.line?.shortName ?? "nil",
                         "StartStop": Steps.transitDetails?.departureStop?.name ?? "nil",
                         "EndStop": Steps.transitDetails?.arrivalStop?.name ?? "nil",
                         "DepartureTime": Steps.transitDetails?.departureTime?.text ?? "nil",
                         "StartStopLat": Steps.transitDetails?.departureStop?.location?.lat ?? "nil",
                         "StartStopLong": Steps.transitDetails?.departureStop?.location?.lng ?? "nil",
                         "EndStopLat": Steps.transitDetails?.arrivalStop?.location?.lat ?? "nil",
                         "EndStopLong": Steps.transitDetails?.arrivalStop?.location?.lng ?? "nil",
                         //"Day": "Friday"
                     ]
                     
                     
                     Alamofire.request(url1, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                         .responseObject{ (response: DataResponse<TransitRouteClass>) in
                             switch response.result {
                             case .success:
                                // print(response.result.value?.triplist as Any)
                                 if response.result.value?.triplist != nil{
                                     if !(response.result.value?.triplist?.isEmpty)!
                                     {
                                         self.realtimeDataResponse = response
                                    
                                         self.tanktransitOptions.add(response)

                                         let url2 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealTimeVehicleandNextStops"
                                         let parameters2: Parameters = [
                                             "TripID" : self.realtimeDataResponse.result.value?.triplist![0].tripID ?? "nil",
                                             "ArrivalTime" : self.realtimeDataResponse.result.value?.triplist![0].arrivalTime ?? "nil",
                                             "DepartureTime" : self.realtimeDataResponse.result.value?.triplist![0].departureTime ?? "nil",
                                             "DepartureStopName" : self.realtimeDataResponse.result.value?.triplist![0].departureStop ?? "nil",
                                             "ArrivalStopName" : self.realtimeDataResponse.result.value?.triplist![0].arrivalStop ?? "nil",
                                             
                                             ]
                                         
                                         
                                         
                                         Alamofire.request(url2, method: .get, parameters: parameters2, encoding: URLEncoding.default)
                                             .responseObject{ (response: DataResponse<RealTimeRootClass>) in
                                                 switch response.result {
                                                 case .success:
                                                    // print("Lat \(String(describing: response.result.value?.lat) )" as Any)
                                                     if response.result.value != nil {
                                                                                if response.result.value!.eTAminutes != nil {
                                                                                if response.result.value!.vehicle! {
                                                                            print("TARC__\(response.result.value?.lat as Any)")
                                                         if self.realTimeTimer == nil
                                                         {
                                                             self.realTimeTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(self.TankRunTimedCode), userInfo: nil, repeats: true)
                                                         }
                                                         
                                                         
                                                         if response.result.value?.lat != 0 && response.result.value?.lng != 0 && response.result.value?.lat != nil
                                                         {
                                                            

                                                            self.busLat = response.result.value!.lat!
                                                            self.busLng = response.result.value!.lng!
                                                            
                                                            if (response.result.value?.busStops?.nextStops!.count)! > 0{
                                                            
                                                            let NextBusStop = response.result.value?.busStops?.nextStops![0].stopName ?? "-"
                                                            let NextBusStopArrival = response.result.value?.busStops?.nextStops![0].arrivalTime ?? "-"
                                                            let NextBusStopStopDistance = response.result.value?.busStops?.nextStops![0].stopDistance ?? "-"
                                                            let NextETA = response.result.value?.busStops?.nextStops![0].eTA ?? "-"
                                                            
                                                            self.realtimeIcon = MKPointAnnotation()
                                                                if response.result.value?.routeShortName != nil
                                                                {
                                                            self.realtimeIcon.title = "#\((response.result.value?.routeShortName)!)"
                                                                }
                                                                else
                                                                {
                                                                 self.realtimeIcon.title = "#"
                                                                }
                                                            self.realtimeIcon.subtitle = "Status:\((response.result.value?.tripStatus)!)\nETA: \((response.result.value?.eTAminutes)!)\n\nNext Stop:\((NextBusStop))\nNext Stop Arrival Time: \((NextBusStopArrival))\nNext Stop Distance: \((NextBusStopStopDistance))\nNext Stop ETA: \((NextETA))"
                                                            self.realtimeIcon.coordinate = CLLocationCoordinate2D(latitude: (response.result.value?.lat!)!, longitude: (response.result.value?.lng!)!)
                                                            self.mapView.addAnnotation(self.realtimeIcon)
                                                            
                                                            }
                                                                                    }
                                                                                    }
                                                                                    
                                                        }
                                                     }
                                                 case .failure(let error):
                                                    // self.showErrorDialogBox(viewController: self)
                                                    // self.view.makeToast("Server Error")
                                                     print(error)
                                                 }
                                                 
                                         }
                                         
                                     }
                                 }
                             case .failure(let error):
                               // self.view.makeToast("Server Error")
                                 //self.showErrorDialogBox(viewController: self)
                                 print(error)
                             }
                             
                     }
            
            }

        }
        
    }
        else if SourceDestination.DisplayType == "Bike"{
            
            let vechelType = isLimeredirect
            let walkingData = dataWalkingDirection.result.value?.route?.first?.legs![0]
            let BikeData = databikeResponseFromDirection.result.value?.routes?.first?.legs![0]
            
            let StartAddressLat = walkingData?.start_location?.lat
            let StartAddressLng = walkingData?.start_location?.lng
            let StartAddress = walkingData?.start_address
            
            let EndAddressLat = BikeData?.endLocation?.lat
            let EndAddressLng = BikeData?.endLocation?.lng
            let EndAddress = BikeData?.endAddress
            
            
            
                    StartMarker = MKPointAnnotation()
                   StartMarker.title = StartAddress
                   StartMarker.coordinate = CLLocationCoordinate2D(latitude: StartAddressLat!, longitude: StartAddressLng!)
                   self.mapView.addAnnotation(StartMarker)
                   
                   EndMarker = MKPointAnnotation()
                   EndMarker.title = EndAddress
                   EndMarker.coordinate = CLLocationCoordinate2D(latitude: EndAddressLat!, longitude: EndAddressLng!)
                   self.mapView.addAnnotation(EndMarker)
            
            
      //      if vechelType == "Bird" {
                //BirdMarker =
                let StartLat = BikeData?.startLocation?.lat
                let Startlong = BikeData?.startLocation?.lng
                 BirdMarker = MKPointAnnotation()
                BirdMarker.title = "Pickup your \(vechelType) vehicle from"
                BirdMarker.subtitle = BikeData?.startAddress
                BirdMarker.coordinate = CLLocationCoordinate2D(latitude: StartLat!, longitude: Startlong!)
                self.mapView.addAnnotation(BirdMarker)
                
                
         //   }
//           else if vechelType == "Lime" {
//                let StartLat = BikeData?.startLocation?.lat
//                let Startlong = BikeData?.startLocation?.lng
//                 LimeMarker = MKPointAnnotation()
//                LimeMarker.title = "Pickup your Lime vehicle from"
//                LimeMarker.subtitle = BikeData?.startAddress
//                LimeMarker.coordinate = CLLocationCoordinate2D(latitude: StartLat!, longitude: Startlong!)
//                self.mapView.addAnnotation(LimeMarker)
//
//            }
//            else if vechelType == "Bolt" {
//                 let StartLat = BikeData?.startLocation?.lat
//                 let Startlong = BikeData?.startLocation?.lng
//                  boltMarker = MKPointAnnotation()
//                 boltMarker.title = "Pickup your Bolt vehicle from"
//                 boltMarker.subtitle = BikeData?.startAddress
//                 boltMarker.coordinate = CLLocationCoordinate2D(latitude: StartLat!, longitude: Startlong!)
//                 self.mapView.addAnnotation(boltMarker)
//
//             }
             
            
            
        }
        else if SourceDestination.DisplayType == "LouVelo"{
            
        let vechelType = isLimeredirect
        let StartWalkingDirection = DataWalkingResponseLouveloStart.result.value?.route?.first?.legs![0]
        let BikeDirection = louveloBikeResponse.result.value?.routes?.first?.legs![0]
        let EndWalkingDirection = DataWalkingResponseLouveloEnd.result.value?.route?.first?.legs![0]
                                   
                              
                       let StartAddressLat = StartWalkingDirection?.start_location?.lat
                       let StartAddressLng = StartWalkingDirection?.start_location?.lng
                       let StartAddress = StartWalkingDirection?.start_address
                       
                       let EndAddressLat = EndWalkingDirection?.end_location?.lat
                       let EndAddressLng = EndWalkingDirection?.end_location?.lng
                       let EndAddress = EndWalkingDirection?.end_address
            

             StartMarker = MKPointAnnotation()
            
            StartMarker.title = StartAddress
           // StartMarker.subtitle = StartAddress
            StartMarker.coordinate = CLLocationCoordinate2D(latitude: StartAddressLat!, longitude: StartAddressLng!)
            self.mapView.addAnnotation(StartMarker)
            
            EndMarker = MKPointAnnotation()
            //EndMarker.title =
            EndMarker.title = EndAddress
            EndMarker.coordinate = CLLocationCoordinate2D(latitude: EndAddressLat!, longitude: EndAddressLng!)
            self.mapView.addAnnotation(EndMarker)
            
            
            let StartLatLouvelo = BikeDirection?.startLocation?.lat
            let StartlongLouvelo = BikeDirection?.startLocation?.lng
                LouveloMarker = MKPointAnnotation()
                          LouveloMarker.title = "Pickup your LouVelo vehicle from"
             LouveloMarker.subtitle = BikeDirection?.startAddress
                          LouveloMarker.coordinate = CLLocationCoordinate2D(latitude: StartLatLouvelo!, longitude: StartlongLouvelo!)
            self.mapView.addAnnotation(LouveloMarker)
            
            let EndLatLouvelo = BikeDirection?.endLocation?.lat
            let EndlongLouvelo = BikeDirection?.endLocation?.lng
                LouveloMarkerEnd = MKPointAnnotation()
                          LouveloMarkerEnd.title = "Drop your LouVelo vehicle at"
              LouveloMarkerEnd.subtitle = BikeDirection?.endAddress
                          LouveloMarkerEnd.coordinate = CLLocationCoordinate2D(latitude: EndLatLouvelo!, longitude: EndlongLouvelo!)
            self.mapView.addAnnotation(LouveloMarkerEnd)
            
            
            
            
        }
    }

        override func viewDidDisappear(_ animated: Bool) {
            if timer != nil {
                timer?.invalidate()
                timer = nil
            }
            NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)

        }
        private func addPullUpController() {
            let pullUpController = makeSearchViewControllerIfNeeded()
            _ = pullUpController.view // call pullUpController.viewDidLoad()
            addPullUpController(pullUpController,
                                initialStickyPointOffset: pullUpController.initialPointOffset,
                                animated: true)
        }
        
        func Shape(TripID:Int){
            
    }
        
        
  
        
        @IBAction private func addButtonTapped() {
            guard
                childViewControllers.filter({ $0 is TripPanelViewController }).count == 0
                else { return }
            addPullUpController()
        }
        @objc func OnBackClicked() {
          
            if(realTimeTimer != nil)
            {
                realTimeTimer.invalidate()
            }
            self.navigationController?.popViewController(animated: true)
            
        }
        
        @IBAction private func removeButtonTapped() {
            let pullUpController = makeSearchViewControllerIfNeeded()
            removePullUpController(pullUpController, animated: true)
        }
        
        
        @IBAction func CloseButton(_ sender: Any) {
            //(parent as? realtimeSchduleViewController)?.OnMenuClicked()
            dismiss(animated: true, completion: nil)
            
        }
        
        
        
        
        @objc func realtime(){}
       
        func showtransitOnMap()
        {
    
        }
    @objc func TankRunTimedCode()
    {
        print("Inside Tank Loop")
        for transState in tanktransitOptions
        {
            realtimeDataResponse = transState as! DataResponse<TransitRouteClass>
            let url2 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealTimeVehicleandNextStops"
            let parameters2: Parameters = [
                "TripID" : realtimeDataResponse.result.value?.triplist![0].tripID ?? "nil",
                "ArrivalTime" : realtimeDataResponse.result.value?.triplist![0].arrivalTime ?? "nil",
                "DepartureTime" : realtimeDataResponse.result.value?.triplist![0].departureTime ?? "nil",
                "DepartureStopName" : realtimeDataResponse.result.value?.triplist![0].departureStop ?? "nil",
                "ArrivalStopName" : realtimeDataResponse.result.value?.triplist![0].arrivalStop ?? "nil",
                
                ]
            
            
            Alamofire.request(url2, method: .get, parameters: parameters2, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<RealTimeRootClass>) in
                    switch response.result {
                    case .success:
                        if response.result.value != nil {
                            if response.result.value!.eTAminutes != nil {
                            if response.result.value!.vehicle! {
                        print("TARC__\(response.result.value?.lat as Any)")
                        for annotation in self.mapView.annotations {
                            
                            if let title = annotation.title, title == "#\((response.result.value?.routeShortName)!)" {
                                self.mapView.removeAnnotation(annotation)
                            }
                        }
//                        for markerDet in self.tankVehicleMarker
//                        {
//                            let removeMarker : GMSMarker = markerDet as! GMSMarker
//
//                            if removeMarker.title!.range(of:"\(self.routeIDDic.value(forKey: "\(response.result.value?.tripID ?? 0)") ?? "nil")") != nil
//
//                            {
//                                DispatchQueue.main.async {
//                                    removeMarker.map = nil;
//
//                                }
//                                self.tankVehicleMarker.remove(markerDet)
//                            }
//                        }
                        if response.result.value?.lat != 0 && response.result.value?.lng != 0 && response.result.value?.lat != nil
                        {
                            if (response.result.value?.busStops?.nextStops!.count)! > 0 {
                            let NextBusStop = response.result.value?.busStops?.nextStops![0].stopName
                            let NextBusStopArrival = response.result.value?.busStops?.nextStops![0].arrivalTime
                            let NextBusStopStopDistance = response.result.value?.busStops?.nextStops![0].stopDistance
                            let NextETA = response.result.value?.busStops?.nextStops![0].eTA
                            
                                self.busLat = response.result.value!.lat!
                                self.busLng = response.result.value!.lng!

                            self.realtimeIcon = MKPointAnnotation()
                            self.realtimeIcon.title = "#\((response.result.value?.routeShortName)!)"
                            self.realtimeIcon.subtitle = "Status:\((response.result.value?.tripStatus)!)\nETA: \((response.result.value?.eTAminutes)!)\n\nNext Stop:\((NextBusStop)!)\nNext Stop Arrival Time: \((NextBusStopArrival)!)\nNext Stop Distance: \((NextBusStopStopDistance)!)\nNext Stop ETA: \((NextETA)!)"
                            self.realtimeIcon.coordinate = CLLocationCoordinate2D(latitude: (response.result.value?.lat!)!, longitude: (response.result.value?.lng!)!)
                            self.mapView.addAnnotation(self.realtimeIcon)
                            
                            }
                            else{
                                self.realtimeIcon = MKPointAnnotation()
                                self.realtimeIcon.title = "#\((response.result.value?.routeShortName)!)"
                                self.realtimeIcon.coordinate = CLLocationCoordinate2D(latitude: (response.result.value?.lat!)!, longitude: (response.result.value?.lng!)!)
                                self.realtimeIcon.subtitle = "Status:\((response.result.value?.tripStatus)!)\nETA: \((response.result.value?.eTAminutes)!)"
                                self.busLat = response.result.value!.lat!
                                self.busLng = response.result.value!.lng!
                                self.mapView.addAnnotation(self.realtimeIcon)
                            }
                            }
                        }
                        }
                        }
                    case .failure(let error):
                        self.view.makeToast("Server ERROR")
                        print(error)
                    
            }
            }
        }
        
        
        
        
    }
    func addmarkerfrom(LatforBike:Double,LongforBike:Double,vechelType:String,Address:String){
        let StartLat = LatforBike
        let Startlong = LongforBike
        print("\(StartLat),\(Startlong)")
        print(vechelType)
       // if vechelType == "Bird" {
        if annoationRemove == false
        {
            annoationRemove = true
        }
        else
        {
            mapView.removeAnnotation(BirdMarker)
        }
                        BirdMarker = MKPointAnnotation()
                       BirdMarker.title = Address
                BirdMarker.subtitle = vechelType
            BirdMarker.coordinate = CLLocationCoordinate2D(latitude: StartLat, longitude: Startlong)
                       self.mapView.addAnnotation(BirdMarker)
                       
                       
//                   }
//                  else if vechelType == "Lime" {
//
//                        LimeMarker = MKPointAnnotation()
//                       LimeMarker.title = Address
//            LimeMarker.coordinate = CLLocationCoordinate2D(latitude: StartLat, longitude: Startlong)
//                       self.mapView.addAnnotation(LimeMarker)
//
//                   }
//                   else if vechelType == "Bolt" {
//
//                         boltMarker = MKPointAnnotation()
//                        boltMarker.title = Address
//                        boltMarker.coordinate = CLLocationCoordinate2D(latitude: StartLat, longitude: Startlong)
//                        self.mapView.addAnnotation(boltMarker)
//
//                    }
        
    }
    
    
    
          func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
              
              let testlineRenderer = MKPolylineRenderer(overlay: overlay)

              
              
              
              if overlay is MKCircle {
                  let renderer = MKCircleRenderer(overlay: overlay)
                  renderer.fillColor = UIColor.clear
                  renderer.strokeColor = UIColor.clear
                  renderer.lineWidth = 1
                  return renderer
              }
            
            if overlay as? MKPolyline == Birdroute_Array.polyline{
                let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                polyLineRenderer.strokeColor = color.hexStringToUIColor(hex: PolylineColor)
                polyLineRenderer.lineWidth = 4
                return polyLineRenderer
            }
            
              if overlay as? MKPolyline  == Walkingroute_Array.polyline {
                  let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                  polyLineRenderer.strokeColor = color.hexStringToUIColor(hex: "#E53638")
                  polyLineRenderer.lineWidth = 6
                   polyLineRenderer.lineDashPattern = [0, 10]
                  return polyLineRenderer
             }
            if overlay as? MKPolyline  == Uberroute_Array.polyline{
                let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                polyLineRenderer.strokeColor = color.hexStringToUIColor(hex: PolylineColor)
                polyLineRenderer.lineWidth = 6
                // polyLineRenderer.lineDashPattern = [0, 10]
                return polyLineRenderer
            }
              else{
                let polyLineRenderer = MKPolylineRenderer(overlay: overlay)
                polyLineRenderer.strokeColor = color.hexStringToUIColor(hex: PolylineColor)
                polyLineRenderer.lineWidth = 6
                // polyLineRenderer.lineDashPattern = [0, 10]
                return polyLineRenderer
            }
              
                     return testlineRenderer

          
          }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        print(annotation.title)
        guard annotation is MKPointAnnotation else { return nil }

        let identifier = "Tarc"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotation.title == StartMarker.title{
            let identifier = "Source"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.image = UIImage(named: "source_Marker")
                annotationView!.canShowCallout = true
                   } else {
                       annotationView!.annotation = annotation
                   }
                    return annotationView
        }
        
        
        else if annotation.title == EndMarker.title{
            let identifier = "Destination"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.image = UIImage(named: "destination_Marker")
                annotationView!.canShowCallout = true
                   } else {
                       annotationView!.annotation = annotation
                   }
                    return annotationView
        }
   else if annotation.title == realtimeIcon.title{
                let identifier = "RealTime"
                var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                if annotationView == nil {
                    
                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                    annotationView!.image = UIImage(named: "Bus_Marker")
                    annotationView!.canShowCallout = true
                       } else {
                           annotationView!.annotation = annotation
                       }
                    let subtitleView = UILabel()
                       subtitleView.font = subtitleView.font.withSize(15)
                       subtitleView.numberOfLines = 0
                       subtitleView.text = annotation.subtitle!
                       annotationView!.detailCalloutAccessoryView = subtitleView
                        return annotationView
            }
            //realtimeIcon
else if annotation.title == StartTransitMarker.title {
            let identifier = "StartTarcTransit"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.image = UIImage(named: "hops_Icon")
                annotationView!.canShowCallout = true
                   } else {
                       annotationView!.annotation = annotation
                   }
                    return annotationView
        }
      else if  annotation.title == EndTransitMarker.title{
            
            let identifier = "EndTarcTransit"
                       var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                       if annotationView == nil {
                           
                           annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                           annotationView!.image = UIImage(named: "hops_Icon")
                           annotationView!.canShowCallout = true
                              } else {
                                  annotationView!.annotation = annotation
                              }
                               return annotationView
            
        }
        
        
        else if annotation.title == BirdMarker.title {
            let identifier = "BirdMarker"
            print(annotation.subtitle)
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
           // if annotationView == nil {
                
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                if annotation.subtitle == "Lime"
                {
                    annotationView!.image = UIImage(named: "lime_Icon")

                }
                else
                {
                    annotationView!.image = UIImage(named: "BirdMapIcon")

                }
                annotationView!.canShowCallout = true
//                   } else {
//                       annotationView!.annotation = annotation
//                   }
                    return annotationView
        }
            
        else if annotation.title == BirdMarker.title {
            let identifier = "LimeMarker"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.image = UIImage(named: "lime_Icon")
                annotationView!.canShowCallout = true
                   } else {
                       annotationView!.annotation = annotation
                   }
                    return annotationView
        }
            else if annotation.title == BirdMarker.title {
                let identifier = "boltMarker"
                var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                if annotationView == nil {
                    
                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                    annotationView!.image = UIImage(named: "bolt_icon")
                    annotationView!.canShowCallout = true
                       } else {
                           annotationView!.annotation = annotation
                       }
                        return annotationView
            }
            //boltMarker
        else if annotation.title == LouveloMarker.title {
            let identifier = "LouveloMarker"
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.image = UIImage(named: "LouveloMapIcon")
                annotationView!.canShowCallout = true
                   } else {
                       annotationView!.annotation = annotation
                   }
                    return annotationView
        }
        
       else if annotation.title == LouveloMarkerEnd.title {
           let identifier = "LouveloMarkerEnd"
           var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
           if annotationView == nil {
               
               annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
               annotationView!.image = UIImage(named: "LouveloMapIcon")
               annotationView!.canShowCallout = true
                  } else {
                      annotationView!.annotation = annotation
                  }
                   return annotationView
       }

      else if annotation.title == placesAnnotation.title  {

            
            let identifier = self.placesAnnotation.title
            var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier!) as? MKPinAnnotationView
               if(pinView == nil) {
                   pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                    pinView!.image = UIImage(named: categoryNameString as String)
                   pinView!.canShowCallout = true
               }
            previousTitle = currentTitle as String
            let subtitleView = UILabel()
            subtitleView.font = subtitleView.font.withSize(15)
            subtitleView.numberOfLines = 0
            subtitleView.text = annotation.subtitle!
            pinView!.detailCalloutAccessoryView = subtitleView
               return pinView
            
            
        }

         
        return annotationView
        
    }

    func drawPolylineTransit(polylinePoint:String,TransitColor:String)
    {
        PolylineColor = TransitColor
        
        let polyline = Polyline(encodedPolyline:polylinePoint)
                    let decodedCoordinates: [CLLocationCoordinate2D]? = polyline.coordinates
        
//        let polylines = MKPolyline(coordinates:decodedCoordinates!, count: decodedCoordinates!.count)
//        mapView.add(polylines)
        let geodesic = MKPolyline(coordinates: decodedCoordinates!, count: decodedCoordinates!.count)
        mapView.add(geodesic)
        self.mapView.setVisibleMapRect(geodesic.boundingMapRect, animated: true)
    }
    func removeSpecificAnnotation() {
        for annotation in self.mapView.annotations {
            if let title = annotation.title, title == previousTitle {
                self.mapView.removeAnnotation(annotation)
            }
        }
    }
    func DrawPolilineForBikeShare(){
        
    }
    func DrawPolyline(SourceLat:Double,SourceLng:Double,DestinationLat:Double,DestinationLng:Double,Type:String,TripColor:String){
       print("\(TripColor) Befor set color ")
        PolylineColor = TripColor
        print("\(TripColor) After set color ")
        if Type == "Walking"{
            
          //  Optional(38.2351501), -85.7603056, 38.2364841, -85.7617595
            let Walkingrequest = MKDirections.Request()
            Walkingrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: SourceLat, longitude: SourceLng), addressDictionary: nil))
            Walkingrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: DestinationLat, longitude: DestinationLng), addressDictionary: nil))
            Walkingrequest.requestsAlternateRoutes = true
            Walkingrequest.transportType = .walking

            let Walkingdirections = MKDirections(request: Walkingrequest)

            Walkingdirections.calculate { [unowned self] response, error in
                guard let WalkingResponse = response else { return }

                //for Birdroute in BirdResponse.routes {
                if WalkingResponse.routes.count > 0 {
                    if WalkingResponse.routes.first != nil
                                       {
                self.Walkingroute_Array = WalkingResponse.routes.first!
                print(self.Walkingroute_Array.polyline)
                    self.mapView.add(self.Walkingroute_Array.polyline)
                    }
                }
            }
            
            
        }
        else if Type == "BikeShare"{
            
            let Birdrequest = MKDirections.Request()
            Birdrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: SourceLat, longitude: SourceLng), addressDictionary: nil))
            Birdrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: DestinationLat, longitude: DestinationLng), addressDictionary: nil))
            Birdrequest.requestsAlternateRoutes = true
            Birdrequest.transportType = .automobile

            let Birddirections = MKDirections(request: Birdrequest)

            Birddirections.calculate { [unowned self] response, error in
                guard let BirdResponse = response else { return }

                //for Birdroute in BirdResponse.routes {
                if BirdResponse.routes.count > 0 {
                self.Birdroute_Array = BirdResponse.routes.first!
                    self.mapView.add(self.Birdroute_Array.polyline)
                }
               // }
            }
            
            
            
            
            
            
        }
        else if Type == "Lime"{
            
        }
        else if Type == "Bolt"{
            
        }

        else if Type == "Uber"{
                
                let Birdrequest = MKDirections.Request()
                Birdrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: SourceLat, longitude: SourceLng), addressDictionary: nil))
                Birdrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: DestinationLat, longitude: DestinationLng), addressDictionary: nil))
                Birdrequest.requestsAlternateRoutes = true
                Birdrequest.transportType = .automobile

                let Birddirections = MKDirections(request: Birdrequest)

                Birddirections.calculate { [unowned self] response, error in
                    guard let UberResponse = response else { return }

                    //for Birdroute in BirdResponse.routes {
                        
                    self.Uberroute_Array = UberResponse.routes.first!
                        self.mapView.add(self.Uberroute_Array.polyline)
                        self.mapView.setVisibleMapRect(self.Uberroute_Array.polyline.boundingMapRect, animated: true)
                   // }
                }
        }
        
        
        
        
        
        
        
    }
    func zoom(to:String) {
        self.hideLoader()
                           let defaults = UserDefaults.standard
                           if defaults.object(forKey: "feedback") == nil {
        //                       defaults.set("No", forKey:"feedback")
        //                       defaults.synchronize()
                           DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                               //            self.functionToCall()
                               
                               let storyboard = UIStoryboard(name: "Main", bundle: nil)
                               let vc = storyboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                               // vc.newsObj = newsObj
                               self.present(vc, animated: true, completion: nil)
                           })
                           }
        
        
        
        
        
    }
          @objc func statusManager(_ notification: NSNotification) {
              offline.updateUserInterface(withoutLogin: false)
          }
          @objc func tapped(sender: UIButton) {
              print("\(sender) has been touched")
              
              
              let actionController = TwitterActionController()
              
              actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
                  
                                  let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                  let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                  self.navigationController?.pushViewController(nextViewController, animated: true)
                  
              }))
              
              
              
              if comVar.DateImplementation {
                  
                  actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                      
                      let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                      let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                      self.navigationController?.pushViewController(nextViewController, animated: true)
                    


                      
                  }))
                  
              }
              
              
              actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
                  
                  let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                  let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
                  self.navigationController?.pushViewController(nextViewController, animated: true)
                  
              }))
              
              actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
                  
                  let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                  let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
                  self.navigationController?.pushViewController(nextViewController, animated: true)
                  
              }))
              
              actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
                  
                  let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                  let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                  self.navigationController?.pushViewController(nextViewController, animated: true)
                  
              }))
              
              
              
              actionController.headerData = "Quick View"
              present(actionController, animated: true, completion: nil)
              
              
              
              
          }
        @objc func OpenBikeApp(){
               
               if isLimeredirect == "Lime"
               {
                   let app2Url: URL = URL(string: "lime://")!
                   
                   if UIApplication.shared.canOpenURL(app2Url) {
                       UIApplication.shared.openURL(app2Url)
                   }
                   else{
                       
                       UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/limebike-your-ride-anytime/id1199780189?ls=1")! as URL)
                   }
               }
               else
               {
                   let app2Url: URL = URL(string: "bolt://")!
                   
                   if UIApplication.shared.canOpenURL(app2Url) {
                       UIApplication.shared.openURL(app2Url)
                   }
                   else{
                       
                       UIApplication.shared.openURL(NSURL(string: "https://apps.apple.com/us/app/bolt-bolt-there-now/id1388810070")! as URL)
                   }
               }
           }
           

           @IBAction func AddFavAction(_ sender: Any) {
            
           }
    
   
    
        
}




