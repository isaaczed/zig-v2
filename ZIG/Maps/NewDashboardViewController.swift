//
//  NewDashboardViewController.swift
//  ZIG
//
//  Created by Isaac on 27/06/19.
//  Copyright © 2019 Isaac. All rights reserved.
//
//
//  secondScreenView.swift
//  ZIG
//
//  Created by MAC on 26/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire
import XLActionController
import NYAlertViewController
import NVActivityIndicatorView
import RealmSwift
import SCLAlertView
import SwiftyJSON
import Crashlytics
import AudioToolbox
//import JJFloatingActionButton
class NewDashboardViewController: UIViewController {
    // let actionButton = JJFloatingActionButton()
    let assistiveTouchSOS = AssistiveTouch()
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var heightTopView: NSLayoutConstraint!
    @IBOutlet weak var YpostionTopView: NSLayoutConstraint!
    @IBOutlet weak var userNameButton: UIButton!
    var userNameString = NSString()
    var loaderView = UIView()
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var secondCollectionView: UICollectionView!
    var cellId = "dashboardCell"
    var uberBool = Bool()
    var isLyft = Bool()
    var profileDataList: DataResponse<GetProfile>!
    var carSelected = Bool()
    var bikeSelected = Bool()
    var carFirstSelected = Bool()
    var bikeFirstSelected = Bool()
    var isRedirectFromLogin = Bool()
    var dashBoardArray = NSArray()
    var notificationCount = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideLoader()
        makeArray()

        print("BOOL==>\(comVar.firstTimeAppOpen)")
        var firsttime = comVar.firstTimeAppOpen as Bool
        if firsttime == true
            {
            firsttime = false
            comVar.firstTimeAppOpen = firsttime as Bool
            sendVersionNo()
        }
        print("BOOL==>\(firsttime)")
        getSupportList()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        // Do any additional setup after loading the view.
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "perferenceSelecteddash"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("perferenceSelecteddash"), object: nil)

        userNameButton.isUserInteractionEnabled = false
        // userNameButton .setImage(UIImage.init(named: "Image"), for: .normal)
        userNameButton .setTitle(" Welcome", for: .normal)
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            userNameButton.titleLabel?.font = UIFont.setTarcHeavy(size: 18)



            break
            // It's an iPhone
        case .pad:
            userNameButton.titleLabel?.font = UIFont.setTarcHeavy(size: 18)

            if UIScreen.main.nativeBounds.size.height > 1024
                {
                userNameButton.titleLabel?.font = UIFont.setTarcHeavy(size: 30)

            }

            break

            // It's an iPad
        case .unspecified: break
            // Uh, oh! What could it be?
        case .tv: break

        case .carPlay: break

        }
        let userDefaults1 = UserDefaults.standard

        if UserDefaults.standard.string(forKey: "accessToken") != nil || UserDefaults.standard.string(forKey: "accessToken") != ""
            {

            if userDefaults1.value(forKey: "userName") == nil || userDefaults1.value(forKey: "ProfileID") == nil || userDefaults1.value(forKey: "phoneNumber") == nil
                {

                let accessToken = userDefaults1.value(forKey: "accessToken")
                // let tokenString = accessToken as! String
                print("Access Token: \((accessToken)!)")
                let url = "\(APIUrl.ZIGTARCAPI)User/Profilelist"
                let parameters: Parameters = [
                    "token": accessToken!,

                ]
                Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                    .responseObject { (response: DataResponse<GetProfile>) in
                        switch response.result {

                        case .success:
                            if response.result.value?.message == "Ok"
                                {
                                //
                                let userDefaults1 = UserDefaults.standard
                                if response.result.value?.message == "Ok"
                                    {


                                    self.profileDataList = response

                                    userDefaults1.set(self.profileDataList.result.value?.result![0].username!, forKey: "userName")
                                    userDefaults1.synchronize()
                                    for profileModel in (self.profileDataList.result.value?.result![0].profileModels)!
                                    {
                                        //if profileModel.Default
                                        if profileModel.Default == true {
                                            userDefaults1.set(profileModel.emailId, forKey: "userEmail")
                                            userDefaults1.set(profileModel.profileId, forKey: "ProfileID")
                                            userDefaults1.set(profileModel.phoneNumber, forKey: "phoneNumber")


                                            if profileModel.Photolink?.range(of: "https://") != nil
                                                {
                                                // self.photolinkUser = profileModel.Photolink!
                                                userDefaults1.set(profileModel.Photolink!, forKey: "userPhoto")
                                                userDefaults1.synchronize()
                                                //  profileImage.downloadedFrom(link: profileModel.Photolink!)
                                            }
                                            else
                                            {
                                                userDefaults1.set("", forKey: "userPhoto")
                                                userDefaults1.synchronize()
                                                //  profileImage.image = UIImage(named:"male-user-profile-picture")
                                            }
                                        }
                                    }
                                    //  self.createUI()
                                }
                                else
                                {
                                    userDefaults1.set("", forKey: "userName")
                                    userDefaults1.set("", forKey: "userPhoto")
                                    userDefaults1.synchronize()
                                    print(response.result.value?.message!)
                                    //  self.showErrorDialogBox(viewController: self)
                                    //  self.showAlertBox(text: response.result.value!.message!)

                                }



                                ///
                                var profilename = response.result.value?.result?[0].profileModels

                                var userprofilename = NSString()
                                var emailAddress = NSString()

                                userprofilename = profilename![0].name! as NSString
                                emailAddress = profilename![0].emailId! as NSString

                                print(userprofilename)
                                print(emailAddress)

                                switch UIDevice.current.userInterfaceIdiom {
                                case .phone:
                                    self.userNameButton .setImage(UIImage.init(named: "Image"), for: .normal)

                                    break
                                    // It's an iPhone
                                case .pad:
                                    self.userNameButton .setImage(UIImage.init(named: "Image-4"), for: .normal)

                                    break
                                case .unspecified:
                                    break
                                case .tv:
                                    break
                                case .carPlay:
                                    break
                                }
                                self.userNameString = " Welcome \((response.result.value?.result![0].username!)!)" as NSString

                                let userDefaults2 = UserDefaults.standard

                                userDefaults2.set(emailAddress, forKey: "userEmail")

                                userDefaults2.set(userprofilename, forKey: "userName")
                                userDefaults2.synchronize()

                                //  self.userNameButton .setTitle(self.userNameString as String, for: .normal)



                            }
                        case .failure(_): break

                        }
                }
            }
            else
            {
                switch UIDevice.current.userInterfaceIdiom {
                case .phone:
                    self.userNameButton .setImage(UIImage.init(named: "Image"), for: .normal)

                    break
                    // It's an iPhone
                case .pad:
                    self.userNameButton .setImage(UIImage.init(named: "Image-4"), for: .normal)

                    break
                case .unspecified:
                    break
                case .tv:
                    break
                case .carPlay:
                    break
                }
                let usernameString: String = userDefaults1.value(forKey: "userName") as! String
                print("\(usernameString)")
                userNameButton .setTitle(" Welcome \(usernameString)", for: .normal)
            }

            if userDefaults1.integer(forKey: "userIdString") == nil {
                print("new")
                let accessToken = userDefaults1.value(forKey: "accessToken")

                let getUserId = "\(APIUrl.ZIGTARCAPI)User/api/GetUserId?Token=\(accessToken!)"

                Alamofire.request(getUserId, method: .post, parameters: nil, encoding: JSONEncoding.default)
                    .responseJSON { response in
                        switch response.result {
                        case .success:


                            if (response.result.value != nil) {

                                let responseVar = response.result.value
                                let json = JSON(responseVar!)
                                print(json.rawString()!)
                                let useridString = json.rawString()! as! String
                                let userid = Int(useridString)
                                let userDefaults = UserDefaults.standard
                                userDefaults.set(userid, forKey: "userIdString")


                            }
                        case .failure(let error):
                            print(error)

                            //



                            self.hideLoader()
                        }
                }
            }

        }
        else
        {
            let userDefaults = UserDefaults.standard
            userDefaults.removeObject(forKey: "accessToken")
            userDefaults.removeObject(forKey: "userName")
            userDefaults.removeObject(forKey: "userPhoto")
            userDefaults.removeObject(forKey: "userEmail")
            userDefaults.removeObject(forKey: "feedback")
            userDefaults.removeObject(forKey: "isLyft")
            userDefaults.removeObject(forKey: "isUber")
            userDefaults.removeObject(forKey: "isCab")
            userDefaults.removeObject(forKey: "ProfileID")
            userDefaults.removeObject(forKey: "IsUserLogin")
            userDefaults.removeObject(forKey: "bikeshare")
            userDefaults.removeObject(forKey: "phoneNumber")
            userDefaults.removeObject(forKey: "EmergencyAlert")
            userDefaults.removeObject(forKey: "ShowEmergencyAlert")

            userDefaults.synchronize()
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
            }
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WithOutLoginMainViewController") as! WithOutLoginMainViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        secondViewDesign()
//        TicketSave.TicketDataUpload(CountForRow: 0, Expiry: false, completion: { (SuccessMessage,Count) in
//                   if SuccessMessage {
//                       print("datasaved Dashboard.....\(Data())")
//                       print("Reloaded Dashboard......\(Date()) ")
//
//
//                   }
//               }
//               )



        //  animateImage()
        //   Emergency Hide june 30
        //view.addSubview(actionButton)
//           actionButton.translatesAutoresizingMaskIntoConstraints = false
//             if #available(iOS 11.0, *) {
//                 actionButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
//                 actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16).isActive = true
//             } else {
//                 actionButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
//                 actionButton.bottomAnchor.constraint(equalTo: bottomLayoutGuide.topAnchor, constant: -16).isActive = true
//             }
//
//        actionButton.handleSingleActionDirectly = false
//        actionButton.buttonDiameter = 65
//        actionButton.overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
//        actionButton.buttonImage = UIImage(named: "EmergencyIcon")
//
//        actionButton.buttonColor = color.hexStringToUIColor(hex: "#2673AF")
//        //actionButton.buttonImageColor = .white
//        actionButton.buttonImageSize = CGSize(width: 50, height: 50)
//
//        actionButton.buttonAnimationConfiguration = .transition(toImage: UIImage(named: "X")!)
//        actionButton.itemAnimationConfiguration = .slideIn(withInterItemSpacing: 14)
//
//        actionButton.layer.shadowColor = UIColor.black.cgColor
//        actionButton.layer.shadowOffset = CGSize(width: 0, height: 1)
//        actionButton.layer.shadowOpacity = Float(0.4)
//        actionButton.layer.shadowRadius = CGFloat(2)
//
//        actionButton.itemSizeRatio = CGFloat(0.75)
//        actionButton.configureDefaultItem { item in
//            item.titlePosition = .trailing
//
//            item.titleLabel.font = .boldSystemFont(ofSize: UIFont.systemFontSize)
//            item.titleLabel.textColor = .white
//            item.buttonColor = .white
//            item.buttonImageColor = .red
//
//            item.layer.shadowColor = UIColor.black.cgColor
//            item.layer.shadowOffset = CGSize(width: 0, height: 1)
//            item.layer.shadowOpacity = Float(0.4)
//            item.layer.shadowRadius = CGFloat(2)
//        }
//
//
//        let item = actionButton.addItem()
//        item.titleLabel.text = "SOS"
//        item.imageView.image = UIImage(named: "SOSAlert")
//       // item.buttonColor = .white
//        //item.size = CGSize(width: 80, height: 80)
//       // item.buttonImageColor = .white
//        item.imageSize = CGSize(width: 70, height: 70)
//        item.action = { item in
//
//            UIDevice.vibrate()
//                      print("Long Press")
//         //   self.addRippleEffect(to: self.actionButton)
//                      currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
//                                 if Success{
//                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
//                                  print("\(Address),\(LatsosLat),\(Longsoslong)")
//                                  MapsScheduleVC.CurrentlocationAddressString = Address
//                                  MapsScheduleVC.latSOS = LatsosLat
//                                  MapsScheduleVC.longSOS = Longsoslong
//                                    MapsScheduleVC.Triggervariable = "SOS"
//                                self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
//                                         //self.CurrentlocationAddress.text = Address
//                                     }
//                                 }
//
//        }
//
//         let StopRequestitem = actionButton.addItem()
//         StopRequestitem.titleLabel.text = "Stop Request"
//         StopRequestitem.imageView.image = UIImage(named: "StopRequest")
//         //StopRequestitem.buttonColor = .white
//         //item.size = CGSize(width: 80, height: 80)
//        // item.buttonImageColor = .white
//         StopRequestitem.imageSize = CGSize(width: 70, height: 70)
//         StopRequestitem.action = { item in
//
//             UIDevice.vibrate()
//                       print("Long Press")
//            // self.addRippleEffect(to: self.actionButton)
//
//                       currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
//                                  if Success{
//                                 let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                                 let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
//                                   print("\(Address),\(LatsosLat),\(Longsoslong)")
//                                   MapsScheduleVC.CurrentlocationAddressString = Address
//                                MapsScheduleVC.Triggervariable = "Stop"
//                                   MapsScheduleVC.latSOS = LatsosLat
//                                   MapsScheduleVC.longSOS = Longsoslong
//                                     MapsScheduleVC.Triggervariable = "Stop"
//                                 self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
//                                          //self.CurrentlocationAddress.text = Address
//                                      }
//                                  }
//
//         }

    }
    //@objc func longPressSOS(gesture: UILongPressGestureRecognizer)

    func addRippleEffect(to referenceView: UIView) {
        /*! Creates a circular path around the view*/
        let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height))
        /*! Position where the shape layer should be */
        let shapePosition = CGPoint(x: referenceView.bounds.size.width / 2.0, y: referenceView.bounds.size.height / 2.0)
        let rippleShape = CAShapeLayer()
        rippleShape.bounds = CGRect(x: 0, y: 0, width: referenceView.bounds.size.width, height: referenceView.bounds.size.height)
        rippleShape.path = path.cgPath
        rippleShape.fillColor = UIColor.clear.cgColor
        rippleShape.strokeColor = color.hexStringToUIColor(hex: "#D33131").cgColor
        rippleShape.lineWidth = 5
        rippleShape.position = shapePosition
        rippleShape.opacity = 0

        /*! Add the ripple layer as the sublayer of the reference view */
        referenceView.layer.addSublayer(rippleShape)
        /*! Create scale animation of the ripples */
        let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
        scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
        scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(2, 2, 1))
        /*! Create animation for opacity of the ripples */
        let opacityAnim = CABasicAnimation(keyPath: "opacity")
        opacityAnim.fromValue = 1
        opacityAnim.toValue = nil
        /*! Group the opacity and scale animations */
        let animation = CAAnimationGroup()
        animation.animations = [scaleAnim, opacityAnim]
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.duration = CFTimeInterval(1)
        animation.repeatCount = 25
        animation.isRemovedOnCompletion = true
        rippleShape.add(animation, forKey: "rippleEffect")
    }
    @objc func tappedSOS(sender: UIButton) {

        let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
            // action here
        }

        SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence", timeout: SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction: timeoutAction))

    }
    func sendVersionNo() {
        if isReachable()
            {

            let userDefaults1 = UserDefaults.standard
            if UserDefaults.standard.string(forKey: "accessToken") != nil
                {
                if userDefaults1.value(forKey: "userIdString") != nil
                    {

                    let accessToken = UserDefaults.standard.value(forKey: "accessToken")
                    let userIDString = userDefaults1.integer(forKey: "userIdString")
                    let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?

                    //Then just cast the object as a String, but be careful, you may want to double check for nil
                    let version = nsObject as! String
                    let dataDic: NSMutableDictionary = [
                        "Token": accessToken!,
                        "AgencyId": 1,
                        "UserId": userIDString,
                        "Issue": "Reference",
                        "Description": "",
                        "Misc1": version,
                        "Misc2": "",
                        "Misc3": ""



                    ]

                    let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
                    let url2 = URL(string: "\(APIUrl.ZIGTARCAPI)Feedback/Log/Add")!
                    var request = URLRequest(url: url2)
                    request.httpMethod = HTTPMethod.post.rawValue
                    request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                    request.httpBody = dataJson


                    Alamofire.request(request)
                        .responseObject { (response: DataResponse<Postpayment>) in
                            switch response.result {
                            case .success:
                                dump(response.result.value)



                            case .failure(let error):

                                print(error)

                            }

                    }

                }
                }
            }

        }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            navigationController?.setNavigationBarHidden(true, animated: false)
            if isRedirectFromLogin == true
                {
                comVar.status = 7
                let userDefaults1 = UserDefaults.standard
                self.uberBool = userDefaults1.bool(forKey: "isUber")
                self.isLyft = userDefaults1.bool(forKey: "isLyft")



                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
                vc.uberSelected = self.uberBool
                vc.lyftSelected = self.isLyft
                vc.isclickFromTripPlanner = true
                vc.saveTripBool = false
                vc.dashBoardScreen = true
                self.present(vc, animated: true, completion: nil)
            }
            let userDefaults1 = UserDefaults.standard

            if userDefaults1.value(forKey: "notificationCount") != nil
                {
                let notifcationInt = userDefaults1.value(forKey: "notificationCount") as! Int
                notificationCount = notifcationInt
                self.secondCollectionView.reloadData()

            }

        }

        //reappears navigation bar on next page
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            isRedirectFromLogin = false
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
        func makeArray() -> Void {


            dashBoardArray = [
                [
                    "title": "Trip Plan",
                    "image": "Dash-Tripplaner"
                ],
                [
                    "title": "Schedules",
                    "image": "helpRealTimeSchedules"
                ],
                [
                    "title": "Alerts",
                    "image": "AlertDash"
                ],
                [
                    "title": "Saved Trips",
                    "image": "Dash-fav"
                ],
                [
                    "title": "Buy Tickets", //My Tickets/ Help
                    "image": "freePass" //Help/freePass
                ],

                [
                    "title": "Preferences",
                    "image": "Image-1"
                ],
                [
                    "title": "Support",
                    "image": "Support"
                ],

                [
                    "title": "View Tickets",
                    "image": "ViewTicket"
                ],
                [
                    "title": "Logout",
                    "image": "Dash-Logout"
                ]
            ]

        }
        @objc func methodOfReceivedNotification(notification: Notification) {
            print("Value of notification : ", notification.object ?? "")
            let uberSelect = notification.object as! NSMutableArray
            let uberDict = uberSelect.object(at: 0) as! NSDictionary
            self.uberBool = uberDict.value(forKey: "uber") as! Bool
            self.isLyft = uberDict.value(forKey: "lyft") as! Bool

            self.carSelected = uberDict.value(forKey: "Cab") as! Bool
            self.bikeSelected = uberDict.value(forKey: "Bike") as! Bool
            self.carFirstSelected = uberDict.value(forKey: "Cabshare") as! Bool
            self.bikeFirstSelected = uberDict.value(forKey: "Bikeshare") as! Bool

            closePreferenceAction(sender: nil)

        }
        @objc func closePreferenceAction(sender: UIButton!)
        {
            UIView.animate(withDuration: 1,
                delay: 0.1,
                options: UIViewAnimationOptions.curveEaseInOut,
                animations: { () -> Void in

                    let userDefaults1 = UserDefaults.standard
                    let accessToken = userDefaults1.value(forKey: "accessToken")

                    userDefaults1.set(true, forKey: "isBus")
                    var cabBool = NSString()
                    cabBool = "true"
                    if self.uberBool == false && self.isLyft == false
                        {
                        cabBool = "false"
                    }
                    let url = "\(APIUrl.ZIGTARCAPI)Preferences/send"
                    let parameters: Parameters = [
                        "TokenId": accessToken!,
                        "bus": "true",
                        "bike": "true",
                        "train": "true",
                        "cab": "true",
                        "uber": "true",
                        "lyft": "true",
                        "lime": "false",
                        "bolt": "false",
                        "bikeshare": "\(self.bikeFirstSelected)",
                        "Cabshare": "\(self.carFirstSelected)",
                        "Message": ""
                    ]
                    print("\(parameters)")
                    print("Aruncheck ==> \(self.uberBool) \(self.isLyft)")
                    Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                        .responseObject { (response: DataResponse<TokenSend>) in
                            switch response.result {
                            case .success:
                                // print(response.result.value?.message ?? "nil")
                                userDefaults1.set("true", forKey: "isBus")
                                //userDefaults1.set(self.bikebuscheckbox.isOn, forKey: "isBike")
                                userDefaults1.set("true", forKey: "isBike")
                                userDefaults1.set("true", forKey: "isTrain")
                                userDefaults1.set(cabBool, forKey: "isCab")
                                userDefaults1.set(self.uberBool, forKey: "isUber")
                                userDefaults1.set(self.isLyft, forKey: "isLyft")

                                userDefaults1.set(self.carSelected, forKey: "Cab")
                                userDefaults1.set(self.bikeSelected, forKey: "Bike")

                                userDefaults1.set(self.carFirstSelected, forKey: "Cabshare")
                                userDefaults1.set(self.bikeFirstSelected, forKey: "Bikeshare")
                                userDefaults1.synchronize()


                            case .failure(let error):
                                print(error.localizedDescription)
                            }

                    }



                }, completion: { (finished) -> Void in

                })
        }
        func secondViewDesign()
        {

            let collectionViewSize = self.view.frame.size.width

            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            // layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            layout.sectionInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
            layout.itemSize = CGSize(width: collectionViewSize / 3.4, height: collectionViewSize / 3.4)
            layout.scrollDirection = .vertical
            layout.minimumLineSpacing = 5
            layout.minimumInteritemSpacing = 5


            switch UIDevice.current.userInterfaceIdiom {
            case .phone: break
                // It's an iPhone
            case .pad:
                // heightTopView.constant = 140

                // YpostionTopView.constant = 100
                layout.sectionInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
                //layout.itemSize = CGSize(width: collectionViewSize/3.6, height: collectionViewSize/3.6)
                layout.itemSize = CGSize(width: collectionViewSize / 5, height: collectionViewSize / 5)

                layout.scrollDirection = .vertical
                layout.minimumLineSpacing = 30
                layout.minimumInteritemSpacing = 5
                break
                // It's an iPad
            case .unspecified: break
                // Uh, oh! What could it be?
            case .tv: break

            case .carPlay: break

            }

            secondCollectionView.backgroundColor = UIColor.clear
            secondCollectionView.delegate = self
            secondCollectionView.dataSource = self
            secondCollectionView.collectionViewLayout = layout
            secondCollectionView.bounces = false


            //        let toplayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            //        toplayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            //
            //        toplayout.itemSize = CGSize(width: topCollectionView.contentInset.left * 2, height: topCollectionView.contentInset.left * 2)
            //        toplayout.scrollDirection = .horizontal
            //
            //
            //        topCollectionView.collectionViewLayout = toplayout



        }

        override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
            // updateCellsLayout()
        }
        //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //
        //        var cellSize: CGSize = collectionView.bounds.size
        //
        //        cellSize.width -= collectionView.contentInset.left * 2
        //        cellSize.width -= collectionView.contentInset.right * 2
        //        cellSize.height = cellSize.width
        //
        //        return cellSize
        //    }


        func updateCellsLayout() {

            let centerX = topCollectionView.contentOffset.x + (topCollectionView.frame.size.width) / 2
            for cell in topCollectionView.visibleCells {
                var offsetX = centerX - cell.center.x
                if offsetX < 0 {
                    offsetX *= -1
                }
                cell.transform = CGAffineTransform.identity

                switch UIDevice.current.userInterfaceIdiom {
                case .phone:
                    let offsetPercentage = offsetX / (view.bounds.width * 2.7)
                    let scaleX = 1 - offsetPercentage
                    cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)

                    break
                    // It's an iPhone
                case .pad:
                    let offsetPercentage = offsetX / (view.bounds.width * 2)
                    let scaleX = 1 - offsetPercentage
                    cell.transform = CGAffineTransform(scaleX: scaleX, y: scaleX)

                    break

                    // It's an iPad
                case .unspecified: break
                    // Uh, oh! What could it be?
                case .tv: break

                case .carPlay: break

                }
            }
        }
        func alertForOffilne()
        {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Internet Setting") {

                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }

                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Setting is opened: \(success)")
                    })
                }

            }
            alertView.addButton("Cancel") {
                if isReachable()
                    {

                }
                else
                {
                    //  self.alertForOffilne()
                }
            }

            alertView.showWarning("No Internet Connection", subTitle: "Please enable Internet access for TARC in your iOS settings")
        }

        func getSupportList()
            {
                notificationCount = 0

                self.showLoader()
                             let userDefaults1 = UserDefaults.standard

                let accessToken = userDefaults1.value(forKey: "accessToken")
               //                 let getUserId = "\(APIUrl.ZIGTARCAPI)User/api/GetUserId?Token=\(accessToken!)"
                let getUserId = "\(APIUrl.ZIGTARCAPI)Feedback/Support/Getallsupport?token=\(accessToken!)"

                                Alamofire.request(getUserId, method: .get, parameters: nil, encoding: JSONEncoding.default)
                                    .responseJSON { response in
                                        switch response.result {
                                        case .success:


                                            if (response.result.value != nil) {

                                                let responseVar = response.result.value
                                              //  let json = JSON(responseVar!)
                                                let dict  = responseVar as! NSDictionary
                                                
                                                print("\(dict)")
                                                var getArray = NSArray()
                                                var notificationCounts = 0

                                                getArray = dict.object(forKey: "Getsupportlist") as! NSArray
                                                print("\(getArray)")
                                                 if userDefaults1.value(forKey: "supportListArray") == nil
                                                 {
                                                let supportArray = NSMutableArray()
                                                for dict in getArray
                                                {
                                                    let localDictionary = dict as! NSDictionary
                                                    print(localDictionary)
                                                    let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                                                    var Title = String()
                                                    if localDictionary.value(forKey: "Title") is NSNull
                                                                                                  {
                                                        Title = ""
                                                                                                    
                                                    }
                                                    else
                                                    {
                                                        Title = localDictionary.value(forKey: "Title") as! String
                                                    }
                                                    
                                                    
                                                    var Description = String()
                                                if localDictionary.value(forKey: "Description") is NSNull
                                                    {
                                                        Description = ""
                                                    }
                                                    else
                                                     {
                                                           Description = localDictionary.value(forKey: "Description") as! String
                                                    }
                                                  
                                                    let Userid = localDictionary.value(forKey: "Userid") as! Int
                                                    let Status = localDictionary.value(forKey: "Status") as! Bool
                                                    let Reportcount = localDictionary.value(forKey: "Reportcount") as! Int
                                                    let Createddate = localDictionary.value(forKey: "Createddate") as! String
                                                        var ReadBool = Bool()
                                                    var totalReadCount = Int()
                                                    totalReadCount = 0
                                                    if Reportcount > 0
                                                    {
                                                        if Status == true
                                                        {
                                                            ReadBool = true
                                                            notificationCounts = notificationCounts + Reportcount
                                                        }
                                                        else
                                                        {
                                                             ReadBool = false
                                                        }
                                                    }
                                                    else
                                                    {
                                                         ReadBool = false
                                                    }
                                                    
                                                    let LocalDict = NSMutableDictionary()
                                                    let reportString = "\(Reportid)" as String
                                                    LocalDict.setValue(reportString, forKey: "Reportid")
                                                   LocalDict.setValue(Title, forKey: "Title")
                                                    LocalDict.setValue(Description, forKey: "Description")
                                                    LocalDict.setValue(Userid, forKey: "Userid")
                                                    LocalDict.setValue(Status, forKey: "Status")
                                                    LocalDict.setValue(Reportcount, forKey: "Reportcount")
                                                    LocalDict.setValue(Createddate, forKey: "Createddate")
                                                    LocalDict.setValue(ReadBool, forKey: "ReadBool")
                                                    LocalDict.setValue(totalReadCount, forKey: "totalReadCount")
                                                    supportArray.add(LocalDict)
                                                    
                                                }
                                       print(supportArray)
                                                let userDefaults1 = UserDefaults.standard
                                                userDefaults1.setValue(supportArray, forKey: "supportListArray")
                                                    self.notificationCount = notificationCounts
                                                userDefaults1.setValue(notificationCounts, forKey: "notificationCount")
                                                userDefaults1.synchronize()
                                               self.secondCollectionView.reloadData()
                                            }
                                                else
                                                 {
                                                   var supportListArray = NSMutableArray()
                                    let  supportListArrays = userDefaults1.value(forKey: "supportListArray") as! NSArray
                    supportListArray = supportListArrays.mutableCopy() as! NSMutableArray
                                                    
                                               // let  supportListArray = userDefaults1.value(forKey: "supportListArray") as! NSMutableArray
                                                    
                                                    let supportArray = NSMutableArray()

                                                    for dict in getArray
                                                                                           {
                                        let localDictionary = dict as! NSDictionary

                            let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                                let uniqueString = "\(Reportid)" as NSString
                        let resultPredicate : NSPredicate = NSPredicate(format: "Reportid contains[c] %@",uniqueString)
                        let searchResults = supportListArray.filtered(using: resultPredicate)
                                            print(searchResults)
                                                                 var notificationCountss = 0
                                        if searchResults.count > 0
                                                                                            {
                                                                                    
                                                                                                
                                                               //previosDictioanry
                                                             let previosArr = searchResults as! NSArray
                                                          let  previosDictioanry = previosArr.object(at: 0) as! NSDictionary
                                                                                                let localDictionary = dict as! NSDictionary
                                                                                                                                           print(localDictionary)
                                                                                                                                           let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                                                                                                                                            var Title = String()
                                                                                                                                                                                          if localDictionary.value(forKey: "Title") is NSNull
                                                                                                                                                                                                                                        {
                                                                                                                                                                                              Title = ""
                                                                                                                                                                                                                                          
                                                                                                                                                                                          }
                                                                                                                                                                                          else
                                                                                                                                                                                          {
                                                                                                                                                                                              Title = localDictionary.value(forKey: "Title") as! String
                                                                                                                                                                                          }
                                                                                                                                                                                          
                                                                                                                                                                                          
                                                                                                                                                                                          var Description = String()
                                                                                                                                                                                      if localDictionary.value(forKey: "Description") is NSNull
                                                                                                                                                                                          {
                                                                                                                                                                                              Description = ""
                                                                                                                                                                                          }
                                                                                                                                                                                          else
                                                                                                                                                                                           {
                                                                                                                                                                                                 Description = localDictionary.value(forKey: "Description") as! String
                                                                                                                                                                                          }
                                                                                                                                                                                                                                                                                                                               let Userid = localDictionary.value(forKey: "Userid") as! Int
                                                                                                                                           let Status = localDictionary.value(forKey: "Status") as! Bool
                                                                                                var Reportcount = localDictionary.value(forKey: "Reportcount") as! Int
                                                                                                                                           let Createddate = localDictionary.value(forKey: "Createddate") as! String
                                                                                                                                               var ReadBool = Bool()
                                                                                                                                           var totalReadCount = Int()
                             totalReadCount = previosDictioanry.value(forKey: "totalReadCount") as! Int
        //totalReadCount = 0
                                                                                                                                           if Reportcount > 0
                                                                                                                                           {
                                                                                                                                               if Status == true
                                                                                                                                               {
                                                                                                                                              //NEw COndtion
                                                                          if Reportcount >= totalReadCount
                                                                          {
                                                                            ReadBool = true
                                                                            
                                                                            let balance = Reportcount - totalReadCount
                                                                        
                                                                        // Reportcount = balance
                                                                            
                                                                            if Reportcount <= 0
                                                                            {
                                                                                ReadBool = false
                                                                            }
                                                                            
                                                                            notificationCounts = notificationCounts + balance
                                                                            if balance <= 0
                                                                            {
                                                                                 ReadBool = false
                                                                            }
                                                                                                                                                }
                                                                            else
                                                                          {
                                                                            
                                                                            ReadBool = false
                                                                                                                                                }
                                                                            }
                                                                                                                                               else
                                                                                                                                               {
                                                                                                                                                    ReadBool = false
                                                                                                                                               }
                                                                                                                                           }
                                                                                                                                           else
                                                                                                                                           {
                                                                                                                                                ReadBool = false
                                                                                                                                           }
                                                                                                                                           
                                                                                                                                           let LocalDict = NSMutableDictionary()
                                                                                                                                           let reportString = "\(Reportid)" as String
                                                                                                                                           LocalDict.setValue(reportString, forKey: "Reportid")
                                                                                                                                          LocalDict.setValue(Title, forKey: "Title")
                                                                                                                                           LocalDict.setValue(Description, forKey: "Description")
                                                                                                                                           LocalDict.setValue(Userid, forKey: "Userid")
                                                                                                                                           LocalDict.setValue(Status, forKey: "Status")
                                                                                                                                           LocalDict.setValue(Reportcount, forKey: "Reportcount")
                                                                                                                                           LocalDict.setValue(Createddate, forKey: "Createddate")
                                                                                                                                           LocalDict.setValue(ReadBool, forKey: "ReadBool")
                                                                                                                                           LocalDict.setValue(totalReadCount, forKey: "totalReadCount")
                                                                                                                                           supportArray.add(LocalDict)
                                                                                                
                                                                                                
                                                                                            }
                                                                                            else
                                        {
                                            let localDictionary = dict as! NSDictionary
                                                                                       print(localDictionary)
                                                                                       let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                                                                         var Title = String()
                                                                                                                                      if localDictionary.value(forKey: "Title") is NSNull
                                                                                                                                                                                    {
                                                                                                                                          Title = ""
                                                                                                                                                                                      
                                                                                                                                      }
                                                                                                                                      else
                                                                                                                                      {
                                                                                                                                          Title = localDictionary.value(forKey: "Title") as! String
                                                                                                                                      }
                                                                                                                                      
                                                                                                                                      
                                                                                                                                      var Description = String()
                                                                                                                                  if localDictionary.value(forKey: "Description") is NSNull
                                                                                                                                      {
                                                                                                                                          Description = ""
                                                                                                                                      }
                                                                                                                                      else
                                                                                                                                       {
                                                                                                                                             Description = localDictionary.value(forKey: "Description") as! String
                                                                                                                                      }
                                                                                                                                                                                                                      let Userid = localDictionary.value(forKey: "Userid") as! Int
                                                                                       let Status = localDictionary.value(forKey: "Status") as! Bool
                                                                                       let Reportcount = localDictionary.value(forKey: "Reportcount") as! Int
                                                                                       let Createddate = localDictionary.value(forKey: "Createddate") as! String
                                                                                           var ReadBool = Bool()
                                                                                       var totalReadCount = Int()
                                                                                       totalReadCount = 0
                                                                                       if Reportcount > 0
                                                                                       {
                                                                                           if Status == true
                                                                                           {
                                                                                               ReadBool = true
                                                                                               notificationCounts = notificationCounts + Reportcount
                                                                                           }
                                                                                           else
                                                                                           {
                                                                                                ReadBool = false
                                                                                           }
                                                                                       }
                                                                                       else
                                                                                       {
                                                                                            ReadBool = false
                                                                                       }
                                                                                       
                                                                                       let LocalDict = NSMutableDictionary()
                                                                                       let reportString = "\(Reportid)" as String
                                                                                       LocalDict.setValue(reportString, forKey: "Reportid")
                                                                                      LocalDict.setValue(Title, forKey: "Title")
                                                                                       LocalDict.setValue(Description, forKey: "Description")
                                                                                       LocalDict.setValue(Userid, forKey: "Userid")
                                                                                       LocalDict.setValue(Status, forKey: "Status")
                                                                                       LocalDict.setValue(Reportcount, forKey: "Reportcount")
                                                                                       LocalDict.setValue(Createddate, forKey: "Createddate")
                                                                                       LocalDict.setValue(ReadBool, forKey: "ReadBool")
                                                                                       LocalDict.setValue(totalReadCount, forKey: "totalReadCount")
                                                                                       supportArray.add(LocalDict)
                                                                                            }
                                                                                            
                                                                                            
                                                                                            
                                                    }

                                                    let userDefaults1 = UserDefaults.standard
                                                    userDefaults1.setValue(supportArray, forKey: "supportListArray")
                                                    self.notificationCount = notificationCounts
                                                    userDefaults1.setValue(notificationCounts, forKey: "notificationCount")
                                                                                           userDefaults1.synchronize()
                                                                                           self.secondCollectionView.reloadData()
                                                    
                                                    
                                                }

           self.hideLoader()


                                            }
                                        case .failure(let error):
                                            print(error)



                                            self.hideLoader()
                                        }
                                }
                
            }


    }

//  Collection View FlowLayout Delegate & Data Source


    extension NewDashboardViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {


        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return dashBoardArray.count
        }


        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! dashboardCell
            cell.newImage.isHidden = true
            cell.noticationCount.isHidden = true
            cell.noticationCount.layer.cornerRadius = 8
                          cell.noticationCount.layer.masksToBounds = true
            
            cell.noticationCount.layer.borderWidth = 0.2
            cell.noticationCount.layer.borderColor = UIColor.clear.cgColor
            cell.noticationCount.layer.shadowColor = UIColor.gray.cgColor
            cell.noticationCount.layer.shadowOffset = CGSize(width: CGFloat(1.0), height: CGFloat(2.0))
            cell.noticationCount.layer.shadowRadius = 1
            cell.noticationCount.layer.shadowOpacity = 0.8
          
            
            
            cell.backgroundColor = UIColor.white
            let dict = dashBoardArray.object(at: indexPath.row)as! NSDictionary
            let TitleName = dict.value(forKey: "title") as! NSString
            let imageName = dict.value(forKey: "image") as! NSString

            cell.dashboardTitle.text = TitleName as String
            cell.dahboardImage.image = UIImage.init(named: imageName as String)
            cell.dashboardTitle.accessibilityLabel = TitleName as String
            cell.layer.cornerRadius = 5
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowRadius = 2
            cell.layer.shadowOpacity = 0.25
            cell.layer.shadowOffset = CGSize(width: 1, height: 1);
            cell.layer.masksToBounds = false
            cell.clipsToBounds = false
            // 1
            cell.dashboardTitle.isAccessibilityElement = true
            // 2
            cell.dashboardTitle.accessibilityTraits = .zero
            // 3
           cell.dashboardTitle.accessibilityLabel = TitleName as String
            

            
            
            
            print(TitleName as String)
            if ((indexPath.row == 4) || (indexPath.row == 6) || (indexPath.row == 7))
                {
                cell.newImage.image = UIImage.gif(name: "new")
                cell.newImage.isHidden = false
                    cell.iPadNewImage.image = UIImage.gif(name: "new")
                     cell.iPadNewImage.isHidden = true
            }else{
                 cell.newImage.isHidden = true
                 cell.iPadNewImage.isHidden = true
                
            }


            if indexPath.row == 6
                {
                if notificationCount > 0
                    {
                    cell.noticationCount.isHidden = false
                       
                       // cell.noticationCount.layer.cornerRadius = 10
                    if notificationCount > 99
                        {
                        cell.noticationCount.text = "\(notificationCount)+ New*"

                    }
                    else
                    {
                        cell.noticationCount.text = "\(notificationCount) New*"

                    }

                        // 1
                                     cell.noticationCount.isAccessibilityElement = true
                                     // 2
                                     cell.noticationCount.accessibilityTraits = .zero
                                     // 3
                                    cell.noticationCount.accessibilityLabel = "\(notificationCount) New Message"
                        cell.noticationCount.adjustsFontForContentSizeCategory = true
                        
                }
                else
                {
                    cell.noticationCount.isHidden = true

                }

            }
            switch UIDevice.current.userInterfaceIdiom {
                    case .phone:
                        break
                    case .pad:
                       //  cell.newImage.isHidden = true
                        
                        cell.dashboardTitle.font = UIFont.setTarcHeavy(size: 24)
                        if UIScreen.main.nativeBounds.size.height > 1024
                        {
                            cell.dashboardTitle.font = UIFont.setTarcHeavy(size: 26)

                        }
                        if ((indexPath.row == 4) || (indexPath.row == 6) || (indexPath.row == 7))
                               {
                                  
                                
                                   cell.newImage.isHidden = true
                                cell.iPadNewImage.isHidden = false
                                
                              
                        }
                        else{
                                cell.newImage.isHidden = true
                                cell.iPadNewImage.isHidden = true
                        }
                        
                        break
                    case .unspecified: break
                    case .tv: break
                    case .carPlay: break
                        
                    }
//             cell.dashboardTitle.font = UIFont.preferredFont(forTextStyle: .body)
//            cell.dashboardTitle.adjustsFontForContentSizeCategory = true
//            cell.dashboardTitle.sizeToFit()

            return cell


        }


        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if isReachable()
                {
                if indexPath.row == 0
                    {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let tripPlannerViewController = mainStoryboard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                    self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
                }
                else if indexPath.row == 1
                    {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let MapsScheduleVC = mainStoryboard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
                    self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                }
                else if indexPath.row == 2
                    {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let MapsScheduleVC = mainStoryboard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                    self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                }
                else if indexPath.row == 3
                    {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let FavoritesTripVC = mainStoryboard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
                    self.navigationController!.pushViewController(FavoritesTripVC, animated: true)
                }
                else if indexPath.row == 4
                    {
                    //            OnBoardMsg.pageHeadings = ["Depart", "Places", "Preferences", "Add to Favorites"]
                    //            OnBoardMsg.pageImages = ["HelpDepartNow","helpPlaces","helpPreferences","FavDeactive"]
                    //            OnBoardMsg.pageSubHeadings = ["You can edit your departure time here.","Find Restaurants, Banks, City Services, etc.","Edit preferences for your route options.","Your favorite routes are stored and accessible here."]
                    //            OnBoardMsg.Buttoncolor = "#297EC3"
                    //            let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
                    //
                    //
                    //            if let walkthroughViewController = storyboard.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController {
                    //
                    //                present(walkthroughViewController, animated: true, completion: nil)
                    //            }
                    // BuyTicketTempViewController
                    //             let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    //                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                    //                self.navigationController?.pushViewController(nextViewController, animated: true)
                    //           // self.view.makeToast("Currently Disabled")

                    if comVar.DateImplementation {
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                    else
                    {

                        if let url = URL(string: "https://mytarc.ridetarc.org") {
                            let vc: SFSafariViewController

                            if #available(iOS 11.0, *) {
                                let config = SFSafariViewController.Configuration()
                                config.entersReaderIfAvailable = false
                                vc = SFSafariViewController(url: url, configuration: config)
                            } else {
                                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
                            }
                            vc.delegate = self as? SFSafariViewControllerDelegate
                            self.present(vc, animated: true)
                        }
                    }

                }
                else if indexPath.row == 5
                    {
                    comVar.status = 7
                    let userDefaults1 = UserDefaults.standard
                    self.uberBool = userDefaults1.bool(forKey: "isUber")
                    self.isLyft = userDefaults1.bool(forKey: "isLyft")



                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "QRCodeViewController") as! QRCodeViewController
                    vc.uberSelected = self.uberBool
                    vc.lyftSelected = self.isLyft
                    vc.isclickFromTripPlanner = true
                    vc.saveTripBool = false
                    vc.dashBoardScreen = true
                    self.present(vc, animated: true, completion: nil)
                }
                else if indexPath.row == 6
                    {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let supportTripVC = mainStoryboard.instantiateViewController(withIdentifier: "supportIndexViewController") as! supportIndexViewController
                    self.navigationController!.pushViewController(supportTripVC, animated: true)
                }

                else if indexPath.row == 7
                    {
//            if let url = URL(string: "https://www.ridetarc.org/privacy-policy/") {
//                let vc: SFSafariViewController
//
//                if #available(iOS 11.0, *) {
//                    let config = SFSafariViewController.Configuration()
//                    config.entersReaderIfAvailable = false
//                    vc = SFSafariViewController(url: url, configuration: config)
//                } else {
//                    vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
//                }
//                vc.delegate = self as? SFSafariViewControllerDelegate
//                present(vc, animated: true)
//            }

                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let supportTripVC = mainStoryboard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
                    self.navigationController!.pushViewController(supportTripVC, animated: true)

                }
                else if indexPath.row == 8
                    {

                    logout()

                }
            }
            else
            {
                if indexPath.row == 7
                    {
                    //            if let url = URL(string: "https://www.ridetarc.org/privacy-policy/") {
                    //                let vc: SFSafariViewController
                    //
                    //                if #available(iOS 11.0, *) {
                    //                    let config = SFSafariViewController.Configuration()
                    //                    config.entersReaderIfAvailable = false
                    //                    vc = SFSafariViewController(url: url, configuration: config)
                    //                } else {
                    //                    vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
                    //                }
                    //                vc.delegate = self as? SFSafariViewControllerDelegate
                    //                present(vc, animated: true)
                    //            }

                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let supportTripVC = mainStoryboard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
                    self.navigationController!.pushViewController(supportTripVC, animated: true)

                }
                else
                {
                    alertForOffilne()
                }
            }
        }


        func logout()
        {
            let alertViewController = NYAlertViewController()

            // Set a title and message
            alertViewController.title = "Logout"
            alertViewController.message = "Are you sure you would like to logout?"

            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = UIColor.white
            alertViewController.titleFont = UIFont.setTarcBold(size: 19)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            let activateAction = NYAlertAction(
                title: "Yes",
                style: .default,
                handler: { (action: NYAlertAction!) -> Void in
                    self.showLoader()
                    self.dismiss(animated: true, completion: nil)
                    let userDefaults = UserDefaults.standard
                    userDefaults.removeObject(forKey: "userIdString")
                    userDefaults.removeObject(forKey: "AnalyticsSessionID")
                    userDefaults.removeObject(forKey: "userName")

                    Crashlytics.sharedInstance().setUserIdentifier("Without Login")


                    let userDefaults1 = UserDefaults.standard
                    let accessToken = userDefaults1.value(forKey: "accessToken")
                    let url1 = "\(APIUrl.ZIGTARCAPI)User/Logout"
                    let parameters1: Parameters = [
                        "Token": accessToken!,
                    ]
                    Alamofire.request(url1, method: .post, parameters: parameters1, encoding: URLEncoding.default)
                        .responseObject { (response: DataResponse<Postpayment>) in
                            switch response.result {
                            case .success:
                                print(response.result.value?.message ?? "nil")
                                let userDefaults = UserDefaults.standard
                                userDefaults.removeObject(forKey: "accessToken")
                                userDefaults.removeObject(forKey: "userName")
                                userDefaults.removeObject(forKey: "userPhoto")
                                userDefaults.removeObject(forKey: "userEmail")
                                userDefaults.removeObject(forKey: "feedback")
                                userDefaults.removeObject(forKey: "isLyft")
                                userDefaults.removeObject(forKey: "isUber")
                                userDefaults.removeObject(forKey: "isCab")
                                userDefaults.removeObject(forKey: "ProfileID")
                                userDefaults.removeObject(forKey: "IsUserLogin")
                                userDefaults.removeObject(forKey: "SavedDictionary")
                                userDefaults.removeObject(forKey: "phoneNumber")
                                userDefaults.removeObject(forKey: "EmergencyAlert")
                                userDefaults.removeObject(forKey: "ShowEmergencyAlert")
                                userDefaults.removeObject(forKey: "supportListArray")
                                userDefaults.removeObject(forKey: "notificationCount")





                                let Ticketlist_db = try! Realm().objects(TicketRealmModelNewNew.self)
                                print(Ticketlist_db.count)
                                print("Updated Date - \(Date())")
                                // print(Ticketlist_db)
                                // print("saveditem ticket api list \(Date())")
                                userDefaults.synchronize()
                                let realma = try! Realm()
                                try! realma.write {
                                    let deletedNotifications = realma.objects(TicketRealmModelNewNew.self)
                                    realma.delete(deletedNotifications)
                                }

                                let realm = try! Realm()
                                try! realm.write {
                                    realm.deleteAll()
                                }


                                print(Ticketlist_db.count)
                                print("Updated Date - \(Date())")
                                print(Ticketlist_db)
                                print("saveditem ticket api list \(Date())")

                                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WithOutLoginMainViewController") as! WithOutLoginMainViewController
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                self.hideLoader()
                            case .failure(let error):
                                self.view.makeToast("Server Error! Please try again later!")
                                self.hideLoader()
                                print(error)
                            }

                    }
                }
            )
            let cancelAction = NYAlertAction(
                title: "No",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    self.dismiss(animated: true, completion: nil)
                }
            )
            alertViewController.addAction(activateAction)
            alertViewController.addAction(cancelAction)

            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)






        }

        func showLoader()
        {
            DispatchQueue.main.async {
                self.loaderView = UIView()
                self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height + 120)
                self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
                let window = UIApplication.shared.keyWindow!

                let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width / 2) - 40, y: (self.view.frame.size.height / 2) - 40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0))
                activityLoader.startAnimating()
                window.addSubview(self.loaderView)
                self.loaderView.addSubview(activityLoader)
            }
        }
        func hideLoader()
        {
            DispatchQueue.main.async {
                self.loaderView.removeFromSuperview()
            }
        }

    }

    class dashboardCell: UICollectionViewCell {

        @IBOutlet weak var dashboardTitle: UILabel!
        @IBOutlet weak var dahboardImage: UIImageView!
        @IBOutlet weak var newImage: UIImageView!
        @IBOutlet weak var noticationCount: UILabel!
        
        @IBOutlet weak var iPadNewImage: UIImageView!
        

    }

    extension UIDevice {
        static func vibrate() {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
    }
