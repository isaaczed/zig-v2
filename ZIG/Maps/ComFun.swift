//
//  ComFun.swift
//  ZIG
//
//  Created by Isaac on 24/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import Toast_Swift
import SCLAlertView
import SafariServices
import CoreLocation

class CheckInternet {
   
}
class OpenExternalSite {
    static func MyTarcCard(_ sender: Any) {
        
//        if let url = URL(string: "https://mytarc.ridetarc.org") {
//            let vc: SFSafariViewController
//
//            if #available(iOS 11.0, *) {
//                let config = SFSafariViewController.Configuration()
//                config.entersReaderIfAvailable = false
//                vc = SFSafariViewController(url: url, configuration: config)
//            } else {
//                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
//            }
//            vc.delegate = self as? SFSafariViewControllerDelegate
//            present(vc, animated: true)
//        }
        
    }
}
class TicketSave {
    
    static func GetallBeaconData() {
         let realm = try! Realm()
        print(APIUrl.MacIDURL)
        Alamofire.request(APIUrl.MacIDURL, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Beacon Data Featch Success")
                    if response.result.value != nil {
                        if let json = response.result.value as? [String: Any] {
                            let Macids = json["BeaconIds"] as! NSArray
                            for Macidlist in Macids {
                                
                                
                                let BeacanMacID = BeaconRealmModel()
                                BeacanMacID.MacID = Macidlist as! String
                                try! realm.write {
                                    
                                    realm.add(BeacanMacID, update: .all)
                                    
                                }
                                
                            }
                            do {
                                let beacon_db = try! Realm().objects(BeaconRealmModel.self)
                                print("beacon_db count\(beacon_db.count)")
                                
                                print("saveditem beacon_db")
                            }
                            catch {
                                print(error)
                            }
                            
                        }
                        
                        
                    }
                case .failure(let error):
                    print(error)
                    
                }
        }
        
        
        
    }
    static func TicketDataUpload(CountForRow:Int,Expiry:Bool,completion: @escaping (Bool,Int) -> ()) {
         if comVar.DateImplementation {
        //loader.instance.showloader()
        print("zed - \(CountForRow)")
        let uiRealm = try! Realm()
        
        let userAccessToken = UserDefaults.standard.object(forKey: "accessToken")
        if  userAccessToken != nil {
        let TicketUrl="\(APIUrl.TicketListURL)?token=\(userAccessToken!)&count=\(CountForRow)&Setup=\(Expiry)"
        print(TicketUrl)
        Alamofire.request(TicketUrl).responseObject { (response: DataResponse<TicketList>) in
            //self.ticket_list = [response.result.value!]
            switch response.result {
            case .success:
            if response.result.value != nil {
                if response.result.value!.count != nil {
                if response.result.value!.count! > 0 && response.result.value?.Message != nil {
            if response.result.value?.Message == "Ok"
            {
                
                if (response.result.value?.count)! > 0{
                    
                    
                    for ticketValue in (response.result.value?.Tickets)!
                    {
                        for subsetValue in (ticketValue.subset)!
                        {
                            let subsetvalue_array  = subsetValue
                            let myDateConv = subsetvalue_array.Expirydate!
                            let ExpiryDateConv = subsetvalue_array.ActivatedDate
                            
                            let ExpiryDateConvert = PDTtoEST(myDate: myDateConv)
                            let ActivateDateConvert = PDTtoEST(myDate: ExpiryDateConv)
                            
                            
                            
                            //if subsetValue.status != 0 {
                            
                            
                            let TicketSave = TicketRealmModelNewNew()
                            TicketSave.Amount = subsetvalue_array.Amount!
                            TicketSave.AgencyID = subsetvalue_array.AgencyId!
                            TicketSave.AgencyName = subsetvalue_array.AgencyName!
                            TicketSave.CategoryID = subsetvalue_array.CategoryId!
                            TicketSave.CategoryName = subsetvalue_array.CategoryName!
                            TicketSave.DestinationAddress = subsetvalue_array.DestinationAddress!
                            TicketSave.ExpriyDate = ExpiryDateConvert
                            TicketSave.FromAddress = subsetvalue_array.FromAddress!
                            TicketSave.IsActive = subsetvalue_array.isActive!
                            TicketSave.IsValid = subsetvalue_array.isValid!
                            TicketSave.ProfileId = subsetvalue_array.ProfileId!
                            TicketSave.ProfileName = subsetvalue_array.ProfileName!
                            TicketSave.Refund = subsetvalue_array.Refund
                            TicketSave.RemainingTime = subsetvalue_array.RemainingTime!
                            TicketSave.RouteID = subsetvalue_array.RouteId!
                            TicketSave.Status = subsetvalue_array.status!
                            TicketSave.TicketID = subsetvalue_array.TicketId!
                            TicketSave.TripID = subsetvalue_array.TripId!
                            TicketSave.CreatedDate = ticketValue.C_transaction_Date!
                            TicketSave.FareType = subsetvalue_array.FareType
                            TicketSave.ActivatedDate = ActivateDateConvert
                            TicketSave.Reactivatecount = subsetvalue_array.Reactivatecount
                            
                            try! uiRealm.write {
                                if CountForRow != 0 {
                                    let boolforReal = addCategory(category: TicketSave)
                                    print("Bool For real \(boolforReal)")
                                    if boolforReal == true
                                    {
                                        uiRealm.add(TicketSave, update: .error)
                                    }

                                
                                print("Ticket saved completed")
                                }
                                else{
                                    uiRealm.add(TicketSave, update: .all)
                                    
                                    print("Ticket Upload completed")
                                    
                                }
                                
                                
                           // }
                            }
                        }
                    }
                    do {
                        let Ticketlist_db = try! Realm().objects(TicketRealmModelNewNew.self)
                        print(Ticketlist_db.count)
                        print("Updated Date - \(Date())")
                        print(Ticketlist_db)
                        print("saveditem ticket api list \(Date())")
                        if var topController = UIApplication.shared.keyWindow?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                                
                            }
                            completion(true,response.result.value!.count!)
//                              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                           // topController.view.makeToast("Ticket Saved in Mobile")
                            //topController
                          
                            // topController should now be your topmost view controller
                        }
                        else{
                            completion(false,response.result.value!.count!)
                        }
                        
                        
                        
                    }
                    catch{
                        completion(false,0)
                        print(error)
                    }
                }
                else{
                    let Ticketlist_db = try! Realm().objects(TicketRealmModelNewNew.self)
                                           print(Ticketlist_db.count)
                                           print("Updated Date - \(Date())")
                                           print(Ticketlist_db)
                                           print("saveditem ticket api list \(Date())")
                    completion(true,response.result.value!.count!)
                }
            }
            else {
                
                completion(false,0)
                
                    }
                    
                }
                else{
                    let Ticketlist_db = try! Realm().objects(TicketRealmModelNewNew.self)
                    print(Ticketlist_db.count)
                    print("Updated Date - \(Date())")
                    print(Ticketlist_db)
                    print("saveditem ticket api list \(Date())")
                     completion(true,response.result.value!.count!)
                }
                }
                else{
                     completion(false,0)
                }
            }
            else{
                completion(false,0)
            }
            case .failure(let error):
                print(error)
               completion(false,0)
                
                
                
            }
            
        }
            
            
        //
        }
        else{
        completion(false,0)
        }
        }
    }
    static func PDTtoEST(myDate:String) -> String{
        
        let dateFormatter = DateFormatter()
        // dateFormatter.locale = Locale(identifier: "en_US_POSIX") // edited
         if myDate.count == 23 {
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
         }
         else if myDate.count == 19{
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
         }
         else if myDate.count == 21{
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
         }
         else if myDate.count == 22{
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
         }
         
        
         dateFormatter.timeZone = NSTimeZone(name: "PST") as TimeZone?
         let date = dateFormatter.date(from: myDate)// create   date from string
         
         // change to a readable time format and change to local time zone
         dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
         dateFormatter.timeZone = NSTimeZone(name: "EDT") as TimeZone?
         let timeStamp = dateFormatter.string(from: date!)
         
         
         return timeStamp
        
    }
    static func addCategory(category: TicketRealmModelNewNew) -> Bool {
        let realm = try! Realm()
        if realm.object(ofType: TicketRealmModelNewNew.self, forPrimaryKey: category.TicketID) != nil {
            return false
        }
        //        try! realm.write {
        //            realm.add(category)
        //        }
        return true
    }
}

class currentlocation{
    
   static func getAddressFromLatLon( completionHandler:@escaping (Bool,String,String,String) -> ())  {
    let locationManager = CLLocationManager()
    //locationManager.requestWhenInUseAuthorization()
    locationManager.requestAlwaysAuthorization()
    var currentLoc: CLLocation!
    if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
    CLLocationManager.authorizationStatus() == .authorizedAlways) {
       currentLoc = locationManager.location
       print(currentLoc.coordinate.latitude)
       print(currentLoc.coordinate.longitude)
      //  print(currentLoc.)
        //getAdressName(coords: currentLoc)
    
         var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(currentLoc.coordinate.latitude)")!
         let lon: Double = Double("\(currentLoc.coordinate.longitude)")!
        
       //  let lat: Double = Double("\(pdblLatitude)")!
        // let lon: Double = Double("\(pdblLongitude)")!
         //72.833770
         let ceo: CLGeocoder = CLGeocoder()
         center.latitude = lat
         center.longitude = lon

         let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


         ceo.reverseGeocodeLocation(loc, completionHandler:
             {(placemarks, error) in
                 if (error != nil)
                 {
                     print("reverse geodcode fail: \(error!.localizedDescription)")
                    completionHandler(false,"reverse geodcode fail: \(error!.localizedDescription)", "", "")
                 }
                 let pm = placemarks! as [CLPlacemark]

                 if pm.count > 0 {
                     let pm = placemarks![0]
                     
                     var addressString : String = ""
                     if pm.subLocality != nil {
                         addressString = addressString + pm.subLocality! + ", "
                     }
                     if pm.thoroughfare != nil {
                         addressString = addressString + pm.thoroughfare! + ", "
                     }
                     if pm.locality != nil {
                         addressString = addressString + pm.locality! + ", "
                     }
                     if pm.country != nil {
                         addressString = addressString + pm.country! + ", "
                     }
                     if pm.postalCode != nil {
                         addressString = addressString + pm.postalCode! + " "
                     }

                    completionHandler(true,addressString, "\(currentLoc.coordinate.latitude)", "\(currentLoc.coordinate.longitude)")
               }
         })

     }
    }
}
//
class offline {
    static func updateUserInterface(withoutLogin:Bool) {
        var userAccessToken = UserDefaults.standard.object(forKey: "Accesstoken")
        guard let status = Network.reachability?.status else { return }
        switch status {
        case .unreachable:
            comVar.internetchecker = false
            
            
        case .wifi:
            comVar.internetchecker = true
            
            
            
        case .wwan:
            comVar.internetchecker = true
            
            
        }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
        if Network.reachability?.isReachable == true {
           // offline.OfflinetoOnline(userTOken: userAccessToken as! String)
            
        }
        else if Network.reachability?.isReachable == false{
//            if withoutLogin == false
//            {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Internet Setting"){
                // ComValue.internetalertstatus = true
//                if withoutLogin == false
//                {
//                let window = UIApplication.shared.keyWindow!
//
//                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
////                let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
//
//                    let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//
//                var rootViewController = window.rootViewController as! UINavigationController
//                rootViewController.pushViewController(tripPlannerViewController, animated: true)
//                }
//                else
//                {
//
//                }
                
                
                guard let settingsUrl = URL(string:UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        // Checking for setting is opened or not
                        print("Setting is opened: \(success)")
                    })
                }
                
            }
            alertView.addButton("Cancel"){
                //ComValue.internetalertstatus = true
          //  updateUserInterface(withoutLogin: withoutLogin)
            }
            
            alertView.showWarning("No Internet Connection", subTitle: "Please enable Internet access for TARC in your iOS settings")

        }
        
        
    }
    static func OfflinetoOnline(userTOken:String){
        print(userTOken)
        let realm = try! Realm()
       
            let isUpdated = false
            do
            {

                let objs = realm.objects(ChangeStatusDataNew.self).filter("isUpdated == %@",isUpdated)
                if objs.count > 0 {

                    for Onlinedata in objs {

                        let beaconadd : NSMutableArray = []


                        let BeaconAddList : NSMutableDictionary = [:]

                        BeaconAddList.setValue(Onlinedata.Latitude, forKey: "latitude")
                        BeaconAddList.setValue(Onlinedata.Longitude, forKey: "longitude")
                        BeaconAddList.setValue(Onlinedata.TicketId, forKey: "ticketid")
                        BeaconAddList.setValue("\(Onlinedata.RouteID)", forKey: "routeid")
                        BeaconAddList.setValue("\(userTOken)", forKey: "accesstoken")
                        BeaconAddList.setValue("\(Onlinedata.Message)", forKey: "message")
                        BeaconAddList.setValue("\(Onlinedata.DateValue)", forKey: "date")
                        BeaconAddList.setValue("\(Onlinedata.BeaconMacID)", forKey: "beaconid")
                        BeaconAddList.setValue("\(Onlinedata.RouteID)", forKey: "tripid")
                        BeaconAddList.setValue(false, forKey: "IsDriver")
                        BeaconAddList.setValue(true, forKey: "IsBibo")


                        beaconadd.add(BeaconAddList)

                        let listDic : NSMutableDictionary = [:]
                        listDic.setValue(beaconadd, forKey: "List")
                        print(listDic)
                        let dataJson = try? JSONSerialization.data(withJSONObject: listDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                        //  print(dataJson)
                        let StatusChangeUrl = URL(string: APIUrl.TicketstateChangeURL)!
                        var request = URLRequest(url: StatusChangeUrl)
                        request.httpMethod = HTTPMethod.post.rawValue
                        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                        request.httpBody = dataJson

print(APIUrl.TicketstateChangeURL)
                        Alamofire.request(request).responseObject{ (response: DataResponse<statuschangeapiVerify>) in
                            switch response.result {


                            case .success:
                                let Statusmessage = response.result.value
                                if Statusmessage?.Message == "Ok"
                                {
                                    print("\(Onlinedata.TicketId)-ChangeAPI CALL Success")
                                    print(dataJson as Any)


                                    let statusChangeApifilter = realm.objects(ChangeStatusDataNew.self).filter("isUpdated == %@", isUpdated).first
                                    try! realm.write {
                                        statusChangeApifilter!.isUpdated = true
                                        print("********************updated")
                                        print(statusChangeApifilter!.isUpdated)
                                    }



                                }
                                else
                                {
                                    print("ERROR Status- \(Statusmessage!.Message!)")
                                }
                            case .failure(let error):
                                print(error)
                            }
                        }




                    }

                }

            
        }
    }
}
//
//
//class TicketSave {
//    static func TicketDataUpload(){
//        //loader.instance.showloader()
//        print("zed")
//        let uiRealm = try! Realm()
//
//        var userAccessToken = UserDefaults.standard.object(forKey: "Accesstoken")
//        let TicketUrl="\(APIUrl.TicketListURL)?token=\(userAccessToken!)"
//        print(TicketUrl)
//        Alamofire.request(TicketUrl).responseObject { (response: DataResponse<TicketList>) in
//            //self.ticket_list = [response.result.value!]
//            dump(response)
//            //loader.instance.showloader()
//            if response.result.value?.Message == "Ok"
//            {
//
//                if (response.result.value?.count)! > 0{
//
//
//                    for ticketValue in (response.result.value?.Tickets)!
//                    {
//                        for subsetValue in (ticketValue.subset)!
//                        {
//
//
//                            let subsetvalue_array  = subsetValue
//
//                            let TicketSave = TicketRealmModelNewNew()
//                            TicketSave.Amount = subsetvalue_array.Amount!
//                            TicketSave.AgencyID = subsetvalue_array.AgencyId!
//                            TicketSave.AgencyName = subsetvalue_array.AgencyName!
//                            TicketSave.CategoryID = subsetvalue_array.CategoryId!
//                            TicketSave.CategoryName = subsetvalue_array.CategoryName!
//                            TicketSave.DestinationAddress = subsetvalue_array.DestinationAddress!
//                            TicketSave.ExpriyDate = subsetvalue_array.Expirydate!
//                            TicketSave.FromAddress = subsetvalue_array.FromAddress!
//                            TicketSave.IsActive = subsetvalue_array.isActive!
//                            TicketSave.IsValid = subsetvalue_array.isValid!
//                            TicketSave.ProfileId = subsetvalue_array.ProfileId!
//                            TicketSave.ProfileName = subsetvalue_array.ProfileName!
//                            TicketSave.Refund = subsetvalue_array.Refund
//                            TicketSave.RemainingTime = subsetvalue_array.RemainingTime!
//                            TicketSave.RouteID = subsetvalue_array.RouteId!
//                            TicketSave.Status = subsetvalue_array.status!
//                            TicketSave.TicketID = subsetvalue_array.TicketId!
//                            TicketSave.TripID = subsetvalue_array.TripId!
//
//                            try! uiRealm.write {
//
//                                uiRealm.add(TicketSave, update: true)
//
//
//
//
//                            }
//                        }
//                    }
//                    do {
//                        let Ticketlist_db = try! Realm().objects(TicketRealmModelNewNew.self)
//                        print(Ticketlist_db.count)
//                        // dump(Ticketlist_db)
//                        print("saveditem ticket api list")
//                        if var topController = UIApplication.shared.keyWindow?.rootViewController {
//                            while let presentedViewController = topController.presentedViewController {
//                                topController = presentedViewController
//
//                            }
//                            topController.view.makeToast("Ticket Saved in Mobile")
//                            //topController
//                            loader.instance.hideloader()
//                            // topController should now be your topmost view controller
//                        }
//
//
//
//                    }
//                    catch{
//                        print(error)
//                    }
//                }
//            }
//            //loader.instance.hideloader()
//        }
//        //
//    }
//
//}
