//
//  CommonFile.swift
//  Zig
//
//  Created by Arun Pandiyan on 6/1/19.
//  Copyright © 2019 Zig. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import SCLAlertView







func isReachable() -> Bool{
    
    let isReach : Bool = ReachabilityNetwork.isConnectedToNetwork()
    return isReach
}
public class ReachabilityNetwork {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr()
        zeroAddress.sa_len = UInt8(MemoryLayout<sockaddr>.size)
        zeroAddress.sa_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }) else { return false }
        
        var flags = SCNetworkReachabilityFlags()
        guard SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) else { return false }
        
        return flags.contains(.reachable) && !flags.contains(.connectionRequired)
    }
    
}

func checkOnServiceError(error:Error) -> String{
    var nsError: Error?
    nsError = error
    var errorStr :String = ""
    if nsError != nil {
        errorStr = error.localizedDescription as String
    }
    else{
        errorStr = "Service Error"
    }
    return errorStr
}

//MARK:Get Data models

