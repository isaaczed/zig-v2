//
//  ShortLinkViewController.swift
//  ZIG
//
//  Created by Isaac on 19/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import UIKit

class ShortLinkViewController: UIViewController {

    @IBOutlet weak var TripPlanerStakView: UIStackView!
    @IBOutlet weak var TripPlanerText: UILabel!
    @IBOutlet weak var TrippplanerImage: UIImageView!
    
    @IBOutlet weak var SchedulesStakeView: UIStackView!
    @IBOutlet weak var SchedulesImage: UIImageView!
    @IBOutlet weak var SchedulesText: UILabel!
    
    @IBOutlet weak var SaveTripStakView: UIStackView!
    @IBOutlet weak var SaveTripImage: UIImageView!
    @IBOutlet weak var saveTripText: UILabel!
    
    
    @IBOutlet weak var AlertStakeView: UIStackView!
    @IBOutlet weak var AlertImage: UIImageView!
    @IBOutlet weak var AlertText: UILabel!
    
    
    @IBOutlet weak var Panel: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

        assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
        assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
        view.addSubview(assistiveTouch)
    
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.TrippplanerClick(_:)))
        TripPlanerStakView.addGestureRecognizer(tapGesture)
        
        
        let RealTime = UITapGestureRecognizer(target: self, action: #selector(self.RealtimeSchedule(_:)))
        SchedulesStakeView.addGestureRecognizer(RealTime)
        
        let saveTrip = UITapGestureRecognizer(target: self, action: #selector(self.SavedTrip(_:)))
        SaveTripStakView.addGestureRecognizer(saveTrip)
        
        let Alert = UITapGestureRecognizer(target: self, action: #selector(self.AlertView(_:)))
        AlertStakeView.addGestureRecognizer(Alert)
        
        
        
    }
    @objc func RealtimeSchedule(_ sender: UITapGestureRecognizer) {
        
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)
        
    }
    @objc func AlertView(_ sender: UITapGestureRecognizer) {
        
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)
        
    }
    @objc func SavedTrip(_ sender: UITapGestureRecognizer) {
        
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)
        
    }
    @objc func TrippplanerClick(_ sender: UITapGestureRecognizer) {

        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        self.present(navController, animated:true, completion: nil)
      print("tapped")
        
    }
    
    
    @IBAction func PressDown(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func tapped(sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}
