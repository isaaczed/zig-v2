//
//  preferenceTableViewCell.swift
//  ZIG
//
//  Created by Arun pandiyan on 16/07/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit

class preferenceTableViewCell: UITableViewCell {

    var trainImage = UIImageView()
    var traincheckbox = UISwitch()
    var trainLable = UILabel()

    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.backgroundColor = UIColor.clear
        
   

        trainImage.frame = CGRect.init(x: 20, y: 10, width: 24, height: 24)
        trainImage.image = UIImage(named:"train_Transit")
        self.addSubview(trainImage)
        
        trainLable.frame = CGRect.init(x: 80, y: 10, width: self.frame.size.width-140, height: 24)
        trainLable.text = "TRAIN"
        trainLable.textAlignment = NSTextAlignment.left
        trainLable.textColor = UIColor.black
        trainLable.font = UIFont.setTarcRegular(size: 15)
        self.addSubview(trainLable)
        
        traincheckbox.frame = CGRect.init(x: trainLable.frame.origin.x + trainLable.frame.size.width-20, y: 10, width: 24, height: 24)
        traincheckbox.tintColor = UIColor.red
        traincheckbox.onTintColor = UIColor.red
        self.addSubview(traincheckbox)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
