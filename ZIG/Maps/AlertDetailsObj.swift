//
//  AlertDetailsObj.swift
//  ZIG
//
//  Created by Isaac on 17/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper
class AlertDetails: Mappable {
    var Lineno:String?
    var LineAlert:String?
    var LineHeader:String?
    var RouteLong:String?
    var RouteShort:String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        Lineno <- map["Lineno"]
        LineAlert <- map["LineAlert"]
        LineHeader <- map["LineHeader"]
        RouteLong <- map["RouteLong"]
        RouteShort <- map["RouteShort"]
    }
    
    
}

