//
//  WalkthroughViewController.swift
//  Cafegram2EN
//
//  Created by Farukh IQBAL on 16/07/2018.
//  Copyright © 2018 Farukh IQBAL. All rights reserved.
//

import UIKit

class WalkthroughViewController: UIViewController, WalkthroughPageViewControllerDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet var pageControl: UIPageControl!
  
    @IBOutlet var nextButton: UIButton! {
        didSet {
            nextButton.layer.cornerRadius = 25.0
            nextButton.layer.masksToBounds = true
           // nextButton.layer.backgroundColor = color.hexStringToUIColor(hex: "#D0302E").cgColor
        }
    }
    
    @IBOutlet var skipButton: UIButton!
    
    // MARK: - Properties
    
    var walkthroughPageViewController: WalkthroughPageViewController?
    
    // MARK: - Actions
    
    @IBAction func skipButtonTapped(sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "hasViewedWalkthrough")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nextButtonTapped(sender: UIButton) {
        if let index = walkthroughPageViewController?.currentIndex {
            switch index {
            case 0...3:
                walkthroughPageViewController?.forwardPage()
                
            case 4:
                UserDefaults.standard.set(true, forKey: "hasViewedWalkthrough")
                
                dismiss(animated: true, completion: nil)
                
            default: break
            }
        }
        
        updateUI()
    }
    
    func updateUI() {
        if let index = walkthroughPageViewController?.currentIndex {
            switch index {
            case 0...3:
                nextButton.setTitle("NEXT", for: .normal)
                skipButton.isHidden = false
            
            case 4:
                nextButton.setTitle("GET STARTED", for: .normal)
                skipButton.isHidden = true
                
            default: break
            }
            
            pageControl.currentPage = index
        }
    }
    
    func didUpdatePageIndex(currentIndex: Int) {
        updateUI()
    }
    
    // MARK: - View controller life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)

        analytics.GetPageHitCount(PageName: "Help")
        
        pageControl.currentPageIndicatorTintColor = color.hexStringToUIColor(hex: OnBoardMsg.Buttoncolor)
  nextButton.backgroundColor = color.hexStringToUIColor(hex: OnBoardMsg.Buttoncolor)
        // Do any additional setup after loading the view.
    }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }

    override func viewDidDisappear(_ animated: Bool) {
         NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if let pageViewController = destination as? WalkthroughPageViewController {
            walkthroughPageViewController = pageViewController
            walkthroughPageViewController?.walkthroughDelegate = self
        }
    }
 

}
