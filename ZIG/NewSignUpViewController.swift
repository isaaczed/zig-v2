//
//  NewSignUpViewController.swift
//  ZIG
//
//  Created by Isaac on 07/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import NYAlertViewController
import IQKeyboardManagerSwift
import PhoneNumberKit

class NewSignUpViewController: UIViewController,UITextFieldDelegate {
    var loaderView = UIView()
    var name = ""
    var phoneNumber = ""
    var Email = ""
    var Password = ""
    var passwordVisibleBool:Bool?
    var passbutton = UIButton()
    @IBOutlet weak var signup: UIButton!
    
    @IBOutlet weak var login: UIButton!
    
     let phoneNumberKit = PhoneNumberKit()
    @IBOutlet weak var UsernameLbl: UILabel!
    @IBOutlet weak var PasswordLab: UILabel!
    @IBOutlet weak var EmailLal: UILabel!
     @IBOutlet weak var PhoneNumberLbl: UILabel!
    
    @IBOutlet weak var UsernameTxt: UITextField!
    @IBOutlet weak var PasswordTxt: UITextField!
    @IBOutlet weak var EmailTxt: UITextField!
    @IBOutlet weak var PhoneTxt: UITextField!
    
    @IBOutlet weak var passwordImage: UIImageView!

    @IBOutlet weak var UserNameSpliter: UIView!
    @IBOutlet weak var PasswordSpliter: UIView!
    @IBOutlet weak var EmailSpliter: UIView!
    @IBOutlet weak var PhoneSpliter: UIView!
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        UsernameTxt.text = ""
        PasswordTxt.text = ""
        EmailTxt.text = ""
        PhoneTxt.text = ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
         analytics.GetPageHitCount(PageName: "Registration")
        
        UsernameTxt.delegate = self
        PasswordTxt.delegate = self
         EmailTxt.delegate = self
         PhoneTxt.delegate = self
        // Do any additional setup after loading the view.
         self.property()
        login.addTarget(self, action:#selector(loginAct), for: UIControlEvents.touchUpInside)
        signup.addTarget(self, action:#selector(signupAct), for: UIControlEvents.touchUpInside)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginNewViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        passwordVisibleBool = true
        PasswordTxt.clearButtonMode = UITextFieldViewMode.never;
        let passwordtap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginNewViewController.passwordClick))
        passwordImage.addGestureRecognizer(passwordtap)
        PasswordTxt.bringSubview(toFront: passwordImage)
        passwordImage.isHidden = true
        passbutton = UIButton(type: .custom)
        passbutton.setImage(UIImage(named: "Image-6"), for: .normal)
        passbutton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        passbutton.frame = CGRect(x: CGFloat(PasswordTxt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        passbutton.addTarget(self, action: #selector(passwordClick), for: .touchUpInside)
        PasswordTxt.rightView = passbutton
        PasswordTxt.rightViewMode = .always
        
        passbutton.isAccessibilityElement = true
        passbutton.accessibilityLabel = "Password Secured or visble"
        
        
    }
    @objc func passwordClick()
    {
        if passwordVisibleBool == true{
            passwordVisibleBool = false
            PasswordTxt.isSecureTextEntry = false
            passbutton.setImage(UIImage(named: "Image-5"), for: .normal)

        }
        else
        {
            passwordVisibleBool = true
            PasswordTxt.isSecureTextEntry = true
            passbutton.setImage(UIImage(named: "Image-6"), for: .normal)
            
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            if textField.returnKeyType == .go{
                signupAct()
            }
            textField.resignFirstResponder()
            return true;
        }
        return false
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
//    @IBOutlet weak var UsernameLbl: UILabel!
//    @IBOutlet weak var PasswordLab: UILabel!
//    @IBOutlet weak var EmailLal: UILabel!
//    @IBOutlet weak var PhoneNumberLbl: UILabel!
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.UsernameTxt
        {
            
            self.UsernameTxt.font = UIFont.setTarcRegular(size: 20.0)
            self.UsernameLbl.isHidden = false
            self.UsernameLbl.text = "Username"
            self.UsernameTxt.placeholder = ""
            UserNameSpliter.backgroundColor = UIColor.black.withAlphaComponent(1)
            
            
        }
            
        else if textField == self.PasswordTxt
        {
            self.PasswordTxt.font = UIFont.setTarcRegular(size: 20.0)
            self.PasswordLab.isHidden = false
            self.PasswordLab.text = "Password"
            self.PasswordTxt.placeholder = ""
            PasswordSpliter.backgroundColor = UIColor.black.withAlphaComponent(1)
            
            
        }
        else if textField == self.EmailTxt
        {
            self.EmailTxt.font = UIFont.setTarcRegular(size: 20.0)
            self.EmailLal.isHidden = false
            self.EmailLal.text = "Email ID"
            self.EmailTxt.placeholder = ""
            EmailSpliter.backgroundColor = UIColor.black.withAlphaComponent(1)
            
            
        }
        else if textField == self.PhoneTxt
        {
            self.PhoneTxt.font = UIFont.setTarcRegular(size: 20.0)
            self.PhoneNumberLbl.isHidden = false
            self.PhoneNumberLbl.text = "Phone"
            self.PhoneTxt.placeholder = ""
            PhoneSpliter.backgroundColor = UIColor.black.withAlphaComponent(1)
            
            
        }
        
        
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        //Checkes if textfield is empty or not after after user ends editing.
        if textField == self.UsernameTxt
        {
            if self.UsernameTxt.text == ""
            {
                self.UsernameTxt.font = UIFont.setTarcBold(size: 15.0)
                self.UsernameLbl.isHidden = true
                self.UsernameTxt.placeholder = "Username"
                UserNameSpliter.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                
                
                
                
            }
            
            
        }
            
            
        else if textField == self.PasswordTxt
        {
            if self.PasswordTxt.text == ""
            {
                self.PasswordTxt.font = UIFont.setTarcBold(size: 15.0)
                self.PasswordLab.isHidden = true
                self.PasswordTxt.placeholder = "Password"
                PasswordSpliter.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                
                
                
            }
            
            
        }
        else if textField == self.EmailTxt
        {
            if self.EmailTxt.text == ""
            {
                self.EmailTxt.font = UIFont.setTarcBold(size: 15.0)
                self.EmailLal.isHidden = true
                self.EmailTxt.placeholder = "EmailID"
                EmailSpliter.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                
                
                
            }
            
            
        }
        else if textField == self.PhoneTxt
        {
            if self.PhoneTxt.text == ""
            {
                self.PhoneTxt.font = UIFont.setTarcBold(size: 15.0)
                self.PhoneNumberLbl.isHidden = true
                self.PhoneTxt.placeholder = "Phone number"
                PhoneSpliter.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                
                
                
            }
            
            
        }
        
        
    }
    
    func property()
    {
//        UserNameSpliter.layer.shadowOffset = CGSize(width: 0, height: 3)
//        UserNameSpliter.layer.shadowOpacity = 0.6
//        UserNameSpliter.layer.shadowRadius = 3.0
//        UserNameSpliter.layer.shadowColor = UIColor.black.cgColor
    UsernameLbl.isHidden =  true
    PasswordLab.isHidden = true
    EmailLal.isHidden =  true
    PhoneNumberLbl.isHidden = true
    
        
        UsernameTxt.text = ""
        PasswordTxt.text = ""
        EmailTxt.text = ""
        PhoneTxt.text = ""
    
    
    
        self.UsernameTxt.font = UIFont.setTarcBold(size: 15.0)
    self.PasswordTxt.font = UIFont.setTarcBold(size: 15.0)
    self.EmailTxt.font = UIFont.setTarcBold(size: 15.0)
    self.PhoneTxt.font = UIFont.setTarcBold(size: 15.0)
    
    self.PhoneTxt.returnKeyType = .go
    self.UsernameTxt.returnKeyType = .next
        self.PasswordTxt.returnKeyType = .next
        self.EmailTxt.returnKeyType = .next
        
    
    PasswordTxt.isSecureTextEntry = true
    UsernameTxt.autocorrectionType = .no
    
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func signupAct()
         {
            if isReachable()
            {
            if UsernameTxt.text != ""{
                if PasswordTxt.text != "" {
                    if EmailTxt.text != "" {
                        if PhoneTxt.text != "" {
                            
                            let registerUrl = "\(APIUrl.ZIGTARCAPI)User/Register"
                            name = UsernameTxt.text!
                            phoneNumber = PhoneTxt.text!
                            Email = EmailTxt.text!
                            Password = PasswordTxt.text!
                            self.showLoader()
                            let allowedCharset = CharacterSet
                                .decimalDigits
                                .union(CharacterSet(charactersIn: "+"))
                            
                            let filteredText = String(phoneNumber.unicodeScalars.filter(allowedCharset.contains))
                            
                            print(filteredText) // +55555555555
                            
                            // print(correct_phone)
                            phoneNumber = filteredText
                            //Check if user has entered all fields
                            // print(profileImageURL)
                            print(name)
                            let profileUrl = "\(APIUrl.ZIGTARCAPI)validation/UserValidation"
                            let parameters: Parameters = [
                                "Username": name.trimmingCharacters(in: .whitespacesAndNewlines),
                                "MailId": ""
                            ]
                            
                            Alamofire.request(profileUrl, method: .get, parameters: parameters, encoding: URLEncoding.default)
                                .responseObject{ (response: DataResponse<getValidation>) in
                                    switch response.result {
                                    case .success:
                                        print("success")
                                        if response.result.value?.status == true
                                        {
                                            let profileUrl1 = "\(APIUrl.ZIGTARCAPI)validation/UserValidation"
                                            let parameters1: Parameters = [
                                                "Username": "",
                                                "MailId": self.Email.trimmingCharacters(in: .whitespacesAndNewlines)
                                            ]
                                            
                                            Alamofire.request(profileUrl1, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                                                .responseObject{ (response: DataResponse<getValidation>) in
                                                    switch response.result {
                                                    case .success:
                                                        print("success")
                                                        
                                                        if response.result.value?.status == true
                                                        {
                                                            if !self.name.isEmpty{
                                                                if self.usernameValidation(username: self.name) == true {
                                                                    if !self.Password.isEmpty{
                                                                        if self.Password.count > 3 {
                                                                            if !self.Email.isEmpty {
                                                                                if self.isValidEmail(testStr: self.Email) {
                                                                                    if !self.phoneNumber.isEmpty{
                                                                                        if self.phoneNumberValidation(values: self.phoneNumber) == true{
                                                                                            print("******* Connect *****")

                                                                                            
                                                                                                
                                                                                                
                                                                                                let dataDic : NSMutableDictionary = [  "Username" : self.name,
                                                                                                                                       "Password" : self.Password,
                                                                                                                                       
                                                                                                                                       "Emailid" : self.Email,
                                                                                                                                       "Phone" : self.phoneNumber,
                                                                                                                                       "Name" : self.name,
                                                                                                                                       
                                                                                                                                       
                                                                                                                                       "PhotoLink" : ""]
                                                                                                
                                                                                                let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                                                                                                let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
                                                                                                let url2 = URL(string: registerUrl)!
                                                                                                var request = URLRequest(url: url2)
                                                                                                request.httpMethod = HTTPMethod.post.rawValue
                                                                                                request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                                                                                                request.httpBody = dataJson
                                                                                                
                                                                                                
                                                                                                Alamofire.request(request)
                                                                                                    .responseObject{ (response: DataResponse<Postpayment>) in
                                                                                                        switch response.result {
                                                                                                        case .success:
                                                                                                            dump(response.result.value)
                                                                                                            if response.result.value?.message == "Ok"{
                                                                                                                self.hideLoader()
                                                                                                                
                                                                                                                let alertViewController = NYAlertViewController()
                                                                                                                alertViewController.title = "Registration"
                                                                                                                alertViewController.message = "Thank you for registering to TARC. Please login."
                                                                                                                
                                                                                                                // Customize appearance as desired
                                                                                                                alertViewController.buttonCornerRadius = 20.0
                                                                                                                alertViewController.view.tintColor = self.view.tintColor
                                                                                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                                                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                                                                alertViewController.swipeDismissalGestureEnabled = true
                                                                                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                                                                                alertViewController.buttonColor = UIColor.red
                                                                                                                // Add alert actions
                                                                                                                
                                                                                                                
                                                                                                                let cancelAction = NYAlertAction(
                                                                                                                    title: "OK",
                                                                                                                    style: .cancel,
                                                                                                                    handler: { (action: NYAlertAction!) -> Void in
                                                                                                                        
                                                                                                                        self.dismiss(animated: true, completion: nil)
                                                                                                                        
                                                                                                                        let tripPlannerViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
                                                                                                                        
                                                                                                                        
                                                                                                                        //  rootViewController?.present(tripPlannerViewController, animated: true, completion: nil)
                                                                                                                        
                                                                                                                        self.navigationController?.pushViewController(tripPlannerViewController, animated: true)
                                                                                                                }
                                                                                                                )
                                                                                                                
                                                                                                                alertViewController.addAction(cancelAction)
                                                                                                                
                                                                                                                // Present the alert view controller
                                                                                                                self.present(alertViewController, animated: true, completion: nil)
                                                                                                                
                                                                                                                
                                                                                                                // self.present(tripPlannerViewController, animated: false, completion: nil)
                                                                                                                
                                                                                                                
                                                                                                            }else{
                                                                                                                //print(response.result.value?.message!)
                                                                                                                self.hideLoader()
                                                                                                                let alertViewController = NYAlertViewController()
                                                                                                                alertViewController.title = "Registration Failed"
                                                                                                                alertViewController.message = "Something went wrong. Please try again."
                                                                                                                
                                                                                                                // Customize appearance as desired
                                                                                                                alertViewController.buttonCornerRadius = 20.0
                                                                                                                alertViewController.view.tintColor = self.view.tintColor
                                                                                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                                                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                                                                alertViewController.swipeDismissalGestureEnabled = true
                                                                                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                                                                                alertViewController.buttonColor = UIColor.red
                                                                                                                // Add alert actions
                                                                                                                
                                                                                                                
                                                                                                                let cancelAction = NYAlertAction(
                                                                                                                    title: "OK",
                                                                                                                    style: .cancel,
                                                                                                                    handler: { (action: NYAlertAction!) -> Void in
                                                                                                                        
                                                                                                                        self.dismiss(animated: true, completion: nil)
                                                                                                                }
                                                                                                                )
                                                                                                                
                                                                                                                alertViewController.addAction(cancelAction)
                                                                                                                
                                                                                                                // Present the alert view controller
                                                                                                                self.present(alertViewController, animated: true, completion: nil)
                                                                                                            }
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                        case .failure(let error):
                                                                                                            
                                                                                                            print(error)
                                                                                                            self.showErrorDialogBox(viewController: self)
                                                                                                        }
                                                                                                        
                                                                                                }
                                                                                            
                                                                                        }
                                                                                            
                                                                                        else{
                                                                                            self.hideLoader()
                                                                                            self.showAlertBox(text: "Enter valid phone number")
                                                                                        }
                                                                                        
                                                                                    }else{
                                                                                        self.hideLoader()
                                                                                        self.showAlertBox(text: "Enter phone number")
                                                                                    }
                                                                                }
                                                                                else{
                                                                                    self.hideLoader()
                                                                                    self.showAlertBox(text: "Enter valid Email")
                                                                                }
                                                                            }
                                                                            else{
                                                                                self.hideLoader()
                                                                                self.showAlertBox(text: "Enter Email ID")
                                                                                
                                                                            }
                                                                        }
                                                                        else{
                                                                            self.hideLoader()
                                                                            self.showAlertBox(text: "Enter Valid Password")
                                                                        }
                                                                    }
                                                                    else{
                                                                        self.hideLoader()
                                                                        self.showAlertBox(text: "Enter Password")
                                                                    }
                                                                }
                                                                else{
                                                                    self.hideLoader()
                                                                    self.showAlertBox(text: "Username should be atleast 3 characters and should not contain spaces")
                                                                }
                                                                
                                                            }else{
                                                                self.hideLoader()
                                                                self.showAlertBox(text: "Enter Username")
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if response.result.value?.message != nil{
                                                                DispatchQueue.main.async {
                                                                    self.hideLoader()
                                                                }
                                                                let alertViewController = NYAlertViewController()
                                                                // Set a title and message
                                                                alertViewController.title = response.result.value?.message!
                                                                alertViewController.message = ""
                                                                // Customize appearance as desired
                                                                alertViewController.buttonCornerRadius = 20.0
                                                                alertViewController.view.tintColor = self.view.tintColor
                                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                alertViewController.swipeDismissalGestureEnabled = true
                                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                                alertViewController.buttonColor = UIColor.red
                                                                
                                                                let cancelAction = NYAlertAction(
                                                                    title: "OK",
                                                                    style: .cancel,
                                                                    handler: { (action: NYAlertAction!) -> Void in
                                                                        // self.sourceTextField.becomeFirstResponder()
                                                                        self.dismiss(animated: true, completion: nil)
                                                                }
                                                                )
                                                                //        alertViewController.addAction(activateAction)
                                                                alertViewController.addAction(cancelAction)
                                                                
                                                                // Present the alert view controller
                                                                self.present(alertViewController, animated: true, completion: nil)
                                                                
                                                            }
                                                            
                                                        }
                                                    case .failure(let error):
                                                        print(error)
                                                        DispatchQueue.main.async {
                                                            self.hideLoader()
                                                        }
                                                        self.showAlertBox(text: "Server Error! Please try again later!")
                                                    }
                                            }
                                        }
                                        else
                                        {
                                            if response.result.value?.message != nil{
                                                DispatchQueue.main.async {
                                                    self.hideLoader()
                                                }
                                                
                                                let alertViewController = NYAlertViewController()
                                                // Set a title and message
                                                alertViewController.title = response.result.value?.message!
                                                alertViewController.message = ""
                                                // Customize appearance as desired
                                                alertViewController.buttonCornerRadius = 20.0
                                                alertViewController.view.tintColor = self.view.tintColor
                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.swipeDismissalGestureEnabled = true
                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                alertViewController.buttonColor = UIColor.red
                                                
                                                let cancelAction = NYAlertAction(
                                                    title: "OK",
                                                    style: .cancel,
                                                    handler: { (action: NYAlertAction!) -> Void in
                                                        // self.sourceTextField.becomeFirstResponder()
                                                        self.dismiss(animated: true, completion: nil)
                                                }
                                                )
                                                //        alertViewController.addAction(activateAction)
                                                alertViewController.addAction(cancelAction)
                                                
                                                // Present the alert view controller
                                                self.present(alertViewController, animated: true, completion: nil)
                                                
                                                
                                                
                                                
                                            }
                                            
                                        }
                                    case .failure(let error):
                                        print(error)
                                        DispatchQueue.main.async {
                                            self.hideLoader()
                                        }
                                        self.showAlertBox(text: "Server Error! Please try again later!")
                                    }
                            }
                            
                        }
                            
                            
                            
                        else{
                            showAlertBox(text: "Enter Phone Number")
                        }
                    }
                    else{
                        showAlertBox(text: "Enter Email ID")
                    }
                }
                else {
                    showAlertBox(text: "Enter Password")
                }
            }
            else{
                showAlertBox(text: "Enter Username")
            }
    }
            else
            {
                self.view.makeToast("No Internet connection.  Please turn on WiFi or enable data.")
            }
        }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    func usernameValidation(username:String) -> Bool {
        return username.range(of: "^.[A-Za-z0-9_@.#!$%&+-+]{2,28}$", options: .regularExpression) != nil
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showAlertBox(text:String){
        let alertViewController = NYAlertViewController()
        // Set a title and message
        alertViewController.title = "Enter Correct Information"
        alertViewController.message = "\(text)"
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = self.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                // self.sourceTextField.becomeFirstResponder()
                self.dismiss(animated: true, completion: nil)
        }
        )
        //        alertViewController.addAction(activateAction)
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
    }
    func showErrorDialogBox(viewController : UIViewController){
        let alertViewController = NYAlertViewController()
        alertViewController.title = "Error"
        alertViewController.message = "Server Error! Please try again later!"
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = viewController.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        // Add alert actions
        
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                
                viewController.dismiss(animated: true, completion: nil)
        }
        )
        
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        viewController.present(alertViewController, animated: true, completion: nil)
        
    }
    func phoneNumberValidation(values : String) -> Bool {
        do {
            // let phoneNumber = try phoneNumberKit.parse("+33 6 89 017383")
            let phoneNumberCustomDefaultRegion = try phoneNumberKit.parse(values, withRegion: "US", ignoreType: true)
            return true
            
        }
        catch {
            print("Generic parser error")
            return false
        }
        
    }
    @objc func loginAct(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    }


