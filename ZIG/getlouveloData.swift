//
//  getlouveloData.swift
//  ZIG
//
//  Created by VC on 05/02/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import ObjectMapper

class getlouveloData: Mappable {
    var last_updated:Int?
    var Count:Int?
    var louveldata:[DataLouvelo]?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        last_updated <- map["last_updated"]
        Count <- map["ttl"]
        louveldata <- map["data.stations"]
    }

}
class DataLouvelo: Mappable {
    var StationName:String?
    var StationID:Int?
    var StationAddress:String?
    var StationLat:Double?
    var StationLong:Double?
    //var Stationaddress:String?
    var capacity:Int?
    var rental_methods:Any?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        StationName <- map["name"]
        StationID <- map["station_id"]
        StationAddress <- map["StationAddress"]
        StationLat <- map["lat"]
        StationLong <- map["lon"]
        capacity <- map["capacity"]
        
    }
    
    
}
