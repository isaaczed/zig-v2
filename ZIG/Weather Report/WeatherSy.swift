//
//	WeatherSy.swift


import Foundation 
import ObjectMapper


class WeatherSy : NSObject, NSCoding, Mappable{

	var country : String?
	var message : Double?
	var sunrise : Int?
	var sunset : Int?


	class func newInstance(map: Map) -> Mappable?{
		return WeatherSy()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		country <- map["country"]
		message <- map["message"]
		sunrise <- map["sunrise"]
		sunset <- map["sunset"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         country = aDecoder.decodeObject(forKey: "country") as? String
         message = aDecoder.decodeObject(forKey: "message") as? Double
         sunrise = aDecoder.decodeObject(forKey: "sunrise") as? Int
         sunset = aDecoder.decodeObject(forKey: "sunset") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if sunrise != nil{
			aCoder.encode(sunrise, forKey: "sunrise")
		}
		if sunset != nil{
			aCoder.encode(sunset, forKey: "sunset")
		}

	}

}
