//
//	WeatherMain.swift


import Foundation 
import ObjectMapper


class WeatherMain : NSObject, NSCoding, Mappable{

	var grndLevel : Double?
	var humidity : Int?
	var pressure : Double?
	var seaLevel : Double?
	var temp : Double?
	var tempMax : Double?
	var tempMin : Double?


	class func newInstance(map: Map) -> Mappable?{
		return WeatherMain()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		grndLevel <- map["grnd_level"]
		humidity <- map["humidity"]
		pressure <- map["pressure"]
		seaLevel <- map["sea_level"]
		temp <- map["temp"]
		tempMax <- map["temp_max"]
		tempMin <- map["temp_min"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         grndLevel = aDecoder.decodeObject(forKey: "grnd_level") as? Double
         humidity = aDecoder.decodeObject(forKey: "humidity") as? Int
         pressure = aDecoder.decodeObject(forKey: "pressure") as? Double
         seaLevel = aDecoder.decodeObject(forKey: "sea_level") as? Double
         temp = aDecoder.decodeObject(forKey: "temp") as? Double
         tempMax = aDecoder.decodeObject(forKey: "temp_max") as? Double
         tempMin = aDecoder.decodeObject(forKey: "temp_min") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if grndLevel != nil{
			aCoder.encode(grndLevel, forKey: "grnd_level")
		}
		if humidity != nil{
			aCoder.encode(humidity, forKey: "humidity")
		}
		if pressure != nil{
			aCoder.encode(pressure, forKey: "pressure")
		}
		if seaLevel != nil{
			aCoder.encode(seaLevel, forKey: "sea_level")
		}
		if temp != nil{
			aCoder.encode(temp, forKey: "temp")
		}
		if tempMax != nil{
			aCoder.encode(tempMax, forKey: "temp_max")
		}
		if tempMin != nil{
			aCoder.encode(tempMin, forKey: "temp_min")
		}

	}

}
