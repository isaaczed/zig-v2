//
//	WeatherRoot.swift


import Foundation 
import ObjectMapper


class WeatherRoot : NSObject, NSCoding, Mappable{

	var base : String?
	var clouds : WeatherCloud?
	var cod : Int?
	var coord : WeatherCoord?
	var dt : Int?
	var id : Int?
	var main : WeatherMain?
	var name : String?
	var sys : WeatherSy?
	var weather : [Weather]?
	var wind : WeatherWind?


	class func newInstance(map: Map) -> Mappable?{
		return WeatherRoot()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		base <- map["base"]
		clouds <- map["clouds"]
		cod <- map["cod"]
		coord <- map["coord"]
		dt <- map["dt"]
		id <- map["id"]
		main <- map["main"]
		name <- map["name"]
		sys <- map["sys"]
		weather <- map["weather"]
		wind <- map["wind"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         base = aDecoder.decodeObject(forKey: "base") as? String
         clouds = aDecoder.decodeObject(forKey: "clouds") as? WeatherCloud
         cod = aDecoder.decodeObject(forKey: "cod") as? Int
         coord = aDecoder.decodeObject(forKey: "coord") as? WeatherCoord
         dt = aDecoder.decodeObject(forKey: "dt") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? Int
         main = aDecoder.decodeObject(forKey: "main") as? WeatherMain
         name = aDecoder.decodeObject(forKey: "name") as? String
         sys = aDecoder.decodeObject(forKey: "sys") as? WeatherSy
         weather = aDecoder.decodeObject(forKey: "weather") as? [Weather]
         wind = aDecoder.decodeObject(forKey: "wind") as? WeatherWind

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if base != nil{
			aCoder.encode(base, forKey: "base")
		}
		if clouds != nil{
			aCoder.encode(clouds, forKey: "clouds")
		}
		if cod != nil{
			aCoder.encode(cod, forKey: "cod")
		}
		if coord != nil{
			aCoder.encode(coord, forKey: "coord")
		}
		if dt != nil{
			aCoder.encode(dt, forKey: "dt")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if main != nil{
			aCoder.encode(main, forKey: "main")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if sys != nil{
			aCoder.encode(sys, forKey: "sys")
		}
		if weather != nil{
			aCoder.encode(weather, forKey: "weather")
		}
		if wind != nil{
			aCoder.encode(wind, forKey: "wind")
		}

	}

}