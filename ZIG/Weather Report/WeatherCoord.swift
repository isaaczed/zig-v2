//
//	WeatherCoord.swift


import Foundation 
import ObjectMapper


class WeatherCoord : NSObject, NSCoding, Mappable{

	var lat : Double?
	var lon : Double?


	class func newInstance(map: Map) -> Mappable?{
		return WeatherCoord()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		lat <- map["lat"]
		lon <- map["lon"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         lat = aDecoder.decodeObject(forKey: "lat") as? Double
         lon = aDecoder.decodeObject(forKey: "lon") as? Double

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lon != nil{
			aCoder.encode(lon, forKey: "lon")
		}

	}

}
