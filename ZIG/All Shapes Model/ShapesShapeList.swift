//
//	ShapesShapeList.swift
//
//	Create by Isaac on 25/1/2018
//	Copyright © 2018. All rights reserved.


import Foundation 
import ObjectMapper


class ShapesShapeList : NSObject, NSCoding, Mappable{

	var newshapes : [ShapesNewshape]?
	var routeColor : String?
	var routeID : String?
	var shapeID : String?


	class func newInstance(map: Map) -> Mappable?{
		return ShapesShapeList()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		newshapes <- map["Newshapes"]
		routeColor <- map["RouteColor"]
		routeID <- map["RouteID"]
		shapeID <- map["ShapeID"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         newshapes = aDecoder.decodeObject(forKey: "Newshapes") as? [ShapesNewshape]
         routeColor = aDecoder.decodeObject(forKey: "RouteColor") as? String
         routeID = aDecoder.decodeObject(forKey: "RouteID") as? String
         shapeID = aDecoder.decodeObject(forKey: "ShapeID") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if newshapes != nil{
			aCoder.encode(newshapes, forKey: "Newshapes")
		}
		if routeColor != nil{
			aCoder.encode(routeColor, forKey: "RouteColor")
		}
		if routeID != nil{
			aCoder.encode(routeID, forKey: "RouteID")
		}
		if shapeID != nil{
			aCoder.encode(shapeID, forKey: "ShapeID")
		}

	}

}
