//
//	ShapesNewshape.swift
//
//	Create by Isaac on 25/1/2018
//	Copyright © 2018. All rights reserved.


import Foundation 
import ObjectMapper


class ShapesNewshape : NSObject, NSCoding, Mappable{

	var lat : String?
	var longField : String?
	var seq : Int?
	var shapeId : String?


	class func newInstance(map: Map) -> Mappable?{
		return ShapesNewshape()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		lat <- map["Lat"]
		longField <- map["Long"]
		seq <- map["Seq"]
		shapeId <- map["ShapeId"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         lat = aDecoder.decodeObject(forKey: "Lat") as? String
         longField = aDecoder.decodeObject(forKey: "Long") as? String
         seq = aDecoder.decodeObject(forKey: "Seq") as? Int
         shapeId = aDecoder.decodeObject(forKey: "ShapeId") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if lat != nil{
			aCoder.encode(lat, forKey: "Lat")
		}
		if longField != nil{
			aCoder.encode(longField, forKey: "Long")
		}
		if seq != nil{
			aCoder.encode(seq, forKey: "Seq")
		}
		if shapeId != nil{
			aCoder.encode(shapeId, forKey: "ShapeId")
		}

	}

}
