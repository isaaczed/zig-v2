//
//	ShapesRoot.swift
//
//	Create by Isaac on 25/1/2018
//	Copyright © 2018. All rights reserved.


import Foundation 
import ObjectMapper


class ShapesRoot : NSObject, NSCoding, Mappable{

	var lastUpdated : String?
	var maxCount : Int?
	var message : String?
	var shapeList : [ShapesShapeList]?


	class func newInstance(map: Map) -> Mappable?{
		return ShapesRoot()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		lastUpdated <- map["LastUpdated"]
		maxCount <- map["MaxCount"]
		message <- map["Message"]
		shapeList <- map["ShapeList"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         lastUpdated = aDecoder.decodeObject(forKey: "LastUpdated") as? String
         maxCount = aDecoder.decodeObject(forKey: "MaxCount") as? Int
         message = aDecoder.decodeObject(forKey: "Message") as? String
         shapeList = aDecoder.decodeObject(forKey: "ShapeList") as? [ShapesShapeList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if lastUpdated != nil{
			aCoder.encode(lastUpdated, forKey: "LastUpdated")
		}
		if maxCount != nil{
			aCoder.encode(maxCount, forKey: "MaxCount")
		}
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if shapeList != nil{
			aCoder.encode(shapeList, forKey: "ShapeList")
		}

	}

}
