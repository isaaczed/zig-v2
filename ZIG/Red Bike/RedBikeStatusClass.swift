//
//	RedBikeStatusClass.swift


import Foundation 
import ObjectMapper


class RedBikeStatusClass : NSObject, NSCoding, Mappable{

	var data : RedBikeStatusData?
	var lastUpdated : Int?
	var ttl : Int?


	class func newInstance(map: Map) -> Mappable?{
		return RedBikeStatusClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		data <- map["data"]
		lastUpdated <- map["last_updated"]
		ttl <- map["ttl"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         data = aDecoder.decodeObject(forKey: "data") as? RedBikeStatusData
         lastUpdated = aDecoder.decodeObject(forKey: "last_updated") as? Int
         ttl = aDecoder.decodeObject(forKey: "ttl") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if lastUpdated != nil{
			aCoder.encode(lastUpdated, forKey: "last_updated")
		}
		if ttl != nil{
			aCoder.encode(ttl, forKey: "ttl")
		}

	}

}