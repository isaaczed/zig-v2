//
//	RedBikeStation.swift


import Foundation 
import ObjectMapper


class RedBikeStation : NSObject, NSCoding, Mappable{

	var address : String?
	var lat : Double?
	var lon : Double?
	var name : String?
	var stationId : String?


	class func newInstance(map: Map) -> Mappable?{
		return RedBikeStation()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		address <- map["address"]
		lat <- map["lat"]
		lon <- map["lon"]
		name <- map["name"]
		stationId <- map["station_id"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "address") as? String
         lat = aDecoder.decodeObject(forKey: "lat") as? Double
         lon = aDecoder.decodeObject(forKey: "lon") as? Double
         name = aDecoder.decodeObject(forKey: "name") as? String
         stationId = aDecoder.decodeObject(forKey: "station_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "address")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "lat")
		}
		if lon != nil{
			aCoder.encode(lon, forKey: "lon")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if stationId != nil{
			aCoder.encode(stationId, forKey: "station_id")
		}

	}

}
