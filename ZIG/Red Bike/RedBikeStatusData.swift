//
//	RedBikeStatusData.swift


import Foundation 
import ObjectMapper


class RedBikeStatusData : NSObject, NSCoding, Mappable{

	var stations : [RedBikeStatusStation]?


	class func newInstance(map: Map) -> Mappable?{
		return RedBikeStatusData()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		stations <- map["stations"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         stations = aDecoder.decodeObject(forKey: "stations") as? [RedBikeStatusStation]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if stations != nil{
			aCoder.encode(stations, forKey: "stations")
		}

	}

}
