//
//	RedBikeStatusStation.swift


import Foundation 
import ObjectMapper


class RedBikeStatusStation : NSObject, NSCoding, Mappable{

	var isInstalled : Int?
	var isRenting : Int?
	var isReturning : Int?
	var lastReported : Int?
	var numBikesAvailable : Int?
	var numDocksAvailable : Int?
	var stationId : String?


	class func newInstance(map: Map) -> Mappable?{
		return RedBikeStatusStation()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		isInstalled <- map["is_installed"]
		isRenting <- map["is_renting"]
		isReturning <- map["is_returning"]
		lastReported <- map["last_reported"]
		numBikesAvailable <- map["num_bikes_available"]
		numDocksAvailable <- map["num_docks_available"]
		stationId <- map["station_id"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         isInstalled = aDecoder.decodeObject(forKey: "is_installed") as? Int
         isRenting = aDecoder.decodeObject(forKey: "is_renting") as? Int
         isReturning = aDecoder.decodeObject(forKey: "is_returning") as? Int
         lastReported = aDecoder.decodeObject(forKey: "last_reported") as? Int
         numBikesAvailable = aDecoder.decodeObject(forKey: "num_bikes_available") as? Int
         numDocksAvailable = aDecoder.decodeObject(forKey: "num_docks_available") as? Int
         stationId = aDecoder.decodeObject(forKey: "station_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if isInstalled != nil{
			aCoder.encode(isInstalled, forKey: "is_installed")
		}
		if isRenting != nil{
			aCoder.encode(isRenting, forKey: "is_renting")
		}
		if isReturning != nil{
			aCoder.encode(isReturning, forKey: "is_returning")
		}
		if lastReported != nil{
			aCoder.encode(lastReported, forKey: "last_reported")
		}
		if numBikesAvailable != nil{
			aCoder.encode(numBikesAvailable, forKey: "num_bikes_available")
		}
		if numDocksAvailable != nil{
			aCoder.encode(numDocksAvailable, forKey: "num_docks_available")
		}
		if stationId != nil{
			aCoder.encode(stationId, forKey: "station_id")
		}

	}

}
