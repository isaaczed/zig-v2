//
//	RedBikeData.swift


import Foundation 
import ObjectMapper


class RedBikeData : NSObject, NSCoding, Mappable{

	var stations : [RedBikeStation]?


	class func newInstance(map: Map) -> Mappable?{
		return RedBikeData()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		stations <- map["stations"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         stations = aDecoder.decodeObject(forKey: "stations") as? [RedBikeStation]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if stations != nil{
			aCoder.encode(stations, forKey: "stations")
		}

	}

}