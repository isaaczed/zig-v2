//
//	GetResult.swift


import Foundation 
import ObjectMapper


class GetResult : NSObject, NSCoding, Mappable{

	var agency : String?
	var message : AnyObject?
	var profileModels : [GetProfileModel]?
	var status : Bool?
	var username : String?


	class func newInstance(map: Map) -> Mappable?{
		return GetResult()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		agency <- map["Agency"]
		message <- map["Message"]
		profileModels <- map["ProfileModels"]
		status <- map["Status"]
		username <- map["Username"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agency = aDecoder.decodeObject(forKey: "Agency") as? String
         message = aDecoder.decodeObject(forKey: "Message") as? AnyObject
         profileModels = aDecoder.decodeObject(forKey: "ProfileModels") as? [GetProfileModel]
         status = aDecoder.decodeObject(forKey: "Status") as? Bool
         username = aDecoder.decodeObject(forKey: "Username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if agency != nil{
			aCoder.encode(agency, forKey: "Agency")
		}
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if profileModels != nil{
			aCoder.encode(profileModels, forKey: "ProfileModels")
		}
		if status != nil{
			aCoder.encode(status, forKey: "Status")
		}
		if username != nil{
			aCoder.encode(username, forKey: "Username")
		}

	}

}