//
//    ProfileModel.swift
//
//    Create by Isaac on 24/5/2018
//    Copyright © 2018. All rights reserved.


import Foundation
import ObjectMapper


class GetProfileModel : NSObject, NSCoding, Mappable{
    
    var category : String?
    var docuemntName : String?
    var emailId : String?
    var masterId : AnyObject?
    var name : String?
    var phoneNumber : String?
    var Photolink : String?
    var profileId : Int?
    var isactive : Bool?
    var Default : Bool?

    
    class func newInstance(map: Map) -> Mappable?{
        return GetProfileModel()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        category <- map["Category"]
        docuemntName <- map["DocuemntName"]
        emailId <- map["EmailId"]
        masterId <- map["MasterId"]
        name <- map["Name"]
        phoneNumber <- map["PhoneNumber"]
        Photolink <- map["Photolink"]
        profileId <- map["ProfileId"]
        isactive <- map["isactive"]
        Default <- map["Default"]

    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        category = aDecoder.decodeObject(forKey: "Category") as? String
        docuemntName = aDecoder.decodeObject(forKey: "DocuemntName") as? String
        emailId = aDecoder.decodeObject(forKey: "EmailId") as? String
        masterId = aDecoder.decodeObject(forKey: "MasterId") as? AnyObject
        name = aDecoder.decodeObject(forKey: "Name") as? String
        phoneNumber = aDecoder.decodeObject(forKey: "PhoneNumber") as? String
        Photolink = aDecoder.decodeObject(forKey: "Photolink") as? String
        profileId = aDecoder.decodeObject(forKey: "ProfileId") as? Int
        isactive = aDecoder.decodeObject(forKey: "isactive") as? Bool
        Default = aDecoder.decodeObject(forKey: "Default") as? Bool

    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if category != nil{
            aCoder.encode(category, forKey: "Category")
        }
        if docuemntName != nil{
            aCoder.encode(docuemntName, forKey: "DocuemntName")
        }
        if emailId != nil{
            aCoder.encode(emailId, forKey: "EmailId")
        }
        if masterId != nil{
            aCoder.encode(masterId, forKey: "MasterId")
        }
        if name != nil{
            aCoder.encode(name, forKey: "Name")
        }
        if phoneNumber != nil{
            aCoder.encode(phoneNumber, forKey: "PhoneNumber")
        }
        if Photolink != nil{
            aCoder.encode(Photolink, forKey: "Photolink")
        }
        if profileId != nil{
            aCoder.encode(profileId, forKey: "ProfileId")
        }
        if isactive != nil{
            aCoder.encode(isactive, forKey: "isactive")
        }
        
    }
    
}
