//
//	GetProfile.swift


import Foundation 
import ObjectMapper


class GetProfile : NSObject, NSCoding, Mappable{

	var message : String?
	var result : [GetResult]?


	class func newInstance(map: Map) -> Mappable?{
		return GetProfile()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		message <- map["Message"]
		result <- map["Result"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "Message") as? String
         result = aDecoder.decodeObject(forKey: "Result") as? [GetResult]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if result != nil{
			aCoder.encode(result, forKey: "Result")
		}

	}

}