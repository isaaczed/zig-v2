//
//	RootClass.swift
//
//	Create by Isaac on 4/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class RootClass : NSObject, NSCoding, Mappable{

	var htmlAttributions : [AnyObject]?
	var nextPageToken : String?
	var results : [Result]?
	var status : String?


	class func newInstance(map: Map) -> Mappable?{
		return RootClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		htmlAttributions <- map["html_attributions"]
		nextPageToken <- map["next_page_token"]
		results <- map["results"]
		status <- map["status"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         htmlAttributions = aDecoder.decodeObject(forKey: "html_attributions") as? [AnyObject]
         nextPageToken = aDecoder.decodeObject(forKey: "next_page_token") as? String
         results = aDecoder.decodeObject(forKey: "results") as? [Result]
         status = aDecoder.decodeObject(forKey: "status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if htmlAttributions != nil{
			aCoder.encode(htmlAttributions, forKey: "html_attributions")
		}
		if nextPageToken != nil{
			aCoder.encode(nextPageToken, forKey: "next_page_token")
		}
		if results != nil{
			aCoder.encode(results, forKey: "results")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
