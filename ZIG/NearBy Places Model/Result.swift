//
//	Result.swift
//
//	Create by Isaac on 4/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class Result : NSObject, NSCoding, Mappable{

	var geometry : Geometry?
	var icon : String?
	var id : String?
	var name : String?
	var openingHours : OpeningHour?
	var photos : [Photo]?
	var placeId : String?
	var priceLevel : Int?
	var rating : Double?
	var reference : String?
	var scope : String?
	var types : [String]?
	var vicinity : String?


	class func newInstance(map: Map) -> Mappable?{
		return Result()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		geometry <- map["geometry"]
		icon <- map["icon"]
		id <- map["id"]
		name <- map["name"]
		openingHours <- map["opening_hours"]
		photos <- map["photos"]
		placeId <- map["place_id"]
		priceLevel <- map["price_level"]
		rating <- map["rating"]
		reference <- map["reference"]
		scope <- map["scope"]
		types <- map["types"]
		vicinity <- map["vicinity"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         geometry = aDecoder.decodeObject(forKey: "geometry") as? Geometry
         icon = aDecoder.decodeObject(forKey: "icon") as? String
         id = aDecoder.decodeObject(forKey: "id") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         openingHours = aDecoder.decodeObject(forKey: "opening_hours") as? OpeningHour
         photos = aDecoder.decodeObject(forKey: "photos") as? [Photo]
         placeId = aDecoder.decodeObject(forKey: "place_id") as? String
         priceLevel = aDecoder.decodeObject(forKey: "price_level") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? Double
         reference = aDecoder.decodeObject(forKey: "reference") as? String
         scope = aDecoder.decodeObject(forKey: "scope") as? String
         types = aDecoder.decodeObject(forKey: "types") as? [String]
         vicinity = aDecoder.decodeObject(forKey: "vicinity") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if geometry != nil{
			aCoder.encode(geometry, forKey: "geometry")
		}
		if icon != nil{
			aCoder.encode(icon, forKey: "icon")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if openingHours != nil{
			aCoder.encode(openingHours, forKey: "opening_hours")
		}
		if photos != nil{
			aCoder.encode(photos, forKey: "photos")
		}
		if placeId != nil{
			aCoder.encode(placeId, forKey: "place_id")
		}
		if priceLevel != nil{
			aCoder.encode(priceLevel, forKey: "price_level")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}
		if reference != nil{
			aCoder.encode(reference, forKey: "reference")
		}
		if scope != nil{
			aCoder.encode(scope, forKey: "scope")
		}
		if types != nil{
			aCoder.encode(types, forKey: "types")
		}
		if vicinity != nil{
			aCoder.encode(vicinity, forKey: "vicinity")
		}

	}

}
