//
//	Photo.swift
//
//	Create by Isaac on 4/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class Photo : NSObject, NSCoding, Mappable{

	var height : Int?
	var htmlAttributions : [String]?
	var photoReference : String?
	var width : Int?


	class func newInstance(map: Map) -> Mappable?{
		return Photo()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		height <- map["height"]
		htmlAttributions <- map["html_attributions"]
		photoReference <- map["photo_reference"]
		width <- map["width"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         height = aDecoder.decodeObject(forKey: "height") as? Int
         htmlAttributions = aDecoder.decodeObject(forKey: "html_attributions") as? [String]
         photoReference = aDecoder.decodeObject(forKey: "photo_reference") as? String
         width = aDecoder.decodeObject(forKey: "width") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if height != nil{
			aCoder.encode(height, forKey: "height")
		}
		if htmlAttributions != nil{
			aCoder.encode(htmlAttributions, forKey: "html_attributions")
		}
		if photoReference != nil{
			aCoder.encode(photoReference, forKey: "photo_reference")
		}
		if width != nil{
			aCoder.encode(width, forKey: "width")
		}

	}

}
