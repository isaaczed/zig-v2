//
//	OpeningHour.swift
//
//	Create by Isaac on 4/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class OpeningHour : NSObject, NSCoding, Mappable{

	var openNow : Bool?
	var weekdayText : [AnyObject]?


	class func newInstance(map: Map) -> Mappable?{
		return OpeningHour()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		openNow <- map["open_now"]
		weekdayText <- map["weekday_text"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         openNow = aDecoder.decodeObject(forKey: "open_now") as? Bool
         weekdayText = aDecoder.decodeObject(forKey: "weekday_text") as? [AnyObject]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if openNow != nil{
			aCoder.encode(openNow, forKey: "open_now")
		}
		if weekdayText != nil{
			aCoder.encode(weekdayText, forKey: "weekday_text")
		}

	}

}
