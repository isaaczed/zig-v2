//
//	Geometry.swift
//
//	Create by Isaac on 4/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class Geometry : NSObject, NSCoding, Mappable{

	var location : Location?
	var viewport : Viewport?


	class func newInstance(map: Map) -> Mappable?{
		return Geometry()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		location <- map["location"]
		viewport <- map["viewport"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         location = aDecoder.decodeObject(forKey: "location") as? Location
         viewport = aDecoder.decodeObject(forKey: "viewport") as? Viewport

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if viewport != nil{
			aCoder.encode(viewport, forKey: "viewport")
		}

	}

}
