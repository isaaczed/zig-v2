//
//	GetModel.swift


import Foundation 
import ObjectMapper


class GetModel : NSObject, NSCoding, Mappable{

	var agencyId : Int?
	var agencyname : String?
	var categortId : Int?
	var categoryname : String?
	var createby : String?
	var createdate : String?
	var isactive : Bool?
	var lastUpdatedby : String?
	var lastUpdateddate : String?


	class func newInstance(map: Map) -> Mappable?{
		return GetModel()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		agencyId <- map["AgencyId"]
		agencyname <- map["Agencyname"]
		categortId <- map["CategortId"]
		categoryname <- map["Categoryname"]
		createby <- map["Createby"]
		createdate <- map["Createdate"]
		isactive <- map["Isactive"]
		lastUpdatedby <- map["LastUpdatedby"]
		lastUpdateddate <- map["LastUpdateddate"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         agencyId = aDecoder.decodeObject(forKey: "AgencyId") as? Int
         agencyname = aDecoder.decodeObject(forKey: "Agencyname") as? String
         categortId = aDecoder.decodeObject(forKey: "CategortId") as? Int
         categoryname = aDecoder.decodeObject(forKey: "Categoryname") as? String
         createby = aDecoder.decodeObject(forKey: "Createby") as? String
         createdate = aDecoder.decodeObject(forKey: "Createdate") as? String
         isactive = aDecoder.decodeObject(forKey: "Isactive") as? Bool
         lastUpdatedby = aDecoder.decodeObject(forKey: "LastUpdatedby") as? String
         lastUpdateddate = aDecoder.decodeObject(forKey: "LastUpdateddate") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if agencyId != nil{
			aCoder.encode(agencyId, forKey: "AgencyId")
		}
		if agencyname != nil{
			aCoder.encode(agencyname, forKey: "Agencyname")
		}
		if categortId != nil{
			aCoder.encode(categortId, forKey: "CategortId")
		}
		if categoryname != nil{
			aCoder.encode(categoryname, forKey: "Categoryname")
		}
		if createby != nil{
			aCoder.encode(createby, forKey: "Createby")
		}
		if createdate != nil{
			aCoder.encode(createdate, forKey: "Createdate")
		}
		if isactive != nil{
			aCoder.encode(isactive, forKey: "Isactive")
		}
		if lastUpdatedby != nil{
			aCoder.encode(lastUpdatedby, forKey: "LastUpdatedby")
		}
		if lastUpdateddate != nil{
			aCoder.encode(lastUpdateddate, forKey: "LastUpdateddate")
		}

	}

}