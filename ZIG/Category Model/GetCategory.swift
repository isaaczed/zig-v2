//
//	GetCategory.swift


import Foundation 
import ObjectMapper


class GetCategory : NSObject, NSCoding, Mappable{

	var message : String?
	var model : [GetModel]?


	class func newInstance(map: Map) -> Mappable?{
		return GetCategory()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		message <- map["Message"]
		model <- map["Model"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "Message") as? String
         model = aDecoder.decodeObject(forKey: "Model") as? [GetModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if model != nil{
			aCoder.encode(model, forKey: "Model")
		}

	}

}