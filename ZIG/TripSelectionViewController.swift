//
//  TripSelectionViewController.swift
//  ZIG
//
//  Created by Isaac on 28/11/17.
//  Copyright © 2017 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import UberRides
import CoreLocation
import UberRides
import XLActionController
import LyftSDK
import UberCore
import NYAlertViewController
import EventKit
import Foundation
import NVActivityIndicatorView
import SwiftyJSON
import SafariServices
import Firebase
import MapKit

struct SourceDestination {
    static var SourceAddress: String?
    static var DestinationAddress: String?
    static var firstMile: String?
    static var LastMileList: String?
    static var boltListApi: [Bolt_list]?
    static var birdListApi: [data_list]?
    static var LimeListApi: [Lime_list]?
    static var BirdStationDistance: Double?
    static var BoltStationDistance: Double?
    static var LimeStationDistance: Double?
    static var DisplayType: String?

}


class TripSelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, LoginButtonDelegate {
    var googlereponseDirection = [[String: Any]]()
    let cellReuseIdentifier = "cell"
    var selectProfileTableView = UITableView()
    var fareListTableView: UITableView = UITableView()
    var skippedArray: NSMutableArray = []
    var backgroundBlurView = UIView()
    var backgroundBlurView1 = UIView()
    var paymentBackView = UIView()
    var sourceString: String = ""
    var destinationString: String = ""
    var sourceLat: Double = 0.0
    var sourceLng: Double = 0.0
    var destinationLat: Double = 0.0
    var destinationLng: Double = 0.0
    var totalTransitOption = 0
       var louveloResponse: DataResponse<getlouveloData>!
    var isBuyAll = false
    var realtimeDataResponse: DataResponse<TransitRouteClass>!
    var profileDataList: DataResponse<GetProfile>!
    var submitBtn = UIButton()
    var indexTicketValue = 0
    var modetype: String?
    var isBikeAvailable: Bool = false
    var isUberAdded: Bool = false
    var uberAddedIndex = 0
    let selectedProfileArray: NSMutableArray = []
    var selectProfileArray: NSMutableArray = []
    let selectProfileBackView = UIView()
    var uberSecIndex = ""
    var tripSchduleTime = Date()
    var preferencesBackView = UIView()
    var totalFareLabel = UILabel()
    var ameniticsTitle: String?
    // var TotalMyTarcFareLabel = UILabel()
    var isMenuClicked = true
    var dataRespone: DataResponse<SuggestDirectionaClass>!
    var DataWalkingResponse: DataResponse<SuggestWalkingDirectionalClass>!
    var LimeDataWalkingResponse: DataResponse<SuggestWalkingDirectionalClass>!
    var BoltDataWalkingResponse: DataResponse<SuggestWalkingDirectionalClass>!

    var dataBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var LimedataBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var BoltdataBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var dataGoogleResponse: DataResponse<SuggestBikeDirectionalClass>!
    var DataWalkingResponseLouveloStart: DataResponse<SuggestWalkingDirectionalClass>!
    var DataWalkingResponseLouveloEnd: DataResponse<SuggestWalkingDirectionalClass>!
    var louveloBikeResponse: DataResponse<SuggestBikeDirectionalClass>!

    let loggedIn = TokenManager.fetchToken()
    var AmenititesTypeTrip: String?
    //var datawalking = [WalkingDetails]()
    //var bikelist_fromAPI = [Bikelist]()
    var data_list_fromAPI = [data_list]()
    var menuBackView = UIView()
    var menuBackView1 = UIView()
    var fareView = UIView()
    var buyNowbtn = UIButton()
    var buyAllBtn = UIButton()
    var menuBackTicketView = UIView()
    let buscheckbox = UISwitch()
    let traincheckbox = UISwitch()
    let bikebuscheckbox = UISwitch()
    let cabbuscheckbox = UISwitch()
    var blurEffectView = UIVisualEffectView()
    let eventsScheduleArray: NSMutableArray = []
    var menuTabelView: UITableView = UITableView()
    var menuTicketTableView: UITableView = UITableView()
    var sugesstionTableView: UITableView = UITableView()
    var isCalenderEvent = false
    var startStop = String()
    var stopStop = String()
    var distanceString = String()
    var timeString = String()
    var busNameStrin = String()
    var busAgency = ""
    var filterOptionIndex = 0
    var busImageUrl = String()
    var stepArray: NSMutableArray = []
    var loaderView = UIView()
    var ticketMenuX = 0, ticketMenuY = 0, ticketMenuWidth = 0, ticketMenuHeight = 0
    var closeBtnWidth = 0, profileImageSize = 0, ticketTableviewHeight = 0, uberBtnWidth = 0, uberButtonX = 0
    var ticketDetails: NSMutableDictionary = [:]
    let addedProfileID: NSMutableArray = []
    var selectProfileLbl = UILabel()
    var selectProfileDropDown = UILabel()
    var BirdBikeID = ""
    var BirdBikeLat: Double?
    var BirdBikeLon: Double?

    var LimeBikeLat: Double?
    var LimeBikeLon: Double?
    var LimeDatalistapicount: Int?

    var BoltBikeLat: Double?
    var BoltBikeLon: Double?
    var BoltDatalistapicount: Int?


    var BirdIsReserved = 0
    var BirdIsDisabled = 0
    var BirdSourceDistance = 0.0
    var BirdtempDistance = 0.0
    var BikedistanceInMeters: Double?
    var LimedistanceInMeters: Double?
    var BoltdistanceInMeters: Double?

    var bikeCostForAnalytics: Int?
    var templat = 0.0
    var templong = 0.0
    var birddistanceInMeters = 0.0
    var WalkingDistance = ""
    var WalkingDuration = 0
    var LimeWalkingDistance = ""
    var LimeWalkingDuration = 0

    var BoltWalkingDistance = ""
    var BoltWalkingDuration = 0

    var sourceLouveloStationID: Int?
    var SourceLouveloLat: Double?
    var SourceLouveloLong: Double?
    var SourceLouveloAddress: String?
    var SourceLouveloDistance = 0.0

    var DestinationLouveloStationID: Int?
    var DestinationLouveloLat: Double?
    var DestinationLouveloLong: Double?
    var DestinationLouveloAddress: String?
    var DestinationloDistance = 0.0
    var BirdDatalistapicount: Int?

    var tempSourceLouveloLat: Double?
    var tempSourceLouveloLong: Double?

    var tempSourceLouveloDistance = 0.0
    var fareStringEXP = 2.75
    var fareStringExpMyCard = 2.50
    var TotalFar = 0.0
    var MycardTotalFare = 0.0
    var MycardTotalFareArray: [Double] = [0.0]
    var FareStringLocal = 1.75
    var fareStringLocalMycard = 1.50
    var NewfareString = ""
    var newMytarcFareString = ""
    var SourceAddress = ""
    var DestinationAddress = ""
    var tempDestinationLouveloLat: Double?
    var tempDestinationLouveloLong: Double?

    var ridetype = [" ", "Local", "Express"]
    var NormalPrice = ["Regular", "$1.75", "$2.75"]
    var TarcCardlPrice = ["TARC Ticket", "$1.50", "$2.50"]
    var isCurrentLoc: Bool?
    var tempDestinationloDistance = 0.0
    var sourceaddressString: String?
    var DestinationAddressString: String?
    var transitType_Analytics: [String]?
    let addTickeValue: NSMutableArray = []
    var boltListApi: [Bolt_list]?
    var birdListApi: [data_list]?
    var LimeListApi: [Lime_list]?

    var cabBool = Bool()
    var bikeBool = Bool()



    override func viewWillAppear(_ animated: Bool) {
        print(destinationString)

    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
        self.hideLoader()
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height + 120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!

            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width / 2) - 40, y: (self.view.frame.size.height / 2) - 40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        SourceDestination.SourceAddress = sourceaddressString
        SourceDestination.DestinationAddress = DestinationAddressString

        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
        getlouvelo()
        let userDefaults1 = UserDefaults.standard
        let accessToken = userDefaults1.value(forKey: "accessToken")
        // let tokenString = accessToken as! String
        print("Access Token: \((accessToken)!)")
        let url = "\(APIUrl.ZIGTARCAPI)User/Profilelist"
        let parameters: Parameters = [
            "token": accessToken!,

        ]
        if userDefaults1.value(forKey: "userName") == nil || userDefaults1.value(forKey: "ProfileID") == nil ||  userDefaults1.value(forKey: "phoneNumber") == nil
                   {

        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<GetProfile>) in
                switch response.result {

                case .success:
                    let userDefaults1 = UserDefaults.standard
                    if response.result.value?.message == "Ok"
                        {


                        self.profileDataList = response

                        userDefaults1.set(self.profileDataList.result.value?.result![0].username!, forKey: "userName")
                        userDefaults1.synchronize()
                        for profileModel in (self.profileDataList.result.value?.result![0].profileModels)!
                        {
                            if profileModel.Default == true{
                                
                            userDefaults1.set(profileModel.emailId, forKey: "userEmail")
                            userDefaults1.set(profileModel.profileId, forKey: "ProfileID")
                            userDefaults1.set(profileModel.phoneNumber, forKey: "phoneNumber")

                            if profileModel.Photolink?.range(of: "https://") != nil
                                {
                                // self.photolinkUser = profileModel.Photolink!
                                userDefaults1.set(profileModel.Photolink!, forKey: "userPhoto")
                                userDefaults1.synchronize()
                                //  profileImage.downloadedFrom(link: profileModel.Photolink!)
                            }
                            else
                            {
                                userDefaults1.set("", forKey: "userPhoto")
                                userDefaults1.synchronize()
                                //  profileImage.image = UIImage(named:"male-user-profile-picture")
                            }
                        }
                            }
                        //  self.createUI()
                    }
                    else
                    {
                        userDefaults1.set("", forKey: "userName")
                        userDefaults1.set("", forKey: "userPhoto")
                        userDefaults1.synchronize()
                        print(response.result.value?.message!)
                        //  self.showErrorDialogBox(viewController: self)
                        //  self.showAlertBox(text: response.result.value!.message!)

                    }
                case .failure(let error):
                    print(error)
                    let userDefaults1 = UserDefaults.standard
                    userDefaults1.set("", forKey: "userName")
                    userDefaults1.set("", forKey: "userPhoto")
                    userDefaults1.synchronize()
                    // self.showErrorDialogBox(viewController: self)
                    print(error.localizedDescription)
                    self.view.makeToast("Server Error! Please try again later!")
                }

        }
    }
        if userDefaults1.integer(forKey: "userIdString") == nil {
         print("new")
         let accessToken = userDefaults1.value(forKey: "accessToken")
        
         let getUserId = "\(APIUrl.ZIGTARCAPI)User/api/GetUserId?Token=\(accessToken!)"

         Alamofire.request(getUserId, method: .post, parameters: nil, encoding: JSONEncoding.default)
                                   .responseJSON { response in
                                       switch response.result {
                                       case .success:
                                           
                                           
                                           if (response.result.value != nil) {
                                               
                                             let responseVar = response.result.value
                                             let json = JSON(responseVar!)
                                             print(json.rawString()!)
                                             let useridString = json.rawString()! as! String
                                             let userid = Int(useridString)
                                             let userDefaults = UserDefaults.standard
                                              userDefaults.set(userid, forKey: "userIdString")

                                               
                                           }
                                       case .failure(let error):
                                           print(error)
                                          
                                          //
                                                                                          
                                      
                                         
                                          //  self.hideLoader()
                                       }
                               }
             }
        
        analytics.GetPageHitCount(PageName: "Suggested Route")

        // let userDefaults1 = UserDefaults.standard
        userDefaults1.set("true", forKey: "isBus")
        userDefaults1.set("true", forKey: "isBike")
        //Arun 23/july
        // userDefaults1.set("true", forKey: "isCab")
        if userDefaults1.object(forKey: "isUber") == nil {
            userDefaults1.set("true", forKey: "isUber")
        }
        var uberBool = Bool()
        var isLyft = Bool()
        var firstMailCarBool = Bool()


        uberBool = userDefaults1.bool(forKey: "isUber")
        isLyft = userDefaults1.bool(forKey: "isLyft")
        firstMailCarBool = userDefaults1.bool(forKey: "Cabshare")
        cabBool = userDefaults1.bool(forKey: "Rideshare")
        bikeBool = userDefaults1.bool(forKey: "Bike")

        if cabBool != nil
            {

        }
        else
        {
            cabBool = false

        }

        if bikeBool != nil
            {

        }
        else
        {
            bikeBool = false

        }
        if firstMailCarBool != nil
            {

        }
        else
        {
            firstMailCarBool = false

        }
        // if uberBool == false && isLyft == false
        if firstMailCarBool == false
            {
            userDefaults1.set("false", forKey: "isCab")

        }
        else
        {
            userDefaults1.set("true", forKey: "isCab")

        }
        userDefaults1.synchronize()

        print("\(sourceLat),\(destinationLng)")
        print("\(sourceString),\(destinationString)")
        //ArunP
        // getlouvelo()
        self.menuBackView1.backgroundColor = UIColor.white
        self.menuBackView.backgroundColor = UIColor.white

        if UIScreen.main.sizeType == .iPhone4 {
            ticketMenuX = 0
            ticketMenuY = 0
            ticketMenuWidth = 0
            ticketMenuHeight = 0
            closeBtnWidth = 40
            profileImageSize = 40
            ticketTableviewHeight = 154
            uberBtnWidth = 260
        } else if UIScreen.main.sizeType == .iPhone5 {
            ticketMenuX = 20
            ticketMenuY = 30
            ticketMenuWidth = 40
            ticketMenuHeight = 150
            closeBtnWidth = 40
            profileImageSize = 40
            ticketTableviewHeight = 154
            uberBtnWidth = 260
        } else if UIScreen.main.sizeType == .iPhone6 {

            ticketMenuX = 40
            ticketMenuY = 40
            ticketMenuWidth = 80
            ticketMenuHeight = 160
            closeBtnWidth = 50
            profileImageSize = 60
            ticketTableviewHeight = 180
            uberBtnWidth = 260
            uberButtonX = 5
        }

        else if UIScreen.main.sizeType == .iPhone6Plus {

            ticketMenuX = 40
            ticketMenuY = 40
            ticketMenuWidth = 80
            ticketMenuHeight = 200
            closeBtnWidth = 60
            profileImageSize = 60
            ticketTableviewHeight = 200
            uberBtnWidth = 280
            uberButtonX = 20
        }
        else if UIScreen.main.nativeBounds.height == 1920 {
            print("iPhone 6plus or 7plus or 8plus")
            ticketMenuX = 40
            ticketMenuY = 40
            ticketMenuWidth = 80
            ticketMenuHeight = 200
            closeBtnWidth = 60
            profileImageSize = 60
            ticketTableviewHeight = 200
            uberBtnWidth = 280
            uberButtonX = 20

        }
        else if UIScreen.main.nativeBounds.height == 2688 {
            print("iPhone XS max")
            ticketMenuX = 40
            ticketMenuY = 40
            ticketMenuWidth = 80
            ticketMenuHeight = 200
            closeBtnWidth = 70
            profileImageSize = 60
            ticketTableviewHeight = 210
            uberBtnWidth = 260
            uberButtonX = 10
        }
        else if UIScreen.main.nativeBounds.height == 1792 {
            print("iPhone Xr max")
            ticketMenuX = 40
            ticketMenuY = 40
            ticketMenuWidth = 80
            ticketMenuHeight = 200
            closeBtnWidth = 70
            profileImageSize = 60
            ticketTableviewHeight = 210
            uberBtnWidth = 260
            uberButtonX = 10

        }

        else if UIScreen.main.sizeType == .iPadAir2andiPadMini4 {

            ticketMenuX = 200
            ticketMenuY = 250
            ticketMenuWidth = 300
            ticketMenuHeight = 500
            closeBtnWidth = 40
            profileImageSize = 40
            ticketTableviewHeight = 250
            uberBtnWidth = 260
        } else if UIScreen.main.sizeType == .iPadPro105 {
            ticketMenuX = 200
            ticketMenuY = 250
            ticketMenuWidth = 300
            ticketMenuHeight = 500
            closeBtnWidth = 40
            profileImageSize = 40
            ticketTableviewHeight = 250
            uberBtnWidth = 260
        } else if UIScreen.main.sizeType == .iPadPro129 {

            ticketMenuX = 200
            ticketMenuY = 250
            ticketMenuWidth = 300
            ticketMenuHeight = 500
            closeBtnWidth = 70
            profileImageSize = 60
            ticketTableviewHeight = 200
            uberBtnWidth = 260
            uberButtonX = 10

        } else if UIScreen.main.sizeType == .iPhoneX {
            ticketMenuX = 40
            ticketMenuY = 40
            ticketMenuWidth = 80
            ticketMenuHeight = 200
            closeBtnWidth = 70
            profileImageSize = 60
            ticketTableviewHeight = 210
            uberBtnWidth = 260
            uberButtonX = 10
        }



        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.title = " "
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
            style: UIBarButtonItemStyle.done,
            target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        let menuButton = UIBarButtonItem(image: UIImage(named: "FilterMenu"),
            style: UIBarButtonItemStyle.plain,
            target: self, action: #selector(OnMenuClicked))
        self.navigationItem.rightBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated: true);

        preferencesBackView.frame = CGRect.init(x: 30, y: -2000, width: self.view.frame.size.width - 60, height: self.view.frame.size.height - 100)
        preferencesBackView.backgroundColor = UIColor.white;
        self.reloadDataInUI()



        if UIDevice.current.userInterfaceIdiom == .pad
            {
            menuBackTicketView.frame = CGRect.init(x: (self.view.frame.size.width / 2) - 200, y: -2000, width: 400, height: 500)
            paymentBackView.frame = CGRect.init(x: (self.view.frame.size.width / 2) - 200, y: -2000, width: 400, height: 500)

        }
        else
        {
            menuBackTicketView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.width - 80, height: self.view.frame.height - 120)
            paymentBackView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.width - 80, height: self.view.frame.height - 120)

        }



        paymentBackView.backgroundColor = UIColor.white
        // self.view.addSubview(paymentBackView)
        paymentBackView.layer.cornerRadius = 12.0



        // Do any additional setup after loading the view.
    }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    //latitude and longitude
    func getlouvelo() {
        var dateInSeconds: TimeInterval
        dateInSeconds = tripSchduleTime.timeIntervalSince1970
        // print(<#T##items: Any...##Any#>)
        let SourceLatLong = CLLocation(latitude: self.sourceLat, longitude: self.sourceLng)
        let DestinationLatLong = CLLocation(latitude: self.destinationLat, longitude: self.destinationLng)
        var CheckBikeCount = 0
        print(SourceLatLong)
        print(DestinationLatLong)
        Alamofire.request("https://lou.publicbikesystem.net/ube/gbfs/v1/en/station_information").responseObject { (response: DataResponse<getlouveloData>) in
            switch response.result {
            case .success:

                if response.result.value != nil
                    {
                    let louData = response.result.value!
                          self.louveloResponse = response
                    for louDataArraay in louData.louveldata! {
                        DispatchQueue.main.async {
                            if CheckBikeCount == 0 {
                                self.sourceLouveloStationID = louDataArraay.StationID
                                self.SourceLouveloLat = louDataArraay.StationLat
                                self.SourceLouveloLong = louDataArraay.StationLong
                                self.DestinationLouveloStationID = louDataArraay.StationID
                                self.DestinationLouveloAddress = louDataArraay.StationName
                                self.DestinationLouveloLat = louDataArraay.StationLat
                                self.DestinationLouveloLong = louDataArraay.StationLong
                                self.SourceLouveloAddress = louDataArraay.StationName

                                print(self.SourceLouveloAddress!)

                                let SourceDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)
                                let DestinationDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)

                                self.SourceLouveloDistance = SourceLatLong.distance(from: SourceDistanceFinder)
                                self.SourceLouveloDistance = (self.SourceLouveloDistance * 100).rounded() / 100


                                self.DestinationloDistance = DestinationLatLong.distance(from: DestinationDistanceFinder)
                                self.DestinationloDistance = (self.DestinationloDistance * 100).rounded() / 100

                                CheckBikeCount = CheckBikeCount + 1


                            }
                            else {
                                self.tempSourceLouveloLat = louDataArraay.StationLat
                                self.tempSourceLouveloLong = louDataArraay.StationLong


                                self.tempDestinationLouveloLat = louDataArraay.StationLat
                                self.tempDestinationLouveloLong = louDataArraay.StationLong

                                let SourceDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)
                                let DestinationDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)


                                self.tempSourceLouveloDistance = SourceLatLong.distance(from: SourceDistanceFinder)
                                self.tempSourceLouveloDistance = (self.tempSourceLouveloDistance * 100).rounded() / 100


                                self.tempDestinationloDistance = DestinationLatLong.distance(from: DestinationDistanceFinder)
                                self.tempDestinationloDistance = (self.tempDestinationloDistance * 100).rounded() / 100

                                if self.tempSourceLouveloDistance < self.SourceLouveloDistance {

                                    self.sourceLouveloStationID = louDataArraay.StationID
                                    self.SourceLouveloLat = louDataArraay.StationLat
                                    self.SourceLouveloLong = louDataArraay.StationLong
                                    self.SourceLouveloDistance = self.tempSourceLouveloDistance
                                    self.SourceLouveloAddress = louDataArraay.StationName
                                    print(self.SourceLouveloAddress!)



                                }
                                else if self.tempDestinationloDistance < self.DestinationloDistance {
                                    self.DestinationLouveloStationID = louDataArraay.StationID
                                    self.DestinationLouveloLat = louDataArraay.StationLat
                                    self.DestinationLouveloLong = louDataArraay.StationLong
                                    self.DestinationloDistance = self.tempDestinationloDistance
                                    self.DestinationLouveloAddress = louDataArraay.StationName

                                }

                                CheckBikeCount = CheckBikeCount + 1



                            }
                            print("\(self.DestinationLouveloLat!) ,\(self.DestinationLouveloLong!) Destination distance - \(self.DestinationloDistance)")
                            print("\(self.SourceLouveloLat!) ,\(self.SourceLouveloLong!) Source distance - \(self.SourceLouveloDistance)")

                            let indexPath = IndexPath(item: 0, section: 2)
                            self.sugesstionTableView.reloadData()
                            // self.sugesstionTableView.reloadRows(at: [indexPath], with: .automatic)














                        }

                    }
                        let delay = 2.0
                                          DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: {
                        self.hideLoader()
                        })

                }
                else
                {
                     self.hideLoader()
                }
            case .failure(let error):
                print(error)
                self.hideLoader()

            }

        }

    }
    func reloadDataInUI() {
        sugesstionTableView.removeFromSuperview()
        sugesstionTableView = UITableView()
        var url = ""
        var parameters = Parameters()
        skippedArray.removeAllObjects()




        var dateInSeconds: TimeInterval
        dateInSeconds = tripSchduleTime.timeIntervalSince1970

        print(dateInSeconds)
        let dateEpochSec = String(format: "%.0f", dateInSeconds)

        // print(modetype)
        if isCalenderEvent || modetype == "ArivalAt"
            {
            url = "https://maps.googleapis.com/maps/api/directions/json"
            parameters = [
                "origin": "place_id:\(self.sourceString)",
                "destination": "place_id:\(self.destinationString)",
                "mode": "transit",
                "arrival_time": dateEpochSec,
                "alternatives": "true",
                "key": Key.GoogleAPIKey
            ]
        }
        else
        {
            if tripSchduleTime != nil
                {
                url = "https://maps.googleapis.com/maps/api/directions/json"
                parameters = [
                    "origin": "place_id:\(self.sourceString)",
                    "destination": "place_id:\(self.destinationString)",
                    "mode": "transit",
                    "departure_time": dateEpochSec,
                    "alternatives": "true",
                    "key": Key.GoogleAPIKey

                ]
            }
            else
            {
                url = "https://maps.googleapis.com/maps/api/directions/json"
                parameters = [
                    "origin": "place_id:\(self.sourceString)",
                    "destination": "place_id:\(self.destinationString)",
                    "mode": "transit",
                    "departure_time": dateEpochSec,
                    "alternatives": "true",
                    "key": Key.GoogleAPIKey
                ]
            }
        }

        //
        var googlewalkingurl = "https://maps.googleapis.com/maps/api/directions/json"
        let googlewalkingorgin = "\(self.sourceLat),\(self.sourceLng)"
        let googlewalkingdestination = "\(self.destinationLat),\(self.destinationLng)"

        let parameters1: Parameters = [
            "origin": googlewalkingorgin,
            "destination": googlewalkingdestination,
            "mode": "bicycling",
            "DepartTime": dateInSeconds,
            "alternatives": "true",
            "key": Key.GoogleAPIKey

        ]


        Alamofire.request(url, method: .get, parameters: parameters1, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<SuggestBikeDirectionalClass>) in
                switch response.result {
                case .success:
                    print(response)
                    //to get status code


                    self.dataGoogleResponse = response
                    print(self.dataGoogleResponse.result.value?.status ?? "user nil")

                    if self.dataGoogleResponse.result.value?.status == "OK" && self.dataGoogleResponse.result.value?.routes != nil
                        {




                    }
                    else
                    {


                    }
                case .failure(let error):
                    print(error)
                    self.showErrorDialogBox(viewController: self)
                }
        }



        ///

        print("https://maps.googleapis.com/maps/api/directions/json?origin=\(self.sourceLat),\(self.sourceLng)&destination=\(self.destinationLat),\(self.destinationLng)&mode=transit&departure_time=\(dateEpochSec)&alternatives=true&key=\(Key.GoogleAPIKey)")
        self.showLoader()
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<SuggestDirectionaClass>) in
                //print(response)
                switch response.result {
                case .success:
                    if response.result.value?.status != "INVALID_REQUEST" && (response.result.value?.routes?.count)! > 0
                        {
                        self.dataRespone = response
                        let modifiedArray: NSMutableArray = []

                        var durationCheckValue = 0

                        // self.sourceString = self.dataRespone.result.value?
                        for dataSteps in (self.dataRespone.result.value?.routes!)!
                        {

                            if self.filterOptionIndex == 0
                                {
                                print("timing == \((dataSteps.legs?[0].duration?.value!)!)")
                                if durationCheckValue > (dataSteps.legs?[0].duration?.value!)! || durationCheckValue == 0
                                    {

                                    durationCheckValue = (dataSteps.legs?[0].duration?.value!)!
                                    if modifiedArray.count > 0
                                        {
                                        if modifiedArray.count > 1
                                            {
                                            modifiedArray.replaceObject(at: 1, with: modifiedArray.object(at: 0))
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                        else
                                        {
                                            modifiedArray.insert(modifiedArray.object(at: 0), at: 1)
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 0)
                                    }
                                }
                                else
                                {
                                    if modifiedArray.count > 1
                                        {
                                        let modifiedCheck = modifiedArray.object(at: 1) as! SuggestRoute
                                        if (modifiedCheck.legs?[0].duration?.value)! > (dataSteps.legs?[0].duration?.value!)!
                                            {
                                            modifiedArray.replaceObject(at: 1, with: dataSteps)
                                        }

                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 1)
                                    }
                                }
                            }
                            else if self.filterOptionIndex == 1 {
                                print("timing == \((dataSteps.legs?[0].steps?.count)!)")
                                if durationCheckValue > (dataSteps.legs?[0].steps?.count)! || durationCheckValue == 0
                                    {

                                    durationCheckValue = (dataSteps.legs?[0].steps?.count)!
                                    if modifiedArray.count > 0
                                        {
                                        if modifiedArray.count > 1
                                            {
                                            modifiedArray.replaceObject(at: 1, with: modifiedArray.object(at: 0))
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                        else
                                        {
                                            modifiedArray.insert(modifiedArray.object(at: 0), at: 1)
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 0)
                                    }
                                }
                                else
                                {
                                    if modifiedArray.count > 1
                                        {
                                        let modifiedCheck = modifiedArray.object(at: 1) as! SuggestRoute
                                        if (modifiedCheck.legs?[0].steps?.count)! > (dataSteps.legs?[0].steps?.count)!
                                            {
                                            modifiedArray.replaceObject(at: 1, with: dataSteps)
                                        }

                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 1)
                                    }
                                }
                            }
                            else if self.filterOptionIndex == 2 {
                                print("timing == \((dataSteps.legs?[0].steps?.count)!)")
                                var stepsDistanceValues = 0
                                for dataStepsValues in (dataSteps.legs?[0].steps)!
                                {
                                    if dataStepsValues.travelMode == "WALKING"
                                        {
                                        print("timing == \((dataStepsValues.distance?.value)!)")
                                        let userDefaults = UserDefaults.standard
                                        if (dataStepsValues.distance?.value)! > 805 && userDefaults.bool(forKey: "isCab")
                                            {
                                            print("uber")
                                        }
                                        else
                                        {
                                            stepsDistanceValues = stepsDistanceValues + (dataStepsValues.distance?.value)!
                                        }

                                    }
                                }
                                if durationCheckValue > stepsDistanceValues || durationCheckValue == 0
                                    {

                                    durationCheckValue = stepsDistanceValues
                                    if modifiedArray.count > 0
                                        {
                                        if modifiedArray.count > 1
                                            {
                                            modifiedArray.replaceObject(at: 1, with: modifiedArray.object(at: 0))
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                        else
                                        {
                                            modifiedArray.insert(modifiedArray.object(at: 0), at: 1)
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 0)
                                    }
                                }
                                else
                                {

                                    if modifiedArray.count > 1
                                        {
                                        let modifiedCheck = modifiedArray.object(at: 1) as! SuggestRoute
                                        var stepsDistanceValues1 = 0
                                        for dataStepsValues1 in (modifiedCheck.legs?[0].steps)!
                                        {
                                            let userDefaults = UserDefaults.standard
                                            if (dataStepsValues1.distance?.value)! > 805 && userDefaults.bool(forKey: "isCab")
                                                {
                                                print("uber")
                                            }
                                            else
                                            {
                                                stepsDistanceValues1 = stepsDistanceValues1 + (dataStepsValues1.distance?.value)!
                                            }
                                        }

                                        if stepsDistanceValues1 > stepsDistanceValues
                                            {
                                            modifiedArray.replaceObject(at: 1, with: dataSteps)
                                        }

                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 1)
                                    }
                                }

                            }
                        }
                        let modifiedCheckTmp0 = modifiedArray.object(at: 0) as! SuggestRoute
                        if modifiedArray.count > 1
                            {
                            let modifiedCheckTmp1 = modifiedArray.object(at: 1) as! SuggestRoute
                            print((modifiedCheckTmp1.legs?[0].duration?.value)!)
                        }
                        print((modifiedCheckTmp0.legs?[0].duration?.value)!)

                        if modifiedArray.count > 0
                            {
                            self.dataRespone.result.value?.routes = modifiedArray as? [SuggestRoute]
                        }


                        print(self.dataRespone.result.value?.status ?? "user nil")

                        if self.dataRespone.result.value?.status == "OK"
                            {
                            if self.BirdDatalistapicount != nil {
                                if self.BirdDatalistapicount! > 0 {
                                    let walkingurl = "https://maps.googleapis.com/maps/api/directions/json"
                                    let walkingorgin = "\(self.sourceLat),\(self.sourceLng)"
                                    let walkingdestination = "\(self.BirdBikeLat!),\(self.BirdBikeLon!)"
                                    print("\(self.BirdBikeLat!),\(self.BirdBikeLon!)")
                                    print("\(self.sourceLat),\(self.sourceLng)")
                                    let BikeDestinationLatlong = "\(self.destinationLat),\(self.destinationLng)"
                                    let WalkingParameters: Parameters = [
                                        "origin": walkingorgin,
                                        "destination": walkingdestination,
                                        "mode": "walking",
                                        "DepartTime": dateInSeconds,
                                        "alternatives": "true",
                                        "key": Key.GoogleAPIKey

                                    ]

                                    print("\(walkingurl)?origin=\(walkingorgin)&destination=\(walkingdestination)&mode=walking&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)")

                                    Alamofire.request(url, method: .get, parameters: WalkingParameters, encoding: URLEncoding.default)
                                        .responseObject { (response: DataResponse<SuggestWalkingDirectionalClass>) in
                                            switch response.result {
                                            case .success:
                                                self.DataWalkingResponse = response


                                                let parameters1: Parameters = [
                                                    "origin": walkingdestination,
                                                    "destination": BikeDestinationLatlong,
                                                    "mode": "bicycling",
                                                    "DepartTime": dateInSeconds,
                                                    "alternatives": "true",
                                                    "key": Key.GoogleAPIKey

                                                ]

                                                print("\(url)?origin=\(walkingdestination)&destination=\(BikeDestinationLatlong)&mode=bicycling&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)")

                                                Alamofire.request(url, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                                                    .responseObject { (response: DataResponse<SuggestBikeDirectionalClass>) in
                                                        switch response.result {
                                                        case .success:
                                                            print(response)
                                                            //to get status code


                                                            self.dataBikeResponse = response
                                                            print(self.dataBikeResponse.result.value?.status ?? "user nil")

                                                            if self.dataBikeResponse.result.value?.status == "OK" && self.dataBikeResponse.result.value?.routes != nil
                                                                {

                                                                self.menuBackView1.backgroundColor = UIColor.white
                                                                self.menuBackView1.layer.borderWidth = 2
                                                                self.menuBackView1.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor

                                                                self.isBikeAvailable = true;

                                                                self.menuBackView.frame = CGRect.init(x: 2000, y: 90, width: self.view.frame.width - 90, height: self.view.frame.height)
                                                                self.menuBackView.backgroundColor = UIColor.white
                                                                self.menuBackView.layer.borderWidth = 2
                                                                self.menuBackView.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor
                                                                self.view.addSubview(self.menuBackView)

                                                                //   self.sugesstionTableView.reloadData()
                                                                let userDefaults = UserDefaults.standard
                                                                print(userDefaults.bool(forKey: "isBike"))
                                                                print("**********************************")
                                                                print(self.skippedArray)
                                                                print(self.skippedArray.count)

                                                                self.skippedArray.add("Transit")
//                                                                if self.bikeBool == true
//                                                                {
                                                                self.skippedArray.add("Bikeshare")

                                                                // }

//                                                                if self.cabBool == true
//                                                                {
                                                                self.skippedArray.add("Rideshare")

                                                                //}


                                                                self.sugesstionTableView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                                                self.sugesstionTableView.register(TripSelectionTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)

                                                                // This view controller itself will provide the delegate methods and row data for the table view.
                                                                self.sugesstionTableView.delegate = self
                                                                self.sugesstionTableView.dataSource = self
                                                                self.sugesstionTableView.bounces = false
                                                                self.sugesstionTableView.backgroundColor = UIColor.white
                                                                self.view.addSubview(self.sugesstionTableView)

                                                            }
                                                            else
                                                            {

                                                                self.isBikeAvailable = false;

                                                                let userDefaults = UserDefaults.standard

                                                                self.skippedArray.add("Transit")
//                                                                if self.bikeBool == true
//                                                                {
                                                                self.skippedArray.add("Bikeshare")

//                                                                }
//
//                                                                if self.cabBool == true
//                                                                {
                                                                self.skippedArray.add("Rideshare")

                                                                //  }

                                                                DispatchQueue.main.async {
                                                                    self.sugesstionTableView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                                                    self.sugesstionTableView.register(TripSelectionTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)

                                                                    // This view controller itself will provide the delegate methods and row data for the table view.
                                                                    self.sugesstionTableView.delegate = self
                                                                    self.sugesstionTableView.dataSource = self
                                                                    self.sugesstionTableView.bounces = false
                                                                    self.sugesstionTableView.backgroundColor = UIColor.white
                                                                    self.view.addSubview(self.sugesstionTableView)
                                                                }

                                                            }
                                                        case .failure(let error):
                                                            print(error)
                                                            self.showErrorDialogBox(viewController: self)
                                                        }
                                                }



                                            case .failure(let error):
                                                print(error)

                                            }
                                    }




                                }
                                else {

                                    let userDefaults = UserDefaults.standard
                                    print(userDefaults.bool(forKey: "isBike"))
                                    print(self.skippedArray)
                                    print("**********************************")

                                    self.skippedArray.add("Transit")
//                                    if self.bikeBool == true
//                                    {
                                    self.skippedArray.add("Bikeshare")

//                                    }
//
//                                    if self.cabBool == true
//                                    {
                                    self.skippedArray.add("Rideshare")

                                    // }



                                    // self.skippedArray.add("Bike")
                                    //self.skippedArray.add("Louvelo")

                                    if userDefaults.bool(forKey: "isCab")
                                        {
                                        if userDefaults.bool(forKey: "isUber") {
                                            //  self.skippedArray.add("Uber")
                                        }
                                        else
                                        {
                                            // self.skippedArray.add("Lyft")
                                        }
                                    }


                                    self.sugesstionTableView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                    self.sugesstionTableView.register(TripSelectionTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)

                                    // This view controller itself will provide the delegate methods and row data for the table view.
                                    self.sugesstionTableView.delegate = self
                                    self.sugesstionTableView.dataSource = self
                                    self.sugesstionTableView.bounces = false
                                    self.sugesstionTableView.backgroundColor = UIColor.white
                                    self.view.addSubview(self.sugesstionTableView)

                                }







                            }
                            else {

                                let userDefaults = UserDefaults.standard
                                print(userDefaults.bool(forKey: "isBike"))
                                print(self.skippedArray)
                                print("**********************************")

                                self.skippedArray.add("Transit")
//                                if self.bikeBool == true
//                                {
                                self.skippedArray.add("Bikeshare")

//                                }
//
//                                if self.cabBool == true
//                                {
                                self.skippedArray.add("Rideshare")

                                // }



                                self.sugesstionTableView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                                self.sugesstionTableView.register(TripSelectionTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)

                                // This view controller itself will provide the delegate methods and row data for the table view.
                                self.sugesstionTableView.delegate = self
                                self.sugesstionTableView.dataSource = self
                                self.sugesstionTableView.bounces = false
                                self.sugesstionTableView.backgroundColor = UIColor.white
                                self.view.addSubview(self.sugesstionTableView)


                            }

                        }
                        else
                        {
                            let userDefaults = UserDefaults.standard
                            if userDefaults.bool(forKey: "isCab")
                                {
                                if userDefaults.bool(forKey: "isUber") {
                                    self.skippedArray.add("Uber")
                                }
                                else
                                {
                                    self.skippedArray.add("Lyft")
                                }
                                DispatchQueue.main.async {
                                    self.sugesstionTableView.reloadData()
                                }
                            }



                        }

                        self.sugesstionTableView.reloadData()
                    }
                    else
                    {
                       // self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Trip Suggestion"
                        alertViewController.message = "There are no results for given address."

                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions


                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in

                                self.dismiss(animated: true, completion: nil)


                                if let vcs = self.navigationController?.viewControllers {
                                    let previousVC = vcs[vcs.count - 2]
                                    if previousVC is LoginNewViewController {
                                        // ... and so on
                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                    }
                                    else {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                }

                                //self.navigationController?.popViewController(animated: true)


                            }
                        )

                        alertViewController.addAction(cancelAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                case .failure(let error):
                    self.showErrorDialogBox(viewController: self)
                    print(error)
                }
        }
        ///
        if self.BoltDatalistapicount != nil {
            if self.BoltDatalistapicount! > 0 {
                var walkingurl = "https://maps.googleapis.com/maps/api/directions/json"
                let walkingorgin = "\(self.sourceLat),\(self.sourceLng)"
                let walkingdestination = "\(self.BoltBikeLat!),\(self.BoltBikeLon!)"
                print("\(self.BoltBikeLat!),\(self.BoltBikeLon!)")
                print("\(self.sourceLat),\(self.sourceLng)")
                let BikeDestinationLatlong = "\(self.destinationLat),\(self.destinationLng)"
                let WalkingParameters: Parameters = [
                    "origin": walkingorgin,
                    "destination": walkingdestination,
                    "mode": "walking",
                    "DepartTime": dateInSeconds,
                    "alternatives": "true",
                    "key": Key.GoogleAPIKey

                ]

                print("\(walkingurl)?origin=\(walkingorgin)&destination=\(walkingdestination)&mode=walking&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)")

                Alamofire.request(url, method: .get, parameters: WalkingParameters, encoding: URLEncoding.default)
                    .responseObject { (response: DataResponse<SuggestWalkingDirectionalClass>) in
                        switch response.result {
                        case .success:
                            self.BoltDataWalkingResponse = response

                            let parameters1: Parameters = [
                                "origin": walkingdestination,
                                "destination": BikeDestinationLatlong,
                                "mode": "bicycling",
                                "DepartTime": dateInSeconds,
                                "alternatives": "true",
                                "key": Key.GoogleAPIKey

                            ]

                            print("\(url)?origin=\(walkingdestination)&destination=\(BikeDestinationLatlong)&mode=bicycling&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)")

                            Alamofire.request(url, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                                .responseObject { (response: DataResponse<SuggestBikeDirectionalClass>) in
                                    switch response.result {
                                    case .success:
                                        print(response)
                                        //to get status code


                                        self.BoltdataBikeResponse = response
                                        print(self.BoltdataBikeResponse.result.value?.status ?? "user nil")

                                        if self.BoltdataBikeResponse.result.value?.status == "OK" && self.BoltdataBikeResponse.result.value?.routes != nil
                                            {

                                            self.menuBackView1.backgroundColor = UIColor.white
                                            self.menuBackView1.layer.borderWidth = 2
                                            self.menuBackView1.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor

                                            self.isBikeAvailable = true;

                                            self.menuBackView.frame = CGRect.init(x: 2000, y: 90, width: self.view.frame.width - 90, height: self.view.frame.height)
                                            self.menuBackView.backgroundColor = UIColor.white
                                            self.menuBackView.layer.borderWidth = 2
                                            self.menuBackView.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor
                                            self.view.addSubview(self.menuBackView)


                                        }
                                        else
                                        {



                                        }
                                    case .failure(let error):
                                        print(error)
                                        self.showErrorDialogBox(viewController: self)
                                    }
                            }



                        case .failure(let error):
                            print(error)

                        }
                }




            }
        }
        //
        if self.LimeDatalistapicount != nil {
            if self.LimeDatalistapicount! > 0 {
                var walkingurl = "https://maps.googleapis.com/maps/api/directions/json"
                let walkingorgin = "\(self.sourceLat),\(self.sourceLng)"
                let walkingdestination = "\(self.LimeBikeLat!),\(self.LimeBikeLon!)"
                print("\(self.LimeBikeLat!),\(self.LimeBikeLon!)")
                print("\(self.sourceLat),\(self.sourceLng)")
                let BikeDestinationLatlong = "\(self.destinationLat),\(self.destinationLng)"
                let WalkingParameters: Parameters = [
                    "origin": walkingorgin,
                    "destination": walkingdestination,
                    "mode": "walking",
                    "DepartTime": dateInSeconds,
                    "alternatives": "true",
                    "key": Key.GoogleAPIKey

                ]

                print("\(walkingurl)?origin=\(walkingorgin)&destination=\(walkingdestination)&mode=walking&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)")

                Alamofire.request(url, method: .get, parameters: WalkingParameters, encoding: URLEncoding.default)
                    .responseObject { (response: DataResponse<SuggestWalkingDirectionalClass>) in
                        switch response.result {
                        case .success:
                            self.LimeDataWalkingResponse = response


                            let parameters1: Parameters = [
                                "origin": walkingdestination,
                                "destination": BikeDestinationLatlong,
                                "mode": "bicycling",
                                "DepartTime": dateInSeconds,
                                "alternatives": "true",
                                "key": Key.GoogleAPIKey

                            ]

                            print("\(url)?origin=\(walkingdestination)&destination=\(BikeDestinationLatlong)&mode=bicycling&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)")

                            Alamofire.request(url, method: .get, parameters: parameters1, encoding: URLEncoding.default)
                                .responseObject { (Limeresponse: DataResponse<SuggestBikeDirectionalClass>) in
                                    switch Limeresponse.result {
                                    case .success:
                                        print(response)
                                        //to get status code
                                        print("**************Lime Data")

                                        self.LimedataBikeResponse = Limeresponse
                                        print(self.LimedataBikeResponse.result.value?.status ?? "user nil")

                                        if self.LimedataBikeResponse.result.value?.status == "OK" && self.LimedataBikeResponse.result.value?.routes != nil
                                            {

                                            self.menuBackView1.backgroundColor = UIColor.white
                                            self.menuBackView1.layer.borderWidth = 2
                                            self.menuBackView1.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor

                                            self.isBikeAvailable = true;

                                            self.menuBackView.frame = CGRect.init(x: 2000, y: 90, width: self.view.frame.width - 90, height: self.view.frame.height)
                                            self.menuBackView.backgroundColor = UIColor.white
                                            self.menuBackView.layer.borderWidth = 2
                                            self.menuBackView.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor
                                            self.view.addSubview(self.menuBackView)


                                        }
                                        else
                                        {



                                        }
                                    case .failure(let error):
                                        print(error)
                                        self.showErrorDialogBox(viewController: self)
                                    }
                            }



                        case .failure(let error):
                            print(error)

                        }
                }




            }
        }
        //


    }
    @objc func OnMenuClicked() {

        let actionController = YoutubeActionController()
        actionController.addAction(Action(ActionData(title: "Quickest Trip", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.filterOptionIndex = 0
                self.reloadDataInUI()
            }))
        actionController.addAction(Action(ActionData(title: "Least Number of Transfers", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.filterOptionIndex = 1
                self.reloadDataInUI()
            }))
        actionController.addAction(Action(ActionData(title: "Least Amount of Walking", image: UIImage(named: "yt-add-to-watch-later-icon")!), style: .default, handler: { action in
                self.filterOptionIndex = 2
                self.reloadDataInUI()
            }))


        actionController.addAction(Action(ActionData(title: "Cancel", image: UIImage(named: "yt-cancel-icon")!), style: .cancel, handler: nil))

        present(actionController, animated: true, completion: nil)

    }



    @objc func closebuttonAction(sender: UIButton!) {
        print("Button tapped")
        backgroundBlurView1.removeFromSuperview();
        UIView.animate(withDuration: 1,
            delay: 0.1,
            options: UIViewAnimationOptions.curveEaseInOut,
            animations: { () -> Void in

                self.menuBackView.frame = CGRect.init(x: 2000, y: 90, width: self.view.frame.size.width - 80, height: self.view.frame.size.height - 120)
                self.menuBackView1.frame = CGRect.init(x: 2000, y: 90, width: self.view.frame.size.width - 80, height: self.view.frame.size.height - 120)


            }, completion: { (finished) -> Void in

            })
    }
    @objc func OnBackClicked() {
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is LoginNewViewController {
                // ... and so on
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else {
                self.navigationController?.popViewController(animated: true)
            }
        }

        // self.navigationController?.popViewController(animated: true)


    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tableView == selectProfileTableView {
            print("profile count \(selectProfileArray.count)")
            return selectProfileArray.count
        }


        if tableView == menuTabelView
            {
            return 9
        }
        else if tableView == fareListTableView {
            return 3
        }
        else if tableView == menuTicketTableView
            {
            return stepArray.count + 1
        }
        else
        {
            if isBikeAvailable {
                let valueResult = dataRespone.result.value
                //let bikevalueResult = dataBikeResponse.result.value
                if self.skippedArray[section] as! String == "Transit" {
                    if (valueResult?.routes?.count)! > 1
                        {
                        if (valueResult?.routes![0].legs![0].steps![0].travelMode == "WALKING") && (valueResult?.routes![0].legs![0].steps?.count == 1) {
                            return 1
                        }
                        else if (valueResult?.routes![1].legs![0].steps![0].travelMode == "WALKING") && (valueResult?.routes![1].legs![0].steps?.count == 1) {
                            return 1
                        }
                        else {
                            return 2
                        }
                    }
                    else {
                        return 1
                    }

                }
                // if self.skippedArray[section] as! String == "Louvelo"{
                    else if self.skippedArray[section] as! String == "Louvelo" {

                        return 1

                }
                else if self.skippedArray[section] as! String == "Bikeshare" {


                    return 1

                }

                else
                {
                    return 1
                }
            }
            else
            {
                let valueResult = dataRespone.result.value
                if section == 0 {
                    if (valueResult?.routes?.count)! > 1
                        {
                        if (valueResult?.routes![0].legs![0].steps![0].travelMode == "WALKING") && (valueResult?.routes![0].legs![0].steps?.count == 1) {
                            return 1
                        }
                        else if (valueResult?.routes![1].legs![0].steps![0].travelMode == "WALKING") && (valueResult?.routes![1].legs![0].steps?.count == 1) {
                            return 1
                        }
                        else {
                            return 2
                        }
                    }
                    else {
                        return 1
                    }


                }
                else
                {
                    return 1
                }
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        self.hideLoader()
        if tableView == menuTabelView || tableView == menuTicketTableView || tableView == selectProfileTableView || tableView == fareListTableView
            {
            return 1
        }
        else
        {
            print(skippedArray.count)
            return self.skippedArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == menuTabelView || tableView == menuTicketTableView || tableView == selectProfileTableView || tableView == fareListTableView
            {
            return 0
        }
        else
        {
            return 45.0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // let vHeader = UITableViewHeaderFooterView()
        if tableView != menuTabelView && tableView != menuTicketTableView && menuTabelView != fareListTableView
            {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.white

            let headerLabel = UILabel()
            let farelabelForBird = UILabel()

            headerLabel.frame = CGRect.init(x: 20, y: 0, width: 165, height: 40)
                headerLabel.font = UIFont.setTarcBold(size: 15.0)
            headerLabel.textColor = UIColor.black
            headerLabel.textAlignment = NSTextAlignment.left
            headerView.addSubview(headerLabel)


                farelabelForBird.font = UIFont.setTarcRegular(size: 15.0)
            farelabelForBird.textColor = UIColor.black
            farelabelForBird.textAlignment = NSTextAlignment.left


                        if skippedArray[section] as? String == "Bikeshare" {
                headerLabel.frame = CGRect.init(x: 20, y: 0, width: 150, height: 40)
                farelabelForBird.frame = CGRect.init(x: headerLabel.frame.origin.x + 40, y: 0, width: self.view.frame.width - 20, height: 40)
                farelabelForBird.text = "- $1 start, then 15 cents/min"
                headerLabel.text = self.skippedArray[section] as? String
                if dataBikeResponse != nil {
                    let valueResult = dataBikeResponse.result.value
                    let routesPlanBIKE = valueResult?.routes![0].legs
                    let durationStringADD = routesPlanBIKE![0].duration?.value
                    var googleWalkingDuration = 0
                    if self.dataGoogleResponse.result.value?.routes != nil
                    {
                    let suggWalking = self.dataGoogleResponse.result.value?.routes![0].legs ?? nil
                    // for DataWalkingResponse in (suggWalking?.route)! {
                    for LegsData in suggWalking! {
                        googleWalkingDuration = (LegsData.duration?.value)!
                        // }
                    }
                    let durationString = googleWalkingDuration
                    print(durationString)
                    let hourString: Int = durationString / 3600
                    let minString: Int = (durationString % 3600) / 60
                    headerLabel.text = "Bike "
                    if hourString > 0
                        {
                        headerLabel.text = "Bikeshare (~\(hourString) hr \(minString) min)"
                    }
                    else
                    {
                        headerLabel.text = "Bikeshare (~\(minString) min) "
                    }
                            }
                }
                else if BoltdataBikeResponse != nil {
                    let valueResult = BoltdataBikeResponse.result.value
                    let routesPlanBIKE = valueResult?.routes![0].legs
                    let durationStringADD = routesPlanBIKE![0].duration?.value
                    var googleWalkingDuration = 0
                    let suggWalking = self.dataGoogleResponse.result.value?.routes![0].legs ?? nil
                    // for DataWalkingResponse in (suggWalking?.route)! {
                    for LegsData in suggWalking! {
                        googleWalkingDuration = (LegsData.duration?.value)!
                        // }
                    }
                    let durationString = googleWalkingDuration
                    print(durationString)
                    let hourString: Int = durationString / 3600
                    let minString: Int = (durationString % 3600) / 60
                    if hourString > 0
                        {
                        headerLabel.text = "Bikeshare (~\(hourString) hr \(minString) min)"
                    }
                    else
                    {
                        headerLabel.text = "Bikeshare (~\(minString) min)"
                    }
                }
              else if LimedataBikeResponse != nil {
                  let valueResult = LimedataBikeResponse.result.value
                  let routesPlanBIKE = valueResult?.routes![0].legs
                  let durationStringADD = routesPlanBIKE![0].duration?.value
                  var googleWalkingDuration = 0
                  let suggWalking = self.dataGoogleResponse.result.value?.routes![0].legs ?? nil
                  // for DataWalkingResponse in (suggWalking?.route)! {
                  for LegsData in suggWalking! {
                      googleWalkingDuration = (LegsData.duration?.value)!
                      // }
                  }
                  let durationString = googleWalkingDuration
                  print(durationString)
                  let hourString: Int = durationString / 3600
                  let minString: Int = (durationString % 3600) / 60
                  if hourString > 0
                      {
                      headerLabel.text = "Bikeshare (~\(hourString) hr \(minString) min) "
                  }
                  else
                  {
                      headerLabel.text = "Bikeshare (~\(minString) min)"
                  }
              }
                    //ArunCheckcrash
                else if louveloResponse != nil && self.dataGoogleResponse != nil {
                    if self.dataGoogleResponse.result.value != nil {
                    if self.dataGoogleResponse.result.value?.routes != nil
                    {
                        var googleWalkingDuration = 0
                        if (self.dataGoogleResponse.result.value?.routes!.count)! > 0 {
                            let suggWalking = self.dataGoogleResponse.result.value?.routes![0].legs ?? nil
                            // for DataWalkingResponse in (suggWalking?.route)! {
                            for LegsData in suggWalking! {
                                googleWalkingDuration = (LegsData.duration?.value)!
                                // }
                            }
                            let durationString = googleWalkingDuration
                            print(durationString)
                            let hourString: Int = durationString / 3600
                            let minString: Int = (durationString % 3600) / 60
                            if hourString > 0
                            {
                                headerLabel.text = "Bikeshare (~\(hourString) hr \(minString) min) "
                            }
                            else
                            {
                                headerLabel.text = "Bikeshare (~\(minString) min)"
                            }
                        }
                        }
                    }
                    else
                    {
                        
                    }
                    
                    
                    
                            }
            }
//            else if skippedArray[section] as? String == "Louvelo"{
//                headerLabel.text = "LouVelo "
//                farelabelForBird.text = "- $3.50/ 30min
//                farelabelForBird.frame = CGRect.init(x: headerLabel.frame.width + 10, y: 0, width: self.view.frame.width-20, height: 40)
//
//            }
            else if skippedArray[section] as? String == "Transit" {
                //                let MyTarcButton = UIImageView()
                //                MyTarcButton.frame = CGRect.init(x:80, y: 10, width: 20, height: 20)
                //                MyTarcButton.image = UIImage(named:"information")
                //                MyTarcButton.layer.zPosition = 10000
                //          //  prevArrowFrame = MyTarcButton.frame
                //                headerView.addSubview(MyTarcButton)

                headerLabel.text = self.skippedArray[section] as? String


                let button = UIButton(type: .custom)
                button.frame = CGRect.init(x: 80, y: 10, width: 20, height: 20)// create button
                button.tag = section
                // the button is image - set image
                button.setImage(UIImage(named: "information"), for: .normal)
                button.addTarget(self, action: #selector(self.FareInformation(_:)), for: .touchUpInside)


                headerView.addSubview(button) // add the button to the view




            } else if skippedArray[section] as? String == "Rideshare"
                {
                //xintvalue and yintvalue
                let LyftTravelTimes = hourminConverter(distValue: comVarUber.Lyft_Travel_Min + 300)
                print(LyftTravelTimes)
                let titleString = self.skippedArray[section] as! String
                headerLabel.text = "\(titleString) (~\(LyftTravelTimes)) "

            }
            else {
                headerLabel.text = self.skippedArray[section] as? String
            }

            //  headerView.addSubview(farelabelForBird)
            let lineLabel = UILabel()
            lineLabel.frame = CGRect.init(x: 20, y: 39, width: self.view.frame.width - 40, height: 1)
            lineLabel.backgroundColor = UIColor.gray
            headerView.addSubview(lineLabel)

            return headerView

        }
        else
        {
            return nil
        }



    }



    @objc func BIRDFareInformation(_ sender: UIButton) {



        backgroundBlurView1.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        backgroundBlurView1.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(backgroundBlurView1)
        // let stepValue = routesPlan![0].steps![indexPath.row]

        UIView.animate(withDuration: 1,
            delay: 0.1,
            options: UIViewAnimationOptions.curveEaseInOut,
            animations: { () -> Void in
                if UIDevice.current.userInterfaceIdiom == .pad
                    {
                    self.menuBackView1.frame = CGRect.init(x: Int((self.view.frame.size.width / 2)) - self.ticketMenuX, y: Int((self.view.frame.size.height / 2)) - self.ticketMenuY, width: self.ticketMenuWidth, height: 220)

                }
                else
                {

                    self.menuBackView1.frame = CGRect.init(x: self.ticketMenuX, y: Int(self.view.frame.size.height / 3), width: Int(self.view.frame.size.width) - self.ticketMenuWidth, height: 220)


                }
                self.backgroundBlurView1.addSubview(self.menuBackView1)

            }, completion: { (finished) -> Void in
                let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.closeBtnWidth, height: self.closeBtnWidth))
                closeButton.setImage(UIImage(named: "closeBtn"), for: .normal)
                closeButton.addTarget(self, action: #selector(self.closebuttonAction), for: .touchUpInside)
                self.menuBackView1.addSubview(closeButton)

                let BirdImage = UIImageView(frame: CGRect(x: Int(closeButton.frame.origin.x + closeButton.frame.width) + 30, y: 15, width: 40, height: 40))
                BirdImage.image = UIImage(named: "Bird1")
                self.menuBackView1.addSubview(BirdImage)

                let PopupTitle = UILabel(frame: CGRect(x: 0, y: 10, width: Int(self.menuBackView1.frame.width), height: self.closeBtnWidth))
                PopupTitle.text = "Bird"
                PopupTitle.textColor = UIColor.black
                PopupTitle.font = UIFont.boldSystemFont(ofSize: 25.0)
                PopupTitle.textAlignment = NSTextAlignment.center

                self.menuBackView1.addSubview(PopupTitle)

                let ContentLabel = UILabel(frame: CGRect(x: 10, y: Int(closeButton.frame.height), width: Int(self.menuBackView1.frame.width) - 50, height: Int(self.menuBackView1.frame.height - 70)))
                // ContentLabel.text = "$1 start, then 15 cents/min"
                ContentLabel.text = "Bird costs a flat fee of $1 to ride the scooter plus 15 cents per minute."

                ContentLabel.textAlignment = NSTextAlignment.center
                ContentLabel.contentMode = .scaleToFill
                ContentLabel.numberOfLines = 3
                ContentLabel.textColor = UIColor.black
                self.menuBackView1.addSubview(ContentLabel)





            })

    }
    @objc func FareInformation(_ sender: UIButton) {





        backgroundBlurView1.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        backgroundBlurView1.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(backgroundBlurView1)
        // let stepValue = routesPlan![0].steps![indexPath.row]

        UIView.animate(withDuration: 1,
            delay: 0.1,
            options: UIViewAnimationOptions.curveEaseInOut,
            animations: { () -> Void in
                if UIDevice.current.userInterfaceIdiom == .pad
                    {
                    self.menuBackView.frame = CGRect.init(x: Int((self.view.frame.size.width / 2)) - self.ticketMenuX, y: Int((self.view.frame.size.height / 2)) - self.ticketMenuY, width: self.ticketMenuWidth, height: 220)

                }
                else
                {

                    self.menuBackView.frame = CGRect.init(x: self.ticketMenuX, y: Int(self.view.frame.size.height / 3), width: Int(self.view.frame.size.width) - self.ticketMenuWidth, height: 220)


                }
                self.backgroundBlurView1.addSubview(self.menuBackView)

            }, completion: { (finished) -> Void in
                let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.closeBtnWidth, height: self.closeBtnWidth))
                closeButton.setImage(UIImage(named: "closeBtn"), for: .normal)
                closeButton.addTarget(self, action: #selector(self.closebuttonAction), for: .touchUpInside)
                self.menuBackView.addSubview(closeButton)

                let PopupTitle = UILabel(frame: CGRect(x: 0, y: 0, width: Int(self.menuBackView.frame.width), height: self.closeBtnWidth))
                PopupTitle.text = "Fares"
                PopupTitle.textColor = UIColor(red: 208 / 255.0, green: 48 / 255.0, blue: 46 / 255.0, alpha: 1.0)
                PopupTitle.font = UIFont.setTarcBold(size: 18.0)
                PopupTitle.textAlignment = NSTextAlignment.center

                self.menuBackView.addSubview(PopupTitle)



                self.fareListTableView = UITableView()
                self.fareListTableView.register(FareTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)

                self.fareListTableView.frame = CGRect.init(x: 0, y: Int(closeButton.frame.size.height), width: Int(self.menuBackView.frame.width), height: 140)
                self.fareListTableView.delegate = self
                self.fareListTableView.dataSource = self
                self.fareListTableView.backgroundColor = UIColor.white
                self.fareListTableView.alwaysBounceVertical = false

                self.menuBackView.addSubview(self.fareListTableView)






            })
    }

    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


        // create a new cell if needed or reuse an old one
        if tableView == selectProfileTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
            tableView.separatorColor = UIColor.clear;
            cell.separatorInset = .zero
            cell.textLabel?.text = selectProfileArray.object(at: indexPath.row) as? String
            cell.selectionStyle = .none

            return cell
        } else {
            if tableView == menuTabelView
                {
                let cell: UITableViewCell = (self.menuTabelView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)!
                tableView.separatorColor = UIColor.clear;
                cell.separatorInset = .zero
                if indexPath.row == 0
                    {
                    cell.textLabel?.text = "My Profile"
                }
                else if indexPath.row == 1
                    {
                    cell.textLabel?.text = "My Tickets"
                }
                else if indexPath.row == 2
                    {
                    cell.textLabel?.text = "Saved Trips"
                }
                else if indexPath.row == 3
                    {
                    cell.textLabel?.text = "Maps & Schedules"
                }
                else if indexPath.row == 4
                    {
                    cell.textLabel?.text = "System Map"
                }
                else if indexPath.row == 5
                    {
                    cell.textLabel?.text = "Calender Events"
                }
                else if indexPath.row == 6
                    {
                    cell.textLabel?.text = "Support"
                }
                else if indexPath.row == 7
                    {
                    cell.textLabel?.text = "Settings"
                }
                else
                {
                    cell.textLabel?.text = "Log Out"
                }
                let lineLabel = UILabel()
                lineLabel.frame = CGRect.init(x: 0, y: 43, width: cell.contentView.frame.size.width, height: 1)
                lineLabel.backgroundColor = UIColor.black
                cell.contentView.addSubview(lineLabel)
                return cell
            }
            else if tableView == fareListTableView
                {
                let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! FareTableViewCell
                cell.ExpressMytarc.text = ridetype[indexPath.row]
                cell.Title1.text = NormalPrice[indexPath.row]
                cell.Title2.text = TarcCardlPrice[indexPath.row]
                cell.selectionStyle = .none
                    cell.ExpressMytarc.font = UIFont.setTarcBold(size: 14.0)
                // headerLabel.font = UIFont.systemFont(ofSize: 16.0)
                cell.ExpressMytarc.textColor = UIColor.black
                cell.ExpressMytarc.textAlignment = NSTextAlignment.left
                if indexPath.row == 0 {
                    cell.Title1.font = UIFont.setTarcBold(size: 14.0)
                    cell.Title1.textColor = UIColor.black

                    cell.Title2.font = UIFont.setTarcBold(size: 14.0)
                    cell.Title2.textColor = UIColor.black
                    cell.Title1.frame = CGRect.init(x: 70, y: 10, width: 95, height: 20)
                    cell.Title2.frame = CGRect.init(x: 160, y: 10, width: 95, height: 20)
                    cell.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                    cell.Title1.textColor = UIColor.white
                    cell.Title2.textColor = UIColor.white
                }
                else {
                    cell.Title1.font = UIFont.setTarcRegular(size: 14.0)
                    cell.Title1.textColor = UIColor.black
                    cell.Title1.textAlignment = NSTextAlignment.right
                    cell.Title2.font = UIFont.setTarcRegular(size: 14.0)
                    cell.Title2.textColor = UIColor.black
                    cell.Title2.textAlignment = NSTextAlignment.right
                    cell.Title1.frame = CGRect.init(x: 80, y: 10, width: 45, height: 20)
                    cell.Title2.frame = CGRect.init(x: 130, y: 10, width: 85, height: 20)
                   // cell.myTarc.image = UIImage(named: "TARC Ticket")


                }




                cell.ExpressMytarc.frame = CGRect.init(x: 20, y: 10, width: 90, height: 20)
                cell.myTarc.frame = CGRect.init(x: 230, y: 5, width: 40, height: 30)
                //cell.ExpressMytarc.backgroundColor = UIColor.black
                cell.contentView.addSubview(cell.ExpressMytarc)
                cell.contentView.addSubview(cell.Title1)
                cell.contentView.addSubview(cell.Title2)
                cell.contentView.addSubview(cell.myTarc)

                return cell
            }
            else if tableView == menuTicketTableView
                {
                let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! TripSelectionTableViewCell
                tableView.separatorColor = UIColor.clear;
                cell.separatorInset = .zero
                if indexPath.row < stepArray.count
                    {



                    print(stepArray[indexPath.row] as! String)
                    if stepArray[indexPath.row] as! String == "Transit Uber"
                        {
                        let uberAccesstoken = UserDefaults.standard.string(forKey: "uberKey")
                        let behavior = RideRequestViewRequestingBehavior(presentingViewController: self)
                        // Optional, defaults to using the user’s current location for pickup

                        let builder = RideParametersBuilder()

                        //                        let valueResult = self.dataPrevRespone.result.value
                        //                        let routesPlan = valueResult?.routes![self.indexValue].legs
                        let resultRes = dataRespone.result.value?.routes![indexTicketValue].legs![0];
                        for stepValue in resultRes!.steps!
                        {
                            print("Sec Index ---> \(uberSecIndex)")
                            let distanceValue = (stepValue.distance?.value)!
                            print((stepValue.distance?.value)!)
                            if stepValue.htmlInstructions! == uberSecIndex || distanceValue > 805
                                {

                                let pickupLocation = CLLocation(latitude: CLLocationDegrees((stepValue.startLocation?.lat)!), longitude: CLLocationDegrees((stepValue.startLocation?.lng)!))
                                let dropoffLocation = CLLocation(latitude: CLLocationDegrees((stepValue.endLocation?.lat)!), longitude: CLLocationDegrees((stepValue.endLocation?.lng)!))

                                builder.pickupLocation = pickupLocation
                                builder.dropoffLocation = dropoffLocation
                                builder.dropoffNickname = "Drop Location"


                                let ridesClient = RidesClient()
                                ridesClient.fetchProducts(pickupLocation: pickupLocation) { products, response in
                                    guard let uberX = products.filter({ $0.productGroup == .uberX }).first else {
                                        // Handle error, UberX does not exist at this location
                                        print("error")
                                        return
                                    }
                                    print(uberX.productID!)
                                    builder.productID = uberX.productID
                                    ridesClient.fetchRideRequestEstimate(parameters: builder.build(), completion: { rideEstimate, response in
                                            //                        guard let rideEstimate = rideEstimate else {
                                            //                            print("error")
                                            //                            // Handle error, unable to get an ride request estimate
                                            //                            return
                                            //                        }

                                            print(rideEstimate?.fare! ?? " empty  ")
                                            builder.upfrontFare = rideEstimate?.fare

                                            let rideParameters = builder.build()
                                            let requestingBehavior = DeeplinkRequestingBehavior(fallbackType: .mobileWeb)

                                            cell.uberBtn = RideRequestButton(rideParameters: rideParameters, requestingBehavior: requestingBehavior)


                                            // let uberbutton = RideRequestButton(rideParameters: builder.build(), requestingBehavior: behavior)
                                            //                    let uberbutton = RideRequestButton()
                                            if UIDevice.current.userInterfaceIdiom == .phone
                                                {
                                                cell.uberBtn.frame = CGRect.init(x: 10 + self.uberButtonX, y: 10, width: self.uberBtnWidth, height: 50)
                                            }
                                            else
                                            {
                                                cell.uberBtn.frame = CGRect.init(x: Int((cell.contentView.frame.size.width / 2) - 130), y: 10, width: self.uberBtnWidth, height: 50)
                                            }
                                            cell.uberBtn.rideParameters = builder.build()
                                            cell.uberBtn.loadRideInformation()


                                            DispatchQueue.main.async {
                                                cell.uberBackView.frame = cell.uberBtn.frame
                                                cell.uberBackView.addSubview(cell.uberBtn)
                                                cell.uberBackView.backgroundColor = .clear
                                                cell.contentView.addSubview(cell.uberBackView)
                                            }

                                        })

                                }
                            }

                            // let uberbutton = RideRequestButton(rideParameters: builder.build(), requestingBehavior: behavior)



                        }

                        //                    }
                    }
                    else
                    {



                        if UIDevice.current.userInterfaceIdiom == .phone
                            {
                            cell.transitNameLabel.frame = CGRect.init(x: 0, y: 0, width: cell.contentView.frame.size.width - 80, height: cell.contentView.frame.size.height)
                            cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width - 80, y: 0, width: 60, height: cell.contentView.frame.size.height)
                        }
                        else
                        {
                            cell.transitNameLabel.frame = CGRect.init(x: 0, y: 0, width: cell.contentView.frame.size.width - 120, height: cell.contentView.frame.size.height)
                            cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width - 120, y: 0, width: 120, height: cell.contentView.frame.size.height)
                        }
                        cell.transitNameLabel.text = stepArray[indexPath.row] as? String
                        cell.transitNameLabel.textColor = UIColor.black
                        cell.transitNameLabel.textAlignment = NSTextAlignment.center
                        cell.transitNameLabel.font = UIFont.setTarcRegular(size: 15.0)
                        cell.contentView.addSubview(cell.transitNameLabel)
                        let userDefaults1 = UserDefaults.standard
                        cell.transitFareLabel.text = "$\(userDefaults1.value(forKey: "generalFare") ?? 0)"
                        cell.transitFareLabel.textColor = UIColor.black
                        cell.transitFareLabel.textAlignment = NSTextAlignment.left
                        cell.transitFareLabel.font = UIFont.setTarcRegular(size: 15.0)
                        cell.contentView.addSubview(cell.transitFareLabel)
                    }
                }
                else
                {

                    var countFare = 0
                    for stepValue in stepArray
                    {
                        if stepValue as! String == "Transit Uber"
                            {
                            print("Uber")
                        }
                        else
                        {
                            countFare = countFare + 1
                        }
                    }
                    if UIDevice.current.userInterfaceIdiom == .phone
                        {
                        cell.transitNameLabel.frame = CGRect.init(x: 0, y: 0, width: cell.contentView.frame.size.width - 80, height: cell.contentView.frame.size.height)
                        cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width - 80, y: 0, width: 60, height: cell.contentView.frame.size.height)
                    }
                    else
                    {
                        cell.transitNameLabel.frame = CGRect.init(x: 0, y: 0, width: cell.contentView.frame.size.width - 120, height: cell.contentView.frame.size.height)
                        cell.transitFareLabel.frame = CGRect.init(x: cell.contentView.frame.size.width - 120, y: 0, width: 120, height: cell.contentView.frame.size.height)
                    }
                    if countFare == 0
                        {
                        cell.transitNameLabel.text = "No Tickets"
                    }
                    else if countFare == 1
                        {
                        cell.transitNameLabel.text = "Buy  ticket"
                    }
                    else
                    {
                        cell.transitNameLabel.text = "Buy All Tickets"
                    }
                    cell.transitNameLabel.textColor = UIColor.black
                    cell.transitNameLabel.textAlignment = NSTextAlignment.center
                    cell.transitNameLabel.font = UIFont.setTarcBold(size: 20.0)
                    cell.contentView.addSubview(cell.transitNameLabel)
                    let userDefaults1 = UserDefaults.standard
                    let fareValue: Double = userDefaults1.value(forKey: "generalValue") as! Double
                    // let fareString = String(format: "$%.2f", Double(countFare) * fareValue) // for adding the transfer
                    let fareString = String(format: "$%.2f", fareValue) // for not adding the transfer
                    cell.transitFareLabel.text = fareString
                    cell.transitFareLabel.textColor = UIColor.black
                    cell.transitFareLabel.textAlignment = NSTextAlignment.left
                    cell.transitFareLabel.font = UIFont.setTarcBold(size: 20.0)
                    cell.contentView.addSubview(cell.transitFareLabel)

                }

                return cell


            }
            else
            {

                print(skippedArray[indexPath.section] as? String)
                if skippedArray[indexPath.section] as? String == "Transit"
                    {

                    var userDefault = UserDefaults.standard
                    let cell: TripSelectionTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TripSelectionTableViewCell?)!
                    cell.selectionStyle = .none
                    tableView.separatorColor = UIColor.clear;
                    cell.separatorInset = .zero
                    print("Bus 1")
                    let durationLabel = UILabel()
                    let BuyNowButton = UIButton()

                    let fareLabel = UILabel()
                    let TotalMyTarcFareLabel = UILabel()
                    let mytarccard = UILabel()
                    let Regular = UILabel()
                    let valueResult = dataRespone.result.value
                    let routesPlan = valueResult?.routes![indexPath.row].legs
                    cell.transitScrollView.delegate = self
                    cell.transitScrollView.backgroundColor = UIColor.clear
                    cell.transitScrollView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
                    cell.transitScrollView.showsHorizontalScrollIndicator = false
                    var durationString = routesPlan![0].duration?.value
                    //  durationLabel.frame = CGRect.init(x: 10, y: 15, width: 95, height: 30)

                    var fareCount: Double = 0.00
                    var MyCardfareCount: Double = 0.00
                    var distValue = 0
                    var UberLyftWaitingTime: Int?
                    durationLabel.textColor = UIColor.black
                    durationLabel.font = UIFont.setTarcRegular(size: 16.0)
                    durationLabel.textAlignment = NSTextAlignment.center

                    let NoTransit = UILabel()
                    var indexValue = 0
                    var prevArrowFrame = CGRect()
                    if valueResult?.routes![0].legs![0].steps?.count == 1 && valueResult?.routes![0].legs![0].steps![0].travelMode == "WALKING" {

                        NoTransit.text = "No Transit available"
                        NoTransit.frame = CGRect.init(x: cell.transitScrollView.frame.origin.x + 10, y: cell.transitScrollView.frame.origin.y + 10, width: cell.transitScrollView.frame.width, height: cell.transitScrollView.frame.height)
                        NoTransit.textColor = UIColor.black
                        NoTransit.font = UIFont.setTarcRegular(size: 16.0)
                        NoTransit.textAlignment = NSTextAlignment.center

                        //cell.contentView.addSubview(durationLabel)
                        cell.transitScrollView.addSubview(NoTransit)
                        cell.contentView.addSubview(cell.transitScrollView)

                    }
                    else {
                        var TransitAvalible = Bool()
                        TransitAvalible = false
                        for stepValue in routesPlan![0].steps!
                        {

var totalFareFirstmile = [Double]()
                            let transitImage = UIImageView()
                            let arrowImage = UIImageView()
                            let MyTarcButton = UIImageView()
                            let busName = UILabel()
                            let busTimingLable = UILabel()

                            let secondImage = UIImageView()
                            let thirdImage = UIImageView()
                            var transitBool = Bool()

                            let firstLabel = UILabel()
                            let secondLabel = UILabel()
                            let thirdLabel = UILabel()
                            firstLabel.textAlignment = .center

                            transitBool = false
                            let distanceValue = (stepValue.distance?.value)!
                            let durationvaluewalking = (stepValue.duration?.value)!
                            let userDefauls = UserDefaults.standard
                            let uberCheck = userDefauls.bool(forKey: "isCab") as Bool
                            if distanceValue > 805 && userDefauls.bool(forKey: "isCab") && stepValue.travelMode == "WALKING"
                                {
                                TransitAvalible = true
                                if UberLyftWaitingTime == nil {
                                    UberLyftWaitingTime = comVarUber.uber_Pickup_min! * 60
                                }
                                let ridesClient = RidesClient()
                                let pickupLocation = CLLocation(latitude: Double((stepValue.startLocation?.lat)!), longitude: Double((stepValue.startLocation?.lng)!))
                                let dropoffLocation = CLLocation(latitude: Double((stepValue.endLocation?.lat)!), longitude: Double((stepValue.endLocation?.lng)!))

                                comFunc.getLyftAPIFirst(SourceLat: (stepValue.startLocation?.lat!)!, SourceLong: (stepValue.startLocation?.lng!)!, DestinationLat: (stepValue.endLocation?.lat!)!, DestinationLong: ((stepValue.endLocation?.lng!)!)) { (MinPrice, MaxPrice, TravelMin, travelMile, Status) in
                                    if Status {
                                        secondLabel.text = "$\(MinPrice) - $\(MaxPrice)"
                                        //
                                        var overallDistance = 0.0
                                        let minutes = TravelMin / 60

                                        let LyftTravelTime = minutes ;
                                        print(LyftTravelTime)

                                        overallDistance = self.distance(lat1: (stepValue.startLocation?.lat!)!, lon1: (stepValue.startLocation?.lng!)!, lat2: (stepValue.endLocation?.lat!)!, lon2: (stepValue.endLocation?.lng!)!, unit: "M")
                                        print(overallDistance)
                                        var calculateMiles = travelMile / 1609.344
                                        // calculateMiles = 2.3
                                        if calculateMiles == 1000
                                            {
                                            calculateMiles = overallDistance
                                        }

                                        var estimatetime = Double(LyftTravelTime)
                                        if TravelMin == 300
                                            {
                                            estimatetime = calculateMiles / 20
                                        }
                                        var timeCalculation = estimatetime * 0.28
                                        print(timeCalculation)
                                        let calculateoverallMiles = calculateMiles * 0.80
                                        print(calculateoverallMiles)
                                        var overallTotal = calculateoverallMiles + 2.30 + timeCalculation
                                        print(overallTotal)
                                        overallTotal = overallTotal.rounded(.toNearestOrAwayFromZero)

                                        // var minimumfareFloat  = NSString()
                                        var minimumfare = overallTotal - 1
                                        if minimumfare <= 5.79
                                            {
                                            minimumfare = 5.80
                                            overallTotal = minimumfare
                                        }
                                       let minimumfareFloat = Int(minimumfare)
                                                let maximumfareFloat = overallTotal + 1
                                                // var maxString  = NSString()
                                                //let maxString = Int(maximumfareFloat)
                                                let minString = minimumfare.rounded(.toNearestOrAwayFromZero)
                                            let maxString = maximumfareFloat.rounded(.toNearestOrAwayFromZero)
                                                firstLabel.text = "$\(minString) - $\(maxString)"
                                                print(MinPrice)
                                                print(MaxPrice)
                                            }
                                        }






                                let builder = RideParametersBuilder()
                                builder.pickupLocation = pickupLocation
                                builder.dropoffLocation = dropoffLocation
                                if userDefauls.bool(forKey: "isUber") {

                                    ridesClient.fetchTimeEstimates(pickupLocation: pickupLocation) { (estimates, response) in
                                        if estimates.count > 0 {
                                            UberLyftWaitingTime = estimates[0].estimate!
                                            print(estimates[0].estimate!)
                                        }
                                        else {
                                            UberLyftWaitingTime = 120
                                        }

                                    }
                                    ridesClient.fetchPriceEstimates(pickupLocation: pickupLocation, dropoffLocation: dropoffLocation, completion: { priceEstimats, response in
                                        print(priceEstimats.count)
                                        if priceEstimats.count != 0 {
                                            durationString = priceEstimats[0].duration! + UberLyftWaitingTime!


                                            print(distanceValue)
                                            print(routesPlan![0].steps!.count == 1 && stepValue.travelMode == "WALKING")
                                            if routesPlan![0].steps!.count == 1 && stepValue.travelMode == "WALKING" {

                                                distValue = durationString!

                                            }
                                            else {
                                                distValue = distValue + durationString! - durationvaluewalking
                                            }


                                            let hourString: Int = distValue / 3600
                                            let minString: Int = (distValue % 3600) / 60
                                            if hourString > 0
                                                {
                                                DispatchQueue.main.async {
                                                    durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                                                    durationLabel.textColor = UIColor.black
                                                    durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                                    durationLabel.textAlignment = NSTextAlignment.center
                                                    //cell.contentView.addSubview(durationLabel)
                                                    cell.transitScrollView.addSubview(durationLabel)
                                                }
                                            }
                                            else
                                            {
                                                DispatchQueue.main.async {
                                                    durationLabel.text = String(describing: "\(minString) min")
                                                    durationLabel.textColor = UIColor.black
                                                    durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                                    durationLabel.textAlignment = NSTextAlignment.center
                                                    //   cell.contentView.addSubview(durationLabel)
                                                    cell.transitScrollView.addSubview(durationLabel)
                                                }
                                            }
                                        }

                                        else {



                                        }

                                    })
                                }
                                else {
                                    let pickup = CLLocationCoordinate2D(latitude: Double((stepValue.startLocation?.lat)!), longitude: Double((stepValue.startLocation?.lng)!))
                                    let destination = CLLocationCoordinate2D(latitude: Double((stepValue.endLocation?.lat)!), longitude: Double((stepValue.endLocation?.lng)!))

                                    LyftAPI.ETAs(to: pickup) { result in

                                        if result.error == nil {

                                            if result.value!.count > 0 && result.value != nil {


                                                UberLyftWaitingTime = result.value![0].minutes
                                                //print("\(UberLyftWaitingTime) sec-isaac")

                                            }
                                            else {
                                                UberLyftWaitingTime = 3
                                            }

                                        }
                                        else {
                                            UberLyftWaitingTime = 3
                                        }
                                    }

                                    LyftAPI.costEstimates(from: pickup, to: destination, rideKind: .Standard) { result in
                                        if result.error == nil {
                                            let costEstimate = result.value![0]
                                            print("Min: \(costEstimate.estimate!.minEstimate.amount)$")
                                            print("Max: \(costEstimate.estimate!.maxEstimate.amount)$")
                                            print("Distance: \(costEstimate.estimate!.distanceMiles) miles")
                                            print("Duration: \(costEstimate.estimate!.durationSeconds) sec")
                                            durationString = costEstimate.estimate!.durationSeconds


                                            print(distanceValue)
                                            if routesPlan![0].steps!.count == 1 && stepValue.travelMode == "WALKING" {

                                                distValue = durationString!

                                            }
                                            else {
                                                distValue = distValue + durationString! - durationvaluewalking

                                            }

                                            let hourString: Int = distValue / 3600
                                            let minString: Int = ((distValue % 3600) / 60) + 3
                                            if hourString > 0
                                                {
                                                DispatchQueue.main.async {
                                                    durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                                                    durationLabel.textColor = UIColor.black
                                                    durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                                    durationLabel.textAlignment = NSTextAlignment.center
                                                    //cell.contentView.addSubview(durationLabel)
                                                    cell.transitScrollView.addSubview(durationLabel)
                                                }
                                            }
                                            else
                                            {
                                                DispatchQueue.main.async {
                                                    durationLabel.text = String(describing: "\(minString) min")
                                                    durationLabel.textColor = UIColor.black
                                                    durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                                    durationLabel.textAlignment = NSTextAlignment.center
                                                    //   cell.contentView.addSubview(durationLabel)
                                                    cell.transitScrollView.addSubview(durationLabel)
                                                }
                                            }

                                        }
                                        else {
                                            // let pickup = CLLocationCoordinate2D(latitude: , longitude:)
                                            //let destination = CLLocationCoordinate2D(latitude: , longitude:)
                                            // getalertTitle(RouteNo: sender.title) { (AlertList, AlertString, AlertBool) in
                                            //
                                            comFunc.getLyftAPIFirst(SourceLat: Double((stepValue.startLocation?.lat)!), SourceLong: Double((stepValue.startLocation?.lng)!), DestinationLat: Double((stepValue.endLocation?.lat)!), DestinationLong: Double((stepValue.endLocation?.lng)!), completion: { (TravelPrice, maxprice, TravelMin, TravelMil, CheckBool) in

                                                    if CheckBool {



                                                        durationString = TravelMin + 300

                                                        if routesPlan![0].steps!.count == 1 && stepValue.travelMode == "WALKING" {

                                                            distValue = durationString!

                                                        }
                                                        else {
                                                            distValue = distValue + durationString! - durationvaluewalking

                                                        }

                                                        let hourString: Int = distValue / 3600
                                                        let minString: Int = ((distValue % 3600) / 60) + 3
                                                        if hourString > 0
                                                            {
                                                            DispatchQueue.main.async {
                                                                durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                                                                //  durationLabel.textColor = UIColor.black
                                                                durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                                                durationLabel.textAlignment = NSTextAlignment.center
                                                                //cell.contentView.addSubview(durationLabel)
                                                                cell.transitScrollView.addSubview(durationLabel)
                                                            }
                                                        }
                                                        else
                                                        {
                                                            DispatchQueue.main.async {
                                                                durationLabel.text = String(describing: "\(minString) min")
                                                                // durationLabel.textColor = UIColor.black
                                                                durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                                                durationLabel.textAlignment = NSTextAlignment.center
                                                                //   cell.contentView.addSubview(durationLabel)
                                                                cell.transitScrollView.addSubview(durationLabel)
                                                            }
                                                        }

                                                    }
                                                })

                                        }
                                    }
                                }
                            }
                            else if distanceValue > 500 && stepValue.travelMode == "WALKING" && uberCheck == false
                                {
                                TransitAvalible = true
                                let birddistanceNew = getBirdDistance(sourceLat: stepValue.startLocation!.lat!, sourceLng: stepValue.startLocation!.lng!)
                                    let blotdistanceNew = getBoltDistance(sourceLat: stepValue.startLocation!.lat!, sourceLng: stepValue.startLocation!.lng!)
                                                                  print(blotdistanceNew)

                                                                  let limedistanceNew = getLimeDisatance(sourceLat: stepValue.startLocation!.lat!, sourceLng: stepValue.startLocation!.lng!)
                                let Birdrequest = MKDirections.Request()
                                Birdrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: stepValue.startLocation!.lat!, longitude: stepValue.startLocation!.lng!), addressDictionary: nil))
                                Birdrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: stepValue.endLocation!.lat!, longitude: stepValue.endLocation!.lng!), addressDictionary: nil))
                                Birdrequest.requestsAlternateRoutes = true
                                Birdrequest.transportType = .automobile

                                let Birddirections = MKDirections(request: Birdrequest)

                                Birddirections.calculate { [unowned self] response, error in
                                    guard let BirdResponse = response else { return }

                                    //for Birdroute in BirdResponse.routes {

                                    print(BirdResponse)
                                    var Birdroute_Array = MKRoute()
                                    Birdroute_Array = BirdResponse.routes.first!
                                    print(distanceValue)
                                    print(Birdroute_Array.distance)
                                    print(Birdroute_Array.expectedTravelTime)

                                    let durationStringADD = Birdroute_Array.expectedTravelTime
                                    let sectominCalWithFare: Double = (Double(durationStringADD / 60).rounded(.toNearestOrAwayFromZero))
                                    print(((sectominCalWithFare * 0.15) + 1))
                                    if  birddistanceNew < 500 &&  birddistanceNew != 500 {
                                    let BirdBike_fare_Cal = ((sectominCalWithFare * 0.15) + 1)
                                    
                                   let firstlabelString = String(format: "$%.2f", BirdBike_fare_Cal)
                                    firstLabel.text = firstlabelString
                                    totalFareFirstmile.append(BirdBike_fare_Cal)
                                    }
                                    if limedistanceNew < 500 && limedistanceNew != 0 {
                                    let LimeBike_fare_Cal = ((sectominCalWithFare * 0.27) + 1)

                                   let secondlabelString = String(format: "$%.2f", LimeBike_fare_Cal)
                                        totalFareFirstmile.append(LimeBike_fare_Cal)

                                    secondLabel.text = secondlabelString
                                    }
                                    if blotdistanceNew < 500 && blotdistanceNew != 0 {
                                    let boltBike_fare_Cal = (sectominCalWithFare * 0.30)

                                   let thirdlabelString = String(format: "$%.2f", boltBike_fare_Cal)
                                            totalFareFirstmile.append(boltBike_fare_Cal)
                                    thirdLabel.text = thirdlabelString
                                    }
                                    if totalFareFirstmile.count > 0 {
                                    print(totalFareFirstmile)
                                    totalFareFirstmile.sort(){$0 < $1}
                                                                   fareCount = fareCount + Double(totalFareFirstmile[0])
                                                                   MyCardfareCount = MyCardfareCount + Double(totalFareFirstmile[0])
                                        
                                    }
                                                                  
                                                                   
                                                                   

                                }


                                let distanceValue = (stepValue.distance?.value)!

                                if birddistanceNew < 500
                                    {
                                    transitImage.image = UIImage(named: "Image-7")
                                    cell.transitScrollView.addSubview(transitImage)

                                    if indexValue == 0
                                        {


                                        firstLabel.frame = CGRect.init(x: 14 * (indexValue + 1), y: 40, width: 50, height: 30)
                                            firstLabel.font = UIFont.setTarcBold(size: 11.0)
                                        cell.transitScrollView.addSubview(firstLabel)
                                    }
                                    else
                                    {

                                        firstLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 4, y: 40, width: 50, height: 30)
                                        firstLabel.font = UIFont.setTarcBold(size: 11.0)
                                        cell.transitScrollView.addSubview(firstLabel)
                                    }
                                }

//                                let blotdistanceNew = getBoltDistance(sourceLat: stepValue.startLocation!.lat!, sourceLng: stepValue.startLocation!.lng!)
//                                print(blotdistanceNew)
//
//                                let limedistanceNew = getLimeDisatance(sourceLat: stepValue.startLocation!.lat!, sourceLng: stepValue.startLocation!.lng!)
                                print(limedistanceNew)

                                print((stepValue.distance?.value)!)

                                uberSecIndex = stepValue.htmlInstructions!
                                uberAddedIndex = indexPath.row + 1

                                if limedistanceNew < 500
                                    {
                                    secondImage.image = UIImage(named: "Image-9")
                                    cell.transitScrollView.addSubview(secondImage)

                                }

                                if boltListApi != nil
                                    {
                                    if boltListApi?.count != 0
                                        {
                                        if blotdistanceNew < 500
                                            {
                                            thirdImage.image = UIImage(named: "Image-8")
                                            cell.transitScrollView.addSubview(thirdImage)
                                            thirdLabel.isHidden = false
                                        }
                                    }
                                    else
                                    {


                                        thirdLabel.isHidden = true
                                    }
                                }
                                else
                                {
                                     if limedistanceNew < 500
                                     {
                                        
                                    }
                                    else
                                     {
                                    transitImage.image = UIImage(named: "walking_Icon")
                                    TransitAvalible = false
                                    thirdLabel.isHidden = true
                                    }
                                }


                                if birddistanceNew > 500 && blotdistanceNew > 500 && limedistanceNew > 500
                                    {
                                    transitImage.image = UIImage(named: "walking_Icon")
                                    TransitAvalible = false

                                }
                                else if birddistanceNew > 500 && limedistanceNew > 500
                                    {
                                    if boltListApi != nil
                                        {
                                        if boltListApi?.count == 0
                                            {
                                            transitImage.image = UIImage(named: "walking_Icon")
                                            TransitAvalible = false
                                        }
                                    }
                                }
                                transitBool = true
                                if indexValue == 0
                                    {
                                    if birddistanceNew < 500
                                        {
                                        secondImage.frame = CGRect.init(x: 35 * (indexValue + 2), y: 5, width: 35, height: 35)
                                        thirdImage.frame = CGRect.init(x: 38 * (indexValue + 3), y: 5, width: 35, height: 35)
                                        secondLabel.frame = CGRect.init(x: 34 * (indexValue + 2), y: 40, width: 50, height: 30)
                                            secondLabel.font = UIFont.setTarcBold(size: 11.0)
                                        cell.transitScrollView.addSubview(secondLabel)
                                        thirdLabel.frame = CGRect.init(x: 38 * (indexValue + 3), y: 40, width: 50, height: 30)
                                            thirdLabel.font = UIFont.setTarcBold(size: 11.0)
                                        cell.transitScrollView.addSubview(thirdLabel)

                                    }
                                    else if birddistanceNew > 500 && blotdistanceNew > 500 && limedistanceNew > 500
                                        {
                                        secondImage.frame = CGRect.init(x: 22 * (indexValue + 2), y: 5, width: 35, height: 35)
                                        thirdImage.frame = CGRect.init(x: 22 * (indexValue + 3), y: 5, width: 35, height: 35)
                                        secondLabel.frame = CGRect.init(x: 22 * (indexValue + 2), y: 40, width: 50, height: 30)
                                        secondLabel.textColor = UIColor.black
                                            secondLabel.font = UIFont.setTarcBold(size: 9.0)
                                        cell.transitScrollView.addSubview(secondLabel)
                                        thirdLabel.frame = CGRect.init(x: 22 * (indexValue + 3), y: 40, width: 50, height: 30)
                                        thirdLabel.textColor = UIColor.black
                                            thirdLabel.font = UIFont.setTarcBold(size: 11.0)
                                        cell.transitScrollView.addSubview(thirdLabel)
                                    }
                                    else
                                    {
                                        if limedistanceNew < 500
                                            {
                                            secondImage.frame = CGRect.init(x: 22 * (indexValue + 1), y: 5, width: 35, height: 35)
                                            thirdImage.frame = CGRect.init(x: 33 * (indexValue + 2), y: 5, width: 35, height: 35)
                                            secondLabel.frame = CGRect.init(x: 22 * (indexValue + 1), y: 40, width: 50, height: 30)

                                            secondLabel.textColor = UIColor.black
                                                secondLabel.font = UIFont.setTarcBold(size: 11.0)
                                            cell.transitScrollView.addSubview(secondLabel)
                                            thirdLabel.frame = CGRect.init(x: 33 * (indexValue + 2), y: 40, width: 60, height: 30)
                                            thirdLabel.textColor = UIColor.black
                                                thirdLabel.font = UIFont.setTarcBold(size: 11.0)
                                            cell.transitScrollView.addSubview(thirdLabel)

                                        }
                                        else
                                        {
                                            secondImage.frame = CGRect.init(x: 22 * (indexValue + 1), y: 5, width: 35, height: 35)
                                            thirdImage.frame = CGRect.init(x: 33 * (indexValue + 1), y: 5, width: 35, height: 35)
//                                            secondLabel.frame = CGRect.init(x: 22*(indexValue+1), y: 40, width: 35, height: 35)
//                                                            secondLabel.text = "$30 - $40"
//                                            secondLabel.textColor = UIColor.black
//                                            secondLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
//                                            cell.transitScrollView.addSubview(secondLabel)
                                            thirdLabel.frame = CGRect.init(x: 33 * (indexValue + 1), y: 40, width: 60, height: 30)
                                            thirdLabel.font = UIFont.setTarcBold(size: 11.0)
                                            thirdLabel.textColor = UIColor.black
                                            cell.transitScrollView.addSubview(thirdLabel)

                                        }

                                    }

                                }
                                else
                                {
                                    print(indexValue)
                                    if limedistanceNew < 500
                                        {
                                        if birddistanceNew < 500
                                            {
                                            secondImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 55, y: 5, width: 35, height: 35)
                                            thirdImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 100, y: 5, width: 35, height: 35)
                                            secondLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 55, y: 40, width: 50, height: 30)
                                            secondLabel.textColor = UIColor.black
                                                secondLabel.font = UIFont.setTarcBold(size: 11.0)
                                            cell.transitScrollView.addSubview(secondLabel)
                                            thirdLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 100, y: 40, width: 60, height: 30)
                                            thirdLabel.textColor = UIColor.black
                                                thirdLabel.font = UIFont.setTarcBold(size: 11.0)
                                            cell.transitScrollView.addSubview(thirdLabel)


                                        }
                                        else
                                        {
                                            secondImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 5, y: 5, width: 35, height: 35)
                                            thirdImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 55, y: 5, width: 35, height: 35)

                                            secondLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 5, y: 40, width: 50, height: 30)
                                            secondLabel.textColor = UIColor.black
                                            secondLabel.font = UIFont.setTarcBold(size: 9.0)
                                            cell.transitScrollView.addSubview(secondLabel)
                                            thirdLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 55, y: 40, width: 60, height: 30)
                                            thirdLabel.textColor = UIColor.black
                                            thirdLabel.font = UIFont.setTarcBold(size: 11.0)
                                            cell.transitScrollView.addSubview(thirdLabel)

                                        }

                                    }
                                    else
                                    {
                                        thirdImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 5, y: 5, width: 35, height: 35)


                                        thirdLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width, y: 40, width: 60, height: 30)
                                        thirdLabel.font = UIFont.setTarcBold(size: 11.0)

                                        cell.transitScrollView.addSubview(thirdLabel)

                                    }

                                }



                                let ridesClient = RidesClient()
                                let pickupLocation = CLLocation(latitude: Double((stepValue.startLocation?.lat)!), longitude: Double((stepValue.startLocation?.lng)!))
                                let dropoffLocation = CLLocation(latitude: Double((stepValue.endLocation?.lat)!), longitude: Double((stepValue.endLocation?.lng)!))
                                let builder = RideParametersBuilder()
                                builder.pickupLocation = pickupLocation
                                builder.dropoffLocation = dropoffLocation

//                                ridesClient.fetchPriceEstimates(pickupLocation: pickupLocation, dropoffLocation: dropoffLocation, completion: { priceEstimats, response in
//                                    print(priceEstimats.count)
//                                    if priceEstimats.count != 0 {
//                                        let uberProduct = priceEstimats[0]
//
//                                        print(uberProduct.estimate!)
//                                        print("\(fareCount + Double(uberProduct.lowEstimate!))")
//                                        fareCount = fareCount + Double(uberProduct.lowEstimate!)
//                                        MyCardfareCount = MyCardfareCount + Double(uberProduct.lowEstimate!)
//                                        let fareString = String(format: "%.2f", fareCount)
//                                        let mycardfarestring = String(format: "%.2f", MyCardfareCount)
//                                        DispatchQueue.main.async {
//                                            fareLabel.text = "$\(fareString)"
//                                            TotalMyTarcFareLabel.text = "$\(mycardfarestring)"
//                                        }
//                                    }
//                                    else {
//                                        print("\(fareCount + Double(comVarUber.uber_estimationPrice!))")
//                                        //Arun
//                                        fareCount = fareCount + Double(comVarUber.uber_estimationPrice!)
//                                        MyCardfareCount = MyCardfareCount + Double(comVarUber.uber_estimationPrice!)
//                                        let fareString = String(format: "%.2f", fareCount)
//                                        let mycardfarestring = String(format: "%.2f", MyCardfareCount)
//                                        DispatchQueue.main.async {
//                                            fareLabel.text = "$\(fareString)"
//                                            TotalMyTarcFareLabel.text = "$\(mycardfarestring)"
//                                        }
//
//                                    }
//
//
//
//                                })


                               



                            }
                            else
                            {

                                // distValue = distValue + (stepValue.duration?.value)!
                                distValue = durationString!
                                let hourString: Int = distValue / 3600
                                let minString: Int = (distValue % 3600) / 60
                                if hourString > 0
                                    {
                                    DispatchQueue.main.async {
                                        durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                                        //durationLabel.textColor = UIColor.black
                                        durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                        durationLabel.textAlignment = NSTextAlignment.center
                                        //   cell.contentView.addSubview(durationLabel)
                                        cell.transitScrollView.addSubview(durationLabel)
                                    }
                                }
                                else
                                {
                                    DispatchQueue.main.async {
                                        durationLabel.text = String(describing: "\(minString) min")
                                        // durationLabel.textColor = UIColor.black
                                        durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                        durationLabel.textAlignment = NSTextAlignment.center
                                        //cell.contentView.addSubview(durationLabel)
                                        cell.transitScrollView.addSubview(durationLabel)
                                    }
                                }
                            }
                            //120
                            if indexValue == 0
                                {

                                transitImage.frame = CGRect.init(x: 20 * (indexValue + 1), y: 5, width: 35, height: 35)

                            }
                            else
                            {
                                print("\(prevArrowFrame.origin.x + prevArrowFrame.size.width + 10)")
                                transitImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 10, y: 5, width: 35, height: 35)

                            }
                            if stepValue.travelMode == "TRANSIT"
                                {




                                let userDefaults1 = UserDefaults.standard
                                if stepValue.transitDetails?.line?.shortName != nil {

                                    if (stepValue.transitDetails?.line?.shortName!.contains("X"))! {

                                        // self.TotalFar = self.fareStringEXP + self.TotalFar
                                        fareCount = fareCount + self.fareStringEXP
                                        MyCardfareCount = self.fareStringExpMyCard

                                    }
                                    else {
                                        // self.TotalFar = self.FareStringLocal + self.TotalFar
                                        fareCount = fareCount + self.FareStringLocal
                                        MyCardfareCount = MyCardfareCount + self.fareStringLocalMycard
                                    }
                                }
                                else {
                                    // self.TotalFar = self.FareStringLocal + self.TotalFar
                                    fareCount = fareCount + self.FareStringLocal
                                    MyCardfareCount = MyCardfareCount + self.fareStringLocalMycard
                                }





                                let urlString = "https:" + (stepValue.transitDetails?.line?.vehicle?.icon)!
                                let url = URL(string: "https:" + (stepValue.transitDetails?.line?.vehicle?.icon)!)




                                if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/subway2.png"
                                    {
                                    transitImage.image = UIImage(named: "train_Transit")
                                }
                                else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/rail2.png"
                                    {
                                    transitImage.image = UIImage(named: "train_Transit")
                                }
                                else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/bus2.png"
                                    {
                                    if stepValue.transitDetails?.line?.agencies![0].name == "Greater Cleveland Regional Transit Authority"
                                        {
                                        transitImage.image = UIImage(named: "tank_bus")
                                    }
                                    else
                                    {
                                        transitImage.image = UIImage(named: "bus_Transit")
                                    }
                                }
                                else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/tram2.png"
                                    {
                                    transitImage.image = UIImage(named: "tram_transit")
                                }
                                else
                                {
                                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                    transitImage.image = UIImage(data: data!)
                                }

                                busName.frame = CGRect.init(x: transitImage.frame.origin.x + transitImage.frame.size.width, y: 15, width: 30, height: 30)
                                busName.text = stepValue.transitDetails?.line?.shortName
                                busName.frame.size = busName.intrinsicContentSize
                                busName.textColor = UIColor.black
                                busName.font = UIFont.setTarcBold(size: 14.0)
                                busName.textAlignment = NSTextAlignment.center
                                cell.transitScrollView.addSubview(busName)

                                busTimingLable.frame = CGRect.init(x: transitImage.frame.origin.x - 5, y: 45, width: 100, height: 30)
                                busTimingLable.text = stepValue.transitDetails?.departureTime?.text
                                busTimingLable.frame.size = busTimingLable.intrinsicContentSize
                                busTimingLable.textColor = UIColor.black
                                busTimingLable.font = UIFont.setTarcBold(size: 12.0)
                                busTimingLable.textAlignment = NSTextAlignment.left
                                cell.transitScrollView.addSubview(busTimingLable)

                                // let dep_epoh =
                                let arr_epoh = stepValue.transitDetails?.arrivalTime?.value!
                                let CurrentDate = NSDate().timeIntervalSince1970
                                let departdatetime = dateconvert(Datevalue: CurrentDate)
                                let arrival_time = dateconvert(Datevalue: arr_epoh!)
                                print(CurrentDate)
                                print(arr_epoh)
                                print(departdatetime)
                                print(arrival_time)
                                print(routesPlan ?? "defaut")



                                let formatter = DateFormatter()
                                formatter.dateFormat = "dd/MM/yyyy"
                                let firstDate = formatter.date(from: departdatetime)
                                let secondDate = formatter.date(from: arrival_time)
                                print(firstDate!)
                                print(secondDate!)
                                let formattera = DateComponentsFormatter()
                                formattera.allowedUnits = [.day]
                                formattera.unitsStyle = .full
                                var DateCalculation = formattera.string(from: firstDate!, to: secondDate!)!

                                // var resss = secondDate.interval(ofComponent: .day, fromDate: firstDate)
                                print(DateCalculation)
                                // print(components.day)

                                if firstDate?.compare(secondDate!) == .orderedAscending {
                                    print("First Date is smaller then second date")
                                    let plusoneday = UILabel()
                                    print("*********** +1 day ***********")
                                    if DateCalculation.contains("days") {
                                        //Str
                                        DateCalculation = DateCalculation.replacingOccurrences(of: "days", with: "day", options:
                                                NSString.CompareOptions.literal, range: nil)
                                        plusoneday.text = "+\(DateCalculation)"
                                    }
                                    else {
                                        plusoneday.text = "+\(DateCalculation)"
                                    }

                                    plusoneday.frame = CGRect.init(x: busTimingLable.frame.origin.x + 51, y: 45, width: 100, height: 30)
                                    plusoneday.frame.size = plusoneday.intrinsicContentSize
                                    plusoneday.textColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                                    plusoneday.font = UIFont.setTarcBold(size: 10.0)
                                    plusoneday.textAlignment = NSTextAlignment.left
                                    cell.transitScrollView.addSubview(plusoneday)
                                }

                            }

                            else
                            {
                                let distanceValue = (stepValue.distance?.value)!
                                print((stepValue.distance?.value)!)
                                let userDefaults = UserDefaults.standard
                                if distanceValue > 805 && userDefaults.bool(forKey: "isCab")
                                    {
                                    //  isUberAdded = true
                                    if userDefaults.bool(forKey: "isUber") {
                                        transitImage.image = UIImage(named: "Uber-1")
                                        uberSecIndex = stepValue.htmlInstructions!
                                        uberAddedIndex = indexPath.row + 1
                                        // firstLabel.backgroundColor = UIColor.lightGray
                                        if indexValue == 0
                                            {


                                            firstLabel.frame = CGRect.init(x: 14 * (indexValue + 1), y: 40, width: 50, height: 30)
                                                firstLabel.font = UIFont.setTarcBold(size: 9.0)
                                            cell.transitScrollView.addSubview(firstLabel)
                                        }
                                        else
                                        {

                                            firstLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 4, y: 40, width: 50, height: 30)
                                            firstLabel.font = UIFont.setTarcBold(size: 9.0)
                                            cell.transitScrollView.addSubview(firstLabel)
                                        }

                                        secondImage.image = UIImage(named: "lyft_preference")
                                        cell.transitScrollView.addSubview(secondImage)

                                        thirdImage.image = UIImage(named: "Uber-1")
                                        cell.transitScrollView.addSubview(thirdImage)
                                        transitBool = true
                                        if indexValue == 0
                                            {

                                            secondImage.frame = CGRect.init(x: 35 * (indexValue + 2), y: 5, width: 35, height: 35)
                                            secondLabel.frame = CGRect.init(x: 35 * (indexValue + 2), y: 40, width: 50, height: 30)
                                            secondLabel.textColor = UIColor.black
                                                secondLabel.font = UIFont.setTarcBold(size: 9.0)
                                            cell.transitScrollView.addSubview(secondLabel)


                                            let userDefaulss = UserDefaults.standard
                                            if userDefaulss.bool(forKey: "isCab")
                                                {

                                            }
                                            else
                                            {
                                                thirdImage.frame = CGRect.init(x: 20 * (indexValue + 3), y: 5, width: 35, height: 35)
                                                thirdLabel.frame = CGRect.init(x: 20 * (indexValue + 3), y: 40, width: 60, height: 30)
                                                thirdLabel.textColor = UIColor.black
                                                thirdLabel.font = UIFont.setTarcBold(size: 9.0)
                                                cell.transitScrollView.addSubview(thirdLabel)
                                            }

                                        }
                                        else
                                        {
                                            // transitImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width+10, y: 15, width: 24, height: 24)
                                            secondImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 50, y: 5, width: 35, height: 35)
                                            secondLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 55, y: 40, width: 50, height: 30)
                                            secondLabel.textColor = UIColor.black
                                            secondLabel.font = UIFont.setTarcBold(size: 9.0)
                                            cell.transitScrollView.addSubview(secondLabel)
                                            let userDefaulss = UserDefaults.standard
                                            if userDefaulss.bool(forKey: "isCab")
                                                {

                                            }
                                            else
                                            {
                                                thirdImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 20, y: 5, width: 35, height: 35)
                                                thirdLabel.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 30, y: 40, width: 60, height: 30)
                                                thirdLabel.textColor = UIColor.black
                                                thirdLabel.font = UIFont.setTarcBold(size: 9.0)
                                                cell.transitScrollView.addSubview(thirdLabel)
                                            }
                                        }



                                        let ridesClient = RidesClient()
                                        let pickupLocation = CLLocation(latitude: Double((stepValue.startLocation?.lat)!), longitude: Double((stepValue.startLocation?.lng)!))
                                        let dropoffLocation = CLLocation(latitude: Double((stepValue.endLocation?.lat)!), longitude: Double((stepValue.endLocation?.lng)!))
                                        let builder = RideParametersBuilder()
                                        builder.pickupLocation = pickupLocation
                                        builder.dropoffLocation = dropoffLocation

                                        ridesClient.fetchPriceEstimates(pickupLocation: pickupLocation, dropoffLocation: dropoffLocation, completion: { priceEstimats, response in
                                            print(priceEstimats.count)
                                            if priceEstimats.count != 0 {
                                                let uberProduct = priceEstimats[0]

                                                print(uberProduct.estimate!)
                                                print("\(fareCount + Double(uberProduct.lowEstimate!))")
                                                fareCount = fareCount + Double(uberProduct.lowEstimate!)
                                                MyCardfareCount = MyCardfareCount + Double(uberProduct.lowEstimate!)
                                                let fareString = String(format: "%.2f", fareCount)
                                                let mycardfarestring = String(format: "%.2f", MyCardfareCount)
                                                DispatchQueue.main.async {
                                                    fareLabel.text = "$\(fareString)"
                                                    TotalMyTarcFareLabel.text = "$\(mycardfarestring)"
                                                }
                                            }
                                            else {
                                                print("\(fareCount + Double(comVarUber.uber_estimationPrice!))")
                                                //Arun
                                                fareCount = fareCount + Double(comVarUber.uber_estimationPrice!)
                                                MyCardfareCount = MyCardfareCount + Double(comVarUber.uber_estimationPrice!)
                                                let fareString = String(format: "%.2f", fareCount)
                                                let mycardfarestring = String(format: "%.2f", MyCardfareCount)
                                                DispatchQueue.main.async {
                                                    fareLabel.text = "$\(fareString)"
                                                    TotalMyTarcFareLabel.text = "$\(mycardfarestring)"
                                                }

                                            }



                                        })
                                    } else {
                                        transitImage.image = UIImage(named: "lyft_preference")
                                        uberSecIndex = stepValue.htmlInstructions!
                                        uberAddedIndex = indexPath.row + 1

                                        let pickup = CLLocationCoordinate2D(latitude: Double((stepValue.startLocation?.lat)!), longitude: Double((stepValue.startLocation?.lng)!))
                                        let destination = CLLocationCoordinate2D(latitude: Double((stepValue.endLocation?.lat)!), longitude: Double((stepValue.endLocation?.lng)!))


                                        // let locationD = CLLocationCoordinate2D(latitude: 37.7833, longitude: -122.4167)




                                        LyftAPI.costEstimates(from: pickup, to: destination, rideKind: .Standard) { result in
                                            if result.error == nil {
                                                result.value?.forEach { costEstimate in
                                                    print("Min: \(costEstimate.estimate!.minEstimate.amount)$")
                                                    print("Max: \(costEstimate.estimate!.maxEstimate.amount)$")
                                                    print("Distance: \(costEstimate.estimate!.distanceMiles) miles")
                                                    print("Duration: \(costEstimate.estimate!.durationSeconds / 60) minutes")
                                                    let costEstimate = costEstimate.estimate!.minEstimate.amount
                                                    fareCount = fareCount + Double(truncating: costEstimate as NSNumber)
                                                    MyCardfareCount = MyCardfareCount + Double(truncating: costEstimate as NSNumber)
                                                    let mycardfarestringlyft = String(format: "%.2f", MyCardfareCount)

                                                    let fareString = String(format: "%.2f", fareCount)
                                                    DispatchQueue.main.async {
                                                        fareLabel.text = "$\(fareString)"
                                                        TotalMyTarcFareLabel.text = "$\(mycardfarestringlyft)"
                                                    }

                                                }
                                            }
                                            else {

                                                comFunc.getLyftAPIFirst(SourceLat: Double((stepValue.startLocation?.lat)!), SourceLong: Double((stepValue.startLocation?.lng)!), DestinationLat: Double((stepValue.endLocation?.lat)!), DestinationLong: Double((stepValue.endLocation?.lng)!), completion: { (TravelPrice, maxprice, TravelMin, TravelMil, CheckBool) in

                                                        if CheckBool {


                                                            let costEstimate = TravelPrice
                                                            fareCount = fareCount + Double(truncating: costEstimate as NSNumber)
                                                            MyCardfareCount = MyCardfareCount + Double(truncating: costEstimate as NSNumber)
                                                            let mycardfarestringlyft = String(format: "%.2f", MyCardfareCount)

                                                            let fareString = String(format: "%.2f", fareCount)
                                                            DispatchQueue.main.async {
                                                                fareLabel.text = "$\(fareString)"
                                                                TotalMyTarcFareLabel.text = "$\(mycardfarestringlyft)"
                                                            }

                                                        }
                                                    })


                                            }
                                        }


                                    }

                                }
                                else if distanceValue > 500 && stepValue.travelMode == "WALKING" && uberCheck == false
                                    {
                                    //                                       let birddistanceNew = getBirdDistance(sourceLat: stepValue.startLocation!.lat!, sourceLng: stepValue.startLocation!.lng!)
                                    //                                    if birddistanceNew < 500
                                    //                                    {
                                    //                                        transitImage.image = UIImage(named:"BirdSelect.png" )
                                    //
                                    //                                    }

                                }
                                else
                                {
                                    transitImage.image = UIImage(named: "walking_Icon")
                                }
                            }


                            cell.transitScrollView.addSubview(transitImage)
                            if indexValue + 1 < (routesPlan![0].steps?.count)!
                                {
                                print(stepValue.transitDetails?.line?.vehicle?.icon ?? "nil", stepValue.transitDetails?.line?.shortName ?? "nil")
                                if stepValue.travelMode == "TRANSIT" && stepValue.transitDetails?.line?.shortName != nil
                                    {
                                    arrowImage.frame = CGRect.init(x: busName.frame.origin.x + busName.frame.size.width + 10, y: 15, width: 24, height: 24)
                                }
                                else
                                {
                                    if transitBool == true
                                        {
                                        print(birdListApi)
                                        print(boltListApi)
                                        print(LimeListApi)

                                        arrowImage.frame = CGRect.init(x: thirdImage.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)

                                        if distanceValue > 500 && stepValue.travelMode == "WALKING" && uberCheck == false
                                            {
                                            if boltListApi != nil
                                                {
                                                if boltListApi!.count > 0
                                                    {
                                                    arrowImage.frame = CGRect.init(x: thirdImage.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)
                                                }
                                                else
                                                {
                                                    arrowImage.frame = CGRect.init(x: secondLabel.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)
                                                }
                                            }
                                            else
                                            {
                                                arrowImage.frame = CGRect.init(x: secondLabel.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)

                                            }
                                        }


                                        let userDefaulss = UserDefaults.standard
                                        if userDefaulss.bool(forKey: "isCab")
                                            {
                                            arrowImage.frame = CGRect.init(x: secondImage.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)
                                        }


                                    }
                                    else
                                    {
                                        arrowImage.frame = CGRect.init(x: transitImage.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)

                                    }
                                }

                                arrowImage.image = UIImage(named: "Right-Arrow")
                                prevArrowFrame = arrowImage.frame
                                cell.transitScrollView.addSubview(arrowImage)
                            }
                            if transitBool == true
                                {
                                let userDefaulss = UserDefaults.standard
                                if userDefaulss.bool(forKey: "isCab")
                                    {
                                    cell.transitScrollView.contentSize = CGSize(width: secondImage.frame.origin.x + secondImage.frame.size.width + 30, height: 80)

                                }
                                else
                                {
                                    cell.transitScrollView.contentSize = CGSize(width: thirdImage.frame.origin.x + thirdImage.frame.size.width + 30, height: 80)
                                }
                            }
                            else
                            {
                                cell.transitScrollView.contentSize = CGSize(width: transitImage.frame.origin.x + transitImage.frame.size.width + 30, height: 80)

                            }
                            indexValue = indexValue + 1

                        }
                        
                          DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                        let fareString = String(format: "%.2f", fareCount)
                        let mycardfarestring = String(format: "%.2f", MyCardfareCount)
                    
                            fareLabel.text = "$\(fareString)"
                            TotalMyTarcFareLabel.text = "$\(mycardfarestring)"
                        })

                        print("fare Count : \(fareCount)")
                        if fareCount > 0
                            {
                            let MyTarcButton = UIImageView()
                            self.TotalFar = 0.0
                            self.MycardTotalFare = 0.0
                            self.MycardTotalFareArray.removeAll()
                            for stepValue in routesPlan![0].steps!
                            {
                                let userDefaults1 = UserDefaults.standard
                                if stepValue.travelMode == "TRANSIT"
                                    {
                                    if stepValue.transitDetails?.line?.shortName != nil {
                                        if (stepValue.transitDetails?.line?.shortName!.contains("X"))! {

                                            self.TotalFar = self.fareStringEXP + self.TotalFar
                                            self.MycardTotalFare = self.fareStringExpMyCard
                                            MycardTotalFareArray.append(self.MycardTotalFare)

                                        }
                                        else {
                                            self.TotalFar = self.FareStringLocal + self.TotalFar
                                            self.MycardTotalFare = self.fareStringLocalMycard
                                            MycardTotalFareArray.append(self.MycardTotalFare)

                                        }
                                    }

                                    else {
                                        self.TotalFar = self.FareStringLocal + self.TotalFar
                                        self.MycardTotalFare = self.fareStringLocalMycard
                                        MycardTotalFareArray.append(self.MycardTotalFare)

                                    }
                                    durationLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 50, y: 0, width: 95, height: 40)
                                    durationLabel.textColor = UIColor.white
                                    durationLabel.backgroundColor = UIColor.black

                                    fareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 160, y: 0, width: 80, height: 40)
                                    Regular.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 160, y: 30, width: 80, height: 40)
                                    TotalMyTarcFareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 245, y: 0, width: 80, height: 40)

                                    mytarccard.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 245, y: 30, width: 80, height: 40)

                                    BuyNowButton.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 335, y: 0, width: 120, height: 40)
                                }
                                else
                                {

                                    durationLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) - 25, y: 0, width: 95, height: 40)
                                    durationLabel.textColor = UIColor.white
                                    durationLabel.backgroundColor = UIColor.black



                                    fareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 85, y: 0, width: 80, height: 40)
                                    Regular.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 85, y: 30, width: 80, height: 40)

                                    TotalMyTarcFareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 175, y: 0, width: 80, height: 40)
                                    mytarccard.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 175, y: 30, width: 80, height: 40)
                                    BuyNowButton.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 270, y: 0, width: 120, height: 40)
                                }
                            }

                            Regular.text = "Regular"
                            Regular.font = UIFont.setTarcBold(size: 11.0)
                            Regular.textAlignment = NSTextAlignment.center
                            Regular.textColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)

                            mytarccard.text = "TARC Ticket "
                            mytarccard.font = UIFont.setTarcBold(size: 11.0)
                            mytarccard.textAlignment = NSTextAlignment.center
                            mytarccard.textColor = UIColor(red: 208 / 255.0, green: 48 / 255.0, blue: 46 / 255.0, alpha: 1.0)
                            self.NewfareString = String(format: "$%.2f", self.TotalFar)
                            fareLabel.text = "\(NewfareString)"
                            fareLabel.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                            fareLabel.textColor = UIColor.white
                            //fareLabel.textColor = UIColor.black
                            fareLabel.font = UIFont.setTarcBold(size: 18.0)
                            fareLabel.textAlignment = NSTextAlignment.center

                            TotalMyTarcFareLabel.backgroundColor = UIColor(red: 208 / 255.0, green: 48 / 255.0, blue: 46 / 255.0, alpha: 1.0)
                            TotalMyTarcFareLabel.textColor = UIColor.white
                            //fareLabel.textColor = UIColor.black
                            TotalMyTarcFareLabel.font = UIFont.setTarcBold(size: 18.0)
                            TotalMyTarcFareLabel.textAlignment = NSTextAlignment.center


                            BuyNowButton.setTitle("Buy Ticket", for: .normal)
                            BuyNowButton.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                            BuyNowButton.addTarget(self, action: #selector(self.BuyTicketAction), for: .touchUpInside)
                            BuyNowButton.tag = indexPath.row
                                BuyNowButton.titleLabel?.font =  UIFont.setTarcBold(size: 12.0)

                                let cellImage = UIImageView(frame: CGRect(x: BuyNowButton.frame.maxX-40, y: BuyNowButton.frame.maxY - 40, width: 40, height: 30))
                               // cellImage.image = UIImage(named: "new.gif")
                                cellImage.image = UIImage.gif(name: "new")
                               // cell.addSubview(cellImage)

                            if MycardTotalFareArray.count > 0 {
                                self.MycardTotalFare = MycardTotalFareArray.max()!
                            }
                            TotalMyTarcFareLabel.text = String(format: "$%.2f", self.MycardTotalFare)

                            //                        print(self.busNameStrin)
                            //                        print(self.busNameStrin.contains("X"))


                            if TransitAvalible == true
                                {
//                                fareLabel.frame = CGRect.zero
//                                TotalMyTarcFareLabel.frame = CGRect.zero
//                                mytarccard.frame = CGRect.zero
//                                Regular.frame = CGRect.zero

                                cell.transitScrollView.addSubview(fareLabel)
                                cell.transitScrollView.addSubview(TotalMyTarcFareLabel)
                                cell.transitScrollView.addSubview(mytarccard)
                                cell.transitScrollView.addSubview(Regular)
                            }
                            else
                            {
                                cell.transitScrollView.addSubview(fareLabel)
                                cell.transitScrollView.addSubview(TotalMyTarcFareLabel)
                                cell.transitScrollView.addSubview(mytarccard)
                                cell.transitScrollView.addSubview(Regular)
                            }

                            if comVar.DateImplementation {
                                cell.transitScrollView.addSubview(BuyNowButton)
                                cell.transitScrollView.addSubview(cellImage)
                            }





                            cell.transitScrollView.contentSize = CGSize(width: BuyNowButton.frame.origin.x + BuyNowButton.frame.size.width + 30, height: 60)
                        }
                        cell.contentView.addSubview(cell.transitScrollView)
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.TransitTap(_:)))
                        tapGesture.numberOfTapsRequired = 1
                        cell.transitScrollView.tag = indexPath.row
                        cell.transitScrollView.addGestureRecognizer(tapGesture)


                        //                    let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
                        //                    tapGesture1.numberOfTapsRequired = 1
                        //                    fareLabel.tag = indexPath.row
                        //                    fareLabel.addGestureRecognizer(tapGesture1)
                        //
                        //                    let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
                        //                    cell.transitScrollView.tag = indexPath.row
                        //                    //cell.transitScrollView.addGestureRecognizer(longPressRecognizer)

                    }
                    return cell
                }
                else if skippedArray[indexPath.section] as? String == "Bikeshare"
                    {
                    let cell: TripSelectionTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TripSelectionTableViewCell?)!
                    tableView.separatorColor = UIColor.clear;
                    cell.selectionStyle = .none
                    print("\(self.view.frame.size.width)")
                    //  cell.contentView.backgroundColor = UIColor.red
                    let firstButton = UIButton()
                    firstButton.frame = CGRect.init(x: 10, y: 0, width: self.view.frame.size.width / 2, height: 50)
                    var image: UIImage? = UIImage(named: "Image-7")?.withRenderingMode(.alwaysOriginal)

                    firstButton.setImage(image, for: .normal)
                    firstButton.backgroundColor = UIColor.white
                    firstButton.imageView?.image = image
                    firstButton.imageView?.tintColor = UIColor.gray.withAlphaComponent(0.5)
                        firstButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                    firstButton.titleLabel?.numberOfLines = 2
                    firstButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                    firstButton.addTarget(self, action: #selector(tapBlurBirdButton), for: .touchUpInside)
                    firstButton.setTitleColor(UIColor.black, for: .normal)
                    firstButton.contentHorizontalAlignment = .left
                    cell.contentView .addSubview(firstButton)

                    if self.BirdDatalistapicount != nil {
                        if self.BirdDatalistapicount! > 0 {
                            let suggWalking = self.DataWalkingResponse.result.value?.route![0].legs
                            // for DataWalkingResponse in (suggWalking?.route)! {

                            for LegsData in suggWalking! {
                                self.WalkingDistance = (LegsData.distance?.text)!
                                self.WalkingDuration = (LegsData.duration?.value)!
                                print(self.WalkingDuration)
                            }

                            let valueResult = dataBikeResponse.result.value
                            if (valueResult?.routes!.count)! > 0 {
                            let routesPlanBIKE = valueResult?.routes![0].legs

                            let durationStringADD = routesPlanBIKE![0].duration?.value
                            let sectominCalWithFare: Double = (Double(durationStringADD! / 60).rounded(.toNearestOrAwayFromZero))
                            print(((sectominCalWithFare * 0.15) + 1))
                            let BirdBike_fare_Cal = ((sectominCalWithFare * 0.15) + 1)
                            print(self.WalkingDuration)
                            print(self.BirdDatalistapicount!)
                            if self.WalkingDuration < 500 && self.BirdDatalistapicount! > 0 && self.WalkingDuration > durationStringADD!{

//                                var maxfare = (BirdBike_fare_Cal/10)
//                                if maxfare < 1
//                                {
//                                    maxfare = 1
//                                }
//                                else{
//                                    maxfare = Int(maxfare)
//                                }
                               let totalvalue = BirdBike_fare_Cal.rounded(.toNearestOrAwayFromZero)
                                let totalString = String(format: "$%.2f", totalvalue)


                                firstButton.setTitle(totalString, for: .normal)
                                firstButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                                bikeCostForAnalytics = Int(BirdBike_fare_Cal)

                            }
                            else
                            {

                                firstButton.setTitle("not \navailable", for: .normal)
                                firstButton.isEnabled = false
                                firstButton.alpha = 0.5



                            }
                            }
                            else{
                                firstButton.setTitle("not \navailable", for: .normal)
                                                           firstButton.isEnabled = false
                                                           firstButton.alpha = 0.5
                            }
                        }
                        else
                        {
                            firstButton.setTitle("not \navailable", for: .normal)
                            firstButton.isEnabled = false
                            firstButton.alpha = 0.5

                        }


                    }
                    else
                    {
                        firstButton.setTitle("not \navailable", for: .normal)
                        firstButton.isEnabled = false
                        firstButton.alpha = 0.5
                    }





                    let secondButton = UIButton()
                    secondButton.frame = CGRect.init(x: firstButton.frame.maxX + 0, y: 0, width: self.view.frame.size.width / 2, height: 50)
                    secondButton.setImage(UIImage(named: "Image-8"), for: .normal)
                    secondButton.backgroundColor = UIColor.white
                    secondButton.imageView?.image = UIImage.init(named: "Image-8")
                    secondButton.titleLabel?.numberOfLines = 2
                    secondButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                    secondButton.addTarget(self, action: #selector(BolttapBlurLimeButton(_:)), for: .touchUpInside)
                    secondButton.setTitleColor(UIColor.black, for: .normal)
                        secondButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                    secondButton.contentHorizontalAlignment = .left
                    cell.contentView .addSubview(secondButton)

                    if self.BoltDatalistapicount != nil {
                        if self.BoltDatalistapicount! > 0 {
                            if BoltdataBikeResponse != nil {
                                if BoltdataBikeResponse.result.value != nil
                                    {
                                        let suggWalking = self.BoltDataWalkingResponse.result.value?.route![0].legs
                                    // for DataWalkingResponse in (suggWalking?.route)! {

                                    for LegsData in suggWalking! {
                                        self.BoltWalkingDistance = (LegsData.distance?.text)!
                                        self.BoltWalkingDuration = (LegsData.duration?.value)!
                                        print(self.WalkingDuration)
                                        // }
                                    }

                                    let valueResult = BoltdataBikeResponse.result.value
                                    let routesPlanBIKE = valueResult?.routes![0].legs
                                    let durationStringADD = routesPlanBIKE![0].duration?.value
                                    let sectominCalWithFare: Double = (Double(durationStringADD! / 60).rounded(.toNearestOrAwayFromZero))
                                    print(((sectominCalWithFare * 0.15) + 1))
                                    let BirdBike_fare_Cal = (sectominCalWithFare * 0.35)
                                    print(self.BoltWalkingDuration)
                                    print(self.BoltDatalistapicount!)
                                    print("BIRD*************")
                                        if  self.BoltWalkingDuration < 500 && self.BoltDatalistapicount! > 0 && self.BoltWalkingDuration > durationStringADD!{

//
                                   let totalvalue = BirdBike_fare_Cal.rounded(.toNearestOrAwayFromZero)
                                    let totalString = String(format: "$%.2f", totalvalue)


                                    secondButton.setTitle(totalString, for: .normal)
                                     }else{
                                        secondButton.setTitle("not \navailable", for: .normal)
                                        secondButton.isEnabled = false
                                        secondButton.alpha = 0.5
                                        }
                                }else{
                                    secondButton.setTitle("not \navailable", for: .normal)
                                    secondButton.isEnabled = false
                                    secondButton.alpha = 0.5
                                }
                            }
                            else
                            {
                                secondButton.setTitle("not \navailable", for: .normal)
                                secondButton.isEnabled = false
                                secondButton.alpha = 0.5

                            }
                        }
                        else {
                            secondButton.setTitle("not \navailable", for: .normal)
                            secondButton.isEnabled = false
                            secondButton.alpha = 0.5

                        }

                    }
                    else
                    {
                        secondButton.setTitle("not \navailable", for: .normal)
                        secondButton.isEnabled = false
                        secondButton.alpha = 0.5

                    }




                    let thirdButton = UIButton()
                    thirdButton.frame = CGRect.init(x: 10, y: 60, width: self.view.frame.size.width / 2, height: 50)
                    thirdButton.setImage(UIImage(named: "Image-9"), for: .normal)
                    thirdButton.backgroundColor = UIColor.white
                    thirdButton.imageView?.image = UIImage.init(named: "Image-9")
                    thirdButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                    thirdButton.addTarget(self, action: #selector(tapBlurLimeButton(_:)), for: .touchUpInside)
                        thirdButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                    thirdButton.titleLabel?.numberOfLines = 2
                    thirdButton.setTitleColor(UIColor.black, for: .normal)
                    thirdButton.contentHorizontalAlignment = .left
                    cell.contentView .addSubview(thirdButton)

                    if self.LimeDatalistapicount != nil {
                        if self.LimeDatalistapicount! > 0 {
                            if LimedataBikeResponse != nil
                                {
                                if LimedataBikeResponse.result.value != nil
                                    {
                                    let suggWalking = self.LimeDataWalkingResponse.result.value?.route![0].legs
                                    // for DataWalkingResponse in (suggWalking?.route)! {

                                    for LegsData in suggWalking! {
                                        self.LimeWalkingDistance = (LegsData.distance?.text)!
                                        self.LimeWalkingDuration = (LegsData.duration?.value)!
                                        print(self.WalkingDuration)
                                        // }
                                    }

                                    let valueResultss = LimedataBikeResponse.result.value
                                    let routesPlanBIKEs = valueResultss?.routes![0].legs
                                    let durationStringADDs = routesPlanBIKEs![0].duration?.value

                                    let sectominCalWithFares: Double = (Double(durationStringADDs! / 60).rounded(.toNearestOrAwayFromZero))

                                    let BirdBike_fare_Cals = ((sectominCalWithFares * 0.27) + 1)
                                    print(BirdBike_fare_Cals)
                                    print(sectominCalWithFares)
                                    print(self.LimeWalkingDuration)
                                    print(self.LimeDatalistapicount!)

                                    print("lime*************")
                                        if self.LimeWalkingDuration < 500 && self.LimeDatalistapicount! > 0 &&  self.LimeWalkingDuration > durationStringADDs!{

                                        let totalvalue = BirdBike_fare_Cals.rounded(.toNearestOrAwayFromZero)
                                                                               let totalString = String(format: "$%.2f", totalvalue)


                                        thirdButton.setTitle(totalString, for: .normal)


                                    }
                                    else
                                    {
                                        var context = CIContext(options: nil)

                                        thirdButton.setTitle("not \navailable", for: .normal)
                                        thirdButton.isEnabled = false
                                        thirdButton.alpha = 0.5

                                    }
                                }
                                    else
                                    {
                                        thirdButton.setTitle("not \navailable", for: .normal)
                                        thirdButton.isEnabled = false
                                        thirdButton.alpha = 0.5
                                    }
                            }
                            else
                            {
                                thirdButton.setTitle("not \navailable", for: .normal)
                                thirdButton.isEnabled = false
                                thirdButton.alpha = 0.5
                            }
                        }
                        else
                        {
                            thirdButton.setTitle("not \navailable", for: .normal)
                            thirdButton.isEnabled = false
                            thirdButton.alpha = 0.5
                        }
                    }
                    else
                    {
                        thirdButton.setTitle("not \navailable", for: .normal)
                        thirdButton.isEnabled = false
                        thirdButton.alpha = 0.5
                    }






                    let fourthButton = UIButton()
                    fourthButton.frame = CGRect.init(x: thirdButton.frame.maxX + 0, y: 60, width: self.view.frame.size.width / 2, height: 50)
                    fourthButton.setImage(UIImage(named: "louveloSelect"), for: .normal)
                    fourthButton.backgroundColor = UIColor.white
                    fourthButton.imageView?.image = UIImage.init(named: "louveloSelect")?.noirs
                    fourthButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                    fourthButton.addTarget(self, action: #selector(tapLouveloButton), for: .touchUpInside)

                        fourthButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                    fourthButton.titleLabel?.numberOfLines = 2
                    fourthButton.setTitleColor(UIColor.black, for: .normal)
                    fourthButton.contentHorizontalAlignment = .left
                    cell.contentView .addSubview(fourthButton)


                    if SourceLouveloLat != nil && SourceLouveloLong != nil && DestinationLouveloLat != nil && DestinationLouveloLong != nil && self.SourceLouveloDistance != nil && self.DestinationloDistance != nil {
                        
                        let Birdrequest = MKDirections.Request()
                        Birdrequest.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: SourceLouveloLat!, longitude: SourceLouveloLong!), addressDictionary: nil))
                        Birdrequest.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: DestinationLouveloLat!, longitude: DestinationLouveloLong!), addressDictionary: nil))
                        Birdrequest.requestsAlternateRoutes = true
                        Birdrequest.transportType = .automobile

                        let Birddirections = MKDirections(request: Birdrequest)

                        Birddirections.calculate { [unowned self] response, error in
                            guard let BirdResponse = response else { return }

                            //for Birdroute in BirdResponse.routes {

                            print(BirdResponse)
                            var Birdroute_Array = MKRoute()
                            Birdroute_Array = BirdResponse.routes.first!
                            print(Birdroute_Array.distance)
                            print(Birdroute_Array.expectedTravelTime)

                            let durationStringADD = Birdroute_Array.expectedTravelTime
                            let sectominCalWithFare: Double = (Double(durationStringADD / 60).rounded(.toNearestOrAwayFromZero))
                            print(sectominCalWithFare)

                            let louvelodivision = sectominCalWithFare / 30
                            let roundedupValue = louvelodivision.rounded(.up)
                            let totalAmount = roundedupValue * 3.5

                            fourthButton.isEnabled = true
                            print(self.SourceLouveloDistance)
                            print(self.DestinationloDistance)

                          //  if  {
                            if self.SourceLouveloDistance < 500 && self.DestinationloDistance < 500 {
                              //  self.hideLoader()

                            fourthButton.alpha = 1.0
                            let totalString = String(format: "$%.2f", totalAmount)
                            fourthButton.setTitle(totalString, for: .normal)
                            }
                            else{
                             //   self.hideLoader()

                                fourthButton.setTitle("not \navailable", for: .normal)
                                                       fourthButton.isEnabled = false
                                                       fourthButton.alpha = 0.5
                            }
//                        }
//                            else{
//                                fourthButton.setTitle("not \navailable", for: .normal)
//                                fourthButton.isEnabled = false
//                                fourthButton.alpha = 0.5
//                            }
                            }

                    }
                    else
                    {
                     //   self.hideLoader()

                        fourthButton.setTitle("not \navailable", for: .normal)
                        fourthButton.isEnabled = false
                        fourthButton.alpha = 0.5
                    }

                    return cell
                }
                else
                {
                    let cell: TripSelectionTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TripSelectionTableViewCell?)!
                    tableView.separatorColor = UIColor.clear;
                    cell.selectionStyle = .none

                    let firstButton = UIButton()
                    firstButton.frame = CGRect.init(x: 10, y: 0, width: cell.contentView.frame.size.width / 2, height: cell.contentView.frame.size.height)
                    firstButton.setImage(UIImage(named: "Uber-1"), for: .normal)
                    firstButton.backgroundColor = UIColor.white
                    firstButton.imageView?.image = UIImage.init(named: "Uber-1")

                    let percentagelowvalue = comVarUber.Lyft_estimationPrice / 100
                    let percentagehighvalue = comVarUber.Lyft_estimationPriceHigh / 100
                    let calculatelowper = Double(percentagelowvalue) / 10 as Double
                    let calculatehighper = Double(percentagehighvalue) / 10 as Double
                    let totalvalueofLow = Double(percentagelowvalue) + calculatelowper
                    let totalvalueofHigh = Double(percentagehighvalue) + calculatehighper
                    print("NEw Total value \(totalvalueofLow) ad \(totalvalueofHigh)")
                    let xvalue = totalvalueofLow.rounded()
                    print("\(xvalue)")
                    let yvalue = totalvalueofHigh.rounded()
                    print("\(yvalue)")
                    let xintValue = Int(xvalue)
                    let yintValue = Int(yvalue)
                    var uberTitleString = ""
                                      comFunc.getLyftAPIFirst(SourceLat: sourceLat, SourceLong: sourceLng, DestinationLat: destinationLat, DestinationLong: destinationLng) { (MinPrice, MaxPrice, TravelMin, travelMile, Status) in
                                                   if Status {
                                                       //
                                                       var overallDistance = 0.0
                                                       let minutes = TravelMin / 60
                                                       let LyftTravelTime = minutes ;
                                                       print(LyftTravelTime)
                                                       overallDistance = self.distance(lat1: self.sourceLat, lon1: self.sourceLng, lat2: self.destinationLat, lon2: self.destinationLng, unit: "M")
                                                       print(overallDistance)
                                                       var calculateMiles = travelMile / 1609.344
                                                       // calculateMiles = 2.3
                                                       if calculateMiles == 1000
                                                           {
                                                           calculateMiles = overallDistance
                                                       }
                                                       var estimatetime = Double(LyftTravelTime)
                                                       if TravelMin == 300
                                                           {
                                                           estimatetime = calculateMiles / 20
                                                       }
                                                       var timeCalculation = estimatetime * 0.28
                                                       print(timeCalculation)
                                                       let calculateoverallMiles = calculateMiles * 0.80
                                                       print(calculateoverallMiles)
                                                       var overallTotal = calculateoverallMiles + 2.30 + timeCalculation
                                                       print(overallTotal)
                                                       overallTotal = overallTotal.rounded(.toNearestOrAwayFromZero)
                                                       // var minimumfareFloat  = NSString()
                                                       var minimumfare = overallTotal - 1
                                                       if minimumfare <= 5.79
                                                           {
                                                           minimumfare = 5.80
                                                           overallTotal = minimumfare
                                                       }
                                                       let minimumfareFloat = Int(minimumfare)
                                                       let maximumfareFloat = overallTotal + 1
                                                       // var maxString  = NSString()
                                                     let minString = minimumfare.rounded(.toNearestOrAwayFromZero)
                                                       let maxString = maximumfareFloat.rounded(.toNearestOrAwayFromZero)
                                                    uberTitleString = "$\(minString) - $\(maxString)"
                                                       firstButton.setTitle(uberTitleString, for: .normal)
                                                    firstButton.isEnabled = true

                                                       print(MinPrice)
                                                       print(MaxPrice)
                                                   }
                                               }
                     
                   
                    // firstButton.setTitle("$\(xintValue)-$\(yintValue)", for: .normal)
                    firstButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                    firstButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                    firstButton.addTarget(self, action: #selector(UberButtonTapFunction), for: .touchUpInside)

                    firstButton.setTitleColor(UIColor.black, for: .normal)
                    firstButton.contentHorizontalAlignment = .left
                    cell.contentView .addSubview(firstButton)
                   

                    let secondButton = UIButton()
                    secondButton.frame = CGRect.init(x: firstButton.frame.maxX + 0, y: 0, width: cell.contentView.frame.size.width / 2, height: cell.contentView.frame.size.height)
                    secondButton.setImage(UIImage(named: "Lyft"), for: .normal)
                    secondButton.backgroundColor = UIColor.white
                    // firstButton.addTarget(self, action: #selector(lyftFunction), for: .touchUpInside)

                    
                    secondButton.imageView?.image = UIImage.init(named: "Lyft")



                    //
                    var overallDistance = 0.0
                    print(comVarUber.LyftDistnce)
                    print(comVarUber.Lyft_Travel_Min)
                    print(comVarUber.LyftDistnce)
                    let LyftTravelTime = self.hourminConverterForUber(distValue: comVarUber.Lyft_Travel_Min + 300)
                    print(LyftTravelTime)

                    overallDistance = self.distance(lat1: self.sourceLat, lon1: self.sourceLng, lat2: self.destinationLat, lon2: self.destinationLng, unit: "M")
                    print(overallDistance)
                    var calculateMiles = comVarUber.LyftDistnce
                    // calculateMiles = 2.3
                    if calculateMiles == 1000
                        {
                        calculateMiles = overallDistance
                    }

                    var estimatetime = Double(LyftTravelTime)
                    if comVarUber.Lyft_Travel_Min == 300
                        {
                        estimatetime = calculateMiles / 20
                    }
                    var timeCalculation = estimatetime! * 0.28
                    print(timeCalculation)
                    let calculateoverallMiles = calculateMiles * 0.80
                    print(calculateoverallMiles)
                    var overallTotal = calculateoverallMiles + 2.30 + timeCalculation
                    print(overallTotal)
                    overallTotal = overallTotal.rounded(.toNearestOrAwayFromZero)

                    // var minimumfareFloat  = NSString()
                    var minimumfare = overallTotal - 1
                    if minimumfare <= 5.79
                        {
                        minimumfare = 5.80
                        overallTotal = minimumfare
                    }
                    let minimumfareFloat = Int(minimumfare)
                    let maximumfareFloat = overallTotal + 1

                    // var maxString  = NSString()
                    let maxString = Int(maximumfareFloat)




                    secondButton.setTitle("$\(minimumfareFloat)-$\(maxString)", for: .normal)
                    secondButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                    secondButton.addTarget(self, action: #selector(lyftFunction), for: .touchUpInside)

                    secondButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                    secondButton.titleLabel?.numberOfLines = 2
                    secondButton.setTitleColor(UIColor.black, for: .normal)
                    secondButton.contentHorizontalAlignment = .left
                    cell.contentView .addSubview(secondButton)
                    firstButton.alpha = 1.0
                    print("\(uberTitleString)")
                    if uberTitleString == ""
                    {
                        firstButton.titleLabel?.numberOfLines = 2
                        firstButton.setTitle("not\navailable", for: .normal)
                        firstButton.isEnabled = false
                                                          // firstButton.alpha = 0.5
                                                           
                    }
                    else
                    {
                        firstButton.isEnabled = true

                    }
                    
                    secondButton.alpha = 1.0
                    secondButton.isEnabled = true
                    return cell
                }




            }

        }



        // set the text from the data model

    }

    @objc func UberButtonTapFunction() {
        print("TAP********")
        //getActivity analytics.geta





        let pickupLocation = CLLocation(latitude: self.sourceLat, longitude: self.sourceLng)
        let dropoffLocation = CLLocation(latitude: self.destinationLat, longitude: self.destinationLng)
        print("\(pickupLocation) and \(dropoffLocation)")


        let builder = RideParametersBuilder()
        builder.pickupLocation = pickupLocation
        builder.dropoffLocation = dropoffLocation
        let rideParameters = builder.build()

        let deeplink = RequestDeeplink(rideParameters: rideParameters, fallbackType: .mobileWeb)
        deeplink.execute()




    }
    @objc func lyftFunction()
    {





        let app2Url: URL = URL(string: "lyft://")!

        if UIApplication.shared.canOpenURL(app2Url) {
            open(scheme: "lyft://ridetype?id=lyft&pickup[latitude]=\(self.sourceLat)&pickup[longitude]=\(self.sourceLng)&destination[latitude]=\(self.destinationLat)&destination[longitude]=\(self.destinationLng)")





        }
        else {

            UIApplication.shared.openURL(NSURL(string: "https://www.lyft.com/signup/SDKSIGNUP")! as URL)

        }
    }
    func open(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////
    ///  This function converts decimal degrees to radians              ///
    ///////////////////////////////////////////////////////////////////////
    func deg2rad(deg: Double) -> Double {
        return deg * M_PI / 180
    }

    ///////////////////////////////////////////////////////////////////////
    ///  This function converts radians to decimal degrees              ///
    ///////////////////////////////////////////////////////////////////////
    func rad2deg(rad: Double) -> Double {
        return rad * 180.0 / M_PI
    }
    func distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double, unit: String) -> Double {
        let theta = lon1 - lon2
        var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta))
        dist = acos(dist)
        dist = rad2deg(rad: dist)
        dist = dist * 60 * 1.1515
        if (unit == "K") {
            dist = dist * 1.609344
        }
        else if (unit == "N") {
            dist = dist * 0.8684
        }
        return dist
    }
    @objc func BuyTicketAction(sender: UIButton!) {

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                   let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                   self.navigationController?.pushViewController(nextViewController, animated: true)
                   
                   


    }
    @objc func tapLouveloButton(_ sender: UITapGestureRecognizer) {
        var dateInSeconds: TimeInterval
              dateInSeconds = tripSchduleTime.timeIntervalSince1970
        let walkingurl = "https://maps.googleapis.com/maps/api/directions/json"
        let walkingorgin = "\(self.sourceLat),\(self.sourceLng)"
        let walkingdestination = "\(self.SourceLouveloLat!),\(self.SourceLouveloLong!)"
        print("\(self.SourceLouveloLat!),\(self.SourceLouveloLong!) Louvelo - ")
        print("\(self.sourceLat),\(self.sourceLng) - Walking")

        let SourceLouvelolatlong = "\(self.SourceLouveloLat!),\(self.SourceLouveloLong!)"

        let DestinationLouvelolatlong = "\((self.DestinationLouveloLat)!),\((self.DestinationLouveloLong)!)"
        let walkingenglatlong = "\(self.destinationLat),\(self.destinationLng)"
        let WalkingParametersSatrt: Parameters = [
            "origin": walkingorgin,
            "destination": walkingdestination,
            "mode": "walking",
            "DepartTime": dateInSeconds,
            "alternatives": "true",
            "key": Key.GoogleAPIKey

        ]
        let BikeParameters: Parameters = [
            "origin": SourceLouvelolatlong,
            "destination": DestinationLouvelolatlong,
            "mode": "bicycling",
            "DepartTime": dateInSeconds,
            "alternatives": "true",
            "key": Key.GoogleAPIKey

        ]
        let WalkingParametersEnd: Parameters = [
            "origin": DestinationLouvelolatlong,
            "destination": walkingenglatlong,
            "mode": "walking",
            "DepartTime": dateInSeconds,
            "alternatives": "true",
            "key": Key.GoogleAPIKey

        ]
        print("\(walkingurl)?origin=\(DestinationLouvelolatlong)&destination=\(walkingenglatlong)&mode=walking&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)")

        Alamofire.request(walkingurl, method: .get, parameters: WalkingParametersSatrt, encoding: URLEncoding.default)
            .responseObject { (Startresponse: DataResponse<SuggestWalkingDirectionalClass>) in
                switch Startresponse.result {
                case .success:
                    self.DataWalkingResponseLouveloStart = Startresponse
                    Alamofire.request(walkingurl, method: .get, parameters: BikeParameters, encoding: URLEncoding.default)
                        .responseObject { (Bikeresponse: DataResponse<SuggestBikeDirectionalClass>) in
                            switch Bikeresponse.result {
                            case .success:
                                self.louveloBikeResponse = Bikeresponse


                                Alamofire.request(walkingurl, method: .get, parameters: WalkingParametersEnd, encoding: URLEncoding.default)
                                    .responseObject { (Endresponse: DataResponse<SuggestWalkingDirectionalClass>) in
                                        switch Endresponse.result {
                                        case .success:
                                            self.DataWalkingResponseLouveloEnd = Endresponse
                                            
                                            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                                   let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripMapViewController") as! TripMapViewController

                                                   nextViewController.AmenititesTypeTrip = self.AmenititesTypeTrip
                                                   nextViewController.ameniticsTitle = self.ameniticsTitle
                                                   nextViewController.BikeCost = self.bikeCostForAnalytics

                                                   nextViewController.databikeResponseFromDirection = self.dataBikeResponse
                                                   nextViewController.dataWalkingDirection = self.DataWalkingResponse
                                                   // nextViewController.indexValue = sender.view?.tag ?? 0

                                                   SourceDestination.DisplayType = "LouVelo"
                                                   nextViewController.isLimeredirect = "LouVelo"
                                                   nextViewController.DisplayType = "LouVelo"
                                                   nextViewController.firstMileBool = false
                                                   nextViewController.sourceAddressLat = self.sourceLat
                                                   nextViewController.DestinationLat = self.destinationLat
                                                   nextViewController.LouveloStationSourceLat = self.SourceLouveloLat
                                                   nextViewController.LouveloStationDestinationLat = self.DestinationLouveloLat
                                                 //  nextViewController.LouveloSourceStationID = self.sourceLouveloStationID
                                                   nextViewController.LouveloSourceStationAddress = self.SourceLouveloAddress
                                                //   nextViewController.LouveloDestinationStationID = self.DestinationLouveloStationID
                                                   nextViewController.LouveloDestinationStationAddress = self.DestinationLouveloAddress
                                                   nextViewController.AmenititesTypeTrip = self.AmenititesTypeTrip
                                                   nextViewController.ameniticsTitle = self.ameniticsTitle
                                                   nextViewController.sourceAddressLong = self.sourceLng
                                                   nextViewController.DestinationLong = self.destinationLng
                                                   nextViewController.LouveloStationSourceLong = self.SourceLouveloLong
                                                   nextViewController.LouveloStationDestinationLong = self.DestinationLouveloLong
                                                   nextViewController.isClickedFromLouvile = true
                                            nextViewController.louveloBikeResponse = self.louveloBikeResponse
                                            nextViewController.DataWalkingResponseLouveloStart = self.DataWalkingResponseLouveloStart
                                            nextViewController.DataWalkingResponseLouveloEnd = self.DataWalkingResponseLouveloEnd
                                            nextViewController.SourceAddressString = self.sourceaddressString
                                             nextViewController.DestinationAddressString = self.DestinationAddressString
                                            


                                                   self.navigationController?.pushViewController(nextViewController, animated: true)


                                        case .failure(let error):
                                            print(error)

                                        } }



                            case .failure(let error):
                                print(error)

                            } }



                case .failure(let error):
                    print(error)

                }

        }




       





    }



    //Lime
//    @objc func tapBlurLimeButton(_ sender: UIButton){
//
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        //let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BikeDirectionViewController") as! BikeDirectionViewController
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripMapViewController") as! TripMapViewController
//        let bikeresponse = self.LimedataBikeResponse.result.value?.routes!
//        for dataBike in bikeresponse! {
//            let legsdatabike = dataBike.legs!
//            for legsdatavalue in legsdatabike {
//                nextViewController.bikefromaddress = legsdatavalue.startAddress
//                nextViewController.biketoAddess = legsdatavalue.endAddress
//            }
//
//        }
//
//        let walkingeresponse = self.LimeDataWalkingResponse.result.value?.route!
//        for datawalking in walkingeresponse!{
//            let datalegs = datawalking.legs!
//            for datalegsdata in datalegs {
//                nextViewController.walkingfromaddress = datalegsdata.start_address!
//                nextViewController.walkingtoAddess = datalegsdata.end_address!
//            }
//        }
//        nextViewController.firstMileBool = false
//        nextViewController.AmenititesTypeTrip = self.AmenititesTypeTrip
//        nextViewController.ameniticsTitle = self.ameniticsTitle
//        nextViewController.BikeCost = self.bikeCostForAnalytics
//        nextViewController.isLimeredirect = "Lime"
//        nextViewController.databikeResponseFromDirection = self.LimedataBikeResponse
//        nextViewController.dataWalkingDirection = self.LimeDataWalkingResponse
//        // nextViewController.indexValue = sender.view?.tag ?? 0
//
//        nextViewController.LimeDatalistapicount = LimeDatalistapicount
//
//        nextViewController.LimeDataWalkingResponse = LimeDataWalkingResponse
//
//        nextViewController.LimedataBikeResponse = LimedataBikeResponse
//
//        nextViewController.LimeWalkingDuration = LimeWalkingDuration
//
//        nextViewController.isClickedFromLouvile = false
//
//
//        //
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//
//    }

    //Bolt
    @objc func BolttapBlurLimeButton(_ sender: UIButton){

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        //let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BikeDirectionViewController") as! BikeDirectionViewController
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripMapViewController") as! TripMapViewController
        let bikeresponse = self.BoltdataBikeResponse.result.value?.routes!
        for dataBike in bikeresponse! {
            let legsdatabike = dataBike.legs!
            for legsdatavalue in legsdatabike {
                nextViewController.bikefromaddress = legsdatavalue.startAddress
                nextViewController.biketoAddess = legsdatavalue.endAddress
            }

        }

        let walkingeresponse = self.BoltDataWalkingResponse.result.value?.route!
        for datawalking in walkingeresponse!{
            let datalegs = datawalking.legs!
            for datalegsdata in datalegs {
                nextViewController.walkingfromaddress = datalegsdata.start_address!
                nextViewController.walkingtoAddess = datalegsdata.end_address!
            }
        }
        nextViewController.firstMileBool = false
        nextViewController.AmenititesTypeTrip = self.AmenititesTypeTrip
        nextViewController.ameniticsTitle = self.ameniticsTitle
        nextViewController.BikeCost = self.bikeCostForAnalytics
        nextViewController.isLimeredirect = "Bolt"
        SourceDestination.DisplayType = "Bike"
        nextViewController.DisplayType = "Bike"
        nextViewController.databikeResponseFromDirection = self.BoltdataBikeResponse
        nextViewController.dataWalkingDirection = self.BoltDataWalkingResponse
        // nextViewController.indexValue = sender.view?.tag ?? 0
        nextViewController.LimeDatalistapicount = BoltDatalistapicount

        nextViewController.LimeDataWalkingResponse = BoltDataWalkingResponse

        nextViewController.LimedataBikeResponse = BoltdataBikeResponse

        nextViewController.LimeWalkingDuration = BoltWalkingDuration
        nextViewController.isClickedFromLouvile = false
        nextViewController.SourceAddressString = self.sourceaddressString
        nextViewController.DestinationAddressString = self.DestinationAddressString
        nextViewController.sourceAddressLong = self.sourceLng
        nextViewController.DestinationLong = self.destinationLng
        nextViewController.sourceAddressLat = self.sourceLat
        nextViewController.DestinationLat = self.destinationLat
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }


    @objc func tapBlurBirdButton(_ sender: UIButton) {
        //if self.
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripMapViewController") as! TripMapViewController

        //        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BikeDirectionViewController") as! BikeDirectionViewController
        let bikeresponse = self.dataBikeResponse.result.value?.routes!
        for dataBike in bikeresponse! {
            let legsdatabike = dataBike.legs!
            for legsdatavalue in legsdatabike {
                nextViewController.bikefromaddress = legsdatavalue.startAddress
                nextViewController.biketoAddess = legsdatavalue.endAddress
            }

        }

        let walkingeresponse = self.DataWalkingResponse.result.value?.route!
        for datawalking in walkingeresponse! {
            let datalegs = datawalking.legs!
            for datalegsdata in datalegs {
                nextViewController.walkingfromaddress = datalegsdata.start_address!
                nextViewController.walkingtoAddess = datalegsdata.end_address!
            }
        }

        if birdListApi == nil {

        }
        nextViewController.DisplayType = "Bike"
        SourceDestination.DisplayType = "Bike"
        nextViewController.AmenititesTypeTrip = self.AmenititesTypeTrip
        nextViewController.firstMileBool = false
        nextViewController.ameniticsTitle = self.ameniticsTitle
        nextViewController.BikeCost = self.bikeCostForAnalytics
        nextViewController.isLimeredirect = "Bird"
        nextViewController.SourceAddressString = self.sourceaddressString
         nextViewController.DestinationAddressString = self.DestinationAddressString
        nextViewController.databikeResponseFromDirection = self.dataBikeResponse
        nextViewController.dataWalkingDirection = self.DataWalkingResponse
        // nextViewController.indexValue = sender.view?.tag ?? 0
        nextViewController.isClickedFromLouvile = false
        nextViewController.sourceAddressLong = self.sourceLng
               nextViewController.DestinationLong = self.destinationLng
               nextViewController.sourceAddressLat = self.sourceLat
               nextViewController.DestinationLat = self.destinationLat
        self.navigationController?.pushViewController(nextViewController, animated: true)
       
    }


    //Lime
    @objc func tapBlurLimeButton(_ sender: UIButton) {

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BikeDirectionViewController") as! BikeDirectionViewController
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripMapViewController") as! TripMapViewController
        let bikeresponse = self.LimedataBikeResponse.result.value?.routes!
        for dataBike in bikeresponse! {
            let legsdatabike = dataBike.legs!
            for legsdatavalue in legsdatabike {
                nextViewController.bikefromaddress = legsdatavalue.startAddress
                nextViewController.biketoAddess = legsdatavalue.endAddress
            }

        }

        let walkingeresponse = self.LimeDataWalkingResponse.result.value?.route!
        for datawalking in walkingeresponse! {
            let datalegs = datawalking.legs!
            for datalegsdata in datalegs {
                nextViewController.walkingfromaddress = datalegsdata.start_address!
                nextViewController.walkingtoAddess = datalegsdata.end_address!
            }
        }
        nextViewController.firstMileBool = false
        nextViewController.AmenititesTypeTrip = self.AmenititesTypeTrip
        nextViewController.ameniticsTitle = self.ameniticsTitle
        nextViewController.BikeCost = self.bikeCostForAnalytics
        nextViewController.isLimeredirect = "Lime"
        SourceDestination.DisplayType = "Bike"
        nextViewController.databikeResponseFromDirection = self.LimedataBikeResponse
        nextViewController.dataWalkingDirection = self.LimeDataWalkingResponse
        // nextViewController.indexValue = sender.view?.tag ?? 0
        nextViewController.SourceAddressString = self.sourceaddressString
         nextViewController.DestinationAddressString = self.DestinationAddressString
        nextViewController.LimeDatalistapicount = LimeDatalistapicount

        nextViewController.LimeDataWalkingResponse = LimeDataWalkingResponse

        nextViewController.LimedataBikeResponse = LimedataBikeResponse

        nextViewController.LimeWalkingDuration = LimeWalkingDuration

        nextViewController.isClickedFromLouvile = false
        nextViewController.sourceAddressLong = self.sourceLng
        nextViewController.DestinationLong = self.destinationLng
        nextViewController.sourceAddressLat = self.sourceLat
        nextViewController.DestinationLat = self.destinationLat

        //
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }


    @objc func TransitTap(_ sender: UITapGestureRecognizer) {

        print("Please Help! %d", sender.view?.tag ?? 0)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        // let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailDescriptionViewController") as! DetailDescriptionViewController

        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripMapViewController") as! TripMapViewController
        nextViewController.AmenititesTypeTrip = self.AmenititesTypeTrip
        nextViewController.ameniticsTitle = self.ameniticsTitle
        nextViewController.dataPrevRespone = dataRespone
        nextViewController.DisplayType = "Transit"
        SourceDestination.DisplayType = "Transit"
        print(sender.view?.tag ?? 0)
        nextViewController.indexValue = sender.view?.tag ?? 0
        //        nextViewController.secIndex = uberSecIndex
        //        nextViewController.uberIndex = uberAddedIndex
        nextViewController.SourceAddressString = self.sourceaddressString
        nextViewController.DestinationAddressString = self.DestinationAddressString
        
        //        nextViewController.sourcePlaceID = self.sourceString
        //        nextViewController.DestinationPlaceID = self.destinationString
        nextViewController.firstMileBool = true
        nextViewController.sourceAddressLong = self.sourceLng
        nextViewController.DestinationLong = self.destinationLng
        nextViewController.sourceAddressLat = self.sourceLat
        nextViewController.DestinationLat = self.destinationLat
        //   nextViewController.SourceAddress =

        if boltListApi == nil {
            SourceDestination.boltListApi = []
        }
        else {
            SourceDestination.boltListApi = boltListApi

        }

        if LimeListApi == nil {
            SourceDestination.LimeListApi = []
        }

        else {
            SourceDestination.LimeListApi = LimeListApi

        }
        if birdListApi == nil {
            SourceDestination.birdListApi = []
        }
        else {
            SourceDestination.birdListApi = birdListApi

        }



        self.navigationController?.pushViewController(nextViewController, animated: true)
        //  self.present(nextViewController, animated:true, completion:nil)




    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == menuTabelView
            {
            return 44.0
        }
        if tableView == fareListTableView
            {
            return 44.0
        }
        else
        {
            if skippedArray[indexPath.section] as? String == "Transit"
                {
                return 70
            }
            else if skippedArray[indexPath.section] as? String == "Bikeshare"
                {
                return 110
            }
            else
            {
                return 50
            }
        }

    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if(tableView == selectProfileTableView) {
            selectedProfileArray.remove(selectProfileArray.object(at: indexPath.row))
        }

        print(selectedProfileArray)
    }




    func loginButton(_ button: LoginButton, didLogoutWithSuccess success: Bool) {
        // success is true if logout succeeded, false otherwise
    }

    func loginButton(_ button: LoginButton, didCompleteLoginWithToken accessToken: AccessToken?, error: NSError?) {
        if let _ = accessToken {
            // AccessToken Saved
            UserDefaults.standard.set(accessToken?.tokenString, forKey: "uberKey")
            UserDefaults.standard.set(accessToken?.refreshToken, forKey: "uberRefershKey")
            //   sugesstionTableView.reloadData()
        }
    }



    func showErrorDialogBox(viewController: UIViewController) {
        let alertViewController = NYAlertViewController()
        alertViewController.title = "Error"
        alertViewController.message = "Server Error! Please try again later!"

        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = viewController.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        // Add alert actions


        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in

                viewController.dismiss(animated: true, completion: nil)
            }
        )

        alertViewController.addAction(cancelAction)

        // Present the alert view controller
        viewController.present(alertViewController, animated: true, completion: nil)

    }

    func ActivitySave() {

        if transitType_Analytics != nil && transitType_Analytics!.count > 0 {

            for TransitType in transitType_Analytics! {


                print(TransitType)


            }


        }


    }

    func getBirdDistance(sourceLat: Double, sourceLng: Double) -> Double
    {
        var BoltCheckBikeCount = 0
        var distanceInMeters: Double?
        var SourceDistance = 0.0
        var tempDistance = 0.0

        let Sourcelatlatlong = CLLocation(latitude: sourceLat, longitude: sourceLng)
        if birdListApi != nil {
            for LimeBike in (birdListApi)! {
                let BirdBikeLat = LimeBike.bike_lat
                let BirdBikeLng = LimeBike.bike_lon


                if BoltCheckBikeCount == 0 {

                    let destinationlatlong = CLLocation(latitude: BirdBikeLat!, longitude: BirdBikeLng!)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    SourceDistance = (distanceInMeters! * 100).rounded() / 100
                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                    SourceDestination.BirdStationDistance = SourceDistance
                    print("\(SourceDistance)")

                }
                else {
                    self.templat = BirdBikeLat!
                    self.templong = BirdBikeLng!
                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    tempDistance = (distanceInMeters! * 100).rounded() / 100
                    if tempDistance < SourceDistance {

                        SourceDistance = tempDistance
                        // BirdStationDistance = SourceDistance
                        print(tempDistance)
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                        SourceDestination.BirdStationDistance = SourceDistance

                    }
                    else {
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                    }
                }





            }
        }
        else {
            SourceDistance = 99999
            SourceDestination.BirdStationDistance = SourceDistance
        }

        return SourceDistance
    }
    func getLimeDisatance(sourceLat: Double, sourceLng: Double) -> Double
    {
        var BoltCheckBikeCount = 0
        var distanceInMeters: Double?
        var SourceDistance = 0.0
        var tempDistance = 0.0

        let Sourcelatlatlong = CLLocation(latitude: sourceLat, longitude: sourceLng)
        if LimeListApi != nil {
            for LimeBike in (LimeListApi)! {
                let BirdBikeLat = LimeBike.bike_lat! as NSString
                let BirdBikeLng = LimeBike.bike_lon! as NSString
                let BoltLatitude = Double("\(BirdBikeLat)")
                let BoltLontitude = Double("\(BirdBikeLng)")

                if BoltCheckBikeCount == 0 {

                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    SourceDistance = (distanceInMeters! * 100).rounded() / 100
                    BoltCheckBikeCount = BoltCheckBikeCount + 1

                    print("\(SourceDistance)")
                    SourceDestination.LimeStationDistance = SourceDistance



                }
                else {
                    self.templat = BoltLatitude!
                    self.templong = BoltLontitude!
                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    tempDistance = (distanceInMeters! * 100).rounded() / 100
                    if tempDistance < SourceDistance {

                        SourceDistance = tempDistance
                        print(tempDistance)
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                        SourceDestination.LimeStationDistance = SourceDistance
                    }
                    else {
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                    }
                }





            }
        }
        else {
            SourceDistance = 99999
            SourceDestination.LimeStationDistance = SourceDistance
        }

        return SourceDistance
    }
    func getBoltDistance(sourceLat: Double, sourceLng: Double) -> Double
    {
        var BoltCheckBikeCount = 0
        var distanceInMeters: Double?
        var SourceDistance = 0.0
        var tempDistance = 0.0

        let Sourcelatlatlong = CLLocation(latitude: sourceLat, longitude: sourceLng)
        if boltListApi != nil {
            for LimeBike in (boltListApi)! {
                let BirdBikeLat = LimeBike.bike_lat! as NSString
                let BirdBikeLng = LimeBike.bike_lon! as NSString
                let BoltLatitude = Double("\(BirdBikeLat)")
                let BoltLontitude = Double("\(BirdBikeLng)")

                if BoltCheckBikeCount == 0 {

                    let destinationlatlong = CLLocation(latitude: BoltLatitude!, longitude: BoltLontitude!)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    SourceDistance = (distanceInMeters! * 100).rounded() / 100
                    BoltCheckBikeCount = BoltCheckBikeCount + 1
                    SourceDestination.BoltStationDistance = SourceDistance
                    print("\(SourceDistance)")




                }
                else {
                    self.templat = BoltLatitude!
                    self.templong = BoltLontitude!
                    let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                    distanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                    tempDistance = (distanceInMeters! * 100).rounded() / 100
                    if tempDistance < SourceDistance {

                        SourceDistance = tempDistance
                        print(tempDistance)
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                        SourceDestination.BoltStationDistance = SourceDistance

                    }
                    else {
                        BoltCheckBikeCount = BoltCheckBikeCount + 1
                    }
                }





            }
        }
        else {
            SourceDistance = 99999
            SourceDestination.BoltStationDistance = SourceDistance
        }

        return SourceDistance
    }




}
extension TripSelectionViewController: RideRequestViewControllerDelegate {
    func rideRequestViewController(_ rideRequestViewController: RideRequestViewController, didReceiveError error: NSError) {
        let errorType = RideRequestViewErrorType(rawValue: error.code) ?? .unknown
        // Handle error here
        switch errorType {
        case .accessTokenMissing:
            print("accesstokenmissin")
            break
        case .accessTokenExpired:
            print("accesstokenexpired")
            break
            // AccessToken expired / invalid
        case .networkError:
            print("networkerror")
            break
            // A network connectivity error
        case .notSupported:
            print("not supported")
            break
            // The attempted operation is not supported on the current device
        default:
            print("errorrrrr")
            break
            // Other error
        }
    }
    func dateconvert(Datevalue: Double) -> String {
        let date = Date(timeIntervalSince1970: Datevalue)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd/MM/yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    func hourminConverter(distValue: Int) -> String {
        let hourString: Int = distValue / 3600
        let minString: Int = (distValue % 3600) / 60
        if hourString > 0
            {
            return "\(hourString) hr \(minString) min"

        }
        else
        {
            return "\(minString) min"
        }
    }
    func hourminConverterForUber(distValue: Int) -> String {
        let minString: Int = (distValue % 3600) / 60

        return "\(minString)"

    }

}


//extension UIImage {
//    var noirs: UIImage? {
//        let context = CIContext(options: nil)
//        guard let currentFilter = CIFilter(name: "CIColorMonochrome") else { return nil }
//        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
//        if let output = currentFilter.outputImage,
//            let cgImage = context.createCGImage(output, from: output.extent) {
//            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
//        }
//        return nil
//    }
//}
extension UIImage {
    var noirs: UIImage {
        guard let ciImage = CIImage(image: self)
            else { return self }
        let filterParameters = [kCIInputColorKey: CIColor.white, kCIInputIntensityKey: 1.0] as [String: Any]
        let grayscale = ciImage.applyingFilter("CIColorMonochrome", parameters: filterParameters)
        return UIImage(ciImage: grayscale)
    }
}

