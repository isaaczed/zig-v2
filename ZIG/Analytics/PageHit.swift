//
//  PageHit.swift
//  ZIG
//
//  Created by Isaac on 29/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//


import Foundation
import UIKit
import Alamofire
import SwiftyJSON

extension analytics {
    static func GetPageHitCount(PageName:String){
        let userDefaults = UserDefaults.standard
        let SessionID = userDefaults.value(forKey: "AnalyticsSessionID")
      //  let DeviceID = userDefaults.value(forKey: "AnalyticsDeviceID")
        if SessionID != nil {
            print(SessionID!)
        print(PageName)
        
        let parameters: [String: Any] = [
            "PageName": PageName,
            "count": 1,
            "sessionId": SessionID!,
            "Application": 1
        ]
       print(parameters)
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)AnalyticsApi/api/AnalyticsDataReport/ActivityMenu", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                case .success:
                    print(response)
                     print("Done Page Hit *********** ")
                    let responseVar = response.result.value
                    
                    let jsonValue = JSON(responseVar!)
                    print(jsonValue["Message"])
                    
                case .failure(let error):
                    print(error)
                    
                }
                
                
                
                
        }
        
        
        
        
        
        
    }
}
}
