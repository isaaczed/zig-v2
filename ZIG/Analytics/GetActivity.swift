//
//  GetActivity.swift
//  ZIG
//
//  Created by Isaac on 22/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

extension analytics {
    static func getActivity(Activity_Type:String, Source_Address:String, Destination_Address:String, Amenitites:String, Source_placeid:String, Destination_placeid:String,Total_walktime:Int, Total_walkingdistance:Int, Total_transitdistance:Double, NumberofTransit:Int, Total_triptime:Int,Total_transittime:Int, Total_tripdistance:Double, highcost:Double, Application:Int, Firstmile:Int,Amenitites_Category:String)
    {
        let userDefaults = UserDefaults.standard
        let SessionID = userDefaults.value(forKey: "AnalyticsSessionID")
        //let DeviceID = userDefaults.value(forKey: "AnalyticsDeviceID")
        if  SessionID != nil {

         let parameters: [String: Any] = [
         
           
            "Session_Id": SessionID!,
            "Activity_Type": Activity_Type,
            "Source_Address": Source_Address,
            "Destination_Address": Destination_Address,
            "Amenitites": Amenitites,
            "Source_placeid": Source_placeid,
            "Destination_placeid": Destination_placeid,
            "Total_walktime": Total_walktime,
            "Total_transittime": Total_transittime,
            "Total_walkingdistance": Total_walkingdistance,
            "Total_transitdistance": Total_transitdistance,
            "NumberofTransit": NumberofTransit,
            "Total_triptime": Total_triptime,
            "Total_tripdistance": Total_tripdistance,
            "Application": 1,
            "Firstmile": Firstmile,
            "Tripcost": highcost,
            "Category": Amenitites_Category
            
        
        ]
        print("Done *********** ")
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)AnalyticsApi/api/AnalyticsDataReport/Activity", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                case .success:
                    print(response)
                    
                    let responseVar = response.result.value
                    
                    let jsonValue = JSON(responseVar!)
                    print(jsonValue["Message"])
                    let DataFromJSON = Int("\(jsonValue["Data"]["activityid"])")
                   print(DataFromJSON!)
                   // print(DataFromJSON!["activityid"])
                    AnalyticsVar.ActivityID = DataFromJSON
                case .failure(let error):
                    print(error)
                    
                }
                
                
                
                
        }
        
    }
    }
}
