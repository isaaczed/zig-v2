//
//  GetTrasitRunningStatus.swift
//  ZIG
//
//  Created by Isaac on 30/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

extension analytics {
    static func GetTransitRunningStatus(trip_id:Int,ETA_arrival_time:String,ETA_depature_time:String,Delay:String,stop_id:String,stop_name:String,Activityid:Int){
        let userDefaults = UserDefaults.standard
        let SessionID = userDefaults.value(forKey: "AnalyticsSessionID")
        //  let DeviceID = userDefaults.value(forKey: "AnalyticsDeviceID")
        if  SessionID != nil {
        print(SessionID)
      //  print(PageName)
        let parameters: [String: Any] = [
            "trip_id": trip_id,
            "ETA_arrival_time": ETA_arrival_time,
            "ETA_depature_time": ETA_depature_time,
            "Delay": Delay,
            "stop_id": stop_id,
            "stop_name": stop_name,
            "Activityid": Activityid,
            "Application": 1
            
            
        ]
        print(parameters)
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)AnalyticsApi/api/AnalyticsDataReport/RunningStatus", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                case .success:
                    print(response)
                    print("Done Page Hit *********** ")
                    let responseVar = response.result.value
                    
                    let jsonValue = JSON(responseVar!)
                    print(jsonValue["Message"])
                   
                    
                    
                case .failure(let error):
                    print(error)
                    
                }
                
                
                
                
        }
        
        
        
        
    }
        
    }
}
