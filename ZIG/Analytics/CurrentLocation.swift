//
//  CurrentLocation.swift
//  ZIG
//
//  Created by Isaac on 22/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

extension analytics {
    static func GetCurrentLocation(Address:String,Lat:Double,Lng:Double) {
        let userDefaults = UserDefaults.standard
        let SessionID = userDefaults.value(forKey: "AnalyticsSessionID")
        let DeviceID = userDefaults.value(forKey: "AnalyticsDeviceID")
        
        if DeviceID != nil && SessionID != nil {
        let parameters: [String: Any] = [
        
            
            "Address_Name": Address,
            "lat": Lat,
            "lng": Lng,
            "count": 1,
            "device_id": DeviceID ?? "00",
            "sessionId": SessionID!,
            "Application": 1 ]
        
        print(Address)
        print("\(Lat),\(Lng)")
       print("\(DeviceID!),\(SessionID!)")
        Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)AnalyticsApi/api/AnalyticsDataReport/Currentlocation", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                case .success:
                print(response)
            
                let responseVar = response.result.value
               
                let jsonValue = JSON(responseVar!)
                print(jsonValue["Message"])
                
                case .failure(let error):
                    print(error)
                    
                }
                
                
                
                
        }
        
        }
        else{
            print("Data Nil")
        }
    }
}
