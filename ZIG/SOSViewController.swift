//
//  SOSViewController.swift
//  ZIG
//
//  Created by Mac HD on 4/24/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import Ripples
import CoreLocation
import Alamofire
import SwiftyJSON
class SOSViewController: UIViewController {
var locationManager = CLLocationManager()
    var CurrentlocationAddressString:String?
    var latSOS:String?
    var longSOS:String?
     var SOSClose : Timer!
    
    @IBOutlet weak var PhoneORStop: UIImageView!
    @IBOutlet weak var Messagesending: UILabel!
    @IBOutlet weak var YourLocation: UIButton!
    @IBOutlet weak var Success: UIView!
    @IBOutlet weak var SuccessFalureGif: UIImageView!
    var seconds = 3
    @IBOutlet weak var Taptoclose: UIButton!
    @IBOutlet weak var SuccessFailureMsg: UILabel!
    var Triggervariable = ""
    @IBOutlet weak var close3sec: UIButton!
    @IBOutlet weak var CurrentlocationAddress: UILabel!
    let ripple = Ripples()
        var counterStopRequest = 0
        override func viewDidLoad() {
            
            super.viewDidLoad()
            view.superview?.bringSubview(toFront: PhoneORStop)
            print("\(self.Triggervariable)------- Isaac Raja")
            if self.Triggervariable == "Stop"{
                
                Messagesending.text = "Sending an stop request alert to validator"
                ripple.backgroundColor = color.hexStringToUIColor(hex: "#ffc4c4").cgColor
                self.view.backgroundColor = color.hexStringToUIColor(hex: "#D40500")
                 close3sec.backgroundColor = color.hexStringToUIColor(hex: "#D40500")
                Messagesending.textColor = color.hexStringToUIColor(hex: "#ffffff")
                  YourLocation.tintColor = color.hexStringToUIColor(hex: "#ffffff")
                
                YourLocation.setTitleColor(.white, for: .normal)
                YourLocation.isHidden = true
                CurrentlocationAddress.isHidden = true
                PhoneORStop.image = UIImage(named: "StopRequest-1")
               // StopRequest-1
                
            }
            else{
                 close3sec.backgroundColor = color.hexStringToUIColor(hex: "#0A3A71")
            }
            self.navigationController?.navigationBar.isHidden = false
                   self.navigationController?.navigationBar.isTranslucent = false
                   self.navigationController?.navigationBar.backgroundColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
                   self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
                   self.navigationController?.navigationBar.tintColor = UIColor.white
                   self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
            CurrentlocationAddress.text = CurrentlocationAddressString
            ripple.radius = 300
            ripple.rippleCount = 5
            view.layer.addSublayer(ripple)
            Taptoclose.backgroundColor = .clear
            Taptoclose.layer.cornerRadius = 5
            Taptoclose.layer.borderWidth = 2
            
           
            close3sec.layer.cornerRadius = 5
            close3sec.layer.borderWidth = 2
            self.close3sec.layer.borderColor = color.hexStringToUIColor(hex: "#ffffff").cgColor
            self.close3sec.setTitleColor(color.hexStringToUIColor(hex: "#ffffff"), for: .normal)
           
            let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction(sender:)))
            self.Success.addGestureRecognizer(gesture)
            
            ripple.start()
           
             self.close3sec.setTitle("Cancel (3)", for: .normal)

           SOSClose = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                self.seconds -= 1
                if self.seconds == 0 {
                    self.close3sec.isHidden = true
                    print("Go!")
                    
                     DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        if self.Triggervariable == "SOS"{
                             SOSandSTOP.SOSEnable = true

                            self.sendSOS()
                        }
                        else {
                            SOSandSTOP.StopRequest = true
                            self.checkStoprequest()
                        }
                        
                   
                        
                       
                    }
                    
                } else {
                    self.close3sec.setTitle("Cancel (\(self.seconds))", for: .normal)
                    print(self.seconds)
                }
            }
        
            
        }
    func checkStoprequest(){
      //  var count
        print(SOSandSTOP.StopRequest)
        if SOSandSTOP.StopRequest {
            counterStopRequest = counterStopRequest + 1
            DispatchQueue.main.asyncAfter(deadline: .now() + 1)
            {
                
                                self.checkStoprequest()
            }
        }
        else{
            self.Success.isHidden = false
                                           self.circleAnim(self.Success, duration: 1.0)
                                            self.ripple.stop()
                                           self.SuccessFailureMsg.text = "Your stop request has been delivered to validator"
                                           self.SuccessFailureMsg.textColor = color.hexStringToUIColor(hex: "#5DCC86")
                                           self.SuccessFalureGif.image = UIImage.gif(name: "success4")
                                            // self.SuccessFalureGif.animationRepeatCount = 1
                                           self.Taptoclose.layer.borderColor = color.hexStringToUIColor(hex: "#5DCC86").cgColor
                                           self.Taptoclose.setTitleColor(color.hexStringToUIColor(hex: "#5DCC86"), for: .normal)
            
        }
        if counterStopRequest == 10 {
            self.Success.isHidden = false
            SOSandSTOP.StopRequest = false
                                       self.circleAnim(self.Success, duration: 1.0)
                                       self.ripple.stop()
                                       self.Success.backgroundColor = color.hexStringToUIColor(hex: "#F1F1F1")
                                       self.SuccessFailureMsg.text = "Message send failure! \nPlease try again"
                                       self.SuccessFailureMsg.textColor = color.hexStringToUIColor(hex: "#EA4335")
                                       self.SuccessFalureGif.image = UIImage.gif(name: "failure")
                                         // self.SuccessFalureGif.animationRepeatCount = 1
                                       self.Taptoclose.layer.borderColor = color.hexStringToUIColor(hex: "#EA4335").cgColor
                                       self.Taptoclose.setTitleColor(color.hexStringToUIColor(hex: "#EA4335"), for: .normal)
            
        }
        
    }
    func sendSOS(){
        if beaconData.CurrentBeaconID != "" {
        let userDefaults1 = UserDefaults.standard
                   var useid =  0
                   var userName = ""
                   var Token = ""
                   if userDefaults1.value(forKey: "userName") == nil
                              {
                              userName = ""
                          }
                          else
                          {
                              userName = userDefaults1.value(forKey: "userName") as! String
                          }
                   if userDefaults1.value(forKey: "userIdString") == nil
                       {
                       useid = 0
                   }
                   else
                   {
                       useid = userDefaults1.integer(forKey: "userIdString")
                   }
                   if userDefaults1.value(forKey: "accessToken") == nil
                       {
                       Token = ""
                   }
                   else
                   {
                       Token = userDefaults1.value(forKey: "accessToken") as! String
                   }
                   
                   
        print("\((latSOS)!),\((longSOS)!),\((CurrentlocationAddressString)!),\(beaconData.CurrentBeaconID) - DataCheck")
        let parameters1: Parameters = [
                         "Latitude": latSOS!,
                         "Longitude" : longSOS!,
                         "Userid" : "\(useid)",
                         "Username" : userName,
                         "Usertoken" : Token,
                         "Bibooaddress" :  beaconData.CurrentBeaconID,
                         "Appfirmware" : 1
                ]
        print(parameters1)
        let getUserId = "\(APIUrl.ZIGTARCAPI)Feedback/SosreeportIssue/Add"

        Alamofire.request(getUserId, method: .post, parameters: parameters1, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                case .success:


                    if (response.result.value != nil) {

                        let responseVar = response.result.value
                        let json = JSON(responseVar!)
                        print(json)
                        if json["Message"] == "Issue added Successfully" {
                            
                           
                            //DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
                              
                                                             
                                self.Success.isHidden = false
                                self.circleAnim(self.Success, duration: 1.0)
                                 self.ripple.stop()
                                self.SuccessFailureMsg.text = "Your alert has been delivered to TARC"
                                self.SuccessFailureMsg.textColor = color.hexStringToUIColor(hex: "#5DCC86")
                                self.SuccessFalureGif.image = UIImage.gif(name: "success4")
                                 // self.SuccessFalureGif.animationRepeatCount = 1
                                self.Taptoclose.layer.borderColor = color.hexStringToUIColor(hex: "#5DCC86").cgColor
                                self.Taptoclose.setTitleColor(color.hexStringToUIColor(hex: "#5DCC86"), for: .normal)
                          //  }
                           
                            
                        }
                        else{
                            
                            //failure
                            self.Success.isHidden = false
                            self.circleAnim(self.Success, duration: 1.0)
                            self.ripple.stop()
                            self.Success.backgroundColor = color.hexStringToUIColor(hex: "#F1F1F1")
                            self.SuccessFailureMsg.text = "Message send failure! \nPlease try again"
                            self.SuccessFailureMsg.textColor = color.hexStringToUIColor(hex: "#EA4335")
                            self.SuccessFalureGif.image = UIImage.gif(name: "failure")
                              // self.SuccessFalureGif.animationRepeatCount = 1
                            self.Taptoclose.layer.borderColor = color.hexStringToUIColor(hex: "#EA4335").cgColor
                            self.Taptoclose.setTitleColor(color.hexStringToUIColor(hex: "#EA4335"), for: .normal)
                            
                        }
                    }
                    else{
                        self.Success.isHidden = false
                                                       self.circleAnim(self.Success, duration: 1.0)
                                                       self.ripple.stop()
                                                       self.Success.backgroundColor = color.hexStringToUIColor(hex: "#F1F1F1")
                                                       self.SuccessFailureMsg.text = "Message send failure! \nPlease try again"
                                                       self.SuccessFailureMsg.textColor = color.hexStringToUIColor(hex: "#EA4335")
                                                       self.SuccessFalureGif.image = UIImage.gif(name: "failure")
                                                       //   self.SuccessFalureGif.animationRepeatCount = 1
                                                       self.Taptoclose.layer.borderColor = color.hexStringToUIColor(hex: "#EA4335").cgColor
                                                       self.Taptoclose.setTitleColor(color.hexStringToUIColor(hex: "#EA4335"), for: .normal)
                        
                    }
                    case .failure(let error):
                        print(error)
                self.Success.isHidden = false
                                                   self.circleAnim(self.Success, duration: 1.0)
                                                   self.ripple.stop()
                                                 self.Success.backgroundColor = color.hexStringToUIColor(hex: "#F1F1F1")
                                                   self.SuccessFailureMsg.text = "Message send failure! \nPlease try again"
                                                   self.SuccessFailureMsg.textColor = color.hexStringToUIColor(hex: "#EA4335")
                                                   self.SuccessFalureGif.image = UIImage.gif(name: "failure")
                                                      self.SuccessFalureGif.animationRepeatCount = 1//
                                                   self.Taptoclose.layer.borderColor = color.hexStringToUIColor(hex: "#EA4335").cgColor
                                                   self.Taptoclose.setTitleColor(color.hexStringToUIColor(hex: "#EA4335"), for: .normal)
                }
        }
        }
        else{
             DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.sendSOS()
            }
        }
    }
       @objc func checkAction(sender : UITapGestureRecognizer) {
            if(SOSClose != nil)
            {
                SOSClose.invalidate()
            }
        self.navigationController?.popViewController(animated: true)
        
        
        }
        override func viewDidLayoutSubviews() {
            super.viewDidLayoutSubviews()
            ripple.position = self.view.center
            
        }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           navigationController?.setNavigationBarHidden(false, animated: true)
       }
   
 func circleAnim(_ view: UIView, duration: CFTimeInterval) {
     let maskDiameter = CGFloat(sqrtf(powf(Float(view.bounds.width), 2) + powf(Float(view.bounds.height), 2)))
     let mask = CAShapeLayer()
     let animationId = "path"

     // Make a circular shape.
     mask.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: maskDiameter, height: maskDiameter), cornerRadius: maskDiameter / 2).cgPath

     // Center the shape in the view.
     mask.position = CGPoint(x: (view.bounds.width - maskDiameter) / 2, y: (view.bounds.height - maskDiameter) / 2)

     // Fill the circle.
     mask.fillColor = UIColor.black.cgColor

     // Add as a mask to the parent layer.
     view.layer.mask = mask

     // Animate.
     let animation = CABasicAnimation(keyPath: animationId)
     animation.duration = duration
   // animation.fillMode
     animation.isRemovedOnCompletion = false

     // Create a new path.
     let newPath = UIBezierPath(roundedRect: CGRect(x: maskDiameter / 2, y: maskDiameter / 2, width: 0, height: 0), cornerRadius: 0).cgPath

     // Set start and end values.
     animation.fromValue =  newPath
     animation.toValue = mask.path

     // Start the animation.
     mask.add(animation, forKey: animationId)
    
 }
    
    @IBAction func Close3secbut(_ sender: Any) {
        
        if(SOSClose != nil)
               {
                   SOSClose.invalidate()
               }
        self.navigationController?.popViewController(animated: true)
       
    }
    @IBAction func CloseButton(_ sender: Any) {
        if(SOSClose != nil)
        {
            SOSClose.invalidate()
        }
        self.navigationController?.popViewController(animated: true)
       
    }
    // func getAdressName(coords: CLLocation)
    }
