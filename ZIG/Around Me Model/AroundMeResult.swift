//
//	AroundMeResult.swift


import Foundation 
import ObjectMapper


class AroundMeResult : NSObject, NSCoding, Mappable{

	var address : String?
	var category : String?
	var distance : Double?
	var imageURL : String?
	var latitude : Double?
	var longitude : Double?
	var title : String?
	var website : String?


	class func newInstance(map: Map) -> Mappable?{
		return AroundMeResult()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		address <- map["Address"]
		category <- map["Category"]
		distance <- map["Distance"]
		imageURL <- map["ImageURL"]
		latitude <- map["Latitude"]
		longitude <- map["Longitude"]
		title <- map["Title"]
		website <- map["Website"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         address = aDecoder.decodeObject(forKey: "Address") as? String
         category = aDecoder.decodeObject(forKey: "Category") as? String
         distance = aDecoder.decodeObject(forKey: "Distance") as? Double
         imageURL = aDecoder.decodeObject(forKey: "ImageURL") as? String
         latitude = aDecoder.decodeObject(forKey: "Latitude") as? Double
         longitude = aDecoder.decodeObject(forKey: "Longitude") as? Double
         title = aDecoder.decodeObject(forKey: "Title") as? String
         website = aDecoder.decodeObject(forKey: "Website") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if address != nil{
			aCoder.encode(address, forKey: "Address")
		}
		if category != nil{
			aCoder.encode(category, forKey: "Category")
		}
		if distance != nil{
			aCoder.encode(distance, forKey: "Distance")
		}
		if imageURL != nil{
			aCoder.encode(imageURL, forKey: "ImageURL")
		}
		if latitude != nil{
			aCoder.encode(latitude, forKey: "Latitude")
		}
		if longitude != nil{
			aCoder.encode(longitude, forKey: "Longitude")
		}
		if title != nil{
			aCoder.encode(title, forKey: "Title")
		}
		if website != nil{
			aCoder.encode(website, forKey: "Website")
		}

	}

}
