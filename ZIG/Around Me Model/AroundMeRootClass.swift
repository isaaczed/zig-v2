//
//	AroundMeRootClass.swift


import Foundation 
import ObjectMapper


class AroundMeRootClass : NSObject, NSCoding, Mappable{

	var count : Int?
	var message : String?
	var result : [AroundMeResult]?


	class func newInstance(map: Map) -> Mappable?{
		return AroundMeRootClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		count <- map["Count"]
		message <- map["Message"]
		result <- map["Result"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         count = aDecoder.decodeObject(forKey: "Count") as? Int
         message = aDecoder.decodeObject(forKey: "Message") as? String
         result = aDecoder.decodeObject(forKey: "Result") as? [AroundMeResult]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if count != nil{
			aCoder.encode(count, forKey: "Count")
		}
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if result != nil{
			aCoder.encode(result, forKey: "Result")
		}

	}

}