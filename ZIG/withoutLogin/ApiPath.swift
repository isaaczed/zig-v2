//
//  ApiPath.swift
//  Tarc
//
//  Created by Isaac on 21/03/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation

struct url {
    static var getAddressFromLatLong = "https://maps.googleapis.com/maps/api/geocode/json?"
    static var GooglePlaces = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
    static var getGoogleDirection = "https://maps.googleapis.com/maps/api/directions/json?"
    
   
}
