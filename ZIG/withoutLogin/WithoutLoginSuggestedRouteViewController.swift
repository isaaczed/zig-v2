//
//  SuggestedRouteViewController.swift
//  Tarc
//
//  Created by Isaac on 22/03/19.
//  Copyright © 2019 Isaac. All rights reserved.


import UIKit
import Alamofire
import CoreLocation
import Foundation
import UberRides
import LyftSDK
import UberCore
import MapKit


class WithoutLoginSuggestedRouteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,LoginButtonDelegate {
 var Louvelo = false
    var getbirdFun = false
    var boltListApi: [Bolt_list]?
     var birdListApi: [data_list]?
     var LimeListApi: [Lime_list]?
    var Total_transitdistance1:Double?
    var Total_transitdistance2:Double?
    var Total_transitduration1:Int?
    var Total_transitduration2:Int?
    var uberTotalcost:Int?
    var LyftTotalCost:Int?
    @IBOutlet weak var tableView: UITableView!
    var sourceAddresstest:String?
    var sourcePlaceId:String?
    var sourcelat:Double?
    var sourcelng:Double?
    var BoltDatalistapicount: Int?
    var DestinationAddresstest:String?
    var DestinationPlaceId:String?
    var Destinationlat:Double?
    var Destinationlng:Double?
    let cellReuseIdentifier = "cell"
    var sourceLouveloStationID:Int?
    var SourceLouveloLat:Double?
    var SourceLouveloLong:Double?
    var SourceLouveloAddress:String?
    var SourceLouveloDistance = 0.0
    
    var DestinationLouveloStationID:Int?
    var DestinationLouveloLat:Double?
    var DestinationLouveloLong:Double?
    var DestinationLouveloAddress:String?
    var DestinationloDistance = 0.0
    
    var tempSourceLouveloLat:Double?
    var tempSourceLouveloLong:Double?
    var BirdBikeID = ""
    var BirdBikeLat = 0.0
    var BirdBikeLon = 0.0
    var BirdIsReserved = 0
    var BirdIsDisabled = 0
    var BirdSourceDistance = 0.0
    var BirdtempDistance = 0.0
    var templat = 0.0
    var templong = 0.0
    var BikedistanceInMeters = 0.0
    var BirdDatalistapicount:Int?
    var tempDestinationLouveloLat:Double?
    var tempDestinationLouveloLong:Double?
     var tempDestinationloDistance = 0.0
    var tempSourceLouveloDistance = 0.0
    var tripSchduleTime = Date()
        var calendarEvent = false
    var filterOptionIndex = 0
    var GoogleTransitResponse:DataResponse<SuggestDirectionaClass>!
    var DataWalkingResponse:DataResponse<SuggestWalkingDirectionalClass>!
    var dataBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var WalkingDistance = ""
   // var googlevalueResult:SuggestDirectionaClass?
    var googlecount = 0
    var WalkingDuration = 0
    
    var fareStringEXP = 2.75
    var fareStringExpMyCard = 2.50
    var TotalFar = 0.0
    var MycardTotalFare = 0.0
    var MycardTotalFareArray:[Double] = [0.0]
    var FareStringLocal = 1.75
    var fareStringLocalMycard = 1.50
    var NewfareString = ""
    var newMytarcFareString = ""
    var TransitTrue:Bool = false
    var walkingTrue:Bool = false
    var bicycleTrue:Bool =  false
    var isCalenderEvent:Bool?

    var ridetype = [" ","Local","Express"]
    var NormalPrice = ["Regular","$1.75","$2.75"]
    var TarcCardlPrice = ["TARC Ticket","$1.50","$2.50"]

    var uberSecIndex = ""
    var uberAddedIndex = 0
    
    var cellHedding : NSMutableArray = ["Transit","Bikeshare","Rideshare"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
      
        // Do any additional setup after loading the view.
        analytics.GetPageHitCount(PageName: "Suggested Route")
        getbird()
        getlouvelo()
        getTransitinfo()
        
    }
    
    func getTransitinfo(){
       
                        let modifiedArray : NSMutableArray = []
                        print(self.GoogleTransitResponse.result.value?.routes?.count)
                        var durationCheckValue = 0
                        
                        
                        for dataSteps in (self.GoogleTransitResponse.result.value?.routes!)!
                        {
                            
                            if self.filterOptionIndex == 0
                            {
                                print("timing == \((dataSteps.legs?[0].duration?.value!)!)")
                                if durationCheckValue > (dataSteps.legs?[0].duration?.value!)! || durationCheckValue == 0
                                {
                                    
                                    durationCheckValue = (dataSteps.legs?[0].duration?.value!)!
                                    if modifiedArray.count>0
                                    {
                                        if modifiedArray.count>1
                                        {
                                            modifiedArray.replaceObject(at: 1, with: modifiedArray.object(at: 0))
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                        else
                                        {
                                            modifiedArray.insert(modifiedArray.object(at: 0), at: 1)
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 0)
                                    }
                                }
                                else
                                {
                                    if modifiedArray.count > 1
                                    {
                                        let modifiedCheck = modifiedArray.object(at: 1) as! SuggestRoute
                                        if (modifiedCheck.legs?[0].duration?.value)! > (dataSteps.legs?[0].duration?.value!)!
                                        {
                                            modifiedArray.replaceObject(at: 1, with: dataSteps)
                                        }
                                        
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 1)
                                    }
                                }
                            }
                            else if self.filterOptionIndex == 1 {
                                print("timing == \((dataSteps.legs?[0].steps?.count)!)")
                                if durationCheckValue > (dataSteps.legs?[0].steps?.count)! || durationCheckValue == 0
                                {
                                    
                                    durationCheckValue = (dataSteps.legs?[0].steps?.count)!
                                    if modifiedArray.count > 0
                                    {
                                        if modifiedArray.count>1
                                        {
                                            modifiedArray.replaceObject(at: 1, with: modifiedArray.object(at: 0))
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                        else
                                        {
                                            modifiedArray.insert(modifiedArray.object(at: 0), at: 1)
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 0)
                                    }
                                }
                                else
                                {
                                    if modifiedArray.count > 1
                                    {
                                        let modifiedCheck = modifiedArray.object(at: 1) as! SuggestRoute
                                        if (modifiedCheck.legs?[0].steps?.count)! > (dataSteps.legs?[0].steps?.count)!
                                        {
                                            modifiedArray.replaceObject(at: 1, with: dataSteps)
                                        }
                                        
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 1)
                                    }
                                }
                            }
                            else if self.filterOptionIndex == 2 {
                                print("timing == \((dataSteps.legs?[0].steps?.count)!)")
                                var stepsDistanceValues = 0
                                for dataStepsValues in (dataSteps.legs?[0].steps)!
                                {
                                    if dataStepsValues.travelMode == "WALKING"
                                    {
                                        print("timing == \((dataStepsValues.distance?.value)!)")
                                        let userDefaults = UserDefaults.standard
                                        if (dataStepsValues.distance?.value)! > 805 && userDefaults.bool(forKey: "isCab")
                                        {
                                            print("uber")
                                        }
                                        else
                                        {
                                            stepsDistanceValues = stepsDistanceValues + (dataStepsValues.distance?.value)!
                                        }
                                        
                                    }
                                }
                                if durationCheckValue > stepsDistanceValues || durationCheckValue == 0
                                {
                                    
                                    durationCheckValue = stepsDistanceValues
                                    if modifiedArray.count > 0
                                    {
                                        if modifiedArray.count>1
                                        {
                                            modifiedArray.replaceObject(at: 1, with: modifiedArray.object(at: 0))
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                        else
                                        {
                                            modifiedArray.insert(modifiedArray.object(at: 0), at: 1)
                                            modifiedArray.replaceObject(at: 0, with: dataSteps)
                                        }
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 0)
                                    }
                                }
                                else
                                {
                                    
                                    if modifiedArray.count > 1
                                    {
                                        let modifiedCheck = modifiedArray.object(at: 1) as! SuggestRoute
                                        var stepsDistanceValues1 = 0
                                        for dataStepsValues1 in (modifiedCheck.legs?[0].steps)!
                                        {
                                            let userDefaults = UserDefaults.standard
                                            if (dataStepsValues1.distance?.value)! > 805 && userDefaults.bool(forKey: "isCab")
                                            {
                                                print("uber")
                                            }
                                            else
                                            {
                                                stepsDistanceValues1 = stepsDistanceValues1 + (dataStepsValues1.distance?.value)!
                                            }
                                        }
                                        
                                        if stepsDistanceValues1 > stepsDistanceValues
                                        {
                                            modifiedArray.replaceObject(at: 1, with: dataSteps)
                                        }
                                        
                                    }
                                    else
                                    {
                                        modifiedArray.insert(dataSteps, at: 1)
                                    }
                                }
                                
                            }
                        }
                        let modifiedCheckTmp0 = modifiedArray.object(at: 0) as! SuggestRoute
                        if modifiedArray.count > 1
                        {
                            let modifiedCheckTmp1 = modifiedArray.object(at: 1) as! SuggestRoute
                            print((modifiedCheckTmp1.legs?[0].duration?.value)!)
                        }
                        print((modifiedCheckTmp0.legs?[0].duration?.value)!)
                        
                        if modifiedArray.count>0
                        {
                            self.GoogleTransitResponse.result.value?.routes = modifiedArray as? [SuggestRoute]
                        }
                        print(self.GoogleTransitResponse.result.value?.routes?.count)
                        self.TransitTrue = true
                        self.googlecount = (self.GoogleTransitResponse.result.value?.routes?.count)!
                        //self.reloadtable()
        
        
    }
    func reloadtable(){
        print(self.walkingTrue && self.TransitTrue && bicycleTrue )
        if self.walkingTrue && self.TransitTrue && bicycleTrue {
            self.tableView.reloadData()
        }
        
    }
    func getbolt(){
        Alamofire.request("https://www.bolt.miami/bolt2/lou/gbfs/en/free_bike_status.json").responseObject { (response: DataResponse<getBoltstation>) in
        switch response.result {
        case .success:
          
            if response.result.value?.code != 400 {
                if (response.result.value?.total)! >= 0 {
                  //  let Sourcelatlatlong = CLLocation(latitude: Double((self.sourceLatTrip)), longitude: Double((self.sourceLngTrip)))
                    
                    let responseResutl = response.result.value
                    print("\(responseResutl?.datalistfromapi?.count)")
               
                 
                    self.boltListApi = responseResutl?.datalistfromapi
                    self.BoltDatalistapicount = responseResutl?.datalistfromapi?.count
                  
                }
            }
            
          case .failure(let error):
            print(error)
            }
        }
    }
    func getbird(){
        var dateInSeconds : TimeInterval
        dateInSeconds = tripSchduleTime.timeIntervalSince1970
        print(dateInSeconds)
        let dateEpochSec = String(format: "%.0f", dateInSeconds)
        
        var CheckBikeCount = 0
        Alamofire.request("https://mds.bird.co/gbfs/louisville/free_bikes").responseObject { (response: DataResponse<getbirdstation>) in
            switch response.result {
            case .success:
                if response.result.value != nil {
                    
                 if response.result.value?.code != 400 {
                    if response.result.value?.total != nil
                    {
                if (response.result.value?.total)! >= 0 {
                    let Sourcelatlatlong = CLLocation(latitude:self.sourcelat!, longitude: self.sourcelng!)
                    
                    let responseResutl = response.result.value
                    
                    self.BirdDatalistapicount = responseResutl?.datalistfromapi?.count
                    self.birdListApi = responseResutl?.datalistfromapi
                    for BirdBike in (responseResutl?.datalistfromapi)! {
                        
                        

                        if CheckBikeCount == 0 {
                            
                            self.BirdBikeID = BirdBike.bike_id!
                            self.BirdBikeLat = BirdBike.bike_lat!
                            self.BirdBikeLon = BirdBike.bike_lon!
                            self.BirdIsReserved = BirdBike.bike_is_reserved!
                            self.BirdIsDisabled = BirdBike.bike_is_disabled!
                            let destinationlatlong = CLLocation(latitude: self.BirdBikeLat, longitude: self.BirdBikeLon)
                            self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                            self.BirdSourceDistance = (self.BikedistanceInMeters*100).rounded()/100
                            CheckBikeCount = CheckBikeCount + 1
                           
                        }
                        else{
                            self.templat = BirdBike.bike_lat!
                            self.templong = BirdBike.bike_lon!
                            let destinationlatlong = CLLocation(latitude: self.templat, longitude: self.templong)
                            self.BikedistanceInMeters = Sourcelatlatlong.distance(from: destinationlatlong)
                            self.BirdtempDistance = (self.BikedistanceInMeters*100).rounded()/100
                            if self.BirdtempDistance < self.BirdSourceDistance {
                                
                                self.BirdBikeID = BirdBike.bike_id!
                                self.BirdBikeLat = BirdBike.bike_lat!
                                self.BirdBikeLon = BirdBike.bike_lon!
                                self.BirdIsReserved = BirdBike.bike_is_reserved!
                                self.BirdIsDisabled = BirdBike.bike_is_disabled!
                                self.BirdSourceDistance = self.BirdtempDistance
                                CheckBikeCount = CheckBikeCount + 1
                                
                            }
                            else{
                                CheckBikeCount = CheckBikeCount + 1
                            }
                        }
                    }
                    let  walkingorgin = "\(self.sourcelat!),\(self.sourcelng!)"
                    let walkingdestination = "\(self.BirdBikeLat),\(self.BirdBikeLon)"
                    let  BikeDestinationLatlong = "\((self.Destinationlat)!),\((self.Destinationlng)!)"
                    
                    let GoogleDirection = "\(url.getGoogleDirection)origin=\(walkingdestination)&destination=\(walkingdestination)&mode=walking&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)"
                    print("Google Walking bird Array -> *************")
                    print(GoogleDirection)
                    Alamofire.request(GoogleDirection, method: .get, encoding: URLEncoding.default)
                        .responseObject{ (response: DataResponse<SuggestWalkingDirectionalClass>) in
                            //print(response)
                            switch response.result {
                            case .success:
                                print("Success Google walking Direction")
                                self.DataWalkingResponse = response
                                self.walkingTrue = true
                                 self.reloadtable()
                                let GoogleBikeDirection = "\(url.getGoogleDirection)origin=\(walkingorgin)&destination=\(walkingdestination)&mode=bicycling&DepartTime=\(dateInSeconds)&alternatives=true&key=\(Key.GoogleAPIKey)"
                                Alamofire.request(GoogleBikeDirection, method: .get, encoding: URLEncoding.default)
                                    .responseObject{ (response: DataResponse<SuggestBikeDirectionalClass>) in
                                        switch response.result {
                                        case .success:
                                            print(response)
                                            //to get status code
                                            
                                            
                                            self.dataBikeResponse  = response
                                            self.bicycleTrue = true
                                            self.getbirdFun = true
                                            self.checkstatus()
                                
                                             self.reloadtable()
                                        case .failure(let error):
                                            print(error)
                                        }
                                }
                                
                                
                              //  let indexPath = IndexPath(item: 0, section: 2)
                                self.tableView.reloadData()
                               self.reloadtable()
                               
                            case .failure(let error):
                                print(error)
                            }
                    }
                
                    
                    
                    
                }
                }
                }
                 else{
                    //self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                }
            case .failure(let error):
                print(error)
                
            }
                
        }
    }
    func getlouvelo(){
        let SourceLatLong = CLLocation(latitude: self.sourcelat!, longitude: self.sourcelng!)
        let DestinationLatLong = CLLocation(latitude: self.Destinationlat!, longitude: self.Destinationlng!)
        var CheckBikeCount = 0
        Alamofire.request("https://lou.publicbikesystem.net/ube/gbfs/v1/en/station_information").responseObject { (response: DataResponse<getlouveloData>) in
            switch response.result {
            case .success:
                //  //dump(response.result.value!)
                
                let louData = response.result.value!
                for louDataArraay in louData.louveldata! {
                    if CheckBikeCount == 0 {
                        self.sourceLouveloStationID = louDataArraay.StationID
                        self.SourceLouveloLat = louDataArraay.StationLat
                        self.SourceLouveloLong = louDataArraay.StationLong
                        self.DestinationLouveloStationID = louDataArraay.StationID
                        self.DestinationLouveloAddress = louDataArraay.StationName
                        self.DestinationLouveloLat = louDataArraay.StationLat
                        self.DestinationLouveloLong = louDataArraay.StationLong
                        self.SourceLouveloAddress = louDataArraay.StationName
                        
                       
                        
                        let SourceDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)
                        let DestinationDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)
                        
                        self.SourceLouveloDistance = SourceLatLong.distance(from: SourceDistanceFinder)
                        self.SourceLouveloDistance = (self.SourceLouveloDistance*100).rounded()/100
                        
                        
                        self.DestinationloDistance = DestinationLatLong.distance(from: DestinationDistanceFinder)
                        self.DestinationloDistance = (self.DestinationloDistance*100).rounded()/100
                        
                        CheckBikeCount = CheckBikeCount + 1
                        
                        
                    }
                    else{
                        self.tempSourceLouveloLat = louDataArraay.StationLat
                        self.tempSourceLouveloLong = louDataArraay.StationLong
                        
                        
                        self.tempDestinationLouveloLat = louDataArraay.StationLat
                        self.tempDestinationLouveloLong = louDataArraay.StationLong
                        
                        var SourceDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)
                        var DestinationDistanceFinder = CLLocation(latitude: louDataArraay.StationLat!, longitude: louDataArraay.StationLong!)
                        
                        
                        self.tempSourceLouveloDistance = SourceLatLong.distance(from: SourceDistanceFinder)
                        self.tempSourceLouveloDistance = (self.tempSourceLouveloDistance*100).rounded()/100
                        
                        
                        self.tempDestinationloDistance = DestinationLatLong.distance(from: DestinationDistanceFinder)
                        self.tempDestinationloDistance = (self.tempDestinationloDistance*100).rounded()/100
                        
                        if self.tempSourceLouveloDistance < self.SourceLouveloDistance {
                            
                            self.sourceLouveloStationID = louDataArraay.StationID
                            self.SourceLouveloLat = louDataArraay.StationLat
                            self.SourceLouveloLong = louDataArraay.StationLong
                            self.SourceLouveloDistance = self.tempSourceLouveloDistance
                            self.SourceLouveloAddress = louDataArraay.StationName
                           
                        }
                        else if self.tempDestinationloDistance < self.DestinationloDistance{
                            self.DestinationLouveloStationID = louDataArraay.StationID
                            self.DestinationLouveloLat = louDataArraay.StationLat
                            self.DestinationLouveloLong = louDataArraay.StationLong
                            self.DestinationloDistance = self.tempDestinationloDistance
                            self.DestinationLouveloAddress = louDataArraay.StationName
                            
                        }
                        
                        CheckBikeCount = CheckBikeCount + 1
                        
                        
                        
                    }
                    
                    let indexPath = IndexPath(item: 0, section: 2)
                    //self.tableView.reloadData()
                    self.Louvelo = true
                      self.checkstatus()
                }
                
            case .failure(let error):
                print(error)
                
            }
        }
        
    }

    func checkstatus(){
        if getbirdFun && Louvelo {
            self.tableView.reloadData()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func OnMenuClicked() {
        
    
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.cellHedding.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if cellHedding[section] as? String == "Transit" {
            
            let googlevalueResult = GoogleTransitResponse.result.value
           
                if (googlevalueResult?.routes?.count)! > 2
                {
                    return 2
                }
                else if (googlevalueResult?.routes?.count)! == 1
                {
                    return 1
                }
                else if (googlevalueResult?.routes?.count)! == 0 {
                    return 0
            }
                else {
                    return (googlevalueResult?.routes?.count)!
            }
            }
       
        else{
            return 1
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 42
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellHedding[indexPath.section] as? String == "Transit"
        {
            return 63
        }
        else if cellHedding[indexPath.section] as? String == "Bikeshare"
        {
            return 124
        }
        else if cellHedding[indexPath.section] as? String == "Rideshare"
        {
            return 63
        }
        else {
            return 50
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
            let headerView  = UIView()
            headerView.backgroundColor = UIColor.white
            
        
            let farelabelForBird = UILabel()
              let headerLabel = UILabel()
            headerLabel.frame = CGRect.init(x: 20, y: 0, width: 150, height: 40)
        headerLabel.font = UIFont.setTarcBold(size: 15.0)
            headerLabel.textColor = UIColor.black
            headerLabel.textAlignment = NSTextAlignment.left
            headerView.addSubview(headerLabel)
            
            
        farelabelForBird.font = UIFont.setTarcRegular(size: 15.0)
            farelabelForBird.textColor = UIColor.black
            farelabelForBird.textAlignment = NSTextAlignment.left
            
            
            if cellHedding[section] as? String == "Bikeshare"{
                headerLabel.frame = CGRect.init(x: 20, y: 0, width: 150, height: 40)
                farelabelForBird.frame = CGRect.init(x: headerLabel.frame.origin.x+40, y: 0, width: self.view.frame.width-20, height: 40)
                headerLabel.text = "Bikeshare "
                farelabelForBird.text = "- $1 start, then 15 cents/min"
                
                
                
                let button = UIButton(type: .custom)
                button.frame = CGRect.init(x:60, y: 10, width: 20, height: 20)// create button
                button.tag = section
                // the button is image - set image
               // button.setImage(UIImage(named: "information"), for: .normal)
                //button.addTarget(self, action: #selector(self.BIRDFareInformation(_:)), for: .touchUpInside)
                
                
              //  headerView.addSubview(button)   // add the button to the view
                
            }
            else if cellHedding[section] as? String == "Rideshare"{
                headerLabel.text = "Rideshare "
                farelabelForBird.text = "- $3.50/ 30min"
                farelabelForBird.frame = CGRect.init(x: headerLabel.frame.width + 10, y: 0, width: self.view.frame.width-20, height: 40)
                
            }
            else if cellHedding[section] as? String == "Transit"{
                //                let MyTarcButton = UIImageView()
                //                MyTarcButton.frame = CGRect.init(x:80, y: 10, width: 20, height: 20)
                //                MyTarcButton.image = UIImage(named:"information")
                //                MyTarcButton.layer.zPosition = 10000
                //          //  prevArrowFrame = MyTarcButton.frame
                //                headerView.addSubview(MyTarcButton)
                
                headerLabel.text = self.cellHedding[section] as? String
                
                
                let button = UIButton(type: .custom)
                button.frame = CGRect.init(x:80, y: 10, width: 20, height: 20)// create button
                button.tag = section
                // the button is image - set image
                button.setImage(UIImage(named: "information"), for: .normal)
               // button.addTarget(self, action: #selector(self.FareInformation(_:)), for: .touchUpInside)
                
                
                headerView.addSubview(button)   // add the button to the view
                
                
                
                
            }
            else {
                headerLabel.text = self.cellHedding[section] as? String
            }
            
            //  headerView.addSubview(farelabelForBird)
            let lineLabel = UILabel()
            lineLabel.frame = CGRect.init(x: 20, y: 39, width: self.view.frame.width-40, height: 1)
            lineLabel.backgroundColor = UIColor.gray
            headerView.addSubview(lineLabel)
            
            return headerView
            
        
        
           
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

     guard
             let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedRoutecell",
                                                      for: indexPath) as? SuggestedRoutecell
             else { return UITableViewCell() }
                    if cellHedding[indexPath.section] as? String == "Transit" && GoogleTransitResponse != nil
                           {
                        
                        var userDefault = UserDefaults.standard
                      
                     
                        cell.selectionStyle = .none
                        tableView.separatorColor = UIColor.clear;
                        cell.separatorInset = .zero
                        print("Bus 1")
                        let durationLabel = UILabel()
                        let BuyNowButton = UIButton()
                        
                        let fareLabel = UILabel()
                        let TotalMyTarcFareLabel = UILabel()
                        let mytarccard = UILabel()
                        let Regular = UILabel()
                        let valueResult = GoogleTransitResponse.result.value
                        let routesPlan = valueResult?.routes![indexPath.row].legs
                        cell.transitScrollView.delegate = self
                        cell.transitScrollView.backgroundColor = UIColor.clear
                        cell.transitScrollView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
                        cell.transitScrollView.showsHorizontalScrollIndicator = false
                        var durationString = routesPlan![0].duration?.value
                        //  durationLabel.frame = CGRect.init(x: 10, y: 15, width: 95, height: 30)
                        
                        var fareCount: Double = 0.00
                        var MyCardfareCount: Double = 0.00
                        var distValue = 0
                        var UberLyftWaitingTime: Int?
                        durationLabel.textColor = UIColor.black
                        durationLabel.font = UIFont.setTarcRegular(size: 16.0)
                        durationLabel.textAlignment = NSTextAlignment.center
                        
                        let NoTransit = UILabel()
                        var indexValue = 0
                        var prevArrowFrame = CGRect()
                        if valueResult?.routes![0].legs![0].steps?.count == 1 && valueResult?.routes![0].legs![0].steps![0].travelMode == "WALKING" {
                            
                            NoTransit.text = "No Transit available"
                            NoTransit.frame = CGRect.init(x: cell.transitScrollView.frame.origin.x + 10, y: cell.transitScrollView.frame.origin.y + 10, width: cell.transitScrollView.frame.width, height: cell.transitScrollView.frame.height)
                            NoTransit.textColor = UIColor.black
                            NoTransit.font = UIFont.setTarcRegular(size: 16.0)
                            NoTransit.textAlignment = NSTextAlignment.center
                            
                            //cell.contentView.addSubview(durationLabel)
                            cell.transitScrollView.addSubview(NoTransit)
                            cell.contentView.addSubview(cell.transitScrollView)
                            
                        }
                        else {
                            var TransitAvalible = Bool()
                            TransitAvalible = false
                            for stepValue in routesPlan![0].steps!
                            {
                                
                                var totalFareFirstmile = [Double]()
                                let transitImage = UIImageView()
                                let arrowImage = UIImageView()
                                let MyTarcButton = UIImageView()
                                let busName = UILabel()
                                let busTimingLable = UILabel()
                                
                                let secondImage = UIImageView()
                                let thirdImage = UIImageView()
                                var transitBool = Bool()
                                
                                let firstLabel = UILabel()
                                let secondLabel = UILabel()
                                let thirdLabel = UILabel()
                                firstLabel.textAlignment = .center
                                
                                transitBool = false
                                let distanceValue = (stepValue.distance?.value)!
                                let durationvaluewalking = (stepValue.duration?.value)!
                                
                                    // distValue = distValue + (stepValue.duration?.value)!
                                    distValue = durationString!
                                    let hourString: Int = distValue / 3600
                                    let minString: Int = (distValue % 3600) / 60
                                    if hourString > 0
                                    {
                                        DispatchQueue.main.async {
                                            durationLabel.text = String(describing: "\(hourString) hr \(minString) min")
                                            //durationLabel.textColor = UIColor.black
                                            durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                            durationLabel.textAlignment = NSTextAlignment.center
                                            //   cell.contentView.addSubview(durationLabel)
                                            cell.transitScrollView.addSubview(durationLabel)
                                        }
                                    }
                                    else
                                    {
                                        DispatchQueue.main.async {
                                            durationLabel.text = String(describing: "\(minString) min")
                                            // durationLabel.textColor = UIColor.black
                                            durationLabel.font = UIFont.setTarcRegular(size: 14.0)
                                            durationLabel.textAlignment = NSTextAlignment.center
                                            //cell.contentView.addSubview(durationLabel)
                                            cell.transitScrollView.addSubview(durationLabel)
                                        }
                                    }
                              
                                //120
                                if indexValue == 0
                                {
                                    
                                    transitImage.frame = CGRect.init(x: 20 * (indexValue + 1), y: 5, width: 35, height: 35)
                                    
                                }
                                else
                                {
                                    print("\(prevArrowFrame.origin.x + prevArrowFrame.size.width + 10)")
                                    transitImage.frame = CGRect.init(x: prevArrowFrame.origin.x + prevArrowFrame.size.width + 10, y: 5, width: 35, height: 35)
                                    
                                }
                                if stepValue.travelMode == "TRANSIT"
                                {
                                    
                                    
                                    
                                    
                                    let userDefaults1 = UserDefaults.standard
                                    if stepValue.transitDetails?.line?.shortName != nil {
                                        
                                        if (stepValue.transitDetails?.line?.shortName!.contains("X"))! {
                                            
                                            // self.TotalFar = self.fareStringEXP + self.TotalFar
                                            fareCount = fareCount + self.fareStringEXP
                                            MyCardfareCount = self.fareStringExpMyCard
                                            
                                        }
                                        else {
                                            // self.TotalFar = self.FareStringLocal + self.TotalFar
                                            fareCount = fareCount + self.FareStringLocal
                                            MyCardfareCount = MyCardfareCount + self.fareStringLocalMycard
                                        }
                                    }
                                    else {
                                        // self.TotalFar = self.FareStringLocal + self.TotalFar
                                        fareCount = fareCount + self.FareStringLocal
                                        MyCardfareCount = MyCardfareCount + self.fareStringLocalMycard
                                    }
                                    
                                    
                                    
                                    
                                    
                                    let urlString = "https:" + (stepValue.transitDetails?.line?.vehicle?.icon)!
                                    let url = URL(string: "https:" + (stepValue.transitDetails?.line?.vehicle?.icon)!)
                                    
                                    
                                    
                                    
                                    if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/subway2.png"
                                    {
                                        transitImage.image = UIImage(named: "train_Transit")
                                    }
                                    else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/rail2.png"
                                    {
                                        transitImage.image = UIImage(named: "train_Transit")
                                    }
                                    else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/bus2.png"
                                    {
                                        if stepValue.transitDetails?.line?.agencies![0].name == "Greater Cleveland Regional Transit Authority"
                                        {
                                            transitImage.image = UIImage(named: "tank_bus")
                                        }
                                        else
                                        {
                                            transitImage.image = UIImage(named: "bus_Transit")
                                        }
                                    }
                                    else if urlString == "https://maps.gstatic.com/mapfiles/transit/iw2/6/tram2.png"
                                    {
                                        transitImage.image = UIImage(named: "tram_transit")
                                    }
                                    else
                                    {
                                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                        transitImage.image = UIImage(data: data!)
                                    }
                                    
                                    busName.frame = CGRect.init(x: transitImage.frame.origin.x + transitImage.frame.size.width, y: 15, width: 30, height: 30)
                                    busName.text = stepValue.transitDetails?.line?.shortName
                                    busName.frame.size = busName.intrinsicContentSize
                                    busName.textColor = UIColor.black
                                    busName.font = UIFont.setTarcBold(size: 14.0)
                                    busName.textAlignment = NSTextAlignment.center
                                    cell.transitScrollView.addSubview(busName)
                                    
                                    busTimingLable.frame = CGRect.init(x: transitImage.frame.origin.x - 5, y: 45, width: 100, height: 30)
                                    busTimingLable.text = stepValue.transitDetails?.departureTime?.text
                                    busTimingLable.frame.size = busTimingLable.intrinsicContentSize
                                    busTimingLable.textColor = UIColor.black
                                    busTimingLable.font = UIFont.setTarcBold(size: 12.0)
                                    busTimingLable.textAlignment = NSTextAlignment.left
                                    cell.transitScrollView.addSubview(busTimingLable)
                                    
                                    // let dep_epoh =
                                    let arr_epoh = stepValue.transitDetails?.arrivalTime?.value!
                                    let CurrentDate = NSDate().timeIntervalSince1970
                                    let departdatetime = dateconvert(Datevalue: CurrentDate)
                                    let arrival_time = dateconvert(Datevalue: arr_epoh!)
                                    print(CurrentDate)
                                    print(arr_epoh)
                                    print(departdatetime)
                                    print(arrival_time)
                                    print(routesPlan ?? "defaut")
                                    
                                    
                                    
                                    let formatter = DateFormatter()
                                    formatter.dateFormat = "dd/MM/yyyy"
                                    let firstDate = formatter.date(from: departdatetime)
                                    let secondDate = formatter.date(from: arrival_time)
                                    print(firstDate!)
                                    print(secondDate!)
                                    let formattera = DateComponentsFormatter()
                                    formattera.allowedUnits = [.day]
                                    formattera.unitsStyle = .full
                                    var DateCalculation = formattera.string(from: firstDate!, to: secondDate!)!
                                    
                                    // var resss = secondDate.interval(ofComponent: .day, fromDate: firstDate)
                                    print(DateCalculation)
                                    // print(components.day)
                                    
                                    if firstDate?.compare(secondDate!) == .orderedAscending {
                                        print("First Date is smaller then second date")
                                        let plusoneday = UILabel()
                                        print("*********** +1 day ***********")
                                        if DateCalculation.contains("days") {
                                            //Str
                                            DateCalculation = DateCalculation.replacingOccurrences(of: "days", with: "day", options:
                                                NSString.CompareOptions.literal, range: nil)
                                            plusoneday.text = "+\(DateCalculation)"
                                        }
                                        else {
                                            plusoneday.text = "+\(DateCalculation)"
                                        }
                                        
                                        plusoneday.frame = CGRect.init(x: busTimingLable.frame.origin.x + 51, y: 45, width: 100, height: 30)
                                        plusoneday.frame.size = plusoneday.intrinsicContentSize
                                        plusoneday.textColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                                        plusoneday.font = UIFont.setTarcBold(size: 10.0)
                                        plusoneday.textAlignment = NSTextAlignment.left
                                        cell.transitScrollView.addSubview(plusoneday)
                                    }
                                    
                                }
                                    
                                else
                                {
                                    let distanceValue = (stepValue.distance?.value)!
                                    print((stepValue.distance?.value)!)
                                    let userDefaults = UserDefaults.standard


                                    
                                        transitImage.image = UIImage(named: "walking_Icon")
                                    
                                }
                                
                                
                                cell.transitScrollView.addSubview(transitImage)
                                if indexValue + 1 < (routesPlan![0].steps?.count)!
                                {
                                    print(stepValue.transitDetails?.line?.vehicle?.icon ?? "nil", stepValue.transitDetails?.line?.shortName ?? "nil")
                                    if stepValue.travelMode == "TRANSIT" && stepValue.transitDetails?.line?.shortName != nil
                                    {
                                        arrowImage.frame = CGRect.init(x: busName.frame.origin.x + busName.frame.size.width + 10, y: 15, width: 24, height: 24)
                                    }
                                    else
                                    {
                                        if transitBool == true
                                        {
                                           
                                            arrowImage.frame = CGRect.init(x: thirdImage.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)
                                            

                                            
                                            
                                            let userDefaulss = UserDefaults.standard
                                            if userDefaulss.bool(forKey: "isCab")
                                            {
                                                arrowImage.frame = CGRect.init(x: secondImage.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)
                                            }
                                            
                                            
                                        }
                                        else
                                        {
                                            arrowImage.frame = CGRect.init(x: transitImage.frame.origin.x + transitImage.frame.size.width + 10, y: 15, width: 24, height: 24)
                                            
                                        }
                                    }
                                    
                                    arrowImage.image = UIImage(named: "Right-Arrow")
                                    prevArrowFrame = arrowImage.frame
                                    cell.transitScrollView.addSubview(arrowImage)
                                }
                                if transitBool == true
                                {
                                    let userDefaulss = UserDefaults.standard
                                    if userDefaulss.bool(forKey: "isCab")
                                    {
                                        cell.transitScrollView.contentSize = CGSize(width: secondImage.frame.origin.x + secondImage.frame.size.width + 30, height: 80)
                                        
                                    }
                                    else
                                    {
                                        cell.transitScrollView.contentSize = CGSize(width: thirdImage.frame.origin.x + thirdImage.frame.size.width + 30, height: 80)
                                    }
                                }
                                else
                                {
                                    cell.transitScrollView.contentSize = CGSize(width: transitImage.frame.origin.x + transitImage.frame.size.width + 30, height: 80)
                                    
                                }
                                indexValue = indexValue + 1
                                
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
                                let fareString = String(format: "%.2f", fareCount)
                                let mycardfarestring = String(format: "%.2f", MyCardfareCount)
                                
                                fareLabel.text = "$\(fareString)"
                                TotalMyTarcFareLabel.text = "$\(mycardfarestring)"
                            })
                            
                            print("fare Count : \(fareCount)")
                            if fareCount > 0
                            {
                                let MyTarcButton = UIImageView()
                                self.TotalFar = 0.0
                                self.MycardTotalFare = 0.0
                                self.MycardTotalFareArray.removeAll()
                                for stepValue in routesPlan![0].steps!
                                {
                                    let userDefaults1 = UserDefaults.standard
                                    if stepValue.travelMode == "TRANSIT"
                                    {
                                        if stepValue.transitDetails?.line?.shortName != nil {
                                            if (stepValue.transitDetails?.line?.shortName!.contains("X"))! {
                                                
                                                self.TotalFar = self.fareStringEXP + self.TotalFar
                                                self.MycardTotalFare = self.fareStringExpMyCard
                                                MycardTotalFareArray.append(self.MycardTotalFare)
                                                
                                            }
                                            else {
                                                self.TotalFar = self.FareStringLocal + self.TotalFar
                                                self.MycardTotalFare = self.fareStringLocalMycard
                                                MycardTotalFareArray.append(self.MycardTotalFare)
                                                
                                            }
                                        }
                                            
                                        else {
                                            self.TotalFar = self.FareStringLocal + self.TotalFar
                                            self.MycardTotalFare = self.fareStringLocalMycard
                                            MycardTotalFareArray.append(self.MycardTotalFare)
                                            
                                        }
                                        durationLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 50, y: 0, width: 95, height: 40)
                                        durationLabel.textColor = UIColor.white
                                        durationLabel.backgroundColor = UIColor.black
                                        
                                       // fareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 160, y: 0, width: 80, height: 40)
                                       // Regular.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 160, y: 30, width: 80, height: 40)
                                      //  TotalMyTarcFareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 245, y: 0, width: 80, height: 40)
                                      //
                                      //  mytarccard.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 160, y: 30, width: 80, height: 40)
                                        
                                        BuyNowButton.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 120, y: 0, width: 120, height: 40)
                                    }
                                    else
                                    {
                                        
                                        durationLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) - 25, y: 0, width: 95, height: 40)
                                        durationLabel.textColor = UIColor.white
                                        durationLabel.backgroundColor = UIColor.black
                                        
                                        
                                        
                                       // fareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 85, y: 0, width: 80, height: 40)
                                       
                                      //  Regular.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 85, y: 30, width: 80, height: 40)
                                        
                                      //  TotalMyTarcFareLabel.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 175, y: 0, width: 80, height: 40)
                                      //  mytarccard.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 175, y: 30, width: 80, height: 40)
                                        BuyNowButton.frame = CGRect.init(x: (cell.transitScrollView.contentSize.width) + 75, y: 0, width: 120, height: 40)
                                    }
                                }
                                
                                Regular.text = "Regular"
                                Regular.font = UIFont.setTarcBold(size: 11.0)
                                Regular.textAlignment = NSTextAlignment.center
                                Regular.textColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                                
                                mytarccard.text = "TARC Ticket"
                                mytarccard.font = UIFont.setTarcBold(size: 11.0)
                                mytarccard.textAlignment = NSTextAlignment.center
                                mytarccard.textColor = UIColor(red: 208 / 255.0, green: 48 / 255.0, blue: 46 / 255.0, alpha: 1.0)
                                self.NewfareString = String(format: "$%.2f", self.TotalFar)
                                fareLabel.text = "\(NewfareString)"
                                fareLabel.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                                fareLabel.textColor = UIColor.white
                                //fareLabel.textColor = UIColor.black
                                fareLabel.font = UIFont.setTarcBold(size: 18.0)
                                fareLabel.textAlignment = NSTextAlignment.center
                                
                                TotalMyTarcFareLabel.backgroundColor = UIColor(red: 208 / 255.0, green: 48 / 255.0, blue: 46 / 255.0, alpha: 1.0)
                                TotalMyTarcFareLabel.textColor = UIColor.white
                                //fareLabel.textColor = UIColor.black
                                TotalMyTarcFareLabel.font = UIFont.setTarcBold(size: 18.0)
                                TotalMyTarcFareLabel.textAlignment = NSTextAlignment.center
                                
                                
                                BuyNowButton.setTitle("View Fare", for: .normal)
                                BuyNowButton.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
                                BuyNowButton.addTarget(self, action: #selector(tapLouveloButton), for: .touchUpInside)
                                BuyNowButton.tag = indexPath.row
                                
                                
                                
                                if MycardTotalFareArray.count > 0 {
                                    self.MycardTotalFare = MycardTotalFareArray.max()!
                                }
                                TotalMyTarcFareLabel.text = String(format: "$%.2f", self.MycardTotalFare)
                                
                                //                        print(self.busNameStrin)
                                //                        print(self.busNameStrin.contains("X"))
                                
                                
                                if TransitAvalible == true
                                {
                                    //                                fareLabel.frame = CGRect.zero
                                    //                                TotalMyTarcFareLabel.frame = CGRect.zero
                                    //                                mytarccard.frame = CGRect.zero
                                    //                                Regular.frame = CGRect.zero
                                    
                                   // cell.transitScrollView.addSubview(fareLabel)
                                    //cell.transitScrollView.addSubview(TotalMyTarcFareLabel)
                                   // cell.transitScrollView.addSubview(mytarccard)
                                  //  cell.transitScrollView.addSubview(Regular)
                                    cell.transitScrollView.addSubview(BuyNowButton)
                                }
                                else
                                {
                                    cell.transitScrollView.addSubview(BuyNowButton)
                                  //  cell.transitScrollView.addSubview(fareLabel)
                                  //  cell.transitScrollView.addSubview(TotalMyTarcFareLabel)
                                  //  cell.transitScrollView.addSubview(mytarccard)
                                  //  cell.transitScrollView.addSubview(Regular)
                                }
                                
                                if comVar.DateImplementation {
                                    // cell.transitScrollView.addSubview(BuyNowButton)
                                }
                                
                                
                                
                                
                                
                                cell.transitScrollView.contentSize = CGSize(width: BuyNowButton.frame.origin.x + BuyNowButton.frame.size.width + 30, height: 60)
                            }
                            cell.contentView.addSubview(cell.transitScrollView)
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapLouveloButton(_:)))
                            tapGesture.numberOfTapsRequired = 1
                            cell.transitScrollView.tag = indexPath.row
                            cell.transitScrollView.addGestureRecognizer(tapGesture)
                            
                            
//                                                let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
//                                                tapGesture1.numberOfTapsRequired = 1
//                                                fareLabel.tag = indexPath.row
//                                                fareLabel.addGestureRecognizer(tapGesture1)
//
//                                                let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
//                                                cell.transitScrollView.tag = indexPath.row
//                                                //cell.transitScrollView.addGestureRecognizer(longPressRecognizer)
                            
                        }
                        return cell
                    }
                    else if cellHedding[indexPath.section] as? String == "Bikeshare"
                    {
                                      //  let cell: TripSelectionTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TripSelectionTableViewCell?)!
                                        tableView.separatorColor = UIColor.clear;
                                        cell.selectionStyle = .none
                                        print("\(self.view.frame.size.width)")
                                        //  cell.contentView.backgroundColor = UIColor.red
                                        let firstButton = UIButton()
                                        firstButton.frame = CGRect.init(x: 10, y: 0, width: self.view.frame.size.width / 2, height: 50)
                                        var image: UIImage? = UIImage(named: "Image-7")?.withRenderingMode(.alwaysOriginal)

                                        firstButton.setImage(image, for: .normal)
                                        firstButton.backgroundColor = UIColor.white
                                        firstButton.imageView?.image = image
                                        firstButton.imageView?.tintColor = UIColor.gray.withAlphaComponent(0.5)
                        firstButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                                        firstButton.titleLabel?.numberOfLines = 2
                                        firstButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                                       firstButton.addTarget(self, action: #selector(tapLouveloButton), for: .touchUpInside)
                                        firstButton.setTitleColor(UIColor.black, for: .normal)
                                        firstButton.contentHorizontalAlignment = .left
                                        cell.contentView .addSubview(firstButton)

                                       
                                            firstButton.setTitle("View Fare", for: .normal)
                                           // firstButton.isEnabled = false
                                           // firstButton.alpha = 0.5
                                       





                                        let secondButton = UIButton()
                                        secondButton.frame = CGRect.init(x: firstButton.frame.maxX + 0, y: 0, width: self.view.frame.size.width / 2, height: 50)
                                        secondButton.setImage(UIImage(named: "Image-8"), for: .normal)
                                        secondButton.backgroundColor = UIColor.white
                                        secondButton.imageView?.image = UIImage.init(named: "Image-8")
                                        secondButton.titleLabel?.numberOfLines = 2
                                        secondButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                                        secondButton.addTarget(self, action: #selector(tapLouveloButton(_:)), for: .touchUpInside)
                                        secondButton.setTitleColor(UIColor.black, for: .normal)
                        secondButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                                        secondButton.contentHorizontalAlignment = .left
                                        cell.contentView .addSubview(secondButton)
                                    secondButton.setTitle("View Fare", for: .normal)




                                        let thirdButton = UIButton()
                                        thirdButton.frame = CGRect.init(x: 10, y: 60, width: self.view.frame.size.width / 2, height: 50)
                                        thirdButton.setImage(UIImage(named: "Image-9"), for: .normal)
                                        thirdButton.backgroundColor = UIColor.white
                                        thirdButton.imageView?.image = UIImage.init(named: "Image-9")
                                        thirdButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                                        thirdButton.addTarget(self, action: #selector(tapLouveloButton(_:)), for: .touchUpInside)
                        thirdButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                                        // thirdButton.titleLabel!.font = UIFont.systemFont(ofSize: 14.0)
                                        thirdButton.titleLabel?.numberOfLines = 2
                                        thirdButton.setTitleColor(UIColor.black, for: .normal)
                                        thirdButton.contentHorizontalAlignment = .left
                                        cell.contentView .addSubview(thirdButton)
 thirdButton.setTitle("View Fare", for: .normal)
                                    





                                        let fourthButton = UIButton()
                                        fourthButton.frame = CGRect.init(x: thirdButton.frame.maxX + 0, y: 60, width: self.view.frame.size.width / 2, height: 50)
                                        fourthButton.setImage(UIImage(named: "louveloSelect"), for: .normal)
                                        fourthButton.backgroundColor = UIColor.white
                                        fourthButton.imageView?.image = UIImage.init(named: "louveloSelect")?.noirs
                                        fourthButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                                        fourthButton.addTarget(self, action: #selector(tapLouveloButton), for: .touchUpInside)

                        fourthButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                                        fourthButton.titleLabel?.numberOfLines = 2
                                        fourthButton.setTitleColor(UIColor.black, for: .normal)
                                        fourthButton.contentHorizontalAlignment = .left
                                        cell.contentView .addSubview(fourthButton)

                                        fourthButton.setTitle("View Fare", for: .normal)


                                        return cell
                                    }
        else if cellHedding[indexPath.section] as? String == "Rideshare"
                                 
                    {
                       // let cell: TripSelectionTableViewCell = (tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TripSelectionTableViewCell?)!
                        tableView.separatorColor = UIColor.clear;
                        cell.selectionStyle = .none

                        let firstButton = UIButton()
                        firstButton.frame = CGRect.init(x: 10, y: 0, width: cell.contentView.frame.size.width / 2, height: cell.contentView.frame.size.height)
                        firstButton.setImage(UIImage(named: "Uber-1"), for: .normal)
                        firstButton.backgroundColor = UIColor.white
                        firstButton.imageView?.image = UIImage.init(named: "Uber-1")

                        let percentagelowvalue = comVarUber.Lyft_estimationPrice / 100
                        let percentagehighvalue = comVarUber.Lyft_estimationPriceHigh / 100
                        let calculatelowper = Double(percentagelowvalue) / 10 as Double
                        let calculatehighper = Double(percentagehighvalue) / 10 as Double
                        let totalvalueofLow = Double(percentagelowvalue) + calculatelowper
                        let totalvalueofHigh = Double(percentagehighvalue) + calculatehighper
                        print("NEw Total value \(totalvalueofLow) ad \(totalvalueofHigh)")
                        let xvalue = totalvalueofLow.rounded()
                        print("\(xvalue)")
                        let yvalue = totalvalueofHigh.rounded()
                        print("\(yvalue)")
                        let xintValue = Int(xvalue)
                        let yintValue = Int(yvalue)

                        firstButton.setTitle("View Fare", for: .normal)
                        firstButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                        firstButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                       // firstButton.addTarget(self, action: #selector(UberButtonTapFunction), for: .touchUpInside)

                        firstButton.setTitleColor(UIColor.black, for: .normal)
                        firstButton.contentHorizontalAlignment = .left
                        cell.contentView .addSubview(firstButton)


                        let secondButton = UIButton()
                        secondButton.frame = CGRect.init(x: firstButton.frame.maxX + 0, y: 0, width: cell.contentView.frame.size.width / 2, height: cell.contentView.frame.size.height)
                        secondButton.setImage(UIImage(named: "Lyft"), for: .normal)
                        secondButton.backgroundColor = UIColor.white
                        firstButton.addTarget(self, action: #selector(tapLouveloButton), for: .touchUpInside)


                        secondButton.imageView?.image = UIImage.init(named: "Lyft")



                        //
                        var overallDistance = 0.0
                        print(comVarUber.LyftDistnce)
                        print(comVarUber.Lyft_Travel_Min)
                        print(comVarUber.LyftDistnce)
                        let LyftTravelTime = self.hourminConverterForUber(distValue: comVarUber.Lyft_Travel_Min + 300)
                        print(LyftTravelTime)

                      




                        secondButton.setTitle("View Fare", for: .normal)
                        secondButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 0.0)
                       secondButton.addTarget(self, action: #selector(tapLouveloButton), for: .touchUpInside)

                        secondButton.titleLabel?.font = UIFont.setTarcBold(size: 11.0)
                        secondButton.titleLabel?.numberOfLines = 2
                        secondButton.setTitleColor(UIColor.black, for: .normal)
                        secondButton.contentHorizontalAlignment = .left
                        cell.contentView .addSubview(secondButton)


                        return cell
                    }
        
        
        return cell
                        
        }
                   
                    




     
    @objc func checkAction(_ sender:UITapGestureRecognizer){

         print("Lyft TAP********")
        
        let activitytype = "Lyft"
        let Source_Address_analytics = self.sourceAddresstest!
        let Destination_Address_analytics =  self.DestinationAddresstest!
        print(Source_Address_analytics)
        print(Destination_Address_analytics)
        print(self.sourcePlaceId!)
        print(self.DestinationPlaceId!)
        print(self.Total_transitdistance2!)
        print(self.Total_transitduration2!)
        print(self.Total_transitdistance2!)
        print(self.LyftTotalCost!)
        
        
        
        analytics.getActivity(Activity_Type: activitytype, Source_Address: Source_Address_analytics, Destination_Address: Destination_Address_analytics, Amenitites: "", Source_placeid: self.sourcePlaceId!, Destination_placeid: self.DestinationPlaceId!, Total_walktime: 0, Total_walkingdistance: 0, Total_transitdistance: self.Total_transitdistance2!, NumberofTransit: 1, Total_triptime: self.Total_transitduration2!, Total_transittime: self.Total_transitduration2!, Total_tripdistance: self.Total_transitdistance2!, highcost: Double(self.LyftTotalCost!), Application: 2, Firstmile: 0, Amenitites_Category: "")
        
        
    }

    @objc func buttonAction() {
       print("TAP********")
   //getActivity analytics.geta
        
        let activitytype = "Uber"
        let Source_Address_analytics = self.sourceAddresstest!
        let Destination_Address_analytics =  self.DestinationAddresstest!
        print(Source_Address_analytics)
        print(Destination_Address_analytics)
        print(self.sourcePlaceId!)
         print(self.DestinationPlaceId!)
         print(self.Total_transitdistance1!)
         print(self.Total_transitduration1!)
         print(self.Total_transitdistance1!)
         print(self.uberTotalcost!)
       
        
        
        analytics.getActivity(Activity_Type: activitytype, Source_Address: Source_Address_analytics, Destination_Address: Destination_Address_analytics, Amenitites: "", Source_placeid: self.sourcePlaceId!, Destination_placeid: self.DestinationPlaceId!, Total_walktime: 0, Total_walkingdistance: 0, Total_transitdistance: self.Total_transitdistance1!, NumberofTransit: 1, Total_triptime: self.Total_transitduration1!, Total_transittime: self.Total_transitduration1!, Total_tripdistance: self.Total_transitdistance1!, highcost: Double(self.uberTotalcost!), Application: 2, Firstmile: 0, Amenitites_Category: "")
        
    }
    func dateconvert(Datevalue:Double) -> String {
        let date = Date(timeIntervalSince1970: Datevalue)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd/MM/yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    @objc func tapBlurButton(_ sender: UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
        
        
        nextViewController.sourceAddresstest = self.sourceAddresstest
        nextViewController.sourcePlaceId = self.sourcePlaceId
        nextViewController.sourcelat = self.sourcelat
        nextViewController.sourcelng = self.sourcelng
        
        nextViewController.DestinationAddresstest = self.DestinationAddresstest
        nextViewController.DestinationPlaceId = self.DestinationPlaceId
        nextViewController.Destinationlat = self.Destinationlat
        nextViewController.Destinationlng = self.Destinationlng
        
        nextViewController.tripSchduleTime = self.tripSchduleTime
        nextViewController.BirdDatalistapicount = self.BirdDatalistapicount
        nextViewController.iscalendarEvent = self.isCalenderEvent
        
        nextViewController.BirdBikeLat = self.BirdBikeLat
        nextViewController.BirdBikeLon = self.BirdBikeLon
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @objc func tapBlurBikeButton(_ sender: UITapGestureRecognizer){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
        
        
        nextViewController.sourceAddresstest = self.sourceAddresstest
        nextViewController.sourcePlaceId = self.sourcePlaceId
        nextViewController.sourcelat = self.sourcelat
        nextViewController.sourcelng = self.sourcelng
        
        nextViewController.DestinationAddresstest = self.DestinationAddresstest
        nextViewController.DestinationPlaceId = self.DestinationPlaceId
        nextViewController.Destinationlat = self.Destinationlat
        nextViewController.Destinationlng = self.Destinationlng
        
        nextViewController.tripSchduleTime = self.tripSchduleTime
        nextViewController.BirdDatalistapicount = self.BirdDatalistapicount
        nextViewController.iscalendarEvent = self.isCalenderEvent
        nextViewController.BirdBikeLat = self.BirdBikeLat
        nextViewController.BirdBikeLon = self.BirdBikeLon
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @objc func tapLouveloButton(_ sender: UITapGestureRecognizer){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
        
        
        nextViewController.sourceAddresstest = self.sourceAddresstest
        nextViewController.sourcePlaceId = self.sourcePlaceId
        nextViewController.sourcelat = self.sourcelat
        nextViewController.sourcelng = self.sourcelng
        
        nextViewController.DestinationAddresstest = self.DestinationAddresstest
        nextViewController.DestinationPlaceId = self.DestinationPlaceId
        nextViewController.Destinationlat = self.Destinationlat
        nextViewController.Destinationlng = self.Destinationlng
        
        nextViewController.tripSchduleTime = self.tripSchduleTime
        nextViewController.BirdDatalistapicount = self.BirdDatalistapicount
        nextViewController.iscalendarEvent = self.isCalenderEvent
        nextViewController.BirdBikeLat = self.BirdBikeLat
        nextViewController.BirdBikeLon = self.BirdBikeLon
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
        
        
        nextViewController.sourceAddresstest = self.sourceAddresstest
        nextViewController.sourcePlaceId = self.sourcePlaceId
        nextViewController.sourcelat = self.sourcelat
        nextViewController.sourcelng = self.sourcelng
        
        nextViewController.DestinationAddresstest = self.DestinationAddresstest
        nextViewController.DestinationPlaceId = self.DestinationPlaceId
        nextViewController.Destinationlat = self.Destinationlat
        nextViewController.Destinationlng = self.Destinationlng
        
        nextViewController.tripSchduleTime = self.tripSchduleTime
        nextViewController.BirdDatalistapicount = self.BirdDatalistapicount
        nextViewController.iscalendarEvent = self.isCalenderEvent
        
        nextViewController.BirdBikeLat = self.BirdBikeLat
        nextViewController.BirdBikeLon = self.BirdBikeLon
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
        
        
        nextViewController.sourceAddresstest = self.sourceAddresstest
        nextViewController.sourcePlaceId = self.sourcePlaceId
        nextViewController.sourcelat = self.sourcelat
        nextViewController.sourcelng = self.sourcelng
        
        nextViewController.DestinationAddresstest = self.DestinationAddresstest
        nextViewController.DestinationPlaceId = self.DestinationPlaceId
        nextViewController.Destinationlat = self.Destinationlat
        nextViewController.Destinationlng = self.Destinationlng
        
        nextViewController.tripSchduleTime = self.tripSchduleTime
        nextViewController.BirdDatalistapicount = self.BirdDatalistapicount
        nextViewController.iscalendarEvent = self.isCalenderEvent
        
        nextViewController.BirdBikeLat = self.BirdBikeLat
        nextViewController.BirdBikeLon = self.BirdBikeLon
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    func loginButton(_ button: LoginButton, didLogoutWithSuccess success: Bool) {
        // success is true if logout succeeded, false otherwise
    }
    
    func loginButton(_ button: LoginButton, didCompleteLoginWithToken accessToken: AccessToken?, error: NSError?) {
        if let _ = accessToken {
            // AccessToken Saved
            UserDefaults.standard.set(accessToken?.tokenString, forKey: "uberKey")
            UserDefaults.standard.set(accessToken?.refreshToken, forKey: "uberRefershKey")
            tableView.reloadData()
        }
    }
    func distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double, unit: String) -> Double {
        let theta = lon1 - lon2
        var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg: lat2)) + cos(deg2rad(deg: lat1)) * cos(deg2rad(deg: lat2)) * cos(deg2rad(deg: theta))
        dist = acos(dist)
        dist = rad2deg(rad: dist)
        dist = dist * 60 * 1.1515
        if (unit == "K") {
            dist = dist * 1.609344
        }
        else if (unit == "N") {
            dist = dist * 0.8684
        }
        return dist
    }
    func deg2rad(deg: Double) -> Double {
           return deg * M_PI / 180
       }
    func rad2deg(rad: Double) -> Double {
          return rad * 180.0 / M_PI
      }

    func hourminConverter(distValue: Int) -> String {
        let hourString: Int = distValue / 3600
        let minString: Int = (distValue % 3600) / 60
        if hourString > 0
            {
            return "\(hourString) hr \(minString) min"

        }
        else
        {
            return "\(minString) min"
        }
    }
    func hourminConverterForUber(distValue: Int) -> String {
        let minString: Int = (distValue % 3600) / 60

        return "\(minString)"

    }
}
class SuggestedRoutecell: UITableViewCell {
    
    let placeSourceLabel = UILabel()
    let placeDestLabel = UILabel()
    let ticketImage = UIImageView()
    let busLable = UILabel()
    let fareLabel = UILabel()
    let validLabel = UILabel()
    let timeLabel = UILabel()
    let activeLable = UILabel()
    let startLable = UILabel()
    let endLable = UILabel()
    let transitScrollView = UIScrollView()
    var uberbutton = RideRequestButton()
    let transitNameLabel = UILabel()
    let transitFareLabel = UILabel()
    var uberBtn = RideRequestButton()
    var lyftButton = LyftButton()
    let uberBackView = UIView()
    let expiryTitleLabel = UILabel()
    let expiryLabel = UILabel()
    let mytarcFare = UILabel()
    let mytarcImage = UIImageView()
    let DistanceLabelUber = UILabel()
    let NoBikestation = UILabel()
    override func prepareForReuse() {
        super.prepareForReuse()
       // TicketnumberLabel.text = nil
        placeDestLabel.text = nil
        mytarcImage.image = nil
        placeDestLabel.text = nil
        ticketImage.image = nil
        busLable.text = nil
        fareLabel.text = nil
        validLabel.text = nil
        timeLabel.text = nil
        activeLable.text = nil
        startLable.text = nil
        endLable.text = nil
        expiryLabel.text = nil
        expiryTitleLabel.text = nil
        transitNameLabel.text = nil
        transitFareLabel.text = nil
        mytarcFare.text = nil
        DistanceLabelUber.text = nil
        NoBikestation.text = nil
        
        uberBtn = RideRequestButton()
        lyftButton = LyftButton()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            if touch.view == lyftButton {
                
            }  else {
                return
            }
        }
        
    }
    
}
