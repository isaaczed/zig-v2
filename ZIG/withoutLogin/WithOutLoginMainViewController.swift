//
//  MainViewController.swift
//  Tarc
//
//  Created by Isaac on 21/03/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces
import Alamofire
import AlamofireObjectMapper
import Toast_Swift
import SCLAlertView
import UberCore




class WithOutLoginMainViewController: UIViewController,CLLocationManagerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,LoginButtonDelegate {
    var currentlocationAnalytics:Bool = true
   //  var tableView: JXBottomSheetTableView!
      var markersArray: [State] = []
    var sourcePlace :GMSPlace?
    var destinationPlaceGoogle : GMSPlace?
    let cellReuseIdentifier = "cell"
    var responseVarAddress = [AddressList]()
    var AddressSearch = [AddressListGoogle]()
     var headerLabel: UILabel!
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var autofillTableView: UITableView = UITableView()
    var autofillTableView1: UITableView = UITableView()
    var resultBackView = UIView()
    var resultBackView1 = UIView()
    //var mapView: GMSMapView!
   // var GoogleTransitResponse:DataResponse<SuggestDirectionaClass>!
    var placesClient: GMSPlacesClient!
    var zoomLevel: Float = 15.0
    var sourcePlaceID:String?
    var sourceLat:Double?
    var sourceLng:Double?
    var sourceAddressText:String?
    var DestinationPlaceID:String?
    var DestinationLat:Double?
    var DestinationLng:Double?
    var DestinationText:String?
    var CorrectRegin:Bool = true
     var tripSchduleTime = Date()
    var source_marker = GMSMarker()
    var destination_marker = GMSMarker()
    var isSourceMarker = false
    var activeTextField:UITextField?
    var autoFillbounds = GMSCoordinateBounds()
    var fetcher: GMSAutocompleteFetcher?
    var GooglePlaceResponse = [Result]()
    @IBOutlet weak var AmenitiesContainer: UIView!
    var SourceBool:Bool = true
    var DestinationBool:Bool = false
        var markerDict : NSMutableArray = []
    var amaType = "bus_station"
    var isCurrentLoc : Bool = true
    var calendarEvent:Bool?
    var BirdDatalistapicount:Int?
    var isCalenderEvent = false
    var alertView = SCLAlertView()

    struct State {
        let name: String
        let long: CLLocationDegrees
        let lat: CLLocationDegrees
        let placeId : String
        let distanceVale : String
        let formattedAddress : String
        let typePlace : String
        let imageUrl : String
    }
    
    @IBOutlet weak var SourceTextBox: UITextField!
    @IBOutlet weak var DestinationTextBox: UITextField!
    @IBOutlet weak var viewMap: GMSMapView!
    // An array to hold the list of likely places.
    var likelyPlaces: [GMSPlace] = []
    
    // The currently selected place.
    var selectedPlace: GMSPlace?
    
    override func viewDidAppear(_ animated: Bool) {
        
        if UserDefaults.standard.bool(forKey: "hasViewedWalkthrough") {
            return
        }
       
        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
        if let walkthroughViewController = storyboard.instantiateViewController(withIdentifier: "WalkthroughViewController") as? WalkthroughViewController {
            present(walkthroughViewController, animated: true, completion: nil)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
    

    }
    func perferenceMethod()
    {
                let userDefaults1 = UserDefaults.standard
        let url = "\(APIUrl.ZIGTARCAPI)Preferences/GetAppPreferences?"
        let parameters: Parameters = [
            "Pin": "ZIG19",
            
            ]
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<preferenceGet>) in
                switch response.result {
                case .success:
                    print(response.result.value ?? "nil")
                    if response.result.value?.Message != nil
                    {
                        userDefaults1.set("true", forKey: "isBusOn")
                        userDefaults1.set(response.result.value?.Bird, forKey: "isBirdOn")
                        userDefaults1.set(response.result.value?.Louvelo, forKey: "LouveloOn")
                        userDefaults1.set(response.result.value?.Uber, forKey: "UberOn")
                        userDefaults1.set(response.result.value?.Lyft, forKey: "LyftOn")
                        userDefaults1.set(response.result.value?.Bolt, forKey: "BoltOn")
                        userDefaults1.set(response.result.value?.Lime, forKey: "LimeOn")
                        userDefaults1.set(response.result.value?.Livepayment, forKey: "Livepayment")
                        userDefaults1.set(response.result.value?.COVID, forKey: "EmergencyAlert")
                      //  userDefaults1.set(response.result.value?.COVID, forKey: "EmergencyAlert")

                        userDefaults1.synchronize()
                            //let ShowEmergencyAlert = userDefaults1.object(forKey: "ShowEmergencyAlert") as! String
                          //  userDefaults1.value(forKey: "ShowEmergencyAlert")
                          //  print(response.result.value!.COVID!)
                        //    print(ShowEmergencyAlert)
//                        if ((response.result.value!.COVID!) && (userDefaults1.object(forKey: "ShowEmergencyAlert") == nil)) {
//                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                            let vc = storyboard.instantiateViewController(withIdentifier: "EmergencyAlertViewcontrollerViewController") as! EmergencyAlertViewcontrollerViewController
//                            // vc.newsObj = newsObj
//                            self.present(vc, animated: true, completion: nil)
//                        }
                        //self.perferenceMethod(accessToken: accessToken as! NSString)
                    }
                    
                case .failure(let error):
                    print(error)
                   
                    
                }
        }
        

     
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
    
         let userDefaults1 = UserDefaults.standard
        let accessToken = userDefaults1.value(forKey: "accessToken")
        print(accessToken ?? nil ?? "no token")
//        UIView.transition(with: self.view, duration: 1, options: .transitionFlipFromLeft, animations: {
//            //            vc.view.removeFromSuperview()
//            //            let viewControllerToBePresented = UIViewController()
//            //            self.view.addSubview(viewControllerToBePresented.view)
//
//
//        }, completion: nil)
        
        SourceTextBox?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                                 for: .editingChanged)
        DestinationTextBox?.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                                      for: .editingChanged)
        
        
        if accessToken != nil
        {
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
//             self.navigationController?.pushViewController(nextViewController, animated: true)
//
//            userDefaults1.set("false", forKey: "showPreferencesOnLogin")
            
        }
       // AmenitiesContainer.isHidden = true
        self.SourceTextBox.delegate = self
        self.DestinationTextBox.delegate = self
       self.navigationController?.navigationBar.barTintColor = color.hexStringToUIColor(hex: "#007FC5")
      self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
//                                         style: UIBarButtonItemStyle.done ,
//                                         target: self, action: #selector(OnBackClicked))
//        self.navigationItem.leftBarButtonItem = backButton
//
//
      // configureExpandingMenuButton()
        // Do any additional setup after loading the view.
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        placesClient = GMSPlacesClient.shared()
        
     
     
      
        
        headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 70))
        headerLabel.textAlignment = .center
        headerLabel.textColor = UIColor.white
        headerLabel.backgroundColor = UIColor.darkGray
        headerLabel.text = "Near by Bus stop's"
      //  tableView.tableHeaderView = headerLabel
      
        
        let camera = GMSCameraPosition.camera(withLatitude: 38.2527, longitude: -85.7585, zoom: 12.0)
        viewMap.camera = camera
        viewMap.isMyLocationEnabled = true
        viewMap.settings.myLocationButton = true
        viewMap.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        analytics.GetPageHitCount(PageName: "Trip Planner - No Login")
        
       perferenceMethod()
    }
    func checkLocationwithoutLogin()
    {
        
        
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways || CLLocationManager.authorizationStatus() ==  .notDetermined){
            if self.SourceTextBox.text != "" && self.DestinationTextBox.text != "" {
                print(SourceTextBox.text == DestinationTextBox.text)
                //            print(SourceTextBox.text)
                //            print(DestinationTextBox.text)
                if SourceTextBox.text == DestinationTextBox.text
                {
                    self.view.makeToast("Source and Destination address are same. Cannot process your search.")
                    
                }
                else {
                    TripPlan()
                    
                }
            }
            else if self.SourceTextBox.text == "" {
                self.view.makeToast("Enter source address")
            }
            else if self.DestinationTextBox.text == "" {
                self.view.makeToast("Enter destination address")
            }
            else{
                self.view.makeToast("Enter Source and Distination")
            }
            
        }
        else{
            
            
          
            
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            alertView.hideView()
            alertView = SCLAlertView(appearance: appearance)
            
            alertView.addButton("Location setting"){
                //ComValue.internetalertstatus = true
                guard let settingsUrl = URL(string:UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        // Checking for setting is opened or not
                        print("Setting is opened: \(success)")
                        self.checkLocationwithoutLogin()
                        
                    })
                }
            }
             alertView.addButton("Close"){
                 self.checkLocationwithoutLogin()
            }
            print("LocationARUN==>C")

            alertView.showWarning("Location Denied", subTitle: "Please enable location access for TARC in your iOS settings")
        }
    }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: true)
    }
    func alertOnboardingSkipped(_ currentStep: Int, maxStep: Int) {
        print("Onboarding skipped the \(currentStep) step and the max step he saw was the number \(maxStep)")
    }
    
    func alertOnboardingCompleted() {
        print("Onboarding completed!")
    }
    
    func alertOnboardingNext(_ nextStep: Int) {
        print("Next step triggered! \(nextStep)")
    }
    @IBAction func SwapButtonFunction(_ sender: Any) {
        
        let sourceY = SourceTextBox.frame.origin.y as! CGFloat
        let destinationY = DestinationTextBox.frame.origin.y as! CGFloat
        
          
        UIView.animate(withDuration: 0.3, animations: {
            self.SourceTextBox.frame = CGRect.init(x: self.SourceTextBox.frame.origin.x, y: destinationY, width: self.SourceTextBox.frame.width, height: self.SourceTextBox.frame.height)
            self.DestinationTextBox.frame = CGRect.init(x: self.DestinationTextBox.frame.origin.x, y: sourceY, width: self.DestinationTextBox.frame.width, height: self.DestinationTextBox.frame.height)
            print("Arun After Swipe Souce\(destinationY) and destination \(sourceY)")
            
            
            
            
            print("***Swipe")
        }) { finish in
            let source = self.SourceTextBox.text as! NSString
            let destination = self.DestinationTextBox.text as! NSString
            
            self.SourceTextBox.text = destination as String
            self.DestinationTextBox.text = source as String
            
            let latSourceMaker =  self.destination_marker.position.latitude
            let lngSourceMaker = self.destination_marker.position.longitude
            
            let latMaker = self.source_marker.position.latitude
            let lngMaker = self.source_marker.position.longitude
            
            let sLatTrip = self.sourceLat
            let sLngTrip = self.sourceLng
            
            let dLatTrip = self.DestinationLat
            let dLngTrip = self.DestinationLng
            
            let sourcePlaceID = self.sourcePlaceID
            let destinationPlacecID = self.DestinationPlaceID
            
            self.destination_marker.position = CLLocationCoordinate2D(latitude: latMaker, longitude: lngMaker)
            self.source_marker.position = CLLocationCoordinate2D(latitude: latSourceMaker, longitude: lngSourceMaker)
            self.sourceLat = dLatTrip
            self.sourceLng = dLngTrip
            self.DestinationLat = sLatTrip
            self.DestinationLng = sLngTrip
            self.sourcePlaceID = destinationPlacecID
            self.DestinationPlaceID = sourcePlaceID

         //   DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // Change `2.0` to the desired number of seconds.
                // Code you want to be delayed
                
                self.SourceTextBox.frame = CGRect.init(x: self.SourceTextBox.frame.origin.x, y: sourceY, width: self.SourceTextBox.frame.width, height: self.SourceTextBox.frame.height)
                self.DestinationTextBox.frame = CGRect.init(x: self.DestinationTextBox.frame.origin.x, y: destinationY, width: self.DestinationTextBox.frame.width, height: self.DestinationTextBox.frame.height)
                
           // }
        }
        
        
        print("***Ended")
    }
    @IBAction func LoginAction(_ sender: Any) {
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
//        self.navigationController?.pushViewController(nextViewController, animated: false)
        
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "initalscreenViewController") as! initalscreenViewController
        self.navigationController?.pushViewController(nextViewController, animated: false)
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select")
        if tableView == autofillTableView {
            if self.responseVarAddress.count > indexPath.row || self.AddressSearch.count > indexPath.row
            {
                
                print("Testing")
                if self.responseVarAddress.count != 0 {
                    
                    //                    let PlaceLat = self.responseVarAddress[indexPath.row].Lat
                    //                    let PlaceLong = self.responseVarAddress[indexPath.row].Long
                    guard let PlaceLat = self.responseVarAddress[indexPath.row].Lat else { return }
                    
                    let PlaceLatDouble = Double(PlaceLat)
                    
                    guard let PlaceLong = self.responseVarAddress[indexPath.row].Long else { return }
                    
                    let PlaceLongDouble = Double(PlaceLong)
                    
                    DispatchQueue.main.async {
                        //  self.sourcePlace = (self.autoFillArray[indexPath.row] as? GMSPlace)
                        // print(self.sourcePlace?.coordinate ?? "nil")
                        
                        
                        let latMaker = PlaceLatDouble
                        let lngMaker = PlaceLongDouble
                        print(latMaker!,lngMaker!)
                        self.source_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                        self.source_marker.title = self.responseVarAddress[indexPath.row].FullAddress
                        self.SourceBool = true
                        self.source_marker.icon = UIImage(named: "source_Marker")
                        self.source_marker.map = self.viewMap
                        self.sourceLat = PlaceLatDouble!
                        self.sourceLng = PlaceLongDouble!
                        self.sourcePlaceID = self.responseVarAddress[indexPath.row].placeID!
                        self.isCurrentLoc = false;
                        self.SourceTextBox.text = self.responseVarAddress[indexPath.row].FullAddress
                        self.SourceTextBox.resignFirstResponder()
                        //  self.destinationTextField.becomeFirstResponder()
                        
                        //self.markersArray.append(State(name: self.responseVarAddress[indexPath.row].FullAddress!, long: CLLocationDegrees(PlaceLatDouble!), lat: CLLocationDegrees(PlaceLongDouble!), placeId:" ", distanceVale: "", formattedAddress:  self.responseVarAddress[indexPath.row].FullAddress!, type: "", imageUrl: ""))
                        
                        
                        
                        self.resultBackView.isHidden = true
                        // self.CheckFavTrip()
                        var camera = GMSCameraPosition()
                        if UIDevice.current.userInterfaceIdiom == .pad{
                            camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 12.0)
                        }
                            
                        else
                        {
                            camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 14.0)
                        }
                        self.viewMap?.animate(to: camera)
                        self.amaType = "bus_station"
                        self.getPlaces(GooglePlaces: self.amaType)
                        
                    }
                    
                }
                else if self.AddressSearch.count != 0 {
                    let Place_ID = AddressSearch[indexPath.row].place_id
                    //  pPlaceID = "ChIJXbmAjccVrjsRlf31U1ZGpDM"
                    self.placesClient.lookUpPlaceID(Place_ID!, callback: { (place, error) -> Void in
                        
                        if let error = error {
                            print("lookup place id query error: \(error.localizedDescription)")
                            return
                        }
                        
                        if let place = place {
                            print("Place name \(place.name)")
                            print("Place address \(place.formattedAddress!)")
                            print("Place placeID \(place.placeID)")
                            // print("Place attributions \((place.attributions)!)")
                            print("\(place.coordinate.latitude)")
                            print("\(place.coordinate.longitude)")
                            self.sourcePlace = place
                            DispatchQueue.main.async {
                                //  self.sourcePlace = (self.autoFillArray[indexPath.row] as? GMSPlace)
                                print(self.sourcePlace?.coordinate ?? "nil")
                                
                                
                                let latMaker = self.sourcePlace?.coordinate.latitude
                                let lngMaker = self.sourcePlace?.coordinate.longitude
                                print(latMaker!,lngMaker!)
                                self.source_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                                self.source_marker.title = self.sourcePlace?.name
                                self.SourceBool = true
                                self.source_marker.icon = UIImage(named: "source_Marker")
                                self.source_marker.map = self.viewMap
                                self.sourceLat = (self.sourcePlace?.coordinate.latitude)!
                                self.sourceLng = (self.sourcePlace?.coordinate.longitude)!
                                self.sourcePlaceID = (self.sourcePlace?.placeID)!
                                self.isCurrentLoc = false;
                                self.SourceTextBox.text = self.sourcePlace?.name
                                self.SourceTextBox.resignFirstResponder()
                                //  self.destinationTextField.becomeFirstResponder()
                                
                                // self.markersArray.append(State(name: self.sourcePlace!.name, long: CLLocationDegrees((self.sourcePlace?.coordinate.latitude)!), lat: CLLocationDegrees((self.sourcePlace?.coordinate.longitude)!), placeId:" ", distanceVale: "", formattedAddress:  (self.sourcePlace?.name)!, type: "", imageUrl: ""))
                                self.DestinationBool = true
                                
                                
                                self.resultBackView.isHidden = true
                                //self.CheckFavTrip()
                                var camera = GMSCameraPosition()
                                if UIDevice.current.userInterfaceIdiom == .pad{
                                    camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 12.0)
                                }
                                    
                                else
                                {
                                    camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 14.0)
                                }
                                self.viewMap?.animate(to: camera)
                                self.amaType = "bus_station"
                                self.getPlaces(GooglePlaces: self.amaType)
                                
                            }
                            
                        } else {
                            print("No place details for \((Place_ID)!)")
                        }
                    })
                }
                
                
            }
            
        }
        else
        {
            if self.responseVarAddress.count > indexPath.row || self.AddressSearch.count > indexPath.row
                
            {
                if self.responseVarAddress.count != 0  {
                    
                    guard let PlaceLat = self.responseVarAddress[indexPath.row].Lat else { return }
                    
                    let PlaceLatDouble = Double(PlaceLat)
                    
                    guard let PlaceLong = self.responseVarAddress[indexPath.row].Long else { return }
                    
                    let PlaceLongDouble = Double(PlaceLong)
                    
                    
                    
                    DispatchQueue.main.async {
                        // self.destinationPlace = (self.autoFillArray[indexPath.row] as! GMSPlace)
                        
                        
                        self.DestinationLat = PlaceLatDouble!
                        self.DestinationLng = PlaceLongDouble!
                        self.DestinationPlaceID = self.responseVarAddress[indexPath.row].placeID!
                        // print(self.destinationPlace?.coordinate ?? "nil")
                        self.DestinationTextBox.text = self.responseVarAddress[indexPath.row].FullAddress
                        self.DestinationTextBox.resignFirstResponder()
                        self.resultBackView1.isHidden = true
                        self.DestinationBool = true
                        
                        let latMaker = PlaceLatDouble
                        let lngMaker = PlaceLongDouble
                        print(latMaker!,lngMaker!)
                        self.destination_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                        self.destination_marker.title = self.responseVarAddress[indexPath.row].FullAddress
                        self.destination_marker.icon = UIImage(named: "destination_Marker")
                        self.destination_marker.map = self.viewMap
                        // self.CheckFavTrip()
                        
                        // self.markersArray.append(State(name: self.responseVarAddress[indexPath.row].FullAddress!, long: CLLocationDegrees(PlaceLatDouble!), lat: CLLocationDegrees(PlaceLongDouble!), placeId:" ", distanceVale: "", formattedAddress:  (self.responseVarAddress[indexPath.row].FullAddress)!, type: "", imageUrl: ""))
                        
                        
                        let camera = GMSCameraPosition.camera(withLatitude:latMaker!, longitude: lngMaker!, zoom: 14.0)
                        self.viewMap?.animate(to: camera)
                    }
                }
                else if self.AddressSearch.count != 0 {
                    let Place_ID = AddressSearch[indexPath.row].place_id
                    //  pPlaceID = "ChIJXbmAjccVrjsRlf31U1ZGpDM"
                    self.placesClient.lookUpPlaceID(Place_ID!, callback: { (place, error) -> Void in
                        
                        if let error = error {
                            print("lookup place id query error: \(error.localizedDescription)")
                            return
                        }
                        
                        if let place = place {
                            print("Place name \(place.name)")
                            print("Place address \(place.formattedAddress!)")
                            print("Place placeID \(place.placeID)")
                            // print("Place attributions \((place.attributions)!)")
                            print("\(place.coordinate.latitude)")
                            print("\(place.coordinate.longitude)")
                            self.destinationPlaceGoogle = place
                            DispatchQueue.main.async {
                                
                                //self.destinationPlace = (self.autoFillArray[indexPath.row] as! GMSPlace)
                                self.DestinationLat = (self.destinationPlaceGoogle?.coordinate.latitude)!
                                self.DestinationLng = (self.destinationPlaceGoogle?.coordinate.longitude)!
                                self.DestinationPlaceID = (self.destinationPlaceGoogle?.placeID)!
                                self.DestinationBool = true
                                self.DestinationTextBox.text = self.destinationPlaceGoogle?.name
                                self.DestinationTextBox.resignFirstResponder()
                                self.resultBackView1.isHidden = true
                                
                                
                                let latMaker = self.destinationPlaceGoogle?.coordinate.latitude
                                let lngMaker = self.destinationPlaceGoogle?.coordinate.longitude
                                print(latMaker!,lngMaker!)
                                self.destination_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                                self.destination_marker.title = self.destinationPlaceGoogle?.name
                                self.destination_marker.icon = UIImage(named: "destination_Marker")
                                self.destination_marker.map = self.viewMap
                                // self.CheckFavTrip()
                                
                                //   self.markersArray.append(State(name: self.destinationPlace!.name, long: CLLocationDegrees((self.destinationPlace?.coordinate.latitude)!), lat: CLLocationDegrees((self.destinationPlace?.coordinate.longitude)!), placeId:" ", distanceVale: "", formattedAddress:  (self.destinationPlace?.name)!, type: "", imageUrl: ""))
                                
                                
                                let camera = GMSCameraPosition.camera(withLatitude:latMaker!, longitude: lngMaker!, zoom: 14.0)
                                self.viewMap?.animate(to: camera)
                            }
                        }
                        
                    })
                }
            }
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if responseVarAddress.count != 0 {
            return responseVarAddress.count
        }
        else if AddressSearch.count != 0 {
            return AddressSearch.count
        }
        else{
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == autofillTableView {
            
            //dump(responseVarAddress)
            
            let cell = autofillTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MyTicketTableViewCell
            // tableView.separatorColor = UIColor.clear;
            //cell.separatorInset = .zero
            
            
            
            cell.Address.frame = CGRect.init(x: 5, y: 0, width: cell.contentView.frame.width - 5, height: 58)
            cell.Address.textColor = UIColor.black
            cell.Address.font = UIFont.setTarcRegular(size: 16.0)
            cell.Address.lineBreakMode = .byWordWrapping
            cell.Address.numberOfLines = 3
            cell.contentView.addSubview(cell.Address)
            
            
            
            //cell.LineCell.frame = CGRect.init(x: 0, y: 59, width: cell.frame.size.width, height: 1)
            //cell.LineCell.backgroundColor = .black
            // cell.contentView.addSubview(cell.LineCell)
            if responseVarAddress.count != 0 {
                if responseVarAddress.count > indexPath.row
                {
                    //               // place = autoFillArray[indexPath.row] as! GMSPlace
                    // let AddressString = responseVarAddress[indexPath.row].FullAddress!.components(separatedBy: .punctuationCharacters).joined().components(separatedBy: " ").filter{!$0.isEmpty}
                    
                    
                    cell.Address.text = "\(responseVarAddress[indexPath.row].FullAddress!)"
                    //
                    
                    print((responseVarAddress[indexPath.row].FullAddress)!)
                }
            }
            else if AddressSearch.count != 0{
                //  cell.LineCell.backgroundColor = .gray
                cell.Address.text = "\(AddressSearch[indexPath.row].description!)"
                //cell.LineCell.frame = CGRect.init(x: 0, y: 58, width: cell.frame.size.width, height: 2)
            }
            
            
            return cell
        }
        else
        {
            
            //dump(autoFillArray)
            
            let cell = autofillTableView1.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! MyTicketTableViewCell
            // tableView.separatorColor = UIColor.clear;
            // cell.separatorInset = .zero
            
            
            
            cell.Address.frame = CGRect.init(x: 5, y: 0, width: cell.contentView.frame.width - 5, height: 58)
            cell.Address.textColor = UIColor.black
            cell.Address.font = UIFont.setTarcRegular(size: 16.0)
            cell.Address.lineBreakMode = .byWordWrapping
            cell.Address.numberOfLines = 3
            cell.contentView.addSubview(cell.Address)
          
            if responseVarAddress.count != 0 {
                if responseVarAddress.count > indexPath.row
                {
                   
                    
                    cell.Address.text = "\(responseVarAddress[indexPath.row].FullAddress!)"
                    //
                    
                    print((responseVarAddress[indexPath.row].FullAddress)!)
                }
            }
            else if AddressSearch.count != 0{
                
                cell.Address.text = "\(AddressSearch[indexPath.row].description!)"
            }
            
            
            return cell
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // self.CheckFavTrip()
        DestinationTextBox.resignFirstResponder()
        SourceTextBox.resignFirstResponder()
        resultBackView.isHidden = true
        resultBackView1.isHidden = true
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //  self.CheckFavTrip()
        DestinationTextBox.resignFirstResponder()
        SourceTextBox.resignFirstResponder()
        resultBackView.isHidden = true
        resultBackView1.isHidden = true
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        responseVarAddress.removeAll()
        AddressSearch.removeAll()
        
        self.DataBaseAutocompleteFunc(SearchString: textField.text!)
        
        DispatchQueue.main.async {
            if textField.tag == 1001
            {
                
                //self.isSourceMarker = true
                
                
                self.resultBackView.frame = CGRect.init(x: self.SourceTextBox.frame.origin.x, y: self.SourceTextBox.frame.origin.y+self.SourceTextBox.frame.size.height+10, width: self.SourceTextBox.frame.size.width, height: self.view.frame.size.height-(self.SourceTextBox.frame.origin.y+70))
                self.resultBackView.isHidden = false
                self.resultBackView.backgroundColor = UIColor.clear
                
                
                self.autofillTableView.register(MyTicketTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
                
                // This view controller itself will provide the delegate methods and row data for the table view.
                self.autofillTableView.delegate = self
                self.autofillTableView.dataSource = self
                self.autofillTableView.backgroundColor = UIColor.white
                self.view.addSubview(self.resultBackView)
                self.resultBackView.addSubview(self.autofillTableView)
                
                // self.fetcher?.sourceTextHasChanged(textField.text!)
                // self.CheckFavTrip()
            }
            else if textField.tag == 1002
            {
                // self.isSourceMarker = false
                self.resultBackView1.frame = CGRect.init(x:  self.DestinationTextBox.frame.origin.x, y: self.DestinationTextBox.frame.origin.y+self.DestinationTextBox.frame.size.height+10, width: self.DestinationTextBox.frame.size.width, height: self.view.frame.size.height-(self.DestinationTextBox.frame.origin.y+70))
                self.resultBackView1.isHidden = false
                self.resultBackView1.backgroundColor = UIColor.clear
                self.autofillTableView1.register(MyTicketTableViewCell.self, forCellReuseIdentifier: self.cellReuseIdentifier)
                
                // This view controller itself will provide the delegate methods and row data for the table view.
                self.autofillTableView1.delegate = self
                self.autofillTableView1.dataSource = self
                self.autofillTableView1.backgroundColor = UIColor.white
                self.view.addSubview(self.resultBackView1)
                self.resultBackView1.addSubview(self.autofillTableView1)
                self.fetcher?.sourceTextHasChanged(textField.text!)
                //  self.CheckFavTrip()
            }
        }
        
        
        
    }
   
    func DataBaseAutocompleteFunc(SearchString:String){
        if SearchString != "" {
            self.autofillTableView.isHidden = false
            self.autofillTableView1.isHidden = false
            self.responseVarAddress.removeAll()
            self.AddressSearch.removeAll()
            let SearchStringSpace = SearchString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Map/GetAutoComplete?address=\((SearchStringSpace)!)", method: .get, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<DataBaseAutoComplete>) in
                    //print(response)
                    switch response.result {
                    case .success:
                        
                        if  response.result.value!.Message == "OK" {
                         if (response.result.value?.address!.count)! > 0 && (response.result.value?.address!.count)! != nil {
                            self.responseVarAddress = response.result.value!.address!
                            
                        }
                        else{
                            self.googleAutoComplete(SearchAddress: SearchString)
                        }
                        
                        }
                        else{
                             self.googleAutoComplete(SearchAddress: SearchString)
                        }
                        
                        
                        if(self.responseVarAddress.count > 5)
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: self.SourceTextBox.frame.size.width, height: 250)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: self.DestinationTextBox.frame.size.width, height: 250)
                            
                        }
                        else
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: Int(self.SourceTextBox.frame.size.width), height: self.responseVarAddress.count*50)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: Int(self.DestinationTextBox.frame.size.width), height: self.responseVarAddress.count*50)
                        }
                        self.autofillTableView.reloadData()
                        self.autofillTableView1.reloadData()
                        
                    case .failure(let error):
                        print(error)
                        
                        self.googleAutoComplete(SearchAddress: SearchString);
                        
                    }
            }
            
        }
        else{
            self.autofillTableView.removeFromSuperview()
            self.autofillTableView1.removeFromSuperview()
            self.autofillTableView.isHidden = true
            self.autofillTableView1.isHidden = true
        }
    }
    func googleAutoComplete(SearchAddress:String){
        let SearchAddressSpace = SearchAddress.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        responseVarAddress.removeAll()
        AddressSearch.removeAll()
        print("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(Key.GoogleAPIKey)&components=country:USA&input=\((SearchAddressSpace)!)")
        Alamofire.request("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(Key.GoogleAPIKey)&components=country:USA&input=\((SearchAddressSpace)!)", method: .get, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<GoogleAutoComplete>) in
                //print(response)
                switch response.result {
                    
                case .success:
                    
                    if response.result.value?.Status == "OK"{
                        
                        self.AddressSearch = response.result.value!.predictions!
                        if(self.AddressSearch.count > 5)
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: self.SourceTextBox.frame.size.width, height: 250)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: self.DestinationTextBox.frame.size.width, height: 250)
                            
                        }
                        else
                        {
                            self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: Int(self.SourceTextBox.frame.size.width), height: self.AddressSearch.count*50)
                            self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: Int(self.DestinationTextBox.frame.size.width), height: self.AddressSearch.count*50)
                        }
                        self.autofillTableView.reloadData()
                        self.autofillTableView1.reloadData()
                    }
                    
                case .failure(let error):
                    print(error)
                    
                    
                }
        }
        
    }
    
    
    @IBAction func TripplanerAction(_ sender: Any?) {
         //self.alertView.show()
        
        if UserDefaults.standard.bool(forKey: "IsUserLogin") == true
        {
            
        }
        else
        {
            checkLocationwithoutLogin()
            
        }
        
       
        
    }

 
  
    func updateBottomSheet(frame: CGRect) {
        AmenitiesContainer.frame = frame
        
        //        backView.frame = self.view.frame.offsetBy(dx: 0, dy: 15 + container.frame.minY - self.view.frame.height)
        //        backView.backgroundColor = UIColor.black.withAlphaComponent(1 - (frame.minY)/200)
    }
    
    func TripPlan(){
        if isReachable()
        {
        if SourceTextBox.text == DestinationTextBox.text
        {
            self.view.makeToast("Source and Destination address are same. Cannot process your search.")
            
        }
        else {
        if SourceBool && DestinationBool && self.sourcePlaceID != nil && self.DestinationPlaceID != nil{
            
             comFunc.uberDataApi(sourceLat: self.sourceLat!, sourceLng: self.sourceLng!, destinationLng: self.DestinationLng!, destinationLat: self.DestinationLat!)
            comFunc.GetLYFT(SourceLat: self.sourceLat!, SourceLong: self.sourceLng!, DestinationLat: self.DestinationLat!, DestinationLong: self.DestinationLng!)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WithoutLoginSuggestedRouteViewController") as! WithoutLoginSuggestedRouteViewController
            nextViewController.sourceAddresstest = self.SourceTextBox.text
            nextViewController.sourcePlaceId = self.sourcePlaceID
            nextViewController.sourcelat = self.sourceLat
            nextViewController.sourcelng = self.sourceLng
            
            nextViewController.DestinationAddresstest = self.DestinationTextBox.text
            nextViewController.DestinationPlaceId = self.DestinationPlaceID
            nextViewController.Destinationlat = self.DestinationLat
            nextViewController.Destinationlng = self.DestinationLng
            
              nextViewController.tripSchduleTime = self.tripSchduleTime
              nextViewController.BirdDatalistapicount = self.BirdDatalistapicount
           nextViewController.isCalenderEvent = self.isCalenderEvent
            
            
            
            var dateInSeconds : TimeInterval
            dateInSeconds = tripSchduleTime.timeIntervalSince1970
            print(dateInSeconds)
            let dateEpochSec = String(format: "%.0f", dateInSeconds)
            let GoogleDirection = "\(url.getGoogleDirection)origin=place_id:\(self.sourcePlaceID!)&destination=place_id:\(self.DestinationPlaceID!)&mode=transit&departure_time=\(dateEpochSec)&alternatives=true&key=\(Key.GoogleAPIKey)"
            print("Google Transit Array -> *************")
            print(GoogleDirection)
            Alamofire.request(GoogleDirection, method: .get, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<SuggestDirectionaClass>) in
                    //print(response)
                    switch response.result {
                    case .success:
                        
                        print("Success Google Direction For TRANSIT")
                        if response.result.value?.status != "INVALID_REQUEST" && (response.result.value?.routes?.count)! > 0
                        {
            
            nextViewController.GoogleTransitResponse = response
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
                        }
                        else{
                            self.view.makeToast("Sorry, transit directions not available from '\(self.SourceTextBox.text ?? "Source Address")' to '\(self.DestinationTextBox.text ?? "Destination Address")'")
                        }
                        
                    case .failure(let error):
                        print(error)
                    }
            }
        }
        else{
           // self.view.makeToast("Kindly Enter Source and Destination")
            print("Kindly re-enter Source and Destination")
        }
        
        
        
    }
        }
        else
        {
            self.view.makeToast("No Internet connection.  Please turn on WiFi or enable data.")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location.coordinate.latitude)")
        let lat:Double = Double(location.coordinate.latitude)
        let long:Double = Double(location.coordinate.longitude)
        
//        let lat:Double = 38.084685
//        let long:Double = -85.5987525
        let GetAddress = "\(url.getAddressFromLatLong)latlng=\(lat),\(long)&key=\(Key.GoogleAPIKey)"
        print(GetAddress)
        Alamofire.request(GetAddress, method: .get, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<MyLocationAddress>) in
                print(response)
                //to get status code
                switch response.result {
                case .success:
                if response.result.value != nil {
                print("Address API success -> ************************************* ")
                if response.result.value?.status == "OK" {
                   
                let CurrentAddressArray = response.result.value?.results![0]
                    
                    self.sourcePlaceID = (CurrentAddressArray?.placeId)!
                    self.sourceLat = Double((CurrentAddressArray?.geometry?.location?.lat)!)
                    self.sourceLng = Double((CurrentAddressArray?.geometry?.location?.lng)!)
                    self.SourceTextBox.text = (CurrentAddressArray?.formattedAddress)!
                     self.SourceTextBox.text = "\((CurrentAddressArray?.formattedAddress)!)"
                     self.sourceAddressText = "\((CurrentAddressArray?.formattedAddress)!)"
                    let addressComponents = CurrentAddressArray?.addressComponents
                    for addressComponentArray in addressComponents!{
                    
//                        if (addressComponentArray.types?.contains("postal_code"))! {
//                                                var postalode:Int = 0
//                                                if addressComponentArray != nil
//                                                {
//                                                if addressComponentArray.longName != nil &&  addressComponentArray.longName != "" {
//                                                    postalode = Int(addressComponentArray.longName!)!
//                                                }
//                                                else {
//                                                    postalode = Int(addressComponentArray.shortName!)!
//                                                }
//                                                }
//                                                if postalode < 40018 || postalode > 40299 {
//                                                    self.CorrectRegin = false
//                                                }
//                                                print(self.CorrectRegin)
//                                            }
//                                            else
                        
                        if (addressComponentArray.types?.contains("administrative_area_level_2"))! {
                            var county = ""
                            if addressComponentArray.longName != nil {
                                county = (addressComponentArray.longName!)
                            }
                            else {
                                county = (addressComponentArray.shortName!)
                            }
                            if county != "Jefferson County" {
                                self.CorrectRegin = false
                            }
                            print(self.CorrectRegin)
                        }
                        else if (addressComponentArray.types?.contains("administrative_area_level_1"))! {
                            var State = ""
                            if addressComponentArray.longName != nil {
                                State = (addressComponentArray.longName!)
                                if State != "Kentucky"{
                                    self.CorrectRegin = false
                                }
                            }
                            else {
                                State = (addressComponentArray.shortName!)
                                if State != "KY" {
                                    self.CorrectRegin = false
                                }
                            }
                           
                            print(self.CorrectRegin)
                        }
                    }
                    print(!self.CorrectRegin)
                    if !self.CorrectRegin {
                        self.sourcePlaceID = "ChIJEdVbsxoLaYgRMv1xICi009Q"
                        self.sourceLat = comVar.LouisvilleLat
                        self.sourceLng = comVar.LouisvilleLng
                      
                        self.view.makeToast("Address range set to Louisville KY")
                        self.sourceAddressText = "Louisville, KY, USA"
                        self.SourceTextBox.text = "Louisville, KY, USA"
                        
                        if self.currentlocationAnalytics {
                            self.currentlocationAnalytics = false
                            analytics.GetCurrentLocation(Address: "\((CurrentAddressArray?.formattedAddress)!)", Lat: Double((CurrentAddressArray?.geometry?.location?.lat)!), Lng: Double((CurrentAddressArray?.geometry?.location?.lng)!))
                        }
                    }
                    else{
                        if self.currentlocationAnalytics {
                            self.currentlocationAnalytics = false
                            analytics.GetCurrentLocation(Address: "\((CurrentAddressArray?.formattedAddress)!)", Lat: Double((CurrentAddressArray?.geometry?.location?.lat)!), Lng: Double((CurrentAddressArray?.geometry?.location?.lng)!))
                        }
                    }
                    self.source_marker.position = CLLocationCoordinate2D(latitude: self.sourceLat!, longitude: self.sourceLng!)
                    self.source_marker.title = self.sourceAddressText
                    self.source_marker.icon = UIImage(named: "source_Marker")
                    self.source_marker.map = self.viewMap
                    
                    
                    let camera = GMSCameraPosition.camera(withLatitude: self.sourceLat!, longitude: self.sourceLng!, zoom: 17.0)
                    self.locationManager.stopUpdatingLocation()
                    self.viewMap.isMyLocationEnabled = true
                    self.viewMap?.animate(to: camera)
                    self.amaType = "bus_station"
                    
                    self.getPlaces(GooglePlaces: self.amaType)
                    }
                }
                case .failure(let error):
                    print(error)
                    self.view.makeToast("Cannot find your current location. Try again!",duration: 3.0)
                    //self.view.makeToastActivity(.center)
                   
                }
        }
        
        
        
    
        

    }
    
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            viewMap.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
            //viewMap.isHidden = true
            
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    func getPlaces(GooglePlaces:String){
        
      //  tableView.defaultMininumDisplayHeight = 350
        let googlePlace = "\(url.GooglePlaces)location=\((self.sourceLat)!),\((self.sourceLng)!)&radius=804&type=\(GooglePlaces)&key=\(Key.GoogleAPIKey)"
       print(googlePlace)
        
        
        Alamofire.request(googlePlace, method: .get, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<RootClass>) in
                switch response.result {
                case .success:
                    if response.result.value?.status == "OK"
                    {
                        self.GooglePlaceResponse = (response.result.value?.results)!
                        //dump(response.result.value)
                        print("GooglePlace API Success ******************")
                        DispatchQueue.main.async {
                            for markerDet in self.markerDict
                            {
                                let removeMarker : GMSMarker = markerDet as! GMSMarker
                                removeMarker.map = nil;
                            }
                            self.markerDict.removeAllObjects()
                            self.markersArray.removeAll()
                            
                            
                            if let threeDayForecast = response.result.value?.results {
                                for forecast in threeDayForecast {
                                    // print(forecast.name ?? "user nil")
                                    //print(forecast.vicinity ?? "user nil")
                                    if let geometryLoc = forecast.geometry
                                    {
                                        if let latlong = geometryLoc.location
                                        {
                                            
                                            
                                            self.markersArray.append(State(name: forecast.name!, long: latlong.lat!, lat: latlong.lng!, placeId: forecast.placeId!, distanceVale: " ", formattedAddress: forecast.vicinity!, typePlace: GooglePlaces, imageUrl: ""))
                                        }
                                    }
                                    
                                    //print(forecast.placeId ?? "place id nil")
                                }
                                
                                
                                for state in self.markersArray {
                                    let state_marker = GMSMarker()
                                    let latMaker = state.lat
                                    let lngMaker = state.long
                                    //  print(latMaker,lngMaker)
                                    state_marker.position = CLLocationCoordinate2D(latitude: lngMaker, longitude: latMaker)
                                    state_marker.title = state.name
                                    state_marker.icon = UIImage(named: state.typePlace)
                                    state_marker.snippet = state.formattedAddress
                                    state_marker.map = self.viewMap
                                    self.markerDict.add(state_marker)
                                }
                               
                            }
                        }
                  //  self.tableView.reloadData()
                        
                        
                    }
                    case .failure(let error):
                    print(error)
                }
        }
    
        
    }
    @objc func OnBackClicked() {
        
        // self.navigationController?.popViewController(animated: true)
    }
    
    func loginButton(_ button: LoginButton, didLogoutWithSuccess success: Bool) {
        // success is true if logout succeeded, false otherwise
    }
    
    func loginButton(_ button: LoginButton, didCompleteLoginWithToken accessToken: AccessToken?, error: NSError?) {
        if let _ = accessToken {
            // AccessToken Saved
            UserDefaults.standard.set(accessToken?.tokenString, forKey: "uberKey")
            UserDefaults.standard.set(accessToken?.refreshToken, forKey: "uberRefershKey")
           
        }
    }
    
}



extension Double
{
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}
//extension WithOutLoginMainViewController: GMSAutocompleteViewControllerDelegate {
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        print(place)
//
//        print("Place name: \((place.name))")
//        print("Place address: \(place.formattedAddress ?? "null")")
//        self.activeTextField!.text = place.formattedAddress
//        print("Place attributions: \(String(describing: place.attributions))")
//        if self.activeTextField == SourceTextBox {
//            isCurrentLoc = false
//         self.sourceLat = place.coordinate.latitude
//            self.sourceLng = place.coordinate.longitude
//            self.sourcePlaceID = place.placeID
//            self.sourceAddressText = place.formattedAddress
//
//            //self.source_marker.position = CLLocationCoordinate2DMake(41.499320 ,-81.694361)
//
//            source_marker.map = nil
//            //source_marker = nil
//            let changedPosition = CLLocationCoordinate2DMake(self.sourceLat! ,self.sourceLng!)
//            source_marker = GMSMarker(position: changedPosition)
//            self.source_marker.title = self.sourceAddressText
//        self.source_marker.icon = UIImage(named: "source_Marker")
//            self.source_marker.map = self.viewMap
//
//
//
//
//        let camera = GMSCameraPosition.camera(withLatitude: self.sourceLat!, longitude: self.sourceLng!, zoom: 14.0)
//      //  self.viewMap.isMyLocationEnabled = true
//        self.viewMap?.animate(to: camera)
//        self.getPlaces(GooglePlaces: "bus_station")
//        self.SourceBool = true
//        }
//        else if self.activeTextField == DestinationTextBox {
//
//            self.DestinationLat = place.coordinate.latitude
//            self.DestinationLng = place.coordinate.longitude
//            self.DestinationPlaceID = place.placeID
//            self.DestinationText = place.formattedAddress
//
//            //self.source_marker.position = CLLocationCoordinate2DMake(41.499320 ,-81.694361)
//
//           destination_marker.map = nil
//            //source_marker = nil
//            let changedPosition = CLLocationCoordinate2DMake(self.DestinationLat! ,self.DestinationLng!)
//            destination_marker = GMSMarker(position: changedPosition)
//            self.destination_marker.title = self.DestinationText
//            self.destination_marker.icon = UIImage(named: "destination_Marker")
//            self.destination_marker.map = self.viewMap
//
//
//
//
//            let camera = GMSCameraPosition.camera(withLatitude: self.DestinationLat!, longitude: self.DestinationLng!, zoom: 11.0)
//            //  self.viewMap.isMyLocationEnabled = true
//            self.viewMap?.animate(to: camera)
//            self.DestinationBool = true
//        }
//        TripPlan()
//        self.dismiss(animated: true, completion: nil)
//    }
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        // TODO: handle the error.
//        //        print("Error: \(error.description)")
//        self.dismiss(animated: true, completion: nil)
//    }
//
//    // User canceled the operation.
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        print("Autocomplete was cancelled.")
//        self.dismiss(animated: true, completion: nil)
//    }
//}
