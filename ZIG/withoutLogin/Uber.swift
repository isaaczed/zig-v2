//
//  Uber.swift
//  ZIG
//
//  Created by Isaac on 14/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//



import Foundation
import ObjectMapper
import Alamofire
class LYFTDATA: Mappable {
    var getLYFTDataList:[getLYFTData]?
    var error:String?
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        error <- map["error"]
        getLYFTDataList <- map["cost_estimates"]
    }
    
    
}
class getLYFTData: Mappable {
    var estimated_duration_seconds:Int?
    var estimated_distance_miles:Double?
    var estimated_cost_cents_min:Int?
    var estimated_cost_cents_max:Int?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
         estimated_duration_seconds <- map["estimated_duration_seconds"]
         estimated_distance_miles <- map["estimated_distance_miles"]
         estimated_cost_cents_min <- map["estimated_cost_cents_min"]
         estimated_cost_cents_max <- map["estimated_cost_cents_max"]
    }
    
    
}
class getUBERData: Mappable {
    var prices:[UberPrice]?
    var code:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        prices <- map["prices"]
        code <- map["code"]
    }
    
    
}

class UberPrice: Mappable {
    var localized_display_name:String?
    var distance:Double?
    var display_name:String?
    var product_id:String?
    var high_estimate:Int?
    var minimum : Int?
    var low_estimate:Int?
    var duration:Int?
    var estimate:String?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        localized_display_name <- map["localized_display_name"]
        distance <- map["distance"]
        display_name <- map["display_name"]
        product_id <- map["product_id"]
        high_estimate <- map["high_estimate"]
        minimum <- map["minimum"]
        low_estimate <- map["low_estimate"]
        duration <- map["duration"]
        estimate <- map["estimate"]
    }
    
    
}

class comFunc {
    
   static func getLyftAPIFirst(SourceLat:Double,SourceLong:Double, DestinationLat:Double,DestinationLong:Double, completion: @escaping (Int,Int,Int,Double,Bool) -> ()) {
        
        print("https://api.lyft.com/v1/cost?start_lat= \(SourceLat)&start_lng= \(SourceLong)&end_lat=\(DestinationLat)&end_lng=\(DestinationLong)")
        
        Alamofire.request("https://api.lyft.com/v1/cost?start_lat=\(SourceLat)&start_lng=\(SourceLong)&end_lat=\(DestinationLat)&end_lng=\(DestinationLong)").responseObject { (response: DataResponse<LYFTDATA>) in
            switch response.result {
            case .success:
                print("Success")
                if response.result.value != nil && response.result.value?.error != "no_service_in_area"{
                    
                    
                    //                    print(response.result.value!.)
                    //                    print(response.result.value!)
                    //                    print(response.result.value!)
                    //                    print(response.result.value!)
                    
                    if response.result.value?.getLYFTDataList![0] != nil {
                        if (response.result.value?.getLYFTDataList!.count)! > 0 {
                            
                            
                            let travelPrice =  Int((response.result.value?.getLYFTDataList![0].estimated_cost_cents_min)! / 100)
                             let travelMaxPrice =  Int((response.result.value?.getLYFTDataList![0].estimated_cost_cents_max)! / 100)
                            let travelMin = response.result.value?.getLYFTDataList![0].estimated_duration_seconds
                            let travelMile = (response.result.value?.getLYFTDataList![0].estimated_distance_miles)! * 1609.344
                            completion(travelPrice,travelMaxPrice,travelMin!,travelMile,true)
                            
                            
                            //                            //comVarUber.Lyft_estimationPrice =
                            //                            //comVarUber.Lyft_Pickup_min = 5
                            //                            comVarUber.Lyft_Travel_Min =
                            //                            comVarUber.LyftDistnce =
                            
                        }
                        else{
                            completion(6,7,300,1000.0,false)
                        }
                    }
                }
                
                
            case .failure(let error):
                print(error)
            }
            
        }
        
    }
    
    static func uberDataApi(sourceLat:Double,sourceLng:Double,destinationLng:Double,destinationLat:Double){
        comVarUber.uber_estimationPrice = 6
        comVarUber.uber_Pickup_min = 5
        comVarUber.uber_Travel_Min = 300
        comVarUber.UberDistnce = 1000.0
        comVarUber.UberEstimatepriceMax = 6
        print("https://api.uber.com/v1/estimates/price?start_latitude=\(sourceLat)&start_longitude=\(sourceLng)&end_latitude=\(destinationLat)&end_longitude=\(destinationLng)&server_token=lON_05bUnjMyAeT-ifA-d41IU7b98YOORKFEqpba")
        Alamofire.request("https://api.uber.com/v1/estimates/price?start_latitude=\(sourceLat)&start_longitude=\(sourceLng)&end_latitude=\(destinationLat)&end_longitude=\(destinationLng)&server_token=lON_05bUnjMyAeT-ifA-d41IU7b98YOORKFEqpba").responseObject { (response: DataResponse<getUBERData>) in
            switch response.result {
            case .success:
                print("Uber Success")
                
                if response.result.value != nil {
                    
                    if response.result.value!.prices != nil  && response.result.value!.code == nil {
                    comVarUber.Uberdata = response.result.value!.prices!
                    comVarUber.uber_estimationPrice = comVarUber.Uberdata[0].low_estimate
                    comVarUber.uber_Pickup_min = comVarUber.Uberdata[0].minimum
                    comVarUber.uber_Product_ID = comVarUber.Uberdata[0].product_id
                    comVarUber.uber_Travel_Min = comVarUber.Uberdata[0].duration
                    comVarUber.UberDistnce = comVarUber.Uberdata[0].distance! * 1609.344
                    comVarUber.UberEstimatepriceMax = comVarUber.Uberdata[0].high_estimate

                    
                    
                }
                else{
                                        comVarUber.UberDistnce = 1000.0
                    
                    comVarUber.uber_estimationPrice = 5
                    comVarUber.uber_Pickup_min = 3
                    
                    comVarUber.uber_Travel_Min = 300
                      comVarUber.UberEstimatepriceMax = 6
                }
                
                
                }
                else{
                    comVarUber.UberDistnce = 1000.0
                    
                    comVarUber.uber_estimationPrice = 5
                    comVarUber.uber_Pickup_min = 3
                    
                    comVarUber.uber_Travel_Min = 300
                }
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    static func GetLYFT(SourceLat:Double,SourceLong:Double, DestinationLat:Double,DestinationLong:Double){
        print("https://api.lyft.com/v1/cost?start_lat= \(SourceLat)&start_lng= \(SourceLong)&end_lat=\(DestinationLat)&end_lng=\(DestinationLong)")
        
        Alamofire.request("https://api.lyft.com/v1/cost?start_lat=\(SourceLat)&start_lng=\(SourceLong)&end_lat=\(DestinationLat)&end_lng=\(DestinationLong)").responseObject { (response: DataResponse<LYFTDATA>) in
            switch response.result {
            case .success:
                print("Success")
                if response.result.value != nil {
                    
                    if let countofResponse = response.result.value?.getLYFTDataList?.count{
                    
                        if countofResponse > 0 {
                    if response.result.value?.getLYFTDataList![0] != nil {
                        if (response.result.value?.getLYFTDataList!.count)! > 0 {
                            comVarUber.Lyft_estimationPrice = (response.result.value?.getLYFTDataList![0].estimated_cost_cents_min)!
                            comVarUber.Lyft_Pickup_min = 5
                            comVarUber.Lyft_Travel_Min = (response.result.value?.getLYFTDataList![0].estimated_duration_seconds)!
                            comVarUber.LyftDistnce = (response.result.value?.getLYFTDataList![0].estimated_distance_miles)! 
                            comVarUber.Lyft_estimationPriceHigh = (response.result.value?.getLYFTDataList![0].estimated_cost_cents_max)!

                    }
                }
                }
                    }
                }


            case .failure(let error):
                print(error)
            }
        }


   }
}

struct comVarUber {
    
    static var uber_estimationPrice:Int?
    static var uber_Pickup_min:Int?
    static var uber_Travel_Min:Int?
    static var uber_Product_ID:String?
    static var Uberdata = [UberPrice]()
    static var UberDistnce:Double?
    static var ActivityID:Int?
    static var UberEstimatepriceMax:Int?
     static var Lyft_estimationPriceHigh = 6
    static var Lyft_estimationPrice = 5
    static var Lyft_Pickup_min = 5
    static var Lyft_Travel_Min = 300
    static var LyftDistnce = 1000.0
    
    
    
    
}
struct AnalyticsVar{
        static var ActivityID:Int?
}
