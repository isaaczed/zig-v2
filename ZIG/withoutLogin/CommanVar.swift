//
//  CommanVar.swift
//  Tarc
//
//  Created by Isaac on 22/03/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import UIKit

struct Key {
    static var GoogleAPIKey = "AIzaSyAtyYITZHllxjw_8L1M-pnqq9-dYECukl0"
    
}
struct SOSandSTOP {
    static var SOSEnable = false
    static var StopRequest = false
}
struct APIUrl {
   
    static let ZIGTARCAPI = mainUrL()
    static let BuyTicket = "\(ZIGTARCAPI)Tickets/Add"
    static let TicketListURL = "\(ZIGTARCAPI)Tickets/GetTickets"
    static let MacIDURL = "\(ZIGTARCAPI)UserBeacon?DropDownCode=3"
    static var TicketActivateURL = "\(ZIGTARCAPI)Tickets/Activate"
    static var ExpiryDate = "\(ZIGTARCAPI)Preferences/GetAppexpiry?pin=ZIG19"
    static var TicketstateChangeURL = "\(ZIGTARCAPI)Beacon/Add"
    static let paymentPostURL = "\(ZIGTARCAPI)Payment/Addreference"
    static let sendFailureMail = "\(ZIGTARCAPI)Payment/Sendemailapp"
    static let sendEmailFailureMail = "https://www.ridetarc.org/DMI/API/Config.php"
    static let ZIGTARCBASEAPI = basemainUrL()

   // static let getUserId = "https://tripplan.ridetarc.org/ZIGSTarc/api/User/api/GetUserId?Token="

}
func mainUrL() -> (String)
{
    var URLString = String()
    let targetName = Bundle.main.infoDictionary?["CFBundleName"] as! String
    if targetName == "ZIG Smart"
    {
      URLString = "https://closerlookdigitalsoftware.com/ZIGSTARCTest/api/"
        return URLString
    }
    else
    {
        URLString = "https://closerlookdigitalsoftware.com/ZIGSTARCTest/api/"
        return URLString

    }

}
func basemainUrL() -> (String)
{
    var URLString = String()
    let targetName = Bundle.main.infoDictionary?["CFBundleName"] as! String
    if targetName == "ZIG Smart"
    {
      URLString = "https://closerlookdigitalsoftware.com/"
        return URLString
    }
    else
    {
        URLString = "https://tripplan.ridetarc.org/"
        return URLString

    }

}

struct beaconData {

    static let BIBO_IN = "BIBO 1.1 A"
    static let BIBO_OUT = "BIBO 1.1 O"
    
    static let kBLEService_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e"
    static let kBLE_Characteristic_uuid_Tx = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
    static let kBLE_Characteristic_uuid_Rx = "6e400003-b5a3-f393-e0a9-e50e24dcca9e"
    static let MaxCharacters = 20
    static var CurrentBeaconID = ""
    
   
    
}
struct comVar {
    
    static var LouisvilleLat:Double = 38.2526647
    static var LouisvilleLng:Double = -85.75845570000001
    static var DateImplementation:Bool = true
     static var ticketId = 0
    static var timer = 0
    static var status = 0
    static var ExpiryDate = ""
   static var internetchecker = false
    static var firstTimeAppOpen = true

}

func addLeftImageTo(txtField:UITextField, andImage img:UIImage)
{
    
    let leftimageuiview = UIImageView(frame: CGRect(x: 5.0, y: 0.0, width: 30 , height: 30))
    let leftimageview = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 35, height: 30))
    leftimageview.addSubview(leftimageuiview)
    leftimageuiview.image = img
    txtField.leftView = leftimageview
    txtField.leftViewMode = .always
}

