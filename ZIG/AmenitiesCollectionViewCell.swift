//
//  AmenitiesCollectionViewCell.swift
//  ZIG
//
//  Created by Isaac on 01/06/18.
//  Copyright © 2018 Isaac. All rights reserved.
//

import UIKit

class AmenitiesCollectionViewCell: UICollectionViewCell {
    let amenitiesImage = UIImageView()
    let amenitiesText = UILabel()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        amenitiesImage.image = nil
        amenitiesText.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    
}

class ModeTypeCollectionViewCell: UICollectionViewCell {
    let TimeImage = UIImageView()
    let ModeTypeText = UILabel()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        TimeImage.image = nil
        ModeTypeText.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
}
