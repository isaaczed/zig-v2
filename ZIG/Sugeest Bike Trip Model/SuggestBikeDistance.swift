//
//	SuggestBikeDistance.swift
//
//	Create by Isaac on 15/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestBikeDistance : NSObject, NSCoding, Mappable{

	var text : String?
	var value : Int?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestBikeDistance()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		text <- map["text"]
		value <- map["value"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         text = aDecoder.decodeObject(forKey: "text") as? String
         value = aDecoder.decodeObject(forKey: "value") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if text != nil{
			aCoder.encode(text, forKey: "text")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}
