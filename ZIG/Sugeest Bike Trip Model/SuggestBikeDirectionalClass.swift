//
//	SuggestBikeDirectionalClass.swift
//
//	Create by Isaac on 15/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestBikeDirectionalClass : NSObject, NSCoding, Mappable{

	var errorMessage : AnyObject?
	var routes : [SuggestBikeRoute]?
	var status : String?
    var geocoded_waypoints:[geocoded_waypointsBike]?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestBikeDirectionalClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		errorMessage <- map["error_message"]
		routes <- map["routes"]
		status <- map["status"]
        geocoded_waypoints <- map["geocoded_waypoints"]
        
		
	}
    

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         errorMessage = aDecoder.decodeObject(forKey: "error_message") as? AnyObject
         routes = aDecoder.decodeObject(forKey: "routes") as? [SuggestBikeRoute]
         status = aDecoder.decodeObject(forKey: "status") as? String
        geocoded_waypoints = aDecoder.decodeObject(forKey: "geocoded_waypoints") as? [geocoded_waypointsBike]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "error_message")
		}
		if routes != nil{
			aCoder.encode(routes, forKey: "routes")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if geocoded_waypoints != nil{
            aCoder.encode(geocoded_waypoints, forKey: "geocoded_waypoints")
        }

	}

}

class geocoded_waypointsBike: Mappable {
    
    var geocoder_status:String?
    var place_id:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        geocoder_status <- map["geocoder_status"]
        place_id <- map["place_id"]
        
    }
    
    
    
}
