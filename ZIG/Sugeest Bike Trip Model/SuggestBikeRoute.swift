//
//	SuggestBikeRoute.swift
//
//	Create by Isaac on 15/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestBikeRoute : NSObject, NSCoding, Mappable{

	var bounds : SuggestBikeBound?
	var copyrights : String?
	var legs : [SuggestBikeLeg]?
	var overviewPolyline : SuggestBikePolyline?
	var summary : String?
	var warnings : [String]?
	var waypointOrder : [AnyObject]?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestBikeRoute()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		bounds <- map["bounds"]
		copyrights <- map["copyrights"]
		legs <- map["legs"]
		overviewPolyline <- map["overview_polyline"]
		summary <- map["summary"]
		warnings <- map["warnings"]
		waypointOrder <- map["waypoint_order"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bounds = aDecoder.decodeObject(forKey: "bounds") as? SuggestBikeBound
         copyrights = aDecoder.decodeObject(forKey: "copyrights") as? String
         legs = aDecoder.decodeObject(forKey: "legs") as? [SuggestBikeLeg]
         overviewPolyline = aDecoder.decodeObject(forKey: "overview_polyline") as? SuggestBikePolyline
         summary = aDecoder.decodeObject(forKey: "summary") as? String
         warnings = aDecoder.decodeObject(forKey: "warnings") as? [String]
         waypointOrder = aDecoder.decodeObject(forKey: "waypoint_order") as? [AnyObject]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if bounds != nil{
			aCoder.encode(bounds, forKey: "bounds")
		}
		if copyrights != nil{
			aCoder.encode(copyrights, forKey: "copyrights")
		}
		if legs != nil{
			aCoder.encode(legs, forKey: "legs")
		}
		if overviewPolyline != nil{
			aCoder.encode(overviewPolyline, forKey: "overview_polyline")
		}
		if summary != nil{
			aCoder.encode(summary, forKey: "summary")
		}
		if warnings != nil{
			aCoder.encode(warnings, forKey: "warnings")
		}
		if waypointOrder != nil{
			aCoder.encode(waypointOrder, forKey: "waypoint_order")
		}

	}

}
