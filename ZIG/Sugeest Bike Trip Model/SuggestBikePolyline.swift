//
//	SuggestBikePolyline.swift
//
//	Create by Isaac on 15/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestBikePolyline : NSObject, NSCoding, Mappable{

	var points : String?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestBikePolyline()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		points <- map["points"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         points = aDecoder.decodeObject(forKey: "points") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if points != nil{
			aCoder.encode(points, forKey: "points")
		}

	}

}
