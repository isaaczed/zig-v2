//
//	SuggestBikeBound.swift
//
//	Create by Isaac on 15/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestBikeBound : NSObject, NSCoding, Mappable{

	var northeast : SuggestBikeNortheast?
	var southwest : SuggestBikeNortheast?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestBikeBound()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		northeast <- map["northeast"]
		southwest <- map["southwest"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         northeast = aDecoder.decodeObject(forKey: "northeast") as? SuggestBikeNortheast
         southwest = aDecoder.decodeObject(forKey: "southwest") as? SuggestBikeNortheast

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if northeast != nil{
			aCoder.encode(northeast, forKey: "northeast")
		}
		if southwest != nil{
			aCoder.encode(southwest, forKey: "southwest")
		}

	}

}
