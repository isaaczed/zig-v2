//
//	SuggestBikeLeg.swift
//
//	Create by Isaac on 15/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestBikeLeg : NSObject, NSCoding, Mappable{

	var arrivalTime : AnyObject?
	var departureTime : AnyObject?
	var distance : SuggestBikeDistance?
	var duration : SuggestBikeDuration?
	var durationInTraffic : AnyObject?
	var endAddress : String?
	var endLocation : SuggestBikeNortheast?
	var startAddress : String?
	var startLocation : SuggestBikeNortheast?
	var steps : [SuggestBikeStep]?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestBikeLeg()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		arrivalTime <- map["arrival_time"]
		departureTime <- map["departure_time"]
		distance <- map["distance"]
		duration <- map["duration"]
		durationInTraffic <- map["duration_in_traffic"]
		endAddress <- map["end_address"]
		endLocation <- map["end_location"]
		startAddress <- map["start_address"]
		startLocation <- map["start_location"]
		steps <- map["steps"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         arrivalTime = aDecoder.decodeObject(forKey: "arrival_time") as? AnyObject
         departureTime = aDecoder.decodeObject(forKey: "departure_time") as? AnyObject
         distance = aDecoder.decodeObject(forKey: "distance") as? SuggestBikeDistance
         duration = aDecoder.decodeObject(forKey: "duration") as? SuggestBikeDuration
         durationInTraffic = aDecoder.decodeObject(forKey: "duration_in_traffic") as? AnyObject
         endAddress = aDecoder.decodeObject(forKey: "end_address") as? String
         endLocation = aDecoder.decodeObject(forKey: "end_location") as? SuggestBikeNortheast
         startAddress = aDecoder.decodeObject(forKey: "start_address") as? String
         startLocation = aDecoder.decodeObject(forKey: "start_location") as? SuggestBikeNortheast
         steps = aDecoder.decodeObject(forKey: "steps") as? [SuggestBikeStep]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if arrivalTime != nil{
			aCoder.encode(arrivalTime, forKey: "arrival_time")
		}
		if departureTime != nil{
			aCoder.encode(departureTime, forKey: "departure_time")
		}
		if distance != nil{
			aCoder.encode(distance, forKey: "distance")
		}
		if duration != nil{
			aCoder.encode(duration, forKey: "duration")
		}
		if durationInTraffic != nil{
			aCoder.encode(durationInTraffic, forKey: "duration_in_traffic")
		}
		if endAddress != nil{
			aCoder.encode(endAddress, forKey: "end_address")
		}
		if endLocation != nil{
			aCoder.encode(endLocation, forKey: "end_location")
		}
		if startAddress != nil{
			aCoder.encode(startAddress, forKey: "start_address")
		}
		if startLocation != nil{
			aCoder.encode(startLocation, forKey: "start_location")
		}
		if steps != nil{
			aCoder.encode(steps, forKey: "steps")
		}

	}

}
