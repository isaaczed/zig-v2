//
//	SuggestBikeStep.swift
//
//	Create by Isaac on 15/12/2017
//	Copyright © 2017. All rights reserved.


import Foundation 
import ObjectMapper


class SuggestBikeStep : NSObject, NSCoding, Mappable{

	var distance : SuggestBikeDistance?
	var duration : SuggestBikeDuration?
	var endLocation : SuggestBikeNortheast?
	var htmlInstructions : String?
	var polyline : SuggestBikePolyline?
	var startLocation : SuggestBikeNortheast?
	var steps : AnyObject?
	var transitDetails : AnyObject?
	var travelMode : String?


	class func newInstance(map: Map) -> Mappable?{
		return SuggestBikeStep()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		distance <- map["distance"]
		duration <- map["duration"]
		endLocation <- map["end_location"]
		htmlInstructions <- map["html_instructions"]
		polyline <- map["polyline"]
		startLocation <- map["start_location"]
		steps <- map["steps"]
		transitDetails <- map["transit_details"]
		travelMode <- map["travel_mode"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         distance = aDecoder.decodeObject(forKey: "distance") as? SuggestBikeDistance
         duration = aDecoder.decodeObject(forKey: "duration") as? SuggestBikeDuration
         endLocation = aDecoder.decodeObject(forKey: "end_location") as? SuggestBikeNortheast
         htmlInstructions = aDecoder.decodeObject(forKey: "html_instructions") as? String
         polyline = aDecoder.decodeObject(forKey: "polyline") as? SuggestBikePolyline
         startLocation = aDecoder.decodeObject(forKey: "start_location") as? SuggestBikeNortheast
         steps = aDecoder.decodeObject(forKey: "steps") as? AnyObject
         transitDetails = aDecoder.decodeObject(forKey: "transit_details") as? AnyObject
         travelMode = aDecoder.decodeObject(forKey: "travel_mode") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if distance != nil{
			aCoder.encode(distance, forKey: "distance")
		}
		if duration != nil{
			aCoder.encode(duration, forKey: "duration")
		}
		if endLocation != nil{
			aCoder.encode(endLocation, forKey: "end_location")
		}
		if htmlInstructions != nil{
			aCoder.encode(htmlInstructions, forKey: "html_instructions")
		}
		if polyline != nil{
			aCoder.encode(polyline, forKey: "polyline")
		}
		if startLocation != nil{
			aCoder.encode(startLocation, forKey: "start_location")
		}
		if steps != nil{
			aCoder.encode(steps, forKey: "steps")
		}
		if transitDetails != nil{
			aCoder.encode(transitDetails, forKey: "transit_details")
		}
		if travelMode != nil{
			aCoder.encode(travelMode, forKey: "travel_mode")
		}

	}

}
