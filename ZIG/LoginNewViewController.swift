//
//  LoginNewViewController.swift
//  ZIG
//
//  Created by Isaac on 12/04/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import NYAlertViewController
import IQKeyboardManagerSwift
import RealmSwift
import Crashlytics
import SwiftyJSON

class LoginNewViewController: UIViewController,UITextFieldDelegate {
   // @IBOutlet weak var SignUpAct: UILabel!
    
    @IBOutlet weak var SignUpAction: UIButton!
    var tripSchduleTime = Date()
    var sourceAddresstest:String?
    var sourcePlaceId:String?
    var sourcelat:Double?
    var sourcelng:Double?
    var loaderView = UIView()
    var DestinationAddresstest:String?
    var DestinationPlaceId:String?
    var Destinationlat:Double?
    var Destinationlng:Double?
    var iscalendarEvent:Bool?
    var BirdDatalistapicount:Int?
    var BirdBikeLat:Double?
    var BirdBikeLon:Double?
    var passwordVisibleBool:Bool?
    var passbutton = UIButton()
    @IBOutlet weak var LoginBtn: UIButton!
    @IBOutlet weak var PasswordSeprator: UIView!
    @IBOutlet weak var UsernameSeprator: UIView!
    @IBOutlet weak var PasswordTxt: UITextField!
    @IBOutlet weak var PasswordLbl: UILabel!
    @IBOutlet weak var UsernameTxt: UITextField!
    @IBOutlet weak var UserNameLbl: UILabel!
    @IBOutlet weak var passwordImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        analytics.GetPageHitCount(PageName: "Login")
        UsernameTxt.delegate = self
        PasswordTxt.delegate = self
        LoginBtn.addTarget(self, action:#selector(loginBtnAction), for: UIControlEvents.touchUpInside)
          SignUpAction.addTarget(self, action:#selector(signupAction), for: UIControlEvents.touchUpInside)
    self.property()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginNewViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        PasswordTxt.isUserInteractionEnabled = true
        passwordVisibleBool = true
        PasswordTxt.clearButtonMode = UITextFieldViewMode.never;
        let passwordtap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginNewViewController.passwordClick))
        passwordImage.addGestureRecognizer(passwordtap)
PasswordTxt.bringSubview(toFront: passwordImage)
        passwordImage.isHidden = true
         passbutton = UIButton(type: .custom)
        passbutton.setImage(UIImage(named: "Image-6"), for: .normal)
        passbutton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        passbutton.frame = CGRect(x: CGFloat(PasswordTxt.frame.size.width - 25), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        passbutton.addTarget(self, action: #selector(passwordClick), for: .touchUpInside)
        PasswordTxt.rightView = passbutton
        PasswordTxt.rightViewMode = .always
        passbutton.isAccessibilityElement = true
        passbutton.accessibilityLabel = "Password Secured or visble"
        // Do any additional setup after loading the view.
    }
    @objc func passwordClick()
    {
        if passwordVisibleBool == true{
            passwordVisibleBool = false
            PasswordTxt.isSecureTextEntry = false
            passbutton.setImage(UIImage(named: "Image-5"), for: .normal)
        }
        else
        {
            passwordVisibleBool = true
            PasswordTxt.isSecureTextEntry = true
            passbutton.setImage(UIImage(named: "Image-6"), for: .normal)

        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            if textField.returnKeyType == .go{
                loginBtnAction()
            }
            textField.resignFirstResponder()
            return true;
        }
        return false
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.UsernameTxt
        {
            
            self.UsernameTxt.font = UIFont.setTarcRegular(size: 20.0)
            self.UserNameLbl.isHidden = false
            self.UserNameLbl.text = self.UsernameTxt.placeholder
            self.UsernameTxt.placeholder = ""
        UsernameSeprator.backgroundColor = UIColor.black.withAlphaComponent(1)
            
            
        }
            
        else if textField == self.PasswordTxt
        {
            self.PasswordTxt.font = UIFont.setTarcRegular(size: 20.0)
            self.PasswordLbl.isHidden = false
            self.PasswordLbl.text = self.PasswordTxt.placeholder
            self.PasswordTxt.placeholder = ""
            PasswordSeprator.backgroundColor = UIColor.black.withAlphaComponent(1)
            
            
        }
            
      
        
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        //Checkes if textfield is empty or not after after user ends editing.
        if textField == self.UsernameTxt
        {
            if self.UsernameTxt.text == ""
            {
                self.UsernameTxt.font = UIFont.setTarcBold(size: 15.0)
                self.UserNameLbl.isHidden = true
                self.UsernameTxt.placeholder = "Username/Email"
               UsernameSeprator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                
                
                
                
            }
            
            
        }
            
            
        else if textField == self.PasswordTxt
        {
            if self.PasswordTxt.text == ""
            {
                self.PasswordTxt.font = UIFont.setTarcBold(size: 15.0)
                self.PasswordLbl.isHidden = true
                self.PasswordTxt.placeholder = "Password"
                PasswordSeprator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
                
                
                
            }
            
            
        }
            
 
        
    }

    func property(){
        
        UserNameLbl.isHidden =  true
        PasswordLbl.isHidden = true
        
        UsernameTxt.text = ""
        PasswordTxt.text = ""
        
        
        
        
        self.UsernameTxt.font = UIFont.setTarcBold(size: 15.0)
        self.PasswordTxt.font = UIFont.setTarcBold(size: 15.0)
        self.PasswordTxt.returnKeyType = .go
        self.UsernameTxt.returnKeyType = .next
       
        PasswordTxt.isSecureTextEntry = true
        UsernameTxt.autocorrectionType = .no

        
//        NameView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//        EmailView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
//        PasswordView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func loginBtnAction() {
        if isReachable()
        {
        self.showLoader()
        
        let username = UsernameTxt.text!
        let password = PasswordTxt.text!
        
        if !username.isEmpty{
            if !password.isEmpty{
                let url = "\(APIUrl.ZIGTARCAPI)User/login"
                let parameters: Parameters = [
                    "username": username.trimmingCharacters(in: .whitespacesAndNewlines),
                    "password": password.trimmingCharacters(in: .whitespacesAndNewlines)
                ]
                
                
                Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default)
                    .responseObject{ (response: DataResponse<Classlogin>) in
                        switch response.result {
                        case .success:
                            dump(response)
                            print(response)
                            //to get status code
                            if response.result.value?.message == "Ok"
                            {
                                print(response.request?.url! ?? "Nil")
                                
                                if response.result.value?.UserType == 0
                                {
                                    let loginValue = response.result.value
                                    print(loginValue ?? "nil")
                                    print(loginValue?.token!)
                                    let userDefaults = UserDefaults.standard
                                     if response.result.value?.UserId != nil
                                     {
                                        let userid = response.result.value?.UserId!
                                        let userString = userid
                                        userDefaults.set(userString, forKey: "userIdString")
                                        userDefaults.set(response.result.value?.username, forKey: "userName")
                                        Crashlytics.sharedInstance().setUserIdentifier("\(userString)")
                                        
                                     }
                                     else
                                     {
                                        userDefaults.set("", forKey: "userId")
                                        userDefaults.set("", forKey: "userId")
                                        Crashlytics.sharedInstance().setUserIdentifier("withoutLoginPage")

                                    }
                                    LoginNewViewController.getSession()
                                    userDefaults.set(true, forKey: "IsUserLogin")
                                    userDefaults.set(loginValue?.token!, forKey: "accessToken")
                                    userDefaults.set("true", forKey: "showPreferencesOnLogin")
                                    userDefaults.synchronize()
                                   // analytics.getSession()
                                    
                                    DispatchQueue.main.async {
                                        
                                        self.perferenceMethod()
                                        
                                        self.GetallBeaconData()
                                        

                                        
                                        
                                    }
                                    
                                    //  self.present(nextViewController, animated:true, completion:nil)
                                    print("login btn presses");
                                }
                                else
                                {
                                    self.hideLoader()
                                    let alertViewController = NYAlertViewController()
                                    // Set a title and message
                                    alertViewController.title = "Login"
                                    alertViewController.message = "This account cant be logged in here."
                                    
                                    // Customize appearance as desired
                                    alertViewController.buttonCornerRadius = 20.0
                                    alertViewController.view.tintColor = self.view.tintColor
                                    alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                    alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                    alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                    alertViewController.swipeDismissalGestureEnabled = true
                                    alertViewController.backgroundTapDismissalGestureEnabled = true
                                    alertViewController.buttonColor = UIColor.red
                                    // Add alert actions
                                    
                                    
                                    let cancelAction = NYAlertAction(
                                        title: "OK",
                                        style: .cancel,
                                        handler: { (action: NYAlertAction!) -> Void in
                                            
                                            self.dismiss(animated: true, completion: nil)
                                    }
                                    )
                                    
                                    alertViewController.addAction(cancelAction)
                                    
                                    // Present the alert view controller
                                    self.present(alertViewController, animated: true, completion: nil)
                                    
                                }
                            }
                                
                            else
                            {
                                self.hideLoader()
                                let alertViewController = NYAlertViewController()
                                // Set a title and message
                                alertViewController.title = "Login"
                                alertViewController.message = response.result.value?.message!
                                
                                // Customize appearance as desired
                                alertViewController.buttonCornerRadius = 20.0
                                alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.swipeDismissalGestureEnabled = true
                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                alertViewController.buttonColor = UIColor.red
                                // Add alert actions
                                
                                
                                let cancelAction = NYAlertAction(
                                    title: "OK",
                                    style: .cancel,
                                    handler: { (action: NYAlertAction!) -> Void in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                }
                                )
                                
                                alertViewController.addAction(cancelAction)
                                
                                // Present the alert view controller
                                self.present(alertViewController, animated: true, completion: nil)
                                
                            }
                        case .failure(let error):
                            print(error)
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            // Set a title and message
                            alertViewController.title = "Login"
                            alertViewController.message = "Server Error! Please try again later!"
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                        
                        
                }
            }else{
                showAlertBox(text: "Enter password")
            }
        }else{
            showAlertBox(text: "Enter username")
        }
        
        
        }
        else
        {
           self.view.makeToast("No Internet connection.  Please turn on WiFi or enable data.")
        }
        
    }
    static func getSession() {
        let userDefaults = UserDefaults.standard
        //if userDefaults.object(forKey: "AnalyticsSessionID") == nil {
        if isReachable()
        {
            let userIDString:Int?
            let DeviceUUID = UUID().uuidString
           // var userIDString = NSString()
            if userDefaults.object(forKey: "userIdString") != nil {
            userIDString = userDefaults.integer(forKey: "userIdString")
                
            }
            else
            {
                userIDString = 0
            }
            let IPAddress = UIDevice.current.ipAddress()
            let parameters: [String: Any] = [
                "Platform" : "Mobile",
                "Userid":userIDString! ,
                "Ipaddress" : IPAddress! ,
                "Macaddress": DeviceUUID
                
            ]
            print(UIDevice.modelName)
            
            print(IPAddress!)
            print(DeviceUUID)
            
            Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)AnalyticsApi/api/AnalyticsDataReport/GetSessionData", method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        print(response)
                        let responseVar = response.result.value
                        let json = JSON(responseVar!)
                        print(json["Sessionid"])
                        let sessionID = "\(json["Sessionid"])"
                        let deviceID = "\(json["Deviceid"])"
                        userDefaults.set(sessionID, forKey: "AnalyticsSessionID")
                        userDefaults.set(deviceID, forKey: "AnalyticsDeviceID")
                        userDefaults.synchronize()
                        //  defaults.set("No", forKey:"AnalyticsSession")
                        userDefaults.synchronize()
                    case .failure(let error):
                        print(error)
                    }
                    
                    
            }
            
        }
        //  }
    }
    func perferenceMethod()
    {
                let userDefaults1 = UserDefaults.standard
                let accessToken = userDefaults1.value(forKey: "accessToken")
                print("\(accessToken)")
        let url = "\(APIUrl.ZIGTARCAPI)Preferences/GetAppPreferences?"
        let parameters: Parameters = [
            "Pin": "ZIG19",
            
            ]
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<preferenceGet>) in
                switch response.result {
                case .success:
                    print(response.result.value ?? "nil")
                    if response.result.value?.Message != nil
                    {
                        userDefaults1.set("true", forKey: "isBusOn")
                        userDefaults1.set(response.result.value?.Bird, forKey: "isBirdOn")
                        userDefaults1.set(response.result.value?.Louvelo, forKey: "LouveloOn")
                        userDefaults1.set(response.result.value?.Uber, forKey: "UberOn")
                        userDefaults1.set(response.result.value?.Lyft, forKey: "LyftOn")
                        userDefaults1.set(response.result.value?.Bolt, forKey: "BoltOn")
                        userDefaults1.set(response.result.value?.Lime, forKey: "LimeOn")
                        userDefaults1.set(response.result.value?.Livepayment, forKey: "Livepayment")
                        userDefaults1.set(response.result.value?.COVID, forKey: "EmergencyAlert")
                        userDefaults1.synchronize()
                        self.perferenceMethod(accessToken: accessToken as! NSString)
                    }
                    else
                    {
                        self.perferenceMethod(accessToken: accessToken as! NSString)
                    }
                case .failure(let error):
                    print(error)
                    self.perferenceMethod(accessToken: accessToken as! NSString)
                    
                }
        }
        

     
    }
    func perferenceMethod(accessToken:NSString)
    {
        //
        var birdBool = Bool()
        var LouveleBool = Bool()
        var cabBool = Bool()
        var uberBool = Bool()
        var lyftBool = Bool()
        
        let userDefaults1 = UserDefaults.standard
        
        let urls = "\(APIUrl.ZIGTARCAPI)Preferences/get"
        let parameterss: Parameters = [
            "TokenID": accessToken,
            
            ]
        
        
        Alamofire.request(urls, method: .get, parameters: parameterss, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<TokenGet>) in
                switch response.result {
                case .success:
                    print(response.result.value ?? "nil")
                    if response.result.value?.message == nil
                    {
                        
                        //Arun 23 june
                        
                        
                        userDefaults1.set(response.result.value?.bus, forKey: "isBus")
                        if !userDefaults1.bool(forKey: "isBirdOn")
                        {
                            birdBool = false
                            userDefaults1.set("false", forKey: "isBike")
                        }
                        else
                        {
                            birdBool = true
                            userDefaults1.set("true", forKey: "isBike")
                        }
                        
                        if !userDefaults1.bool(forKey: "LouveloOn")
                        {
                            LouveleBool = false
                            userDefaults1.set("false", forKey: "isTrain")
                            
                        }
                        else
                        {
                            LouveleBool = false
                            userDefaults1.set(response.result.value?.train, forKey: "isTrain")
                            
                        }
                        
                        if response.result.value?.uber == nil && response.result.value?.lyft == nil
                        {
                            if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                            {
                                uberBool = false
                                lyftBool = false
                                cabBool = false
                                userDefaults1.set(false, forKey: "isCab")
                                userDefaults1.set(false, forKey: "isUber")
                                userDefaults1.set(false, forKey: "isLyft")
                            }
                            else
                            {
                                uberBool = true
                                lyftBool = false
                                cabBool = true
                                userDefaults1.set(true, forKey: "isCab")
                                userDefaults1.set(true, forKey: "isUber")
                                userDefaults1.set(false, forKey: "isLyft")
                            }
                            
                        }
                        else
                        {
                            if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                            {
                                uberBool = false
                                lyftBool = false
                                cabBool = false
                                userDefaults1.set(false, forKey: "isCab")
                                userDefaults1.set(false, forKey: "isUber")
                                userDefaults1.set(false, forKey: "isLyft")
                            }
                            else
                            {
                                uberBool = response.result.value!.uber!
                                lyftBool = response.result.value!.lyft!
                                cabBool = response.result.value!.cab!
                                userDefaults1.set(response.result.value?.cab, forKey: "isCab")
                                userDefaults1.set(response.result.value?.uber, forKey: "isUber")
                                userDefaults1.set(response.result.value?.lyft, forKey: "isLyft")
                            }
                        }
                        
                        if response.result.value?.bikeshare == nil
                        {
                            userDefaults1.set("false", forKey: "bikeshare")
                        }
                        else
                        {
                            userDefaults1.set(response.result.value?.bikeshare, forKey: "bikeshare")
                        }
                        userDefaults1.synchronize()
                        
                        
                        
                        let url = "\(APIUrl.ZIGTARCAPI)Preferences/send"
                        let parameters: Parameters = [
                            "TokenId": accessToken,
                            "bus": "true",
                            "bike": "\(birdBool)",
                            "train": "\(LouveleBool)",
                            "cab": "\(cabBool)",
                            "uber": "\(uberBool)",
                            "lyft": "\(lyftBool)",
                            "lime": "false",
                            "bolt": "false"
                        ]
                        userDefaults1.set("true", forKey: "isBus")
                        userDefaults1.set(birdBool, forKey: "isBike")
                        userDefaults1.set(LouveleBool, forKey: "isTrain")
                        userDefaults1.set(cabBool, forKey: "isCab")
                        userDefaults1.set(uberBool, forKey: "isUber")
                        userDefaults1.set(lyftBool, forKey: "isLyft")
                        userDefaults1.synchronize()
                        self.hideLoader()
                        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
                            .responseObject{ (response: DataResponse<TokenSend>) in
                                switch response.result {
                                case .success:
                                    
                                    break
                                case .failure(let error):
                                    print(error.localizedDescription)
                                }
                                
                        }
                        
                        self.newRedirection()

                        
                    }
                    else
                    {
                        userDefaults1.set("true", forKey: "isBus")
                        
                        if !userDefaults1.bool(forKey: "isBirdOn")
                        {
                            userDefaults1.set("false", forKey: "isBike")
                        }
                        else
                        {
                            userDefaults1.set("true", forKey: "isBike")
                        }
                        
                        if !userDefaults1.bool(forKey: "LouveloOn")
                        {
                            userDefaults1.set("false", forKey: "isTrain")
                            
                        }
                        else
                        {
                            userDefaults1.set(response.result.value?.train, forKey: "isTrain")
                            
                        }
                        
                        if !userDefaults1.bool(forKey: "UberOn") && !userDefaults1.bool(forKey: "LyftOn")
                        {
                            userDefaults1.set(false, forKey: "isCab")
                            userDefaults1.set(false, forKey: "isUber")
                            userDefaults1.set(false, forKey: "isLyft")
                        }
                        else
                        {
                            userDefaults1.set(true, forKey: "isCab")
                            userDefaults1.set(true, forKey: "isUber")
                            userDefaults1.set(false, forKey: "isLyft")
                        }
                        userDefaults1.synchronize()
                        self.newRedirection()

                    }
                case .failure(let error):
                    print(error)
                    self.newRedirection()
                }
        }
    }
    func newRedirection()
    {
        self.hideLoader()

        if self.sourcelat != nil && self.sourcelng != nil && self.sourcePlaceId != nil && self.Destinationlat != nil && self.Destinationlng != nil && self.sourcePlaceId != nil && self.BirdBikeLat != nil && self.BirdBikeLon != nil{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripSelectionViewController") as! TripSelectionViewController
            nextViewController.sourceString = self.sourcePlaceId!
            nextViewController.sourceLat = self.sourcelat!
            nextViewController.sourceLng = self.sourcelng!
            nextViewController.destinationString = self.DestinationPlaceId!
            nextViewController.destinationLat = self.Destinationlat!
            nextViewController.destinationLng = self.Destinationlng!
            nextViewController.isCalenderEvent = self.iscalendarEvent!
            nextViewController.tripSchduleTime = self.tripSchduleTime
            nextViewController.BirdDatalistapicount = self.BirdDatalistapicount
            nextViewController.BirdBikeLat = self.BirdBikeLat
            nextViewController.BirdBikeLon = self.BirdBikeLon
            nextViewController.ameniticsTitle = ""
            nextViewController.AmenititesTypeTrip = ""
            print(self.DestinationAddresstest!)
            print(self.sourceAddresstest!)
            nextViewController.DestinationAddressString = self.DestinationAddresstest
            nextViewController.sourceaddressString = self.sourceAddresstest
            self.navigationController?.pushViewController(nextViewController, animated: false)
            
            
        }
        else{
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
            nextViewController.isRedirectFromLogin = true
            self.navigationController?.pushViewController(nextViewController, animated: false)
        }
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
        
    }
    func showAlertBox(text:String){
        DispatchQueue.main.async {
            self.hideLoader()
        }
        let alertViewController = NYAlertViewController()
        // Set a title and message
        alertViewController.title = "Details Missing"
        alertViewController.message = "\(text)"
        
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = self.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        
        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in
                // self.sourceTextField.becomeFirstResponder()
                self.dismiss(animated: true, completion: nil)
        }
        )
        //        alertViewController.addAction(activateAction)
        alertViewController.addAction(cancelAction)
        
        // Present the alert view controller
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    @IBAction func Tripplan(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WithOutLoginMainViewController") as! WithOutLoginMainViewController
        self.navigationController?.pushViewController(nextViewController, animated: false)
        
    }
    @objc func signupAction() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewSignUpViewController") as! NewSignUpViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        
    }
    @IBAction func ResetPassword(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
        nextViewController.NextPageEmail = UsernameTxt.text
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        
    }
    func GetallBeaconData() {
         let realm = try! Realm()
        print(APIUrl.MacIDURL)
        Alamofire.request(APIUrl.MacIDURL, method: .get, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    print("Beacon Data Featch Success")
                    if response.result.value != nil {
                        if let json = response.result.value as? [String: Any] {
                            let Macids = json["BeaconIds"] as! NSArray
                            for Macidlist in Macids {
                                
                                
                                let BeacanMacID = BeaconRealmModel()
                                BeacanMacID.MacID = Macidlist as! String
                                try! realm.write {
                                    
                                    realm.add(BeacanMacID, update: .all)
                                    
                                }
                                
                            }
                            do {
                                let beacon_db = try! Realm().objects(BeaconRealmModel.self)
                                print("beacon_db count\(beacon_db.count)")
                                
                                print("saveditem beacon_db")
                            }
                            catch {
                                print(error)
                            }
                            
                        }
                        
                        
                    }
                case .failure(let error):
                    print(error)
                    
                }
        }
        
        
        
    }
}
