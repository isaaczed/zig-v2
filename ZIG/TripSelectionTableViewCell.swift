//
//  TripSelectionTableViewCell.swift
//  ZIG
//
//  Created by Isaac on 22/12/17.
//  Copyright © 2017 Isaac. All rights reserved.
//

import UIKit
import UberRides
import SwipeCellKit
import LyftSDK
class TripSelectionTableViewCell: SwipeTableViewCell {

    let TicketnumberLabel = UILabel()
    let placeSourceLabel = UILabel()
    let placeDestLabel = UILabel()
    let ticketImage = UIImageView()
    let busLable = UILabel()
    let fareLabel = UILabel()
    let validLabel = UILabel()
    let timeLabel = UILabel()
    let activeLable = UILabel()
    let startLable = UILabel()
    let endLable = UILabel()
    let transitScrollView = UIScrollView()
    var uberbutton = RideRequestButton()
    let transitNameLabel = UILabel()
    let transitFareLabel = UILabel()
    var uberBtn = RideRequestButton()
    var lyftButton = LyftButton()
    let uberBackView = UIView()
    let expiryTitleLabel = UILabel()
    let expiryLabel = UILabel()
    let mytarcFare = UILabel()
    let mytarcImage = UIImageView()
    let DistanceLabelUber = UILabel()
    let NoTransit = UILabel()
    
    
    let durationLabel = UILabel()
    let TotalMyTarcFareLabel = UILabel()
    let mytarccard = UILabel()
    let Regular = UILabel()
    
    let transitImage = UIImageView()
    let arrowImage = UIImageView()
    let MyTarcButton = UIImageView()
    let busName = UILabel()
    let busTimingLable = UILabel()
    
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        transitImage.image = nil
        arrowImage.image = nil
        MyTarcButton.image = nil
        busName.text = nil
        busTimingLable.text = nil
        
        NoTransit.text = nil
        durationLabel.text = nil
        TotalMyTarcFareLabel.text = nil
        Regular.text = nil
        mytarccard.text = nil
        TicketnumberLabel.text = nil
        placeDestLabel.text = nil
        mytarcImage.image = nil
        placeDestLabel.text = nil
        ticketImage.image = nil
        busLable.text = nil
        fareLabel.text = nil
        validLabel.text = nil
        timeLabel.text = nil
        activeLable.text = nil
        startLable.text = nil
        endLable.text = nil
        expiryLabel.text = nil
        expiryTitleLabel.text = nil
        transitNameLabel.text = nil
        transitFareLabel.text = nil
        mytarcFare.text = nil
        DistanceLabelUber.text = nil
        uberBtn = RideRequestButton()
        lyftButton = LyftButton()
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
       
       
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class FareTableViewCell: UITableViewCell {
    let Title1 = UILabel()
    let Title2 = UILabel()
    let LocalTitle = UILabel()
    let ExpTitle = UILabel()
    let LocalNormal = UILabel()
    let LocalMytarc = UILabel()
    let ExpressNormal = UILabel()
    let ExpressMytarc = UILabel()
    let myTarc = UIImageView()
    
}
