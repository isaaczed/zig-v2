//
//  ResetPasswordViewController.swift
//  ZIG
//
//  Created by Isaac on 18/04/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Toast_Swift
import Alamofire
import NYAlertViewController
import NVActivityIndicatorView

class ResetPasswordViewController: UIViewController, UITextFieldDelegate {
    var NextPageEmail:String?
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var EmailLab: UILabel!
     var loaderView = UIView()
    @IBOutlet weak var Resetpassword: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        analytics.GetPageHitCount(PageName: "Reset Password")
        
         EmailTextField.delegate = self
         Resetpassword.addTarget(self, action:#selector(resetpassword), for: UIControlEvents.touchUpInside)
 EmailLab.isHidden =  true
        EmailTextField.text = ""
        // Do any additional setup after loading the view.
        self.EmailTextField.font = UIFont.setTarcBold(size: 15.0)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginNewViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        if NextPageEmail != nil{
            EmailTextField.text = NextPageEmail
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        textField.resignFirstResponder()
        return true
    }
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.EmailTextField
        {
            
            self.EmailTextField.font = UIFont.setTarcRegular(size: 20.0)
            self.EmailLab.isHidden = false
            self.EmailLab.text = self.EmailTextField.placeholder
            self.EmailTextField.placeholder = ""
           
            
            
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
        //Checkes if textfield is empty or not after after user ends editing.
        if textField == self.EmailTextField
        {
            if self.EmailTextField.text == ""
            {
                self.EmailTextField.font = UIFont.setTarcBold(size: 15.0)
                self.EmailLab.isHidden = true
                self.EmailTextField.placeholder = "EmailID"
               
                
                
                
                
            }
            
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func Signup(_ sender: Any) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewSignUpViewController") as! NewSignUpViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @objc func resetpassword() {
        if isReachable()
        {
        self.showLoader()
        let EmailID = EmailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if EmailID != nil && EmailID != "" {
            
        
        let Url_Reset = "\(APIUrl.ZIGTARCAPI)User/ResetPassword?EmailId=\(EmailID)"
        Alamofire.request(Url_Reset, method: .post,  encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<ResetPasswordModel>) in
                switch response.result {
                case .success:
                    print(response)
                    //to get status code
                    if response.result.value?.Message == "An email has been sent to reset your password"
                    {
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Reset Password"
                        alertViewController.message = "An email has been sent to reset your password"
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                                
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginNewViewController") as! LoginNewViewController
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        

                        
                        
                    }
                        
                    else
                    {
                        
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        // Set a title and message
                        alertViewController.title = "Reset Password"
                        alertViewController.message = response.result.value?.Message ?? "Something wrong "
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                    }
                case .failure(let error):
                    print(error)
                    self.hideLoader()
                    let alertViewController = NYAlertViewController()
                    // Set a title and message
                    alertViewController.title = "Reset Password"
                    alertViewController.message = "Server Error! Please try again later!"
                    
                    // Customize appearance as desired
                    alertViewController.buttonCornerRadius = 20.0
                    alertViewController.view.tintColor = self.view.tintColor
                    alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                    alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.swipeDismissalGestureEnabled = true
                    alertViewController.backgroundTapDismissalGestureEnabled = true
                    alertViewController.buttonColor = UIColor.red
                    // Add alert actions
                    
                    
                    let cancelAction = NYAlertAction(
                        title: "OK",
                        style: .cancel,
                        handler: { (action: NYAlertAction!) -> Void in
                            
                            self.dismiss(animated: true, completion: nil)
                    }
                    )
                    
                    alertViewController.addAction(cancelAction)
                    
                    // Present the alert view controller
                    self.present(alertViewController, animated: true, completion: nil)
                }
                
                
        }
    }
        else {
            self.hideLoader()
            let alertViewController = NYAlertViewController()
            // Set a title and message
            alertViewController.title = "Reset Password"
            alertViewController.message = "Enter your Email address"
            
            // Customize appearance as desired
            alertViewController.buttonCornerRadius = 20.0
            alertViewController.view.tintColor = self.view.tintColor
            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
            alertViewController.swipeDismissalGestureEnabled = true
            alertViewController.backgroundTapDismissalGestureEnabled = true
            alertViewController.buttonColor = UIColor.red
            // Add alert actions
            
            
            let cancelAction = NYAlertAction(
                title: "OK",
                style: .cancel,
                handler: { (action: NYAlertAction!) -> Void in
                    
                    self.dismiss(animated: true, completion: nil)
            }
            )
            
            alertViewController.addAction(cancelAction)
            
            // Present the alert view controller
            self.present(alertViewController, animated: true, completion: nil)
        }
        }
        else
        {
            self.view.makeToast("No Internet connection.  Please turn on WiFi or enable data.")

        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
        
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
        
    }
    
    
}

