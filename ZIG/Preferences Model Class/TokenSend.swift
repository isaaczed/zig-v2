//
//	TokenSend.swift


import Foundation 
import ObjectMapper


class TokenSend : NSObject, NSCoding, Mappable{

	var message : String?


	class func newInstance(map: Map) -> Mappable?{
		return TokenSend()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		message <- map["Message"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "Message") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}

	}

}