//
//	TokenGet.swift


import Foundation 
import ObjectMapper


class TokenGet : NSObject, NSCoding, Mappable{

	var message : AnyObject?
	var tokenId : String?
	var bike : Bool?
	var bus : Bool?
	var cab : Bool?
	var train : Bool?
    var uber : Bool?
    var lyft : Bool?
    var bikeshare : Bool?


	class func newInstance(map: Map) -> Mappable?{
		return TokenGet()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		message <- map["Message"]
		tokenId <- map["TokenId"]
		bike <- map["bike"]
		bus <- map["bus"]
		cab <- map["cab"]
		train <- map["train"]
        uber <- map["uber"]
        lyft <- map["lyft"]
        bikeshare <- map["bikeshare"]

		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "Message") as? AnyObject
         tokenId = aDecoder.decodeObject(forKey: "TokenId") as? String
         bike = aDecoder.decodeObject(forKey: "bike") as? Bool
         bus = aDecoder.decodeObject(forKey: "bus") as? Bool
         cab = aDecoder.decodeObject(forKey: "cab") as? Bool
         train = aDecoder.decodeObject(forKey: "train") as? Bool
        uber = aDecoder.decodeObject(forKey: "uber") as? Bool
        lyft = aDecoder.decodeObject(forKey: "lyft") as? Bool
        bikeshare = aDecoder.decodeObject(forKey: "bikeshare") as? Bool

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "Message")
		}
		if tokenId != nil{
			aCoder.encode(tokenId, forKey: "TokenId")
		}
		if bike != nil{
			aCoder.encode(bike, forKey: "bike")
		}
		if bus != nil{
			aCoder.encode(bus, forKey: "bus")
		}
		if cab != nil{
			aCoder.encode(cab, forKey: "cab")
		}
		if train != nil{
			aCoder.encode(train, forKey: "train")
		}
        if uber != nil{
            aCoder.encode(uber, forKey: "uber")
        }
        if lyft != nil{
            aCoder.encode(lyft, forKey: "lyft")
        }
        if bikeshare != nil{
            aCoder.encode(bikeshare, forKey: "bikeshare")
        }
        
	}

}
