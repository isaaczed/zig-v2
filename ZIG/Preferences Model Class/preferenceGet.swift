//
//    TokenGet.swift


import Foundation
import ObjectMapper


class preferenceGet : NSObject, NSCoding, Mappable{

    var Message : AnyObject?
    var Uber : Bool?
    var Lyft : Bool?
    var Louvelo : Bool?
    var Bird : Bool?
    var Lime : Bool?
    var Bolt : Bool?
    var Livepayment : Bool?
    var COVID:Bool?
    var CovidTitle:String?
    var CovidimageURL:String?
    var CovidContent:String?
    var NotificationStatus:Bool?
    var NotificationTitle:String?
    var NotificationContent:String?



    class func newInstance(map: Map) -> Mappable?{
        return preferenceGet()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        Message <- map["Message"]
        Uber <- map["Uber"]
        Lyft <- map["Lyft"]
        Louvelo <- map["Louvelo"]
        Bird <- map["Bird"]
        Lime <- map["Lime"]
        Bolt <- map["Bolt"]
        Livepayment <- map["Livepayment"]
        COVID <- map["Covid.Status"]
        CovidTitle <- map["Covid.Title"]
        CovidimageURL <- map["Covid.Url"]
        CovidContent <- map["Covid.Content"]
        NotificationStatus <- map["Notification.Status"]
        NotificationTitle <- map["Notification.Title"]
        NotificationContent <- map["Notification.Content"]

        
        
    }

    
    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         Message = aDecoder.decodeObject(forKey: "Message") as? AnyObject
         Uber = aDecoder.decodeObject(forKey: "Uber") as? Bool
         Lyft = aDecoder.decodeObject(forKey: "Lyft") as? Bool
         Louvelo = aDecoder.decodeObject(forKey: "Louvelo") as? Bool
         Bird = aDecoder.decodeObject(forKey: "Bird") as? Bool
         Lime = aDecoder.decodeObject(forKey: "Lime") as? Bool
         Bolt = aDecoder.decodeObject(forKey: "Bolt") as? Bool
        Livepayment = aDecoder.decodeObject(forKey: "Livepayment") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if Message != nil{
            aCoder.encode(Message, forKey: "Message")
        }
    
        if Uber != nil{
            aCoder.encode(Uber, forKey: "Uber")
        }
        if Lyft != nil{
            aCoder.encode(Lyft, forKey: "Lyft")
        }
        if Louvelo != nil{
            aCoder.encode(Louvelo, forKey: "Louvelo")
        }
        if Bird != nil{
            aCoder.encode(Bird, forKey: "Bird")
        }
        if Lime != nil{
            aCoder.encode(Lime, forKey: "Lime")
        }
        if Bolt != nil{
            aCoder.encode(Bolt, forKey: "Bolt")
        }
        if Livepayment != nil{
            aCoder.encode(Livepayment, forKey: "Livepayment")
        }
    }

}


class notificationpreferenceGet : NSObject, NSCoding, Mappable{

    var Message : AnyObject?
    var NotificationStatus:Bool?
    var NotificationTitle:String?
    var NotificationContent:String?



    class func newInstance(map: Map) -> Mappable?{
        return notificationpreferenceGet()
    }
    required init?(map: Map){}
    private override init(){}

    func mapping(map: Map)
    {
        Message <- map["Message"]
        NotificationStatus <- map["Notification.Status"]
        NotificationTitle <- map["Notification.Title"]
        NotificationContent <- map["Notification.Content"]

        
        
    }

    
    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         Message = aDecoder.decodeObject(forKey: "Message") as? AnyObject


    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if Message != nil{
            aCoder.encode(Message, forKey: "Message")
        }

    }

}



//class CovidArray:Mappable {
//    var Status:Bool?
//    var Title:String?
//    var URLImage:String?
//    var Content:String?
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//
//    }
//
//
//}
