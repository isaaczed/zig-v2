//
//  Mapsandroute.swift
//  ZIG
//
//  Created by Mac HD on 3/29/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import Foundation
import ObjectMapper

class LineDetailsList:Mappable{
    var TripHeadsignNow:String?
    var RouteID:String?
    var TripDetailsTripId:Int?
    var DepartureTime:String?
    var ArrivalTime:String?
    var TripTitle:String?
    var RouteNumber:String?
    var RouteColor:String?
    var Message:String?
    required init?(map: Map) {
    
    }
    
    func mapping(map: Map) {
        TripHeadsignNow <- map["TripHeadsign"]
        RouteID <- map["RouteID"]
        TripDetailsTripId <- map["TripDetails.TripId"]
        DepartureTime <- map["TripDetails.DepartureTime"]
        ArrivalTime <- map["TripDetails.ArrivalTime"]
        TripTitle <- map["TripDetails.TripTitle"]
        RouteNumber <- map["TripDetails.RouteNumber"]
        RouteColor <- map["TripDetails.RouteColor"]
        Message <- map["Message"]
        
    }
    
    
    
    
}
