//
//	ScheduleRootClass.swift


import Foundation 
import ObjectMapper


class ScheduleRootClass : NSObject, Mappable{

	var routes : [ScheduleRoute]?


	class func newInstance(map: Map) -> Mappable?{
		return ScheduleRootClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		routes <- map
		
	}

//    /**
//    * NSCoding required initializer.
//    * Fills the data from the passed decoder
//    */
//    @objc required init(coder aDecoder: NSCoder)
//    {
//         routes = aDecoder.decodeObject(forKey:) as? [ScheduleRoute]
//
//    }
//
//    /**
//    * NSCoding required method.
//    * Encodes mode properties into the decoder
//    */
//    @objc func encode(with aCoder: NSCoder)
//    {
//        if routes != nil{
//            aCoder.encode(routes, forKey: "routes")
//        }
//
//    }

}
