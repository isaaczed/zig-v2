//
//	MapsTripBusStop.swift


import Foundation 
import ObjectMapper


class MapsTripBusStop : NSObject, NSCoding, Mappable{

	var arrivalTime : String?
	var departureTime : String?
	var distance : AnyObject?
	var lat : String?
	var location : AnyObject?
	var longField : String?
	var status : Bool?
	var stopCode : AnyObject?
	var stopId : String?
	var stopName : String?
	var stopSequence : Int?
	var tripId : Int?


	class func newInstance(map: Map) -> Mappable?{
		return MapsTripBusStop()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		arrivalTime <- map["ArrivalTime"]
		departureTime <- map["DepartureTime"]
		distance <- map["Distance"]
		lat <- map["Lat"]
		location <- map["Location"]
		longField <- map["Long"]
		status <- map["Status"]
		stopCode <- map["StopCode"]
		stopId <- map["StopId"]
		stopName <- map["StopName"]
		stopSequence <- map["StopSequence"]
		tripId <- map["TripId"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         arrivalTime = aDecoder.decodeObject(forKey: "ArrivalTime") as? String
         departureTime = aDecoder.decodeObject(forKey: "DepartureTime") as? String
         distance = aDecoder.decodeObject(forKey: "Distance") as? AnyObject
         lat = aDecoder.decodeObject(forKey: "Lat") as? String
         location = aDecoder.decodeObject(forKey: "Location") as? AnyObject
         longField = aDecoder.decodeObject(forKey: "Long") as? String
         status = aDecoder.decodeObject(forKey: "Status") as? Bool
         stopCode = aDecoder.decodeObject(forKey: "StopCode") as? AnyObject
         stopId = aDecoder.decodeObject(forKey: "StopId") as? String
         stopName = aDecoder.decodeObject(forKey: "StopName") as? String
         stopSequence = aDecoder.decodeObject(forKey: "StopSequence") as? Int
         tripId = aDecoder.decodeObject(forKey: "TripId") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if arrivalTime != nil{
			aCoder.encode(arrivalTime, forKey: "ArrivalTime")
		}
		if departureTime != nil{
			aCoder.encode(departureTime, forKey: "DepartureTime")
		}
		if distance != nil{
			aCoder.encode(distance, forKey: "Distance")
		}
		if lat != nil{
			aCoder.encode(lat, forKey: "Lat")
		}
		if location != nil{
			aCoder.encode(location, forKey: "Location")
		}
		if longField != nil{
			aCoder.encode(longField, forKey: "Long")
		}
		if status != nil{
			aCoder.encode(status, forKey: "Status")
		}
		if stopCode != nil{
			aCoder.encode(stopCode, forKey: "StopCode")
		}
		if stopId != nil{
			aCoder.encode(stopId, forKey: "StopId")
		}
		if stopName != nil{
			aCoder.encode(stopName, forKey: "StopName")
		}
		if stopSequence != nil{
			aCoder.encode(stopSequence, forKey: "StopSequence")
		}
		if tripId != nil{
			aCoder.encode(tripId, forKey: "TripId")
		}

	}

}