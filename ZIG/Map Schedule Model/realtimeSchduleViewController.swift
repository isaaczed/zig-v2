//
//  realtimeSchduleViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 26/07/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SCLAlertView
import CoreLocation
import ExpandableCell
import SafariServices
import MapKit
import NVActivityIndicatorView
import NYAlertViewController
import XLActionController
struct RealTimeSchedules {
    static var SelectedSchedule = ""
    static var SelectedScheduleRoute = ""
    static var SelectedScheduleRouteColor = ""

}
class realtimeSchduleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, CLLocationManagerDelegate {
    let assistiveTouchSOS = AssistiveTouch()
    var LineDetails = [mapsandSchudleList]()
  //  var whatsNearMe = UIButton()
    var SchudleArrayList = [[String: Any]]()
    var SchudleArrayValue: NSMutableDictionary = [:]
    var coordinateArray: [CLLocationCoordinate2D] = []
    var scheduleArray: NSMutableArray = []
    var dummyScheduleArray: NSMutableArray = []
    var realTimeMapTimer: Timer!
    let selectProfileBackView = UIView()
    let locationManager = CLLocationManager()
    var menuBackView = UIView()
    var paymentBackView = UIView()
    let selectedProfileArray: NSMutableArray = []
    var routeIDString = ""
    var rasterSize: CGFloat = 11.0
    var isSorta = true
    var startStop = String()
    var stopStop = String()
    var routeColorGlobal = String()
    var selectProfileTableView = UITableView()
    var totalFareLabel = UILabel()
    var profileDataList: DataResponse<GetProfile>!
    var viewConstraints: [NSLayoutConstraint]?
    var vehicleMarkerArray: NSMutableArray = []
    var menuTabelView: ExpandableTableView = ExpandableTableView()
    var busBtn = UIButton()
    var brtBtn = UIButton()
    var railBtn = UIButton()
    var trolleyBtn = UIButton()
    var btnFlag = 0
    var backgroundBlurView = UIView()
    var backgroundBlurView1 = UIView()
    var loaderView = UIView()
    var stepArray: NSMutableArray = []
    var submitBtn = UIButton()
    var i: UInt = 0
    var animateTimer: Timer!
    let addedProfileID: NSMutableArray = []
    var selectProfileLbl = UILabel()
    var selectProfileDropDown = UILabel()
    var buyNowbtn = UIButton()
    var selectProfileArray: NSMutableArray = []
    var cellEx: UITableViewCell {
        return (menuTabelView.dequeueReusableCell(withIdentifier: ExpandedCell.ID)!)
    }
    let cellReuseIdentifier = "cell"
    var routeDetails: NSMutableDictionary = [:]
    var routeIDArray: NSMutableArray = []
    var isMenuActive = false
    var busNameStrin = String()
    @IBOutlet private weak var mapView: MKMapView!
    var mylocation = CLLocationCoordinate2D(latitude: 38.2526647, longitude: -85.75845570000001)
    var mklocationManager = CLLocationManager()
    var lat = 0.0
    var lng = 0.0
    var firstTimeApiCall = true
    var realTimeArray = [realtimeSchduleClass]()
    var   pdfButton = UIButton()

    override func viewWillAppear(_ animated: Bool) {
    }

    override func viewDidDisappear(_ animated: Bool) {
        isMenuActive = false
        self.OnMenuClicked()
    }

    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height + 120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            let window = UIApplication.shared.keyWindow!

            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width / 2) - 40, y: (self.view.frame.size.height / 2) - 40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }

    }

    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }

    }

    override func viewWillDisappear(_ animated: Bool) {
        if realTimeMapTimer != nil {
            realTimeMapTimer?.invalidate()
            realTimeMapTimer = nil
        }

//       self.hideLoader()
//        NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: Network.reachability)
    }


    override func viewDidLoad() {


        super.viewDidLoad()
        let userDefaults1 = UserDefaults.standard
        let accessToken = userDefaults1.value(forKey: "realTimeTable")
        if accessToken  == nil
        {
            self.scheduleArray = NSMutableArray()
        }
        else
        {
            let scduleArr = userDefaults1.value(forKey: "realTimeTable") as! NSArray
            self.scheduleArray = scduleArr.mutableCopy() as! NSMutableArray
            print(self.scheduleArray)
        }
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
        analytics.GetPageHitCount(PageName: "Maps & Schedules")
        mapView.delegate = self
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let region = MKCoordinateRegion(center: mylocation, span: span)
        mapView.setRegion(region, animated: true)
        busBtn.isHidden = true
        railBtn.isHidden = true
        brtBtn.isHidden = true
        trolleyBtn.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationItem.title = " "
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
            style: UIBarButtonItemStyle.done,
            target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.setHidesBackButton(true, animated: true);
        let menuButton = UIBarButtonItem(image: UIImage(named: "mapsSchdMenu"),
            style: UIBarButtonItemStyle.plain,
            target: self, action: #selector(OnMenuClicked))
        menuButton.accessibilityLabel = "RealTime Menu"

        self.navigationItem.rightBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.title = "Real-Time Schedules"
       //mapView.frame.size.height - 60
             let xPostion = self.view.frame.size.width/2 - 120
                      let   whatsNearMe = UIButton()
                                     whatsNearMe.frame = CGRect.init(x: xPostion, y: 10, width: 150, height: 40)
                whatsNearMe .setTitle("Routes & Schedules", for: .normal)
                whatsNearMe.setTitleColor(UIColor.white, for: .normal)
        whatsNearMe.setBackgroundColor(UIColor.init(red: 42/255.0, green: 127/255.0, blue: 195/255.0, alpha: 1.0), for: .normal)
        whatsNearMe.titleLabel?.font  = UIFont.init(name: "Linotte-Bold", size: 14.0)
        whatsNearMe.titleLabel!.numberOfLines = 1;
        whatsNearMe.titleLabel!.adjustsFontSizeToFitWidth = true;               whatsNearMe.addTarget(self, action: #selector(nearbyAction), for: .touchUpInside)
               // view.addSubview(whatsNearMe)
                  whatsNearMe.layer.cornerRadius = 20
                  whatsNearMe.layer.masksToBounds = true
               // mapView.bringSubview(toFront: whatsNearMe)
        
                   pdfButton = UIButton()
                pdfButton.frame = CGRect.init(x: self.view.frame.size.width/2 - 40, y: 10, width: 80, height: 40)
                          pdfButton .setTitle("PDF", for: .normal)
                          pdfButton.setTitleColor(UIColor.init(red: 42/255.0, green: 127/255.0, blue: 195/255.0, alpha: 1.0), for: .normal)
                  pdfButton.setBackgroundColor(UIColor.white, for: .normal)
                  pdfButton.titleLabel?.font  = UIFont.init(name: "Linotte-Bold", size: 14.0)
                  pdfButton.titleLabel!.numberOfLines = 1;
                  pdfButton.titleLabel!.adjustsFontSizeToFitWidth = true;               pdfButton.addTarget(self, action: #selector(MyTarcCard(_:)), for: .touchUpInside)
                          view.addSubview(pdfButton)
                  pdfButton.layer.cornerRadius = 20
                  pdfButton.layer.masksToBounds = true
                          mapView.bringSubview(toFront: pdfButton)
        pdfButton.isHidden = true
        
        
        menuBackView.frame = CGRect.init(x: 2000, y: 0, width: (self.view.frame.size.width / 2) + 90, height: self.view.frame.size.height)
        menuBackView.backgroundColor = UIColor.white
        menuBackView.layer.borderWidth = 2
        menuBackView.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor
        self.view.addSubview(menuBackView)
      //  self.menuBackView.isHidden = true

        paymentBackView.frame = CGRect.init(x: 40, y: -2000, width: self.view.frame.width - 80, height: self.view.frame.height - 120)
        paymentBackView.backgroundColor = UIColor.white
        self.view.addSubview(paymentBackView)
        paymentBackView.layer.borderWidth = 2
        paymentBackView.layer.borderColor = UIColor.init(red: 225 / 255.0, green: 225 / 255.0, blue: 225 / 255.0, alpha: 1.0).cgColor

        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            self.OnMenuClicked()
        }
        // Do any additional setup after loading the view.
        let assistiveTouch = AssistiveTouch(frame: CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56))
        assistiveTouch.accessibilityLabel = "Quick Menu"

        assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
        assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
        view.addSubview(assistiveTouch)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
        tapGesture.numberOfTapsRequired = 1
        assistiveTouchSOS.addGestureRecognizer(tapGesture)
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
        longPress.minimumPressDuration = 1.0
        self.assistiveTouchSOS.addGestureRecognizer(longPress)
        assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
        assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
       // view.addSubview(assistiveTouchSOS)
        

        
    }
    @objc func nearbyAction()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let vc = storyboard.instantiateViewController(withIdentifier: "WhatsNearMeMapViewController") as! WhatsNearMeMapViewController
               // vc.newsObj = newsObj
               self.present(vc, animated: true, completion: nil)
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
           if gesture.state == UIGestureRecognizerState.began {
               UIDevice.vibrate()
               print("Long Press")
               currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                          if Success{
                         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                         let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                           print("\(Address),\(LatsosLat),\(Longsoslong)")
                           MapsScheduleVC.CurrentlocationAddressString = Address
                           MapsScheduleVC.latSOS = LatsosLat
                           MapsScheduleVC.longSOS = Longsoslong
                         self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                  //self.CurrentlocationAddress.text = Address
                              }
                          }
               
           }
       }
     
       @objc func tappedSOS(sender: UIButton) {
        
           let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                   // action here
           }
                   
           SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
          
       }
    /**
     @brief To check Network reachablity
     
      @discussion This method is used to check network reachablity if network  not avalibe means shown alert
     */
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    /**
     @brief assistiveTouch method action
     @discussion This method is used to shown assistiveTouch view
     */
    @objc func tapped(sender: UIButton) {
        print("\(sender) has been touched")
        let actionController = TwitterActionController()
        actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }))
        if comVar.DateImplementation {
            actionController.addAction(Action(ActionData(title: " Buy tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
//                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
//                    self.navigationController?.pushViewController(nextViewController, animated: true)
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                           let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketNewViewController") as! BuyTicketNewViewController
                           self.navigationController?.pushViewController(nextViewController, animated: true)
                           
                           
                }))
        }
        actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
            }))
        actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }))

        actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)

            }))

        actionController.headerData = "Quick View"
        present(actionController, animated: true, completion: nil)

    }
    /**
     @discussion This method is displaying realtime routes
     */
    @objc func OnMenuClicked() {
        menuTabelView.expansionStyle = .single
        if !isMenuActive {
            if firstTimeApiCall
            {
                firstTimeApiCall = false
            self.showLoader()
            DispatchQueue.main.async {
                self.menuTabelView.removeFromSuperview()
            }
            isMenuActive = true
            UIView.animate(withDuration: 1,
                delay: 0.1,
                options: UIViewAnimationOptions.curveEaseInOut,
                animations: { () -> Void in
                    self.menuBackView.frame = CGRect.init(x: (self.view.frame.size.width / 2) - 90, y: 0, width: (self.view.frame.size.width / 2) + 90, height: self.view.frame.size.height)
                }, completion: { (finished) -> Void in
                    var paymentURL1: URL
                    if self.btnFlag == 0
                        {
                        paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=rail")!
                    }

                    else
                    {
                        paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=bus")!
                    }
              
                    Alamofire.request(paymentURL1, method: .get, encoding: URLEncoding.default)
                        .responseArray{ (response: DataResponse<[realtimeSchduleClass]>) in
                            switch response.result {
                            case .success:
                                if response.result.value != nil {
                                    let Realtimedata = response.result.value
                                    if response.result.value!.count > 0 {
                                        self.realTimeArray = Realtimedata!

                                         self.menuClickList()
                                        
                                    }
                                    else
                                    {
                                        self.hideLoader()

                                    }
                                    
                                }
                                else
                                {
                                    self.hideLoader()

                                }
                            case .failure(let error):
                                print(error)
                                self.hideLoader()

                            }
                    }
                    
                    
                    
                    
                })
        }
        else
        {
            DispatchQueue.main.async {
                self.menuTabelView.removeFromSuperview()
            }
            UIView.animate(withDuration: 1,
                           delay: 0.1,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            self.menuBackView.frame = CGRect.init(x: (self.view.frame.size.width / 2) - 90, y: 0, width: (self.view.frame.size.width / 2) + 90, height: self.view.frame.size.height)
            }, completion: { (finished) -> Void in
                self.isMenuActive = true
            self.menuClickList()
            })
                
        }
        }
        else
        {
            isMenuActive = false
            UIView.animate(withDuration: 2,
                delay: 0.1,
                options: UIViewAnimationOptions.curveEaseInOut,
                animations: { () -> Void in
                    DispatchQueue.main.async {
                    }
                    self.menuBackView.frame = CGRect.init(x: 2000, y: 0, width: (self.view.frame.size.width / 2) + 90, height: self.view.frame.size.height)
                }, completion: { (finished) -> Void in
                })
        }
    }
    func menuClickList()
    {
        self.dummyScheduleArray.addObjects(from: self.scheduleArray as! [Any])
        DispatchQueue.main.async {
            self.railBtn.frame = CGRect.init(x: 20, y: 70, width: (self.menuBackView.frame.width / 4) - 15, height: 40)
            self.railBtn.setTitle("RAIL", for: .normal)
            self.railBtn.setTitleColor(UIColor.white, for: .normal)
            if self.btnFlag == 0
            {
                self.railBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0), for: .normal)
            }
            else
            {
                self.railBtn.setBackgroundColor(UIColor.init(red: 221.0 / 255.0, green: 85.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0), for: .normal)
            }
            self.railBtn.tag = 0
            self.railBtn.titleLabel?.font = UIFont.setTarcRegular(size: 13.0)
            self.railBtn.addTarget(self, action: #selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
            self.menuBackView.addSubview(self.railBtn)
            self.busBtn.frame = CGRect.init(x: self.railBtn.frame.size.width + self.railBtn.frame.origin.x + 10, y: 70, width: (self.menuBackView.frame.width / 4) - 15, height: 40)
            self.busBtn.titleLabel?.font = UIFont.setTarcRegular(size: 13.0)
            self.busBtn.setTitle("BUS", for: .normal)
            self.busBtn.setTitleColor(UIColor.white, for: .normal)
            if self.btnFlag == 1
            {
                self.busBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0), for: .normal)
            }
            else
            {
                self.busBtn.setBackgroundColor(UIColor.init(red: 221.0 / 255.0, green: 85.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0), for: .normal)
            }
            self.busBtn.tag = 1
            self.busBtn.addTarget(self, action: #selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
            self.menuBackView.addSubview(self.busBtn)
            self.brtBtn.frame = CGRect.init(x: self.busBtn.frame.size.width + self.busBtn.frame.origin.x + 10, y: 70, width: (self.menuBackView.frame.width / 4) - 15, height: 40)
            self.brtBtn.titleLabel?.font = UIFont.setTarcRegular(size: 13.0)
            self.brtBtn.setTitle("BRT", for: .normal)
            self.brtBtn.setTitleColor(UIColor.white, for: .normal)
            if self.btnFlag == 2
            {
                self.brtBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0), for: .normal)
            }
            else
            {
                self.brtBtn.setBackgroundColor(UIColor.init(red: 221.0 / 255.0, green: 85.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0), for: .normal)
            }
            self.brtBtn.tag = 2
            self.brtBtn.addTarget(self, action: #selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
            self.menuBackView.addSubview(self.brtBtn)
            self.trolleyBtn.frame = CGRect.init(x: self.brtBtn.frame.size.width + self.brtBtn.frame.origin.x + 10, y: 70, width: (self.menuBackView.frame.width / 4) - 15, height: 40)
            self.trolleyBtn.titleLabel?.font = UIFont.setTarcRegular(size: 12.0)
            
            self.trolleyBtn.setTitle("TROLLEY", for: .normal)
            self.trolleyBtn.setTitleColor(UIColor.white, for: .normal)
            if self.btnFlag == 3
            {
                self.trolleyBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0), for: .normal)
            }
            else
            {
                self.trolleyBtn.setBackgroundColor(UIColor.init(red: 221.0 / 255.0, green: 85.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0), for: .normal)
            }
            self.trolleyBtn.tag = 3
            self.trolleyBtn.addTarget(self, action: #selector(self.sortaBtnAction), for: UIControlEvents.touchUpInside)
            self.menuBackView.addSubview(self.trolleyBtn)
            self.menuTabelView = ExpandableTableView()
            self.menuTabelView.expandableDelegate = self
            self.menuTabelView.expansionStyle = .single
            self.menuTabelView.animation = .automatic
            self.menuTabelView.register(UINib(nibName: "NormalCell", bundle: nil), forCellReuseIdentifier: NormalCell.ID)
            self.menuTabelView.register(UINib(nibName: "ExpandedCell", bundle: nil), forCellReuseIdentifier: ExpandedCell.ID)
            self.menuTabelView.register(UINib(nibName: "ExpandableCell", bundle: nil), forCellReuseIdentifier: ExpandableCell2.ID)
            self.menuTabelView.frame = CGRect.init(x: 0, y: 0, width: self.menuBackView.frame.width, height: self.menuBackView.frame.height)
            self.menuTabelView.allowsMultipleSelection = true
            self.menuTabelView.backgroundColor = UIColor.white
            self.menuBackView.addSubview(self.menuTabelView)
        //    self.menuBackView.isHidden = true
            self.hideLoader()
        }
    }
    
    /**
     @brief  This method is mapit action
     @discussion To passed route id in api and shown that particular route polyline and real time buses
     */
    @objc func sortaBtnAction(sender: UIButton!) {
        isSorta = true
        var paymentURL1: URL
        print(sender.tag)
        self.btnFlag = sender.tag
        if self.btnFlag == 0
            {
            self.railBtn.setBackgroundColor(UIColor.init(red: 0, green: 139.0 / 255.0, blue: 198.0 / 255.0, alpha: 1.0), for: .normal)
            self.busBtn.setBackgroundColor(UIColor.init(red: 221.0 / 255.0, green: 85.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0), for: .normal)
            self.brtBtn.setBackgroundColor(UIColor.init(red: 221.0 / 255.0, green: 85.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0), for: .normal)
            self.trolleyBtn.setBackgroundColor(UIColor.init(red: 221.0 / 255.0, green: 85.0 / 255.0, blue: 85.0 / 255.0, alpha: 1.0), for: .normal)
            paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=rail")!
        }

        else
        {
            paymentURL1 = URL(string: "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetStandardBusLines?filter=bus")!
        }
//        var request1 = URLRequest(url: paymentURL1)
//        request1.httpMethod = "GET"
//        URLSession.shared.dataTask(with: request1) { (data, response, error) -> Void in
//            do
//            {
//                if data != nil
//                    {
//                    self.dummyScheduleArray.removeAllObjects()
//                    self.scheduleArray.removeAllObjects()
//
//                    self.scheduleArray = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSMutableArray)!
//
//                    self.dummyScheduleArray.addObjects(from: self.scheduleArray as! [Any])
//                    DispatchQueue.main.async {
//                        self.menuTabelView.reloadData()
//                    }
//
//                }
//            } catch {
//
//            }
//
//        }.resume();
        
        Alamofire.request(paymentURL1, method: .get, encoding: URLEncoding.default)
            .responseArray{ (response: DataResponse<[realtimeSchduleClass]>) in
                switch response.result {
                case .success:
                    if response.result.value != nil {
                        let Realtimedata = response.result.value
                        if response.result.value!.count > 0 {
                            self.realTimeArray = Realtimedata!

                            
                            
                        }
                        
                    }
                    
                case .failure(let error):
                    print(error)
                }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 60;

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stepArray.count + 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        tableView.separatorColor = UIColor.clear;
        cell.separatorInset = .zero
        cell.textLabel?.text = selectProfileArray.object(at: indexPath.row) as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if(tableView == selectProfileTableView) {
            selectedProfileArray.remove(selectProfileArray.object(at: indexPath.row))
        }

        print(selectedProfileArray)
    }

    @objc func OnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @objc func openSchedules(sender: UIButton!) {
       // let valueDic: NSDictionary = self.scheduleArray[sender.tag % 10000] as! NSDictionary
        //        cell.textLabel?.text = "\(realTimeArray[indexPath.row].RouteID!). \(realTimeArray[indexPath.row].LineName!)"

        RealTimeSchedule.SelectedSchedule = "\(realTimeArray[sender.tag % 10000].RouteID!)"
        RealTimeSchedule.SelectedScheduleRoute = "\(realTimeArray[sender.tag % 10000].RouteID!) - \(realTimeArray[sender.tag % 10000].LineName!)"
        let homeView = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        present(homeView, animated: true, completion: nil)

    }
    @objc func openPassportAppLink(sender: UIButton!) {
//(realTimeArray[sender.tag % 10000].RouteID!)
      //  let valueDic: NSMutableDictionary = self.scheduleArray[sender.tag % 1000] as! NSMutableDictionary
        routeIDArray.add((realTimeArray[sender.tag % 1000].RouteID!))
        var routeIDString = realTimeArray[sender.tag % 1000].RouteID!
        print(routeIDString)
       
        routeIDString = routeIDString.replacingOccurrences(of: " ", with: "%20")

        if let url = URL(string: "\(APIUrl.ZIGTARCBASEAPI)img/pdf/\(routeIDString).pdf") {
            let vc: SFSafariViewController
            if #available(iOS 11.0, *) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = false
                vc = SFSafariViewController(url: url, configuration: config)
            } else {
                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
            }
            vc.delegate = self as? SFSafariViewControllerDelegate
            present(vc, animated: true)
        }


    }

    func GetRealTimeSchedule(RouteID: String) {
  DispatchQueue.main.async {
      let allAnnotations = self.mapView.annotations
      self.mapView.removeAnnotations(allAnnotations)
        }
        
        let RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeBusesforRouteID?RouteID=\(RouteID)"
        print(RouteUrl)
        Alamofire.request(RouteUrl, method: .get, encoding: URLEncoding.default)
            .responseArray { (response: DataResponse<[RealTimeScheduleBus]>) in
                switch response.result {
                case .success:
                    if response.result.value != nil {
                         DispatchQueue.main.async {
                        let Realtimedata = response.result.value
                        if response.result.value!.count > 0 {

                            for RealtimeDataList in Realtimedata! {
                                if RealtimeDataList.vehicle! {
                                    let nextBustop = RealtimeDataList.nextstops![0].stopName ?? " "

                                    let RealTimeBus = MKPointAnnotation()
                                    RealTimeBus.title = "Trip ID: \((RealtimeDataList.tripID)!)"
                                    RealTimeBus.subtitle = "Next Stop: \(nextBustop)"
                                    RealTimeBus.coordinate = CLLocationCoordinate2D(latitude: RealtimeDataList.lat!, longitude: RealtimeDataList.lng!)
                                    self.mapView.addAnnotation(RealTimeBus)
                                }

                            }
                            }
                        }

                    }

                case .failure(let error):
                    self.hideLoader()
                    self.showErrorDialogBox(viewController: self)

                    print(error)
                }
        }


    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRender = MKPolylineRenderer(overlay: overlay)
        polylineRender.strokeColor = color.hexStringToUIColor(hex: "#\(routeColorGlobal)")
        polylineRender.lineWidth = 5
        return polylineRender

    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let reuseId = "Tarc"
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.image = UIImage(named: "bus_Transit")
            anView!.canShowCallout = true
            
        }
        else {
            anView!.annotation = annotation
        }
        let subtitleView = UILabel()
        subtitleView.font = subtitleView.font.withSize(13)
        subtitleView.numberOfLines = 0
        subtitleView.text = annotation.subtitle!
        anView!.detailCalloutAccessoryView = subtitleView
        return anView
    }
}

extension realtimeSchduleViewController: ExpandableDelegate {
    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCellsForRowAt indexPath: IndexPath) -> [UITableViewCell]? {
        let cell1 = menuTabelView.dequeueReusableCell(withIdentifier: ExpandedCell.ID) as! ExpandedCell


        let RealTimeButton = UIButton()
        let lineLable = UILabel()
        let buyTicketBtn = UIButton()
        RealTimeButton.frame = CGRect.init(x: 0, y: 0, width: (self.menuTabelView.frame.size.width / 2) - 1, height: 44)
        RealTimeButton.tag = indexPath.row
        
        RealTimeButton.setTitle("Real - Time", for: .normal)
        RealTimeButton.setTitleColor(UIColor.white, for: .normal)
        RealTimeButton.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
        RealTimeButton.titleLabel?.font = UIFont.setTarcRegular(size: 17)

        RealTimeButton.addTarget(self, action: #selector(self.DrawPolyline), for: .touchUpInside)
        cell1.contentView.addSubview(RealTimeButton)

        lineLable.frame = CGRect.init(x: (self.menuTabelView.frame.size.width / 2) - 1, y: 0, width: 1, height: 44)
        lineLable.backgroundColor = UIColor.white
        cell1.contentView.addSubview(lineLable)
        buyTicketBtn.tag = 1000 + indexPath.row
        buyTicketBtn.frame = CGRect.init(x: (self.menuTabelView.frame.size.width / 2), y: 0, width: (self.menuTabelView.frame.size.width / 2) - 1, height: 44)
        buyTicketBtn.setTitle("PDF", for: .normal)
        buyTicketBtn.titleLabel?.font = UIFont.setTarcRegular(size: 17)
        buyTicketBtn.addTarget(self, action: #selector(self.openSchedules), for: .touchUpInside)
        buyTicketBtn.tag = 10000 + indexPath.row
        buyTicketBtn.backgroundColor = UIColor(red: 37 / 255.0, green: 114 / 255.0, blue: 176 / 255.0, alpha: 1.0)
        buyTicketBtn.setTitleColor(UIColor.white, for: .normal)
        cell1.contentView.addSubview(buyTicketBtn)
        return [cell1]
    }



    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if tableView == selectProfileTableView {
            print("Select profile table view")
            if !selectedProfileArray.contains(selectProfileArray.object(at: indexPath.row))
                {
                selectedProfileArray.add(selectProfileArray.object(at: indexPath.row))
            }
            else
            {

            }
            print(selectedProfileArray)


        }
    }


    @objc func DrawPolyline(sender: UIButton!){
        let routeno = realTimeArray[sender.tag].RouteID
        print(sender.tag)
        self.routeIDString = routeno!
        pdfButton.isHidden = false
       let url = URL(string: "\(APIUrl.ZIGTARCBASEAPI)img/pdf/\(routeIDString).pdf")!
       let request = URLRequest(url: url)

        let task = URLSession.shared.dataTask(with: request) {data, response, error in

           if let httpResponse = response as? HTTPURLResponse {
               print("statusCode: \(httpResponse.statusCode)")
            if httpResponse.statusCode == 404
            {
                 DispatchQueue.main.async {
                self.pdfButton.isHidden = true
                }
            }
           }

       }
       task.resume()
        
        

        self.showLoader()
        routeDetails.removeAllObjects()
        routeIDArray.removeAllObjects()
        self.coordinateArray = []
        
        let allAnnotations = self.mapView.annotations
        DispatchQueue.main.async {
        self.mapView.removeAnnotations(allAnnotations)
            self.mapView.removeOverlays(self.mapView.overlays)
        }
        
        
        let polylineURL = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetTripListforBuslines?RouteID=\((routeno)!)"
        Alamofire.request(polylineURL).responseArray { (response: DataResponse<[mapsandSchudleList]>) in
                   
                   switch response.result {
                   case .success:
                    if response.result.value != nil{
                   // self.GetRealTimeSchedule(RouteID: self.routeIDString)
                        self.LineDetails = response.result.value!
                       
                    
                       if response.result.value!.count > 0 {
                        
                        self.GetRealTimeSchedule(RouteID: routeno!)
                        
                        for LineDetailsArray in self.LineDetails {
                            if LineDetailsArray.TripId != 0 && LineDetailsArray.TripId != nil {
                            let tripValueID: Int = LineDetailsArray.TripId!
                            let RouteColor: String = LineDetailsArray.RouteColor!
                            self.routeColorGlobal = RouteColor
                            self.getmapSchduleAPIFirst(tripValueID: tripValueID, completion: { (coordiante, response, checkbool) in

                                if checkbool == true
                                    {
                                    let apolyline = MKPolyline(coordinates: coordiante, count: coordiante.count)
                                    self.mapView.add(apolyline)

                                    if let first = self.mapView.overlays.first {
                                        let rect = self.mapView.overlays.reduce(first.boundingMapRect, { MKMapRectUnion($0, $1.boundingMapRect) })
                                        self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                                        
                                       // self.hideLoader()
                                    }
                                }
                                else{
                                                                           // self.hideLoader()
                                }
                            })

                            
                           
                        }
                            else{
                                print("noTrip")
                                                                      //  self.hideLoader()
                            }
                        }
                        
                    }
                    
                    }
                    else{
                        
                    }
                                                            self.hideLoader()
                    case .failure(let error):
                                   print(error)
                                   
                                   self.view.makeToast("No trips available at the moment! Please try again later!")
                    
                                   self.hideLoader()
                                   self.OnMenuClicked()
                               }
        }
        
        DispatchQueue.main.async {
                      if self.realTimeMapTimer == nil
                          {
                          self.realTimeMapTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
                      }
                  }
        self.isMenuActive = false
        UIView.animate(withDuration: 1,
            delay: 0.1,
            options: UIViewAnimationOptions.curveEaseInOut,
            animations: { () -> Void in
                self.menuBackView.frame = CGRect.init(x: 2000, y: 0, width: (self.view.frame.size.width / 2) + 90, height: self.view.frame.size.height)
            }, completion: { (finished) -> Void in


            })
    }
    
    
    
    @IBAction func MyTarcCard(_ sender: Any) {

        print("https://tripplan.ridetarc.org/img/pdf/\(routeIDString).pdf")
        if let url = URL(string: "\(APIUrl.ZIGTARCBASEAPI)img/pdf/\(routeIDString).pdf") {
            let vc: SFSafariViewController
            
            if #available(iOS 11.0, *) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = false
                vc = SFSafariViewController(url: url, configuration: config)
            } else {
                vc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
            }
            vc.delegate = self as? SFSafariViewControllerDelegate
            present(vc, animated: true)
        }
        
        
    }

    @objc func runTimedCode(){

        let allAnnotations = self.mapView.annotations
       
        let RouteUrl = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Trip/GetRealtimeBusesforRouteID?RouteID=\(self.routeIDString)"
        print(RouteUrl)
        Alamofire.request(RouteUrl, method: .get, encoding: URLEncoding.default)
            .responseArray { (response: DataResponse<[RealTimeScheduleBus]>) in
                switch response.result {
                case .success:
                    if response.result.value != nil {
                        let Realtimedata = response.result.value
                        if response.result.value!.count > 0 {
                           // self.mapView.removeAnnotation(allAnnotations)
                             self.mapView.removeAnnotations(allAnnotations)
                            for RealtimeDataList in Realtimedata! {
                                if RealtimeDataList.vehicle! {
                                    var latestDataWithoutpassedData =  [RealTimeNextStopSchudle]()
                                    for NewaDatanextstop in RealtimeDataList.nextstops! {
                                        
                                        if NewaDatanextstop.eTA != "Passed"{
                                            
                                          let latestDataWithoutpassed = NewaDatanextstop
                                            
                                            latestDataWithoutpassedData.append(latestDataWithoutpassed)
                                            
                                        }
                                    }
                                  //  print(latestDataWithoutpassedData.first)
                                    
                                    if latestDataWithoutpassedData.count > 0 {
                                        
                                        let nextBustop = latestDataWithoutpassedData.first?.stopName ?? " "
                                    let NextStopArrival = latestDataWithoutpassedData.first?.arrivalTime ?? " "
                                         let NextStopETA = latestDataWithoutpassedData.first?.eTA ?? " "
                                       // let NextStopMiles = latestDataWithoutpassedData.first?.stopDistance ?? " "

                                    let RealTimeBus = MKPointAnnotation()
                                    RealTimeBus.title = "#\(self.routeIDString)"
                                    RealTimeBus.subtitle = "Trip ID: \((RealtimeDataList.tripID)!)\nNext Stop: \(nextBustop)\nNext Stop Arrival: \(NextStopArrival)\nNext Stop ETA: \(NextStopETA)"
                                    RealTimeBus.coordinate = CLLocationCoordinate2D(latitude: RealtimeDataList.lat!, longitude: RealtimeDataList.lng!)
                                    
                                    self.mapView.addAnnotation(RealTimeBus)
                                }
                                    else{
                                        let RealTimeBus = MKPointAnnotation()
                                       RealTimeBus.title = "#\(self.routeIDString)"
                                        RealTimeBus.subtitle = "Trip ID: \((RealtimeDataList.tripID)!)"
                                        RealTimeBus.coordinate = CLLocationCoordinate2D(latitude: RealtimeDataList.lat!, longitude: RealtimeDataList.lng!)
                                        
                                        self.mapView.addAnnotation(RealTimeBus)
                                    }
                                   
                                }
 self.hideLoader()
                            }

                        }
                        
                       self.hideLoader()

                    }
                    else{
                         self.hideLoader()
                    }
                case .failure(let error):
                    self.hideLoader()
                    self.showErrorDialogBox(viewController: self)

                    print(error)
                }
        }


    }
    
    //new method
    func getmapSchduleAPIFirst(tripValueID: Int, completion: @escaping ([CLLocationCoordinate2D], DataResponse<MapsTripRootClass>, Bool) -> ()) {
        //
        let url2: String
        url2 = "\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Stop/GetRealtimetripDetails"
        let parameters2: Parameters = [
            "TripID": tripValueID
        ]
        

        Alamofire.request(url2, method: .get, parameters: parameters2, encoding: URLEncoding.default)
            .responseObject { (response: DataResponse<MapsTripRootClass>) in
                switch response.result {
                case .success:
                    print(response)
                    if response.result.value?.shapes != nil
                        {
                        for shapesValue in (response.result.value?.shapes)!
                        {


                            let latit = Double(shapesValue.lat!)
                            let longit = Double(shapesValue.longField!)
                            // path.addLatitude(latit!, longitude: longit!)
                            self.coordinateArray.append(CLLocationCoordinate2D(latitude: latit!, longitude: longit!))

                            //print(self.coordinateArray)
                        }
                        print(self.coordinateArray)
                        completion(self.coordinateArray, response, true)
                        self.coordinateArray = []

                        print("**************************************")

                    }
                case .failure(let error):
                    print(error)
                    self.hideLoader()
                    self.showErrorDialogBox(viewController: self)
                }
        }

    }


    func showErrorDialogBox(viewController: UIViewController) {
        let alertViewController = NYAlertViewController()
        alertViewController.title = "Error"
        alertViewController.message = "Server Error! Please try again later!"
        // Customize appearance as desired
        alertViewController.buttonCornerRadius = 20.0
        alertViewController.view.tintColor = viewController.view.tintColor
        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
        alertViewController.swipeDismissalGestureEnabled = true
        alertViewController.backgroundTapDismissalGestureEnabled = true
        alertViewController.buttonColor = UIColor.red
        // Add alert actions


        let cancelAction = NYAlertAction(
            title: "OK",
            style: .cancel,
            handler: { (action: NYAlertAction!) -> Void in

                viewController.dismiss(animated: true, completion: nil)
            }
        )

        alertViewController.addAction(cancelAction)
        // Present the alert view controller
        viewController.present(alertViewController, animated: true, completion: nil)

    }
    func expandableTableView(_ expandableTableView: ExpandableTableView, heightsForExpandedRowAt indexPath: IndexPath) -> [CGFloat]? {

        return [44.0]

    }

    func numberOfSections(in tableView: ExpandableTableView) -> Int {
        return 1
    }

    func expandableTableView(_ expandableTableView: ExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return self.realTimeArray.count
    }

    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectRowAt indexPath: IndexPath) {
        print("didSelectRow:\(indexPath)")


    }

    func expandableTableView(_ expandableTableView: ExpandableTableView, didSelectExpandedRowAt indexPath: IndexPath) {

    }

    func expandableTableView(_ expandableTableView: ExpandableTableView, expandedCell: UITableViewCell, didSelectExpandedRowAt indexPath: IndexPath) {
        if let cell = expandedCell as? ExpandedCell {

            cell.accessoryType = cell.isSelected ? .checkmark : .none
        }
    }

    func expandableTableView(_ expandableTableView: ExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = expandableTableView.dequeueReusableCell(withIdentifier: ExpandableCell2.ID) else { return cellEx }
        print("\(self.scheduleArray)")
//        let valueDic: NSDictionary = self.scheduleArray[indexPath.row] as! NSDictionary
//route_list[indexPath.row].Title!
        cell.separatorInset = .zero
       // cell.textLabel?.text = "\(valueDic.value(forKey: "RouteID") as! String). \(valueDic.value(forKey: "LineName") as! String)"
        cell.textLabel?.text = "\(realTimeArray[indexPath.row].RouteID!). \(realTimeArray[indexPath.row].LineName!)"

        cell.textLabel?.font = UIFont.setTarcRegular(size: 13.0)
        return cell
    }




    func expandableTableView(_ expandableTableView: ExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 44
    }

    func expandableTableView(_ expandableTableView: UITableView, didHighlightRowAt indexPath: IndexPath) {

    }

    func expandableTableView(_ expandableTableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func closebar() {

    }
}
