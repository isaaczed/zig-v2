//
//	ScheduleRoute.swift


import Foundation 
import ObjectMapper


class ScheduleRoute : NSObject, NSCoding, Mappable{

	var lineName : String?
	var routeID : String?
	var routeName : String?


	class func newInstance(map: Map) -> Mappable?{
		return ScheduleRoute()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		lineName <- map["LineName"]
		routeID <- map["RouteID"]
		routeName <- map["RouteName"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         lineName = aDecoder.decodeObject(forKey: "LineName") as? String
         routeID = aDecoder.decodeObject(forKey: "RouteID") as? String
         routeName = aDecoder.decodeObject(forKey: "RouteName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if lineName != nil{
			aCoder.encode(lineName, forKey: "LineName")
		}
		if routeID != nil{
			aCoder.encode(routeID, forKey: "RouteID")
		}
		if routeName != nil{
			aCoder.encode(routeName, forKey: "RouteName")
		}

	}

}