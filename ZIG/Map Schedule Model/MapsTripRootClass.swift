//
//	MapsTripRootClass.swift


import Foundation 
import ObjectMapper


class MapsTripRootClass : NSObject, NSCoding, Mappable{

	var arrivalTime : String?
	var busStops : [MapsTripBusStop]?
	var departureTime : String?
	var distance : AnyObject?
	var nextStops : [AnyObject]?
	var routeColor : String?
	var routeNumber : AnyObject?
	var routeTextColor : AnyObject?
	var shapeId : AnyObject?
	var shapes : [MapsTripShape]?
	var tripId : Int?
	var tripTitle : AnyObject?


	class func newInstance(map: Map) -> Mappable?{
		return MapsTripRootClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		arrivalTime <- map["ArrivalTime"]
		busStops <- map["BusStops"]
		departureTime <- map["DepartureTime"]
		distance <- map["Distance"]
		nextStops <- map["NextStops"]
		routeColor <- map["RouteColor"]
		routeNumber <- map["RouteNumber"]
		routeTextColor <- map["RouteTextColor"]
		shapeId <- map["ShapeId"]
		shapes <- map["Shapes"]
		tripId <- map["TripId"]
		tripTitle <- map["TripTitle"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         arrivalTime = aDecoder.decodeObject(forKey: "ArrivalTime") as? String
         busStops = aDecoder.decodeObject(forKey: "BusStops") as? [MapsTripBusStop]
         departureTime = aDecoder.decodeObject(forKey: "DepartureTime") as? String
         distance = aDecoder.decodeObject(forKey: "Distance") as? AnyObject
         nextStops = aDecoder.decodeObject(forKey: "NextStops") as? [AnyObject]
         routeColor = aDecoder.decodeObject(forKey: "RouteColor") as? String
         routeNumber = aDecoder.decodeObject(forKey: "RouteNumber") as? AnyObject
         routeTextColor = aDecoder.decodeObject(forKey: "RouteTextColor") as? AnyObject
         shapeId = aDecoder.decodeObject(forKey: "ShapeId") as? AnyObject
         shapes = aDecoder.decodeObject(forKey: "Shapes") as? [MapsTripShape]
         tripId = aDecoder.decodeObject(forKey: "TripId") as? Int
         tripTitle = aDecoder.decodeObject(forKey: "TripTitle") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if arrivalTime != nil{
			aCoder.encode(arrivalTime, forKey: "ArrivalTime")
		}
		if busStops != nil{
			aCoder.encode(busStops, forKey: "BusStops")
		}
		if departureTime != nil{
			aCoder.encode(departureTime, forKey: "DepartureTime")
		}
		if distance != nil{
			aCoder.encode(distance, forKey: "Distance")
		}
		if nextStops != nil{
			aCoder.encode(nextStops, forKey: "NextStops")
		}
		if routeColor != nil{
			aCoder.encode(routeColor, forKey: "RouteColor")
		}
		if routeNumber != nil{
			aCoder.encode(routeNumber, forKey: "RouteNumber")
		}
		if routeTextColor != nil{
			aCoder.encode(routeTextColor, forKey: "RouteTextColor")
		}
		if shapeId != nil{
			aCoder.encode(shapeId, forKey: "ShapeId")
		}
		if shapes != nil{
			aCoder.encode(shapes, forKey: "Shapes")
		}
		if tripId != nil{
			aCoder.encode(tripId, forKey: "TripId")
		}
		if tripTitle != nil{
			aCoder.encode(tripTitle, forKey: "TripTitle")
		}

	}

}
