//
//  RealTimeScheduleBus.swift
//  ZIG
//
//  Created by Isaac on 11/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper

class realtimeSchduleClass: Mappable {

    var LineName : String?
    var RouteID : String?
    var RouteName : String?
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        LineName <- map["LineName"]
        RouteID <- map["RouteID"]
        RouteName <- map["RouteName"]
     
    }
    
    
}


