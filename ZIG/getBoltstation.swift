//
//  getLimestation.swift
//  ZIG
//
//  Created by VC on 21/01/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import Foundation
import ObjectMapper

class getBoltstation: Mappable {
    var lastupdated:Int?
    var total:Int?
    var code:Int?
    var datalistfromapi: [Bolt_list]?
    required init?(map: Map) {
        
    
    }
    
     func mapping(map: Map) {
        lastupdated <- map["last_updated"]
        total <- map["ttl"]
        datalistfromapi <- map["data.bikes"]
        code <- map["code"]
        
        
    }
    
    
    
}
//class data_list: Mappable {
//    var bikes_list: [Bikelist]?
//    required init?(map: Map) {
//
//    }
//
//     func mapping(map: Map) {
//        bikes_list <- map["bikes"]
//
//    }
//
//
//}
class Bolt_list: Mappable {
    var bike_id:String?
    var bike_lat:String?
    var bike_lon:String?
    var bike_is_reserved:Int?
    var bike_is_disabled:Int?
   
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        bike_id <- map["bike_id"]
        bike_lat <- map["lat"]
        bike_lon <- map["lon"]
        bike_is_reserved <- map["is_reserved"]
        bike_is_disabled <- map["is_disabled"]
        
    }
    
    
}
