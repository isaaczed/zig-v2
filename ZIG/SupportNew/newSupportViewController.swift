//
//  newSupportViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 16/04/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class newSupportViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var tableView = UITableView()
var TitleTransaction = NSString()

var firstTime = Bool()
    var chatArray = [[String: String]]()
  var  isSubmitButton = Bool()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        isSubmitButton = false
        
        analytics.GetPageHitCount(PageName: "Support-Main")
            NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
            offline.updateUserInterface(withoutLogin: false)
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
            self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
            self.title = "Support"
            
            
            let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                             style: UIBarButtonItemStyle.done ,
                                             target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

             self.navigationItem.leftBarButtonItem = backButton
        
        
        
        let now = Date()
          let formatter = DateFormatter()
          formatter.timeZone = TimeZone.current
          formatter.dateFormat = "hh:mm:aa"
          let dateString = formatter.string(from: now)
        print(dateString)
//        [
//            "Message" : "Hey 👋🏻👋🏻👋🏻",
//            "type" : "Sender-First",
//            "Date":dateString
//        ], Hello, and welcome to our tech support.
        chatArray = [
            [
                "Message" : "Hello, and welcome to our tech support.",
                "type" : "Sender",
                 "Date":dateString
            ],
                    [
                        "Message" : "What can we help you with today ?\nPlease select a type of issue below:",
                        "type" : "Sender",
                         "Date":dateString
                    ],
                    [
                        "Message" : "",
                        "type" : "Sender-Button"
                    ]
                ]
        
        firstTime = true
        tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.white
        tableView.delegate = self
        tableView.dataSource = self
        self.view .addSubview(tableView)
        tableView.separatorStyle = .none


        let viewsDict:[String:Any] = ["tableView":tableView as Any
                             
        ]
                  
                  NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-0-[tableView]-0-|",
                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                              metrics: nil,
                                                                              views: viewsDict))
        
        
   
     
        
                  
                  NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[tableView]-0-|",
                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                              metrics: nil,
                                                                              views: viewsDict))
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifier"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationIdentifierBack"), object: nil)

         NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationBack(notification:)), name: Notification.Name("NotificationIdentifierBack"), object: nil)
        //

    }

    @objc func methodOfReceivedNotificationBack(notification: Notification) {
        let questiondicts = ["Message" : "What can we help you with today ?\nPlease select a type of issue below:", "type" : "Sender"] as NSDictionary
                      chatArray.append(questiondicts as! [String : String])

                      let questiondict = ["Message" : "", "type" : "Sender-Button"] as NSDictionary
                                               chatArray.append(questiondict as! [String : String])
                      tableView.reloadData()
                      scrollToBottom()
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        let questiondicts = ["Message" : "Thank you!\nYour message has been successfully sent.\nWe will be in touch shortly\n\nPlease choose the another problem", "type" : "Sender"] as NSDictionary
               chatArray.append(questiondicts as! [String : String])

               let questiondict = ["Message" : "", "type" : "Sender-Button"] as NSDictionary
                                        chatArray.append(questiondict as! [String : String])
               tableView.reloadData()
               scrollToBottom()
    }

    @objc func OnBackClicked() {
                 self.navigationController?.popViewController(animated: true)
     }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return chatArray.count

     
        
      }
  
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.chatArray.count-1, section: 0)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let Dict  =  chatArray[indexPath.row] as NSDictionary
             let typeString = Dict.value(forKey: "type") as! NSString
        switch typeString {
             case "Sender-First":
            return 65
               case "Sender":
                return UITableViewAutomaticDimension
            case "textView-Button":
                 if UIScreen.main.sizeType == .iPadPro129{
                    return 85

                 }
                    else if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
                    return 100

                                   }else if UIScreen.main.sizeType == .iPadPro105{

                    return 100

                                   }
            else
                 {
                    return 120

            }
                 case "Sender-Button":
                    
                if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
                             
                                      return  150
                                   }else if UIScreen.main.sizeType == .iPadPro105{
                              
                                      return  180

                                   }
                  else  if UIScreen.main.sizeType == .iPadPro129{
                        return 180

                    }
                    else
                    {
                        return 180

                    }

            case "ticket-Button":
                
                if UIScreen.main.sizeType == .iPadAir2andiPadMini4{

                                             return  120

                           }else if UIScreen.main.sizeType == .iPadPro105{
                                             return  140
                           }
                  if UIScreen.main.sizeType == .iPadPro129{
                              return 140
            }
            else
                  {
                    return 130

            }
             case "ticketResponse-Button":

                  return 60
           
                      case "Receiver":
                        return UITableViewAutomaticDimension

                        default:
        return 0
            
        }
    }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

  
        let Dict  =  chatArray[indexPath.row] as NSDictionary
        let typeString = Dict.value(forKey: "type") as! NSString
        if indexPath.row == chatArray.count - 1
        {
            isSubmitButton = true
        }
        else
        {
            isSubmitButton = false

        }
        switch typeString {
            case "Sender-First":
                     let MessageString = Dict.value(forKey: "Message") as! NSString

                      let cell = FirstCellTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                     cell.selectionStyle = UITableViewCell.SelectionStyle.none
                     cell.backgroundColor = UIColor.white
                     cell.descriptionLabel?.text = MessageString as String
                     cell.descriptionLabel?.numberOfLines = 0
                     cell.timeLabel.text = ""
                         return cell
          case "Sender":
            let MessageString = Dict.value(forKey: "Message") as! NSString

             let cell = SenderTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.backgroundColor = UIColor.white
            cell.descriptionLabel?.text = MessageString as String
            cell.descriptionLabel?.numberOfLines = 0
            cell.timeLabel.text = ""
                return cell
        case "textView-Button":
            let MessageString = Dict.value(forKey: "Message") as! NSString

                     let cell = TextViewSupportTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    cell.backgroundColor = UIColor.white
                    cell.descriptionLabel?.text = MessageString as String
                        return cell
            
          case "Sender-Button":
                let cell = questionTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                   cell.backgroundColor = UIColor.clear
              
               
                   cell.firstButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                   cell.secondButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                   cell.thirdButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                   cell.fourthButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                cell.fifthButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                cell.sixthButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                cell.seventButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                cell.eightButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
                cell.ninthButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
//                cell.tenthButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
//                cell.eleventhButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
//                cell.twelveButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
//                cell.thirteenButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
//                cell.fourteenButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
//                cell.fifteenButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)
//                cell.sixteenButton.addTarget(self, action: #selector(questionAction), for: .touchUpInside)

                
                if isSubmitButton == true
                {
                    cell.isUserInteractionEnabled = true

                }
                else
                {
                    cell.isUserInteractionEnabled = false
                }
                
                   return cell
            case "ticket-Button":
                           let cell = TransactionTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                               cell.selectionStyle = UITableViewCell.SelectionStyle.none
                              cell.backgroundColor = UIColor.clear
                         cell.setupConstraints(transBool: false)

                          
                              cell.firstButton.addTarget(self, action: #selector(tranactionAction), for: .touchUpInside)
                              cell.secondButton.addTarget(self, action: #selector(tranactionAction), for: .touchUpInside)
                              cell.thirdButton.addTarget(self, action: #selector(tranactionAction), for: .touchUpInside)
                              cell.fourthButton.addTarget(self, action: #selector(tranactionAction), for: .touchUpInside)
                           cell.fifthButton.addTarget(self, action: #selector(tranactionAction), for: .touchUpInside)
                           cell.sixthButton.addTarget(self, action: #selector(tranactionAction), for: .touchUpInside)

                       if isSubmitButton == true
                                      {
                                          cell.isUserInteractionEnabled = true

                                      }
                                      else
                                      {
                                          cell.isUserInteractionEnabled = false
                                      }

                              return cell
            case "ticketResponse-Button":
                            let cell = TransactionTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                               cell.backgroundColor = UIColor.clear
                            cell.setupConstraints(transBool: true)
                            cell.firstButton.setTitle("Yes, Please", for: .normal)
                           cell.secondButton.setTitle("No, Thanks", for: .normal)

                               cell.firstButton.addTarget(self, action: #selector(tranactionOkay), for: .touchUpInside)
                               cell.secondButton.addTarget(self, action: #selector(tranactionNotOkay), for: .touchUpInside)
                            cell.thirdButton.isHidden = true
                            cell.fourthButton.isHidden = true
                            cell.fifthButton.isHidden = true
                            cell.sixthButton.isHidden = true

                        if isSubmitButton == true
                                                             {
                                                                 cell.isUserInteractionEnabled = true

                                                             }
                                                             else
                                                             {
                                                                 cell.isUserInteractionEnabled = false
                                                             }

                               return cell
            
            
            case "Receiver":
                              let cell = ResponseTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                              let MessageString = Dict.value(forKey: "Message") as! NSString

                                                      cell.selectionStyle = UITableViewCell.SelectionStyle.none
                                                     cell.backgroundColor = UIColor.clear
                              cell.descriptionLabel?.text = MessageString as String
                              cell.timeLabel.text = ""

                                                     cell.descriptionLabel?.numberOfLines = 0
                                                     return cell
          default:
              let cell = ResponseTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                               cell.selectionStyle = UITableViewCell.SelectionStyle.none
                              cell.backgroundColor = UIColor.clear
                              cell.descriptionLabel?.text = "Transit Authority of River City (TARC), the public transportation system serving the Greater Louisville Region, provides over 12.5 million passenger trips covering more than 12 million miles annually. TARC runs 43 routes in five counties across two states, owns and operates 102 paratransit vehicles and 223 buses. TARC averages over 41,000 daily riders. Of all trips taken, 83% are for work or school."
                              cell.descriptionLabel?.numberOfLines = 0
                              return cell
          }
        
        
  
      }
    @objc func questionAction(sender:UIButton)
 {
    let now = Date()

    let formatter = DateFormatter()
             formatter.timeZone = TimeZone.current
             formatter.dateFormat = "hh:mm:aa"
             let dateString = formatter.string(from: now)
           print(dateString)
    
    print("ARUNCOUNT==>\(chatArray.count)")
    var messageName = NSString()
    messageName = sender.titleLabel!.text! as NSString
    if messageName == "Mobile Payment Issues"
    {
         let dict = ["Message" : messageName, "type" : "Receiver","Date": dateString] as NSDictionary
                   chatArray.append(dict as! [String : String])
 
            let questiondict = ["Message" : "", "type" : "ticket-Button"] as NSDictionary
                        chatArray.append(questiondict as! [String : String])
            tableView.reloadData()
            scrollToBottom()
            
    }
    else  if messageName == "MyTarc Card"
    {
        TitleTransaction = messageName as NSString

        let dict = ["Message" : messageName, "type" : "Receiver","Date": dateString] as NSDictionary
                          chatArray.append(dict as! [String : String])
        
                  let Senderdict = ["Message" : "Have questions about your MyTarc card? Please visit the MyTarc website to submit your inquiries.\nhttps://mytarc.ridetarc.org/", "type" : "textView-Button","Date": dateString] as NSDictionary
                                                chatArray.append(Senderdict as! [String : String])
       let questiondicts = ["Message" : "What can we help you with today ?\nPlease select a type of issue below:", "type" : "Sender"] as NSDictionary
               chatArray.append(questiondicts as! [String : String])

               let questiondict = ["Message" : "", "type" : "Sender-Button"] as NSDictionary
                                        chatArray.append(questiondict as! [String : String])
    
        
                   tableView.reloadData()
                   scrollToBottom()
    }
    else
    {
         let dict = ["Message" : messageName, "type" : "Receiver","Date": dateString] as NSDictionary
                   chatArray.append(dict as! [String : String])

            tableView.reloadData()
            scrollToBottom()
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                      let supportTripVC =  mainStoryboard.instantiateViewController(withIdentifier: "submitScreenViewController") as! submitScreenViewController
        supportTripVC.titleName = messageName as NSString
            supportTripVC.TransactionBool = false
                      self.navigationController!.pushViewController(supportTripVC, animated: true)
    }
   
    
 }
     @objc func tranactionAction(sender:UIButton)
    {
       let now = Date()
isSubmitButton = true
       let formatter = DateFormatter()
                formatter.timeZone = TimeZone.current
                formatter.dateFormat = "hh:mm:aa"
                let dateString = formatter.string(from: now)
              print(dateString)
       
       print("ARUNCOUNT==>\(chatArray.count)")
       var messageName = String()
       messageName = sender.titleLabel!.text!
        TitleTransaction = messageName as NSString
            let dict = ["Message" : messageName, "type" : "Receiver","Date": dateString] as NSDictionary
                      chatArray.append(dict as! [String : String])
    
        
        var messageString = NSString()
        //
        
               if messageName == "Ticket Not Generated " || messageName == "Transaction Failed"
               {
                   messageString = "Your receipt will be emailed to you if your payment was successful. If not, submit your request here for an investigation.\nStill have a question or concern ?\nSubmit your question here and we will respond by the next business day."
                let Senderdict = ["Message" : messageString, "type" : "Sender","Date": dateString] as NSDictionary
                                          chatArray.append(Senderdict as! [String : String])
                
             //   let Submitrdict = ["Message" : "Do you want to submit a report?", "type" : "Sender","Date": dateString] as NSDictionary
                                   //            chatArray.append(Submitrdict as! [String : String])
                  
                  
                         let questiondict = ["Message" : "", "type" : "ticketResponse-Button"] as NSDictionary
                                     chatArray.append(questiondict as! [String : String])
                
               }
               else if messageName == "Main Menu"
               {
                let questiondicts = ["Message" : "What can we help you with today ?\nPlease select a type of issue below:", "type" : "Sender"] as NSDictionary
                                                chatArray.append(questiondicts as! [String : String])

                                                let questiondict = ["Message" : "", "type" : "Sender-Button"] as NSDictionary
                                                                         chatArray.append(questiondict as! [String : String])
               }
               else if messageName == "Ticket Disappears"
               {
                   messageString = "Please verify the expiry date on your tickets. Expired tickets are not stored.\nStill have a question or concern ? Submit your question here and we will respond by the next business day."
                let Senderdict = ["Message" : messageString, "type" : "Sender","Date": dateString] as NSDictionary
                                          chatArray.append(Senderdict as! [String : String])
                
               // let Submitrdict = ["Message" : "Do you want to submit a report?", "type" : "Sender","Date": dateString] as NSDictionary
                                        //       chatArray.append(Submitrdict as! [String : String])
                  
                  
                         let questiondict = ["Message" : "", "type" : "ticketResponse-Button"] as NSDictionary
                                     chatArray.append(questiondict as! [String : String])
                
               }
               else if messageName == "Onboard Validator Issues"
               {
                  messageString = "Unable to scan your tickets on the bus? Check if your bluetooth is on.\nStill have a question or concern ?\nSubmit your question here and we will respond by the next business day."
                let Senderdict = ["Message" : messageString, "type" : "Sender","Date": dateString] as NSDictionary
                                          chatArray.append(Senderdict as! [String : String])
                
               // let Submitrdict = ["Message" : "Do you want to submit a report?", "type" : "Sender","Date": dateString] as NSDictionary
                         //                      chatArray.append(Submitrdict as! [String : String])
                  
                  
                         let questiondict = ["Message" : "", "type" : "ticketResponse-Button"] as NSDictionary
                                     chatArray.append(questiondict as! [String : String])
                
               }
                else if messageName == "Ticket Refunds"
                {
                   messageString = "We do not issue refunds or exchanges. "
                 let Senderdict = ["Message" : messageString, "type" : "Sender","Date": dateString] as NSDictionary
                                           chatArray.append(Senderdict as! [String : String])
                    
                    
                    let questiondicts = ["Message" : "What can we help you with today ?\nPlease select a type of issue below:", "type" : "Sender"] as NSDictionary
                                 chatArray.append(questiondicts as! [String : String])

                                 let questiondict = ["Message" : "", "type" : "Sender-Button"] as NSDictionary
                                                          chatArray.append(questiondict as! [String : String])
                    
                 
                // let Submitrdict = ["Message" : "Do you want to submit a report?", "type" : "Sender","Date": dateString] as NSDictionary
                          //                      chatArray.append(Submitrdict as! [String : String])
                   
                   
                       //   let questiondict = ["Message" : "", "type" : "ticketResponse-Button"] as NSDictionary
                                 //     chatArray.append(questiondict as! [String : String])
                 
                }
               else if messageName == "Other Issue"
               {
                isSubmitButton = false
                 
                         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let supportTripVC =  mainStoryboard.instantiateViewController(withIdentifier: "submitScreenViewController") as! submitScreenViewController
                                  supportTripVC.titleName = TitleTransaction as NSString
                supportTripVC.TransactionBool = true
                                            self.navigationController!.pushViewController(supportTripVC, animated: true)
               }
                
        else
               {
                
        }
        
  
       
        
  
               tableView.reloadData()
               scrollToBottom()
               
       

    }
    
    @objc func tranactionOkay(sender:UIButton)
       {
//        isSubmitButton = false
        if TitleTransaction == "My Tarc Card"
        {
    guard let url = URL(string: "https://mytarc.ridetarc.org/") else { return }
    UIApplication.shared.open(url)
        }
        else
        {
          let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                             let supportTripVC =  mainStoryboard.instantiateViewController(withIdentifier: "submitScreenViewController") as! submitScreenViewController
                   supportTripVC.titleName = TitleTransaction as NSString
            supportTripVC.TransactionBool = false
                             self.navigationController!.pushViewController(supportTripVC, animated: true)
        }
       }
     @objc func tranactionNotOkay(sender:UIButton)
     {
        var messageName = String()
             messageName = sender.titleLabel!.text!
//        isSubmitButton = false

        let dict = ["Message" : messageName, "type" : "Receiver","Date": ""] as NSDictionary
                             chatArray.append(dict as! [String : String])
        let questiondicts = ["Message" : "What can we help you with today ?\nPlease select a type of issue below:", "type" : "Sender"] as NSDictionary
        chatArray.append(questiondicts as! [String : String])

        let questiondict = ["Message" : "", "type" : "Sender-Button"] as NSDictionary
                                 chatArray.append(questiondict as! [String : String])
        tableView.reloadData()
        scrollToBottom()
    }
}

