//
//  questionTableViewCell.swift
//  chatbox
//
//  Created by Arun pandiyan on 07/04/20.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

class questionTableViewCell: UITableViewCell {

    var backView: UIView!
    var firstButton: UIButton!
    var secondButton: UIButton!
    var thirdButton: UIButton!
    var fourthButton: UIButton!
    var fifthButton: UIButton!
    var sixthButton: UIButton!
    var seventButton: UIButton!
    var eightButton: UIButton!
    var ninthButton: UIButton!
    var tenthButton: UIButton!
    var eleventhButton: UIButton!
    var twelveButton: UIButton!
//    var thirteenButton: UIButton!
//    var fourteenButton: UIButton!
//    var fifteenButton: UIButton!
//    var sixteenButton: UIButton!

    var width90 = Int()
        var width100 = Int()
        var width120 = Int()
        var width140 = Int()
        var width150 = Int()
    var heightButton = Int()
    var spaceHeight = Int()
    var spacingWidth = Int()
    var overallheight = Int()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.createSubViews()
       self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func createSubViews() -> Void {
        
       // let countArray = titleArray.count
        
      
        
        backView = UIView()
        backView.translatesAutoresizingMaskIntoConstraints = false
       // backView.frame = CGRect.init(x: 10, y: 0, width: 200, height: 50)
        backView.backgroundColor = UIColor.white
        self .addSubview(backView)

        
        firstButton = UIButton()
        firstButton.translatesAutoresizingMaskIntoConstraints = false
        firstButton.setTitle("Mobile Payment Issues", for: .normal)
        firstButton.backgroundColor = UIColor.white
        backView .addSubview(firstButton)
        firstButton .setTitleColor(UIColor.black, for: .normal)
        firstButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

        firstButton.layer.borderWidth = 1
        firstButton.layer.borderColor = UIColor.lightGray.cgColor
        firstButton.layer.cornerRadius = 15
        firstButton.layer.masksToBounds = false
        
        secondButton = UIButton()
        secondButton.translatesAutoresizingMaskIntoConstraints = false
        secondButton.setTitle("MyTarc Card", for: .normal)
        secondButton.backgroundColor = UIColor.white
        backView .addSubview(secondButton)
        secondButton .setTitleColor(UIColor.black, for: .normal)
        secondButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

        secondButton.layer.borderWidth = 1
        secondButton.layer.borderColor = UIColor.lightGray.cgColor
        secondButton.layer.cornerRadius = 15
        secondButton.layer.masksToBounds = false

        thirdButton = UIButton()
        thirdButton.translatesAutoresizingMaskIntoConstraints = false
        thirdButton.setTitle("App Suggestions", for: .normal)
        thirdButton.backgroundColor = UIColor.white
        backView .addSubview(thirdButton)
        thirdButton .setTitleColor(UIColor.black, for: .normal)
        thirdButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

        thirdButton.layer.borderWidth = 1
        thirdButton.layer.borderColor = UIColor.lightGray.cgColor
        thirdButton.layer.cornerRadius = 15
        thirdButton.layer.masksToBounds = false
        
        fourthButton = UIButton()
        fourthButton.translatesAutoresizingMaskIntoConstraints = false
        fourthButton.setTitle("Bug Report", for: .normal)
        fourthButton.titleLabel?.textAlignment = .right
        fourthButton.backgroundColor = UIColor.white
        backView .addSubview(fourthButton)
        fourthButton .setTitleColor(UIColor.black, for: .normal)
        fourthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

        fourthButton.layer.borderWidth = 1
        fourthButton.layer.borderColor = UIColor.lightGray.cgColor
        fourthButton.layer.cornerRadius = 15
        fourthButton.layer.masksToBounds = false
        
        
        fifthButton = UIButton()
        fifthButton.translatesAutoresizingMaskIntoConstraints = false
        fifthButton.setTitle("Lost & Found", for: .normal)
        fifthButton.backgroundColor = UIColor.white
        backView .addSubview(fifthButton)
        fifthButton .setTitleColor(UIColor.black, for: .normal)
        fifthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
        
        fifthButton.layer.borderWidth = 1
        fifthButton.layer.borderColor = UIColor.lightGray.cgColor
        fifthButton.layer.cornerRadius = 15
        fifthButton.layer.masksToBounds = false
            //
        sixthButton = UIButton()
        sixthButton.translatesAutoresizingMaskIntoConstraints = false
        sixthButton.setTitle("Bus Timings", for: .normal)
        sixthButton.backgroundColor = UIColor.white
        backView .addSubview(sixthButton)
        sixthButton .setTitleColor(UIColor.black, for: .normal)
        sixthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
        
        sixthButton.layer.borderWidth = 1
        sixthButton.layer.borderColor = UIColor.lightGray.cgColor
        sixthButton.layer.cornerRadius = 15
        sixthButton.layer.masksToBounds = false
        
        
        seventButton = UIButton()
        seventButton.translatesAutoresizingMaskIntoConstraints = false
        seventButton.setTitle("Service Requests", for: .normal)
        seventButton.backgroundColor = UIColor.white
        backView .addSubview(seventButton)
        seventButton .setTitleColor(UIColor.black, for: .normal)
        seventButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
        
        seventButton.layer.borderWidth = 1
        seventButton.layer.borderColor = UIColor.lightGray.cgColor
        seventButton.layer.cornerRadius = 15
        seventButton.layer.masksToBounds = false
        
        
        eightButton = UIButton()
        eightButton.translatesAutoresizingMaskIntoConstraints = false
        eightButton.setTitle("Safety", for: .normal)
        eightButton.backgroundColor = UIColor.white
        backView .addSubview(eightButton)
        eightButton .setTitleColor(UIColor.black, for: .normal)
        eightButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
        
        eightButton.layer.borderWidth = 1
        eightButton.layer.borderColor = UIColor.lightGray.cgColor
        eightButton.layer.cornerRadius = 15
        eightButton.layer.masksToBounds = false
        
        ninthButton = UIButton()
        ninthButton.translatesAutoresizingMaskIntoConstraints = false
        ninthButton.setTitle("Other Issue", for: .normal) //
        ninthButton.backgroundColor = UIColor.white
        backView .addSubview(ninthButton)
        ninthButton .setTitleColor(UIColor.black, for: .normal)
        ninthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
               
        ninthButton.layer.borderWidth = 1
        ninthButton.layer.borderColor = UIColor.lightGray.cgColor
        ninthButton.layer.cornerRadius = 15
        ninthButton.layer.masksToBounds = false
        
        
//        tenthButton = UIButton()
//           tenthButton.translatesAutoresizingMaskIntoConstraints = false
//           tenthButton.setTitle("Safety", for: .normal) //
//           tenthButton.backgroundColor = UIColor.white
//           backView .addSubview(tenthButton)
//           tenthButton .setTitleColor(UIColor.black, for: .normal)
//           tenthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
//
//           tenthButton.layer.borderWidth = 1
//           tenthButton.layer.borderColor = UIColor.lightGray.cgColor
//           tenthButton.layer.cornerRadius = 15
//           tenthButton.layer.masksToBounds = false
//
//        eleventhButton = UIButton()
//           eleventhButton.translatesAutoresizingMaskIntoConstraints = false
//           eleventhButton.setTitle("Other Issue", for: .normal) //
//           eleventhButton.backgroundColor = UIColor.white
//           backView .addSubview(eleventhButton)
//           eleventhButton .setTitleColor(UIColor.black, for: .normal)
//           eleventhButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
//
//           eleventhButton.layer.borderWidth = 1
//           eleventhButton.layer.borderColor = UIColor.lightGray.cgColor
//           eleventhButton.layer.cornerRadius = 15
//           eleventhButton.layer.masksToBounds = false
        
//        twelveButton = UIButton()
//           twelveButton.translatesAutoresizingMaskIntoConstraints = false
//           twelveButton.setTitle("Other Issues", for: .normal) //
//           twelveButton.backgroundColor = UIColor.white
//           backView .addSubview(twelveButton)
//           twelveButton .setTitleColor(UIColor.black, for: .normal)
//           twelveButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
//
//           twelveButton.layer.borderWidth = 1
//           twelveButton.layer.borderColor = UIColor.lightGray.cgColor
//           twelveButton.layer.cornerRadius = 15
//           twelveButton.layer.masksToBounds = false
        
        
       if UIScreen.main.sizeType == .iPhone4{
                          firstButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                                             secondButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                                             thirdButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                                             fourthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                                              fifthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                                              sixthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                                              seventButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                                              eightButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                         ninthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                            //     tenthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
        //eleventhButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
       // twelveButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)


                     }else if UIScreen.main.sizeType == .iPhone5{
         firstButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                            secondButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                            thirdButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                            fourthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                             fifthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                             sixthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                             seventButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
                             eightButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
        ninthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
            //    tenthButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
      //  eleventhButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)
       // twelveButton.titleLabel?.font = UIFont.setTarcRegular(size: 10)

                     }else if UIScreen.main.sizeType == .iPhone6{
                       
                     }else if UIScreen.main.sizeType == .iPhone6Plus{

                        
                     }
                     else if UIScreen.main.nativeBounds.height == 1920 {

                         
                     }
                     else if UIScreen.main.nativeBounds.height == 2688 {
                         print("iPhone XS max")

                     }
                     else if UIScreen.main.nativeBounds.height == 1792 {
                         print("iPhone Xr max")
                     }
                     else if UIScreen.main.sizeType == .iPadAir2andiPadMini4{

                     }else if UIScreen.main.sizeType == .iPadPro105{


                     }else if UIScreen.main.sizeType == .iPadPro129{
                    firstButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
                    secondButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
                    thirdButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
                    fourthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
                     fifthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
                     sixthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
                     seventButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
                     eightButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
ninthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
     //   tenthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
     //   eleventhButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
       // twelveButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)

                     }else if UIScreen.main.sizeType == .iPhoneX{
                        
                     }
        
    }
    @objc func actionQue(sender:UIButton)
    {
        print("\(sender.titleLabel!.text!)")
    }
    func setupConstraints() -> Void {
        
    

    
            width90 = 90
            width100 = 100
            width120 = 120
            width140 = 150
            width150 = 160
         heightButton = 30
         spaceHeight = 10
         spacingWidth = 10
        overallheight = 180
        if UIScreen.main.sizeType == .iPhone4{
                    width90 = 80
                              width100 = 90
                              width120 = 110
                              width140 = 130
                              width150 = 140
                           heightButton = 30
                           spaceHeight = 10
                           spacingWidth = 5
                          overallheight = 180
  
                }else if UIScreen.main.sizeType == .iPhone5{
              width90 = 80
                                      width100 = 90
                                      width120 = 110
                                      width140 = 130
                                      width150 = 140
                                   heightButton = 30
                                   spaceHeight = 10
                                   spacingWidth = 5
                                  overallheight = 180
                   
                }else if UIScreen.main.sizeType == .iPhone6{
                  
                }else if UIScreen.main.sizeType == .iPhone6Plus{

                   
                }
                else if UIScreen.main.nativeBounds.height == 1920 {

                    
                }
                else if UIScreen.main.nativeBounds.height == 2688 {
                    print("iPhone XS max")

                }
                else if UIScreen.main.nativeBounds.height == 1792 {
                    print("iPhone Xr max")
                }
                else if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
            width90 = 90
                       width100 = 110
                       width120 = 150
                       width140 = 170
                       width150 = 180
                       heightButton = 40
                       spaceHeight = 15
                       spacingWidth = 10
                   overallheight = 140
                }else if UIScreen.main.sizeType == .iPadPro105{
            width90 = 120
                       width100 = 130
                       width120 = 170
                       width140 = 190
                       width150 = 200
                       heightButton = 50
                       spaceHeight = 15
                       spacingWidth = 10
                   overallheight = 180

                }else if UIScreen.main.sizeType == .iPadPro129{
            width90 = 120
                width100 = 130
                width120 = 170
                width140 = 190
                width150 = 200
                heightButton = 50
                spaceHeight = 15
                spacingWidth = 15
            overallheight = 180

                }else if UIScreen.main.sizeType == .iPhoneX{
                   
                }
        
        let widthMatrics:[String:Any] = [ "width90":width90 ,
                                             "width100":width100 ,
                                             "width120":width120 ,
                                             "width140":width140 ,
                                             "width150":width150,
                                             "heightButton":heightButton,
                                             "spaceHeight":spaceHeight,
                                             "spacingWidth":spacingWidth,
                                             "overallheight":overallheight
                                          
               ]
        
        let viewsDict:[String:Any] = [ "backView":backView as Any,
                                      "firstButton":firstButton as Any,
                                      "secondButton":secondButton as Any,
                                      "thirdButton":thirdButton as Any,
                                      "fourthButton":fourthButton as Any,
                                      "fifthButton":fifthButton as Any,
                                    "sixthButton":sixthButton as Any,
                                    "seventButton":seventButton as Any,
                                    "eightButton":eightButton as Any,
                                    "ninthButton":ninthButton as Any
                                 //   "tenthButton":tenthButton as Any,
                                  //   "eleventhButton":eleventhButton as Any,
 //                                     "twelveButton":twelveButton as Any
//                                       "thirteenButton":thirteenButton as Any,
//                                        "fourteenButton":fourteenButton as Any,
//                                        "fifteenButton":fifteenButton as Any,
//                                        "sixteenButton":sixteenButton as Any


        ]
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[backView]-5-|",
                                                                         options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                         metrics: nil,
                                                                         views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[backView(overallheight)]",
                                                                         options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                         metrics: widthMatrics,
                                                                         views: viewsDict))
        
        
    
        
     
    if UIScreen.main.sizeType == .iPadAir2andiPadMini4 || UIScreen.main.sizeType == .iPadPro105 || UIScreen.main.sizeType == .iPadPro129 {

        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[firstButton(width140)]-spacingWidth-[secondButton(width90)]-spacingWidth-[thirdButton(width120)]-spacingWidth-[fourthButton(width90)]-spacingWidth-[fifthButton(width100)]",
                                                                           options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                           metrics: widthMatrics,
                                                                           views: viewsDict))
               
            
               
               
               NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[sixthButton(width100)]-spacingWidth-[seventButton(width120)]-spacingWidth-[eightButton(width90)]-spacingWidth-[ninthButton(width90)]",
                                                                                 options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                 metrics: widthMatrics,
                                                                                 views: viewsDict))
               
//             NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[eleventhButton(width120)]",
//                                                                                      options: NSLayoutConstraint.FormatOptions(rawValue: 0),
//                                                                                      metrics: widthMatrics,
//                                                                                      views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[thirdButton(heightButton)]-spaceHeight-[seventButton(heightButton)]",
                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                              metrics: widthMatrics,
                                                                              views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[fourthButton(heightButton)]-spaceHeight-[eightButton(heightButton)]",
                                                                                   options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                   metrics: widthMatrics,
                                                                                   views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[fourthButton(heightButton)]-spaceHeight-[eightButton(heightButton)]",
                                                                                         options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                         metrics: widthMatrics,
                                                                                         views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[fifthButton(heightButton)]-spaceHeight-[ninthButton(heightButton)]",
                                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                        metrics: widthMatrics,
                                                                                        views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]-spaceHeight-[sixthButton(heightButton)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: widthMatrics,
                                                                        views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: widthMatrics,
                                                                        views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]-spaceHeight-[sixthButton(heightButton)]",
                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                              metrics: widthMatrics,
                                                                              views: viewsDict))
        
            
             NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[secondButton(heightButton)]",
                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                             metrics: widthMatrics,
                                                                             views: viewsDict))
        
        
        
        
                        }
        else
    {
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[firstButton(width150)]-spacingWidth-[secondButton(width90)]",
                                                                           options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                           metrics: widthMatrics,
                                                                           views: viewsDict))
               
               NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[thirdButton(width120)]-spacingWidth-[fourthButton(width90)]-spacingWidth-[fifthButton(width90)]",
                                                                                 options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                 metrics: widthMatrics,
                                                                                 views: viewsDict))
               
               
               NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[sixthButton(width90)]-spacingWidth-[seventButton(width120)]-spacingWidth-[eightButton(width90)]",
                                                                                 options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                 metrics: widthMatrics,
                                                                                 views: viewsDict))
               
               NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[ninthButton(width100)]",
                                                                                  options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                  metrics: widthMatrics,
                                                                                  views: viewsDict))
               

        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]-spaceHeight-[fourthButton(heightButton)]-spaceHeight-[seventButton(heightButton)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: widthMatrics,
                                                                        views: viewsDict))
        
        
             NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]-spaceHeight-[thirdButton(heightButton)]-spaceHeight-[seventButton(heightButton)]",
                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                             metrics: widthMatrics,
                                                                             views: viewsDict))
        
            
             NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[secondButton(heightButton)]-spaceHeight-[fifthButton(heightButton)]-spaceHeight-[eightButton(heightButton)]-spaceHeight-[ninthButton(heightButton)]",
                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                             metrics: widthMatrics,
                                                                             views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[secondButton(heightButton)]-spaceHeight-[fifthButton(heightButton)]-spaceHeight-[eightButton(heightButton)]",
                                                                                  options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                  metrics: widthMatrics,
                                                                                  views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]-spaceHeight-[fifthButton(heightButton)]-spaceHeight-[sixthButton(heightButton)]",
                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                              metrics: widthMatrics,
                                                                              views: viewsDict))
        
        
        }
       
        
        
        backView.layer.cornerRadius = 5
        backView.layer.masksToBounds = false
        
    }

}
