//
//  FavoriteTableViewCell.swift
//  ZIG
//
//  Created by Arun pandiyan on 19/03/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class FavoriteTableViewCell: UITableViewCell {

    var centerName = UILabel()
    var centertimeandlanguage = UILabel()
  
    override func awakeFromNib() {
         super.awakeFromNib()
         // Initialization code
     }
     
     override func setSelected(_ selected: Bool, animated: Bool) {
         super.setSelected(selected, animated: animated)
         
         // Configure the view for the selected state
     }
     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
         super.init(style: style, reuseIdentifier: reuseIdentifier)
       setupViews()
     }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupViews()
    {
      


        centerName = UILabel()
        centerName.translatesAutoresizingMaskIntoConstraints = false
        centerName.font = UIFont.setTarcRegular(size: 18)
        centerName.minimumScaleFactor = 10/UIFont.labelFontSize
        centerName.adjustsFontSizeToFitWidth = true
        centerName.numberOfLines = 2
        centerName.textAlignment = .left
        self .addSubview(centerName)
       
        centertimeandlanguage = UILabel()
        centertimeandlanguage.translatesAutoresizingMaskIntoConstraints = false
        centertimeandlanguage.font = UIFont.setTarcRegular(size: 16)
        centertimeandlanguage.numberOfLines = 2
        centertimeandlanguage.textColor = UIColor.black
        centertimeandlanguage.textAlignment = .left
        centertimeandlanguage.minimumScaleFactor = 10/UIFont.labelFontSize
        centertimeandlanguage.adjustsFontSizeToFitWidth = true
        self .addSubview(centertimeandlanguage)

        let viewsDict:[String:Any] = [
                                      "centerName":centerName,
                                        "centertimeandlanguage":centertimeandlanguage,
        ]
      

        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[centerName]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[centertimeandlanguage]-0-|",
                                                                          options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                          metrics: nil,
                                                                          views: viewsDict))
        
       
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[centerName(30)]-5-[centertimeandlanguage(30)]",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
     
        
    }
}
