//
//  submitScreenViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 20/04/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import NVActivityIndicatorView
import NYAlertViewController
import MessageUI
import Foundation
import PhoneNumberKit
import GooglePlaces
import MobileCoreServices
import AVFoundation

class submitScreenViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,CLLocationManagerDelegate {
    var TitleLabel = UILabel()
    var subTitleLabel = UILabel()
    var loaderView = UIView()
    var screenshot: UIImageView!
    var firstTitleLabel = UILabel()
    var firsttextField = UIView()
    var firstText = UITextField()
    var newImage: UIImage?
    var videoURL : URL!
    var compressVideoURL : URL!
    var locManager = CLLocationManager()

    var DocumentURL_API = ""
    let phoneNumberKit = PhoneNumberKit()
    var AddressSearch = [AddressListGoogle]()
    let placesClient = GMSPlacesClient.shared()
    var sourcePlace :GMSPlace?
    var textViewCount = UILabel()
    var TransactionBool = Bool()
    var secondTitleLabel = UILabel()
    var secondTextfield = UITextView()
    var addAPhoto = UILabel()
    var addAPhotoButton = UIButton()
    var chooseAPhotoLabel = UILabel()
    var thirdTitleLabel = UILabel()
    var thirdTextfield = UIView()
    var fourthTitleLabel = UILabel()
    var fourthTextfield = UIView()
    var contactLabel = UILabel()
    var phoneTextfield = UIView()
    var emailTextfield = UIView()
    var submitButton = UIButton()
    var titleName = NSString()
    var issuetypeString = NSString()
    var imageCloseButton = UIButton()
    
    var inbetweenSpace = Int()
    var textfieldHeight = Int()
    var reportHeight = Int()
    var titleHeight = Int()
    var inbetween10Space = Int()
    
    var firsttitleName = NSString()
    var thirdTitleName = NSString()
    var fourthTitleName = NSString()
    
    var myPickerView: UIPickerView!
    var RidePickerView: UIPickerView!
    var profilename:String?
    var latitudeString:String?
    var logtitudeString:String?
    
    var thirdText = UITextField()
    var fourthText = UITextField()
    var phoneText = UITextField()
    var emailText = UITextField()
    var datePickerView = UIDatePicker()
    var PurchasedatePickerView = UIDatePicker()
    
    var  favoriteTableView = UITableView()
    var responseVarAddress = [AddressList]()
    
    var RideData = ["Single Ride", "24 Hour Pass", "7 Day Pass", "30 Day Pass"]
    var CountData = ["1", "2", "3", "4","5","6"]
//    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        latitudeString = ""
        logtitudeString = ""
        firsttitleName = "Location :"
        thirdTitleName = "Route No"
        fourthTitleName = "Date & Time"
        
        if titleName == "Ticket Not Generated " || titleName == "Ticket Refunds"
        {
            firsttitleName = "Reference Number :"
            thirdTitleName = "Fare Type:"
            fourthTitleName = "No of Ticket"
        }
        else if titleName == "Transaction Failed"
        {
            firsttitleName = "Error Message"
            thirdTitleName = "Fare Type:"
            fourthTitleName = "No of Ticket"
        }
        else if titleName == "Ticket Disappears"
        {
            firsttitleName = "#Ticket Id:"
            thirdTitleName = "Purchase Date:"
            fourthTitleName = "Expiry Date:"
        }
        else if titleName == "Onboard Validator Issues"
        {
            firsttitleName = "#Ticket Id:"
            thirdTitleName = "Route No"
            fourthTitleName = "Issue Date & Time:"
        }
        else if titleName == "Other Issue"  && TransactionBool == true
        {
            firsttitleName = "Subject:"
            
        }
            else if titleName == "App Suggestions" || titleName == "Bug Report"
                   {
                       firsttitleName = "Subject:"
                       
                   }
        else
        {
            
        }
        // Do any additional setup after loading the view.
        analytics.GetPageHitCount(PageName: "Support-Submit")
        NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
        offline.updateUserInterface(withoutLogin: false)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
        self.title = "Support"
        
        
        let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                         style: UIBarButtonItemStyle.done ,
                                         target: self, action: #selector(OnBackClicked))
      backButton.accessibilityLabel = "Back"

        self.navigationItem.leftBarButtonItem = backButton
        
        
        
        datePickerView = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        if  fourthTitleName == "Expiry Date:"
        {
            
        }
        else
        {
            datePickerView.maximumDate = Date()
            
        }
        
        PurchasedatePickerView = UIDatePicker()
        PurchasedatePickerView.datePickerMode = UIDatePickerMode.date
        PurchasedatePickerView.maximumDate = Date()
        
        supportDesign()
        
        
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
            if self.firsttitleName == "Location :"
                                {
            self.view.makeToast("Location access was limited. Kindly enable full permissions.")
            }
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            if self.firsttitleName == "Location :"
                                {
            self.view.makeToast("Location access was disabled. Please enable Location permissions for TARC.")
            }
        //mapBackgorundView.isHidden = false
        case .notDetermined:
            if self.firsttitleName == "Location :"
                       {
            self.view.makeToast("Location access was disabled. Please enable Location permissions for TARC.")
            }
            print("Location status not determined.")
        case .authorizedAlways:
            if self.firsttitleName == "Location :"
            {
            self.currentLocation()
            }

        case .authorizedWhenInUse:
            print("Location status is OK.")
            if self.firsttitleName == "Location :"
            {
            self.currentLocation()
            }
            //viewMap.isHidden = true
            
        }
    }
    func supportDesign()
    {
        var tltlefontSize = CGFloat()
        var fontSize =  CGFloat()
        
        tltlefontSize = 16
        fontSize = 14
        if UIScreen.main.sizeType == .iPhone4{
            tltlefontSize = 14
            fontSize = 12
            
        }else if UIScreen.main.sizeType == .iPhone5{
            
            tltlefontSize = 14
            fontSize = 12
            
        }
        TitleLabel = UILabel()
        TitleLabel.translatesAutoresizingMaskIntoConstraints = false
        TitleLabel.text = titleName as String
        TitleLabel.font = UIFont.setTarcBold(size: 22)
        // TitleLabel.backgroundColor = UIColor.red
        self.view .addSubview(TitleLabel)
        
        subTitleLabel = UILabel()
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        if titleName == "Ticket Not Generated " ||  titleName == "Ticket Disappears" || titleName == "Onboard Validator Issues" || titleName == "Ticket Refunds"
        {
            subTitleLabel.text = "For Ticket Purchase Information, Please Check your registered Email"
            
        }
        else
        {
            subTitleLabel.text = ""
            
        }
        subTitleLabel.font = UIFont.setTarcBold(size: 10)
        subTitleLabel.textColor = UIColor.init(red: 119/255, green: 119/255, blue: 119/255, alpha: 1.0)
        self.view .addSubview(subTitleLabel)
        
        
        firstTitleLabel = UILabel()
        firstTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        firstTitleLabel.text = "\(firsttitleName)*"
        firstTitleLabel.font = UIFont.setTarcBold(size: tltlefontSize)
        self.view .addSubview(firstTitleLabel)
        
        firsttextField = UIView()
        firsttextField.translatesAutoresizingMaskIntoConstraints = false
        firsttextField.layer.borderWidth = 1
        firsttextField.layer.borderColor = UIColor.init(red: 194/255, green: 194/255, blue: 194/255, alpha: 1.0).cgColor
        self.view .addSubview(firsttextField)
        
        firstText = UITextField()
        firstText.translatesAutoresizingMaskIntoConstraints = false
        firstText.font = UIFont.setTarcRegular(size: fontSize)
        firsttextField .addSubview(firstText)
        firstText.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                            for: .editingChanged)
        firstText.delegate = self
        firstText.placeholder = firsttitleName as String
        
        locManager = CLLocationManager()
         locManager.delegate = self
         locManager.desiredAccuracy = kCLLocationAccuracyBest;
         locManager.distanceFilter = kCLDistanceFilterNone;
         locManager.requestWhenInUseAuthorization()
        print(firsttitleName)
        if firsttitleName == "Location :"
        {
             firstText.clearButtonMode = .whileEditing
            
           if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                    case .notDetermined, .restricted, .denied:
                        print("No access")

                    case .authorizedAlways, .authorizedWhenInUse:
                        print("Access")
                        self.currentLocation()
                    @unknown default:
                    break
                }
                } else {
                    print("Location services are not enabled")


            }
            
        }
        
        if firsttitleName == "Reference Number :" || firsttitleName == "#Ticket Id:"
        {
            firstText.keyboardType = .numberPad
        }
        
        secondTitleLabel = UILabel()
        secondTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        secondTitleLabel.text = "What is this report about ?*"
        secondTitleLabel.font = UIFont.setTarcBold(size: tltlefontSize)
        self.view .addSubview(secondTitleLabel)
        
        secondTextfield = UITextView()
        
        secondTextfield.translatesAutoresizingMaskIntoConstraints = false
        secondTextfield.layer.borderWidth = 1
        secondTextfield.layer.borderColor = UIColor.init(red: 194/255, green: 194/255, blue: 194/255, alpha: 1.0).cgColor
        secondTextfield.delegate = self
        secondTextfield.isScrollEnabled = true
        secondTextfield.showsVerticalScrollIndicator = true
        secondTextfield.font = UIFont.setTarcRegular(size: fontSize)
        secondTextfield.clipsToBounds = true
        self.view .addSubview(secondTextfield)
        
        textViewCount = UILabel()
        textViewCount.translatesAutoresizingMaskIntoConstraints = false
        textViewCount.font = UIFont.setTarcBold(size: 12)
        textViewCount.textAlignment = .right
        textViewCount.text = "0/1000"
        self.view .addSubview(textViewCount)
        
        if titleName == "Other Issue"
        {
            secondTextfield.text = "What is this report about ?"
            secondTextfield.textColor = UIColor.lightGray
        }
        else
        {
            secondTextfield.text = titleName as String
            let numberOfChars = titleName.length
            textViewCount.text = "\(numberOfChars)/1000"
        }
        
        
        
        
        addAPhoto = UILabel()
        addAPhoto.translatesAutoresizingMaskIntoConstraints = false
        addAPhoto.text = "Add a Photo/Video"
        addAPhoto.font = UIFont.setTarcBold(size: tltlefontSize)
        self.view .addSubview(addAPhoto)
        
        addAPhotoButton = UIButton()
        addAPhotoButton.translatesAutoresizingMaskIntoConstraints = false
        addAPhotoButton .setTitle("Choose File", for: .normal)
        addAPhotoButton.setTitleColor(UIColor.black, for: .normal)
        addAPhotoButton.layer.borderWidth = 1
        addAPhotoButton.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        addAPhotoButton.layer.borderColor =  UIColor.init(red: 194/255, green: 194/255, blue: 194/255, alpha: 1.0).cgColor
        addAPhotoButton.backgroundColor = UIColor.init(red: 236/255, green: 236/255, blue: 236/255, alpha: 1.0)
        addAPhotoButton.addTarget(self, action: #selector(ImageBtnTap), for: .touchUpInside)
        self.view .addSubview(addAPhotoButton)
        addAPhotoButton.layer.masksToBounds = false
        addAPhotoButton.layer.shadowRadius = 3.0
        addAPhotoButton.layer.shadowColor = UIColor.black.cgColor
        addAPhotoButton.layer.shadowOffset = CGSize.init(width: 1.0, height: 1.0)
        addAPhotoButton.layer.shadowOpacity = 1.0
        
        
        
        chooseAPhotoLabel = UILabel()
        chooseAPhotoLabel.translatesAutoresizingMaskIntoConstraints = false
        chooseAPhotoLabel.text = "No file selected"
        chooseAPhotoLabel.font = UIFont.setTarcRegular(size: fontSize)
        self.view .addSubview(chooseAPhotoLabel)
        imageCloseButton = UIButton()
        imageCloseButton.translatesAutoresizingMaskIntoConstraints = false
        imageCloseButton.setImage(UIImage.init(named: "closeBtn"), for: .normal)
        imageCloseButton.addTarget(self, action: #selector(imageCloseAction), for: .touchUpInside)
        self.view .addSubview(imageCloseButton)
        imageCloseButton.isHidden = true
        
        
        thirdTitleLabel = UILabel()
        thirdTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        if thirdTitleName == "Route No"
        {
            thirdTitleLabel.text = thirdTitleName as String
            
        }
        else
        {
            thirdTitleLabel.text = "\(thirdTitleName)*"
            
        }
        thirdTitleLabel.font = UIFont.setTarcBold(size: tltlefontSize)
        self.view .addSubview(thirdTitleLabel)
        
        thirdTextfield = UIView()
        thirdTextfield.translatesAutoresizingMaskIntoConstraints = false
        thirdTextfield.layer.borderWidth = 1
        thirdTextfield.layer.borderColor =  UIColor.init(red: 194/255, green: 194/255, blue: 194/255, alpha: 1.0).cgColor
        self.view .addSubview(thirdTextfield)
        
        
        thirdText = UITextField()
        thirdText.translatesAutoresizingMaskIntoConstraints = false
        thirdText.delegate = self
        thirdText.font = UIFont.setTarcRegular(size: tltlefontSize)
        thirdTextfield .addSubview(thirdText)
        thirdText.backgroundColor = UIColor.clear
        
        
        
        if titleName == "Ticket Not Generated " ||  titleName == "Transaction Failed" || titleName == "Ticket Refunds"
        {
            thirdTextfield.backgroundColor = UIColor.init(red: 236/255, green: 236/255, blue: 236/255, alpha: 1.0)
            
            thirdText.rightView = UIImageView(image: UIImage.init(named: "down_arrow"))
            thirdText.rightView?.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
            thirdText.rightViewMode = .always
            thirdTextfield.layer.masksToBounds = false
            thirdTextfield.layer.shadowRadius = 3.0
            thirdTextfield.layer.shadowColor = UIColor.black.cgColor
            thirdTextfield.layer.shadowOffset = CGSize.init(width: 1.0, height: 1.0)
            thirdTextfield.layer.shadowOpacity = 1.0
            thirdText.text = "Single Ride"
        }
        else if  titleName == "Ticket Disappears"
        {
            thirdText.font = UIFont.setTarcRegular(size: fontSize)
            
            thirdText.inputView = PurchasedatePickerView
            PurchasedatePickerView.datePickerMode = .dateAndTime
            PurchasedatePickerView.addTarget(self, action: #selector(PurchasedatePickerFromValueChanged), for: UIControlEvents.valueChanged)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
            thirdText.text = dateFormatter.string(from: Date())
            
            
        }
        thirdText.placeholder = thirdTitleName as String
        //        if thirdTitleName == "Route No"
        //                     {
        //                         thirdText.keyboardType = .numberPad
        //                     }
        fourthTitleLabel = UILabel()
        fourthTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        fourthTitleLabel.text = "\(fourthTitleName)*"
        fourthTitleLabel.font = UIFont.setTarcBold(size: tltlefontSize)
        self.view .addSubview(fourthTitleLabel)
        
        fourthTextfield = UIView()
        fourthTextfield.translatesAutoresizingMaskIntoConstraints = false
        fourthTextfield.layer.borderWidth = 1
        fourthTextfield.layer.borderColor =  UIColor.init(red: 194/255, green: 194/255, blue: 194/255, alpha: 1.0).cgColor
        self.view .addSubview(fourthTextfield)
        
        fourthText = UITextField()
        fourthText.translatesAutoresizingMaskIntoConstraints = false
        fourthText.delegate = self
        fourthText.font = UIFont.setTarcRegular(size: fontSize)
        fourthText.backgroundColor = UIColor.clear
        fourthTextfield .addSubview(fourthText)
        if titleName == "Ticket Not Generated " ||  titleName == "Transaction Failed" || titleName == "Ticket Refunds"
        {
            fourthTextfield.backgroundColor = UIColor.init(red: 236/255, green: 236/255, blue: 236/255, alpha: 1.0)
            
            fourthText.rightView = UIImageView(image: UIImage.init(named: "down_arrow"))
            fourthText.rightView?.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
            fourthText.rightViewMode = .always
            fourthTextfield.layer.masksToBounds = false
            fourthTextfield.layer.shadowRadius = 3.0
            fourthTextfield.layer.shadowColor = UIColor.black.cgColor
            fourthTextfield.layer.shadowOffset = CGSize.init(width: 1.0, height: 1.0)
            fourthTextfield.layer.shadowOpacity = 1.0
            fourthText.text = "1"
            
        }
        else
        {
            fourthText.inputView = datePickerView
            datePickerView.datePickerMode = .dateAndTime
            datePickerView.addTarget(self, action: #selector(datePickerFromValueChanged), for: UIControlEvents.valueChanged)
            fourthText.placeholder = fourthTitleName as String
            
            
            if  fourthTitleName == "Expiry Date:"
            {
                
            }
            else
            {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
                fourthText.text = dateFormatter.string(from: Date())
            }
            
            
            
            
            
        }
        
        contactLabel = UILabel()
        contactLabel.translatesAutoresizingMaskIntoConstraints = false
        contactLabel.text = "Contact info*"
        contactLabel.font = UIFont.setTarcBold(size: tltlefontSize)
        self.view .addSubview(contactLabel)
        
        phoneTextfield = UIView()
        phoneTextfield.translatesAutoresizingMaskIntoConstraints = false
        phoneTextfield.layer.borderWidth = 1
        phoneTextfield.layer.borderColor = UIColor.init(red: 194/255, green: 194/255, blue: 194/255, alpha: 1.0).cgColor
        
        self.view .addSubview(phoneTextfield)
        
        phoneText = UITextField()
        phoneText.translatesAutoresizingMaskIntoConstraints = false
        phoneText.delegate = self
        phoneText.placeholder = "Enter Your Phone Number"
        phoneText.font = UIFont.setTarcRegular(size: fontSize)
        phoneTextfield .addSubview(phoneText)
        phoneText.keyboardType = .phonePad
        phoneText.addTarget(self, action: #selector(textFieldDidChange(textField:)),
                            for: .editingChanged)
        let userDefaults1 = UserDefaults.standard
        
        if userDefaults1.value(forKey: "phoneNumber") == nil
        {
            phoneText.placeholder = "Enter Your Phone Number"
        }
        else
        {
            let emailAddress =  userDefaults1.object(forKey: "phoneNumber") as! String
            let stringN = emailAddress.toPhoneNumber()
            phoneText.text = stringN as String
        }
        
        emailTextfield = UIView()
        emailTextfield.translatesAutoresizingMaskIntoConstraints = false
        emailTextfield.layer.borderWidth = 1
        emailTextfield.layer.borderColor =  UIColor.init(red: 194/255, green: 194/255, blue: 194/255, alpha: 1.0).cgColor
        
        self.view .addSubview(emailTextfield)
        
        let emailAddress =  userDefaults1.object(forKey: "userEmail") as! NSString
        
        emailText = UITextField()
        emailText.translatesAutoresizingMaskIntoConstraints = false
        emailText.delegate = self
        emailText.placeholder = "Enter Your Email"
        emailText.font = UIFont.setTarcRegular(size: fontSize)
        if userDefaults1.object(forKey: "userEmail") != nil || emailAddress != ""
        {
            emailText.text = emailAddress as String
        }
        emailTextfield .addSubview(emailText)
        emailText.keyboardType = .emailAddress
        
        
        submitButton = UIButton()
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton .setTitle("Submit Reports", for: .normal)
        submitButton.setTitleColor(UIColor.white, for: .normal)
        submitButton.titleLabel?.font = UIFont.setTarcHeavy(size: 20)
        submitButton.backgroundColor = UIColor.red
        submitButton.addTarget(self, action: #selector(SendMail), for: .touchUpInside)
        self.view .addSubview(submitButton)
        
        
        favoriteTableView = UITableView()
        favoriteTableView.translatesAutoresizingMaskIntoConstraints = false
        favoriteTableView.delegate = self
        favoriteTableView.dataSource = self
        favoriteTableView.separatorStyle = .singleLine
        favoriteTableView.tag = 100
        self.view .addSubview(favoriteTableView)
        favoriteTableView .bringSubview(toFront: secondTitleLabel)
        favoriteTableView .bringSubview(toFront: secondTextfield)
        favoriteTableView .bringSubview(toFront: addAPhoto)
        favoriteTableView .bringSubview(toFront: addAPhotoButton)
        favoriteTableView .bringSubview(toFront: chooseAPhotoLabel)
        favoriteTableView .bringSubview(toFront: thirdTitleLabel)
        favoriteTableView .bringSubview(toFront: thirdText)
        favoriteTableView .bringSubview(toFront: thirdTextfield)
        favoriteTableView .bringSubview(toFront: fourthText)
        favoriteTableView .bringSubview(toFront: fourthTextfield)
        favoriteTableView .bringSubview(toFront: fourthTitleLabel)
        favoriteTableView .bringSubview(toFront: textViewCount)
        
        
        favoriteTableView.isHidden = true
        
        var widthofFouth = Int()
        widthofFouth = 170
        inbetweenSpace = 0
        textfieldHeight = 35
        reportHeight = 100
        titleHeight = 30
        inbetween10Space = 15
        if UIScreen.main.sizeType == .iPhone4{
            
            inbetweenSpace = 5
            textfieldHeight = 30
            reportHeight = 80
            titleHeight = 30
            
        }else if UIScreen.main.sizeType == .iPhone5{
            inbetween10Space = 5
            
            inbetweenSpace = 5
            textfieldHeight = 25
            reportHeight = 80
            titleHeight = 25
            
        }else if UIScreen.main.sizeType == .iPhone6{
            
        }else if UIScreen.main.sizeType == .iPhone6Plus{
            
            inbetweenSpace = 10
            textfieldHeight = 40
            reportHeight = 100
            titleHeight = 30
            
        }
        else if UIScreen.main.nativeBounds.height == 1920 {
            inbetweenSpace = 5
            textfieldHeight = 35
            
        }
        else if UIScreen.main.nativeBounds.height == 2688 {
            print("iPhone XS max")
            inbetweenSpace = 10
            textfieldHeight = 40
        }
        else if UIScreen.main.nativeBounds.height == 1792 {
            print("iPhone Xr max")
            inbetweenSpace = 5
            textfieldHeight = 35
            
        }
            
            
        else if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
            inbetweenSpace = 15
            textfieldHeight = 50
            reportHeight = 200
            titleHeight = 55
            inbetween10Space = 15
            subTitleLabel.font = UIFont.boldSystemFont(ofSize: 15)
            
            widthofFouth = 300
            
            
        }else if UIScreen.main.sizeType == .iPadPro105{
            inbetweenSpace = 15
            textfieldHeight = 50
            reportHeight = 200
            titleHeight = 55
            inbetween10Space = 15
            widthofFouth = 300
            
            subTitleLabel.font = UIFont.boldSystemFont(ofSize: 15)
            
        }else if UIScreen.main.sizeType == .iPadPro129{
            inbetweenSpace = 20
            textfieldHeight = 50
            reportHeight = 200
            titleHeight = 55
            inbetween10Space = 15
            subTitleLabel.font = UIFont.boldSystemFont(ofSize: 15)
            widthofFouth = 500
        }else if UIScreen.main.sizeType == .iPhoneX{
            
        }
        
        
        
        let metricsNew:[String:Any] = ["inbetweenSpace":inbetweenSpace ,
                                       "textfieldHeight":textfieldHeight,
                                       "reportHeight":reportHeight,
                                       "titleHeight":titleHeight,
                                       "inbetween10Space":inbetween10Space,
                                       "widthofFouth":widthofFouth
        ]
        let viewsDict:[String:Any] = ["TitleLabel":TitleLabel as Any,
                                      "firstTitleLabel":firstTitleLabel as Any,
                                      "firsttextField":firsttextField as Any,
                                      "secondTitleLabel":secondTitleLabel as Any,
                                      "secondTextfield":secondTextfield as Any,
                                      "addAPhoto":addAPhoto as Any,
                                      "addAPhotoButton":addAPhotoButton as Any,
                                      "chooseAPhotoLabel":chooseAPhotoLabel as Any,
                                      "thirdTitleLabel":thirdTitleLabel as Any,
                                      "thirdTextfield":thirdTextfield as Any,
                                      "fourthTitleLabel":fourthTitleLabel as Any,
                                      "fourthTextfield":fourthTextfield as Any,
                                      "contactLabel":contactLabel as Any,
                                      "phoneTextfield":phoneTextfield as Any,
                                      "emailTextfield":emailTextfield as Any,
                                      "submitButton":submitButton as Any,
                                      "subTitleLabel":subTitleLabel as Any,
                                      "firstText": firstText as Any,
                                      "thirdText": thirdText as Any,
                                      "fourthText": fourthText as Any,
                                      "phoneText": phoneText as Any,
                                      "emailText": emailText as Any,
                                      "favoriteTableView":favoriteTableView,
                                      "textViewCount":textViewCount,
                                      "imageCloseButton":imageCloseButton
            
            
            
            
        ]
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[TitleLabel]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[subTitleLabel]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[firstTitleLabel]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[firsttextField]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[secondTitleLabel]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[secondTextfield]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[textViewCount]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[addAPhoto]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[addAPhotoButton(150)]-10-[chooseAPhotoLabel]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-170-[chooseAPhotoLabel]-5-[imageCloseButton(20)]-20-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[thirdTitleLabel(widthofFouth)]-10-[fourthTitleLabel]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: metricsNew,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[thirdTextfield(widthofFouth)]-10-[fourthTextfield]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: metricsNew,
                                                                    views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[contactLabel]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[phoneTextfield]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[emailTextfield]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-60-[submitButton]-60-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[TitleLabel(titleHeight)]-0-[subTitleLabel(15)]-inbetween10Space-[firstTitleLabel(20)]-inbetweenSpace-[firsttextField(textfieldHeight)]-0-[favoriteTableView]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: metricsNew,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[TitleLabel(titleHeight)]-5-[subTitleLabel(15)]-inbetweenSpace-[firstTitleLabel(20)]-inbetweenSpace-[firsttextField(textfieldHeight)]-inbetween10Space-[secondTitleLabel(20)]-inbetweenSpace-[secondTextfield(reportHeight)]-0-[textViewCount(15)]-inbetween10Space-[addAPhoto(30)]-inbetweenSpace-[addAPhotoButton(40)]",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: metricsNew,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[TitleLabel(titleHeight)]-5-[subTitleLabel(15)]-inbetweenSpace-[firstTitleLabel(20)]-inbetweenSpace-[firsttextField(textfieldHeight)]-inbetween10Space-[secondTitleLabel(20)]-inbetweenSpace-[secondTextfield(reportHeight)]-0-[textViewCount(15)]-inbetween10Space-[addAPhoto(30)]-inbetweenSpace-[imageCloseButton(40)]",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: metricsNew,
                                                                    views: viewsDict))
        
        
        if  titleName == "Other Issue" && TransactionBool == true
        {
            thirdTextfield.isHidden = true
            thirdTitleLabel.isHidden = true
            fourthTextfield.isHidden = true
            fourthTitleLabel.isHidden = true
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[TitleLabel(titleHeight)]-0-[subTitleLabel(15)]-inbetween10Space-[firstTitleLabel(20)]-inbetweenSpace-[firsttextField(textfieldHeight)]-inbetween10Space-[secondTitleLabel(20)]-inbetweenSpace-[secondTextfield(reportHeight)]-0-[textViewCount(15)]-inbetween10Space-[addAPhoto(30)]-inbetweenSpace-[chooseAPhotoLabel(40)]-inbetween10Space-[contactLabel(20)]-inbetweenSpace-[phoneTextfield(textfieldHeight)]-inbetween10Space-[emailTextfield(textfieldHeight)]-inbetween10Space-[submitButton(textfieldHeight)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: metricsNew,
                                                                        views: viewsDict))
        }

        else if titleName == "App Suggestions" || titleName == "Bug Report"
        {
            thirdTextfield.isHidden = true
                      thirdTitleLabel.isHidden = true
                      fourthTextfield.isHidden = true
                      fourthTitleLabel.isHidden = true
                      NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[TitleLabel(titleHeight)]-0-[subTitleLabel(15)]-inbetween10Space-[firstTitleLabel(20)]-inbetweenSpace-[firsttextField(textfieldHeight)]-inbetween10Space-[secondTitleLabel(20)]-inbetweenSpace-[secondTextfield(reportHeight)]-0-[textViewCount(15)]-inbetween10Space-[addAPhoto(30)]-inbetweenSpace-[chooseAPhotoLabel(40)]-inbetween10Space-[contactLabel(20)]-inbetweenSpace-[phoneTextfield(textfieldHeight)]-inbetween10Space-[emailTextfield(textfieldHeight)]-inbetween10Space-[submitButton(textfieldHeight)]",
                                                                                  options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                  metrics: metricsNew,
                                                                                  views: viewsDict))
        }
        else
        {
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[TitleLabel(titleHeight)]-0-[subTitleLabel(15)]-inbetween10Space-[firstTitleLabel(20)]-inbetweenSpace-[firsttextField(textfieldHeight)]-inbetween10Space-[secondTitleLabel(20)]-inbetweenSpace-[secondTextfield(reportHeight)]-0-[textViewCount(15)]-inbetween10Space-[addAPhoto(30)]-inbetweenSpace-[chooseAPhotoLabel(40)]-inbetween10Space-[thirdTitleLabel(20)]-inbetweenSpace-[thirdTextfield(textfieldHeight)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: metricsNew,
                                                                        views: viewsDict))
            
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-20-[TitleLabel(titleHeight)]-0-[subTitleLabel(15)]-inbetween10Space-[firstTitleLabel(20)]-inbetweenSpace-[firsttextField(textfieldHeight)]-inbetween10Space-[secondTitleLabel(20)]-inbetweenSpace-[secondTextfield(reportHeight)]-0-[textViewCount(15)]-inbetween10Space-[addAPhoto(30)]-inbetweenSpace-[chooseAPhotoLabel(40)]-inbetween10Space-[fourthTitleLabel(20)]-inbetweenSpace-[fourthTextfield(textfieldHeight)]-inbetween10Space-[contactLabel(20)]-inbetweenSpace-[phoneTextfield(textfieldHeight)]-inbetween10Space-[emailTextfield(textfieldHeight)]-inbetween10Space-[submitButton(textfieldHeight)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: metricsNew,
                                                                        views: viewsDict))
            
        }
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[firstText]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[firstText]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[thirdText]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[thirdText]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[fourthText]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[fourthText]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[phoneText]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[phoneText]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[emailText]-10-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[emailText]-0-|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[favoriteTableView]|",
                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                    metrics: nil,
                                                                    views: viewsDict))
        
        
        
        
        secondTextfield.contentInset = UIEdgeInsets.zero;
        secondTextfield.clipsToBounds = true
        
        firsttextField.layer.cornerRadius = CGFloat(textfieldHeight/2)
        firsttextField.layer.masksToBounds = false
        secondTextfield.layer.cornerRadius = 5
        // secondTextfield.layer.masksToBounds = false
        addAPhotoButton.layer.cornerRadius = 20
        addAPhotoButton.layer.masksToBounds = false
        thirdTextfield.layer.cornerRadius = CGFloat(textfieldHeight/2)
        thirdTextfield.layer.masksToBounds = false
        fourthTextfield.layer.cornerRadius = CGFloat(textfieldHeight/2)
        fourthTextfield.layer.masksToBounds = false
        phoneTextfield.layer.cornerRadius = CGFloat(textfieldHeight/2)
        phoneTextfield.layer.masksToBounds = false
        emailTextfield.layer.cornerRadius = CGFloat(textfieldHeight/2)
        emailTextfield.layer.masksToBounds = false
        submitButton.layer.cornerRadius = 5
        submitButton.layer.masksToBounds = false
        
    }


    @objc func OnBackClicked() {
        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierBack"), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @objc func statusManager(_ notification: NSNotification) {
        offline.updateUserInterface(withoutLogin: false)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == myPickerView
        {
            return CountData.count
            
        }
        else
        {
            return RideData.count
            
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == myPickerView
        {
            return CountData[row]
            
        }
        else
        {
            return RideData[row]
            
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == myPickerView
        {
            fourthText.text =  "  \(CountData[row])"
            fourthText.resignFirstResponder()
            
        }
        else
        {
            thirdText.text =   "  \(RideData[row])"
            thirdText.resignFirstResponder()
            
        }
        // state.text = pickerData[row]
    }
    //MARK:- TextFiled Delegate
    
    
    @objc func doneClick() {
        fourthText.resignFirstResponder()
        
    }
    @objc func cancelClick() {
        fourthText.resignFirstResponder()
        phoneText.becomeFirstResponder()
    }
    @objc func done() {
        thirdText.resignFirstResponder()
    }
    @objc func cancel() {
        
        thirdText.resignFirstResponder()
        fourthText.becomeFirstResponder()
    }
    func pickUp(_ textField: UITextField) {
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        textField.inputView = self.myPickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "next", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    func TicketpickUp(_ textField: UITextField) {
        
        // UIPickerView
        self.RidePickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.RidePickerView.delegate = self
        self.RidePickerView.dataSource = self
        self.RidePickerView.backgroundColor = UIColor.white
        textField.inputView = self.RidePickerView
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(done))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "next", style: .plain, target: self, action: #selector(cancel))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)

        if titleName == "Ticket Not Generated " ||  titleName == "Transaction Failed" || titleName == "Ticket Refunds"
        {
            
            if textField == thirdText
            {
                self.TicketpickUp(textField)
            }
            else if textField == fourthText
            {
                self.pickUp(textField)
            }
            
        }
    }
    
    //Process to happen when the user selects the image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        /*
         Get the image from the info dictionary.
         If no need to edit the photo, use `UIImagePickerControllerOriginalImage`
         instead of `UIImagePickerControllerEditedImage`
         */
        imageCloseButton.isHidden = false
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMddyyyyhhmm"
        let dateInFormat = dateFormatter.string(from: NSDate() as Date)
        profilename = "\(dateInFormat).jpeg"
        
        if let editedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            //  self.screenshot.image = editedImage
            profilename = "\(dateInFormat).jpeg"
            self.newImage = editedImage
            chooseAPhotoLabel.text = "\((profilename)!)"
            
            
            //  self.imageUploadBtn.setTitle("   \((profilename)!)", for: .normal)
        }
        else if let editedImage = info[UIImagePickerControllerMediaURL] as? NSURL
        {
            profilename = "\(dateInFormat).MOV"
            videoURL = editedImage as URL
            let destinationDir = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let destination = destinationDir.appendingPathComponent("SomeFileName.zip")
            
            chooseAPhotoLabel.text = "\((profilename)!)"

        }
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true, completion: nil)
    }
    @objc func imageCloseAction()
    {
        chooseAPhotoLabel.text = "No file selected"
        profilename = ""
        self.newImage = nil
        imageCloseButton.isHidden = true
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func imageError(image: UIImage,didFinishSavingWithError error:NSErrorPointer, contextInfo:UnsafeRawPointer){
        if error != nil{
            let alert = UIAlertController(title: "Save Failed", message: "Failed to save image", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            self.present(alert,animated: true,completion: nil)
        }
    }
    func presentCameraSettings() {
        
        let alertController = UIAlertController(title: "Error",
                                      message: "Camera access is denied",
                                      preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })

        present(alertController, animated: true)
    }
    func videoPermission()
    {
        let microPhoneStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)

        switch microPhoneStatus {

        case .notDetermined: break
           // self.openVideo()
        case .restricted:
            
           miccrophonepresentCameraSettings()
        case .denied:
            miccrophonepresentCameraSettings()
        case .authorized:
            self.openVideo()


        }
    }
    func miccrophonepresentCameraSettings() {
        let alertController = UIAlertController(title: "Error",
                                      message: "Microphone access is denied",
                                      preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })

        present(alertController, animated: true)
    }
    @IBAction func ImageBtnTap(_ sender: UIButton) {
        DispatchQueue.main.async {

        print("Tapped on Image")
                   switch AVCaptureDevice.authorizationStatus(for: .video) {
                      case .denied:
                          print("Denied, request permission from settings")
                           DispatchQueue.main.async {
                          self.presentCameraSettings()
                    }
                      case .restricted:
                          print("Restricted, device owner must approve")
                      case .authorized:
                          print("Authorized, proceed")
                           DispatchQueue.main.async {
                          self.VideoPermission(senderButton: sender)
                    }

       //YEs
                      case .notDetermined:
                          AVCaptureDevice.requestAccess(for: .video) { success in
                              if success {
                                  print("Permission granted, proceed")
       //YES
                                 DispatchQueue.main.async {
                                self.VideoPermission(senderButton: sender)
                                }
                              } else {
                                  print("Permission denied")
                                 DispatchQueue.main.async {
                               self.presentCameraSettings()
                                }

                              }
                          }
                      }
        
        print("work")
        }
    }
    func VideoPermission(senderButton:UIButton)
    {
       // DispatchQueue.main.async {

        let session = AVAudioSession.sharedInstance()
        if (session.responds(to: #selector(AVAudioSession.requestRecordPermission(_:)))) {
            AVAudioSession.sharedInstance().requestRecordPermission({(granted: Bool)-> Void in
                if granted {
                    print("granted")
                     DispatchQueue.main.async {
                    self.openAlertScren(sender: senderButton)
                    }
                } else {
                    print("not granted")
                     DispatchQueue.main.async {
                    self.miccrophonepresentCameraSettings()
                    }
                }
            })
       // }
        }

    }
    func openAlertScren(sender:UIButton)
    {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { _ in
            self.openCamera()


            
        }))
        
        alert.addAction(UIAlertAction(title: "Take Video", style: .default, handler: { _ in
            self.videoPermission()


               }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                   self.openVideoGallary()
               }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = (sender as UIView)
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    func openGallary(){
        let imagePicker = UIImagePickerController()
        // self.screenshot.isHidden = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        // imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func openCamera(){
        let imagePicker = UIImagePickerController()

        // self.screenshot.isHidden = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        //If you dont want to edit the photo then you can set allowsEditing to false
        // imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func openVideo(){
        let imagePicker = UIImagePickerController()

           // self.screenshot.isHidden = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .camera)!
       // imagePicker.cameraCaptureMode = .video
        imagePicker.videoMaximumDuration = 60
        imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]

           //If you dont want to edit the photo then you can set allowsEditing to false
           // imagePicker.allowsEditing = true
           imagePicker.delegate = self
           self.present(imagePicker, animated: true, completion: nil)
       }
    func openVideoGallary(){
        let imagePicker = UIImagePickerController()

        // self.screenshot.isHidden = false
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        // imagePicker.allowsEditing = true
        imagePicker.mediaTypes = [(kUTTypeMovie as String)]
              imagePicker.allowsEditing = true
        imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String, kUTTypePNG as String, kUTTypeJPEG as String, kUTTypeImage as String]
        imagePicker.videoMaximumDuration = 60

            //  imagePicker.showsCameraControls = false

       // imagePicker.modalPresentationStyle = .custom
        
        
        
        
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func SendMail(_ sender: Any) {
        if isReachable()
        {
            
            
            let userDefaults1 = UserDefaults.standard
            
            if userDefaults1.object(forKey: "userName") == nil || userDefaults1.object(forKey: "userEmail") == nil
            {
                userDefaults1.set("", forKey: "userEmail")
                
                userDefaults1.set("", forKey: "userName")
            }
            else
            {
                
            }
            
            let userprofilename = userDefaults1.object(forKey: "userName") as! NSString
            // let emailAddress =  userDefaults1.object(forKey: "userEmail") as! NSString
            
            self.showLoader()
            let firstMessage = firstText.text
            var SecondMessage = secondTextfield.text
            if SecondMessage == "What is this report about ?"
            {
                SecondMessage = ""
            }
            let ThirdMessage = thirdText.text
            let fouthMessage = fourthText.text
            var phoneMessage = phoneText.text
            let emailMessage = emailText.text
            if titleName == "Other Issue" && TransactionBool == true
            {
                
                if firstMessage != nil && firstMessage != "" && SecondMessage != nil && SecondMessage != "" && phoneMessage != nil && phoneMessage != "" && emailMessage != nil && emailMessage != ""
                {
                    
                    let emailValidation = isValidEmail(testStr: emailMessage!) as Bool
                    
                    if emailValidation == true
                    {
                        
                        let allowedCharset = CharacterSet
                            .decimalDigits
                            .union(CharacterSet(charactersIn: "+"))
                        
                        let filteredText = String(phoneMessage!.unicodeScalars.filter(allowedCharset.contains))
                        
                        print(filteredText)
                        phoneMessage = filteredText
                        let PhoneValidation = phoneNumberValidation(values: phoneMessage!) as Bool
                        
                        if PhoneValidation == true
                        {
                            
                            
                            
                            SendNewMAilID(firstMessage: firstMessage! as NSString, secondMessage: SecondMessage! as NSString, thirdMessage: ThirdMessage! as NSString, fouthMessage: fouthMessage! as NSString, emailMessage: emailMessage! as NSString, PhoneMessage: phoneMessage! as NSString,userprofilename: userprofilename)
                        }
                        else
                        {
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Support"
                            alertViewController.message = "Please Enter Valid Phone number"
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        alertViewController.title = "Support"
                        alertViewController.message = "Please Enter Valid Emailid"
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                    
                }
                else{
                    
                    self.hideLoader()
                    let alertViewController = NYAlertViewController()
                    alertViewController.title = "Support"
                    alertViewController.message = "Please complete all the fields required."
                    
                    // Customize appearance as desired
                    alertViewController.buttonCornerRadius = 20.0
                    alertViewController.view.tintColor = self.view.tintColor
                    alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                    alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.swipeDismissalGestureEnabled = true
                    alertViewController.backgroundTapDismissalGestureEnabled = true
                    alertViewController.buttonColor = UIColor.red
                    // Add alert actions
                    
                    
                    let cancelAction = NYAlertAction(
                        title: "OK",
                        style: .cancel,
                        handler: { (action: NYAlertAction!) -> Void in
                            
                            self.dismiss(animated: true, completion: nil)
                    }
                    )
                    
                    alertViewController.addAction(cancelAction)
                    
                    // Present the alert view controller
                    self.present(alertViewController, animated: true, completion: nil)
                    
                    
                    
                }
            }
           else if titleName == "App Suggestions" || titleName == "Bug Report"
           {
                
                if firstMessage != nil && firstMessage != "" && SecondMessage != nil && SecondMessage != "" && phoneMessage != nil && phoneMessage != "" && emailMessage != nil && emailMessage != ""
                {
                    
                    let emailValidation = isValidEmail(testStr: emailMessage!) as Bool
                    
                    if emailValidation == true
                    {
                        
                        let allowedCharset = CharacterSet
                            .decimalDigits
                            .union(CharacterSet(charactersIn: "+"))
                        
                        let filteredText = String(phoneMessage!.unicodeScalars.filter(allowedCharset.contains))
                        
                        print(filteredText)
                        phoneMessage = filteredText
                        let PhoneValidation = phoneNumberValidation(values: phoneMessage!) as Bool
                        
                        if PhoneValidation == true
                        {
                            
                            SendNewMAilID(firstMessage: firstMessage! as NSString, secondMessage: SecondMessage! as NSString, thirdMessage: ThirdMessage! as NSString, fouthMessage: fouthMessage! as NSString, emailMessage: emailMessage! as NSString, PhoneMessage: phoneMessage! as NSString,userprofilename: userprofilename)
                        }
                        else
                        {
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Support"
                            alertViewController.message = "Please Enter Valid Phone number"
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                    }
                    else
                    {
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        alertViewController.title = "Support"
                        alertViewController.message = "Please Enter Valid Emailid"
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                    
                }
                else{
                    
                    self.hideLoader()
                    let alertViewController = NYAlertViewController()
                    alertViewController.title = "Support"
                    alertViewController.message = "Please complete all the fields required."
                    
                    // Customize appearance as desired
                    alertViewController.buttonCornerRadius = 20.0
                    alertViewController.view.tintColor = self.view.tintColor
                    alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                    alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                    alertViewController.swipeDismissalGestureEnabled = true
                    alertViewController.backgroundTapDismissalGestureEnabled = true
                    alertViewController.buttonColor = UIColor.red
                    // Add alert actions
                    
                    
                    let cancelAction = NYAlertAction(
                        title: "OK",
                        style: .cancel,
                        handler: { (action: NYAlertAction!) -> Void in
                            
                            self.dismiss(animated: true, completion: nil)
                    }
                    )
                    
                    alertViewController.addAction(cancelAction)
                    
                    // Present the alert view controller
                    self.present(alertViewController, animated: true, completion: nil)
                    
                    
                    
                }
            }
            else
            {
                
                if thirdTitleName == "Route No"
                {
                    if firstMessage != nil && firstMessage != "" && SecondMessage != nil && SecondMessage != ""  && fouthMessage != nil && fouthMessage != "" && phoneMessage != nil && phoneMessage != "" && emailMessage != nil && emailMessage != ""
                    {
                        
                        let emailValidation = isValidEmail(testStr: emailMessage!) as Bool
                        
                        if emailValidation == true
                        {
                            
                            let allowedCharset = CharacterSet
                                .decimalDigits
                                .union(CharacterSet(charactersIn: "+"))
                            
                            let filteredText = String(phoneMessage!.unicodeScalars.filter(allowedCharset.contains))
                            
                            print(filteredText)
                            phoneMessage = filteredText
                            let PhoneValidation = phoneNumberValidation(values: phoneMessage!) as Bool
                            
                            if PhoneValidation == true
                            {
                                
                                SendNewMAilID(firstMessage: firstMessage! as NSString, secondMessage: SecondMessage! as NSString, thirdMessage: ThirdMessage! as NSString, fouthMessage: fouthMessage! as NSString, emailMessage: emailMessage! as NSString, PhoneMessage: phoneMessage! as NSString,userprofilename: userprofilename)
                            }
                            else
                            {
                                self.hideLoader()
                                let alertViewController = NYAlertViewController()
                                alertViewController.title = "Support"
                                alertViewController.message = "Please Enter Valid Phone number"
                                
                                // Customize appearance as desired
                                alertViewController.buttonCornerRadius = 20.0
                                alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.swipeDismissalGestureEnabled = true
                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                alertViewController.buttonColor = UIColor.red
                                // Add alert actions
                                
                                
                                let cancelAction = NYAlertAction(
                                    title: "OK",
                                    style: .cancel,
                                    handler: { (action: NYAlertAction!) -> Void in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                }
                                )
                                
                                alertViewController.addAction(cancelAction)
                                
                                // Present the alert view controller
                                self.present(alertViewController, animated: true, completion: nil)
                            }
                        }
                        else
                        {
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Support"
                            alertViewController.message = "Please Enter Valid Emailid"
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        alertViewController.title = "Support"
                        alertViewController.message = "Please complete all the fields required."
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                else
                {
                    if firstMessage != nil && firstMessage != "" && SecondMessage != nil && SecondMessage != "" && ThirdMessage != nil && ThirdMessage != "" && fouthMessage != nil && fouthMessage != "" && phoneMessage != nil && phoneMessage != "" && emailMessage != nil && emailMessage != ""
                    {
                        
                        let emailValidation = isValidEmail(testStr: emailMessage!) as Bool
                        
                        if emailValidation == true
                        {
                            
                            let allowedCharset = CharacterSet
                                .decimalDigits
                                .union(CharacterSet(charactersIn: "+"))
                            
                            let filteredText = String(phoneMessage!.unicodeScalars.filter(allowedCharset.contains))
                            
                            print(filteredText)
                            phoneMessage = filteredText
                            let PhoneValidation = phoneNumberValidation(values: phoneMessage!) as Bool
                            
                            if PhoneValidation == true
                            {
                                
                                SendNewMAilID(firstMessage: firstMessage! as NSString, secondMessage: SecondMessage! as NSString, thirdMessage: ThirdMessage! as NSString, fouthMessage: fouthMessage! as NSString, emailMessage: emailMessage! as NSString, PhoneMessage: phoneMessage! as NSString,userprofilename: userprofilename)
                            }
                            else
                            {
                                self.hideLoader()
                                let alertViewController = NYAlertViewController()
                                alertViewController.title = "Support"
                                alertViewController.message = "Please Enter Valid Phone number"
                                
                                // Customize appearance as desired
                                alertViewController.buttonCornerRadius = 20.0
                                alertViewController.view.tintColor = self.view.tintColor
                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                alertViewController.swipeDismissalGestureEnabled = true
                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                alertViewController.buttonColor = UIColor.red
                                // Add alert actions
                                
                                
                                let cancelAction = NYAlertAction(
                                    title: "OK",
                                    style: .cancel,
                                    handler: { (action: NYAlertAction!) -> Void in
                                        
                                        self.dismiss(animated: true, completion: nil)
                                }
                                )
                                
                                alertViewController.addAction(cancelAction)
                                
                                // Present the alert view controller
                                self.present(alertViewController, animated: true, completion: nil)
                            }
                        }
                        else
                        {
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Support"
                            alertViewController.message = "Please Enter Valid Emailid"
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                        
                    }
                    else{
                        
                        self.hideLoader()
                        let alertViewController = NYAlertViewController()
                        alertViewController.title = "Support"
                        alertViewController.message = "Please complete all the fields required."
                        
                        // Customize appearance as desired
                        alertViewController.buttonCornerRadius = 20.0
                        alertViewController.view.tintColor = self.view.tintColor
                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                        alertViewController.swipeDismissalGestureEnabled = true
                        alertViewController.backgroundTapDismissalGestureEnabled = true
                        alertViewController.buttonColor = UIColor.red
                        // Add alert actions
                        
                        
                        let cancelAction = NYAlertAction(
                            title: "OK",
                            style: .cancel,
                            handler: { (action: NYAlertAction!) -> Void in
                                
                                self.dismiss(animated: true, completion: nil)
                        }
                        )
                        
                        alertViewController.addAction(cancelAction)
                        
                        // Present the alert view controller
                        self.present(alertViewController, animated: true, completion: nil)
                        
                        
                        
                    }
                }
                
                
            }
        }
        else
        {
            self.view.makeToast("No Internet Conenction")
        }
    }
    
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    func emailsending(Username:String,Email:String,message:String,Url:String,Address:String,Coach:String,Incidenttime:String,Phone:String,Longitude:String,Latitude:String,T_Referenceno:String,T_Faretype:String,T_Noofticket:String,T_Errormsgorsub:String,T_Ticketid:String,T_Purchasedate:String,T_Expirydate:String){
        
        
        
        // let testmsg = message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        print("\(message)")
        //  let testmsg = message.addingPercentEncoding(withAllowedCharacters: CharacterSet(charactersIn: "!*'();:@&=+$,/?%#[]{} ").inverted)
        let appVersion = getAppVersion() as NSString
        
        let note = "Mobile: \(UIDevice.modelName) \n IP: \((UIDevice.current.ipAddress())!) \n iOS Version:\(UIDevice.current.systemVersion) \n App Version:\(appVersion)"
        //.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        //            let Url_Reset = "https://www.ridetarc.org/DMI/mail.php?&Message=\((testmsg)!)&url=\(Url)&emailaddress=\(Email)&name=\(Username)&note=\((note)!)"
        //            print(Url_Reset)
        //            Alamofire.request(Url_Reset, method: .get,encoding: URLEncoding.default)
        //                .responseObject{ (response: DataResponse<ResetPasswordModel>) in
        //                    switch response.result {
        //                    case .success:
        //                        print(response)
        //                        //to get status code
        //
        //                            self.view.makeToast("Your email has been successfully sent.")
        //                        print("messageeeeeeeeeeeee")
        //
        //
        //
        //                    case .failure(let error):
        //                        print(error)
        //
        //                    }
        //
        //            }
        
        
        let dataDic :  [String: Any]  = [
            "Username":Username,
            "EmailID":Email,
            "IssueType":self.issuetypeString,
            "Info":"\((note))",
            "Address":Address,
            "Description":message,
            "Coach":Coach,
            "Incidenttime":Incidenttime,
            "Phone":Phone,
            "Longitude":Longitude,
            "Link":self.DocumentURL_API,
            "Latitude":Latitude,
            "T_Referenceno":T_Referenceno,
            "T_Faretype":T_Faretype,
            "T_Noofticket":T_Noofticket,
            "T_Errormsgorsub":T_Errormsgorsub,
            "T_Ticketid":T_Ticketid,
            "T_Purchasedate":T_Purchasedate,
            "T_Expirydate":T_Expirydate
        ]
        
        
        
        //
        //                                                let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
        //                                                let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
        let url2 = URL(string: "https://ridetarc.org/DMI/API/SupportMailAPI.php")!
        //                                                var request = URLRequest(url: url2)
        //                                                request.httpMethod = HTTPMethod.post.rawValue
        //                                                request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        //                                                request.httpBody = dataJsonStrin
        
        //
        //                                                Alamofire.request(request)
        //                                                    .responseObject{ (response: DataResponse<ResetPasswordModel>) in
        //                                                        switch response.result {
        //                                                        case .success:
        //                                                            dump(response.result.value)
        //
        //
        //                                                             self.hideLoader()
        //
        //                                                        case .failure(let error):
        //
        //                                                            print(error)
        //                                                           self.view.makeToast(error.localizedDescription)
        //                                                            self.hideLoader()
        //                                                        }
        //
        //                                                }
        print(dataDic)
        
        
        Alamofire.request(url2, method: .post, parameters: dataDic, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                case .success:
                    
                    
                    if (response.result.value != nil) {
                        
                          //  let responseVar = response.result.value
                        
                        
                    } else {
                        
                        
                        
                        
                        self.hideLoader()
                    }
                case .failure(let error):
                    print(error)
                    
                    
                    
                    self.hideLoader()
                }
        }
        
        
    }
    func getAppVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let versionValue = dictionary["CFBundleShortVersionString"] ?? "0"
        let buildValue = dictionary["CFBundleVersion"] ?? "0"
        return "\(versionValue) (build \(buildValue))"
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: testStr)
    }
    func phoneNumberValidation(values : String) -> Bool {
        do {
            // let phoneNumber = try phoneNumberKit.parse("+33 6 89 017383")
            let phoneNumberCustomDefaultRegion = try phoneNumberKit.parse(values, withRegion: "US", ignoreType: true)
            return true
            
        }
        catch {
            print("Generic parser error")
            return false
        }
        
    }
    
    func compressVideo(inputURL: URL,
                       outputURL: URL,
                       handler:@escaping (_ exportSession: AVAssetExportSession?) -> Void) {
        self.showLoader()
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset,
                                                       presetName: AVAssetExportPresetLowQuality) else {
            handler(nil)

            return
        }

        exportSession.outputURL = outputURL
        exportSession.outputFileType = .mov
        exportSession.exportAsynchronously {
            self.compressVideoURL = exportSession.outputURL
            self.hideLoader()
            handler(exportSession)

                       return
        }
    }
    
    func SendNewMAilID(firstMessage:NSString,secondMessage:NSString,thirdMessage:NSString,fouthMessage:NSString,emailMessage:NSString,PhoneMessage:NSString,userprofilename:NSString)
    {
        
        
        if titleName == "Ticket Not Generated "
        {
            issuetypeString = "TNG"
        }
        else if titleName == "Transaction Failed"
        {
            issuetypeString = "TF"
            
        }
        else if titleName == "Ticket Disappears"
        {
            issuetypeString = "TD"
            
        }
        else if titleName == "Onboard Validator Issues"
        {
            issuetypeString = "OVI"
            
        }
        else if titleName == "Driver is coughing"
        {
            issuetypeString = "DIC"
            
        }
        else if titleName == "Passenger is coughing"
        {
            issuetypeString = "PIC"
            
        }
        else if titleName == "Violence"
        {
            issuetypeString = "Vio"
            
        }
        else if titleName == "Over crowding"
        {
            issuetypeString = "OC"
            
        }
        else if titleName == "Bus is late"
        {
            issuetypeString = "BIL"
            
        }
        else if titleName == "Bus left early"
        {
            issuetypeString = "BIE"
            
        }
            
        else if titleName == "Bus not clean"
        {
            issuetypeString = "BNC"
            
        }
        else if titleName == "Driver is rude"
        {
            issuetypeString = "DIR"
            
        }
        else if titleName == "Fraud issues"
        {
            issuetypeString = "FI"
            
        }
        else if titleName == "Ticket refunds"
        {
            issuetypeString = "TR"
        }
        else if titleName == "Safety"
        {
            issuetypeString = "Sa"
            
        }
            
        else if titleName == "Other Issue" && TransactionBool == true
        {
            issuetypeString = "TOI"
            
        }
        else if titleName == "App Suggestions"
        {
             issuetypeString = "AS"
        }
        else if  titleName == "Bug Report"
        {
            issuetypeString = "BR"
        }
       
        else if titleName == "Other Issue" && TransactionBool == false
        {
            issuetypeString = "OI"
            
        }
        else if titleName == "Passengers without masks"
        {
            issuetypeString = "PWM"
        }
        else if titleName ==  "Increased service needed"
        {
            issuetypeString = "ISN"
        }
        else if titleName ==  "Lost & Found"
        {
            issuetypeString = "LAF"
        }
            
        else if titleName == "Bus Timings"
        {
            issuetypeString = "BT"
            
        }
            else if titleName == "Service Requests"
            {
                issuetypeString = "SER"
                
            }
            else if titleName == "Schedule Requests"
            {
                issuetypeString = "SCR"
                
            }
            else if titleName == "Hygiene"
            {
                issuetypeString = "HY"
                
            }
            else if titleName == "Covid"
            {
                issuetypeString = "CO"
                
            }

        else
        {
            issuetypeString = "OOI"
            
        }
        
        
        var Latitude = NSString()
        var Longitude = NSString()
        var Address = NSString()
        var Coach = NSString()
        var Incidenttime = NSString()
        var Phone = NSString()
        var T_Referenceno = NSString()
        var T_Errormsgorsub = NSString()
        var T_Purchasedate = NSString()
        var T_Expirydate = NSString()
        var T_Faretype = Int()
        var T_Noofticket = Int()
        var T_Ticketid = Int()
        
        
        
        if thirdText.text == "  Single Ride" || thirdText.text == "Single Ride"
        {
            T_Faretype = 9
        }
        else if thirdText.text == "  24 Hour Pass"
        {
            T_Faretype = 10
            
        }
        else if thirdText.text == "  7 Day Pass"
        {
            T_Faretype = 11
            
        }
        else if thirdText.text == "  30 Day Pass"
        {
            T_Faretype = 12
            
        }
        else
        {
            T_Faretype = 0
        }
        
        Latitude = latitudeString! as NSString
        Longitude = logtitudeString! as NSString
        Address = firstMessage
        Coach = thirdMessage
        Incidenttime = fouthMessage
        Phone = PhoneMessage
        T_Referenceno = ""
        T_Errormsgorsub = ""
        T_Purchasedate = ""
        T_Expirydate = ""
        T_Noofticket = 0
        T_Ticketid = 0
        
        
        if issuetypeString == "TNG"
        {
            
            let trimmedString = fouthMessage.trimmingCharacters(in: .whitespaces)
            let NOT:Int? = Int(trimmedString as String) // firstText is UITextField
            
            Latitude = ""
            Longitude = ""
            Address = ""
            Coach = ""
            Incidenttime = ""
            Phone = PhoneMessage
            T_Referenceno = firstMessage
            T_Errormsgorsub = ""
            T_Purchasedate = ""
            T_Expirydate = ""
            //   T_Faretype = 0
            T_Noofticket = NOT!
            T_Ticketid = 0
        }
        else if issuetypeString == "TR"
        {
            
            let trimmedString = fouthMessage.trimmingCharacters(in: .whitespaces) as String
            let NOT:Int? = Int(trimmedString as String) // firstText is UITextField
            
            Latitude = ""
            Longitude = ""
            Address = ""
            Coach = ""
            Incidenttime = ""
            Phone = PhoneMessage
            T_Referenceno = firstMessage
            T_Errormsgorsub = ""
            T_Purchasedate = ""
            T_Expirydate = ""
            //   T_Faretype = 0
            T_Noofticket = NOT!
            T_Ticketid = 0
        }
        else if issuetypeString == "TF"
        {
            let trimmedString = fouthMessage.trimmingCharacters(in: .whitespaces) as String
            
            let NOT:Int? = Int(trimmedString as String) // firstText is UITextField
            
            Latitude = ""
            Longitude = ""
            Address = ""
            Coach = ""
            Incidenttime = ""
            Phone = PhoneMessage
            T_Referenceno = ""
            T_Errormsgorsub = firstMessage
            T_Purchasedate = ""
            T_Expirydate = ""
            T_Noofticket = NOT!
            T_Ticketid = 0
        }
        else if issuetypeString == "TD"
        {
            
            let TID:Int? = Int(firstMessage as String) // firstText is UITextField
            
            Latitude = ""
            Longitude = ""
            Address = ""
            Coach = ""
            Incidenttime = ""
            Phone = PhoneMessage
            T_Referenceno = ""
            T_Errormsgorsub = ""
            T_Purchasedate = thirdMessage
            T_Expirydate = fouthMessage
            T_Noofticket = 0
            T_Ticketid = TID!
        }
        else if issuetypeString == "OVI"
        {
            let TID:Int? = Int(firstMessage as String) // firstText is UITextField
            
            Latitude = ""
            Longitude = ""
            Address = ""
            Coach = thirdMessage
            Incidenttime = fouthMessage
            Phone = PhoneMessage
            T_Referenceno = ""
            T_Errormsgorsub = ""
            T_Purchasedate = ""
            T_Expirydate = ""
            T_Noofticket = 0
            T_Ticketid = TID!
        }
        else if issuetypeString == "DIC"
        {
            
        }
        else if issuetypeString == "PIC"
        {
            
        }
        else if issuetypeString == "Vio"
        {
            
        }
        else if issuetypeString == "OC"
        {
            
        }
        else if issuetypeString == "BSI"
        {
            
        }
        else if issuetypeString == "BMN"
        {
            
        }
        else if issuetypeString == "OI"
        {
            
            
            Latitude = latitudeString! as NSString
            Longitude = logtitudeString! as NSString
            Address = firstMessage
            Coach = thirdMessage
            Incidenttime = fouthMessage
            Phone = PhoneMessage
            T_Referenceno = ""
            T_Errormsgorsub = ""
            T_Purchasedate = ""
            T_Expirydate = ""
            T_Noofticket = 0
            T_Ticketid = 0
            
            
        }
        else if  issuetypeString == "TOI"
        {
            
            
            Latitude = ""
            Longitude = ""
            Address = ""
            Coach = ""
            Incidenttime = ""
            Phone = PhoneMessage
            T_Referenceno = ""
            T_Errormsgorsub = firstMessage
            T_Purchasedate = ""
            T_Expirydate = ""
            T_Noofticket = 0
            T_Ticketid = 0
            
            
        }
         else if  issuetypeString == "AS"
        {
           Latitude = ""
                       Longitude = ""
                       Address = ""
                       Coach = ""
                       Incidenttime = ""
                       Phone = PhoneMessage
                       T_Referenceno = ""
                       T_Errormsgorsub = firstMessage
                       T_Purchasedate = ""
                       T_Expirydate = ""
                       T_Noofticket = 0
                       T_Ticketid = 0
        }
         else if  issuetypeString == "BR"
        {
           Latitude = ""
                       Longitude = ""
                       Address = ""
                       Coach = ""
                       Incidenttime = ""
                       Phone = PhoneMessage
                       T_Referenceno = ""
                       T_Errormsgorsub = firstMessage
                       T_Purchasedate = ""
                       T_Expirydate = ""
                       T_Noofticket = 0
                       T_Ticketid = 0
        }
        else
        {
            
        }
        
        
        if newImage != nil {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMddyyyyhhmm"
            let dateInFormat = dateFormatter.string(from: NSDate() as Date)
            
            guard let imageCompress = self.newImage,
                let imageData = UIImageJPEGRepresentation(imageCompress, 0.5) else {
                    return
            }
            
            
            print("\(APIUrl.ZIGTARCAPI)User/DocumentUpload?Username=\((userprofilename))&Profilename=\((userprofilename))")
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(imageData, withName: "photo_path", fileName: "\(dateInFormat).jpeg", mimeType: "image/jpeg")
                
            }, to:"\(APIUrl.ZIGTARCAPI)User/DocumentUpload?Username=\((userprofilename))&Profilename=\((userprofilename))")
                
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (Progress) in
                        print("Upload Progress: \(Progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        dump(response)
                        if let ResultValue = response.result.value as? [String:Any] {
                            let responseMessage = ResultValue["Message"] as? String
                            if (responseMessage == "Ok") {
                                self.DocumentURL_API = (ResultValue["DocPath"]! as? String)!
                                print(self.DocumentURL_API)
                                self.view.makeToast("Your document has been successfully uploaded.")
                                
                                
                                print("******* Connect *****")
                                
                                
                                let accessToken = UserDefaults.standard.value(forKey: "accessToken")
                                //userEmail
                                // let emailid = UserDefaults.standard.string(forKey: "userEmail")! as NSString
                                //  print("\(emailid)")
                                let dataDic : NSMutableDictionary = [
                                    "token": accessToken!,
                                    // "issuetype":"Issue/Support iOS",
                                    "link": self.DocumentURL_API ,
                                    "description": secondMessage,
                                    "Callback":"false",
                                    "emailid":emailMessage,
                                    "agencyid":1,
                                    "Latitude": Latitude,
                                    "Longitude": Longitude,
                                    "Address": Address,
                                    "Coach": Coach,
                                    "Incidenttime": Incidenttime,
                                    "Phone": Phone,
                                    "T_Referenceno": T_Referenceno,
                                    "T_Faretype": T_Faretype,
                                    "T_Noofticket": T_Noofticket,
                                    "T_Errormsgorsub": T_Errormsgorsub,
                                    "T_Ticketid": T_Ticketid,
                                    "T_Purchasedate": T_Purchasedate,
                                    "T_Expirydate": T_Expirydate,
                                    "issuetype":self.issuetypeString
                                    
                                ]
                                
                                
                                
                                
                                let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                                let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
                                let url2 = URL(string: "\(APIUrl.ZIGTARCAPI)Feedback/ReportIssue/Add")!
                                var request = URLRequest(url: url2)
                                request.httpMethod = HTTPMethod.post.rawValue
                                request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                                request.httpBody = dataJson
                                
                                
                                Alamofire.request(request)
                                    .responseObject{ (response: DataResponse<Postpayment>) in
                                        switch response.result {
                                        case .success:
                                            dump(response.result.value)
                                            if response.result.value?.message == "Issue added Successfully"  || response.result.value?.message == "Your feedback has been successfully received." {
                                                //                                                    self.emailsending(Username: userprofilename as String, Email: emailMessage as String, message: secondMessage as String, Url: self.DocumentURL_API
                                                self.emailsending(Username: userprofilename as String, Email: emailMessage as String, message: secondMessage as String, Url: self.DocumentURL_API, Address: Address as String, Coach: Coach as String, Incidenttime: Incidenttime as String, Phone: Phone as String, Longitude: Longitude as String, Latitude: Latitude as String, T_Referenceno: T_Referenceno as String, T_Faretype: "\(T_Faretype)", T_Noofticket: "\(T_Noofticket)", T_Errormsgorsub: T_Errormsgorsub as String, T_Ticketid: "\(T_Ticketid)", T_Purchasedate: T_Purchasedate as String, T_Expirydate: T_Expirydate as String)
                                                self.hideLoader()
                                                //  self.screenshot.image = nil
                                                //self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                                                // self.view.makeToast(response.result.value?.message)
                                                
                                                self.hideLoader()
                                                let alertViewController = NYAlertViewController()
                                                alertViewController.title = "Thank you!"
                                                alertViewController.message = "Your message has been successfully sent.\nWe will be in touch shortly!"
                                                
                                                // Customize appearance as desired
                                                alertViewController.buttonCornerRadius = 20.0
                                                alertViewController.view.tintColor = self.view.tintColor
                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.swipeDismissalGestureEnabled = true
                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                alertViewController.buttonColor = UIColor.red
                                                // Add alert actions
                                                
                                                
                                                let cancelAction = NYAlertAction(
                                                    title: "OK",
                                                    style: .cancel,
                                                    handler: { (action: NYAlertAction!) -> Void in
                                                        self.dismiss(animated: true, completion: nil)
                                                        
                                                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                                                       
                                                          NotificationCenter.default.post(name: Notification.Name("NotificationForUpdateTicketList"), object: nil)
                                                        
                                                        self.navigationController?.popViewController(animated: true)
                                                        
                                                        //                                                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                                        //                                                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                                        //
                                                        //                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                                                        
                                                        
                                                }
                                                )
                                                
                                                alertViewController.addAction(cancelAction)
                                                
                                                // Present the alert view controller
                                                self.present(alertViewController, animated: true, completion: nil)
                                                
                                                
                                                
                                            }
                                            else{
                                                //print(response.result.value?.message!)
                                                self.hideLoader()
                                                //    self.screenshot.image = nil
                                                //  self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                                                let alertViewController = NYAlertViewController()
                                                alertViewController.title = "Support"
                                                alertViewController.message = response.result.value?.message
                                                
                                                // Customize appearance as desired
                                                alertViewController.buttonCornerRadius = 20.0
                                                alertViewController.view.tintColor = self.view.tintColor
                                                alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                alertViewController.swipeDismissalGestureEnabled = true
                                                alertViewController.backgroundTapDismissalGestureEnabled = true
                                                alertViewController.buttonColor = UIColor.red
                                                // Add alert actions
                                                
                                                
                                                let cancelAction = NYAlertAction(
                                                    title: "OK",
                                                    style: .cancel,
                                                    handler: { (action: NYAlertAction!) -> Void in
                                                        
                                                        self.dismiss(animated: true, completion: nil)
                                                }
                                                )
                                                
                                                alertViewController.addAction(cancelAction)
                                                
                                                // Present the alert view controller
                                                self.present(alertViewController, animated: true, completion: nil)
                                            }
                                            
                                            self.hideLoader()
                                            
                                        case .failure(let error):
                                            
                                            print(error)
                                            self.view.makeToast(error.localizedDescription)
                                            self.hideLoader()
                                        }
                                        
                                }
                                
                                
                                
                                
                                
                                
                            }
                            else{
                                self.DocumentURL_API = ""
                                self.view.makeToast("Error: \(responseMessage!)")
                                self.hideLoader()
                                
                                
                            }
                            
                            
                            
                            
                            
                            //                        let Timestamp = Date()
                            //                        let dateFormatter = DateFormatter()
                            //                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                            //                        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
                            //                        let utctimeStamp = dateFormatter.string(from: Timestamp)
                            
                            
                        }
                    }
                    
                    
                    
                case .failure(let encodingError):
                    //self.delegate?.showFailAlert()
                    print(encodingError)
                    self.DocumentURL_API = ""
                    self.view.makeToast("Error: \(encodingError)")
                    
                }
            }
        }

         else   if videoURL != nil {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMddyyyyhhmm"
                let dateInFormat = dateFormatter.string(from: NSDate() as Date)
                
            
            compressVideoURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mov")
//            compressVideo(inputURL: videoURL, outputURL: compressVideoURL) { exportSession in
//                 guard let session = exportSession else {
//                    self.compressVideoURL = exportSession?.outputURL
//                    print("compressed")
//                               return
//            }
//            }
            
            let urlAsset = AVURLAsset(url: videoURL, options: nil)
                   guard let exportSession = AVAssetExportSession(asset: urlAsset,
                                                                  presetName: AVAssetExportPresetLowQuality) else {
                     //  handler(nil)

                       return
                   }

                   exportSession.outputURL = compressVideoURL
                   exportSession.outputFileType = .mov
                   exportSession.exportAsynchronously {
                       self.compressVideoURL = exportSession.outputURL
                     //  handler(exportSession)
                                print("\(APIUrl.ZIGTARCAPI)User/DocumentUpload?Username=\((userprofilename))&Profilename=\((userprofilename))")
                    
                                    Alamofire.upload(multipartFormData: { (multipartFormData) in
                                        multipartFormData.append(self.compressVideoURL!, withName: "video_path", fileName: "\(dateInFormat).mov", mimeType: "video/mov")
                    
                                    }, to:"\(APIUrl.ZIGTARCAPI)User/DocumentUpload?Username=\((userprofilename))&Profilename=\((userprofilename))")
                    
                                    { (result) in
                                        switch result {
                                        case .success(let upload, _, _):
                    
                                            upload.uploadProgress(closure: { (Progress) in
                                                print("Upload Progress: \(Progress.fractionCompleted)")
                                            })
                    
                                            upload.responseJSON { response in
                                                dump(response)
                                                if let ResultValue = response.result.value as? [String:Any] {
                                                    let responseMessage = ResultValue["Message"] as? String
                                                    if (responseMessage == "Ok") {
                                                        self.DocumentURL_API = (ResultValue["DocPath"]! as? String)!
                                                        print(self.DocumentURL_API)
                                                        self.view.makeToast("Your document has been successfully uploaded.")
                    
                    
                                                        print("******* Connect *****")
                    
                    
                                                        let accessToken = UserDefaults.standard.value(forKey: "accessToken")
                                                        //userEmail
                                                        // let emailid = UserDefaults.standard.string(forKey: "userEmail")! as NSString
                                                        //  print("\(emailid)")
                                                        let dataDic : NSMutableDictionary = [
                                                            "token": accessToken!,
                                                            // "issuetype":"Issue/Support iOS",
                                                            "link": self.DocumentURL_API ,
                                                            "description": secondMessage,
                                                            "Callback":"false",
                                                            "emailid":emailMessage,
                                                            "agencyid":1,
                                                            "Latitude": Latitude,
                                                            "Longitude": Longitude,
                                                            "Address": Address,
                                                            "Coach": Coach,
                                                            "Incidenttime": Incidenttime,
                                                            "Phone": Phone,
                                                            "T_Referenceno": T_Referenceno,
                                                            "T_Faretype": T_Faretype,
                                                            "T_Noofticket": T_Noofticket,
                                                            "T_Errormsgorsub": T_Errormsgorsub,
                                                            "T_Ticketid": T_Ticketid,
                                                            "T_Purchasedate": T_Purchasedate,
                                                            "T_Expirydate": T_Expirydate,
                                                            "issuetype":self.issuetypeString
                    
                                                        ]
                    
                    
                    
                    
                                                        let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
                                                        let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
                                                        let url2 = URL(string: "\(APIUrl.ZIGTARCAPI)Feedback/ReportIssue/Add")!
                                                        var request = URLRequest(url: url2)
                                                        request.httpMethod = HTTPMethod.post.rawValue
                                                        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
                                                        request.httpBody = dataJson
                    
                    
                                                        Alamofire.request(request)
                                                            .responseObject{ (response: DataResponse<Postpayment>) in
                                                                switch response.result {
                                                                case .success:
                                                                    dump(response.result.value)
                                                                    if response.result.value?.message == "Issue added Successfully"  || response.result.value?.message == "Your feedback has been successfully received." {
                                                                        //                                                    self.emailsending(Username: userprofilename as String, Email: emailMessage as String, message: secondMessage as String, Url: self.DocumentURL_API
                                                                        self.emailsending(Username: userprofilename as String, Email: emailMessage as String, message: secondMessage as String, Url: self.DocumentURL_API, Address: Address as String, Coach: Coach as String, Incidenttime: Incidenttime as String, Phone: Phone as String, Longitude: Longitude as String, Latitude: Latitude as String, T_Referenceno: T_Referenceno as String, T_Faretype: "\(T_Faretype)", T_Noofticket: "\(T_Noofticket)", T_Errormsgorsub: T_Errormsgorsub as String, T_Ticketid: "\(T_Ticketid)", T_Purchasedate: T_Purchasedate as String, T_Expirydate: T_Expirydate as String)
                                                                        self.hideLoader()
                                                                        //  self.screenshot.image = nil
                                                                        //self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                                                                        // self.view.makeToast(response.result.value?.message)
                    
                                                                        self.hideLoader()
                                                                        let alertViewController = NYAlertViewController()
                                                                        alertViewController.title = "Thank you!"
                                                                        alertViewController.message = "Your message has been successfully sent.\nWe will be in touch shortly!"
                    
                                                                        // Customize appearance as desired
                                                                        alertViewController.buttonCornerRadius = 20.0
                                                                        alertViewController.view.tintColor = self.view.tintColor
                                                                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                        alertViewController.swipeDismissalGestureEnabled = true
                                                                        alertViewController.backgroundTapDismissalGestureEnabled = true
                                                                        alertViewController.buttonColor = UIColor.red
                                                                        // Add alert actions
                    
                    
                                                                        let cancelAction = NYAlertAction(
                                                                            title: "OK",
                                                                            style: .cancel,
                                                                            handler: { (action: NYAlertAction!) -> Void in
                                                                                self.dismiss(animated: true, completion: nil)
                    
                                                                                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                                                                                NotificationCenter.default.post(name: Notification.Name("NotificationForUpdateTicketList"), object: nil)

                                                                                self.navigationController?.popViewController(animated: true)
                    
                                                                                //                                                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                                                                //                                                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                                                                //
                                                                                //                                                            self.navigationController?.pushViewController(nextViewController, animated: true)
                    
                    
                                                                        }
                                                                        )
                    
                                                                        alertViewController.addAction(cancelAction)
                    
                                                                        // Present the alert view controller
                                                                        self.present(alertViewController, animated: true, completion: nil)
                    
                    
                    
                                                                    }
                                                                    else{
                                                                        //print(response.result.value?.message!)
                                                                        self.hideLoader()
                                                                        //    self.screenshot.image = nil
                                                                        //  self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                                                                        let alertViewController = NYAlertViewController()
                                                                        alertViewController.title = "Support"
                                                                        alertViewController.message = response.result.value?.message
                    
                                                                        // Customize appearance as desired
                                                                        alertViewController.buttonCornerRadius = 20.0
                                                                        alertViewController.view.tintColor = self.view.tintColor
                                                                        alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                                                                        alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                        alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                                                                        alertViewController.swipeDismissalGestureEnabled = true
                                                                        alertViewController.backgroundTapDismissalGestureEnabled = true
                                                                        alertViewController.buttonColor = UIColor.red
                                                                        // Add alert actions
                    
                    
                                                                        let cancelAction = NYAlertAction(
                                                                            title: "OK",
                                                                            style: .cancel,
                                                                            handler: { (action: NYAlertAction!) -> Void in
                    
                                                                                self.dismiss(animated: true, completion: nil)
                                                                        }
                                                                        )
                    
                                                                        alertViewController.addAction(cancelAction)
                    
                                                                        // Present the alert view controller
                                                                        self.present(alertViewController, animated: true, completion: nil)
                                                                    }
                    
                                                                    self.hideLoader()
                    
                                                                case .failure(let error):
                    
                                                                    print(error)
                                                                    self.view.makeToast(error.localizedDescription)
                                                                    self.hideLoader()
                                                                }
                    
                                                        }
                    
                    
                    
                    
                    
                    
                                                    }
                                                    else{
                                                        self.DocumentURL_API = ""
                                                        self.view.makeToast("Error: \(responseMessage!)")
                                                        self.hideLoader()
                    
                    
                                                    }
                    
                    
                    
                    
                    
                                                    //                        let Timestamp = Date()
                                                    //                        let dateFormatter = DateFormatter()
                                                    //                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                                                    //                        dateFormatter.timeZone = TimeZone(abbreviation: "EST")
                                                    //                        let utctimeStamp = dateFormatter.string(from: Timestamp)
                    
                    
                                                }
                                            }
                    
                    
                    
                                        case .failure(let encodingError):
                                            //self.delegate?.showFailAlert()
                                            print(encodingError)
                                            self.DocumentURL_API = ""
                                            self.view.makeToast("Error: \(encodingError)")
                    
                                        }
                                    }
                    
                    
                    
            }
         
                    print(self.compressVideoURL!);

            }
        else{
            
            
            let accessToken = UserDefaults.standard.value(forKey: "accessToken")
            
            let dataDic : NSMutableDictionary = [
                "Token": accessToken!,
                
                "Link": self.DocumentURL_API ,
                "Description": secondMessage,
                "AgencyId":1,
                "Latitude": Latitude,
                "Longitude": Longitude,
                "Address": Address,
                "Coach": Coach,
                "Incidenttime": Incidenttime,
                "Phone": Phone,
                "T_Referenceno": T_Referenceno,
                "T_Faretype": T_Faretype,
                "T_Noofticket": T_Noofticket,
                "T_Errormsgorsub": T_Errormsgorsub,
                "T_Ticketid": T_Ticketid,
                "T_Purchasedate": T_Purchasedate,
                "T_Expirydate": T_Expirydate,
                "issuetype":issuetypeString,
                "emailid":emailMessage
                
                
                
            ]
            
            let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
            let url2 = URL(string: "\(APIUrl.ZIGTARCAPI)Feedback/ReportIssue/Add")!
            var request = URLRequest(url: url2)
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = dataJson
            
            
            Alamofire.request(request)
                .responseObject{ (response: DataResponse<Postpayment>) in
                    switch response.result {
                    case .success:
                        dump(response.result.value)
                        if response.result.value?.message == "Issue added Successfully" || response.result.value?.message == "Your feedback has been successfully received." {
                            
                            // self.emailsending(Username: userprofilename as String, Email: emailMessage as String, message: secondMessage as String, Url: self.DocumentURL_API)
                            
                            self.emailsending(Username: userprofilename as String, Email: emailMessage as String, message: secondMessage as String, Url: self.DocumentURL_API, Address: Address as String, Coach: Coach as String, Incidenttime: Incidenttime as String, Phone: Phone as String, Longitude: Longitude as String, Latitude: Latitude as String, T_Referenceno: T_Referenceno as String, T_Faretype: "\(T_Faretype)", T_Noofticket: "\(T_Noofticket)", T_Errormsgorsub: T_Errormsgorsub as String, T_Ticketid: "\(T_Ticketid)", T_Purchasedate: T_Purchasedate as String, T_Expirydate: T_Expirydate as String)
                            
                            
                            self.hideLoader()
                            // if self.screenshot
                            // self.screenshot.image = nil
                            // self.imageUploadBtn.setTitle("Upload Screenshot", for: .normal)
                            // self.view.makeToast(response.result.value?.message)
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Thank you!"
                            alertViewController.message = "Your message has been successfully sent.\nWe will be in touch shortly"
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            //  self.screenshot.image = nil
                            // self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    self.dismiss(animated: true, completion: nil)
                                    
                                    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                                    NotificationCenter.default.post(name: Notification.Name("NotificationForUpdateTicketList"), object: nil)

                                    self.navigationController?.popViewController(animated: true)
                                    
                                    //                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                    //                                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                    //
                                    //                                        self.navigationController?.pushViewController(nextViewController, animated: true)
                                    
                                    
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                            
                            
                            
                        }
                        else{
                            //   self.screenshot.image = nil
                            //  self.imageUploadBtn.setTitle("   Upload Screenshot", for: .normal)
                            
                            //print(response.result.value?.message!)
                            self.hideLoader()
                            let alertViewController = NYAlertViewController()
                            alertViewController.title = "Support"
                            alertViewController.message = response.result.value?.message
                            
                            // Customize appearance as desired
                            alertViewController.buttonCornerRadius = 20.0
                            alertViewController.view.tintColor = self.view.tintColor
                            alertViewController.titleFont = UIFont.setTarcBold(size: 19.0)
                            alertViewController.messageFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.cancelButtonTitleFont = UIFont.setTarcSemiBold(size: 16.0)
                            alertViewController.swipeDismissalGestureEnabled = true
                            alertViewController.backgroundTapDismissalGestureEnabled = true
                            alertViewController.buttonColor = UIColor.red
                            // Add alert actions
                            
                            
                            let cancelAction = NYAlertAction(
                                title: "OK",
                                style: .cancel,
                                handler: { (action: NYAlertAction!) -> Void in
                                    
                                    self.dismiss(animated: true, completion: nil)
                            }
                            )
                            
                            alertViewController.addAction(cancelAction)
                            
                            // Present the alert view controller
                            self.present(alertViewController, animated: true, completion: nil)
                        }
                        
                        self.hideLoader()
                        
                    case .failure(let error):
                        
                        print(error)
                        self.view.makeToast(error.localizedDescription)
                        self.hideLoader()
                    }
                    //self.sendEmail()
                    
            }
            
            
        }
    }
    
    @objc func datePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        fourthText.text = dateFormatter.string(from: sender.date)
        
    }
    @objc func PurchasedatePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        thirdText.text = dateFormatter.string(from: sender.date)
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if responseVarAddress.count != 0 {
            return responseVarAddress.count
        }
        else if AddressSearch.count != 0 {
            return AddressSearch.count
        }
        else{
            return 0
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.responseVarAddress.count > indexPath.row || self.AddressSearch.count > indexPath.row
        {
            
            if self.responseVarAddress.count != 0 {
                
                //                    let PlaceLat = self.responseVarAddress[indexPath.row].Lat
                //                    let PlaceLong = self.responseVarAddress[indexPath.row].Long
                guard let PlaceLat = self.responseVarAddress[indexPath.row].Lat else { return }
                
                let PlaceLatDouble = Double(PlaceLat)
                
                guard let PlaceLong = self.responseVarAddress[indexPath.row].Long else { return }
                
                let PlaceLongDouble = Double(PlaceLong)
                
                DispatchQueue.main.async {
                    //  self.sourcePlace = (self.autoFillArray[indexPath.row] as? GMSPlace)
                    
                    
                    let latMaker = PlaceLatDouble
                    let lngMaker = PlaceLongDouble
                    self.latitudeString = PlaceLat
                    self.logtitudeString = PlaceLong
                    
                    print(latMaker!,lngMaker!)
                    //                    self.source_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                    //                    self.source_marker.title = self.responseVarAddress[indexPath.row].FullAddress
                    //
                    //                    self.source_marker.icon = UIImage(named: "source_Marker")
                    //                    self.source_marker.map = self.mapBackgorundView
                    //                    self.sourceLatTrip = PlaceLatDouble!
                    //                    self.sourceLngTrip = PlaceLongDouble!
                    //                    self.sourceStringTrip = self.responseVarAddress[indexPath.row].placeID!
                    //                    self.isCurrentLoc = false;
                    self.firstText.text = self.responseVarAddress[indexPath.row].FullAddress
                    self.firstText.resignFirstResponder()
                    self.favoriteTableView.isHidden = true
                    
                    //  self.destinationTextField.becomeFirstResponder()
                    
                    //                    self.markersArray.append(State(name: self.responseVarAddress[indexPath.row].FullAddress!, long: CLLocationDegrees(PlaceLatDouble!), lat: CLLocationDegrees(PlaceLongDouble!), placeId:" ", distanceVale: "", formattedAddress:  self.responseVarAddress[indexPath.row].FullAddress!, type: "", imageUrl: ""))
                    //
                    //
                    //
                    //                    self.resultBackView.isHidden = true
                    //                    self.CheckFavTrip()
                    //                    var camera = GMSCameraPosition()
                    //                    if UIDevice.current.userInterfaceIdiom == .pad{
                    //                        camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 12.0)
                    //                    }
                    //
                    //                    else
                    //                    {
                    //                        camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 14.0)
                    //                    }
                    //                    self.mapBackgorundView?.animate(to: camera)
                    //                    self.typeString = "bus_station"
                    //                    self.getbusStops(type: self.typeString)
                    
                }
                
            }
            else if self.AddressSearch.count != 0 {
                let Place_ID = AddressSearch[indexPath.row].place_id
                //  pPlaceID = "ChIJXbmAjccVrjsRlf31U1ZGpDM"
                self.placesClient.lookUpPlaceID(Place_ID!, callback: { (place, error) -> Void in
                    
                    if let error = error {
                        print("lookup place id query error: \(error.localizedDescription)")
                        return
                    }
                    
                    if let place = place {
                        print("Place name \(place.name)")
                        print("Place address \(place.formattedAddress!)")
                        print("Place placeID \(place.placeID)")
                        // print("Place attributions \((place.attributions)!)")
                        print("\(place.coordinate.latitude)")
                        print("\(place.coordinate.longitude)")
                        self.latitudeString = "\(place.coordinate.latitude)"
                        self.logtitudeString = "\(place.coordinate.longitude)"
                        self.sourcePlace = place
                        DispatchQueue.main.async {
                            //  self.sourcePlace = (self.autoFillArray[indexPath.row] as? GMSPlace)
                            //   print(self.sourcePlace?.coordinate ?? "nil")
                            
                            
                            //                            let latMaker = self.sourcePlace?.coordinate.latitude
                            //                            let lngMaker = self.sourcePlace?.coordinate.longitude
                            //                            print(latMaker!,lngMaker!)
                            //                            self.source_marker.position = CLLocationCoordinate2D(latitude: latMaker!, longitude: lngMaker!)
                            //                            self.source_marker.title = self.sourcePlace?.name
                            //
                            //                            self.source_marker.icon = UIImage(named: "source_Marker")
                            //                            self.source_marker.map = self.mapBackgorundView
                            //                            self.sourceLatTrip = (self.sourcePlace?.coordinate.latitude)!
                            //                            self.sourceLngTrip = (self.sourcePlace?.coordinate.longitude)!
                            //                            self.sourceStringTrip = (self.sourcePlace?.placeID)!
                            //                            self.isCurrentLoc = false;
                            self.firstText.text = self.sourcePlace?.name
                            self.firstText.resignFirstResponder()
                            self.favoriteTableView.isHidden = true
                            //  self.destinationTextField.becomeFirstResponder()
                            
                            //       self.markersArray.append(State(name: self.sourcePlace!.name!, long: CLLocationDegrees((self.sourcePlace?.coordinate.latitude)!), lat: CLLocationDegrees((self.sourcePlace?.coordinate.longitude)!), placeId:" ", distanceVale: "", formattedAddress:  (self.sourcePlace?.name)!, type: "", imageUrl: ""))
                            
                            
                            
                            //                            self.resultBackView.isHidden = true
                            //                            self.CheckFavTrip()
                            //                            var camera = GMSCameraPosition()
                            //                            if UIDevice.current.userInterfaceIdiom == .pad{
                            //                                camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 12.0)
                            //                            }
                            //
                            //                            else
                            //                            {
                            //                                camera = GMSCameraPosition.camera(withLatitude: latMaker!, longitude: lngMaker!, zoom: 14.0)
                            //                            }
                            //                            self.mapBackgorundView?.animate(to: camera)
                            //                            self.typeString = "bus_station"
                            //                            // self.getPlaces(type: self.typeString)
                            //                            self.getbusStops(type: self.typeString)
                            
                        }
                        
                    } else {
                        print("No place details for \((Place_ID)!)")
                    }
                })
            }
            
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = FavoriteTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        //        let dict  = arrayHome.object(at: indexPath.row) as! NSDictionary
        //            let nameString = dict .value(forKey: "title") as! NSString
        //        let EventDate = dict.value(forKey: "EventDate") as! NSString
        //        let Locationname = dict.value(forKey: "Locationname") as! NSString
        //        let Language = dict.value(forKey: "Language") as! NSString
        cell.selectionStyle = .none
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        // cell.centerImage.image = UIImage.imag
        
        if responseVarAddress.count != 0 {
            if responseVarAddress.count > indexPath.row
            {
                //               // place = autoFillArray[indexPath.row] as! GMSPlace
                // let AddressString = responseVarAddress[indexPath.row].FullAddress!.components(separatedBy: .punctuationCharacters).joined().components(separatedBy: " ").filter{!$0.isEmpty}
                
                
                cell.centerName.text = "\(responseVarAddress[indexPath.row].FullAddress!)"
                //
                
                print((responseVarAddress[indexPath.row].FullAddress)!)
            }
        }
        else if AddressSearch.count != 0{
            
            cell.centerName.text = "\(AddressSearch[indexPath.row].description!)"
        }
        
        
        //  cell.centerName.text = "dal"
        //  cell.centertimeandlanguage.text = "text"
        
        return cell
        
        
    }
    
    func DataBaseAutocompleteFunc(SearchString:String){
        if SearchString != "" {
            // self.autofillTableView.isHidden = false
            //   self.autofillTableView1.isHidden = false
            self.responseVarAddress.removeAll()
            //    AddressSearch.removeAll()
            
            let SearchStringSpace = SearchString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            Alamofire.request("\(APIUrl.ZIGTARCBASEAPI)tarcapi/api/Map/GetAutoComplete?address=\((SearchStringSpace)!)", method: .get, encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<DataBaseAutoComplete>) in
                    //print(response)
                    switch response.result {
                    case .success:
                        
                        
                        if  response.result.value!.Message == "OK" {
                            if (response.result.value?.Count)! > 0 && (response.result.value?.address!) != nil {
                                self.responseVarAddress = response.result.value!.address!
                                
                            }
                            else{
                                self.googleAutoComplete(SearchAddress: SearchString)
                            }
                            
                        }
                        else{
                            self.googleAutoComplete(SearchAddress: SearchString)
                        }
                        
                        
                        
                        if(self.responseVarAddress.count > 5)
                        {
                            //   self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: self.sourceTextField.frame.size.width, height: 250)
                            //   self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: self.destinationTextField.frame.size.width, height: 250)
                            
                        }
                        else
                        {
                            //     self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: Int(self.sourceTextField.frame.size.width), height: self.responseVarAddress.count*50)
                            //       self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: Int(self.destinationTextField.frame.size.width), height: self.responseVarAddress.count*50)
                        }
                        self.favoriteTableView.reloadData()
                        //   self.autofillTableView1.reloadData()
                        
                    case .failure(let error):
                        print(error)
                        self.googleAutoComplete(SearchAddress: SearchString);
                        
                    }
            }
            
        }
        else{
            //            self.autofillTableView.removeFromSuperview()
            //            self.autofillTableView1.removeFromSuperview()
            //            self.autofillTableView.isHidden = true
            //            self.autofillTableView1.isHidden = true
        }
    }
    func googleAutoComplete(SearchAddress:String){
        let SearchAddressSpace = SearchAddress.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        responseVarAddress.removeAll()
        //    AddressSearch.removeAll()
        print("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(Key.GoogleAPIKey)&components=country:USA&input=\((SearchAddressSpace)!)")
        Alamofire.request("https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(Key.GoogleAPIKey)&components=country:USA&input=\((SearchAddressSpace)!)", method: .get, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<GoogleAutoComplete>) in
                //print(response)
                switch response.result {
                    
                case .success:
                    
                    if response.result.value?.Status == "OK"{
                        
                        self.AddressSearch = response.result.value!.predictions!
                        if(self.AddressSearch.count > 5)
                        {
                            //     self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: self.sourceTextField.frame.size.width, height: 250)
                            //      self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: self.destinationTextField.frame.size.width, height: 250)
                            
                        }
                        else
                        {
                            //     self.autofillTableView.frame = CGRect.init(x:0, y: 0, width: Int(self.sourceTextField.frame.size.width), height: self.AddressSearch.count*50)
                            //      self.autofillTableView1.frame = CGRect.init(x:0, y: 0, width: Int(self.destinationTextField.frame.size.width), height: self.AddressSearch.count*50)
                        }
                        self.favoriteTableView.reloadData()
                        //  self.autofillTableView1.reloadData()
                    }
                    
                case .failure(let error):
                    print(error)
                    
                    
                }
        }
        
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        if textField == phoneText
        {
            let StringN  =  textField.text?.toPhoneNumber()
            textField.text = StringN
        }
        else  if firsttitleName == "Location :"
        {
                            if textField.text!.count == 0

                {
                              favoriteTableView.isHidden = true
            }
            else
                        {
            responseVarAddress.removeAll()
            favoriteTableView.isHidden = false
            self.DataBaseAutocompleteFunc(SearchString: textField.text!)
            }
            
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //Single Ride
        if textField == firstText
        {
            favoriteTableView.isHidden = true
            
        }
        else if textField == thirdText
        {
            if titleName == "Ticket Disappears"
            {
                if textField.text == ""
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
                    textField.text = dateFormatter.string(from: Date())
                }
            }
        }
        else if textField == fourthText
        {
            if titleName == "Ticket Not Generated " ||  titleName == "Transaction Failed" || titleName == "Ticket Refunds"
            {
                
            }
            else
            {
                if textField.text == ""
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
                    textField.text = dateFormatter.string(from: Date())
                }
            }
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.contentInset = UIEdgeInsets.zero;
        
        if (textView.text == "What is this report about ?") {
            textView.text = ""
            textView.textColor = UIColor.black //optional
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == "") {
            textView.text = "What is this report about ?"
            textView.textColor = UIColor.lightGray //optional
        }
        textView.resignFirstResponder()
    }
    
    func currentLocation()
    {
 

        var currentLocation: CLLocation!
        
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locManager.location
        
        
        let url = "https://maps.googleapis.com/maps/api/geocode/json"
        let parameters: Parameters = [
            "latlng": "\(currentLocation.coordinate.latitude),\(currentLocation.coordinate.longitude)",
            "key": Key.GoogleAPIKey
        ]
        
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default)
            .responseObject{ (response: DataResponse<MyLocationAddress>) in
                
                
                switch response.result {
                case .success:
                    // print(response)
                    //to get status code
                    
                    if response.result.value?.results != nil
                    {
                        
                        if(response.result.value?.status == "OK"){
                            
                            let weatherResponse = response.result.value?.results![0]
                            // print(weatherResponse ?? "nil")
                            
                            //   if self.isSourceMarker
                            //   {
                            //                                self.sourceLatTrip =  Double((weatherResponse?.geometry?.location?.lat)!)
                            //                                self.sourceLngTrip =  Double((weatherResponse?.geometry?.location?.lng)!)
                            //                                self.sourceStringTrip = (weatherResponse?.placeId)!
                            self.latitudeString = "\((weatherResponse?.geometry?.location?.lat)!)"
                            self.logtitudeString = "\((weatherResponse?.geometry?.location?.lng)!)"
                            self.firstText.text = "\((weatherResponse?.formattedAddress)!)"
                            
                            //   self.source_marker.title = (weatherResponse?.formattedAddress)!
                            //Arun
                            //                                          self.markersArray.append(State(name: (weatherResponse?.formattedAddress)!, long: CLLocationDegrees( self.sourceLatTrip), lat: CLLocationDegrees(self.sourceLngTrip ), placeId:" ", distanceVale: "", formattedAddress:  (weatherResponse?.formattedAddress)!, type: "", imageUrl: ""))
                            
                            
                            //    }
                            
                            
                            //  self.getPlaces(type: self.typeString)
                        }
                        
                        
                        
                    }
                    else
                    {
                        
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
        }
        
    }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        if numberOfChars <= 1000
        {
        textViewCount.text = "\(numberOfChars)/1000"
        }
        return numberOfChars < 1000    // 10 Limit Value
    }
}



extension String {
    public func toPhoneNumber() -> String {
        return self.replacingOccurrences(of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: nil)
    }
}
