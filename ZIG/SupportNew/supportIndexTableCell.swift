//
//  supportIndexTableCell.swift
//  ZIG
//
//  Created by Arun pandiyan on 29/06/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class supportIndexTableCell: UITableViewCell {


                    var descriptionLabel: UILabel!
                    var timeLabel: UILabel!
                    var statusLabel: UILabel!
                    var arrowImage: UIImageView!
                    var lineView: UIView!
        var newMessagelabel: UILabel!

                    override func awakeFromNib() {
                        super.awakeFromNib()
                        // Initialization code
                    }
                    
                    override func setSelected(_ selected: Bool, animated: Bool) {
                        super.setSelected(selected, animated: animated)
                        
                        // Configure the view for the selected state
                    }
                    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
                        super.init(style: style, reuseIdentifier: reuseIdentifier)
                        self.createSubViews()
                        self.setupConstraints()
                    }
                    
                    required init?(coder aDecoder: NSCoder) {
                        fatalError("init(coder:) has not been implemented")
                    }
                    func createSubViews() -> Void {

                        
                        descriptionLabel = UILabel()
                        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
                        descriptionLabel.backgroundColor = UIColor.clear
                        descriptionLabel.textColor = UIColor.black
                        descriptionLabel.font = UIFont.setTarcBold(size: 18)
                        self .addSubview(descriptionLabel)
                        
                        
                        

                        timeLabel = UILabel()
                        timeLabel.translatesAutoresizingMaskIntoConstraints = false
                                timeLabel.backgroundColor = UIColor.clear
                                timeLabel.textColor = UIColor.black
                        timeLabel.font = UIFont.setTarcRegular(size: 16)
                        self .addSubview(timeLabel)
                        
                        newMessagelabel = UILabel()
                                              newMessagelabel.translatesAutoresizingMaskIntoConstraints = false
                                              newMessagelabel.backgroundColor = UIColor.red
                                              newMessagelabel.textColor = UIColor.white
                        newMessagelabel.text = "New Message"
                        newMessagelabel.font = UIFont.setTarcHeavy(size: 11)
                        newMessagelabel.textAlignment = .center
                                              self .addSubview(newMessagelabel)
                        
                        arrowImage = UIImageView()
                        arrowImage.translatesAutoresizingMaskIntoConstraints = false
                        let templateImage = UIImage.init(named: "Right arrow dsds")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                        arrowImage.image = templateImage
                        arrowImage.tintColor = UIColor.lightGray
                        self .addSubview(arrowImage)

                        statusLabel = UILabel()
                        statusLabel.translatesAutoresizingMaskIntoConstraints = false
                        statusLabel.backgroundColor = UIColor.clear
                        statusLabel.textAlignment = .right
                        statusLabel.font = UIFont.setTarcBold(size: 18)
                        self .addSubview(statusLabel)
                        
                        lineView = UIView()
                        lineView.translatesAutoresizingMaskIntoConstraints = false
                        lineView.backgroundColor = UIColor.lightGray
                        self.addSubview(lineView)
                        
                    }
                    func setupConstraints() -> Void {

                   
                        let viewsDict:[String:Any] = [
                                                    "descriptionLabel":descriptionLabel as Any,
                                                        "timeLabel":timeLabel as Any,
                                                        "lineView":lineView as Any,
                                                        "statusLabel":statusLabel,
                                                        "arrowImage":arrowImage,
                                                        "newMessagelabel":newMessagelabel]
                        
                        
                  
                   
                            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-15-[descriptionLabel]-15-|",
                                                                                                       options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                       metrics: nil,
                                                                                                       views: viewsDict))
                    
                       
                        
                         NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-15-[timeLabel]-5-[statusLabel(60)]-40-|",
                                                                                                       options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                       metrics: nil,
                                                                                                       views: viewsDict))
                        
                        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[lineView]|",
                                                                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                                              metrics: nil,
                                                                                                                              views: viewsDict))
                        
                        
                        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:[arrowImage(30)]-10-|",
                                                                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                                              metrics: nil,
                                                                                                                              views: viewsDict))
                        
                        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:[newMessagelabel(100)]-10-|",
                                                                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                                              metrics: nil,
                                                                                                                              views: viewsDict))
                        
                        
                                  NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[descriptionLabel(35)]",
                                                                                                                               options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                                               metrics: nil,
                                                                                                                               views: viewsDict))
                        
                        

                        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[statusLabel(35)]",
                                                                                                                     options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                                     metrics: nil,
                                                                                                                     views: viewsDict))
                                  
                                  NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[descriptionLabel(35)]-0-[timeLabel(35)]-0-[lineView(1)]",
                                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                              metrics: nil,
                                                                                              views: viewsDict))
                        //
                        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[arrowImage(35)]-0-[newMessagelabel(30)]",
                                                                                                                     options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                                     metrics: nil,
                                                                                                                     views: viewsDict))
                     

                        newMessagelabel.layer.cornerRadius = 15
                        newMessagelabel.layer.masksToBounds = true
                        
                    }

                
    }
