//
//  inboxSupportViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 29/06/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import Toast_Swift
class inboxSupportViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var chatTableView = UITableView()
    var chatArray = [[String: String]]()
    var backView = UIView()
    var vc = AVPlayerLayer()
    var loaderView = UIView()
    var chatTextField = UITextField()
var CaputuredImage = UIImage()
    var supportid = Int()
    var chatBool = Bool()
    var videoOrimage = Bool()
    var videoURL : URL!
    var replayButton = UIButton()
var titleString = String()
    var supportListArray = NSMutableArray()
    var firsttitleName = String()
                                              var thirdTitleName = String()
                                              var fourthTitleName = String()

    /// Back Button Action
    @objc func OnBackClicked() {
           vc.player?.pause()
                  self.navigationController?.popViewController(animated: true)
      }
    
    /// Status Manager
       @objc func statusManager(_ notification: NSNotification) {
            offline.updateUserInterface(withoutLogin: false)
        }
    func chatDesign()
    {
        
        chatTableView = UITableView()
              chatTableView.translatesAutoresizingMaskIntoConstraints = false
              chatTableView.backgroundColor = UIColor.white
              chatTableView.delegate = self
              chatTableView.dataSource = self
              self.view .addSubview(chatTableView)
              chatTableView.separatorStyle = .none
        
        let chatView = UIView()
        chatView.translatesAutoresizingMaskIntoConstraints = false
        chatView.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        self.view .addSubview(chatView)
         chatTextField = UITextField()
        chatTextField.translatesAutoresizingMaskIntoConstraints = false
       
        chatView.addSubview(chatTextField)
        
        let sendButton = UIButton()
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        let templateImage = UIImage.init(named: "Sendarrow")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        sendButton .setImage(templateImage, for: .normal)
        sendButton.tintColor = UIColor.init(red: 50/255, green: 108/255, blue: 175/255, alpha: 1.0)
        sendButton.addTarget(self, action: #selector(sendAction), for: .touchUpInside)
        chatView .addSubview(sendButton)
        if chatBool == true
               {
                   chatTextField.placeholder = "Type a Message"

               }
               else
               {
                   chatTextField.placeholder = "Note: This ticket has been Closed."
                   chatTextField.isUserInteractionEnabled = false
                sendButton.isHidden = true

               }
        
                          let viewsDict:[String:Any] = ["chatView":chatView as Any,
                                                        "chatTextField":chatTextField,
                                                        "sendButton":sendButton,
                                                        "chatTableView":chatTableView]
                          
                          
                          NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[chatView]|",
                                                                                                options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                metrics: nil,
                                                                                                views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[chatTableView]|",
                                                                                                     options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                     metrics: nil,
                                                                                                     views: viewsDict))
        
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-0-[chatTableView]-0-[chatView(60)]-0-|",
                                                                                                       options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                       metrics: nil,
                                                                                                       views: viewsDict))
           
           
           NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[chatTextField]-5-[sendButton(50)]-5-|",
                                                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                    metrics: nil,
                                                                                                    views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:[chatTextField(50)]-10-|",
                                                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                              metrics: nil,
                                                                                                              views: viewsDict))
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:[sendButton(50)]-10-|",
                                                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                              metrics: nil,
                                                                                                              views: viewsDict))
        
        
    }
    @objc func sendAction()
    {
        if chatTextField.text != ""
        {
            SendSupport(message: chatTextField.text!)
        }
        else
        {
           self.view.makeToast("Please Enter Text")

        }
    }
    /// TableView delegates and datasource
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Dict  =  chatArray[indexPath.row] as NSDictionary
                let typeString = Dict.value(forKey: "type") as! NSString
                if indexPath.row == chatArray.count - 1
                {
                }
                else
                {

                }
                switch typeString {

                  case "Sender":
                    let MessageString = Dict.value(forKey: "Message") as! NSString
                    let linkString = Dict.value(forKey: "link") as! NSString
                    
                     let cell = inboxFirstMsgTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    cell.backgroundColor = UIColor.white
                    cell.ScreenShotimage.backgroundColor = UIColor.white
                    if linkString.length > 0
                    {
                        
                       
                        var linkStr = String()
linkStr = linkString as String
                        let lastWord = linkStr.suffix(3)       // world"
print(lastWord)
linkStr = "\(lastWord)"
                        if linkStr == "mov" || linkStr == "mp4"
                        {
                             videoOrimage = true
                            let url = URL(string: linkString as String)!

                            videoURL = url
                      let asset = AVAsset(url: videoURL)
                        let imageGenerator = AVAssetImageGenerator(asset: asset)
                            let time = CMTimeMake(1, 1)
                        var imageRef: CGImage? = nil
                        do {
                            imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
                        } catch {
                            print("catch")
                        }
                        var thumbnail: UIImage? = nil
                        if let imageRef = imageRef {
                            thumbnail = UIImage(cgImage: imageRef)
                            cell.ScreenShotimage.image = thumbnail
                        }
                            
                            
                            cell.setupConstraints(image: true)
                        }
                        else if linkStr == "zip"
                        {
                            
                        }
                        else
                        {
                             videoOrimage = false
                        let url = URL(string:linkString as String)
                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                        cell.ScreenShotimage.image = UIImage(data: data!)
                        
                        
                        self.CaputuredImage = cell.ScreenShotimage.image!
                                                          cell.setupConstraints(image: true)
                        }

                    }
                    else
                    {
                        cell.setupConstraints(image: false)

                    }
                    cell.descriptionLabel.font = UIFont.setTarcRegular(size: 16)
                    cell.descriptionLabel.attributedText = attributedText(minStr: MessageString)
//                    cell.descriptionLabel?.text = MessageString as String
                    cell.descriptionLabel?.numberOfLines = 0
                    cell.isUserInteractionEnabled = true
                    cell.ScreenShotimage.isUserInteractionEnabled = true
          let tapGesture = UITapGestureRecognizer(target: self, action: #selector(myviewTapped(_:)))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
                    cell.ScreenShotimage.addGestureRecognizer(tapGesture)

                        return cell
                    case "Receiver":
                                      let cell = ResponseTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                                      let MessageString = Dict.value(forKey: "Message") as! NSString

                                                              cell.selectionStyle = UITableViewCell.SelectionStyle.none
                                                             cell.backgroundColor = UIColor.clear
                                      cell.descriptionLabel?.text = MessageString as String
                                      cell.timeLabel.text = ""

                                                             cell.descriptionLabel?.numberOfLines = 0
                                                             return cell
                  default:
                      let cell = ResponseTableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
                                       cell.selectionStyle = UITableViewCell.SelectionStyle.none
                                      cell.backgroundColor = UIColor.clear
                                      cell.descriptionLabel?.text = "Transit Authority of River City (TARC), the public transportation system serving the Greater Louisville Region, provides over 12.5 million passenger trips covering more than 12 million miles annually. TARC runs 43 routes in five counties across two states, owns and operates 102 paratransit vehicles and 223 buses. TARC averages over 41,000 daily riders. Of all trips taken, 83% are for work or school."
                                      cell.descriptionLabel?.numberOfLines = 0
                                      return cell
                  }

    }
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
            return chatArray.count

        
           
         }
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.chatArray.count-1, section: 0)
            self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
         backView = UIView()
        backView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        backView.backgroundColor = UIColor.black
        self.view .addSubview(backView)
               self.view .bringSubview(toFront: backView)
        if videoOrimage == true
        {
        let player = AVPlayer(url: videoURL)
         vc = AVPlayerLayer()
        vc.player = player
        vc.frame =  CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            vc.videoGravity = AVLayerVideoGravity.resizeAspect
            NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)

        backView.layer.addSublayer(vc)

        //present(vc, animated: true) {
            vc.player?.play()
      //  }
        }
        else
        {
let fullImageView = UIImageView()
        fullImageView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        fullImageView.image = CaputuredImage
        fullImageView.contentMode = UIViewContentMode.scaleAspectFit
        backView .addSubview(fullImageView)
        fullImageView.isUserInteractionEnabled = true
        }
        let closeButton = UIButton()
        closeButton.frame = CGRect.init(x: self.view.frame.size.width - 100, y: 10, width: 90, height: 50)
        closeButton.setTitle("Close", for: .normal)
        closeButton.layer.borderWidth = 1
        closeButton.layer.borderColor = UIColor.white.cgColor
        closeButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        backView.addSubview(closeButton)
        backView .bringSubview(toFront: closeButton)
    }
    @objc func closeAction()
    {
        if videoOrimage == true
               {
                 vc.player?.pause()

        }
        
        backView.isHidden = true
    }
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
         replayButton = UIButton()
        replayButton.frame = CGRect.init(x: backView.center.x-100, y: backView.center.y, width: 200, height: 50)
        replayButton.setTitle("Replay", for: .normal)
        replayButton.layer.borderWidth = 1
           replayButton.layer.borderColor = UIColor.white.cgColor
           replayButton.addTarget(self, action: #selector(replayAction), for: .touchUpInside)
           backView.addSubview(replayButton)
           backView .bringSubview(toFront: replayButton)
        replayButton.layer.cornerRadius = 25
        replayButton.layer.masksToBounds = true

    }
   @objc func replayAction()
    {
        replayButton.isHidden = true
        vc.player!.seek(to: kCMTimeZero)
        vc.player?.play()
        replayButton.isHidden = true

    }
    func getSupportList()
    {
        self.showLoader()
                     let userDefaults1 = UserDefaults.standard
        
                   var userName = ""
                   if userDefaults1.value(forKey: "userName") == nil
                              {
                              userName = ""
                          }
                          else
                          {
                              userName = userDefaults1.value(forKey: "userName") as! String
                          }
        let getUserId = "\(APIUrl.ZIGTARCAPI)Feedback/Supportchat/Get?Supportid=\(supportid)"

                        Alamofire.request(getUserId, method: .get, parameters: nil, encoding: JSONEncoding.default)
                            .responseJSON { response in
                                switch response.result {
                                case .success:


                                    if (response.result.value != nil) {

                                        let responseVar = response.result.value
                                      //  let json = JSON(responseVar!)
                                        let dict  = responseVar as! NSDictionary
                                        
                                        print("\(dict)")
                                        var getArray = NSArray()
                                        getArray = dict.object(forKey: "ReportIssue") as! NSArray
                                        print("\(getArray)")
                                        if getArray.count > 0
                                        {
                                            let dict = getArray.object(at: 0) as! NSDictionary
                                            let name = userName
                                            
                                            var emailID = String()
                                            if (dict.value(forKey: "EmailID") as? String) != nil
                                            {
                                        emailID = dict.value(forKey: "EmailID") as! String

                                            }
                                            var phone = String()

                                            if (dict.value(forKey: "Phone") as? String) != nil
                                                                                       {
                                            phone = dict.value(forKey: "Phone") as! String

                                                        }
                                            
                                            var lcoation = String()
                                            if (dict.value(forKey: "Address") as? String) != nil
                                                                                                                                  {
                                                                                       lcoation = dict.value(forKey: "Address") as! String

                                                                                                   }
                                            
                                            
                                            var coachNo = String()
                                                                                       if (dict.value(forKey: "Coach") as? String) != nil
                                                                                                                                                                             {
                                                                                                                                  coachNo = dict.value(forKey: "Coach") as! String

                                                                                                                                              }
                                            
                                            var dateandtime = String()
                                            if let category = dict["Incidenttime"] as? String  {
                                                dateandtime = dict.value(forKey: "Incidenttime") as! String

                                            }
                                            
                                                var Linkss = String()
                                            Linkss = ""
                                           let  Link = dict.value(forKey: "Link") as! String
                                            if Link.count > 0
                                            {
                                                Linkss = "Attached File:"
                                            }

                                            var message = String()
                                                                                                                                  if (dict.value(forKey: "Description") as? String) != nil
                                                                                                                                                                                                                        {
                                                                                                                                                                                                                message = dict.value(forKey: "Description") as! String

                                            }
                                            
                                            
                                            let dateFormatter2 = DateFormatter()
                                             if dateandtime.count == 23 {
                                                                           dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                                                                       }
                                                                       else if dateandtime.count == 19{
                                                                           dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                                                                       }
                                                                       else if dateandtime.count == 21{
                                                                           dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
                                                                       }
                                                                       else if dateandtime.count == 22{
                                                                           dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
                                                                       }
                                            
                                            let ActivationDatetime = dateFormatter2.date(from:dateandtime)!
                                                      //  dateFormatter2.timeZone = NSTimeZone(name: "EDT") as TimeZone?
                                                        dateFormatter2.dateFormat = "MMM dd , hh:mm a"

                                            var ActivationDatetimeString = dateFormatter2.string(from:ActivationDatetime)
                                            
                                            
                                          
                                            self.firsttitleName = "Location:"
                                            self.thirdTitleName = "Route No:"
                                            self.fourthTitleName = "Incident Date&Time:"
                                            
                                            if self.titleString == "Ticket Not Generated"
                                            {
                                                lcoation = dict.value(forKey: "T_Referenceno") as! String
                                              let T_Faretype = dict.value(forKey: "T_Faretype") as! Int
                                                
                                                if T_Faretype == 9
                                                       {
                                                           coachNo = "Single Ride"
                                                       }
                                                       else if T_Faretype == 10
                                                       {
                                                        coachNo = "24 Hour Pass"
                                                           //24 Hour Pass
                                                       }
                                                       else if T_Faretype == 11
                                                       {
                                                        coachNo = "7 Day Pass"

                                                           
                                                       }
                                                       else if T_Faretype == 12
                                                       {
                                                           coachNo = "30 Day Pass"

                                                       }
                                                
                                                
                                                let T_Noofticket  = dict.value(forKey: "T_Noofticket") as! Int
                                                ActivationDatetimeString = "\(T_Noofticket)"
                                                self.firsttitleName = "Reference No:"
                                                self.thirdTitleName = "Fare Type:"
                                                self.fourthTitleName = "No of Ticket:"
                                                
                                                self.chatArray = [
                                                      [
                                                          "Message" : "Name: \(name)\n\nEmail:  \(emailID)\n\nPhone: \(phone)\n\n\(self.firsttitleName) \(lcoation)\n\n\(self.thirdTitleName) \(coachNo)\n\n\(self.fourthTitleName) \(ActivationDatetimeString)\n\nMessage: \(message)\n\n\(Linkss)",
                                                        "link": Link,
                                                          "type" : "Sender"
                                                      ]
                                                          ]
                                                
                                            }
                                            else if self.titleString == "Transaction Failed"
                                            {
                                                lcoation = dict.value(forKey: "T_Errormsgorsub") as! String
                                                                                                       let T_Faretype = dict.value(forKey: "T_Faretype") as! Int
                                                                                                                                       
                                                                                                                                       if T_Faretype == 9
                                                                                                                                              {
                                                                                                                                                  coachNo = "Single Ride"
                                                                                                                                              }
                                                                                                                                              else if T_Faretype == 10
                                                                                                                                              {
                                                                                                                                               coachNo = "24 Hour Pass"
                                                                                                                                                  //24 Hour Pass
                                                                                                                                              }
                                                                                                                                              else if T_Faretype == 11
                                                                                                                                              {
                                                                                                                                               coachNo = "7 Day Pass"

                                                                                                                                                  
                                                                                                                                              }
                                                                                                                                              else if T_Faretype == 12
                                                                                                                                              {
                                                                                                                                                  coachNo = "30 Day Pass"

                                                                                                                                              }
                                                                                                                                       
                                                                                                                                       
                                                                                                                                       let T_Noofticket  = dict.value(forKey: "T_Noofticket") as! Int
                                                                                                                                       ActivationDatetimeString = "\(T_Noofticket)"
                                                self.firsttitleName = "Error Message:"
                                                self.thirdTitleName = "Fare Type:"
                                                self.fourthTitleName = "No of Ticket:"
                                                
                                                self.chatArray = [
                                                      [
                                                          "Message" : "Name: \(name)\n\nEmail:  \(emailID)\n\nPhone: \(phone)\n\n\(self.firsttitleName) \(lcoation)\n\n\(self.thirdTitleName) \(coachNo)\n\n\(self.fourthTitleName) \(ActivationDatetimeString)\n\nMessage: \(message)\n\n\(Linkss)",
                                                        "link": Link,
                                                          "type" : "Sender"
                                                      ]
                                                          ]
                                                
                                            }
                                            else if self.titleString == "Onboard Validator Issues"
                                            {
                                                let T_Ticketid = dict.value(forKey: "T_Ticketid") as! Int
                                                lcoation = "\(T_Ticketid)"

                                                
                                                self.firsttitleName = "#Ticket Id:"
                                                
                                                self.chatArray = [
                                                      [
                                                          "Message" : "Name: \(name)\n\nEmail:  \(emailID)\n\nPhone: \(phone)\n\n\(self.firsttitleName) \(lcoation)\n\n\(self.thirdTitleName) \(coachNo)\n\n\(self.fourthTitleName) \(ActivationDatetimeString)\n\nMessage: \(message)\n\n\(Linkss)",
                                                        "link": Link,
                                                          "type" : "Sender"
                                                      ]
                                                          ]
                                                
                                            }
                                                else if self.titleString == "Bug Report"
                                            {
                                                 self.firsttitleName = "Subject:"
                                                lcoation = dict.value(forKey: "T_Errormsgorsub") as! String

                                                self.chatArray = [
                                                      [
                                                        

                                                        
                                                          "Message" : "Name: \(name)\n\nEmail:  \(emailID)\n\nPhone: \(phone)\n\n\(self.firsttitleName) \(lcoation)\n\nMessage: \(message)\n\n\(Linkss)",
                                                        "link": Link,
                                                          "type" : "Sender"
                                                      ]
                                                ]
                                                
                                            }
                                                else if self.titleString == "App Suggestions"
                                                                                      {
                                                                                        
                                                                                        lcoation = dict.value(forKey: "T_Errormsgorsub") as! String

                                                                                        
                                                self.firsttitleName = "Subject:"
                                                                                        
                                                                                        self.chatArray = [
                                                                                              [
                                                                                                  "Message" : "Name: \(name)\n\nEmail:  \(emailID)\n\nPhone: \(phone)\n\n\(self.firsttitleName) \(lcoation)\n\nMessage: \(message)\n\n\(Linkss)",
                                                                                                "link": Link,
                                                                                                  "type" : "Sender"
                                                                                              ]
                                                                                        ]
                                                                                        
                                                                                        
                                                                                      }
                                                else if self.titleString == "Mobile Payment: Other Issue"
                                                                                      {
                                                                  self.firsttitleName = "Subject:"
                                                                                        
                                                                                        lcoation = dict.value(forKey: "T_Errormsgorsub") as! String

                                                                                        self.chatArray = [
                                                                                              [
                                                                                                  "Message" : "Name: \(name)\n\nEmail:  \(emailID)\n\nPhone: \(phone)\n\n\(self.firsttitleName) \(lcoation)\n\nMessage: \(message)\n\n\(Linkss)",
                                                                                                "link": Link,
                                                                                                  "type" : "Sender"
                                                                                              ]
                                                                                        ]
                                                                                        
                                                                                      }
                                            else
                                            {
                                                self.chatArray = [
                                                      [
                                                          "Message" : "Name: \(name)\n\nEmail:  \(emailID)\n\nPhone: \(phone)\n\n\(self.firsttitleName) \(lcoation)\n\n\(self.thirdTitleName) \(coachNo)\n\n\(self.fourthTitleName) \(ActivationDatetimeString)\n\nMessage: \(message)\n\n\(Linkss)",
                                                        "link": Link,
                                                          "type" : "Sender"
                                                      ]
                                                          ]
                                            }
                                            
                                            
                                            
                                            
                                            

                                        
                                        }
                                        
                                     var Supportchat = NSArray()
                                        Supportchat = dict.object(forKey: "Supportchat") as! NSArray
                                        if Supportchat.count > 0
                                        {
                                            for i in Supportchat
                                            {
                                               let dict = i as! NSDictionary
                                                  var message = String()
                                                if (dict.value(forKey: "Message") as? String) != nil
                                                {
                                message = dict.value(forKey: "Message") as! String

                                                }
                                                else
                                                {
                                                        message = ""

                                                }
                                                let Adminorapp = dict.value(forKey: "Adminorapp") as! Int
                                                var typeSting = String()
                                                typeSting = "Receiver"
                                                if Adminorapp == 1
                                                {
                                                    typeSting = "Sender"
                                                }
                                                let status = dict.value(forKey: "Status") as! Int
                                                let dicts = ["Message" : message, "type" : typeSting,"status": "\(status)", "link": ""] as NSDictionary
                                                self.chatArray.append(dicts as! [String : String])
                                            }
                                        }
                                        
                                        self.chatTableView.reloadData()
                                        self.scrollToBottom()
                                        self.hideLoader()



                                    }
                                case .failure(let error):
                                    print(error)



                                    self.hideLoader()
                                }
                        }
        
    }
    func attributedText(minStr:NSString) -> NSAttributedString {
           
        

        var attributedString = NSMutableAttributedString(string: minStr as String, attributes: [NSAttributedStringKey.font:UIFont.setTarcRegular(size: 16.0)])
    if UIScreen.main.sizeType == .iPhone5{
        attributedString = NSMutableAttributedString(string: minStr as String, attributes: [NSAttributedStringKey.font:UIFont.setTarcRegular(size: 14.0)])
           }
        var boldFontAttribute = [NSAttributedStringKey.font: UIFont.setTarcBold(size: 16.0),NSAttributedStringKey.foregroundColor: UIColor.black]
    if UIScreen.main.sizeType == .iPhone5{
        boldFontAttribute = [NSAttributedStringKey.font: UIFont.setTarcBold(size: 14.0),NSAttributedStringKey.foregroundColor: UIColor.black]
           }
           // Part of string to be bold
           attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Name:"))
           attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Email:"))
           attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Phone:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Location:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Route No:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Incident Date&Time:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Message:"))
        
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Reference No:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Fare Type:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "No of Ticket:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Error Message:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "#Ticket Id:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Attached File:"))
        attributedString.addAttributes(boldFontAttribute, range: minStr.range(of: "Subject:"))

           // 4 Attached file:
           return attributedString
       }
    func SendSupport(message:String)
    {
        self.showLoader()
                     let userDefaults1 = UserDefaults.standard
        
                   var Token = ""

        
        if userDefaults1.value(forKey: "accessToken") == nil
                              {
                              Token = ""
                          }
                          else
                          {
                              Token = userDefaults1.value(forKey: "accessToken") as! String
                          }
        
     
        let dataDic :  Parameters  = [
                  "Supportid": "\(supportid)",
                                                  "Subject" : "User",
                                                  "Message" : message,
                                                  "Usertoken" : Token,
                                                  "Adminorapp":"1"
               ]
        let getUserId = "\(APIUrl.ZIGTARCAPI)Feedback/Supportchat/Add"

        
        
        
        let dataJson = try? JSONSerialization.data(withJSONObject: dataDic, options: JSONSerialization.WritingOptions.prettyPrinted)
        let dataJsonStrin = NSString(data: dataJson!, encoding: String.Encoding.utf8.rawValue)
        print(dataJsonStrin)
        let url2 = URL(string: getUserId)!
        var request = URLRequest(url: url2)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = dataJson
        
        
        Alamofire.request(request)
            .responseObject{ (response: DataResponse<Postpayment>) in
                switch response.result {
                case .success:
                    dump(response.result.value)
                    self.chatTextField.text = ""
                    self.hideLoader()
                    self.getSupportList()
                    
                case .failure(let error):
                    
                    print(error)
                   
                }
                
        }
        
   
        
    }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        analytics.GetPageHitCount(PageName: "Support-Index")
                      NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
                      offline.updateUserInterface(withoutLogin: false)
                      self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
                      self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
                      self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
                      self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
                     
        
        if titleString == "TNG"
               {
                   titleString = "Ticket Not Generated"
               }
               else if titleString == "TF"
               {
                   titleString = "Transaction Failed"
                   
               }
               else if titleString == "TD"
               {
                   titleString = "Ticket Disappears"
                   
               }
               else if titleString == "OVI"
               {
                   titleString = "Onboard Validator Issues"
                   
               }
               else if titleString == "TOI"
               {
                   titleString = "Mobile Payment: Other Issue"
                   
               }
               else if titleString == "AS"
               {
                    titleString = "App Suggestions"
               }
               else if  titleString == "BR"
               {
                   titleString = "Bug Report"
               }
              
               else if titleString == "OI"
               {
                   titleString = "Other Issue"
                   
               }
               else if titleString ==  "LAF"
               {
                   titleString = "Lost & Found"
               }
                   
               else if titleString == "BT"
               {
                   titleString = "Bus Timings"
                   
               }
                   else if titleString == "SER"
                   {
                       titleString = "Service Requests"
                       
                   }
            else if titleString == "Sa" || titleString == "SA"
                                              {
                                                  titleString = "Safety"
                                                  
                                              }
               else
               {
                //   titleString = "Other Issue"
                   
               }
        
        
        
        self.title = titleString
                      
                      let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                                       style: UIBarButtonItemStyle.done ,
                                                       target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

                       self.navigationItem.leftBarButtonItem = backButton
        let now = Date()
                 let formatter = DateFormatter()
                 formatter.timeZone = TimeZone.current
                 formatter.dateFormat = "hh:mm:aa"
                 let dateString = formatter.string(from: now)
               print(dateString)
        UIApplication.shared.keyWindow?.backgroundColor = UIColor.white


        self.chatDesign()
        self.getSupportList()
        // Do any additional setup after loading the view.
    }
    


}

