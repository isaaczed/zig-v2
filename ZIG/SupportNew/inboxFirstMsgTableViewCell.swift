//
//  inboxFirstMsgTableViewCell.swift
//  ZIG
//
//  Created by Arun pandiyan on 30/06/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class inboxFirstMsgTableViewCell: UITableViewCell {
     var backView: UIView!
        var AutoWidth =  CGFloat()
                var descriptionLabel: UILabel!
                var ScreenShotimage: UIImageView!

        override func layoutSubviews() {
            super.layoutSubviews()

            backView.roundCorners(corners: [.topLeft, .topRight, .bottomRight], radius: 10)
        }
                override func awakeFromNib() {
                    super.awakeFromNib()
                    // Initialization code
                }
                
                override func setSelected(_ selected: Bool, animated: Bool) {
                    super.setSelected(selected, animated: animated)
                    
                    // Configure the view for the selected state
                }
                override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
                    super.init(style: style, reuseIdentifier: reuseIdentifier)
                    self.createSubViews()
                }
                
                required init?(coder aDecoder: NSCoder) {
                    fatalError("init(coder:) has not been implemented")
                }
                func createSubViews() -> Void {
                    
                    backView = UIView()
                    backView.translatesAutoresizingMaskIntoConstraints = false
                    backView.backgroundColor = UIColor.init(red: 226/255, green: 232/255, blue: 238/255, alpha: 1.0)
                    self .addSubview(backView)
                
                    
                    descriptionLabel = UILabel()
                    descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
                    descriptionLabel.backgroundColor = UIColor.clear
                    descriptionLabel.textColor = UIColor.black
                    backView .addSubview(descriptionLabel)
                    descriptionLabel.numberOfLines = 0
                    descriptionLabel.sizeToFit()

                    ScreenShotimage = UIImageView()
                    ScreenShotimage.translatesAutoresizingMaskIntoConstraints = false
                        
                    backView .addSubview(ScreenShotimage)
                    
                    
                }
    func setupConstraints(image:Bool) -> Void {
                    var backViewWidth = Int()
                    AutoWidth  = descriptionLabel.size.width
                    switch UIDevice.current.userInterfaceIdiom {
                    case .phone:
                           // It's an iPhone
                        backViewWidth = 300
                        break
                    case .pad:
                           // It's an iPad (or macOS Catalyst)
                         if UIScreen.main.sizeType == .iPadPro129{
                            backViewWidth = 1200
                            descriptionLabel.font = UIFont.setTarcRegular(size: 19)

                         }
                         else
                         {
                            backViewWidth = 700

                         }
    break
                    case .unspecified: break
                           // Uh, oh! What could it be?
                    case .tv: break
                        //
                    case .carPlay: break
                        //
                    }
                    
                    let metricsCele:[String:Any] = ["backViewWidth":backViewWidth
                                                    ]
                    
                    
                    let viewsDict:[String:Any] = ["backView":backView as Any,
                                                "descriptionLabel":descriptionLabel as Any,
                                                    "ScreenShotimage":ScreenShotimage as Any]
                    
                    
                    NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[backView(<=backViewWidth)]",
                                                                                          options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                          metrics: metricsCele,
                                                                                          views: viewsDict))
                              
                              NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[backView]-5-|",
                                                                                          options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                          metrics: nil,
                                                                                          views: viewsDict))
               
                        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[descriptionLabel]-10-|",
                                                                                                   options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                   metrics: nil,
                                                                                                   views: viewsDict))
                
                   
                    
                     NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[ScreenShotimage(100)]",
                                                                                                   options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                   metrics: nil,
                                                                                                   views: viewsDict))
                              
                              if image == true
                              {
                              NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[descriptionLabel]-0-[ScreenShotimage(50)]-10-|",
                                                                                          options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                          metrics: nil,
                                                                                          views: viewsDict))
                    
        }
        else
                              {
                                NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[descriptionLabel]-0-[ScreenShotimage(0)]-10-|",
                                                                                                                         options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                                         metrics: nil,
                                                                                                                         views: viewsDict))
        }
                             
                  //backView.layer.cornerRadius = 10
                  //  backView.layer.masksToBounds = false

                }

            

        }
    extension UIView {

        func inboxroundCornerss(_ corners: UIRectCorner, radius: CGFloat) {
             let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
             let mask = CAShapeLayer()
             mask.path = path.cgPath
             self.layer.mask = mask
        }

    }
