//
//  FirstCellTableViewCell.swift
//  ZIG
//
//  Created by Arun pandiyan on 22/04/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class FirstCellTableViewCell: UITableViewCell {

     var backView: UIView!

                var descriptionLabel: UILabel!
                var timeLabel: UILabel!

                override func awakeFromNib() {
                    super.awakeFromNib()
                    // Initialization code
                }
                
                override func setSelected(_ selected: Bool, animated: Bool) {
                    super.setSelected(selected, animated: animated)
                    
                    // Configure the view for the selected state
                }
                override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
                    super.init(style: style, reuseIdentifier: reuseIdentifier)
                    self.createSubViews()
                    self.setupConstraints()
                }
                
                required init?(coder aDecoder: NSCoder) {
                    fatalError("init(coder:) has not been implemented")
                }
                func createSubViews() -> Void {
                    
                    backView = UIView()
                    backView.translatesAutoresizingMaskIntoConstraints = false
                    backView.backgroundColor = UIColor.init(red: 226/255, green: 232/255, blue: 238/255, alpha: 1.0)
                    self .addSubview(backView)
                    
                    descriptionLabel = UILabel()
                    descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
                    descriptionLabel.backgroundColor = UIColor.clear
                    descriptionLabel.textColor = UIColor.black
                    descriptionLabel.font = UIFont.setTarcRegular(size: 16)
                    backView .addSubview(descriptionLabel)

                    timeLabel = UILabel()
                            timeLabel.translatesAutoresizingMaskIntoConstraints = false
                            timeLabel.backgroundColor = UIColor.clear
                            timeLabel.textColor = UIColor.black
                    timeLabel.textAlignment = .right
                    timeLabel.font = UIFont.systemFont(ofSize: 11)
                    timeLabel.isHidden = true
                            backView .addSubview(timeLabel)
                    
                    
                }
                func setupConstraints() -> Void {
                    var backViewWidth = Int()
                    switch UIDevice.current.userInterfaceIdiom {
                    case .phone:
                           // It's an iPhone
                        backViewWidth = 150
                        break
                    case .pad:
                           // It's an iPad (or macOS Catalyst)
                        backViewWidth = 150
    break
                    case .unspecified: break
                           // Uh, oh! What could it be?
                    case .tv: break
                        //
                    case .carPlay: break
                        //
                    }
                    
                    let metricsCele:[String:Any] = ["backViewWidth":backViewWidth
                                                    ]
                    
                    
                    let viewsDict:[String:Any] = ["backView":backView as Any,
                                                "descriptionLabel":descriptionLabel as Any,
                                                    "timeLabel":timeLabel as Any]
                    
                    
                    NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[backView(backViewWidth)]",
                                                                                          options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                          metrics: metricsCele,
                                                                                          views: viewsDict))
                              
                              NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[backView(50)]-5-|",
                                                                                          options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                          metrics: nil,
                                                                                          views: viewsDict))
               
                        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-15-[descriptionLabel(120)]",
                                                                                                   options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                   metrics: nil,
                                                                                                   views: viewsDict))
                
                   
                    
                     NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[timeLabel]-5-|",
                                                                                                   options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                   metrics: nil,
                                                                                                   views: viewsDict))
                              
                              
                              NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[descriptionLabel(40)]-0-[timeLabel(0)]-5-|",
                                                                                          options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                          metrics: nil,
                                                                                          views: viewsDict))
                    
                    
                  backView.layer.cornerRadius = 25
                  backView.layer.masksToBounds = false

                    
                }

            
}
