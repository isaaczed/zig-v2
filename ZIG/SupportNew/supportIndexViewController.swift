//
//  supportIndexViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 29/06/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import NYAlertViewController
class supportIndexViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var raiseComplaintTable = UITableView()
    var loaderView = UIView()
    var supportListArray = NSMutableArray()

    /// Raise A Complaint View Design
    func raiseAComplaintDesign()
    {
       let raiseAComplaintButton = UIButton()
        raiseAComplaintButton.translatesAutoresizingMaskIntoConstraints = false
        raiseAComplaintButton.backgroundColor = UIColor.red
        raiseAComplaintButton .setTitle("Open Support Request", for: .normal)
        raiseAComplaintButton.setImage(UIImage.init(named: "Sendarrow"), for: .normal)
        raiseAComplaintButton.setTitleColor(UIColor.white, for: .normal)
        raiseAComplaintButton.titleLabel?.font = UIFont.setTarcBold(size: 17)
        raiseAComplaintButton.addTarget(self, action: #selector(raiseAComplaintAction), for: .touchUpInside)
        self.view .addSubview(raiseAComplaintButton)
        raiseAComplaintButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        raiseAComplaintButton.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        raiseAComplaintButton.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        raiseAComplaintButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 40)

         
        raiseComplaintTable = UITableView()
           raiseComplaintTable.translatesAutoresizingMaskIntoConstraints = false
           raiseComplaintTable.backgroundColor = UIColor.white
           raiseComplaintTable.delegate = self
           raiseComplaintTable.dataSource = self
           self.view .addSubview(raiseComplaintTable)
           raiseComplaintTable.separatorStyle = .none
                       
                       let viewsDict:[String:Any] = ["raiseAComplaintButton":raiseAComplaintButton as Any,
                                                     "raiseComplaintTable":raiseComplaintTable]
                       
                       
                       NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-20-[raiseAComplaintButton]-20-|",
                                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                             metrics: nil,
                                                                                             views: viewsDict))
        
        
        NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[raiseComplaintTable]|",
                                                                                                 options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                                 metrics: nil,
                                                                                                 views: viewsDict))
                                 
                                 NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[raiseAComplaintButton(50)]-10-[raiseComplaintTable]-10-|",
                                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                             metrics: nil,
                                                                                             views: viewsDict))
    }
    /// TableView delegates and datasource
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 80
    }
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = supportIndexTableCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let dict = self.supportListArray.object(at: indexPath.row) as! NSDictionary
        var descriptionString = String()
       if (dict.value(forKey: "Title") as? String) != nil
       {
         descriptionString = dict.value(forKey: "Title") as! String
        }
        else
       {
        descriptionString = ""
        }
        if descriptionString == "TNG"
                     {
                         descriptionString = "Ticket Not Generated"
                     }
                     else if descriptionString == "TF"
                     {
                         descriptionString = "Transaction Failed"
                         
                     }
                     else if descriptionString == "TD"
                     {
                         descriptionString = "Ticket Disappears"
                         
                     }
                     else if descriptionString == "OVI"
                     {
                         descriptionString = "Onboard Validator Issues"
                         
                     }
                     else if descriptionString == "TOI"
                     {
                         descriptionString = "Mobile Payment: Other Issue"
                         
                     }
                     else if descriptionString == "AS"
                     {
                          descriptionString = "App Suggestions"
                     }
                     else if  descriptionString == "BR"
                     {
                         descriptionString = "Bug Report"
                     }
                    
                     else if descriptionString == "OI"
                     {
                         descriptionString = "Other Issue"
                         
                     }
                     else if descriptionString ==  "LAF"
                     {
                         descriptionString = "Lost & Found"
                     }
                         
                     else if descriptionString == "BT"
                     {
                         descriptionString = "Bus Timings"
                         
                     }
                         else if descriptionString == "SER"
                         {
                             descriptionString = "Service Requests"
                             
                         }
            else if descriptionString == "Sa" || descriptionString == "SA"
                                    {
                                        descriptionString = "Safety"
                                        
                                    }
                     else
                     {
                      //   titleString = "Other Issue"
                         
                     }
        
        
        
        let ReportidString = dict.value(forKey: "Reportid") as! String
        let CreateddateString = dict.value(forKey: "Createddate") as! String
        let Status = dict.value(forKey: "Status") as! Bool



        let ActivationDatetimeString = supportIndexViewController.PDTtoEST(myDate: CreateddateString)
        
        cell.descriptionLabel.text = descriptionString
        
        cell.timeLabel.text = "#\(ReportidString) | \(ActivationDatetimeString)"
        if Status == true
        {
            cell.statusLabel.text = "Active"
            cell.statusLabel.textColor = UIColor.init(red: 0/255, green: 133/255, blue: 0/255, alpha: 1.0)
            
            let ReadBool = dict.value(forKey: "ReadBool") as! Bool
            if ReadBool == true
            {
                cell.newMessagelabel.isHidden  = false

            }
            else
            {
                cell.newMessagelabel.isHidden  = true

            }

        }
        else
        {
            cell.statusLabel.text = "Closed"
            cell.statusLabel.textColor = UIColor.red
            cell.newMessagelabel.isHidden  = true


        }
        return cell

    }
    static func PDTtoEST(myDate:String) -> String{
        
        let dateFormatter = DateFormatter()
        // dateFormatter.locale = Locale(identifier: "en_US_POSIX") // edited
         if myDate.count == 23 {
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
         }
         else if myDate.count == 19{
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
         }
         else if myDate.count == 21{
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
         }
         else if myDate.count == 22{
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS"
         }
         

         dateFormatter.timeZone = NSTimeZone(name: "PST") as TimeZone?
         let date = dateFormatter.date(from: myDate)// create   date from string
         
         // change to a readable time format and change to local time zone

         dateFormatter.dateFormat = "MMM dd , hh:mm a"

         dateFormatter.timeZone = NSTimeZone(name: "America/New_York") as TimeZone?
         let timeStamp = dateFormatter.string(from: date!)
         
         
         return timeStamp
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
           return 1
       }
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
            return supportListArray.count

        
           
         }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        header.backgroundColor = UIColor.init(red: 198/255, green: 198/255, blue: 198/255, alpha: 1.0)
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect.init(x: 15, y: 0, width: header.frame.size.width, height: header.frame.size.height)
        titleLabel.text = "All Conversation threads"
        titleLabel.font = UIFont.setTarcBold(size: 15)
        header.addSubview(titleLabel)
        
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.supportListArray.object(at: indexPath.row) as! NSDictionary
        let ReportidString = dict.value(forKey: "Reportid") as! String
        let Status = dict.value(forKey: "Status") as! Bool
 var Description = String()
      if (dict.value(forKey: "Title") as? String) != nil
      {
        Description = dict.value(forKey: "Title") as! String
       }
       else
      {
       Description = ""
       }
       // supportListArray[indexPath.row]["ReadBool"] = false
        
        let Reportid = dict.value(forKey: "Reportid") as! String
        let Title = dict.value(forKey: "Title") as! String
    let Userid = dict.value(forKey: "Userid") as! Int
        let Reportcount = dict.value(forKey: "Reportcount") as! Int
        let Createddate = dict.value(forKey: "Createddate") as! String
        
        var ReadBool = dict.value(forKey: "ReadBool") as! Bool
        var totalReadCount = dict.value(forKey: "totalReadCount") as! Int

                        if ReadBool == true
                        {
                      ReadBool = false
                            let userDefaults1 = UserDefaults.standard

                            if userDefaults1.value(forKey: "notificationCount") != nil
                                   {
                                    var notifcationInt = userDefaults1.value(forKey: "notificationCount") as! Int
                            notifcationInt = notifcationInt - Reportcount
                                    userDefaults1.setValue(notifcationInt, forKey: "notificationCount")

                                    
                                    
                            }
                            totalReadCount = Reportcount
                            let LocalDict = NSMutableDictionary()
                                                                             LocalDict.setValue(Reportid, forKey: "Reportid")
                                                                            LocalDict.setValue(Title, forKey: "Title")
                                                                             LocalDict.setValue(Description, forKey: "Description")
                                                                             LocalDict.setValue(Userid, forKey: "Userid")
                                                                             LocalDict.setValue(Status, forKey: "Status")
                                                                             LocalDict.setValue(Reportcount, forKey: "Reportcount")
                                                                             LocalDict.setValue(Createddate, forKey: "Createddate")
                                                                             LocalDict.setValue(ReadBool, forKey: "ReadBool")
                                                                             LocalDict.setValue(totalReadCount, forKey: "totalReadCount")

                                  
                                 var dublicateArray = NSMutableArray()
                                  dublicateArray = supportListArray.mutableCopy() as! NSMutableArray
                                  dublicateArray.replaceObject(at: indexPath.row, with: LocalDict)
                                  print(dublicateArray)
                                  supportListArray = dublicateArray.mutableCopy() as! NSMutableArray
                                  userDefaults1.setValue(supportListArray, forKey: "supportListArray")
                                  userDefaults1.synchronize()
                                  tableView.reloadData()
                            
                            
                        }
                                      
                                                   
                                                 
          let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                       let supportTripVC =  mainStoryboard.instantiateViewController(withIdentifier: "inboxSupportViewController") as! inboxSupportViewController
        var report = Int()
        report = Int(ReportidString)!
        supportTripVC.supportid = report
        supportTripVC.titleString = Description
        supportTripVC.chatBool = Status
                       self.navigationController!.pushViewController(supportTripVC, animated: true)
    }
   @objc func raiseAComplaintAction()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                 let supportTripVC =  mainStoryboard.instantiateViewController(withIdentifier: "newSupportViewController") as! newSupportViewController
                 self.navigationController!.pushViewController(supportTripVC, animated: true)
    }
    /// Back Button Action
    @objc func OnBackClicked() {
                  self.navigationController?.popViewController(animated: true)
      }
    
    /// Status Manager
       @objc func statusManager(_ notification: NSNotification) {
            offline.updateUserInterface(withoutLogin: false)
        }
//    func getSupportList()
//    {
//        self.showLoader()
//                     let userDefaults1 = UserDefaults.standard
//
//        let accessToken = userDefaults1.value(forKey: "accessToken")
//       //                 let getUserId = "\(APIUrl.ZIGTARCAPI)User/api/GetUserId?Token=\(accessToken!)"
//        let getUserId = "\(APIUrl.ZIGTARCAPI)Feedback/Support/Getallsupport?token=\(accessToken!)"
//
//                        Alamofire.request(getUserId, method: .get, parameters: nil, encoding: JSONEncoding.default)
//                            .responseJSON { response in
//                                switch response.result {
//                                case .success:
//
//
//                                    if (response.result.value != nil) {
//
//                                        let responseVar = response.result.value
//                                      //  let json = JSON(responseVar!)
//                                        let dict  = responseVar as! NSDictionary
//
//                                        print("\(dict)")
//                                        var getArray = NSArray()
//                                        getArray = dict.object(forKey: "Getsupportlist") as! NSArray
//                                        print("\(getArray)")
//                                        self.supportListArray = getArray.mutableCopy() as! NSMutableArray
//                                        self.raiseComplaintTable.reloadData()
//                                     //   let dict = json as! NSDictionary
//                                        //self.supportListArray = dict["Getsupportlist"] as! NSMutableArray
//                                      //  print(self.supportListArray)
//
//                                        self.hideLoader()
//
//
//
//                                    }
//                                case .failure(let error):
//                                    print(error)
//
//
//
//                                    self.hideLoader()
//                                }
//                        }
//
//    }
    func getSupportList()
        {
            self.showLoader()
                         let userDefaults1 = UserDefaults.standard

            let accessToken = userDefaults1.value(forKey: "accessToken")
           //                 let getUserId = "\(APIUrl.ZIGTARCAPI)User/api/GetUserId?Token=\(accessToken!)"
            let getUserId = "\(APIUrl.ZIGTARCAPI)Feedback/Support/Getallsupport?token=\(accessToken!)"

                            Alamofire.request(getUserId, method: .get, parameters: nil, encoding: JSONEncoding.default)
                                .responseJSON { response in
                                    switch response.result {
                                    case .success:


                                        if (response.result.value != nil) {

                                            let responseVar = response.result.value
                                          //  let json = JSON(responseVar!)
                                            let dict  = responseVar as! NSDictionary
                                            
                                            print("\(dict)")
                                            var getArray = NSArray()
                                            getArray = dict.object(forKey: "Getsupportlist") as! NSArray
                                            print("\(getArray)")
                                             if userDefaults1.value(forKey: "supportListArray") == nil
                                             {
                                            let supportArray = NSMutableArray()
                                                var notificationCount = 0
                                            for dict in getArray
                                            {
                                                let localDictionary = dict as! NSDictionary
                                                print(localDictionary)
                                                let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                                               var Title = String()
                                                                                               if localDictionary.value(forKey: "Title") is NSNull
                                                                                                                                             {
                                                                                                   Title = ""
                                                                                                                                               
                                                                                               }
                                                                                               else
                                                                                               {
                                                                                                   Title = localDictionary.value(forKey: "Title") as! String
                                                                                               }
                                                                                               
                                                                                               
                                                                                               var Description = String()
                                                                                           if localDictionary.value(forKey: "Description") is NSNull
                                                                                               {
                                                                                                   Description = ""
                                                                                               }
                                                                                               else
                                                                                                {
                                                                                                      Description = localDictionary.value(forKey: "Description") as! String
                                                                                               }
                                                                                             
                                                let Userid = localDictionary.value(forKey: "Userid") as! Int
                                                let Status = localDictionary.value(forKey: "Status") as! Bool
                                                let Reportcount = localDictionary.value(forKey: "Reportcount") as! Int
                                                let Createddate = localDictionary.value(forKey: "Createddate") as! String
                                                    var ReadBool = Bool()
                                                var totalReadCount = Int()
                                                totalReadCount = 0
                                                if Reportcount > 0
                                                {
                                                    if Status == true
                                                    {
                                                        ReadBool = true
                                                        notificationCount = notificationCount + Reportcount
                                                    }
                                                    else
                                                    {
                                                         ReadBool = false
                                                    }
                                                }
                                                else
                                                {
                                                     ReadBool = false
                                                }
                                                
                                                let LocalDict = NSMutableDictionary()
                                                let reportString = "\(Reportid)" as String
                                                LocalDict.setValue(reportString, forKey: "Reportid")
                                               LocalDict.setValue(Title, forKey: "Title")
                                                LocalDict.setValue(Description, forKey: "Description")
                                                LocalDict.setValue(Userid, forKey: "Userid")
                                                LocalDict.setValue(Status, forKey: "Status")
                                                LocalDict.setValue(Reportcount, forKey: "Reportcount")
                                                LocalDict.setValue(Createddate, forKey: "Createddate")
                                                LocalDict.setValue(ReadBool, forKey: "ReadBool")
                                                LocalDict.setValue(totalReadCount, forKey: "totalReadCount")
                                                supportArray.add(LocalDict)
                                                
                                            }
                                   print(supportArray)
                                            let userDefaults1 = UserDefaults.standard
                                            userDefaults1.setValue(supportArray, forKey: "supportListArray")
                                            userDefaults1.setValue(notificationCount, forKey: "notificationCount")
                                            userDefaults1.synchronize()
                                                self.supportListArray = supportArray.mutableCopy() as! NSMutableArray

self.raiseComplaintTable.reloadData()
                                                self.hideLoader()

                                                
                                             }
                                            else
                                             {
                                               
                                                
                                                var  supportListArray = userDefaults1.value(forKey: "supportListArray") as! NSArray
                                                let supportArray = NSMutableArray()
                                                var notificationCount = 0

                                                for dict in getArray
                                                                                       {
                                    let localDictionary = dict as! NSDictionary

                        let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                            let uniqueString = "\(Reportid)" as NSString
                    let resultPredicate : NSPredicate = NSPredicate(format: "Reportid contains[c] %@",uniqueString)
                    let searchResults = supportListArray.filtered(using: resultPredicate)
                                        print(searchResults)
                                                                     
                                    if searchResults.count > 0
                                                                                        {
                                                                                            
                                                                                            
                                                           //previosDictioanry
                                                         let previosArr = searchResults as! NSArray
                                                      let  previosDictioanry = previosArr.object(at: 0) as! NSDictionary
                                                                                            let localDictionary = dict as! NSDictionary
                                                                                                                                       print(localDictionary)
                                                                                                                                       let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                                                                                                                                        var Title = String()
                                                                                                                                                                                      if localDictionary.value(forKey: "Title") is NSNull
                                                                                                                                                                                                                                    {
                                                                                                                                                                                          Title = ""
                                                                                                                                                                                                                                      
                                                                                                                                                                                      }
                                                                                                                                                                                      else
                                                                                                                                                                                      {
                                                                                                                                                                                          Title = localDictionary.value(forKey: "Title") as! String
                                                                                                                                                                                      }
                                                                                                                                                                                      
                                                                                                                                                                                      
                                                                                                                                                                                      var Description = String()
                                                                                                                                                                                  if localDictionary.value(forKey: "Description") is NSNull
                                                                                                                                                                                      {
                                                                                                                                                                                          Description = ""
                                                                                                                                                                                      }
                                                                                                                                                                                      else
                                                                                                                                                                                       {
                                                                                                                                                                                             Description = localDictionary.value(forKey: "Description") as! String
                                                                                                                                                                                      }
                                                                                                                                                                                    
                                                                                                                                       let Userid = localDictionary.value(forKey: "Userid") as! Int
                                                                                                                                       let Status = localDictionary.value(forKey: "Status") as! Bool
                                                                                            var Reportcount = localDictionary.value(forKey: "Reportcount") as! Int
                                                                                                                                       let Createddate = localDictionary.value(forKey: "Createddate") as! String
                                                                                                                                           var ReadBool = Bool()
                                                                                                                                       var totalReadCount = Int()
                         totalReadCount = previosDictioanry.value(forKey: "totalReadCount") as! Int
    //totalReadCount = 0
                                                                                                                                       if Reportcount > 0
                                                                                                                                       {
                                                                                                                                           if Status == true
                                                                                                                                           {
                                                                                                                                          //NEw COndtion
                                                                      if Reportcount >= totalReadCount
                                                                      {
                                                                        ReadBool = true
                                                                        
                                                                        let balance = Reportcount - totalReadCount
                                                                    
                                                                 //    Reportcount = balance
                                                                        
                                                                        if Reportcount <= 0
                                                                        {
                                                                            ReadBool = false
                                                                        }
                                                                        
                                                                        notificationCount = notificationCount + balance
                                                                        
                                                                        if balance <= 0
                                                                        {
                                                                             ReadBool = false
                                                                        }
                                                                                                                                            }
                                                                        else
                                                                      {
                                                                        
                                                                        ReadBool = false
                                                                                                                                            }
                                                                        }
                                                                                                                                           else
                                                                                                                                           {
                                                                                                                                                ReadBool = false
                                                                                                                                           }
                                                                                                                                       }
                                                                                                                                       else
                                                                                                                                       {
                                                                                                                                            ReadBool = false
                                                                                                                                       }
                                                                                                                                       
                                                                                                                                       let LocalDict = NSMutableDictionary()
                                                                                                                                       let reportString = "\(Reportid)" as String
                                                                                                                                       LocalDict.setValue(reportString, forKey: "Reportid")
                                                                                                                                      LocalDict.setValue(Title, forKey: "Title")
                                                                                                                                       LocalDict.setValue(Description, forKey: "Description")
                                                                                                                                       LocalDict.setValue(Userid, forKey: "Userid")
                                                                                                                                       LocalDict.setValue(Status, forKey: "Status")
                                                                                                                                       LocalDict.setValue(Reportcount, forKey: "Reportcount")
                                                                                                                                       LocalDict.setValue(Createddate, forKey: "Createddate")
                                                                                                                                       LocalDict.setValue(ReadBool, forKey: "ReadBool")
                                                                                                                                       LocalDict.setValue(totalReadCount, forKey: "totalReadCount")
                                                                                                                                       supportArray.add(LocalDict)
                                                                                            
                                                                                            
                                                                                        }
                                                                                        else
                                    {
                                        let localDictionary = dict as! NSDictionary
                                                                                   print(localDictionary)
                                                                                   let Reportid = localDictionary.value(forKey: "Reportid") as! Int
                                                                         var Title = String()
                                                                                                                                  if localDictionary.value(forKey: "Title") is NSNull
                                                                                                                                                                                {
                                                                                                                                      Title = ""
                                                                                                                                                                                  
                                                                                                                                  }
                                                                                                                                  else
                                                                                                                                  {
                                                                                                                                      Title = localDictionary.value(forKey: "Title") as! String
                                                                                                                                  }
                                                                                                                                  
                                                                                                                                  
                                                                                                                                  var Description = String()
                                                                                                                              if localDictionary.value(forKey: "Description") is NSNull
                                                                                                                                  {
                                                                                                                                      Description = ""
                                                                                                                                  }
                                                                                                                                  else
                                                                                                                                   {
                                                                                                                                         Description = localDictionary.value(forKey: "Description") as! String
                                                                                                                                  }
                                                                                                                                
                                                                                   let Userid = localDictionary.value(forKey: "Userid") as! Int
                                                                                   let Status = localDictionary.value(forKey: "Status") as! Bool
                                                                                   let Reportcount = localDictionary.value(forKey: "Reportcount") as! Int
                                                                                   let Createddate = localDictionary.value(forKey: "Createddate") as! String
                                                                                       var ReadBool = Bool()
                                                                                   var totalReadCount = Int()
                                                                                   totalReadCount = 0
                                                                                   if Reportcount > 0
                                                                                   {
                                                                                       if Status == true
                                                                                       {
                                                                                           ReadBool = true
                                                                                           notificationCount = notificationCount + Reportcount
                                                                                       }
                                                                                       else
                                                                                       {
                                                                                            ReadBool = false
                                                                                       }
                                                                                   }
                                                                                   else
                                                                                   {
                                                                                        ReadBool = false
                                                                                   }
                                                                                   
                                                                                   let LocalDict = NSMutableDictionary()
                                                                                   let reportString = "\(Reportid)" as String
                                                                                   LocalDict.setValue(reportString, forKey: "Reportid")
                                                                                  LocalDict.setValue(Title, forKey: "Title")
                                                                                   LocalDict.setValue(Description, forKey: "Description")
                                                                                   LocalDict.setValue(Userid, forKey: "Userid")
                                                                                   LocalDict.setValue(Status, forKey: "Status")
                                                                                   LocalDict.setValue(Reportcount, forKey: "Reportcount")
                                                                                   LocalDict.setValue(Createddate, forKey: "Createddate")
                                                                                   LocalDict.setValue(ReadBool, forKey: "ReadBool")
                                                                                   LocalDict.setValue(totalReadCount, forKey: "totalReadCount")
                                                                                   supportArray.add(LocalDict)
                                                                                        }
                                                                                        
                                                                                        
                                                                                        
                                                }

                                                let userDefaults1 = UserDefaults.standard
                                                self.supportListArray = supportArray.mutableCopy() as! NSMutableArray
                                                userDefaults1.setValue(supportArray, forKey: "supportListArray")
                                                userDefaults1.setValue(notificationCount, forKey: "notificationCount")
                                                                                       userDefaults1.synchronize()
self.raiseComplaintTable.reloadData()
                                                self.hideLoader()

                                                
                                            }



                                        }
                                    case .failure(let error):
                                        print(error)



                                        self.hideLoader()
                                    }
                            }
            
        }
    func showLoader()
    {
        DispatchQueue.main.async {
            self.loaderView = UIView()
            self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
            self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            let window = UIApplication.shared.keyWindow!
            
            let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
            activityLoader.startAnimating()
            window.addSubview(self.loaderView)
            self.loaderView.addSubview(activityLoader)
        }
    }
    func hideLoader()
    {
        DispatchQueue.main.async {
            self.loaderView.removeFromSuperview()
        }
    }
    @objc func methodOfReceivedNotification(notification: Notification) {


                    getSupportList()


    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // NAvigation and Google Analytics
        analytics.GetPageHitCount(PageName: "Support-Index")
                 NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .flagsChanged, object: Network.reachability)
                 offline.updateUserInterface(withoutLogin: false)
                 self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
                 self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
                 self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
                 self.navigationController?.navigationItem.titleView?.tintColor = UIColor.white
                 self.title = "Support"
                 
                 
                 let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                                  style: UIBarButtonItemStyle.done ,
                                                  target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

                  self.navigationItem.leftBarButtonItem = backButton
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("NotificationForUpdateTicketList"), object: nil)
              NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationForUpdateTicketList"), object: nil)
        
        raiseAComplaintDesign()
        let userDefaults1 = UserDefaults.standard

        if userDefaults1.value(forKey: "supportListArray") == nil
        {
            getSupportList()

        }
else
        {
         let supportListArrayz = userDefaults1.value(forKey: "supportListArray") as! NSArray
            supportListArray = supportListArrayz.mutableCopy() as! NSMutableArray
            self.raiseComplaintTable.reloadData()

        }
        // Do any additional setup after loading the view.
    }

}
