//
//  ResponseTableViewCell.swift
//  chatbox
//
//  Created by Arun pandiyan on 07/04/20.
//  Copyright © 2020 Arun. All rights reserved.
//

import UIKit

class ResponseTableViewCell: UITableViewCell {


       var backView: UIView!

        var descriptionLabel: UILabel!
        var timeLabel: UILabel!

        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        override func layoutSubviews() {
            super.layoutSubviews()

            backView.roundCornerdd([.topLeft, .topRight, .bottomLeft], radius: 10)
        }
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.createSubViews()
            self.setupConstraints()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        func createSubViews() -> Void {
            
            backView = UIView()
            backView.translatesAutoresizingMaskIntoConstraints = false
            backView.backgroundColor = UIColor.init(red: 50/255, green: 108/255, blue: 175/255, alpha: 1.0)
            self .addSubview(backView)
            
            descriptionLabel = UILabel()
            descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
            descriptionLabel.backgroundColor = UIColor.clear
            descriptionLabel.textColor = UIColor.white
            descriptionLabel.font = UIFont.setTarcRegular(size: 16)
            descriptionLabel.numberOfLines = 0
            descriptionLabel.sizeToFit()
            backView .addSubview(descriptionLabel)
            
            timeLabel = UILabel()
            timeLabel.translatesAutoresizingMaskIntoConstraints = false
            timeLabel.backgroundColor = UIColor.clear
            timeLabel.textColor = UIColor.white
            timeLabel.font = UIFont.systemFont(ofSize: 11)
            timeLabel.textAlignment = .right
            timeLabel.isHidden = true
            backView .addSubview(timeLabel)
            
            
            
        }
        func setupConstraints() -> Void {
            
            
                            var backViewWidth = Int()
                            
                            switch UIDevice.current.userInterfaceIdiom {
                            case .phone:
                                   // It's an iPhone
                                backViewWidth = 300
                                break
                            case .pad:
                                   // It's an iPad (or macOS Catalyst)
                                backViewWidth = 400
                                descriptionLabel.font = UIFont.setTarcRegular(size: 19)

            break
                            case .unspecified: break
                                   // Uh, oh! What could it be?
                            case .tv: break
                                //
                            case .carPlay: break
                                //
                            }
                            
                            let metricsCele:[String:Any] = ["backViewWidth":backViewWidth  ]
            
            
            let viewsDict:[String:Any] = ["backView":backView as Any,
                                        "descriptionLabel":descriptionLabel as Any,
                                         "timeLabel":timeLabel as Any
            ]
            
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:[backView(<=backViewWidth)]-10-|",
                                                                                  options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                  metrics: metricsCele,
                                                                                  views: viewsDict))
                      
                      NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[backView]-5-|",
                                                                                  options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                  metrics: nil,
                                                                                  views: viewsDict))
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[descriptionLabel]-10-|",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: nil,
                                                                        views: viewsDict))
            
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[timeLabel]-5-|",
                                                                                 options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                 metrics: nil,
                                                                                 views: viewsDict))
            
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[descriptionLabel]-0-[timeLabel(0)]-10-|",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: nil,
                                                                        views: viewsDict))
            
            
        

            
        }

    

}
extension UIView {

    func roundCornerdd(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }

}
