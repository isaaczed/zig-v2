//
//  TransactionTableViewCell.swift
//  ZIG
//
//  Created by Arun pandiyan on 20/04/20.
//  Copyright © 2020 Isaac. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

   
        var backView: UIView!
        var firstButton: UIButton!
        var secondButton: UIButton!
        var thirdButton: UIButton!
        var fourthButton: UIButton!
        var fifthButton: UIButton!
    var sixthButton: UIButton!

         var width100 = Int()
         var width120 = Int()
         var width140 = Int()
     var heightButton = Int()
     var spaceHeight = Int()
     var spacingWidth = Int()
     var overallheight = Int()
    var TransactionBool = Bool()

        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            self.createSubViews()
          // self.setupConstraints()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        func createSubViews() -> Void {
            
           // let countArray = titleArray.count
            
            
            
            backView = UIView()
            backView.translatesAutoresizingMaskIntoConstraints = false
           // backView.frame = CGRect.init(x: 10, y: 0, width: 200, height: 50)
            backView.backgroundColor = UIColor.white
            self .addSubview(backView)


            
            firstButton = UIButton()
            firstButton.translatesAutoresizingMaskIntoConstraints = false
            firstButton.setTitle("Ticket Not Generated ", for: .normal)
            firstButton.backgroundColor = UIColor.white
            backView .addSubview(firstButton)
            firstButton .setTitleColor(UIColor.black, for: .normal)
            firstButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

            firstButton.layer.borderWidth = 1
            firstButton.layer.borderColor = UIColor.lightGray.cgColor
            firstButton.layer.cornerRadius = 15
            firstButton.layer.masksToBounds = false
            
            secondButton = UIButton()
            secondButton.translatesAutoresizingMaskIntoConstraints = false
            secondButton.setTitle("Transaction Failed", for: .normal)
            secondButton.backgroundColor = UIColor.white
            backView .addSubview(secondButton)
            secondButton .setTitleColor(UIColor.black, for: .normal)
            secondButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

            secondButton.layer.borderWidth = 1
            secondButton.layer.borderColor = UIColor.lightGray.cgColor
            secondButton.layer.cornerRadius = 15
            secondButton.layer.masksToBounds = false

            thirdButton = UIButton()
            thirdButton.translatesAutoresizingMaskIntoConstraints = false
            thirdButton.setTitle("Onboard Validator Issues", for: .normal)
            thirdButton.backgroundColor = UIColor.white
            backView .addSubview(thirdButton)
            thirdButton .setTitleColor(UIColor.black, for: .normal)
            thirdButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

            thirdButton.layer.borderWidth = 1
            thirdButton.layer.borderColor = UIColor.lightGray.cgColor
            thirdButton.layer.cornerRadius = 15
            thirdButton.layer.masksToBounds = false
            
            fourthButton = UIButton()
            fourthButton.translatesAutoresizingMaskIntoConstraints = false
            fourthButton.setTitle("Ticket Refunds", for: .normal)
            fourthButton.titleLabel?.textAlignment = .right
            fourthButton.backgroundColor = UIColor.white
            backView .addSubview(fourthButton)
            fourthButton .setTitleColor(UIColor.black, for: .normal)
            fourthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

            fourthButton.layer.borderWidth = 1
            fourthButton.layer.borderColor = UIColor.lightGray.cgColor
            fourthButton.layer.cornerRadius = 15
            fourthButton.layer.masksToBounds = false
            
            
            fifthButton = UIButton()
            fifthButton.translatesAutoresizingMaskIntoConstraints = false
            fifthButton.setTitle("Other Issue", for: .normal)
            fifthButton.backgroundColor = UIColor.white
            backView .addSubview(fifthButton)
            fifthButton .setTitleColor(UIColor.black, for: .normal)
            fifthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)
            
            fifthButton.layer.borderWidth = 1
            fifthButton.layer.borderColor = UIColor.lightGray.cgColor
            fifthButton.layer.cornerRadius = 15
            fifthButton.layer.masksToBounds = false
            
            
            sixthButton = UIButton()
                     sixthButton.translatesAutoresizingMaskIntoConstraints = false
                     sixthButton.setTitle("Main Menu", for: .normal)
                     sixthButton.backgroundColor = UIColor.white
                     backView .addSubview(sixthButton)
                     sixthButton .setTitleColor(UIColor.black, for: .normal)
                     sixthButton.titleLabel?.font = UIFont.setTarcRegular(size: 12)

                     sixthButton.layer.borderWidth = 1
                     sixthButton.layer.borderColor = UIColor.lightGray.cgColor
                     sixthButton.layer.cornerRadius = 15
                     sixthButton.layer.masksToBounds = false
            
  if UIScreen.main.sizeType == .iPadPro129{
            firstButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
    secondButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
    thirdButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
    fourthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
    fifthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)
  //  sixthButton.titleLabel?.font = UIFont.setTarcRegular(size: 15)

            }

        }
        @objc func actionQue(sender:UIButton)
        {
            print("\(sender.titleLabel!.text!)")
        }
    func setupConstraints(transBool:Bool) -> Void {
            
             width100 = 100
             width120 = 120
             width140 = 140
             heightButton = 30
             spaceHeight = 10
             spacingWidth = 10
            if transBool == true
            {
                overallheight = 60

            }
            else
            {
                overallheight = 130

            }

        if UIScreen.main.sizeType == .iPadAir2andiPadMini4{
     width100 = 130
                                width120 = 150
                                width140 = 170
                                heightButton = 40
                                spaceHeight = 15
                                spacingWidth = 15
                          
                          if transBool == true
                          {
                              overallheight = 60

                          }
                          else
                          {
                              overallheight = 120

                          }
            
            
            
            }else if UIScreen.main.sizeType == .iPadPro105{
                                width100 = 130
                                width120 = 150
                                width140 = 170
                                heightButton = 50
                                spaceHeight = 15
                                spacingWidth = 10
                          
                          if transBool == true
                          {
                              overallheight = 60

                          }
                          else
                          {
                              overallheight = 140

                          }

            }
             else if UIScreen.main.sizeType == .iPadPro129{
                          width100 = 130
                          width120 = 150
                          width140 = 170
                          heightButton = 50
                          spaceHeight = 15
                          spacingWidth = 15
                    
                    if transBool == true
                    {
                        overallheight = 60

                    }
                    else
                    {
                        overallheight = 140

                    }

                          }
            
             let matrcis:[String:Any] = [ "width100":width100,
                                           "width120":width120,
                                           "width140":width140,
                                           "heightButton":heightButton,
                                           "spaceHeight":spaceHeight,
                                           "spacingWidth":spacingWidth,
                                           "overallheight":overallheight
                                            ]
            
            let viewsDict:[String:Any] = [ "backView":backView as Any,
                                          "firstButton":firstButton as Any,
                                          "secondButton":secondButton as Any,
                                          "thirdButton":thirdButton as Any,
                                          "fourthButton":fourthButton as Any,
                                          "fifthButton":fifthButton as Any,
                                          "sixthButton":sixthButton as Any


            ]
            
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-10-[backView]-5-|",
                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                             metrics: nil,
                                                                             views: viewsDict))
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-10-[backView(overallheight)]",
                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                             metrics: matrcis,
                                                                             views: viewsDict))
            
             if UIScreen.main.sizeType == .iPadAir2andiPadMini4 || UIScreen.main.sizeType == .iPadPro105 || UIScreen.main.sizeType == .iPadPro129 {
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]-spaceHeight-[fifthButton(heightButton)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: matrcis,
                                                                        views: viewsDict))
            
             NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[secondButton(heightButton)]-spaceHeight-[sixthButton(heightButton)]",
                                                                             options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                             metrics: matrcis,
                                                                             views: viewsDict))
            
         
                NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[thirdButton(heightButton)]",
                                                                                            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                            metrics: matrcis,
                                                                                            views: viewsDict))
                
                NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[fourthButton(heightButton)]",
                                                                                            options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                            metrics: matrcis,
                                                                                            views: viewsDict))
                
            
         
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[firstButton(width140)]-spacingWidth-[secondButton(width140)]-spacingWidth-[thirdButton(190)]-spacingWidth-[fourthButton(width120)]",
                                                                        options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                        metrics: matrcis,
                                                                        views: viewsDict))
            
          
            
            
            NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[fifthButton(width100)]-spacingWidth-[sixthButton(width120)]",
                                                                              options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                              metrics: matrcis,
                                                                              views: viewsDict))
            
           
        }
        else
             {
                
                   NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[firstButton(heightButton)]-spaceHeight-[thirdButton(heightButton)]-spaceHeight-[fifthButton(heightButton)]",
                                                                               options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                               metrics: matrcis,
                                                                               views: viewsDict))
                   
                    NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|-5-[secondButton(heightButton)]-spaceHeight-[fourthButton(heightButton)]-spaceHeight-[sixthButton(heightButton)]",
                                                                                    options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                    metrics: matrcis,
                                                                                    views: viewsDict))
                   
                
                   
                
                   
                   NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[firstButton(width140)]-spacingWidth-[secondButton(width140)]",
                                                                               options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                               metrics: matrcis,
                                                                               views: viewsDict))
                   
                   NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[thirdButton(150)]-spacingWidth-[fourthButton(width120)]",
                                                                                     options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                     metrics: matrcis,
                                                                                     views: viewsDict))
                   
                   
                   NSLayoutConstraint .activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|-5-[fifthButton(width100)]-spacingWidth-[sixthButton(width100)]",
                                                                                     options: NSLayoutConstraint.FormatOptions(rawValue: 0),
                                                                                     metrics: matrcis,
                                                                                     views: viewsDict))
                   
                
        }
            
            backView.layer.cornerRadius = 5
            backView.layer.masksToBounds = false
            
        }

    }
