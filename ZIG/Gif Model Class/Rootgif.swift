//
//	Rootgif.swift


import Foundation 
import ObjectMapper


class Rootgif : NSObject, NSCoding, Mappable{

	var imageUrl : String?


	class func newInstance(map: Map) -> Mappable?{
		return Rootgif()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		imageUrl <- map["ImageUrl"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         imageUrl = aDecoder.decodeObject(forKey: "ImageUrl") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if imageUrl != nil{
			aCoder.encode(imageUrl, forKey: "ImageUrl")
		}

	}

}