//
//  BuyTicketNewViewController.swift
//  ZIG
//
//  Created by Arun pandiyan on 15/10/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import RealmSwift
import XLActionController
import SCLAlertView
class BuyTicketNewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    let assistiveTouch = AssistiveTouch()
    let assistiveTouchSOS = AssistiveTouch()
    var menuButton = UIBarButtonItem()
    @IBOutlet weak var buyTicketTable: UITableView!
    var ticketArray = NSArray()
    var titleString = NSString()
    var noofTickets: String?
    var fareID: Int?
    var loaderView = UIView()

       var ticketPrice: String?
    var listArray = NSArray()
    private var originalPullUpControllerViewSize: CGSize = .zero

    override func viewDidLoad() {
        super.viewDidLoad()
        analytics.GetPageHitCount(PageName: "Fare List")

        //ticketArray = [[String: AnyObject]]()
     assistiveTouch.frame = CGRect(x: self.view.frame.width - 66, y: self.view.frame.height / 2, width: 56, height: 56)
        assistiveTouch.accessibilityLabel = "Quick Menu"

     assistiveTouch.addTarget(self, action: #selector(tapped(sender:)), for: .touchUpInside)
     assistiveTouch.setImage(UIImage(named: "AsstisTouch"), for: .normal)
     view.addSubview(assistiveTouch)
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tappedSOS))  //Tap
//        tapGesture.numberOfTapsRequired = 1
//        assistiveTouchSOS.addGestureRecognizer(tapGesture)
//        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressSOS(gesture:)))
//        longPress.minimumPressDuration = 1.0
//        self.assistiveTouchSOS.addGestureRecognizer(longPress)
//        assistiveTouchSOS.frame = CGRect(x: 0, y: self.view.frame.height / 2, width: 56, height: 56)
//        assistiveTouchSOS.setImage(UIImage(named: "sos"), for: .normal)
//        view.addSubview(assistiveTouchSOS)
        
        
        self.title = "Buy Tickets"
             self.navigationController?.navigationBar.isHidden = false
             self.navigationController?.navigationItem.title = " "
             self.navigationController?.navigationBar.barTintColor = UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0)
             let backButton = UIBarButtonItem(image: UIImage(named: "Back-Btn"),
                                              style: UIBarButtonItemStyle.done ,
                                              target: self, action: #selector(OnBackClicked))
        backButton.accessibilityLabel = "Back"

            self.view.backgroundColor = UIColor.init(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
            buyTicketTable.backgroundColor = UIColor.init(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)

             self.navigationItem.leftBarButtonItem = backButton
             self.navigationItem.setHidesBackButton(true, animated:true);
             //self.view.layoutIfNeeded()
            buyTicketTable.isScrollEnabled = false
            buyTicketTable.separatorStyle = .none
            menuButton = UIBarButtonItem(title: "View Tickets",
                                              style: UIBarButtonItemStyle.plain,
                                              target: self,
                                              action: #selector(ticketViewAction))
         
             
             self.navigationItem.rightBarButtonItem = menuButton
              self.navigationItem.rightBarButtonItem?.tintColor = color.hexStringToUIColor(hex: "#EFAC40")
             
             self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        // Do any additional setup after loading the view.
        makeArray()

        
    }
    @objc func longPressSOS(gesture: UILongPressGestureRecognizer) {
          if gesture.state == UIGestureRecognizerState.began {
              UIDevice.vibrate()
              print("Long Press")
              currentlocation.getAddressFromLatLon() { (Success, Address,LatsosLat,Longsoslong) in
                         if Success{
                        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let MapsScheduleVC =  mainStoryboard.instantiateViewController(withIdentifier: "SOSViewController") as! SOSViewController
                          print("\(Address),\(LatsosLat),\(Longsoslong)")
                          MapsScheduleVC.CurrentlocationAddressString = Address
                          MapsScheduleVC.latSOS = LatsosLat
                          MapsScheduleVC.longSOS = Longsoslong
                        self.navigationController!.pushViewController(MapsScheduleVC, animated: true)
                                 //self.CurrentlocationAddress.text = Address
                             }
                         }
              
          }
      }
    
      @objc func tappedSOS(sender: UIButton) {
       
          let timeoutAction: SCLAlertView.SCLTimeoutConfiguration.ActionType = {
                  // action here
          }
                  
          SCLAlertView().showInfo("SOS Alert", subTitle: "SOS is an emergency feature to alert the driver. Hold to initiate SOS-info sentence",timeout:SCLAlertView.SCLTimeoutConfiguration(timeoutValue: 3.0, timeoutAction:timeoutAction))
         
      }
        @objc func tapped(sender: UIButton) {
            print("\(sender) has been touched")
            
            
            let actionController = TwitterActionController()
            
            actionController.addAction(Action(ActionData(title: "Trip Planner", subtitle: "", image: UIImage(named: "Dash-Tripplaner")!), style: .default, handler: { action in
                
              let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                          let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TripPlannerViewController") as! TripPlannerViewController
                                          self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            if comVar.DateImplementation {
                
                actionController.addAction(Action(ActionData(title: "View tickets", subtitle: "", image: UIImage(named: "freePass")!), style: .default, handler: { action in
                    
    //                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    //                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "BuyTicketTempViewController") as! BuyTicketTempViewController
    //                self.navigationController?.pushViewController(nextViewController, animated: true)
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                               let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
                               self.navigationController?.pushViewController(nextViewController, animated: true)
                               
                               
                }))
                
            }
            actionController.addAction(Action(ActionData(title: "Real - Time Schedules", subtitle: "", image: UIImage(named: "Dash-Map & sch")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "realtimeSchduleViewController") as! realtimeSchduleViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            actionController.addAction(Action(ActionData(title: "Saved Trips", subtitle: "", image: UIImage(named: "Dash-fav")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MyTripTableViewController") as! MyTripTableViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            actionController.addAction(Action(ActionData(title: "Alerts / News", subtitle: "", image: UIImage(named: "AlertDash")!), style: .default, handler: { action in
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }))
            
            
            
            actionController.headerData = "Quick View"
            present(actionController, animated: true, completion: nil)
            
            
            
            
        }
    override func viewWillAppear(_ animated: Bool) {
                     self.navigationController?.navigationBar.isHidden = false

    }
    private func makeSearchViewControllerIfNeeded() -> NewTicketAddViewController {
        
        let currentPullUpController = childViewControllers
            .filter({ $0 is NewTicketAddViewController })
            .first as? NewTicketAddViewController
        let pullUpController: NewTicketAddViewController = currentPullUpController ?? UIStoryboard(name: "Main",bundle: nil).instantiateViewController(withIdentifier: "NewTicketAddViewController") as! NewTicketAddViewController
        print(titleString)
        print(fareID)

        pullUpController.TitleString = titleString
        pullUpController.ticketPrice = ticketPrice
        pullUpController.fareID = fareID
        if originalPullUpControllerViewSize == .zero {
            originalPullUpControllerViewSize = pullUpController.view.bounds.size
        }
        
        return pullUpController
    }
    override func viewWillDisappear(_ animated: Bool) {
        let pullUpController = makeSearchViewControllerIfNeeded()

        removePullUpController(pullUpController, animated: false)

    }
    private func addPullUpController() {
           
        let pullUpControllers = makeSearchViewControllerIfNeeded()
        
               _ = pullUpControllers.view // call pullUpController.viewDidLoad()
               addPullUpController(pullUpControllers,
                                   initialStickyPointOffset: pullUpControllers.initialPointOffset,
                                   animated: true)
        
        
        
    }
        func makeArray() -> Void {
            
            ticketArray = [
                [
                    "title" : "Single Ride",
                    "price" : "1.50",
                    "FareId": 9
                ],
                [
                    "title" : "24 Hour Pass",
                    "price" : "3.50",
                     "FareId": 10
                ],
                [
                    "title" : "7 Day Pass",
                    "price" : "15.00",
                     "FareId": 11
                ],
                [
                    "title" : "30 Day Pass",
                    "price" : "50.00",
                     "FareId": 12
                ]
            ]
    //        self.showLoader()
    //        let userDefaults1 = UserDefaults.standard
    //               let accessToken = userDefaults1.value(forKey: "accessToken") as! NSString
    //        let RouteUrl = "\(APIUrl.ZIGTARCAPI)Fare/GetAllFare?token=\(accessToken)"
    //        print(RouteUrl)
    //        let uiRealm = try! Realm()
    //
    //                Alamofire.request(RouteUrl).responseObject { (response: DataResponse<paymentDynamic>) in
    //                    //self.ticket_list = [response.result.value!]
    //                    switch response.result {
    //                    case .success:
    //
    //                    if response.result.value?.Message == "Ok"
    //                    {
    //
    //                        self.listArray = response.result.value?.list as! NSArray
    //
    //                        if self.listArray.count > 0
    //                        {
    //                            print(self.listArray)
    //                            for index in 0..<self.listArray.count {
    //
    //                                let Isactive = response.result.value?.list![index].Isactive!
    //
    //                                if Isactive == true
    //                                {
    //                                   let listDictionary = NSDictionary()
    //                                    var Fareamount = Float()
    //                                    var CategoryId = Int()
    //                                    var FareId = Int()
    //                                    var Zonename = NSString()
    //                                    var RouteName = NSString()
    //                                    var Validtill = NSString()
    //                                    var Createddate = NSString()
    //                                    var AgencyId = Int()
    //                                    var Serverdate = NSString()
    //                                    var type = NSString()
    //                                    var Agencyname = NSString()
    //                                    var Category = NSString()
    //                                    var ZoneId = Int()
    //                                    var Message = NSString()
    //                                    var Farename = NSString()
    //
    //                                    Fareamount = (response.result.value?.list![index].Fareamount)!
    //                                    CategoryId = (response.result.value?.list![index].CategoryId)!
    //                                    FareId = (response.result.value?.list![index].FareId)!
    //                                    Zonename = (response.result.value?.list![index].Zonename)! as NSString
    //                                    RouteName = (response.result.value?.list![index].RouteName)! as NSString
    //                                    Validtill = (response.result.value?.list![index].Validtill)! as NSString
    //                                    Createddate = (response.result.value?.list![index].Createddate)! as NSString
    //                                    AgencyId = (response.result.value?.list![index].AgencyId)!
    //                                    Serverdate = (response.result.value?.list![index].Serverdate)! as NSString
    //                                    type = (response.result.value?.list![index].type)! as NSString
    //                                    Agencyname = (response.result.value?.list![index].Agencyname)! as NSString
    //                                    Category = (response.result.value?.list![index].Category)! as NSString
    //                                    ZoneId = (response.result.value?.list![index].ZoneId)!
    //                                    Message = (response.result.value?.list![index].Message)! as NSString
    //                                    Farename = (response.result.value?.list![index].Farename)! as NSString
    //
    //
    //
    //
    //                                     let TicketSave = fareRealmModel()
    //                                     TicketSave.Fareamount = Fareamount
    //                                     TicketSave.CategoryId = CategoryId
    //                                     TicketSave.FareId = FareId
    //                                    TicketSave.Zonename = Zonename as String
    //                                    TicketSave.RouteName = RouteName as String
    //                                    TicketSave.Isactive = Isactive!
    //                                    TicketSave.Validtill = Validtill as String
    //                                    TicketSave.Createddate = Createddate as String
    //                                     TicketSave.AgencyId = AgencyId
    //                                    TicketSave.Serverdate = Serverdate as String
    //                                    TicketSave.type = type as String
    //                                    TicketSave.Agencyname = Agencyname as String
    //                                    TicketSave.Category = Category as String
    //                                     TicketSave.ZoneId = ZoneId
    //                                    TicketSave.Message = Message as String
    //                                    TicketSave.Farename = Farename as String
    //
    //
    //                                     try! uiRealm.write {
    //
    //                                        let boolforReal = BuyTicketNewViewController.addCategorys(category: TicketSave)
    //                                             print("Bool For real \(boolforReal)")
    //                                             if boolforReal == true
    //                                             {
    //                                                uiRealm.add(TicketSave, update: .error)
    //                                             }
    //
    //
    //                                         print("Ticket saved completed")
    //
    //
    //
    //                                    // }
    //                                     }
    //
    //
    //
    //                                    let keys = ["Fareamount", "CategoryId", "FareId", "Zonename", "RouteName", "Validtill", "Createddate", "AgencyId", "Type","Agencyname","Category","ZoneId","Message","Farename"]
    //                                    let values = [Fareamount, CategoryId, FareId,Zonename,RouteName,Validtill,Createddate,AgencyId,type,Agencyname,Category,ZoneId,Message,Farename] as [Any]
    //
    //                                    let dict = Dictionary(uniqueKeysWithValues: zip(keys, values))
    //
    //                                    print(dict)
    //                                    self.ticketArray.append(dict as [String : AnyObject])
    //
    //                                    print(self.ticketArray)
    //
    //                                    self.buyTicketTable.reloadData()
    //                                }
    //
    //
    //
    //
    //                            }
    //                              self.hideLoader()
    //                        }
    //                       print("ok")
    //                    }
    //                    else {
    //
    //                        let Ticketlist_db = try! Realm().objects(fareRealmModel.self)
    //                                   print(Ticketlist_db)
    //                             for offlinefarelist in Ticketlist_db
    //                             {
    //                                 let Isactive = offlinefarelist.Isactive
    //
    //                                                          if Isactive == true
    //                                                          {
    //                                                              let listDictionary = NSDictionary()
    //                                                              var Fareamount = Float()
    //                                                              var CategoryId = Int()
    //                                                              var FareId = Int()
    //                                                              var Zonename = NSString()
    //                                                              var RouteName = NSString()
    //                                                              var Validtill = NSString()
    //                                                              var Createddate = NSString()
    //                                                              var AgencyId = Int()
    //                                                              var Serverdate = NSString()
    //                                                              var type = NSString()
    //                                                              var Agencyname = NSString()
    //                                                              var Category = NSString()
    //                                                              var ZoneId = Int()
    //                                                              var Message = NSString()
    //                                                              var Farename = NSString()
    //
    //                                                              Fareamount = offlinefarelist.Fareamount
    //                                                              CategoryId = offlinefarelist.CategoryId
    //                                                              FareId = offlinefarelist.FareId
    //                                                           Zonename = offlinefarelist.Zonename as NSString
    //                                                           RouteName = offlinefarelist.RouteName as NSString
    //                                                           Validtill = offlinefarelist.Validtill as NSString
    //                                                           Createddate = offlinefarelist.Createddate as NSString
    //                                                              AgencyId = offlinefarelist.AgencyId
    //                                                           Serverdate = offlinefarelist.Serverdate as NSString
    //                                                           type = offlinefarelist.type as NSString
    //                                                           Agencyname = offlinefarelist.Agencyname as NSString
    //                                                           Category = offlinefarelist.Category as NSString
    //                                                              ZoneId = offlinefarelist.ZoneId
    //                                                           Message = offlinefarelist.Message as NSString
    //                                                           Farename = offlinefarelist.Farename as NSString
    //
    //
    //
    //
    //                                                              let keys = ["Fareamount", "CategoryId", "FareId", "Zonename", "RouteName", "Validtill", "Createddate", "AgencyId", "Type","Agencyname","Category","ZoneId","Message","Farename"]
    //                                                              let values = [Fareamount, CategoryId, FareId,Zonename,RouteName,Validtill,Createddate,AgencyId,type,Agencyname,Category,ZoneId,Message,Farename] as [Any]
    //
    //                                                              let dict = Dictionary(uniqueKeysWithValues: zip(keys, values))
    //
    //                                                              print(dict)
    //                                                              self.ticketArray.append(dict as [String : AnyObject])
    //
    //                                                              print(self.ticketArray)
    //
    //                                                              self.buyTicketTable.reloadData()
    //                                                          }
    //                             }
    //
    //                         self.hideLoader()
    //                            }
    //
    //
    //
    //                    case .failure(let error):
    //                        print(error)
    //                        let Ticketlist_db = try! Realm().objects(fareRealmModel.self)
    //                              print(Ticketlist_db)
    //                        for offlinefarelist in Ticketlist_db
    //                        {
    //                            let Isactive = offlinefarelist.Isactive
    //
    //                           if Isactive == true
    //                           {
    //                              let listDictionary = NSDictionary()
    //                               var Fareamount = Float()
    //                               var CategoryId = Int()
    //                               var FareId = Int()
    //                               var Zonename = NSString()
    //                               var RouteName = NSString()
    //                               var Validtill = NSString()
    //                               var Createddate = NSString()
    //                               var AgencyId = Int()
    //                               var Serverdate = NSString()
    //                               var type = NSString()
    //                               var Agencyname = NSString()
    //                               var Category = NSString()
    //                               var ZoneId = Int()
    //                               var Message = NSString()
    //                               var Farename = NSString()
    //
    //                               Fareamount = offlinefarelist.Fareamount
    //                               CategoryId = offlinefarelist.CategoryId
    //                               FareId = offlinefarelist.FareId
    //                            Zonename = offlinefarelist.Zonename as NSString
    //                            RouteName = offlinefarelist.RouteName as NSString
    //                            Validtill = offlinefarelist.Validtill as NSString
    //                            Createddate = offlinefarelist.Createddate as NSString
    //                               AgencyId = offlinefarelist.AgencyId
    //                            Serverdate = offlinefarelist.Serverdate as NSString
    //                            type = offlinefarelist.type as NSString
    //                            Agencyname = offlinefarelist.Agencyname as NSString
    //                            Category = offlinefarelist.Category as NSString
    //                               ZoneId = offlinefarelist.ZoneId
    //                            Message = offlinefarelist.Message as NSString
    //                            Farename = offlinefarelist.Farename as NSString
    //
    //
    //
    //
    //                               let keys = ["Fareamount", "CategoryId", "FareId", "Zonename", "RouteName", "Validtill", "Createddate", "AgencyId", "Type","Agencyname","Category","ZoneId","Message","Farename"]
    //                               let values = [Fareamount, CategoryId, FareId,Zonename,RouteName,Validtill,Createddate,AgencyId,type,Agencyname,Category,ZoneId,Message,Farename] as [Any]
    //
    //                               let dict = Dictionary(uniqueKeysWithValues: zip(keys, values))
    //
    //                               print(dict)
    //                               self.ticketArray.append(dict as [String : AnyObject])
    //
    //                               print(self.ticketArray)
    //
    //                               self.buyTicketTable.reloadData()
    //                           }
    //                        }
    // self.hideLoader()
    //
    //
    //                    }
    //
    //                }
            
            
            
            
        }
    static func addCategorys(category: fareRealmModel) -> Bool {
        let realm = try! Realm()
        if realm.object(ofType: fareRealmModel.self, forPrimaryKey: category.FareId) != nil {
            return false
        }
        //        try! realm.write {
        //            realm.add(category)
        //        }
        return true
    }
    func showLoader()
       {
           DispatchQueue.main.async {
               self.loaderView = UIView()
               self.loaderView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height+120)
               self.loaderView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
               let window = UIApplication.shared.keyWindow!
               
               let activityLoader = NVActivityIndicatorView(frame: CGRect.init(x: (self.view.frame.size.width/2)-40, y: (self.view.frame.size.height/2)-40, width: 80, height: 80), type: NVActivityIndicatorType.ballRotateChase, color: UIColor(red: 37/255.0, green: 114/255.0, blue: 176/255.0, alpha: 1.0))
               activityLoader.startAnimating()
               window.addSubview(self.loaderView)
               self.loaderView.addSubview(activityLoader)
           }
       }
       func hideLoader()
       {
           DispatchQueue.main.async {
               self.loaderView.removeFromSuperview()
           }
       }
    @objc func OnBackClicked() {
        if let vcs = self.navigationController?.viewControllers {
            let previousVC = vcs[vcs.count - 2]
            if previousVC is TicketViewController {
                // ... and so on
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "NewDashboardViewController") as! NewDashboardViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else {
                self.navigationController?.popViewController(animated: true)
            }
        }

        // self.navigationController?.popViewController(animated: true)


    }
    @objc func ticketViewAction()
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let tripPlannerViewController =  mainStoryboard.instantiateViewController(withIdentifier: "TicketViewController") as! TicketViewController
        self.navigationController!.pushViewController(tripPlannerViewController, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentCell
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        let dict = ticketArray[indexPath.row] as! NSDictionary
        let TitleName = dict["title"] as? String
        let priceName = dict["price"] as? String
        cell.selectionStyle = .none
        cell.titleName.text =  TitleName!
        let orginalStr = String(format: "$%@", priceName!)

        cell.priceLabel.text = orginalStr
        return cell
    
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let dict = ticketArray[indexPath.row] as! NSDictionary
               let TitleName = dict["title"] as? String
        
        let priceName = dict["price"] as! String
        fareID = (dict["FareId"] as! Int)
        print(fareID)
           let orginalStr = "$\(priceName)"
        titleString = ""
        titleString = TitleName! as NSString
        ticketPrice = orginalStr
        let pullUpController = makeSearchViewControllerIfNeeded()
               removePullUpController(pullUpController, animated: false)
        addPullUpController()

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ticketArray.count
       }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           let headerView  = UIView()
        headerView.backgroundColor = UIColor.init(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
           
           let AvalibleLabel = UILabel()
        AvalibleLabel.frame = CGRect.init(x: 10, y: 10, width: 200, height: 30)
           AvalibleLabel.text = "Available Fare:"
          AvalibleLabel.textColor = UIColor.init(red: 225/255.0, green: 0/255.0, blue: 3/255.0, alpha: 1.0)
        AvalibleLabel.font = UIFont.setTarcHeavy(size: 25)

                 if UIScreen.main.sizeType == .iPhone5{
                    AvalibleLabel.font = UIFont.setTarcHeavy(size: 25)

               }
           headerView.addSubview(AvalibleLabel)
          
           return headerView
       }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
              return 50
    }
    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
}
