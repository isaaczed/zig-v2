//
//  RealTimeScheduleBus.swift
//  ZIG
//
//  Created by Isaac on 11/06/19.
//  Copyright © 2019 Sanjeev. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
class paymentDynamic: Mappable {

    var Message : String?
    var list : [listArray]?
   
    
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        Message <- map["Message"]
        list <- map["list"]
       
    }
    
    
}







class listArray: Mappable {

    var Fareamount : Float?
    var CategoryId : Int?
    var FareId : Int?
    var Zonename : String?
    var RouteName : String?
    var Isactive : Bool?
    var Validtill : String?
    var Createddate : String?
    var Createdby : String?
    var AgencyId : Int?
    var Serverdate : String?
    var type : String?
    var Agencyname : String?
    var Category : String?
    var ZoneId : Int?
    var Message : String?
    var Farename : String?
    required init?(map: Map) {
           
       }
   func mapping(map: Map) {
       Fareamount <- map["Fareamount"]
       CategoryId <- map["CategoryId"]
       FareId <- map["FareId"]
       Zonename <- map["Zonename"]
       RouteName <- map["RouteName"]
       Isactive <- map["Isactive"]
       Validtill <- map["Validtill"]
    Createddate <- map["Createddate"]
    Createdby <- map["Createdby"]
    AgencyId <- map["AgencyId"]
    Serverdate <- map["Serverdate"]
    type <- map["Type"]
    Agencyname <- map["Agencyname"]
    Category <- map["Category"]
    ZoneId <- map["ZoneId"]
    Message <- map["Message"]
    Farename <- map["Farename"]

       
   }
    
}


class fareRealmModel: Object {
    @objc dynamic var Fareamount : Float = 0
    @objc dynamic var CategoryId = 0
    @objc dynamic var FareId = 0
    @objc dynamic var Zonename = ""
    @objc dynamic var RouteName = ""
    @objc dynamic var Isactive : Bool = false
    @objc dynamic var Validtill  = ""
    @objc dynamic var Createddate = ""
    @objc dynamic var AgencyId = 0
    @objc dynamic var Serverdate = ""
    @objc dynamic var type = ""
    @objc dynamic var Agencyname = ""
    @objc dynamic var Category = ""
    @objc dynamic var ZoneId = 0
    @objc dynamic var Message = ""
    @objc dynamic var Farename = "0"
    
    override class func primaryKey() -> String? {
        return "FareId"
    }
    
}
