//
//  WhatsNearMePanelViewController.swift
//  ZIG
//
//  Created by Isaac on 30/08/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import MapKit
import PullUpController
import Alamofire
import NVActivityIndicatorView
import NYAlertViewController
import SafariServices
import SwiftyJSON
import XLActionController

import LyftSDK
import UberCore
import UberRides
import AlamofireObjectMapper
import ObjectMapper
import Foundation
import SwiftGifOrigin

class NewTicketAddViewController: PullUpController {
    
    
    
    @IBOutlet weak var AlertButton: UIButton!
    
    enum InitialState {
        case contracted
        case expanded
    }
   
    var TitleString = NSString()
   var countLabel = UILabel()

    var fareID:Int?

    
    var initialState: InitialState = .expanded
   
    
    var walkingfromaddress:String?
    var walkingtoAddess:String?
    var AmenititesTypeTrip:String?
    var ameniticsTitle:String?
    var BikeCost:Int?
    var isLimeredirect =  NSString()
  
    var TicketValue =  NSString()
    var noofTickets: String?
       var ticketPrice: String?
    var halfScreenDesign = UIView()

    var indexValue = 0
  
    var minusButton = UIButton()
    var plusButton = UIButton()
    var  condtionScreenDesign = UIView()

    // MARK: - IBOutlets
   
    
    @IBOutlet private weak var visualEffectView: UIVisualEffectView!
    @IBOutlet private weak var searchBoxContainerView: UIView!
    @IBOutlet private weak var searchSeparatorView: UIView! {
        didSet {
            searchSeparatorView.layer.cornerRadius = searchSeparatorView.frame.height/2
        }
    }
    @IBOutlet private weak var firstPreviewView: UIView!
    @IBOutlet private weak var secondPreviewView: UIView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var destinationView: UIView!

    //new
    var dataPrevRespone:DataResponse<SuggestDirectionaClass>!
    var dataPrevBikeResponse: DataResponse<SuggestBikeDirectionalClass>!
    var googlereponseDirection = [[String:Any]]()
    var isUberAdded : Bool = false
    var isFirstScreenBool : Bool = true

    //
    var realtimeDataResponse:DataResponse<TransitRouteClass>!
       
       
     
       
      

    //
    var route_list = [mapsandSchudleList]()
    var initialPointOffset: CGFloat {
        switch initialState {
        case .contracted:
            return searchBoxContainerView?.frame.height ?? 0
        case .expanded:
            return pullUpControllerPreferredSize.height
        }
    }
    
    private var locations = [(title: String, location: CLLocationCoordinate2D)]()
    
    public var portraitSize: CGSize = .zero
    public var landscapeFrame: CGRect = .zero
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                              height: 300)
        landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 600)
        
        TicketValue = "1"
      
        
   pullUpControllerMoveToVisiblePoint(pullUpControllerMiddleStickyPoints[0], animated: true, completion: nil)

   
        simpleTicketScreen()
    }
    override func viewWillAppear(_ animated: Bool) {
        print("enw")
    }
    override func viewWillDisappear(_ animated: Bool) {
                print("enw")
      
    }
    func simpleTicketScreen()
    {
         halfScreenDesign = UIView()
        switch UIDevice.current.userInterfaceIdiom {
         case .phone:
         // It's an iPhone
         halfScreenDesign.frame = CGRect.init(x: 0, y: 5, width: self.view.frame.size.width, height: 300)
break
         case .pad:
        halfScreenDesign.frame = CGRect.init(x: 0, y: 5, width: self.view.frame.size.width, height: 400)

             break
         // It's an iPad
         case .unspecified: break
         // Uh, oh! What could it be?
         case .tv: break
             
         case .carPlay: break
             
         }
        halfScreenDesign.frame = CGRect.init(x: 0, y: 5, width: self.view.frame.size.width, height: 300)

        halfScreenDesign.backgroundColor = UIColor.white
        halfScreenDesign.layer.cornerRadius = 10
        //halfScreenDesign.dropShadow(scale: true)
        
        
        let shadowSize : CGFloat = 3.0
                      let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                                 y: -shadowSize / 2,
                                                                 width: halfScreenDesign.frame.size.width + shadowSize,
                                                                 height: halfScreenDesign.frame.size.height + shadowSize))
                      halfScreenDesign.layer.masksToBounds = false
                      halfScreenDesign.layer.shadowColor = UIColor.black.cgColor
                      halfScreenDesign.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
               halfScreenDesign.layer.shadowOpacity = 0.3
               halfScreenDesign.layer.shadowPath = shadowPath.cgPath
        
        
        
        
        
//
//        let shadowSize : CGFloat = 5.0
//        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
//                                                   y: -shadowSize / 2,
//                                                   width: halfScreenDesign.frame.size.width + shadowSize,
//                                                   height: halfScreenDesign.frame.size.height + shadowSize))
//        halfScreenDesign.layer.masksToBounds = false
//        halfScreenDesign.layer.shadowColor = UIColor.black.cgColor
//        halfScreenDesign.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//        halfScreenDesign.layer.shadowOpacity = 0.3
//        halfScreenDesign.layer.shadowPath = shadowPath.cgPath
        self.view .addSubview(halfScreenDesign)
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect.init(x: 35, y: 10, width: 200, height: 30)
        titleLabel.text = "TARC"
        titleLabel.font = UIFont.setTarcHeavy(size: 15.0)
        titleLabel.textColor = UIColor.init(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0)
        halfScreenDesign .addSubview(titleLabel)
        
        let titleSubLabel = UILabel()
        titleSubLabel.frame = CGRect.init(x: 35, y: titleLabel.frame.maxY+5, width: 200, height: 30)
        titleSubLabel.text = TitleString as String
        titleSubLabel.font = UIFont.setTarcSemiBold(size: 15.0)
        titleSubLabel.textColor = UIColor.black
        halfScreenDesign .addSubview(titleSubLabel)
        
        let tarcLogo = UIImageView()
        tarcLogo.frame = CGRect.init(x: halfScreenDesign.frame.maxX-80, y: 10, width: 50, height: 50)
        tarcLogo.image = UIImage.init(named: "tarclogo")
        halfScreenDesign .addSubview(tarcLogo)

        let countScreenView = UIView()
        
        switch UIDevice.current.userInterfaceIdiom {
         case .phone:
         // It's an iPhone
         countScreenView.frame  = CGRect.init(x: 30, y: titleSubLabel.frame.maxY+30, width:halfScreenDesign.frame.size.width - 60 , height: halfScreenDesign.frame.size.height / 2)
break
         case .pad:
            countScreenView.frame  = CGRect.init(x: 30, y: titleSubLabel.frame.maxY+30, width:halfScreenDesign.frame.size.width - 60 , height: halfScreenDesign.frame.size.height / 1.8)

             break
         // It's an iPad
         case .unspecified: break
         // Uh, oh! What could it be?
         case .tv: break
             
         case .carPlay: break
             
         }
        
        countScreenView.backgroundColor = UIColor.init(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1.0)
        countScreenView.layer.cornerRadius = 5
        countScreenView.layer.masksToBounds = true
        halfScreenDesign .addSubview(countScreenView)

        countscreenDesign(subView: countScreenView)
    }
    
    @objc func minusFuction(sender:UIButton)
    {
        var setMinimumValue = Int()
        var setMaximumValue = Int()
        setMinimumValue  = 1
        setMaximumValue = 6

        if  TitleString .isEqual(to: "Single Ride")
              {
                 // minStr = "4 hours"
                setMinimumValue  = 1
                setMaximumValue = 6
              }
              else if  TitleString .isEqual(to: "24 Hour Pass")
              {
                //   minStr = "24 hours"
                setMinimumValue  = 1
                setMaximumValue = 6
              }
              else if  TitleString .isEqual(to: "7 Day Pass")
              {
                 //  minStr = "7 days"
              }
              else if  TitleString .isEqual(to: "30 Day Pass")
              {
                  // minStr = "30 days"
                               setMaximumValue = 6
              }
        
        
        if sender.tag == 1
        {
            print(TicketValue)
           var A = Int()// firstText is UITextField
A = Int(TicketValue.intValue)
            if A >= setMaximumValue
                   {
                   print(TicketValue)
                    plusButton.setTitleColor(UIColor.lightGray, for: .normal)

            }
            else
            {
            var A = Int()
                   var b = Int()
                   A = Int(TicketValue.intValue)
                   b = A + 1
                   TicketValue = String(b) as NSString
            countLabel.text = TicketValue as String
            minusButton.setTitleColor(UIColor.init(red: 0/255.0, green: 121/255.0, blue: 201/225.0, alpha: 1.0), for: .normal)
            }
        }
        else
        {
            plusButton.setTitleColor(UIColor.init(red: 0/255.0, green: 121/255.0, blue: 201/225.0, alpha: 1.0), for: .normal)

            if TicketValue as String == "\(setMinimumValue)"
        {
            countLabel.text = "\(setMinimumValue)"
            minusButton.setTitleColor(UIColor.lightGray, for: .normal)


        }
        else
        {
            var A = Int()
            var b = Int()
            A = Int(TicketValue.intValue)
            b = A - 1
            TicketValue = String(b) as NSString
            countLabel.text = TicketValue as String
            if TicketValue as String == "\(setMinimumValue)"
                   {
                       minusButton.setTitleColor(UIColor.lightGray, for: .normal)


                   }

        }
        }
    }
    
    func countscreenDesign(subView:UIView)
    {
         minusButton = UIButton()
        minusButton.frame = CGRect.init(x: subView.frame.size.width/2-100, y: 10, width: 50, height: 50)
        minusButton.setTitle("-", for: .normal)
        minusButton.titleLabel!.font = UIFont.setTarcHeavy(size: 30.0)
        minusButton.setTitleColor(UIColor.lightGray, for: .normal)
        minusButton.backgroundColor = UIColor.clear
        minusButton.tag = 2
        minusButton.addTarget(self, action: #selector(minusFuction), for: .touchUpInside)
        subView .addSubview(minusButton)
        
         countLabel = UILabel()
        countLabel.frame = CGRect.init(x: minusButton.frame.maxX+5, y: 10, width: 80, height: 60)
        
        var setMinimumValue = Int()
        setMinimumValue  = 1
        if  TitleString .isEqual(to: "Single Ride")
              {
                 // minStr = "4 hours"
                setMinimumValue  = 1
              }
              else if  TitleString .isEqual(to: "24 Hour Pass")
              {
                //   minStr = "24 hours"
                setMinimumValue  = 1
              }
              else if  TitleString .isEqual(to: "7 Day Pass")
              {
                 //  minStr = "7 days"
              }
              else if  TitleString .isEqual(to: "30 Day Pass")
              {
                  // minStr = "30 days"
              }
        
        TicketValue = "\(setMinimumValue)" as NSString

        
        countLabel.text = "\(setMinimumValue)"
        countLabel.textAlignment = .center
        countLabel.font = UIFont.setTarcBold(size: 30)
        countLabel.textColor = UIColor.init(red: 0/255.0, green: 121/255.0, blue: 201/225.0, alpha: 1.0)
        countLabel.backgroundColor = UIColor.clear
        subView .addSubview(countLabel)
        
        plusButton = UIButton()
        plusButton.frame = CGRect.init(x: countLabel.frame.maxX+10, y: 10, width: 50, height: 50)
        plusButton.setTitle("+", for: .normal)
        plusButton.setTitleColor(UIColor.init(red: 0/255.0, green: 121/255.0, blue: 201/225.0, alpha: 1.0), for: .normal)
        plusButton.titleLabel!.font = UIFont.setTarcHeavy(size: 30.0)
        plusButton.backgroundColor = UIColor.clear
        plusButton.tag = 1
        plusButton.addTarget(self, action: #selector(minusFuction), for: .touchUpInside)
        subView .addSubview(plusButton)
        
        
        let numberofTickets = UILabel()
        numberofTickets.frame = CGRect.init(x: 0, y: plusButton.frame.maxY + 10, width: subView.frame.size.width, height: 20)
        numberofTickets.text = "Number of Tickets"
        numberofTickets.textAlignment = .center
        numberofTickets.font = UIFont.setTarcSemiBold(size: 15.0)
        subView .addSubview(numberofTickets)

        let SelectButton = UIButton()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
        // It's an iPhone
        SelectButton.frame = CGRect.init(x: 30, y: numberofTickets.frame.maxY + 10, width: subView.frame.size.width-60, height: 40)
break
        case .pad:
           SelectButton.frame = CGRect.init(x: 30, y: numberofTickets.frame.maxY + 10, width: subView.frame.size.width-60, height: 50)

            break
        // It's an iPad
        case .unspecified: break
        // Uh, oh! What could it be?
        case .tv: break
            
        case .carPlay: break
            
        }
        SelectButton.setTitle("Select", for: .normal)
        SelectButton.setTitleColor(UIColor.white, for: .normal)
        SelectButton.titleLabel!.font = UIFont.setTarcHeavy(size: 20.0)
        SelectButton.backgroundColor = UIColor.init(red: 225/255.0, green: 0/255.0, blue: 3/255.0, alpha: 1.0)
        SelectButton.addTarget(self, action: #selector(selectorAction), for: .touchUpInside)
        SelectButton.layer.cornerRadius = 5
        SelectButton.layer.masksToBounds = true
        subView .addSubview(SelectButton)

    }
    
    @objc func selectorAction()
    {
        halfScreenDesign.isHidden = true
       portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                                     height: 550)
        landscapeFrame = CGRect(x: 5, y: 50, width: 280, height: 600)
               isFirstScreenBool = true
        pullUpControllerMoveToVisiblePoint(pullUpControllerDidMove[0], animated: true, completion: nil)
        conditionScreen()
    }
    @objc func backPopupAction()
    {
        condtionScreenDesign.isHidden = true
        halfScreenDesign.isHidden = false
        portraitSize = CGSize(width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height),
                                    height: 300)
            isFirstScreenBool = false
          pullUpControllerMoveToVisiblePoint(pullUpControllerDidMove[0], animated: true, completion: nil)
        
    }
    func conditionScreen()
    {
              condtionScreenDesign = UIView()
           condtionScreenDesign.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 900)
        
        
           condtionScreenDesign.backgroundColor = UIColor.white
           condtionScreenDesign.layer.cornerRadius = 10
            let shadowSize : CGFloat = 5.0
               let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                          y: -shadowSize / 2,
                                                          width: condtionScreenDesign.frame.size.width + shadowSize,
                                                          height: condtionScreenDesign.frame.size.height + shadowSize))
               condtionScreenDesign.layer.masksToBounds = false
               condtionScreenDesign.layer.shadowColor = UIColor.black.cgColor
               condtionScreenDesign.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        condtionScreenDesign.layer.shadowOpacity = 0.3
        condtionScreenDesign.layer.shadowPath = shadowPath.cgPath
        self.view .addSubview(condtionScreenDesign)
        
        let backButton = UIButton()
        backButton.frame = CGRect.init(x: 10, y: 10, width: 30, height: 30)
        backButton.setImage(UIImage.init(named: "Back-Btn"), for: .normal)
        backButton.addTarget(self, action: #selector(backPopupAction), for: .touchUpInside)
        backButton.accessibilityLabel = "Back"

        condtionScreenDesign .addSubview(backButton)

        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect.init(x: 15, y: backButton.frame.maxY+10, width: 200, height: 25)
        titleLabel.text = "TARC"
        titleLabel.font = UIFont.setTarcHeavy(size: 15.0)
        titleLabel.textColor = UIColor.init(red: 140/255, green: 140/255, blue: 140/255, alpha: 1.0)
        condtionScreenDesign .addSubview(titleLabel)
               
               let titleSubLabel = UILabel()
               titleSubLabel.frame = CGRect.init(x: 15, y: titleLabel.frame.maxY+5, width: 200, height: 30)
               titleSubLabel.text = TitleString as String
        titleSubLabel.font = UIFont.setTarcSemiBold(size: 15.0)
               titleSubLabel.textColor = UIColor.black
               condtionScreenDesign .addSubview(titleSubLabel)
        
        
        let ticketRules = UILabel()
        ticketRules.frame  = CGRect.init(x: 15, y: titleSubLabel.frame.maxY+5, width: condtionScreenDesign.frame.size.width, height: 30)
        ticketRules.text = "Disclaimer acceptance"
        ticketRules.textColor = UIColor.init(red: 225/255.0, green: 0/255.0, blue: 3/255.0, alpha: 1.0)
        ticketRules.font = UIFont.setTarcHeavy(size: 30)

          if UIScreen.main.sizeType == .iPhone5{
            ticketRules.font = UIFont.setTarcHeavy(size: 25)

        }
        condtionScreenDesign .addSubview(ticketRules)

        let detailTextfield = UILabel()
        detailTextfield.frame = CGRect.init(x: 20, y: ticketRules.frame.maxY+5, width: condtionScreenDesign.frame.size.width - 20, height: 130)
        detailTextfield.font = UIFont.setTarcSemiBold(size: 14.0)
        detailTextfield.textColor = UIColor.init(red: 125/255, green: 153/255, blue: 167/255, alpha: 1.0)
        detailTextfield.numberOfLines = 0
        var font = UIFont.setTarcSemiBold(size: 14.0)
         if UIScreen.main.sizeType == .iPhone5{
            font = UIFont.setTarcSemiBold(size: 12.0)
        }
        var minStr =  NSString()
        if  TitleString .isEqual(to: "Single Ride")
        {
            minStr = "4 hours"
        }
        else if  TitleString .isEqual(to: "24 Hour Pass")
        {
             minStr = "24 hours"
        }
        else if  TitleString .isEqual(to: "7 Day Pass")
        {
             minStr = "7 days"
        }
        else if  TitleString .isEqual(to: "30 Day Pass")
        {
             minStr = "30 days"
        }
        else
        {
             minStr = "2 hours"
        }
        
 var string = "Tickets expire 180 days from purchase date.\n\n\(TitleString) Tickets can be used for \(minStr) after activation.\n\nNo refunds or exchanges.\n\nTitle VI: No person in the united state shall on the  ground of race ,color or nation orgin be excluded from participation in be denied the benefits of or be subjected to discrimination under any program or activity receiving Federal financial assistance.\n\nActivate just prior to boarding the bus.\n\nI accept the Terms and Condition." as NSString
        if UIScreen.main.sizeType == .iPhone5{
              string = "Tickets expire 180 days from purchase date.\n\(TitleString) Tickets can be used for \(minStr) after activation.\nNo refunds or exchanges.\nTitle VI: No person in the united state shall on the  ground of race ,color or nation orgin be excluded from participation in be denied the benefits of or be subjected to discrimination under any program or activity receiving Federal financial assistance.\n\nActivate just prior to boarding the bus.\n\nI accept the Terms and Condition."  as NSString
                          
                      }
        let height = heightForView(text: string as String, font: font, width:condtionScreenDesign.frame.size.width - 20 )
        
        detailTextfield.attributedText = attributedText(minStr:minStr)
        condtionScreenDesign .addSubview(detailTextfield)
        detailTextfield.frame = CGRect.init(x: 15, y: ticketRules.frame.maxY+5, width: condtionScreenDesign.frame.size.width - 20, height: height)

        let acceptButton = UIButton()
        switch UIDevice.current.userInterfaceIdiom {
                   case .phone:
                   // It's an iPhone
                    if UIScreen.main.sizeType == .iPhone5{
                        acceptButton.frame = CGRect.init(x: 30, y: detailTextfield.frame.maxY+10, width: detailTextfield.frame.size.width-60, height: 40)

                    }
                    else
                    {
                        acceptButton.frame = CGRect.init(x: 30, y: detailTextfield.frame.maxY+0, width: detailTextfield.frame.size.width-60, height: 50)

                    }
break
                   case .pad:
                               acceptButton.frame = CGRect.init(x: 30, y: detailTextfield.frame.maxY+40, width: detailTextfield.frame.size.width-60, height: 50)

                       break
                   // It's an iPad
                   case .unspecified: break
                   // Uh, oh! What could it be?
                   case .tv: break
                       
                   case .carPlay: break
                       
                   }
                      acceptButton.setTitle("Accept", for: .normal)
        acceptButton.setTitleColor(UIColor.white, for: .normal)
        acceptButton.titleLabel!.font = UIFont.setTarcHeavy(size: 20.0)
        acceptButton.backgroundColor = UIColor.init(red: 225/255.0, green: 0/255.0, blue: 3/255.0, alpha: 1.0)
        acceptButton.layer.cornerRadius = 5
        acceptButton.layer.masksToBounds = true
        acceptButton.addTarget(self, action: #selector(acceptAction), for: .touchUpInside)
        condtionScreenDesign .addSubview(acceptButton)
        
        condtionScreenDesign.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 180+height+500)
    
    }
    func attributedText(minStr:NSString) -> NSAttributedString {
        
        var string = "Tickets expire 180 days from purchase date.\n\n\(TitleString) Tickets can be used for \(minStr) after activation.\n\nNo refunds or exchanges.\n\nTitle VI: No person in the united state shall on the  ground of race ,color or nation orgin be excluded from participation in be denied the benefits of or be subjected to discrimination under any program or activity receiving Federal financial assistance.\nActivate just prior to boarding the bus.\n\nI accept the Terms and Condition." as NSString

         if UIScreen.main.sizeType == .iPhone5{
        string = "Tickets expire 180 days from purchase date.\n\(TitleString) Tickets can be used for \(minStr) after activation.\nNo refunds or exchanges.\nTitle VI: No person in the united state shall on the  ground of race ,color or nation orgin be excluded from participation in be denied the benefits of or be subjected to discrimination under any program or activity receiving Federal financial assistance.\n\nActivate just prior to boarding the bus.\n\nI accept the Terms and Condition."  as NSString
                    
                }

        var attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:UIFont.setTarcSemiBold(size: 14.0)])
 if UIScreen.main.sizeType == .iPhone5{
    attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:UIFont.setTarcSemiBold(size: 12.0)])
        }
        var boldFontAttribute = [NSAttributedStringKey.font: UIFont.setTarcBold(size: 15.0),NSAttributedStringKey.foregroundColor: UIColor.black]
 if UIScreen.main.sizeType == .iPhone5{
     boldFontAttribute = [NSAttributedStringKey.font: UIFont.setTarcBold(size: 12.0),NSAttributedStringKey.foregroundColor: UIColor.black]
        }
        // Part of string to be bold
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "180 days"))
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "\(minStr)"))
        attributedString.addAttributes(boldFontAttribute, range: string.range(of: "No refunds or exchanges."))

        // 4
        return attributedString
    }
     @objc func acceptAction()
        {
            print("\(fareID)")
         let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         let supportTripVC =  mainStoryboard.instantiateViewController(withIdentifier: "paymentViewController") as! paymentViewController
            supportTripVC.alreadyCardString = "true"
            supportTripVC.ticketPrice = ticketPrice
            supportTripVC.noofTickets = countLabel.text
            supportTripVC.ticketType = TitleString as String
            supportTripVC.fareID = fareID
         self.navigationController!.pushViewController(supportTripVC, animated: true)
        }
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect.init(x:0,y: 0,width: width, height:CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.layer.cornerRadius = 12
    }
    
    override func pullUpControllerWillMove(to stickyPoint: CGFloat) {
        //        print("will move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidMove(to stickyPoint: CGFloat) {
        //        print("did move to \(stickyPoint)")
    }
    
    override func pullUpControllerDidDrag(to point: CGFloat) {
        //        print("did drag to \(point)")
    }
    

    // MARK: - PullUpController
    
    override var pullUpControllerPreferredSize: CGSize {
        return portraitSize
    }
    
    override var pullUpControllerPreferredLandscapeFrame: CGRect {
        return landscapeFrame
    }
     var pullUpControllerDidMove: [CGFloat] {
        if isFirstScreenBool == true
        {
            return  [500]

        }
        else
        {
            return  [300]

        }
    }
    override var pullUpControllerMiddleStickyPoints: [CGFloat] {
    

        switch initialState {
        case .contracted:
            return [30]
        case .expanded:
            return [300, 60]
        }
    }
    
    override var pullUpControllerBounceOffset: CGFloat {
        return 20
    }
    
    override func pullUpControllerAnimate(action: PullUpController.Action,
                                          withDuration duration: TimeInterval,
                                          animations: @escaping () -> Void,
                                          completion: ((Bool) -> Void)?) {
        switch action {
        case .move:
            UIView.animate(withDuration: 0.3,
                           delay: 0,
                           usingSpringWithDamping: 0.7,
                           initialSpringVelocity: 0,
                           options: .curveEaseInOut,
                           animations: animations,
                           completion: completion)
        default:
            UIView.animate(withDuration: 0.3,
                           animations: animations,
                           completion: completion)
        }
    }

    
}

extension UIView {

  // OUTPUT 1
  func dropShadow(scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = CGSize(width: -1, height: -1)
    layer.shadowRadius = 1

    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }

  // OUTPUT 2
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}

