//
//  FeedbackViewController.swift
//  ZIG
//
//  Created by Isaac on 18/05/19.
//  Copyright © 2019 Isaac. All rights reserved.
//

import UIKit
import Toast_Swift
import Alamofire

class FeedbackViewController: UIViewController,RatingViewDelegate, UITextViewDelegate {
     @IBOutlet weak var SendBtn: UIButton!
    @IBOutlet weak var PopUpWindow: UIView!
    @IBOutlet weak var StarRating: StarRateView!
    
    var checkBox: UIButton!

    @IBOutlet weak var closebutton: UIButton!
    @IBOutlet weak var CommandTxt: UITextView!
    
    var starratingValue:Int?
    var DNDBool =  Bool()
    
    //Action
    @objc func tapDetected() {
        let defaults = UserDefaults.standard
       
           // defaults.set("yes", forKey:"feedback")
        defaults.removeObject(forKey: "feedback")
            defaults.synchronize()
       self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
   setupRatingView()
      CommandTxt.delegate = self
        DNDBool = true
        CommandTxt.text = "Describe your experience (optional)"
        CommandTxt.textColor = UIColor.lightGray
     
        SendBtn.tintColor = .white
        SendBtn.layer.cornerRadius = 8
        //SendBtn.rightImage(image: UIImage(named: "Sendarrow")!, renderMode: .alwaysOriginal)
        
        SendBtn.setImage(UIImage(named: "Sendarrow"), for: UIControl.State.normal)
        SendBtn.semanticContentAttribute = .forceRightToLeft
        SendBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 9, bottom: 0, right: 0)
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        closebutton.isUserInteractionEnabled = true
        closebutton.addGestureRecognizer(singleTap)
        
        checkBox = UIButton()
        checkBox.frame = CGRect.init(x: 10, y: SendBtn.frame.maxY+8, width: PopUpWindow.frame.size.width-20, height: 25)
        checkBox .setImage(UIImage.init(named: "checkbox_in"), for: .normal)
        checkBox.setTitle("Do not Show Again", for: .normal)
        checkBox.titleLabel?.font = UIFont.setTarcRegular(size: 17)
        checkBox.setTitleColor(UIColor.black, for: .normal)
        checkBox.addTarget(self, action: #selector(checkBoxAction), for: .touchUpInside)
        PopUpWindow .addSubview(checkBox)
        
    }
    @objc func checkBoxAction()
    {
        if DNDBool == true
        {
            checkBox .setImage(UIImage.init(named: "checkbox_out"), for: .normal)
            DNDBool = false
        }
        else
        {
            checkBox .setImage(UIImage.init(named: "checkbox_in"), for: .normal)
           DNDBool = true
        }
    }
    func setupRatingView() {
        StarRating.delegate = self
        StarRating.ratingValue = -1
    }
    func updateRatingFormatValue(_ value: Int) {
        print("Rating : = ", value)
        starratingValue = value
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if CommandTxt.textColor == UIColor.lightGray {
            CommandTxt.text = nil
            CommandTxt.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if CommandTxt.text.isEmpty {
            CommandTxt.text = "Describe your experience (optional)"
            CommandTxt.textColor = UIColor.lightGray
        }
    }
    @IBAction func SubmitCommand(_ sender: Any) {
   
        if starratingValue != nil && starratingValue != 0{
             let accessToken = UserDefaults.standard.value(forKey: "accessToken")
            let parameters_Feed: [String: Any] = [
                "Token" : accessToken!,
                "AgencyId" : 1,
                "AppRatingId" : starratingValue!,
                "Stars":starratingValue!,
                "Comments":CommandTxt.text,
                "Misc":""
                
            ]
            
            let Url_Reset = "\(APIUrl.ZIGTARCAPI)Feedback/AppRating/Add"
            Alamofire.request(Url_Reset, method: .post, parameters: parameters_Feed,encoding: URLEncoding.default)
                .responseObject{ (response: DataResponse<ResetPasswordModel>) in
                    switch response.result {
                    case .success:
                        print(response)
                        //to get status code
                        if response.result.value?.Message == "Rating added Successfully"
                        {
                            if self.DNDBool == true
                            {
                            let defaults = UserDefaults.standard
                            defaults.set("Yes", forKey:"feedback")
                            defaults.synchronize()
                            }
                            self.view.makeToast("Thank you for rating our app!")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                           self.dismiss(animated: true, completion: nil)
                            })
                            
                        }
                        else{
                            self.view.makeToast(response.result.value?.Message)
                            
                        }
                    case .failure(let error):
                        print(error)
                        
                    }
                    
            }
            
            
        }
        else{
            self.view.makeToast("Kindly select the star rating.")
        }
    
    }
    
}
